<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>List</customSettingsType>
    <description>Stores field mapping for JIRA integration.  Mapping is from source field name to target field name.</description>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>alm_pm2__Active__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Used to indicate that a field mapping should be enabled</description>
        <externalId>false</externalId>
        <inlineHelpText>Used to indicate that a field mapping should be enabled</inlineHelpText>
        <label>Active?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>alm_pm2__Is_Relational__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Used to indicate that a field mapping is for a relational field in JIRA</description>
        <externalId>false</externalId>
        <inlineHelpText>Used to indicate that a field mapping is for a relational field in JIRA</inlineHelpText>
        <label>Is Relational</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>alm_pm2__Property_Name__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The name of the property in JIRA if the Target Field Type is &quot;object&quot;</inlineHelpText>
        <label>Property Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>alm_pm2__Source_Field__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The API name for the source field</inlineHelpText>
        <label>Source Field</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>alm_pm2__Source_Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The API name for the source SObject Type</inlineHelpText>
        <label>Source Type</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>alm_pm2__Target_Field_Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The type of field in the target. Types supported are &quot;string&quot;, &quot;object&quot;, and &quot;array&quot; Blank types will be treated as strings.</inlineHelpText>
        <label>Target Field Type</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>alm_pm2__Target_Field__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Stores the API name of the field in the target.</inlineHelpText>
        <label>Target Field</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>JIRA Field Mapping</label>
    <visibility>Public</visibility>
</CustomObject>
