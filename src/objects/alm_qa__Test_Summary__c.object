<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Summary of test results grouped by test date.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>alm_qa__Apex_Tests_Rollup__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of Apex Test Classes.  Apex test classes with unit tests.</inlineHelpText>
        <label>Apex Tests Rollup</label>
        <summaryFilterItems>
            <field>alm_qa__Test_Result__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Apex Test Class</value>
        </summaryFilterItems>
        <summaryForeignKey>alm_qa__Test_Result__c.alm_qa__Test_Summary__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>alm_qa__Apex_Tests__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of Apex Test Classes.  Apex test classes with unit tests.</inlineHelpText>
        <label>Apex Tests</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>alm_qa__Automated_Tests_Rollup__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of automated functional tests (Selenium scripts).</inlineHelpText>
        <label>Automated Tests Rollup</label>
        <summaryFilterItems>
            <field>alm_qa__Test_Result__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Test Script</value>
        </summaryFilterItems>
        <summaryForeignKey>alm_qa__Test_Result__c.alm_qa__Test_Summary__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>alm_qa__Automated_Tests__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of automated functional tests (Selenium scripts).</inlineHelpText>
        <label>Automated Tests</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>alm_qa__Completed__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Indicates when a BUTR run is finished.  Used to trigger email notification workflow.</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates when a BUTR run is finished.  Used to trigger email notification workflow.</inlineHelpText>
        <label>Completed</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>alm_qa__Error_Details__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Log field for detailed error messages from testing.</inlineHelpText>
        <label>Error Details</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>alm_qa__Failed_Apex_Tests_Rollup__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of failed Apex Test Classes. Apex test classes with unit tests.</inlineHelpText>
        <label>Failed Apex Tests Rollup</label>
        <summaryFilterItems>
            <field>alm_qa__Test_Result__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Apex Test Class</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>alm_qa__Test_Result__c.alm_qa__Result__c</field>
            <operation>equals</operation>
            <value>Fail</value>
        </summaryFilterItems>
        <summaryForeignKey>alm_qa__Test_Result__c.alm_qa__Test_Summary__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>alm_qa__Failed_Apex_Tests__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of failed Apex Test Classes. Apex test classes with unit tests.</inlineHelpText>
        <label>Failed Apex Tests</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>alm_qa__Failed_Automated_Tests_Rollup__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of failed automated functional tests (Selenium scripts).</inlineHelpText>
        <label>Failed Automated Tests Rollup</label>
        <summaryFilterItems>
            <field>alm_qa__Test_Result__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Test Script</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>alm_qa__Test_Result__c.alm_qa__Result__c</field>
            <operation>equals</operation>
            <value>Fail</value>
        </summaryFilterItems>
        <summaryForeignKey>alm_qa__Test_Result__c.alm_qa__Test_Summary__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>alm_qa__Failed_Automated_Tests__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of failed automated functional tests (Selenium scripts).</inlineHelpText>
        <label>Failed Automated Tests</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>alm_qa__Failed_Manual_Tests_Rollup__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of failed manual tests.</inlineHelpText>
        <label>Failed Manual Tests Rollup</label>
        <summaryFilterItems>
            <field>alm_qa__Test_Result__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Test Case</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>alm_qa__Test_Result__c.alm_qa__Result__c</field>
            <operation>equals</operation>
            <value>Fail</value>
        </summaryFilterItems>
        <summaryForeignKey>alm_qa__Test_Result__c.alm_qa__Test_Summary__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>alm_qa__Failed_Manual_Tests__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of failed manual tests.</inlineHelpText>
        <label>Failed Manual Tests</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>alm_qa__Instance_Name__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The instance where the tests were executed in.</inlineHelpText>
        <label>Instance Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Production</fullName>
                    <default>false</default>
                    <label>Production</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>alm_qa__Lock_Summary_Values__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Locks summary fields (Total_...) and prevents any result changes to get modify them.</inlineHelpText>
        <label>Lock Summary Values</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>alm_qa__Manual_Tests_Rollup__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of manual tests.</inlineHelpText>
        <label>Manual Tests Rollup</label>
        <summaryFilterItems>
            <field>alm_qa__Test_Result__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Test Case</value>
        </summaryFilterItems>
        <summaryForeignKey>alm_qa__Test_Result__c.alm_qa__Test_Summary__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>alm_qa__Manual_Tests__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of manual tests.</inlineHelpText>
        <label>Manual Tests</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>alm_qa__Result__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(alm_qa__System_Error__c, &quot;Fail&quot;,
IF(AND(alm_qa__Total_Failed_Tests__c = 0, alm_qa__Total_Coverage__c = 0), &quot;Pass&quot;, 
IF(AND(alm_qa__Total_Failed_Tests__c = 0, alm_qa__Total_Coverage__c &gt;= 0.75), &quot;Pass&quot;, &quot;Fail&quot; ) 
))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The overall test result.  Total Coverage must be 75% or higher and there are no failed test results.</inlineHelpText>
        <label>Result</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>alm_qa__Success_Rate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(  alm_qa__Total_Tests_Formula__c  != 0 , (  alm_qa__Total_Tests_Formula__c  -  alm_qa__Total_Failed_Tests_Formula__c  )  /   alm_qa__Total_Tests_Formula__c  , NULL)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The percentage of passing tests.</inlineHelpText>
        <label>Success Rate</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>alm_qa__System_Error__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>System Error</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>alm_qa__Test_Date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The test date.</inlineHelpText>
        <label>Test Date</label>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>alm_qa__Time_s__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The time (in seconds) it took to run all the automated test scripts, Apex Code Coverage and Apex classes with unit tests.</inlineHelpText>
        <label>Time (s)</label>
        <summarizedField>alm_qa__Test_Result__c.alm_qa__Time__c</summarizedField>
        <summaryFilterItems>
            <field>alm_qa__Test_Result__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Test Case</value>
        </summaryFilterItems>
        <summaryForeignKey>alm_qa__Test_Result__c.alm_qa__Test_Summary__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>alm_qa__Total_Apex_Unit_Tests_Rollup__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of Apex unit tests executed.</inlineHelpText>
        <label>Total Apex Unit Tests Rollup</label>
        <summarizedField>alm_qa__Test_Result__c.alm_qa__Total_Unit_Tests__c</summarizedField>
        <summaryFilterItems>
            <field>alm_qa__Test_Result__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Apex Test Class</value>
        </summaryFilterItems>
        <summaryForeignKey>alm_qa__Test_Result__c.alm_qa__Test_Summary__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>alm_qa__Total_Apex_Unit_Tests__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of Apex unit tests executed.</inlineHelpText>
        <label>Total Apex Unit Tests</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>alm_qa__Total_Coverage__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Total code coverage of all Apex classes and triggers.  75% is needed to pass.</inlineHelpText>
        <label>Total Coverage</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>alm_qa__Total_Failed_Apex_Unit_Tests_Rollup__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of failed unit tests.</inlineHelpText>
        <label>Total Failed Apex Unit Tests Rollup</label>
        <summarizedField>alm_qa__Test_Result__c.alm_qa__Total_Failed_Unit_Tests__c</summarizedField>
        <summaryFilterItems>
            <field>alm_qa__Test_Result__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Apex Test Class</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>alm_qa__Test_Result__c.alm_qa__Result__c</field>
            <operation>equals</operation>
            <value>Fail</value>
        </summaryFilterItems>
        <summaryForeignKey>alm_qa__Test_Result__c.alm_qa__Test_Summary__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>alm_qa__Total_Failed_Apex_Unit_Tests__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of failed unit tests.</inlineHelpText>
        <label>Total Failed Apex Unit Tests</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>alm_qa__Total_Failed_Tests_Formula__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>alm_qa__Failed_Apex_Tests_Rollup__c  +  alm_qa__Failed_Automated_Tests_Rollup__c +  alm_qa__Failed_Manual_Tests_Rollup__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The total number of failed tests. Failed Apex Tests + Failed Automated Tests + Failed Manual Tests.</inlineHelpText>
        <label>Total Failed Tests Formula</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>alm_qa__Total_Failed_Tests__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of failed tests.  Failed Apex Tests + Failed Automated Tests + Failed Manual Tests.</inlineHelpText>
        <label>Total Failed Tests</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>alm_qa__Total_Tests_Formula__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>alm_qa__Apex_Tests_Rollup__c +  alm_qa__Automated_Tests_Rollup__c +  alm_qa__Manual_Tests_Rollup__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The total number of all tests. Apex Tests + Automated Tests + Manual Tests.</inlineHelpText>
        <label>Total Tests Formula</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>alm_qa__Total_Tests__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of all tests.  Apex Tests + Automated Tests + Manual Tests.</inlineHelpText>
        <label>Total Tests</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>alm_qa__VF_Date_Hook__c</fullName>
        <deprecated>false</deprecated>
        <description>Field to accept date ranges for deleting testing summaries from the Testing Management VF page</description>
        <externalId>false</externalId>
        <label>VF Date Hook</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <label>Test Summary</label>
    <listViews>
        <fullName>alm_qa__All</fullName>
        <columns>NAME</columns>
        <columns>alm_qa__Instance_Name__c</columns>
        <columns>alm_qa__Test_Date__c</columns>
        <columns>alm_qa__Result__c</columns>
        <columns>alm_qa__Total_Coverage__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>TS-{00000}</displayFormat>
        <label>Test Summary</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Test Summaries</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>alm_qa__Automated</fullName>
        <active>true</active>
        <description>A test summary of autmoated test results</description>
        <label>Automated</label>
        <picklistValues>
            <picklist>alm_qa__Instance_Name__c</picklist>
            <values>
                <fullName>Production</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>alm_qa__Manual</fullName>
        <active>true</active>
        <description>A test summary of manual test results</description>
        <label>Manual</label>
        <picklistValues>
            <picklist>alm_qa__Instance_Name__c</picklist>
            <values>
                <fullName>Production</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>alm_qa__Test_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>alm_qa__Instance_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>alm_qa__Result__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>alm_qa__Test_Date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>alm_qa__Instance_Name__c</lookupDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
