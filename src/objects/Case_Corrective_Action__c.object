<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Junction object to relate Corrective Actions to the Case complaints they are responding to</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Action_Date__c</fullName>
        <externalId>false</externalId>
        <label>Action Due Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Action_Monitored_Method__c</fullName>
        <externalId>false</externalId>
        <label>Action Monitored Method</label>
        <length>2500</length>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Action_Monitored__c</fullName>
        <externalId>false</externalId>
        <label>Action Monitored</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>No</fullName>
                    <default>false</default>
                    <label>No</label>
                </value>
                <value>
                    <fullName>yes</fullName>
                    <default>false</default>
                    <label>yes</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Action_Monitored_by_Department__c</fullName>
        <externalId>false</externalId>
        <label>Action Monitored by Department</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Department</fullName>
                    <default>false</default>
                    <label>Department</label>
                </value>
                <value>
                    <fullName>List</fullName>
                    <default>false</default>
                    <label>List</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Case_Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>If CCA relates to a specific case product, can populate related product in this field</description>
        <externalId>false</externalId>
        <label>Case Product</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Case_Product__c.Case__c</field>
                <operation>equals</operation>
                <valueField>$Source.Case__c</valueField>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>Case_Product__c</referenceTo>
        <relationshipLabel>Case Corrective Actions</relationshipLabel>
        <relationshipName>Case_Corrective_Actions</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Case__c</fullName>
        <description>Field to create junction relationship between Case and Corrective Action</description>
        <externalId>false</externalId>
        <label>Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>Case Corrective Actions</relationshipLabel>
        <relationshipName>Case_Corrective_Actions</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Corrective_Action_Detail__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Corrective Action in Detail</inlineHelpText>
        <label>Corrective Action Detail</label>
        <length>5000</length>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Department_Responsible__c</fullName>
        <externalId>false</externalId>
        <label>Department Responsible</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>true</sorted>
                <value>
                    <fullName>Accounting Error</fullName>
                    <default>false</default>
                    <label>Accounting Error</label>
                </value>
                <value>
                    <fullName>Application Error</fullName>
                    <default>false</default>
                    <label>Application Error</label>
                </value>
                <value>
                    <fullName>Color Error</fullName>
                    <default>false</default>
                    <label>Color Error</label>
                </value>
                <value>
                    <fullName>CSR Error</fullName>
                    <default>false</default>
                    <label>CSR Error</label>
                </value>
                <value>
                    <fullName>Customer Error</fullName>
                    <default>false</default>
                    <label>Customer Error</label>
                </value>
                <value>
                    <fullName>Damaged Packaging</fullName>
                    <default>false</default>
                    <label>Damaged Packaging</label>
                </value>
                <value>
                    <fullName>Distributor Error</fullName>
                    <default>false</default>
                    <label>Distributor Error</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
                <value>
                    <fullName>Product Fault</fullName>
                    <default>false</default>
                    <label>Product Fault</label>
                </value>
                <value>
                    <fullName>Production Error</fullName>
                    <default>false</default>
                    <label>Production Error</label>
                </value>
                <value>
                    <fullName>Quality Control</fullName>
                    <default>false</default>
                    <label>Quality Control</label>
                </value>
                <value>
                    <fullName>Shipping Error</fullName>
                    <default>false</default>
                    <label>Shipping Error</label>
                </value>
                <value>
                    <fullName>Sika Advice</fullName>
                    <default>false</default>
                    <label>Sika Advice</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Initiated_by_Department__c</fullName>
        <externalId>false</externalId>
        <label>Initiated by Department</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Department</fullName>
                    <default>false</default>
                    <label>Department</label>
                </value>
                <value>
                    <fullName>List</fullName>
                    <default>false</default>
                    <label>List</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Investigator__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Investigator</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Case_Corrective_Actions</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Monitored_By__c</fullName>
        <externalId>false</externalId>
        <label>Monitored By</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Notes__c</fullName>
        <externalId>false</externalId>
        <label>Notes</label>
        <length>10000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Priority__c</fullName>
        <externalId>false</externalId>
        <label>Priority</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Low</fullName>
                    <default>false</default>
                    <label>Low</label>
                </value>
                <value>
                    <fullName>Medium</fullName>
                    <default>false</default>
                    <label>Medium</label>
                </value>
                <value>
                    <fullName>High</fullName>
                    <default>false</default>
                    <label>High</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Responsible_Party__c</fullName>
        <externalId>false</externalId>
        <label>Responsible Party</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Identified</fullName>
                    <default>false</default>
                    <label>Identified</label>
                </value>
                <value>
                    <fullName>Initiated</fullName>
                    <default>false</default>
                    <label>Initiated</label>
                </value>
                <value>
                    <fullName>Monitored</fullName>
                    <default>false</default>
                    <label>Monitored</label>
                </value>
                <value>
                    <fullName>Completed</fullName>
                    <default>false</default>
                    <label>Completed</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Case Corrective Action</label>
    <nameField>
        <label>Case Corrective Action Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Case Corrective Actions</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Require_Investigator_with_Dept_Resp</fullName>
        <active>false</active>
        <errorConditionFormula>OR(
AND(NOT(ISBLANK(TEXT(Department_Responsible__c ))), ISBLANK(  Investigator__c  )),
AND(ISBLANK(TEXT(Department_Responsible__c )), NOT(ISBLANK(  Investigator__c  )))
)</errorConditionFormula>
        <errorMessage>Please include both Investigator and Department Responsible</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
