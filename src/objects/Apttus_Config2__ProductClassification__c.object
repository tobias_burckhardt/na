<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Represents a product classification which is a member of the list of products associated with a classification.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Apttus_Config2__ClassificationId__c</fullName>
        <deprecated>false</deprecated>
        <description>ID of the associated category</description>
        <externalId>false</externalId>
        <label>Category</label>
        <referenceTo>Apttus_Config2__ClassificationHierarchy__c</referenceTo>
        <relationshipLabel>Products</relationshipLabel>
        <relationshipName>Products</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Apttus_Config2__DefaultQuantity__c</fullName>
        <defaultValue>1</defaultValue>
        <deprecated>false</deprecated>
        <description>The default quantity that should be presented to the user for selection. This is applicable only to options or components.</description>
        <externalId>false</externalId>
        <label>Default Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Apttus_Config2__Default__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Indicates whether the option is a default for the product. This is applicable only to options or components.</description>
        <externalId>false</externalId>
        <label>Default</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Apttus_Config2__MaxQuantity__c</fullName>
        <defaultValue>1</defaultValue>
        <deprecated>false</deprecated>
        <description>The maximum quantity of the option that may be selected. The quantity can be zero. This is applicable only to options or components.</description>
        <externalId>false</externalId>
        <label>Max Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Apttus_Config2__MinQuantity__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <description>The minimum quantity of the option that must be selected. The quantity can be zero. This is applicable only to options or components.</description>
        <externalId>false</externalId>
        <label>Min Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Apttus_Config2__Modifiable__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Indicates whether the option or component is modifiable during selection. This is applicable only to options or components.</description>
        <externalId>false</externalId>
        <label>Modifiable</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Apttus_Config2__ProductId__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>ID of the associated product</description>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Categories</relationshipLabel>
        <relationshipName>Categories</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Apttus_Config2__Sequence__c</fullName>
        <deprecated>false</deprecated>
        <description>The sequence which the product should appear in the category.</description>
        <externalId>false</externalId>
        <inlineHelpText>The sequence which the product should appear in the category.</inlineHelpText>
        <label>Sequence</label>
        <precision>5</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Category_Text__c</fullName>
        <externalId>false</externalId>
        <formula>Apttus_Config2__ClassificationId__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Category Text</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Include_in_QuotetoNOA_layer__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Custom field added to capture if the Product should be seen on the NOA layer</description>
        <externalId>false</externalId>
        <inlineHelpText>Check if Product/Part should be seen on the NOA layer</inlineHelpText>
        <label>Include in QuotetoNOA layer</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Product_Active__c</fullName>
        <externalId>false</externalId>
        <formula>Apttus_Config2__ProductId__r.IsActive</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product - Active?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Product_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Apttus_Config2__ProductId__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product_Proposal_Active__c</fullName>
        <externalId>false</externalId>
        <formula>Apttus_Config2__ProductId__r.ProposalActive__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product - Proposal Active?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Product_Roofing_Description__c</fullName>
        <externalId>false</externalId>
        <formula>Apttus_Config2__ProductId__r.APTS_Roofing_Description__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product Roofing Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SAP_Part_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Apttus_Config2__ProductId__r.SAP_Part_Number__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>SAP Part Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Product Classification</label>
    <nameField>
        <displayFormat>CP-{0000000000}</displayFormat>
        <label>Classification Id</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Product Classifications</pluralLabel>
    <searchLayouts>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <excludedStandardButtons>New</excludedStandardButtons>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
