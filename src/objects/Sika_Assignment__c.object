<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>The asset assignment link</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Account_Address__c</fullName>
        <externalId>false</externalId>
        <formula>Asset_Installation__r.Account_City__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account Address</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account_Name__c</fullName>
        <description>Name of the Account where this is installed.</description>
        <externalId>false</externalId>
        <formula>Asset_Installation__r.Account__r.Name</formula>
        <inlineHelpText>Name of the Account where this is installed.</inlineHelpText>
        <label>Account Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Active__c</fullName>
        <defaultValue>true</defaultValue>
        <description>Indicates whether or not the asset is active.</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates whether or not the asset is active.</inlineHelpText>
        <label>Active</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AssetProduct__c</fullName>
        <externalId>false</externalId>
        <formula>Sika_Asset__r.Product__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Asset_Installation__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Asset Installation lookup</description>
        <externalId>false</externalId>
        <inlineHelpText>Asset Installation lookup</inlineHelpText>
        <label>Asset Installation</label>
        <referenceTo>Sika_Asset_Installation__c</referenceTo>
        <relationshipName>Asset_Assignments</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Asset_Manufacturer__c</fullName>
        <externalId>false</externalId>
        <formula>Text(Sika_Asset__r.Manufacturer__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Asset Manufacturer</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Asset_Purchase_Date__c</fullName>
        <externalId>false</externalId>
        <formula>Sika_Asset__r.Purchase_Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Asset Purchase Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Asset_Size__c</fullName>
        <externalId>false</externalId>
        <formula>Text(Sika_Asset__r.Asset_Size__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Asset Size</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Asset_Type__c</fullName>
        <description>Asset Type from Asset</description>
        <externalId>false</externalId>
        <formula>Text(Sika_Asset__r.Asset_Type__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Type of Equipment</inlineHelpText>
        <label>Asset Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Decommission_Dtae__c</fullName>
        <description>Date of decommission</description>
        <externalId>false</externalId>
        <inlineHelpText>Date of decommission</inlineHelpText>
        <label>Decommission Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <description>Asset Description</description>
        <externalId>false</externalId>
        <formula>Sika_Asset__r.Asset_Description__c</formula>
        <inlineHelpText>Asset Description</inlineHelpText>
        <label>Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Employee__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Link To Employee Record - For IT Assets only</description>
        <externalId>false</externalId>
        <inlineHelpText>Link To Employee Record - For IT Assets only</inlineHelpText>
        <label>Employee</label>
        <referenceTo>Employee__c</referenceTo>
        <relationshipLabel>Sika Asset Assignments</relationshipLabel>
        <relationshipName>Sika_Asset_Assignments</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Installation_Date__c</fullName>
        <description>Installation Date</description>
        <externalId>false</externalId>
        <inlineHelpText>Please enter the date of installation</inlineHelpText>
        <label>Installation Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Product Dispensed by System (Remove, LInk Product on Asset Object Instead)</description>
        <externalId>false</externalId>
        <inlineHelpText>Product Dispensed by System (Remove, LInk Product on Asset Object Instead)</inlineHelpText>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Sika Asset Assignments</relationshipLabel>
        <relationshipName>Sika_Asset_Assignments</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Serial_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Sika_Asset__r.ID_Serial__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Serial Number</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sika_Asset__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Sika Asset</description>
        <externalId>false</externalId>
        <inlineHelpText>Sika Asset</inlineHelpText>
        <label>Sika Asset</label>
        <referenceTo>Sika_Asset__c</referenceTo>
        <relationshipName>Asset_Assignments</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <formula>Text(Sika_Asset__r.Status__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Sika Asset Assignment</label>
    <nameField>
        <displayFormat>SAA-{0}</displayFormat>
        <label>Assignment Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Sika Asset Assignments</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <startsWith>Vowel</startsWith>
    <visibility>Public</visibility>
</CustomObject>
