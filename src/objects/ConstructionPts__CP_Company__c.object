<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>ConstructionPts__Account_Name__c</fullName>
        <deprecated>false</deprecated>
        <description>Linked to Account Name</description>
        <externalId>false</externalId>
        <formula>IF(!ISNULL(ConstructionPts__Account__c) &amp;&amp; (ConstructionPts__Account__c != &apos;&apos;), ConstructionPts__Account__r.Name, &apos;&apos;)</formula>
        <label>Account Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Account_Owner__c</fullName>
        <deprecated>false</deprecated>
        <description>Linked to Account Owner</description>
        <externalId>false</externalId>
        <formula>IF(!ISNULL(ConstructionPts__Account__c) &amp;&amp; (ConstructionPts__Account__c != &apos;&apos;), ConstructionPts__Account__r.Owner.FirstName + &apos; &apos; + ConstructionPts__Account__r.Owner.LastName  , &apos;&apos;)</formula>
        <label>Account Owner</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Linked to Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Dodge Companies</relationshipLabel>
        <relationshipName>CP_Companies</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ConstructionPts__Address__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Address</label>
        <length>161</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__BatchID__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>BatchID</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__CKMS_ID__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Dodge CKMS ID</label>
        <length>15</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__City__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>City</label>
        <length>50</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__CompanyID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <deprecated>false</deprecated>
        <externalId>true</externalId>
        <label>Dodge Factor Key</label>
        <length>20</length>
        <required>true</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Company_Report_URL__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Company Report URL</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>ConstructionPts__Company_key__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Dodge Company ID</label>
        <length>13</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Country__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Country</label>
        <length>30</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__County__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>County</label>
        <length>30</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__DUNS__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>DUNS</label>
        <length>9</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Dodge_Company_PipeLine_URL__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>HYPERLINK(&apos;/apex/ConstructionPts__Dodge_PipeLine?CompanyReport=&apos; + ConstructionPts__Company_key__c + &apos;?idp=U2FsZXNGb3JjZQ2&apos; ,&apos;View Company&apos;,&apos;_tab&apos; )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Dodge Company (PipeLine)</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Email__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Email</label>
        <length>50</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Fax_Number__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Fax Number</label>
        <length>15</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Has_Account__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(ConstructionPts__Account__c != null, &apos;YES&apos;, &apos;NO&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Has Account</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Has_Lead__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(ConstructionPts__Lead__c != null, &apos;YES&apos;, &apos;NO&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Has Lead</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Latitude__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Latitude</label>
        <precision>18</precision>
        <required>false</required>
        <scale>6</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Lead__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Linked to Lead</label>
        <referenceTo>Lead</referenceTo>
        <relationshipLabel>Dodge Companies</relationshipLabel>
        <relationshipName>CP_Companies</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ConstructionPts__Linked_By_User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Linked By</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Dodge_Companies</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ConstructionPts__Longitude__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Longitude</label>
        <precision>18</precision>
        <required>false</required>
        <scale>6</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Matched_By_Date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Linked Date</label>
        <length>50</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Matched_By_UserId__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Linked By UserId</label>
        <length>18</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Parent_DUNS__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Parent DUNS</label>
        <length>9</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Phone_Extension__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Phone Extension</label>
        <length>10</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Phone_Number__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Phone Number</label>
        <length>15</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Pipeline_Import_TimeStamp__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Pipeline Import TimeStamp</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>ConstructionPts__Pipeline_Objcount_TimeStamp__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Pipeline Objcount TimeStamp</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>ConstructionPts__Related_Contacts__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Contacts</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Related_Projects__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Projects</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__ReportsCompanyID__c</fullName>
        <deprecated>false</deprecated>
        <externalId>true</externalId>
        <label>Reports Company ID</label>
        <length>20</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__SIC_code12__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>SIC Code</label>
        <length>12</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__SIC_code__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>SIC Code</label>
        <length>6</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__SIC_code_description__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>SIC Code Description</label>
        <length>100</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__State__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>State</label>
        <length>2</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Website__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Website</label>
        <length>50</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConstructionPts__Zipcode__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Zipcode</label>
        <length>10</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Dodge Company</label>
    <listViews>
        <fullName>ConstructionPts__All</fullName>
        <columns>NAME</columns>
        <columns>ConstructionPts__DUNS__c</columns>
        <columns>ConstructionPts__Account__c</columns>
        <columns>ConstructionPts__Lead__c</columns>
        <columns>LAST_UPDATE</columns>
        <columns>ConstructionPts__Address__c</columns>
        <columns>ConstructionPts__City__c</columns>
        <columns>ConstructionPts__County__c</columns>
        <columns>ConstructionPts__State__c</columns>
        <columns>ConstructionPts__Country__c</columns>
        <columns>ConstructionPts__Zipcode__c</columns>
        <columns>ConstructionPts__Phone_Number__c</columns>
        <columns>ConstructionPts__Phone_Extension__c</columns>
        <columns>ConstructionPts__Fax_Number__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Dodge Company (SF)</label>
        <trackFeedHistory>false</trackFeedHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Dodge Companies</pluralLabel>
    <searchLayouts>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>New</excludedStandardButtons>
        <searchResultsAdditionalFields>ConstructionPts__City__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ConstructionPts__State__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ConstructionPts__Account_Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ConstructionPts__Account_Owner__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>ConstructionPts__Find_possible_matches</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Find possible matches</masterLabel>
        <openType>sidebar</openType>
        <page>ConstructionPts__CPCompanyMatch</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>ConstructionPts__View_Map</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>View Map</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!REQUIRESCRIPT(&quot;https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js&quot;)}
{!REQUIRESCRIPT(&quot;/soap/ajax/32.0/connection.js&quot;)}
var j$ = jQuery.noConflict();
function getGoogleAPIKey() {
        sforce.connection.defaultNamespace = &apos;ConstructionPts__&apos;;
	var result = sforce.connection.queryAll(&quot;SELECT Access_Level__c,Config_Long_Value__c,Config_Name__c,Config_Value__c FROM CPConfig__c WHERE Config_Name__c = &apos;GoogleMapAPIKey&apos;&quot;);
        var records;
        try {
	   records = result.getArray(&quot;records&quot;);
        } catch (error) {
           console.log( &apos;There has been an error getting the GoogleMap API Key: &apos; + error);
        }
	if (records == null || records.length == 0 || records.length &gt; 1) {
            return &quot;&quot;;
        }
	
       var record = records[0];
       return record.Config_Value__c;	
}

var head = document.getElementsByTagName(&apos;head&apos;)[0];

var key = getGoogleAPIKey();

function loadMap() {
  var map;
  var marker;
 
  try {
   var geocoder = new google.maps.Geocoder();
   
  var address = &quot;{!JSENCODE(ConstructionPts__CP_Company__c.ConstructionPts__Address__c)}, &quot; + &quot;{!JSENCODE(ConstructionPts__CP_Company__c.ConstructionPts__City__c)}, &quot; + &quot;{!JSENCODE(ConstructionPts__CP_Company__c.ConstructionPts__Zipcode__c)}, &quot; + &quot;{!JSENCODE(ConstructionPts__CP_Company__c.ConstructionPts__Country__c)}&quot;;
 
  var infowindow = new google.maps.InfoWindow({
    content: &quot;&lt;b&gt;{!JSENCODE(ConstructionPts__CP_Company__c.Name)}&lt;/b&gt;&lt;br&gt;{!JSENCODE(ConstructionPts__CP_Company__c.ConstructionPts__Address__c)}&lt;br&gt;{!JSENCODE(ConstructionPts__CP_Company__c.ConstructionPts__City__c)}, {!JSENCODE(ConstructionPts__CP_Company__c.ConstructionPts__Zipcode__c)}&lt;br&gt;{!JSENCODE(ConstructionPts__CP_Company__c.ConstructionPts__Country__c)}&quot;
  });

geocoder.geocode( { address: address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK &amp;&amp; results.length) {
      if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {

        var myOptions = {
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false
        }
     
        map = new google.maps.Map(document.getElementById(&quot;map-canvas2&quot;), myOptions);
    
        map.setCenter(results[0].geometry.location);

        marker = new google.maps.Marker({
            position: results[0].geometry.location,
            map: map,
            title: &quot;{!JSENCODE(ConstructionPts__CP_Company__c.Name)}&quot;
        });
      
        google.maps.event.addListener(marker, &apos;click&apos;, function() {
          infowindow.open(map,marker);
        });
        google.maps.event.addListener(infowindow, &apos;closeclick&apos;, function() {
          map.setCenter(marker.getPosition()); 
        });

      }

    } else {
      j$(&apos;#map-canvas2&apos;).css({&apos;height&apos; : &apos;15px&apos;});
      j$(&apos;#map-canvas2&apos;).html(&quot;Oops! &quot; + &quot; Address for {!JSENCODE(ConstructionPts__CP_Company__c.Name)}&quot; + &quot; could not be found, please make sure the address is correct.&quot;);
    
 
    }
 });

 } catch (error) {
  
  
 } 
}

console.log(&apos; here1&apos;);
var gMapLink = &quot;https://maps.google.com/maps/api/js?key=&quot; + key + &quot;&amp;callback=initMap&quot; ;
j$(head).append(&apos;&lt;script type=&quot;text/javascript&quot;&gt;&apos; + loadMap.toString() + &apos;  function initMap() {  loadMap();} &lt;/script&gt;&apos;);
if (j$(&apos;head script[src=&quot;&apos; + gMapLink  + &apos;&quot;]&apos;).length &lt; 1) {

   j$(head).append(&apos;&lt;script async type=&quot;text/javascript&quot;  src=&quot;https://maps.google.com/maps/api/js?key=&apos; + key + &apos;&amp;callback=initMap&quot;&gt;&lt;/script&gt;&apos;);
   console.log(&apos; here2&apos;);
}

console.log(&apos; here3&apos;);

if (j$(&quot;#map-canvas2&quot;).length &gt; 0) {
   //j$(&quot;#cpMapContainer&quot;).remove();
   //console.log(&apos; here4&apos;);
}
console.log(&apos; here5&apos;);

if (j$(&quot;#map-canvas2&quot;).length &lt; 1) {
   j$(&quot;.mainTitle&quot;).parents(&quot;.pbHeader&quot;).siblings(&quot;.pbBody&quot;).find(&quot;div:contains(&apos;Information&apos;)&quot;).next(&apos;div.pbSubsection&apos;).after(&apos;&lt;a name=&quot;cpMapAnchor&quot; id=&quot;cpMapAnchor&quot;&gt;&lt;/a&gt;&lt;div tabindex=&quot;-1&quot; id=&quot;cpMapContainer&quot; style=&quot;width: 100%;&quot; class=&quot;brandTertiaryBrd pbSubheader tertiaryPalette&quot;&gt;&lt;div id=&quot;cpMapHeader&quot; class=&quot;pBlock&quot; style=&quot;margin-bottom: 5px;&quot;&gt; &lt;img src=&quot;/img/s.gif&quot; alt=&quot;Hide CP Map&quot; class=&quot;hideListButton&quot; onclick=&quot;twistSection(this);&quot; onkeypress=&quot;if (event.keyCode==\&apos;13\&apos;)twistSection(this);&quot; style=&quot;cursor:pointer;&quot;&gt;&lt;h3&gt;Company Location Map&lt;h3&gt;&lt;/div&gt;&lt;div id=&quot;map-canvas2&quot; style=&quot;width: 100%; height: 600px; resize: both;overflow: auto; &quot;&gt;&lt;/div&gt;&lt;/br&gt;&lt;/div&gt;&apos;).show();
 console.log(&apos; here6&apos;);
}

j$(&quot;#cpMapHeader&quot;).find(&quot;img.showListButton&quot;).click();
document.getElementById(&quot;cpMapContainer&quot;).focus();
document.getElementById(&quot;cpMapContainer&quot;).blur();</url>
    </webLinks>
</CustomObject>
