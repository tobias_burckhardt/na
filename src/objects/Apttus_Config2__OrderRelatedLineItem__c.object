<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Holds the relationships between order line items</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Apttus_Config2__LineItemId__c</fullName>
        <deprecated>false</deprecated>
        <description>ID of the associated order line item</description>
        <externalId>false</externalId>
        <inlineHelpText>ID of the associated order line item</inlineHelpText>
        <label>Line Item</label>
        <referenceTo>Apttus_Config2__OrderLineItem__c</referenceTo>
        <relationshipLabel>Related Line Items (To)</relationshipLabel>
        <relationshipName>RelatedLineItemsTo</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Apttus_Config2__RelatedAssetLineItemId__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>ID of the related asset line item</description>
        <externalId>false</externalId>
        <inlineHelpText>ID of the related asset line item</inlineHelpText>
        <label>Related Asset Line Item</label>
        <referenceTo>Apttus_Config2__AssetLineItem__c</referenceTo>
        <relationshipLabel>Related Order Line Items</relationshipLabel>
        <relationshipName>RelatedOrderLineItems</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Apttus_Config2__RelatedLineItemId__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>ID of the related order line item</description>
        <externalId>false</externalId>
        <inlineHelpText>ID of the related order line item</inlineHelpText>
        <label>Related Line Item</label>
        <referenceTo>Apttus_Config2__OrderLineItem__c</referenceTo>
        <relationshipLabel>Related Line Items (From)</relationshipLabel>
        <relationshipName>RelatedLineItemsFrom</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Apttus_Config2__RelationType__c</fullName>
        <deprecated>false</deprecated>
        <description>The relation type</description>
        <externalId>false</externalId>
        <inlineHelpText>The relation type</inlineHelpText>
        <label>Relation Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Service</fullName>
                    <default>true</default>
                    <label>Service</label>
                </value>
                <value>
                    <fullName>Wallet</fullName>
                    <default>false</default>
                    <label>Wallet</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Apttus_Config2__UseType__c</fullName>
        <deprecated>false</deprecated>
        <description>The application type</description>
        <externalId>false</externalId>
        <inlineHelpText>The application type</inlineHelpText>
        <label>Application Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Asset</fullName>
                    <default>false</default>
                    <label>Asset</label>
                </value>
                <value>
                    <fullName>Cart</fullName>
                    <default>false</default>
                    <label>Cart</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Apttus_Config2__WeightageAmount__c</fullName>
        <deprecated>false</deprecated>
        <description>The weightage amount</description>
        <externalId>false</externalId>
        <inlineHelpText>The weightage amount</inlineHelpText>
        <label>Weightage Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Apttus_Config2__WeightagePercent__c</fullName>
        <deprecated>false</deprecated>
        <description>The effective weightage percent</description>
        <externalId>false</externalId>
        <inlineHelpText>The effective weightage percent</inlineHelpText>
        <label>Weightage Percent</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Apttus_Config2__WeightageType__c</fullName>
        <deprecated>false</deprecated>
        <description>The weightage type</description>
        <externalId>false</externalId>
        <inlineHelpText>The weightage type</inlineHelpText>
        <label>Weightage Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Percentage</fullName>
                    <default>false</default>
                    <label>Percentage</label>
                </value>
                <value>
                    <fullName>Amount</fullName>
                    <default>false</default>
                    <label>Amount</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Apttus_Config2__WeightedNetPrice__c</fullName>
        <deprecated>false</deprecated>
        <description>The weighted net price</description>
        <externalId>false</externalId>
        <inlineHelpText>The weighted net price</inlineHelpText>
        <label>Weighted Net Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <label>Order Related Line Item</label>
    <nameField>
        <displayFormat>RLI-{0000000000}</displayFormat>
        <label>Related Item Id</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Order Related Line Items</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
