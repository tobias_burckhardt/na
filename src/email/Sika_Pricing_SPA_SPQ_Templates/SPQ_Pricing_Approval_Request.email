<messaging:emailTemplate subject="{!$Label.Quote_Submission1} - {!relatedTo.Name}({!relatedTo.Account__r.name})" recipientType="User" language="{!recipient.LanguageLocaleKey}" relatedToType="Quote">
<messaging:htmlEmailBody >

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"> </meta><!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width"> </meta><!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"></meta> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  </meta><!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

    <!-- Web Font / @font-face : BEGIN -->
    <!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->
    
    <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
    <!--[if mso]>
        <style>
            * {
                font-family: sans-serif !important;
            }
        </style>
    <![endif]-->
    
    <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
    <!--[if !mso]><!-->
        <!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
    <!--<![endif]-->

    <!-- Web Font / @font-face : END -->
    
    <!-- CSS Reset -->
    <style>

        /* What it does: Remove spaces around the email design added by some email clients. */
        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }
        
        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        
        /* What it does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }
        
        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }
                
        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto; 
        }
        
        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }
        
        /* What it does: A work-around for iOS meddling in triggered links. */
        .mobile-link--footer a,
        a[x-apple-data-detectors] {
            color:inherit !important;
            text-decoration: underline !important;
        }
        
        /* What it does: Prevents underlining the button text in Windows 10 */
        .button-link {
            text-decoration: none !important;
        } 
    </style>
</head>
<body width="100%" style="margin: 0;">
    
        
        <div style="max-width: 680px; margin: auto;">
            <!--[if mso]>
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="680" align="center">
            <tr>
            <td>
            <![endif]-->
            <div style="margin: auto;font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; margin: 0; padding: 0;border-collapse:collapse;color:#666666;">
                
                <!-- Email Body : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;">
                    <tr>
                        <td style="padding:10px;">  
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="left" width="100%">
                                 <tr>
                                    <td width="100%" style="vertical-align: top; text-align: left; padding: 0px 0px 10px;" align="left" valign="top"><p align="left" style="color: #666666; font-family: 'Helvetica', 'Arial', sans-serif; text-align: left; line-height: 1.3; word-break: normal; font-size:14px; margin: 0; padding:10px 0px;">{!$Label.HI} {!recipient.name}, </p>
                                        <p align="left" style="color: #666666; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height:19px; font-size: 14px; margin: 0; padding:10px 0px;"> {!$Label.has_requested_your_approval_for_the_following_item1}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="100%" valign="top" width="100%" style="padding: 0px 0;text-align: left;">
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" valign="middle">
                                          <tbody>
                                          <tr>
                                                <td align="left" style="font-size:14px; font-family: Helvetica, Arial, sans-serif; color: #666666;">{!$ObjectType.Quote.Fields.Account__c.Label}:</td>
                                                <td align="left" style="font-size:12px; font-family: Helvetica, Arial, sans-serif; color: #666666;" >{!relatedTo.Account__r.name}</td>
                                              </tr>
                                              <tr>
                                                <td align="left" style="font-size:14px; font-family: Helvetica, Arial, sans-serif; color: #666666;">{!$ObjectType.Quote.Fields.Name.Label}:</td>
                                                <td align="left" style="font-size:12px; font-family: Helvetica, Arial, sans-serif; color: #666666;" >{!relatedTo.Name}</td>
                                              </tr>
                                              <tr>
                                                <td align="left" style="font-size:14px; font-family: Helvetica, Arial, sans-serif; color: #666666;">{!$ObjectType.Quote.Fields.GrandTotal.Label}:</td>
                                                <td align="left" style="font-size:12px; font-family: Helvetica, Arial, sans-serif; color: #666666;" >{!relatedTo.GrandTotal}</td>
                                              </tr>
                                              <tr>
                                                <td align="left" style="font-size:14px; font-family: Helvetica, Arial, sans-serif; color: #666666;">{!$Label.Number_of_products_line_items}:</td>
                                                <td align="left" style="font-size:12px; font-family: Helvetica, Arial, sans-serif; color: #666666;" >{!relatedTo.LineItemCount}</td>
                                              </tr>
                                              
                                                <tr>
                                                <td colspan="2" align="left" style="font-size:14px;font-family: Helvetica, Arial, sans-serif; color: #666666;">{!$Label.View_Request_in_Salesforce}</td>
                                               
                                              </tr>
                                                 <tr>
                                                
                                                <td align="left" style="font-size:12px; font-family: Helvetica, Arial, sans-serif; color: #666666;" >
                                                    <apex:outputText value="https://sikana.my.salesforce.com/{!relatedTo.Id}"></apex:outputText>
                                                </td>
                                              </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="100%" valign="top" width="100%" style="padding: 10px 0;text-align: left;">
                                        <table width="100%" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left;">
                                            <tbody>
                                                <tr style="vertical-align: top; text-align: left; " align="left">
                                                    <td width="100%" style="ord-break:break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #666666; " align="left" valign="top"><br></br>
                                                        <div style="padding: 10px 0px 0px 0px;">{!$Label.Thank_you}</div>                                                 
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                </table>
                </td>
                </tr>
                </table>
            </div>
            <!--[if mso]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </div>
    
</body>
</html>

</messaging:htmlEmailBody>
</messaging:emailTemplate>