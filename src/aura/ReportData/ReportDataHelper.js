({
    getData: function(component, accountId, selectedMonths, reportType, salesRepId, userType, Vkbur, Spart, SBU, Vtweg, selectedyear, companyCode)
    {
        this.getAccessibleColumns(component, reportType);
        var dontGroup;
        var action;
        console.log("reportType",reportType);
        switch (reportType)
        {
            case 'salesFigures':
                action = component.get("c.getSalesFiguresData");
                document.title = 'Sales Figures';
                break;
            case 'salesFiguresBySalesReps':
                action = component.get("c.getSalesFiguresBySalesRepsData");
                document.title = 'Sales Figures By Sales Reps';
                break;
            case 'shipToSalesFigures':
                action = component.get("c.getShipToSalesFiguresData");
                document.title = 'Ship To Sales Figures';
                break;
            case 'shipToSalesFiguresBySalesReps':
                action = component.get("c.getShipToSalesFiguresBySalesRepsData");
                document.title = 'Ship To Sales Figures By Sales Reps';
                break;
            case 'articleMix':
                action = component.get("c.getArticleMixData");
                document.title = 'Article Mix';
                break;
            case 'articleMixBySalesReps':
                action = component.get("c.getArticleMixBySalesRepsData");
                document.title = 'Article Mix By Sales Reps';
                break;
            case 'productMix':
                action = component.get("c.getProductMixData");
                document.title = 'Product Mix';
                break;
            case 'productMixBySalesReps':
                action = component.get("c.getProductMixBySalesRepsData");
                document.title = 'Product Mix By Sales Reps';
                break;
            case 'accountsTop100':
                action = component.get("c.getAccountsTop100Data");
                document.title = 'Accounts Top 100';
                break;
            case 'CompanyTurnOverOfficeSet':
                action = component.get('c.getCompanyTurnOversData');
                document.title = 'Turnover Company SA SO Month';
                break;

            case 'CompanyTurnoverAreaSet':
                action = component.get('c.getCompanyTurnoversAreaSetData');
                document.title = 'Turnover Company SA Month';
                break;

            case 'CompanyTurnOverSBUSet':
                action = component.get('c.getCompanyTurnOverSBUSet');
                document.title = 'Turnover per Company, Sales Area, SBU';
                break;

            case 'TurnoverCompanySASBUAC':
                action = component.get('c.getTurnoverCompanySASBUAC');
                document.title = 'Turnover Company SA SBU AC';
                break;

            case 'TurnoverCompanySASOAC':
                action = component.get('c.getTurnoverCompanySASOAC');
                document.title = 'Turnover Company SA SO AC';
                break;

        }
        if ( salesRepId !== undefined )
        {
            // document.title = 'My ' + document.title;
        }

        var sortedMonths = selectedMonths !== undefined ? selectedMonths : [12];
        sortedMonths.sort(this.sortNumbers);
        accountId = accountId !== undefined ? accountId : '';

        var params = { accountId: accountId };
        // DSE: always set the selected year parameter
        params.selectedYear = selectedyear;
        params.selectedMonths = sortedMonths.join();
        params.salesRepId = salesRepId;
        params.userType = userType;
        params.companyCode = companyCode;

        if ( reportType === 'articleMix' ||  reportType === 'articleMixBySalesReps' || reportType === 'productMix' || reportType === 'productMixBySalesReps')
        {
            params.erpNo = component.get('v.erpNo');
            params.accountType = component.get('v.accountType');
        }

        if(reportType === 'CompanyTurnOverOfficeSet' || reportType ==='TurnoverCompanySASOAC')
        {
            params.Vkbur = Vkbur;
        }

        if(reportType === 'CompanyTurnoverAreaSet' || reportType === 'CompanyTurnOverOfficeSet' || reportType === 'CompanyTurnOverSBUSet' || reportType === 'TurnoverCompanySASBUAC' || reportType === 'TurnoverCompanySASOAC')
        {
            params.Spart = Spart;
        }

        if(reportType === 'CompanyTurnoverAreaSet' || reportType === 'CompanyTurnOverOfficeSet' || reportType === 'CompanyTurnOverSBUSet' || reportType === 'TurnoverCompanySASBUAC' || reportType === 'TurnoverCompanySASOAC')
        {
            params.Vtweg = Vtweg;
        }

        if(reportType === 'CompanyTurnOverSBUSet' || reportType === 'TurnoverCompanySASBUAC')
        {
            params.SBU = SBU;
        }

        /*
        if(reportType=='getShipToSalesFiguresData' || reportType=='getShipToSalesFiguresBySalesRepsData' || reportType=='productMixBySalesReps' || reportType=='articleMixBySalesReps' || reportType=='salesFiguresBySalesReps' || reportType=='productMix' || reportType=='articleMix' || reportType == 'salesFigures' || reportType == 'CompanyTurnoverAreaSet' || reportType =='CompanyTurnOverOfficeSet' || reportType =='CompanyTurnOverSBUSet' || reportType=='TurnoverCompanySASBUAC' || reportType=='TurnoverCompanySASOAC'){
            params.selectedYear = selectedyear;
        }
        */
        action.setParams(params);
        action.setCallback(this, function(response)
        {
            var state = response.getState();

            // console.log(response);

            if ( component.isValid() && state === 'SUCCESS')
            {
                var data = response.getReturnValue();
                console.log("returned data",data);
                var groupedData;
                switch ( reportType )
                {
                    case 'salesFiguresBySalesReps':
                    case 'articleMixBySalesReps':
                    case 'productMixBySalesReps':
                    case 'shipToSalesFiguresBySalesReps':
                    case 'accountsTop100':
                    {
                        groupedData = this.getGroupedData(data, true, ['SalesRep__c']);
                        break;
                    }
                    case 'CompanyTurnoverAreaSet':
                    {
                        groupedData = this.getGroupedData(data, true, ['Vkorg__c', 'Vtweg__c', 'Spart__c']);
                        break;
                    }
                    case 'TurnoverCompanySASBUAC':
                    {
                        groupedData = this.getGroupedData(data, true, ['Vkorg__c', 'Vtweg__c', 'Spart__c', 'Sbu__c']);
                        break;
                    }
                    case 'CompanyTurnOverSBUSet':
                    {
                        groupedData = this.getGroupedData(data, true, ['Vkorg__c', 'Vtweg__c', 'Spart__c','SBU__c']);
                        break;
                    }
                    case 'TurnoverCompanySASOAC':
                    case 'CompanyTurnOverOfficeSet':
                    {
                        groupedData = this.getGroupedData(data, true, ['Vkorg__c', 'Vtweg__c', 'Spart__c', 'Vkbur__c']);
                        break;
                    }
                    default:
                        groupedData = this.getGroupedData(data, false);
                }
                var moreThanOne = groupedData.length > 1;
                component.set('v.moreThanOneSalesRep',moreThanOne);
                component.set('v.data', groupedData);
                this.filterReport(component, selectedMonths, reportType);
            }
            else
            {
                console.error('Error occured: ', response.error);
                // DSE create an arror message on the client
                var errors = response.getError();
                var errorMessage;
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        errorMessage = errors[0].message;
                    }
                }

                $A.createComponents([
                        ["ui:message",{
                            "title" : "ERROR occured",
                            "severity" : "error",
                        }],
                        ["ui:outputText",{
                            "value" : errorMessage
                        }]
                        ],
                        function(components, status, errorMessage){
                            if (status === "SUCCESS") {
                                var message = components[0];
                                var outputText = components[1];
                                // set the body of the ui:message to be the ui:outputText
                                message.set("v.body", outputText);
                                var div1 = component.find("div1");
                                // Replace div body with the dynamic component
                                div1.set("v.body", message);
                            }
                            else if (status === "INCOMPLETE") {
                                console.log("No response from server or client is offline.")
                                // Show offline error
                            }
                            else if (status === "ERROR") {
                                console.log("Error: " + errorMessage);
                                // Show error message
                            }
                        }
                    );
            }
        });
        $A.enqueueAction(action);
        var reportsWithGrouping = [
            'salesFiguresBySalesReps', 'shipToSalesFiguresBySalesReps', 'articleMixBySalesReps', 'productMixBySalesReps',
            'accountsTop100', 'CompanyTurnoverAreaSet', 'CompanyTurnOverSBUSet', 'CompanyTurnOverOfficeSet', 'TurnoverCompanySASBUAC', 'TurnoverCompanySASOAC'];
        return ( reportsWithGrouping.indexOf(reportType) > -1);
    },

    getGroupedData: function(data, enableGrouping, groupingFields)
    {
        var groupedData = [];
        if ( enableGrouping )
        {
            var dataByGroups = {};
            if ( !$A.util.isUndefinedOrNull(data) )
            {
                data.forEach(function(item)
                {
                    if ( (item.Cust__c && item.Cust__c === 'SUMME') || (item.Spart_Txt__c && item.Spart_Txt__c === 'Result') || (item.Month__c === '00') ){
                    }
                    else{
                        var key = '';
                        groupingFields.forEach(function(field){
                            key += item['Salesrep__c'];
                        });
                        var groupData = dataByGroups[key];
                        if ( groupData === undefined )
                        {
                            groupData = [];
                        }
                        groupData.push(item);
                        dataByGroups[key] = groupData;
                    }
                });
                for ( var key in dataByGroups )
                {   
                    if ( dataByGroups.hasOwnProperty(key) )
                    {
                        var groupData = dataByGroups[key];
                        groupedData.push(groupData);
                    }
                }
                if ( data.length === 0 )
                {
                    groupedData.push(data);
                }
            }
        }
        else
        {
            if ( !$A.util.isUndefinedOrNull(data) )
            {
                groupedData.push(data);
            }
            //TEMP
            // else {
            //     groupedData.push([
            //                     {Matnr__c: '123', MatnrDesc__c: 'test', ProductDesc__c: 'test', BaseUom__c: 'EUR', Calmonth__c:'01.2016'},
            //                     {Matnr__c: '123', MatnrDesc__c: 'test', ProductDesc__c: 'test', BaseUom__c: 'EUR', Calmonth__c:'02.2016'},
            //                     {Matnr__c: '123', MatnrDesc__c: 'test', ProductDesc__c: 'test', BaseUom__c: 'EUR', Calmonth__c:'03.2016'}]);
            // }
        }
        return groupedData;
    },

    filterReport: function(component, selectedMonths, reportType)
    {
        var data = component.get('v.data');
        console.log('data' , data);
        var addEmptyRows = reportType === 'salesFigures' || reportType === 'salesFiguresBySalesReps' || reportType === 'shipToSalesFigures' || reportType === 'shipToSalesFiguresBySalesReps';
        var filteredData = this.filterMonths(data, selectedMonths, addEmptyRows,reportType);
        console.log("filteredData",filteredData);
        component.set('v.filteredData', filteredData);
    },

    filterMonths: function(groupedData, selectedMonths, addEmptyRows,reportType)
    {
        var filteredGroupedData = [];
        if ( groupedData !== undefined ) {
            groupedData.forEach(function(item)
            {
                var filteredData = [];
                var monthsWithData = [];
                item.forEach(function(row)
                {
                    var calMonth = row.Calmonth__c;
                    var rowMonth = parseInt(calMonth !== undefined ? calMonth.substring(0, 2) : '0', 10);
                    if ( rowMonth === 0 )
                    {
                        rowMonth = parseInt(row.Month__c !== undefined ? row.Month__c : '0', 10);
                    }
                    if ( selectedMonths.indexOf(rowMonth) > -1  || rowMonth === 0 ) {
                        row.Month__c = parseInt(row.Month__c, 10);
                        filteredData.push(row);
                        monthsWithData.push(rowMonth);
                    }
                });
               // alert(item.length+' item length'+ 'selected month'+selectedMonths.length);
                if ( item.length > 0 && item.length < selectedMonths.length + 1 && addEmptyRows )
                {
                    var salesRepNum = item[0].Salesrep__c !== undefined ? item[0].Salesrep__c : '';
                    var salesRepName = item[0].Salesrepname__c !== undefined ? item[0].Salesrepname__c : '';
                    selectedMonths.forEach(function(month) {
                        if ( monthsWithData.indexOf(month) === -1 )
                        {
                            var emptyRow;
                            //if(reportType === 'articleMix' ||  reportType === 'articleMixBySalesReps' || reportType === 'productMix' || reportType === 'productMixBySalesReps'){
                                emptyRow = {Month__c: month, SalesRep__c: salesRepNum, SalesRepName__c: salesRepName,
                                            ActualMatMargin__c: 0.0, ActualMatMarginPercent__c: 0, PrevYearMatMarginPercent__c: 0, PrevYearMatMargin__c:0,
                                            ActualGrossSales__c: 0, PrevYearGrossSales__c: 0, ActualNetWeight__c: 0, PrevYearNetWeight__c: 0,
                                            GrossSalesDeviation__c: 0, GrossSalesDeviationPercent__c: 0};
                            // }else{
                            //     emptyRow = {Month__c: month, SalesRep__c: salesRepNum, SalesRepName__c: salesRepName,
                            //                 MatMarginCurrYear__c: 0.0, MatMarginPercentCurrYear__c: 0, MatMarginPercentPrevYear__c: 0, MatMarginPrevYear__c:0,
                            //                 GrossSalesCurrYear__c: 0, GrossSalesPrevYear__c: 0, NetWeightCurrYear__c: 0, NetWeightPrevYear__c: 0,
                            //                 GrossSalesDeviation__c: 0, GrossSalesDeviationPercent__c: 0};
                            // }
                            filteredData.push(emptyRow);
                        }
                    });
                }
                filteredGroupedData.push(filteredData);
            });
        }
        return filteredGroupedData;
    },

    getHeaderInfo: function(component)
    {
        var accountId = component.get('v.accountId');
        var getAccountInfoAction = component.get('c.getHeaderInfo');
        var newReports = ['CompanyTurnoverAreaSet', 'CompanyTurnOverOfficeSet', 'CompanyTurnOverSBUSet', 'TurnoverCompanySASBUAC', 'TurnoverCompanySASOAC'];
        var reportType = component.get('v.reportType');
        getAccountInfoAction.setParams(
        {
            accountId: accountId
        });
        getAccountInfoAction.setCallback(this, function(response)
        {
            if ( component.isValid() && response.getState() === 'SUCCESS' )
            {
                var responseData = JSON.parse(response.getReturnValue());
                var selectedCompanyCode = component.get('v.companyCode');
                component.set('v.accountName', responseData.accountName);
                component.set('v.sapCompanyCode', newReports.indexOf(reportType) > -1 ? selectedCompanyCode : responseData.sapCompanyCode);
                component.set('v.salesRepName', responseData.salesRepName);
            }
            else
            {
                console.error(response.error);
            }
        });
        $A.enqueueAction(getAccountInfoAction);
    },

    getAccessibleColumns: function(component, reportType)
    {
        var getAccessibleColumnsAction = component.get('c.getAccessibleColumns');
        getAccessibleColumnsAction.setParams(
        {
            reportType: reportType
        });
        getAccessibleColumnsAction.setCallback(this, function(response)
        {
            if ( response.getState() === 'SUCCESS' && component.isValid() )
            {
                var accessibleColumns = response.getReturnValue();
                var reportColumns = {
                    grosssalescurryear__c: false,
                    grosssalesdeviation__c: false,
                    grosssalesdeviationpercent__c: false,
                    grosssalesprevyear__c: false,
                    matmargincurryear__c: false,
                    matmarginpercentcurryear__c: false,
                    matmarginpercentprevyear__c: false,
                    matmarginprevyear__c: false,
                    netweightcurryear__c: false,
                    netweightprevyear__c: false
                };
                accessibleColumns.forEach(function(item)
                {
                    reportColumns[item] = true;
                });
                component.set('v.accessibleColumns', reportColumns);
            }
            else
            {
                console.error(response.error);
            }
        });
        $A.enqueueAction(getAccessibleColumnsAction);
    },

    sortNumbers: function(a, b)
    {
        return a - b;
    }
})