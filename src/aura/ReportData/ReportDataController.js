({
	init: function(component, event, helper)
	{
		console.log("in record data init");
        var startYear = new Date().getFullYear();
        //alert('startYear=='+startYear);
		var reportType = component.get('v.reportType');
		var accountId = component.get('v.accountId');
		var salesRepId = component.get('v.salesRepId');

        var Vkbur = component.get('v.Vkbur');
        var Spart = component.get('v.Spart');
        var SBU = component.get('v.SBU');
        var Vtweg = component.get('v.Vtweg');
        var userType = component.get('v.userType');
        var companyCode = component.get('v.companyCode');

		var currentMonth;
		// Depending on the report type the data should be loaded fully from the server and filtered on the client, or filterring on server side
		if (( reportType === 'salesFigures' || reportType === 'salesFiguresBySalesReps' ) )
		{
			currentMonth = 11;
		}
		else
		{
			currentMonth = new Date().getMonth();
		}
		var selectedMonths = Array.apply(null, {length: currentMonth + 1}).map(function(num, index){
			return index + 1;
		});
		var reportWithGrouping = helper.getData(component, accountId, selectedMonths, reportType, salesRepId, userType, Vkbur, Spart, SBU, Vtweg, startYear, companyCode);
		var grandTotals = {
			actualSales: 0,
			actualWeight: 0,
			actualWeightBU: 0,
			actualMM: 0,
			actualMMPercent: 0,
			prevYearSales: 0,
			prevYearWeight: 0,
			prevYearWeightBU: 0,
			prevYearMM: 0,
			prevYearMMPercent: 0,
			netSalesDeviation: 0,
			netSalesDeviationPercent: 0
		};

		component.set('v.grandTotals', grandTotals);
		console.log('grandTotals: ', component.get('v.grandTotals'));
		component.set('v.reportWithGrouping', reportWithGrouping);
		helper.getHeaderInfo(component);
	},

	reloadReport: function(component, event, helper)
	{
		console.log('Reload report handler');
		component.set('v.showSpinner', true);
		var grandTotals = {
			actualSales: 0,
			actualWeight: 0,
			actualWeightBU: 0,
			actualMM: 0,
			actualMMPercent: 0,
			prevYearSales: 0,
			prevYearWeight: 0,
			prevYearWeightBU: 0,
			prevYearMM: 0,
			prevYearMMPercent: 0,
			netSalesDeviation: 0,
			netSalesDeviationPercent: 0
		};
		component.set('v.grandTotals', grandTotals);
		console.log('grandTotals: ', component.get('v.grandTotals'));
		var selectedMonths = event.getParam('selectedMonths');
        var selectedYear = event.getParam('selectedYear');
        var isyearchange = event.getParam('isyearchange');
        //alert('isyearchange=='+isyearchange);
		var accountId = component.get('v.accountId');
		var reportType = component.get('v.reportType');
		var salesRepId = component.get('v.salesRepId');

        var Vkbur = component.get('v.Vkbur');
        var Spart = component.get('v.Spart');
        var SBU = component.get('v.SBU');
        var Vtweg = component.get('v.Vtweg');
        var userType = component.get('v.userType');
        var companyCode = component.get('v.companyCode');

		// Depending on the report type the data should be loaded filtered from the server (getData) or filtered on the client
		if (( reportType === 'salesFigures' || reportType === 'salesFiguresBySalesReps' || reportType === 'CompanyTurnOverSBUSet' || reportType === 'CompanyTurnOverOfficeSet' || reportType === 'CompanyTurnoverAreaSet') && isyearchange != 'True')
		{
			console.log("filtering report");
			helper.filterReport(component, selectedMonths, reportType);
		}
		else
		{
			console.log("getting data");
			helper.getData(component, accountId, selectedMonths, reportType, salesRepId, userType, Vkbur, Spart, SBU, Vtweg, selectedYear, companyCode);
		}
	},

	updateGrandTotals: function(component, event)
	{
		var innerComponentTotals = event.getParam('currentTotals');
		console.log("innerComponentTotals: ",innerComponentTotals);
		var grandTotals = component.get('v.grandTotals');
		grandTotals.actualSales += innerComponentTotals.actualSales;
		grandTotals.actualWeight += innerComponentTotals.actualWeight;
		grandTotals.actualWeightBU += innerComponentTotals.actualWeightBU;
		grandTotals.actualMM += innerComponentTotals.actualMM;
		grandTotals.prevYearSales += innerComponentTotals.prevYearSales;
		grandTotals.prevYearWeight += innerComponentTotals.prevYearWeight;
		grandTotals.prevYearWeightBU += innerComponentTotals.prevYearWeightBU;
		grandTotals.prevYearMM += innerComponentTotals.prevYearMM;
		grandTotals.netSalesDeviation += innerComponentTotals.netSalesDeviation;
		if ( grandTotals.actualSales !== 0 ) {
            grandTotals.actualMMPercent = grandTotals.actualMM / grandTotals.actualSales * 100;
        } else {
            grandTotals.actualMMPercent = 0;
        }
        if ( grandTotals.prevYearSales !== 0) {
            grandTotals.prevYearMMPercent = grandTotals.prevYearMM / grandTotals.prevYearSales * 100;
        } else {
            grandTotals.prevYearMMPercent = 0;
        }
        if ( grandTotals.prevYearSales !== 0 ) {
        	grandTotals.netSalesDeviationPercent = grandTotals.netSalesDeviation / grandTotals.prevYearSales * 100;
        } else {
        	grandTotals.netSalesDeviationPercent = 0;
        }
        console.log("final grand totals: ", grandTotals);
        component.set('v.grandTotals', grandTotals);
        component.set('v.showSpinner', false);
	},

	showSpinner: function(component)
	{
		component.set('v.showSpinner', true);
	},

	hideSpinner: function(component)
	{
		console.log('doneWaiting handler');
		component.set('v.showSpinner', false);
		var grandTotals = component.find('grandTotals');
		var headerInfo = component.find('headerInfo');
		$A.util.removeClass(grandTotals, 'slds-hide');
		$A.util.removeClass(headerInfo, 'slds-hide');
	}
})