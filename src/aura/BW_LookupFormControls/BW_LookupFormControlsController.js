({
    init : function(cmp, e, helper) {
        cmp.set('v.queryFilters', []);
        cmp.set('v.availableOperators', helper.AVAILABLE_OPERATORS());
        var fields = cmp.get('v.objectInfo').fields;
        cmp.set('v.fieldInfoList', Object.keys(fields).map(function(x){
            return fields[x];
        }));
        helper.planAndExecuteSearch(cmp);
    },
    addFilter : function(cmp, e, helper) {
        var filters = cmp.get('v.queryFilters');
        filters.push({
            fieldPath : cmp.get('v.displayedColumns')[0].name,
            operator : helper.AVAILABLE_OPERATORS()[0].name,
            value : null
        });
        cmp.set('v.queryFilters', filters);
        helper.planAndExecuteSearch(cmp);
    },
    deleteFilter : function(cmp, e, helper) {
        var filters = cmp.get('v.queryFilters');
        cmp.set('v.queryFilters', filters.slice(0, filters.length - 1));
        helper.planAndExecuteSearch(cmp);
    },
    requery : function(cmp, e, helper) {
        helper.planAndExecuteSearch(cmp);
    }
})