({
    executeSearch : function(cmp, fieldPaths, objectName, qFilter, callback) {
        this.serverSideRead(cmp,
            'c.dynamicQuery',
            {
                fieldPaths : fieldPaths,
                objectName : objectName,
                queryFilterJson : ((qFilter !== null)? JSON.stringify(qFilter): null)
            },
            callback);
    },
    planAndExecuteSearch : function(cmp) {
        var t = this;
        var queryFiltersTranslated = cmp.get('v.queryFilters').map(function(qf) {
            var opAndVal = t.mapOperatorAndValue(qf.operator, qf.value);
            return new t.QueryFilter(
                qf.fieldPath,
                opAndVal.operator,
                opAndVal.value,
                qf.subfilters
            );
        });
        var fieldPaths = cmp.get('v.displayedColumns').map(function(x) {
                return x.name;
            });
        this.fireSearchStarted(cmp);
        var callback = function(searchResults, errorMessage) {
                if (errorMessage !== null) {
                    cmp.set('v.errorMessage', errorMessage);
                } else {
                    t.fireSearchResults(cmp, searchResults);
                }
            };
        this.executeSearch(
            cmp,
            fieldPaths,
            cmp.get('v.objectInfo').name,
            ((queryFiltersTranslated.length >= 1)? this.andxFactory(queryFiltersTranslated) : null),
            callback);
    },
    QueryFilter : function(fieldPath, operator, value, subfilters) {
        this.fieldPath = fieldPath;
        this.operator = operator;
        this.value = value;
        this.subfilters = subfilters;
    },
    andxFactory : function(subfilters) {
        return new this.QueryFilter(null, 'AND', null, subfilters);
    },
    orxFactory : function(subfilters) {
        return new this.QueryFilter(null, 'OR', null, subfilters);
    },
    mapOperatorAndValue : function(alphabetName, value) {
        var newOp = alphabetName,
            newValue = value;
        var translations = {
            'equals' : '=',
            'lessThan' : '<',
            'lessThanEquals' : '<=',
            'greaterThan' : '>',
            'greaterThanEquals' : '>=',
            'notEquals' : '!=',
            'startswith': 'LIKE',
            'contains': 'LIKE'
        };
        if (alphabetName in translations) {
            newOp = translations[alphabetName];
            if (alphabetName === 'startswith') {
                newValue = value + '%';
            } else if (alphabetName === 'contains') {
                newValue = '%' + value + '%';
            }
        }
        return {
            operator : newOp,
            value : newValue
        };
    },
    AVAILABLE_OPERATORS : function() {
        return [
            { name: 'equals', label: 'equals' },
            { name: 'notEquals', label: 'not equals to' },
            { name: 'lessThan', label: 'less than' },
            { name: 'greaterThan', label: 'greater than' },
            { name: 'lessThanEquals', label: 'less or equal' },
            { name: 'greaterThanEquals', label: 'greater or equal' },
            { name: 'startswith', label: 'starts with'},
            { name: 'contains', label: 'contains'}
        ];
    },
    fireSearchStarted : function(cmp) {
        var evt = cmp.getEvent('searchEvent');
        evt.setParams({
            searchEventType : 'started'
        });
        evt.fire();
    },
    fireSearchResults : function(cmp, results) {
        var evt = cmp.getEvent('searchEvent');
        evt.setParams({
            searchEventType : 'results',
            resultsList : results
        });
        evt.fire();
    }
})