({
	initialize: function(component) {
		FindRepUtils.initializeAttributeFromMethod(component, 'v.targetMarketOptions', 'c.getTargetMarketOptions', function() {
			component.find('targetMarketSelect').set('v.value', null);
		});
		FindRepUtils.initializeAttributeFromMethod(component, 'v.stateOptions', 'c.getStateOptions', function() {
			component.find('stateSelect').set('v.value', null);
		});
		FindRepUtils.initializeAttributeFromMethod(component, 'v.verticalOptions', 'c.getVerticalOptions', function() {
			component.find('verticalSelect').set('v.value', null);
		});
	},

	doSearch: function(component, event, helper) {
		var targetMarketSelect = component.find('targetMarketSelect');
		var stateSelect = component.find('stateSelect');
		var verticalSelect = component.find('verticalSelect');
		helper.doSearch(component, {
			'targetMarket': targetMarketSelect.get('v.value'),
			'state': stateSelect.get('v.value'),
			'vertical': verticalSelect.get('v.value')
		});
	},

	handleSearchTypeChangeToState: function(component) {
		component.set('v.searchType', 'state');
		component.find('verticalSelect').set('v.value', '');
	},

	handleSearchTypeChangeToVertical: function(component) {
		component.set('v.searchType', 'vertical');
		component.find('stateSelect').set('v.value', '');
	}
})