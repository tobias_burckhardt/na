({
	doSearch: function(component, searchParams) {
		var search = component.getEvent('executeSearch');
		search.setParams(searchParams);
		search.fire();
	}
})