({
	getRelatedRecords : function(component,helper) {

		var action = component.get("c.getRelatedData");

        action.setParams({ 
        	"parentApiName": component.get("v.parentApiName"),
        	"childApiName": component.get("v.childApiName"),
        	"fieldNames" : component.get("v.fieldsToDisplay"),
        	"filterFieldNames" : component.get("v.filterFields")

        });


        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
            	var envelope = JSON.parse(response.getReturnValue());
                helper.buildTableInfo(component,envelope.records, envelope.childFieldMap, envelope.fieldTypeMap);
                
                component.set("v.filterFieldsList", component.get("v.filterFields").split(','));
                console.log(envelope);
                helper.buildFilterObjects(component,helper,envelope);
                component.set("v.isLoading", "false");

            } else {
                var errors = response.getError();
                
            }
        });
        $A.enqueueAction(action);
		
	},

    getInitData : function(component,helper) {

        var action = component.get("c.getObjectNameFromApiName");

        action.setParams({ 
            "apiName": component.get("v.childApiName")
        });


        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                var label = response.getReturnValue();
                component.set("v.childObjLabel", label);

            } else {
                var errors = response.getError();
                console.log(errors);
                
            }
        });
        $A.enqueueAction(action);
        
    },

	
	buildTableInfo : function(component,objsList,childFieldMap, fieldTypeMap ) {
		 var columns = [];
		 var fieldNames = component.get("v.fieldsToDisplay").split(',');
		 

		 for(var x =0; x<fieldNames.length; x++){
		 	var column = {
                label: childFieldMap[fieldNames[x].toLowerCase()], 
                fieldName: fieldNames[x], 
                type: fieldTypeMap[fieldNames[x].toLowerCase()].toLowerCase(),
                sortable: true
            }
		 	columns.push(column);
		 }

		 component.set("v.columns", columns);
		 component.set("v.data", objsList);
		 component.set("v.filteredData",objsList);



	},
    sortData: function (component, fieldName, sortDirection) {
        var data = component.get("v.filteredData");
        var reverse = sortDirection !== 'asc';

        data = Object.assign([],
            data.sort(this.sortBy(fieldName, reverse ? -1 : 1))
        );
        component.set("v.filteredData", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer
            ? function(x) { return primer(x[field]) }
            : function(x) { return x[field] };

        return function (a, b) {
            var A = key(a);
            var B = key(b);
            return reverse * ((A > B) - (B > A));
        };
    },



    buildFilterObjects:function(component,helper, envelope){

    	var filterObjects = [];
    	var filterFields = component.get("v.filterFields").split(',');
    	

    	for(var x = 0; x<filterFields.length; x++){
    		var selectOptions = envelope.fieldToPicklistEntries[filterFields[x]];
    		if(selectOptions.length>0){
    			selectOptions = helper.createSelectOptions(selectOptions);
    		}

    		var object = {
    			selectedValue: "",
    			type: envelope.fieldTypeMap[filterFields[x].toLowerCase()].toLowerCase(),
    			label: envelope.childFieldMap[filterFields[x].toLowerCase()],
    			picklistValues: selectOptions,
    			apiName: filterFields[x]
    		};
    		filterObjects.push(object);
    	}
    	component.set("v.filters", filterObjects);
    },

    createObject:function(component,helper){

        var dTable = component.find("dataTable");
        var selectedRows = dTable.getSelectedRows();
        var childObjIds = [];
        for(var x = 0; x< selectedRows.length; x++){
            childObjIds.push(selectedRows[x].Id);
        }

        if(childObjIds.length > 0){
            var action = component.get("c.handleSaving");
            var test = component.get("v.recordId");
             

            action.setParams({ 
                "childIds": childObjIds,
                "junctionApiName": component.get("v.junctionApiName"),
                "parentApiName" : component.get("v.parentApiName"),
                "childApiName" : component.get("v.childApiName"),
                "parentId": component.get("v.recordId")

            });


            action.setCallback(this, function(response) {
                if (response.getState() === 'SUCCESS') {

                   $A.get('e.force:refreshView').fire();
                   helper.showToast(component,helper, "success", "Records created!");
                   component.set("v.isOpen", false);
          
                } else {
                    var errors = response.getError();
                    console.log(errors);
                    
                }
            });
            $A.enqueueAction(action);

            }
            else{
                helper.showToast(component,helper,"error", "Please select a record");
            }
        

    },

     showToast:function(component,helper,type,message){
        var toastEvent = $A.get("e.force:showToast");

            toastEvent.setParams({
                "type": type,
                "message": message
            });
        toastEvent.fire();
    },

  


    createSelectOptions:function(values){

    	var selectOptions = [];
    	var blankOption = { 
    		value: "",
    		label: "",
    		selected: "true"
    	}
    	selectOptions.push(blankOption);

    	for(var x = 0; x<values.length; x++){
    		var option = { 
	    		value: values[x],
	    		label: values[x],
	    		selected: "false"
	    	}
	    	selectOptions.push(option);
    	}
    	
    	return selectOptions
    },



  


    filterResults:function(component,helper){
        var ogFilter = component.get("v.filters");
        var filteredData = component.get("v.data")

        var filters = [];

        for (var x = 0; x<ogFilter.length; x++){
            if(ogFilter[x].selectedValue != ""){
                filters.push(ogFilter[x]);
            }
        }


        var toReset = true;
            for(var x = 0; x < filters.length; x++){
                if(filters[x].selectedValue != ""){
                    toReset = false;
                }
        }


        if(!toReset){

            for(var x = 0; x< filters.length; x++){
                filteredData = filteredData.filter(function(data) {

                    if(data[filters[x].apiName] == null){
                        return false;
                    }
   
                    var jsonData =JSON.stringify(data[filters[x].apiName]).toLowerCase();

                    return jsonData.indexOf(filters[x].selectedValue.toLowerCase()) !== -1;
                });
            }
        }
        component.set("v.filteredData", filteredData);


    },


     filterResults3:function(component,helper){
        var filters = component.get("v.filters");
        var filteredData = component.get("v.data")

        for(var x = 0; x< filters.length; x++){

            filteredData = filteredData.filter((data) =>  
                JSON.stringify(data).toLowerCase().indexOf(filters[x].selectedValue.toLowerCase()) !== -1);
        }
        component.set("v.filteredData", filteredData);

    },




     WORKING:function(component,helper){
        var filters = component.get("v.filters");
        var filteredData = component.get("v.data")

        for(var x = 0; x< filters.length; x++){

            filteredData = filteredData.filter((data) =>  
                data[filters[x].apiName].toLowerCase().indexOf(filters[x].selectedValue.toLowerCase()) !== -1);
        }
        component.set("v.filteredData", filteredData);

    }


})