({
    init:function(component,event,helper){
        helper.getInitData(component,helper);
    },
    openPicker : function(component, event, helper) {
        component.set("v.isOpen",true);
        helper.getRelatedRecords(component,helper);
    },
    closePicker: function(component,event,helper){
        component.set("v.isOpen",false);
        component.set("v.isLoading", true);
    },

    calculateFilters:function(component,event,helper){
   
      helper.filterResults(component,helper);
    },

    onSubmit:function(component,event,helper){
      
      helper.createObject(component,helper);
    },

     updateColumnSorting: function (component, event, helper) {
        // We use the setTimeout method here to simulate the async
        // process of the sorting data, so that user will see the
        // spinner loading when the data is being sorted.
        setTimeout(function() {
            var fieldName = event.getParam('fieldName');
            var sortDirection = event.getParam('sortDirection');
            component.set("v.sortedBy", fieldName);
            component.set("v.sortedDirection", sortDirection);
            helper.sortData(component, fieldName, sortDirection);
        }, 0);
    },
})