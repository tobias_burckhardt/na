({
    calculateTotals: function(data) {
        console.log("in rercalculate");
        var totals = {
            actualSales: 0,
            actualWeight: 0,
            actualMM: 0,
            actualMMPercent: 0,
            actualWeightBU: 0,
            prevYearSales: 0,
            prevYearWeight: 0,
            prevYearWeightBU: 0,
            prevYearMM: 0,
            prevYearMMPercent: 0,
            netSalesDeviation: 0,
            netSalesDeviationPercent: 0
        };
        if ( data !== undefined ) {
            data.forEach(function(item)
            {
                if(item.GrossSalesCurrYear__c){
                    totals.actualSales += parseFloat(item.GrossSalesCurrYear__c !== undefined ? item.GrossSalesCurrYear__c : 0);
                }else{
                    totals.actualSales += parseFloat(item.ActualGrossSales__c !== undefined ? item.ActualGrossSales__c : 0);
                }
                if(item.ActualNetWeight__c){
                    totals.actualWeight += parseFloat(item.ActualNetWeight__c !== undefined ? item.ActualNetWeight__c : 0);
                }else{
                    totals.actualWeight += parseFloat(item.NetWeightCurrYear__c !== undefined ? item.NetWeightCurrYear__c : 0);
                }
                totals.prevYearWeightBU += parseFloat(item.NetWeightBuomPrevYear__c !== undefined ? item.NetWeightBuomPrevYear__c : 0);
                totals.actualWeightBU += parseFloat(item.NetWeightBuomCurrYear__c !== undefined ? item.NetWeightBuomCurrYear__c : 0);
                if(item.ActualMatMargin__c){
                    totals.actualMM += parseFloat(item.ActualMatMargin__c !== undefined ? item.ActualMatMargin__c : 0);
                }else{
                    totals.actualMM += parseFloat(item.MatMarginCurrYear__c !== undefined ? item.MatMarginCurrYear__c : 0);
                }
                if(item.PrevYearGrossSales__c){
                    totals.prevYearSales += parseFloat(item.PrevYearGrossSales__c !== undefined ? item.PrevYearGrossSales__c : 0);
                }else{
                    totals.prevYearSales += parseFloat(item.GrossSalesPrevYear__c !== undefined ? item.GrossSalesPrevYear__c : 0);
                }
                if(item.PrevYearNetWeight__c){
                    totals.prevYearWeight += parseFloat(item.PrevYearNetWeight__c !== undefined ? item.PrevYearNetWeight__c : 0);
                }else{
                    totals.prevYearWeight += parseFloat(item.NetWeightPrevYear__c !== undefined ? item.NetWeightPrevYear__c : 0);
                }
                if(item.PrevYearMatMargin__c){
                    totals.prevYearMM += parseFloat(item.PrevYearMatMargin__c !== undefined ? item.PrevYearMatMargin__c : 0);
                }else{
                    totals.prevYearMM += parseFloat(item.MatMarginPrevYear__c !== undefined ? item.MatMarginPrevYear__c : 0);
                }
                totals.netSalesDeviation += parseFloat(item.GrossSalesDeviation__c !== undefined ? item.GrossSalesDeviation__c : 0);
            });
            if ( totals.actualSales !== 0 ) {
                totals.actualMMPercent = (totals.actualMM / totals.actualSales * 100);
            } else {
                totals.actualMMPercent = 0;
            }
            if ( totals.prevYearSales !== 0) {
                totals.prevYearMMPercent = (totals.prevYearMM / totals.prevYearSales * 100);
            } else {
                totals.prevYearMMPercent = 0;
            }
            if ( totals.prevYearSales !== 0 ) {
                totals.netSalesDeviationPercent = totals.netSalesDeviation / totals.prevYearSales * 100;
            } else {
                totals.netSalesDeviationPercent = 0;
            }
        }
        return totals;
    }
})