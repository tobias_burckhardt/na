({
	init: function(component, event, helper)
	{
		console.log('init inner data');
		var data = component.get('v.data');
		console.log('innerData', data);
		var totals = helper.calculateTotals(data);
		console.log("in controller after calc totals : ",totals);
		component.set('v.totals', totals);
		var recalculateTotalsEvent = $A.get('e.c:FinancialReports_UpdateTotals');
		recalculateTotalsEvent.setParams({
			currentTotals: totals
		});
		recalculateTotalsEvent.fire();
		var table = component.find('innerReportTable');
		if ( table !== undefined )
		{
			$(table.getElement()).stupidtable();
		}
		var accountCurrency;
		if(data[0].Currency__c){
			accountCurrency = data !== undefined && data.length > 0 ? data[0].Currency__c : 'USD';
		}else{
			accountCurrency = data !== undefined && data.length > 0 ? data[0].Waers__c : 'USD';
		}
		var accountMeasurement;
		if(data[0].WeigthUom__c){
			accountMeasurement = data !== undefined && data.length > 0 ? data[0].WeigthUom__c : 'KG';
		}else{
			accountMeasurement = data !== undefined && data.length > 0 ? data[0].Uom__c : 'KG';
		}
		
		var baseUnit = data !== undefined && data.length > 0 ? data[0].BaseUom__c : 'KG';
		component.set('v.reportCurrency', accountCurrency);
		component.set('v.measurement', accountMeasurement);
		component.set('v.baseUnit', baseUnit);
		var reportType = component.get('v.reportType');
		var reportsWithGrouping = ['salesFiguresBySalesReps', 'articleMixBySalesReps', 'productMixBySalesReps', 'shipToSalesFiguresBySalesReps', 'CompanyTurnoverAreaSet', 'CompanyTurnOverSBUSet', 'CompanyTurnOverOfficeSet', 'TurnoverCompanySASBUAC', 'TurnoverCompanySASOAC'];
		if ( reportsWithGrouping.indexOf(reportType) > -1 )
		{
			if ( data !== undefined && data.length > 0 )
			{
				var salesOrg = data[0].Vkorg__c + ' ' + data[0].Vkorg_Txt__c;
				var distributionChannel = data[0].Vtweg__c + ' ' + data[0].Vtweg_Txt__c;
				var division = data[0].Spart__c + ' ' + data[0].Spart_Txt__c;
				component.set('v.salesOrg', salesOrg);
				component.set('v.distributionChannel', distributionChannel);
				component.set('v.division', division);
				switch ( reportType )
				{
					case 'CompanyTurnoverAreaSet':
					{
						component.set('v.showHeader', true);
						break;
					}
					case 'CompanyTurnOverSBUSet':
					{
						var fullSbu = data[0].SBU__c + ' ' + data[0].SBU_Txt__c;
						component.set('v.sbu', fullSbu);
						component.set('v.showHeader', true);
						break;
					}
					case 'TurnoverCompanySASBUAC':
					{
						var fullSbu = data[0].Sbu__c + ' ' + data[0].Sbu_Txt__c;
						component.set('v.sbu', fullSbu);
						component.set('v.showHeader', true);
						break;
					}
					case 'TurnoverCompanySASOAC':
					case 'CompanyTurnOverOfficeSet':
					{
						var salesOffice = data[0].Vkbur__c + ' ' + data[0].Vkbur_Txt__c;
						component.set('v.salesOffice', salesOffice);
						component.set('v.showHeader', true);
						break;
					}
					default:
					{
						var salesRepNum = data[0].Salesrep__c;
						var salesRepName = data[0].Salesrepname__c;
						component.set('v.salesRepNum', salesRepNum);
						component.set('v.salesRepName', salesRepName);
					}

				}
			}
		}
	},

	initStupidTables: function(component, event, helper)
	{
		var table = component.find('innerReportTable');
		var firstRender = component.get('v.firstRender');
		if ( table !== undefined )
		{
			var tableElement = table.getElement();
			if ( !firstRender )
			{
				var lang = $A.get('$Locale.userLocaleLang');
				var country = $A.get('$Locale.userLocaleCountry');
				var locale = lang + '-' + country;
				try {
					numeral.language(locale);
				} catch ( ex ) {
					try {
						numeral.language(lang);
					} catch ( ex2 ) {
						numeral.language('en');
					}
				}
				$(tableElement).stupidtable(
				{
					"localizedFloat": function(a,b) {
						a = numeral().unformat(a);
						b = numeral().unformat(b);
						return a - b;
					}
				});
				firstRender = true;
				component.set('v.firstRender', firstRender);
				var reportType = component.get('v.reportType');
				if ( reportType === 'salesFigures' || reportType === 'salesFiguresBySalesReps' )
				{
					$(tableElement).find('thead tr>th.month').stupidsort();
				}
			}
		}
	},

	backToAccount: function(component, event, helper)
	{
		if ( typeof sforce !== 'undefined' && sforce !== null )
		{
			var accountId = component.get('v.accountId');
			var navEvt = $A.get("e.force:navigateToSObject");
			navEvt.setParams({
				"recordId": accountId
			});
			navEvt.fire();
			return false;
		}
	}

})