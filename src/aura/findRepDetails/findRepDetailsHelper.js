({
	showTab: function(component, tabName) {
		component.set('v.currentPage', tabName);
	}
})