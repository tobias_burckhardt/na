({
	selectTab: function(component, event, helper) {
		helper.showTab(component, event.getParam('page'));
	},

	showRepInfo: function(component, event, helper) {
		helper.showTab(component, 'repInfo');
	},

	showRegionInfo: function(component, event, helper) {
		helper.showTab(component, 'regionInfo');
	},

	showReferral: function(component, event, helper) {
		helper.showTab(component, 'referral');
	},

	goToResults: function(component) {
		var evt = component.getEvent('selectResult');
		evt.setParams({
			'result': null
		});
		evt.fire();
	},

	goToSearch: function(component) {
		component.getEvent('clearResults').fire();
	},

	goToRepUser: function(component) {
		var evt = $A.get('e.force:navigateToSObject');
		evt.setParams({
			'recordId': component.get('v.result.repUserId')
		});
		evt.fire();
	}
})