({
    init : function(cmp) {
        var record = cmp.get('v.record');
        var cells = cmp.get('v.displayedColumns').map(function(fieldInfo){
            return {
                displayType : fieldInfo.displayType,
                value : record[fieldInfo.name]
            };
        });
        cmp.set('v.cells', cells);
    }
})