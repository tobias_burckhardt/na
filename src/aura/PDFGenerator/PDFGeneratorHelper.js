({
	generatePdf: function(component)
	{
		var that = this;
		var reportType = component.get('v.reportType');
		console.log('reportType', reportType);
		var greyedColumns = reportType == 'articleMix' || reportType == 'articleMixBySalesReps' || reportType == 'articleMixBySalesReps2' ? 3 : 1;

		try {
			this.showHideMaterialMarginColumns(false);

			var pdfsize = 'a2';
			var pdf = new jsPDF('l', 'pt', pdfsize);

			var totalPagesExp = '{total_pages_count_string}';

			var startY = 100;
			var dataColumnOffset = 0;
			var beforePageContent = that.createHeader(component, pdf);
			var afterPageContent = that.createFooter(pdf, totalPagesExp);
			if ( reportType=='TurnoverCompanySASOAC' || reportType=='TurnoverCompanySASBUAC' || reportType=='CompanyTurnoverAreaSet' || reportType=='CompanyTurnOverOfficeSet' || reportType=='CompanyTurnOverSBUSet' || reportType == 'salesFiguresBySalesReps' || reportType == 'articleMixBySalesReps' || reportType == 'articleMixBySalesReps2' || reportType == 'productMixBySalesReps' || reportType == 'productMixBySalesReps2')
			{
				var dataTables = document.querySelectorAll('table.cReportInnerData');
				if ( dataTables )
				{
					for ( var i = 0; i < dataTables.length; i++ )
					{
						var dataTable = dataTables[i];
						console.log('dataTable.id', dataTable.id);
						var data = pdf.autoTableHtmlToJson(document.getElementById(dataTable.id));
						var salesRepData = dataTable.parentElement.children[0].textContent;
						pdf.setFontType('bold');
						pdf.text(40, startY + 30, salesRepData);
						pdf.setFontType('normal');
						startY = startY + 40;
						//pdf.rect(data.settings.margin.left, row.y, data.table.width, 40, 'DF');
						if ( i === 0 )
						{
							that.generatePDFTable(pdf, data, component, totalPagesExp, reportType, greyedColumns, startY, dataColumnOffset, beforePageContent, afterPageContent);
						}
						else
						{
							that.generatePDFTable(pdf, data, component, totalPagesExp, reportType, greyedColumns, startY, dataColumnOffset, function(data){}, afterPageContent);
						}

						startY = pdf.autoTableEndPosY() + 20;
					}
				}
			}
			else
			{
				var res = pdf.autoTableHtmlToJson(document.getElementById("dataTable_0"));
				console.log('res', res);
				this.generatePDFTable(pdf, res, component, totalPagesExp, reportType, greyedColumns, startY, dataColumnOffset, beforePageContent, afterPageContent);
			}
			console.log('offset', dataColumnOffset);
			var grandTotalData = pdf.autoTableHtmlToJson(document.getElementById("grandTotal"));
			pdf.autoTable(grandTotalData.columns, grandTotalData.data,
			{
				startY: pdf.autoTableEndPosY(),
				styles:
				{
					fillColor:[245,179,37],
					overflow: 'linebreak',
					fontSize: 14,
					rowHeight: 40,
					columnWidth: that.getMetricColumnWidth(reportType),
					halign: 'right',
					valign: 'middle'
				},
				columnStyles:
				{
					0:{halign: 'left', columnWidth:140},
					1:{halign: 'left', columnWidth:'auto'}
				}
			});

			// Total page number plugin only available in jspdf v1.0+
    		if (typeof pdf.putTotalPages === 'function') {
        		pdf.putTotalPages(totalPagesExp);
    		}
			//console.log('pdf', pdf.output());
			pdf.save("report.pdf");
			this.showHideMaterialMarginColumns(true);
		}
		catch ( ex )
		{
			console.error(ex);
		}

	},

	generatePDFTable: function(pdf, res, component, totalPagesExp, reportType, greyedColumns, startY, dataColumnOffset, beforePageContent, afterPageContent)
	{
		var that = this;
		console.log('columns', res.columns);
		console.log('data', res.data);
		pdf.autoTable(res.columns, res.data,
		{
			startY: startY,
			beforePageContent: beforePageContent,
			afterPageContent: afterPageContent,
			headerStyles:
			{
				fillColor: [245,245,245],
				textColor: 80,//[51, 51, 51],
				fontSize: 14,
				fontStyle: 'normal',
				valign: 'top'
			},
			styles:
			{
				overflow: 'linebreak',
				fontSize: 14,
				rowHeight: 40,
				columnWidth: that.getMetricColumnWidth(reportType),
				//lineWidth: 0,
				//lineColor: 255,
				halign: 'right',
				valign: 'middle'
			},
			columnStyles: that.getDescriptionColumnStyles(greyedColumns),
			createdRow: function(row, data)
			{
				if ( data.row.dataKey === 0 )
				{
					row.styles.fillStyle = 'DF';
				}
			},

			createdCell: function(cell, data)
			{
				if (data.column.dataKey < greyedColumns && data.row.index > 0 )
				{
					// console.log('cell styles', cell.styles);
					cell.styles.fillColor = [245, 245, 245];
					cell.styles.fillStyle = 'DF';
					cell.styles.columnWidth = 'auto';
					cell.styles.halign = 'left';
					// cell.styles.halign = 'right';
					// if (cell.raw > 600) {
					//     cell.styles.textColor = [255, 100, 100];
					//     cell.styles.fontStyle = 'bolditalic';
					// }
					// cell.text = '$' + cell.text;
				}
				if ( data.column.dataKey == greyedColumns - 1 )
				{
					dataColumnOffset = cell.x;
				}
			},
			drawCell: function(cell, data)
			{
				var rows = data.table.rows;
				if (data.row.index == rows.length - 1 && (reportType == 'salesFiguresBySalesReps' || reportType == 'articleMixBySalesReps' || reportType == 'articleMixBySalesReps2' || reportType == 'productMixBySalesReps' || reportType == 'productMixBySalesReps2') )
				{
					pdf.setFillColor(245, 179, 37);
					pdf.setTextColor(255, 255, 255);
					pdf.setFontType('bold');
				}
			}
			/*
			drawRow: function(row, data)
			{
// 					var offset = row.cells[3].x;
// 					var rowWidth = data.table.width - offset;
// 					console.log('offset:', offset);
// 					pdf.setFillColor(245,245,245);
// 					pdf.rect(data.settings.margin.left, row.y, data.table.width, 40, 'DF');

			},
*/
		});
	},

	createHeader: function(component, pdf)
	{
		var logo = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAEsASwAAD/4QCMRXhpZgAATU0AKgAAAAgABQESAAMAAAABAAEAAAEaAAUAAAABAAAASgEbAAUAAAABAAAAUgEoAAMAAAABAAIAAIdpAAQAAAABAAAAWgAAAAAAAASwAAAAAQAABLAAAAABAAOgAQADAAAAAQABAACgAgAEAAAAAQAAAhygAwAEAAAAAQAAAPQAAAAA/+0AOFBob3Rvc2hvcCAzLjAAOEJJTQQEAAAAAAAAOEJJTQQlAAAAAAAQ1B2M2Y8AsgTpgAmY7PhCfv/AABEIAPQCHAMBIgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2wBDAAICAgICAgMCAgMEAwMDBAUEBAQEBQcFBQUFBQcIBwcHBwcHCAgICAgICAgKCgoKCgoLCwsLCw0NDQ0NDQ0NDQ3/2wBDAQICAgMDAwYDAwYNCQcJDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ3/3QAEACL/2gAMAwEAAhEDEQA/AP38ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//Q/fyiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD/9H9/KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP/0v38ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAoorlPHPjLRPh74R1bxp4il8rT9ItnuZiPvNt4VFHd5GIRR3YgVMpKKcpbI2w+HqV6saFGPNKTSSW7b0SXqz41/bV/aJ1b4YWWkeB/AV99l8S300OpXM6cm2sraQMiMP+nmRNpHeJXH8Qr6s+EfxL0f4ufD7SPHejYRb+HFxBnLW11H8s0Le6OCAe64boRX8+HxA8ca38SvGmr+OvETZvtXuGmZAcrDGPlihTP8EUYVB64z1Jr6x/Yd+Nf/Cv/H7fD7XZ9mheLpUSEucJbaoBtib2FwoETf7Qj9DXxeC4gc8wfO/clovLs/n19fI/q7i3wVp4XgumsJG+LoJzm1vO6XPHz5UlyeUXZXkz9p6KKK+2P5LCiiigAooooAKKKKACiiigAooooAKKKKACiiigD//T/fyiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACvyP8A2+fjV/b3iC3+DegXG6w0V0u9ZZDlZb4rmGAkdRAjb2H99l7pX6D/AB++Llj8FvhnqfjCXZJqBH2TSrZz/r7+YERKR3VMGR/9hT3xX89V/f32q31zqmqTvdXt7NJcXM8hy8s0rF3dj6sxJNfJ8T5jyU1hYPWW/p/wf63P6V+j3wN9bxkuIsXH3KTtTv1n1l/24np/ed1rEq0oLKQyMyMpDKynDKwOQQRyCDyD2NJRXwZ/Y5++f7K3xpX4z/C+1vtRlVvEOildP1hejNMi/u7jH924Qb/TfvUfdr6Vr+fn9mf4yy/BX4oWWu3cjDQdT26frUY5H2Z2+WfH963f5/XZvA+9X9AUM0VxEk8DrJFIodHQhlZWGQQRwQRyDX6bkWY/WsPaT96Oj/R/P8z/AD98Y+Bv9Xc8lLDxth615Q7L+aH/AG63p/dcSSiiivbPyUKKKKACiiigAooooAKKKKACiiigAooooA//1P38ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiivj79sv42H4VfDV9B0O48vxJ4qWSysyhxJbWuALm59QVVgiH++4I+6a58ViYYelKtPZHtcO5Fis6zKjlmDV51Hb0XVvySu35I/Oz9sP41f8AC2vidJpejz+Z4b8LNLY2O05S4uc4ubnqQQzL5aH+4mR9418l0iqFUKowAMAUtfk2JxE69WVapuz/AEqyDI8Nk+XUcswatCmrLz7t+bd2/NhRRRWB7AdeDX7IfsJfGs+M/BUnwv16436z4ViX7GznL3GlE7Y+vU27YiP+yY/U1+N9d38MviDrPwr8d6R480LLXGlzhpIc4W5tn+WaBvaRCR7Nhuor08px7wmIVT7Oz9P+AfA+JPBlPiXI6mBS/er3qb7TWy9JK8X636I/pNornvCXinRfG/hnTPF3h2cXOm6tbR3VtIOpSQZww7Mp+Vh1DAg9K6Gv1SMlJKS2P85q1GpRqSpVVaUW00901o0/QKKKKZkFFFFABRRRQAUUUUAFFFFABRRRQB//1f38ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAKOp6lYaNpt1q+qTpbWVjDJcXE0hwkcUSlndj2CqCTX88fxz+LGofGj4lan41uPMSydha6VbNnMFhESIVx2eQkyOB/G5HYV+gn7fnxq/svR7b4LaBPi71ZEvNbZDzHZBsxW5x0M7ruYf881x0evlH9nLwJYXFvP471OFZ5kna209XGVjMYHmSgH+PJ2qe2Djk1+T+JPF1HK8LKc9VC2i+1J7L5de2umh/W3gtw5SyHJ6nFeYQvUq+7TXXlvv5c7W/8ALG6vzWPGtI+DPxK1q2W8ttGeCJxuQ3ciWzMD0IVyG59wK6HTv2eviTfJM1xBaWDRkBVuLgEy/wC75QkAA9SRX2P4x8e+GPAtrFd+I7oxtcEiGGNTLPKV+8VQdhnliQB65ri9J+Pnwz1N/Lmv5tOb1vbdkU/8CTePzxX88x484rxdB4nB4Rez6NQlLbzvr2en3H6auLeIMRSdfDYdcnRqMn+uvnofDfifw1q3hDWp9A1tI0u7cIzCJxIhWRQykMPUH6iuu8GfCTxf470yTWNEFqlrHM0Ae5mMZd1ALbQFY4GRycc1gePtfj8UeNNZ16Bi0F3dOYCQRmFMJHweRlVBr7m+Cemf2X8MdEQjDXUcl431nkZh/wCO4r7Pi3ibHZTklDFWSrz5U01om4ty0v0atufT8Q57i8uyulXsvay5U9NL2bel+6sfD/jbwFr3gC+trDXzbmS7hM8Rt5fMXarbTnKqQc+1cXXuX7Q+pfbviRLaKcrp9nb249mYGVv/AEMV4bX1PDmMxGLyyhisVbnnFSdtFrqvwse/kuJrYjAUq+I+KSTdvPVfgfpZ+wH8a/7P1G5+CfiC4xb3xlvtCZzwk4G65thns4BlQf3hJ3Ir9XK/mH0nVtT0DVbLXdFuGtNQ064iurSdPvRTQsGRh9COR3HFf0PfBD4q6Z8ZfhvpXjiwCxTzp5GoWynP2a+hwJovXG75kz1RlPev1fhjMfaU/qs3rHb0/wCAfyN9ILgb6ljo8QYSP7us7Tt0qd/+30v/AAJNvdHrVFFFfVn83hRRRQAUUUUAFFFFABRRRQAUUUUAf//W/fyiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigArh/iT4+0T4YeCNX8deIGxZ6VbtLsBw80p+WKFP8AblkKovufSu4r8dv28PjV/wAJd4wi+E+gz7tJ8My+bqRQ/LPqhXAQ9iLZGI/66MwPKivNzXHrCYd1Ouy9T73w34NqcS55SwGvsl71R9oLf5y0ivN32TPiTxj4t1vx34p1Xxn4kl87UtYuXupyD8qluFjTPRI0ARB2VRX358INM/sn4a6BbMu15bb7U4/2rhmk/kwr84ooHupY7WIZed1iUf7TkKP1Nfq7YWUdhZ2umx8JbRRQL9I1CD+VfyN4yY5/VcPh29ZSlJ/9uq3/ALcf2f4hzp4fB4fBUVyxT0S2SirJLyV9D4I/aA1RtR+Jd5bhi0enW9vaqM8K23zHx/wJ+a8WrpfGeqf214v1vVs5F1f3Dqf9kOVX/wAdArmq/Tchwf1TLcPhraxhFP1sr/iff5Rhvq+Bo0P5YpfO2v4hseT93Hy7nao9S3A/Wv1X0TTl0vSNP0mMYFpbQW4H/XNAv8xX5reANL/trxxoOlkZWfUIN4/2I23t/wCOqa/SrWdRXS9Kv9WkOBaW09wT/wBc0Zv6V+R+MGIlUrYXBQ395/e0l+TPzrxIrOdXD4WO+r++yX5M/Nf4g6p/bXjrX9TBys2oThD/ALEbeWv6KK4+je8pMsnLuS7H/abk/rRX7XhcPHD0IUI7RSS+SsfqGHoqjSjRjtFJfcrBX2R+xb8a/wDhWHxJXwtrU/l+HfFzx2sxc4S2v/u203PADk+U59CpPC18b0hGQQe/pwa78JiZ4erGtDdHlcR5Dhs6y2tleMXuVFb0e6a84uzXmj+omivlL9kP42H4v/DGG21ifzPEnhvy7DU9x+edQv7i6/7bIMMf+eiv7V9W1+s4bEQr0o1aezP81c/yTE5PmNbLMYrVKbafn2a8mrNeTCiiitzyAooooAKKKKACiiigAooooA//1/38ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAoopCQoLMcAcknoBQB4H+0l8Y7f4KfDC/8Rwsja1ef6Do0DYO+8lU4cr3SFQZG7ELt6sK/n5nnuLqeW6u5XnuJ5GlmlkO55JJCWd2PdmYkk+pr6X/AGr/AI0t8ZPijcSaXOZPDnh/zNP0kA/JLhv39yB/02dRtP8AzzVPU18x1+Z59mP1rEWi/djov1f9dD/QDwb4G/1eySNTERtiK1pT7pfZh/26nd/3m+yO8+F+l/2x8RPD9iw3J9ujmcf7EGZT/wCgV+ifiXUxo/h7VdXc4+x2dxPn/aRGI/XFfF/7N+mfbPH02oMMrp2nzPn0eZljH6Fq+jPjrqf9m/DHVgDhr0wWa+/myAt/46pr+aPEL/b+JsLly29xP/t6Wv4WDjH/AGvPaGDW3ur/AMClr+Fj4V8JeF9X8aa3b+H9I8s3dwruWmbairGu52Y4JwPYEk16N4m+BXjHwroV54h1C50+S2sUEkqxSuZCpYL8oaNQTkjjNZPwk8a6N4C8Tza5rVvcXCNZyW8X2cKWR3ZCSQxUYKqR1r0z4o/HHQfGXhCfw3odnewy3csJlkuAiIIomDkDazEliAK/Qc3x/EEc6pYfA0v9m93mla/X3tb6WX4n2GY4vOFmdOjhKf7n3eZ289db9Ecn+zzpn2/4kQ3TDKafZ3FwfZmAiX/0M19TfGnU/wCy/hjrkgOGuYktF+tw6of/AB3NeN/svaZ83iHWmHQW1mp/76kb/wBlrov2m9S8jwppWkqcG9vzKw9Vt4z/AOzOK/P+IP8AhQ45o4bdQcF8or2j/Nnx+c/7ZxXSodIuK+733+bPiqiiiv3o/XAooooA9t/Z8+L918FPidp3i0s7aVN/oWsQLz5ljKw3MB3eFgJV9dpX+I1/QlZ3lrqNnBqFhKlxbXMaTQyxnckkcgDKykcEMCCD6V/MBX67fsEfGr/hIvDM/wAH9fuN2o+HozPpLOfmm01mwYxnqbZ2AH/TNlA4U19bwvmPJN4Wb0e3r2+f9bn80fSE4G+tYSPEeEj79L3alusOkv8At1uz/uvtE/RKuf8AEvizwv4N01tY8W6tZaNYqdpuL6dLePceihnIBY9gOTXQV+eHjDW5p7Z/jpq11Zx3t/45bwrpNzqdl/aVvoOiWlxNaySW9qSALmeW3aWSQfOQyoDtXFfX4zFexjdLX9F1/I/mPhbh1ZrXcakmoppabylK/LFaO10pNu0rJO0W7J/a3hD4m/Dzx+ZU8FeI9M1qS3G6WKzuo5ZY16ZaMHeoPYkYNdzXwXpdtf8Axh0fxb4hs7yx1DUPCCRXng7x/pumPpEst9HHI89qyMzedBE8axTgExSLIyFd65H2N4A8T/8ACbeBfD3jHyvIOuaXZ6gYh0jNzCshUZ7KWxU4PFOrpJeaffW3n+bTummbcT8OU8ufNQk7JqMoveLcVKOtot3TejhGUZRlGUVZN9dRRRXcfIBRRRQAUUUUAf/Q/fyiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACviT9t342H4c/D0eB9CuPL8QeLY5LcMhw9tpw+W4m45Vnz5SH1ZiOVr7F17XdK8MaJf+ItcuFtdP0y3lurqZzhY4oVLMfwA6dT0r+dr4w/E7VfjB8RNW8eapujS8k8uytmOfstjFkQRemQvzPjq7Me9fP8RZj9Xw/s4P3paei6v9P+GP23wO4G/tzOfr+KjfD4e0n2lP7Mfw5n5Kz+I8xACgKowAMAe1LRRX5uf3efYX7L+mbNM1/WmHM1xBaof9mJS7fq4qf9p7U/K0LQ9HU83N3LcOP9mBNo/WSu7+Ammf2d8MtOkIw1/LcXh9w7lV/8dQV4H+0rqf2rxvZaYpyNP05Mj0e4dnP/AI6Fr8Eyz/hR48qVd1CUv/JI8i/Gx+RYD/bOLZ1OkXL/AMlXKvxseSaD4D8ZeKLRr/w/o9zfWyOYmljChN4AJUFiuSARnGcVna94a1/wtdx2PiKwm0+eWPzUSYDLJkjcCCQRkEV98/BXTf7L+GOiIRhrqOS8b63Dsw/8dxXyz+0Rqn234jT2wOU02zt4MejMDK3/AKHX2eRcaYrMs/rZXGnH2UOfXW9ovlXW2r8j6fKeJ8Rjc3q4CMF7OPNrrfR2721fkfQv7O+mfYfhxHdsMPqN7cTk+qoREv8A6Aa8e/ab1Pz/ABVpOkqeLKwMrD/auJD/AOyxivqT4e6X/YvgXQNMIw0OnwFx/tyL5jf+PMa+Lfivb6x4u+LesWOj2VxezxSxWcccMZY4hjVc9MAbsnJIHvXxXB1SGM4txWYTdox9pK72SvyrX/Cz5fhqccTxFXxk37sed3fa/KvwZ5houg614kvhpmgWU1/dFWfyoVyQi9WJJAAHqTWtr3gPxl4XtEv/ABDpFxYW0kgiWWULtMhBIXKscEgHrX218Hfhifh9pM1xqRSXWdSC/aTGdyQxrysKH+LBOWPQt04Az4l+0T48tNYvrfwXpUglh02UzXsinKm5wVWMHofLBO7/AGjjsa+ywHHNfMs+/s7LacZUF8U9b2S1a1ta+i0d9+un02E4rrY3NvqWCgpUlvLXZbta2tfRaa/l4hoHgfxf4pt5Lvw7pNzfwQv5byRAbA+M7csVBOCDx61un4QfE8c/8I3e/wDkP/4uu6+G/wAb7PwD4YTw6+iSXbrcTTtPHcLHvMpB5UocEAAdegrvf+Go7H/oW5//AALT/wCN10ZlnHFlPFVIYLBQlTT91tq7Xf41v6GuOzLiGGInHDYWLgno21drv8a39D5y1L4eePNGtXvdU0C/t7eIZeUwlkUepK7sD36VH4C8b638OPGOkeOfDj7b/SLhZ0XOFmjPyyQvj+CWMsjexz1Ar7++HfxAsPiJo8+rWFpNZfZpzbyxTFW+baGyrLwykN3A+lfEHxh0Sw8P/EbV9P0yNYbZmiuEiQYWMzxq7Ko7AMTgdhV8JcZ4zG5jVy3H0VTrU1zaPs15vXVNNNp/nlk+eTzStWynM6CT5Xdbpp6NNa7p99Uf0H+A/GmifETwfpPjbw7L5un6vbJcRZ+8hPDxuOzxuCjDswNfMXjHwN4g8D+I1kXSdV17wO3iqDxnbDQUjuNS0vVFLPcW8lq5DT2VxIzShoMyxuzKVKkEfKf7BHxq/wCEe8SXHwd1+fbp+uu11o7O3yxagFzLCM9BcIu5R/z0UgcvX67V/R+DrQzDDRq3tJb+vX5P+tT+L+KMqxXBGf18Ao89GWsb396DvytNbTjrG/dSTTi2n8SNZ+KPGereKtG+EmkeI/DmlePPL/ty/wBfsv7NsNMZ0MV3dafbzAXUt7dRYUjYsIkAlZt24N9kaLo+n+HtGsNA0mLybLTbaGztoxzshgQIi/gqgVp0V6GHwyptybu3/wAPp8/62t8XnWfzx8YUYw5YR87ttJRTlLS7UUktEkul3JsooorpPnwooooAKKKKAP/R/fyiiigAooooAKKKKACivn3U/wBqr9n3RtSu9H1PxpZW95YTy2tzE0c5Mc0LFHQ4iIyrAjg4rqfBHx2+EHxI1I6P4J8V6fql+FLi1RzHOyryxWOQIzADk7QcDrXNHGYeUuSM032uj3q/C2dUKLxNbB1Y00r8zpySt3u1ax6zRXk/in45/CfwT4qt/BHinxHbafrl19nMNlIkpd/tTmOHBVCvzsMDn6113jPxv4V+HmgTeKPGeox6XpVu8cctzKGZFaZgiAhFZvmYgdK09vT973lpvrt69jieT49OknQner8Huv3/APDp7262vudVRXmunfGD4a6v4Eu/ibpuuwT+F7HzvtGpKsgij8g7ZMgoH+U8HC1L4N+LXw6+IOh6h4l8G63BqmmaSzpeXESyKsLRxiVgQ6KThCG4BpLEUm0lJa6rXddyqmSZjThOpPDzUYS5ZNxlaMv5W7aS8nqei0V5h8PfjP8ADD4rS3kHw91+31p9PSKS5ECSL5aTFghPmIv3ip6Z6V6DqmpWOi6Zd6xqkwt7OxgkubiZskRwwqXdjgE4VQTwM1UKsJx54NNd+hji8uxeFxH1TE0pQqae7KLUtdtGr69O5eor5uH7Xv7NzAMPHVhg8j91cf8AxqvVPA3xR+HnxLt5rnwH4gsdaW3x5y2soaWLPQvGcOgPYsoB7VnTxdCo+WnNN+TR343hnOMHSdfGYSpCC6yhKK+9pI72ivKdV+OHwp0Px1D8NNV8RW1v4nuJre3i05klMrS3YBhUEIUy4YY+bvzTfHvxz+E/wv1W30Tx74jttHvrq3F1DDMkrM8JZk3jy0YY3KR1zxTeJopNuastHqtGZU+H80qTp0qeGqOU1zRShJuUf5oq2q81oesUVhaH4m0HxL4etfFehXsV7pF9bi7t7uIkxyQkZ3DjPTqCMg8EZrjPh98aPhf8VLi8tPh94gt9am0+OOW5WFJVMaSlghPmIvUqemelX7andLmV3trv6dznjleNcKtVUZctPSb5XaDbslJ292701troeoUV414+/aD+Dfwx1H+x/Gvii0sdRCh2s0ElzcIrcgvHAsjJkcjcBkcis7wb+0x8DvH+t2nhvwp4qt7vVb5mW3tGhuIJZWVS5CiWJASFUnr0FZPGUFP2bmubtdXO6HC2cywv16OEqOla/PyS5bb35rWt57Hu1FRTzxWsElzO2yKJGd2PQKoyT+Ar5zH7Xv7NzAMPHVhg8j91cf8Axqrq4ilSt7SSV+7SObLsjzHMFJ4DDzqctr8kZStfa9k7Xs7eh9I0V85w/tbfs53E8VtB43sXlmdYo1EdxlnchVH+q7kgV0XjD9or4K+AfENz4U8YeKrTTNWtFjae1lSYugmQSISUjZfmVgeD3rP69hrc3tI29Udr4Rz1VVQeCq87TaXs53aVk2la9k2rvpddz2qivAdJ/am/Z71u+i02w8c6X587BIxO726ljwBvmREyfrXrfirxb4c8EeHbvxZ4qvo9P0iwRHuLuQMyRq7KikhAxOWYDgHrWkMTRnFyhJNLfVHJishzPC1oYfE4ecJz0jGUJJye1kmrvXTQ6OiuK8CfETwV8TdGk8QeBNVi1fTorh7V54VdVE0aqzJh1U5AdT0xzXO6d8cfhRq3juT4Zad4itp/E8M89s+mqkolWW2VnlXJQJlFUk/Njin9YpWT5lZ7a7+nchZLmDqVaSoT5qSbmuWV4JbuSt7qXVux6vRXlPxC+OHwo+FU8Nn498R2ml3VwnmxWp3zXDR5xv8AKhV3CkggMQASDzxXF+Hv2sf2ffE+p2ujaT4uga9vp47a3hmt7mAyzSsFRFMsSrlmIAGetZzxmHjP2cppPtdXOvD8K51iMN9coYOpKlvzKEnG3e6VrH0VRXg2v/tPfAXwvrd94b1/xhZ2epabO1td27xzlopU+8pKxEZHsTWR/wANd/s3/wDQ82P/AH6uP/jVS8fhk7OpH71/mbU+Ds/qQVSngari1dNU5tNPZp22Z9IUVi+HfEOi+LNCsfEvh26S+0zUoUuLW5QELLE4yrAMAcEeoFbVdSaautj56rSnTm6dRNSTs09Gmt013CiiimQFFFFABRRXnHxa+JOj/CT4f6v471rDx6fCfIgzhrm6k+WGFfeRyB7DLdAaipUjCLnN2SOrBYOti8RDC4aPNObUYpdW3ZL7z4H/AG/vjV5cVr8EfD8/zTCK/wBfZD0jzutrU4P8ZAlcf3QnZjX5bVs+I/EOseLdf1HxT4gnNzqerXMl3dS9mllOSAOyqMKo7KAO1e9fAv4aeGfGukazqPia1e5Ec8drblZXi8v5N7sNpGW+ZeuQPSvxHi7iejhKdTM8WnyJpJLV2bsrXa73evc/0R4aybB8F8O08LLXls5tbynK13rb0X91I+bKa2Qp28nHA96+rvEH7Md2srS+FNYjeInIg1BSrr7ebGCD+KCr3gT9nS603WbfV/GV5bTw2kiyx2drucSyIcr5jsq/IDztAOehIHX5CfiLkKwzxMa12l8NnzN9rW/HbzPXlxplKoOtGrfys7+lrfjt5n0d4S0z+xvC2j6Rt2m0sbeJl9GEY3f+PZr8/fi5qDa18StfliO7F39ki+kCrEAP+BKa/QbxP4is/Cmg33iPUGAisomkwesknREHqXfAH1r86PA9pN4m+IWjQXfzyX2qRzznrn5/Ok/ka/PfC+E1Uxud11pGL17t+/L7rL7z43gOMlPFZpVWiT18/if3WX3n6R6Lpy6XpOn6TGMC0toLcD/rmir/ADFfnL4nlfxf8TtQER3f2nrJt07/ACGUQr/46K/RjXNRXStH1HVpDgWlrPcE+8aMw/UV+fXwW09tY+J2h+aN3kSyXsn1hRnz/wB94rDw0nKhQzDN6m8Ib+dpSf4pGXA8nSpYzMZ7xj/nJ/kj9FgiRARR8IgCqPZeB+lKBjcRxnhiO+OxrkfH2sPoHgnXNYicxy21jMYmHBEjjYhB9QzDFfnv4L8deIPA+sx6vplxJKpYfaraR2Mdyn8QcEn5j2bqDz9fl+GOB8TneDrYmhUScHZJr4na7V76dOj36Hg5FwrXzTDVa9KaTjok1u7X36dOj36H1x8fPF/jDwvoltD4ejNvZ35aG51JDmWFj0iX/nmXGcP17DB5r4u8O6Df+J9csvD+lgNdX0ojQuflXOSzseuFUFj34r9J7G78O/ELwqlyiJe6Tq0BDxSeh4ZG/uujccchhkdq8l8AfAw+CPGj+JTqi3VrAkyWcHlkSgTDbmViduVU4+UcnnjpX1fCXGGDyfKsRg6tNU8TDmto/flrZS7OL0adlbbqfQcPcSYbLcvrYapDkrxvbR+89bJ+aelnZW+ZraD8C/hx4bsRLrNuuqTRrumur9ysQI6kRgqiL6ZyfU1YGl/AO5k+xLF4aaQnaEV4Qc+mQw/nUnxp8C67488M29joE6ie0uPPa1kfy47ldpXaW6blPK7uOvIOK+U7L4EfE29uBbSaQtqhOGluZ4liA9flZmP4A1zZHSp5rhpY7M82lTqXfu81rfJyWj6KKS6bmGVU4ZhQeKx2YuE7vTmtb5NrfySXQ+8dA8OaD4Ysjp3h2yisbV5DMY4c7WdwMtkkk5AHfGOlfnr8WdRGq/EnxDdKcol4bdD/ALNuqxfzU1+gfg3QpfCvhjSvD89ybyTTrdYWnIxvIJPAPIUZwuewFfB/xS+G+t+Bb5NR1a9tr1NXubl43h3Bwwbe29WHH3+xIzXT4YYjDxzrEOrW5pyTjFu9563b1v0inq76m/Alaiszre0q80pK0W73lrdvXySep5lZXt7pt7b6lps72t5ZzR3FvPGcPFNEwdHU9irAEV/Qn+z78XbL41fDLTfFybI9SQfY9Wt0/wCWN9CB5gA7I+RIn+ww75r+eOvq/wDZA+Nf/CovidFp+sT+X4b8UNFY6huOEgnzi2ufQbGYo5/uOSfuiv6byDMfq2I5Zv3ZaP8ARk+NHA3+sGSOvho3xFC8o95L7Ufmldf3kl1Z+7NFFFfpZ/AgUUUUAFFFFABRRRQB/9L9/KKKKACiiigAooooA/CXwZonwz8Q/tSeKNL+Ls9vb+Gn1XxA80l1dmyiE6XDmIGVXQgk5wN3NX/+Ed8F6b+1/wCGtJ+A0rajoltrOkTQSWUz3iJgq15tmJYmJE3byWKgEjOOKo+CvBHgb4h/tS+KPC/xFuTZ6JLqviCaSUXS2eJYrlzGPNbgZJPHerPxa03wt+z38ctAl+AHiOaZYoLSedor1bvZNJcMkltI8fDxyxqpaNs/e+mPzRRtS9rKK5VPf7Xp6H9+Va6qZl/Z9GtUdWeEsqbT+r6p+9Jq9pdNttL62PRP2u/+TvPDf+74c/8ASxq+z/25/wDk3PW/+v3TP/SuOvir9s6ZNG/ap8P6zqWYbOK10K6aUgkeTb3khkYY67Qpzj0r6c/bU+Jnw91v4B3elaJ4j0zUrzVbzT2tILO7iuJJFjnSV22xsxCqikljgdupAr2lUjGOOUnZ6/kz8oqYOvWq8IVKMHKKUbtJtK0oN3fSyTueUfDH/lHn4y/3dZ/9Hitb9h//AJN/+JX/AF93v/pujrO+G1vPB/wTx8WPMjIs8WsyRkjG5PtG3cPbKkfhWj+w/wD8m/8AxK/6+73/ANN0dThf4+H/AOvf6M6OImnk2eNf9B6/9Kgcn/wTV/5C/jf/AK8NJ/8AQ7iv0j+LP/JK/GX/AGL+qf8ApLJX5c/8E9vGHhLwjqfjCXxXrWn6Ml1Y6YsDahdRWwlKNOWCGRl3FcjOOmRX6NeOPHngfxb8M/HFp4V8Q6XrM9v4c1OSWOwvIbl40a2lAZljdiATwCeM135HUh/ZijfW0vzZ8d4vYDEvj+riFTlyc1H3rO3wwW+25+X37GfwC+HnxvtPFTeO4ryVtHOnram0unt9ouVnL7tv3v8AVrjPSm/DLw8vwh/bfs/BHha7uPsFrq82mBpmBkls7i0M3lSkAB9pK84HKhsZr0T/AIJ8eNvBvg+y8b/8JZrum6L9qbSjB/aF3FbeaI1uN+zzGXdt3DOOmRXDaDruk+Mf2+LXxB4YuF1HTrvxE0kFzB80cscNgyO6nugKNhuhAyOK8GjToxwuFqQS53NX72u/+AfsOaY3Na3EHEODxMpvCRw0nFO/Ipezh8N9L6y28+x0fxh/5SB6H/2GvDP/AKLhqP8A4KE20178afC1lbLvmudAghiXON0kl7OqjJ9WIFSfGH/lIHof/Ya8M/8AouGrn7eP/JwXgX/sG6f/AOnKWtcUr4fEr/p5+pw8Oycc64fkumBf/pCO9/Yb+JtyvhzxX8DfEbNDf6Gl5e6dDNw6xFmS7t8HoYZzux/00PZa4v8A4JxyGLXfHkq8lNM09hn1Elwai/ap8N6r8Afj/o/xz8Iw4sfEEkktxGOI2vfLMd5C/XAuoG3j/b3ntU3/AATpjLa94/ijBJOmWCqO5zJcYrXDTnDG0MJU3puS+TWh52e4bCYnhLN+JMDZUsbChNr+WpGo1VX32d+rbPIv2WPhb4e/aG+K/iWX4mS3d6kVtLq04inaJ7m6ubgLmSRfn2qC3CkducDB/S7wH+yR8G/ht41sPHnhO0v7fUtNEwgWW9kng/fxtExKSbiSFY45GDX58fsHeL/DPgz4reIrfxbqdto51DSmtrd72VYI2nguAzx73IUPjJAJycHHIrvfiV8QtR1P9t/wvaeF/FFxdaDPqOgRNDp+ovJYSEtiVCkUhibP8Yxz3p5ZUwtLCU61SClNyt5p33M/EDB8Q5lxLi8pwWKnRwsMPzWXN7OUVFKUbJpa3a67WP1cu7aK9tZrOfJjnjaJ8HB2uCDg9uDX5QftWfsufCf4N/ClPFngm3v4tQ/tSzs91zeyXCeVNv3fK3GflGD2r9Z6+If+CgP/ACQRP+w9p38pa+gzvD0p4Sc5xTaTs+x+KeEedY/CcSYTCYatKNOrUgpxTaUkn9pdd3955B+zJ+yl8IfiP8JfDnxC8S22oPrE89xLI8N9JFEWtLuRI8RrwAFjXI78+teOftA+HNM8YftxR+E9aV20/WNQ0CyuljcxuYZ7aBX2sOVODwR0r71/Yo/5Nu8Lf7+o/wDpbPX57/tOaDqvir9sm58MaHdjT9R1W50OztLss6CCaa1hVJN0fzrtJzleR2rwcdh6VPLaEoQV24X89HufsvCGd5hjOO83oYvEy5KUMSoNttU17WGsV0SsnZdkdx+11+zB8Lvg58PdO8V+Cft0N1catFp8sF3dG5jlimimckBxuDKYx0OME5HTHrup6nfav/wTkW81KZ7icaNbweZISzFLfUkhjBJ5OERR+FfB3xm+HPjz4YePLDwf8YNa1DULF/JuI9ShmmvkazkYLPJapcsMyxchkO05A/hYE/qD8b9A8L6L+xXq2ifD5/tmgWmhWDWEyN5nnWqTwSmYsAMllzIxwOSeB0EYVKVTEzhDkSg04+fc6OIKs8PguH8NisW8XOeKhUjWs7cnMly8zb11Wjd9HdKxg/8ABPX/AJIhqP8A2Ml7/wCiLavmX4a/8pCdT/7D+v8A/pLPXtH7BPxC8C6F8KNY0DXtf07TNQg1y4u3gvbmO2cwTQwBJF8xl3KSjAkZwRzjjPhHwW1Ow8Uft43mv+H5lvtPutX167huIuUe3NvMokB/uMSMHvketae0i8Pgop68yOWODr0s74rrVYNRdCrZtNJ3V1Z7arY5D44r4Zuf2zNXi+LEk0fhs6pZJfupkDLYfY4vLwY8yCPdjJTnBbHNfdfw7+AX7IniPW9K8X/C6e2vL3RbuDUIBYaxNcFZLdw6edBJK5C7gMhlFd78U/Av7Mvxa1+50fx/caM3iXS1S3lZNQWy1OBWUSIrFZEdl2sGUOGUZ4HNflt8WPB3hn4F/HzQLb4PeIJr9IJNOvI51uI55raeW5KNbNLCFDqyKCVYZKvhsjGSvSWCqzrVIRqQlK9/tK7/AEJynMJcW5fhsqwWJxGDxNKgo8qTVGajFK7s18S6vpok+v378e/2UfhBceH/AB78V5rbUDr7WOo6wXF9IIftccLSKfL+7t3KMr0I4r5H/Y3/AGfvhz8b9K8UXnjyG8lk0i5s4rY2t09uAs8bs24J945UYz0r9S/jr/yRbx3/ANi5qn/pNJXxD/wTZ/5AHjz/AK/tO/8ARMldeLwOH/tOlDkVmpN6b7nzfDfF2d/8Q/zLFfWp+0pTpRhLmd4xvFWi76K2lkfod4O8J6P4F8LaX4O0BZE03R7aO0tVlcyyCKMYXc55Y+5rpaKK+ojFRSjHZH88V69StUlWqu8pNtt7tvVt+oUUUUzIKKKKACvxc/bk+NX/AAn3j5fh3oU+/QvCUrpcFD8lzqpBWVvcW6kxL/tmT2r9Ef2pvjQnwX+F91qGnyqviHWd2n6Mh5Kzuvzz4/u26fP6btqn71fgUWd2LyMzuxLM7nLMzHJYk8kk8k9zXx3FGY8sVhIPV6v9F+p/Uf0eOBva1pcS4uPuxvGlfrLaUvkvdXm31iJXv/wq+M+nfD7QpNBv9Imu1kupLkz28qqx3hRgo4A428ENXgFFfmmb5NhMzw7wuMjeF092tV6WP6qzLLcPj6P1fEq8d92vyPsO0/ag0prmRb/QLmO33HynhnSSTb23qwUZ+jYrWuv2mvB0cJaz0zUp5ccI4iiXPu29v0Br4mor5Ofhjw/KSkqTXkpSt+Lv9zPnp8CZO5XUGvLmf+Z6V8Q/il4h+Ik8aXwSz063bfBYwklA3Te7HmR8cAnAHYDmsDwR4pk8FeJrTxLFax3r2gkAhkYop8xCmdwBIIzkcVylFfXUsnwVLBvL6dNKk01yrTR76769Xe/mfRU8tw1PDPBwglTaasuz389e97n0P4t/aF1XxP4cv/D0ejQWQ1CIwSTrO8jLGxG7apVRkgYznjNeZfDzxxL8PvEDa9BYx37tbSW/lyOY9okKksGAPPy46dDXC0Vx4XhjLMNhKmAoUrUp/ErvW6tve+y7nNh8hwFDDTwlKnaE91d69N73/E938efHbVPG/hybw3/ZMFhFcvG0sqTNKxWNg4UAqoGWAyeeBXhFFFdeVZPg8tovD4GHJFu9rt699W30R05fluGwNL2OFhyxvfq9fm32PUfht8VNa+HNxLFDEL7TLlt81m7lMOBjzI2wdj44PBDDr0BHpXiD9pnXbuAweGtKh05mGPPuX+0yL/uoAqA/Xd9K+ZKK83G8H5Pi8X9dxNBSqdXrZ+qvZ/NHDiuG8txOI+tV6Kc/nZ+qvZ/NH0R4V/aN8VaRGbbxJbJrkeSVmLC3uFz2JVSjD0yoI9a6m+/aicxEaX4cCydmubrco/4CiAn8xXydRXHiOAMgrVvbTwyv5OSX3Jpfgc1bhDJ6tT2sqCv5NpfcmkfROm/tK+NLe9abVLKwvLVj/qI0aBkH+zJuYn/gQNcJ8TvibefEm+s55bNLC1sI3SGFXMrFpCC7sxC5J2gAADAHvXmNFd+D4TyjCYqOMw1BRqJWTV+1tr2vbra52Ybh7LsPXWJoUlGa0TV/Ta9vna4UjKGUqwyCMEexpaK+iPZP3C/Yv+Nh+KXw2Xw5rlx5viPwosdndFz+8ubQgi2uPUkqpjc/30JP3hX2NX86PwP+K2ofBj4k6X44tN8lpE32bU7dD/x8WExHnJjuy4Eif7ajsTX9D2lapp+uaXaa1pM6XVjfwR3NtPGcpJDKoZGU+jKQa/SeHsx+s4fkm/ejo/NdGfwX428Df2DnTxeGjbD17yj2jL7Ufvd15Oy2ZoUUUV75+LhRRRQAUUUUAf/T/fyiiigAooooAKKKKAPgvxN/wT/+G/ijxDqviK98R64kurX1zfyxJ9lMaPcyNKyrugJ2gtgZJOO9dB8O/wBhT4P+AvEdl4muLjU9euNOmS4tYL+SIWqTRnKO0cMUe8qwDKGJXI5Br7UorzY5PglPnVNXPva3ihxVVwzwcsdP2bVrKy0ta10k9vM8R+NP7P8A8Pvjpp1rbeL4Z4L3T9/2LUrJxFdQCTG5MsrK8bEAlHUjIyMHmvlvRv8AgnJ8NrPUVudY8TazqFoGy1tGkFqZB/daVELY9du0+hFfolRV18rwlaftKsE2c2TeInEmVYT6hl+MlClrZaO19+W6bjrrpbXXc8y8SfCjwtrvwsvPhBYRtoug3On/ANmRJYhQ1vAMY8veGBbjksCSck5Jri/hF+zx4a+D3grX/BGiapf31r4gkllnmu/K82MywLAQnloi4CrkZB5r6Bord4Si5qo46pWXp2PHp8SZnDCVMDGs/ZzlzyW95q3vNvW+i6n53p/wTi+GCxrH/wAJR4gIQADP2Tt/2wr1n4Xfsf8Agn4VQeKYNH1rVbseK9Hk0a5Nz5GYoZAwLx7Il+cbzjdke1fW9FctLJ8HTkpwppM+hx/ihxTjaEsNisZKUJWurR1s0107pM/PAf8ABOP4YBVU+KPEB2jAz9k/+MV758Gf2V/hd8E9SfxBoEd3qWtvE0K6hqUiySQxv99YUREjj38BiF3EcbsEivpOinRynB0pqdOmk0ZZp4lcT5jhpYPGY2cqctGtFddnZK68tmfL/ij9ljwl4q+Ndn8cLvWNTh1Wzu9Pu0s4vJ+ys2nKioDujMmG2Dd82fTFTfGT9l7wn8aPG2j+ONd1fUrC60aCC3ihs/J8p1gna4BbzI3bJZsHBHHvX01RWssvw8lKLjpJ3fm+5w0eN88o1aNaniGpUYezg9Pdha3KtNrd9TzD4v8Awo8N/GjwRc+B/EzTQ280sVxDc2+0T208LZWSMuGXOMqcggqxHevO/gR+zL4V+Aeo6xqXh3VtS1J9ZhgglW+8nagt2dlK+VGhyd5znPSvpOirlg6MqyruPvLqceH4ozWhllTJqVdrDzd5Q0s3o+190tn0Pin4qfsM/C34keIbvxTp17feG7/UZWnvEshHLazTOctJ5MqnY7nltjBSecZyTn/Db9g/4d/D/wAU6V4vk1/WNTvdFu4r61jPkW8HnQsGXeqRlmXI5G4Zr7mormeUYN1Pa+zV9/6Wx70PE7imOB/s5Y2fsrcttG7Wtbma5rW032CvI/jV8H9F+N/gweCtevrvT7UXkF751l5fm74N2F/eK64O7njNeuUV3VaUakHTmrpnx+XZhiMDiYYzCS5akHeLXRrqecfCb4a6X8IvAeneANGu7m+tNNM5jnu9nnN58rzHdsVV4LkDAHFeTeJf2WPCXif422vxyu9Y1OHVbS8sLxbOPyfspfT0REU5jMmGCDd82fTFfUFFZTwdGUI0pR0ja3lbY9PC8U5rh8XXx9Gs1VrKSnLS8lN3lfS2r10seN/Gz4IeEPjp4Zh8O+KTNbSWdwtzZ31psFzbP0cIXVlKyL8rqQQeD1UEQ/Cf4KaV8LPAt38ODqt74j0G4aYR22rLDIsMNyCJoF2IgMTkltrA4LNjg4r2qin9Uo+19vy+9a1/IiPEmZrLllPtn7BS5lHSyl3TtdP0a3fd3/PbxN/wTq+F+q6k934f17VtFtXYsLPEN3HFk52xtKvmBR0G5mI9a+hPgj+zR8OvgV9pvvDi3OoaxeRCGfU791efyshvLjVFVI0LAEhRliBuJwMfQtFc9HKsJSqe1p00mezmniNxLmOC/s/G4yc6T3Ttr/iaScvm2fEvxW/YZ+HXxP8AFepeNW1vV9M1PVpzcXQUw3MDSFQuVSVNyABQAA+B2AqH4YfsIfC/4e+JLHxTqWpah4hutMnS5tILkRQWiTxnckjRxLlyjAMoZtuQCQa+4KKn+yMH7T23s1zb/P8AI3XidxSsD/Zqxs/ZcvLbS/La1ua3Na2m+xznjDw1a+MvCeseEb6WSC31qxuLCWWHHmIlzG0bMm4FdwDZGQRmvI/gN+z14b+AFlrNl4c1O/1JdamgmlN95WUNurIAnlInBDc5zXv9FdksPTlUVZr3lsz5mhnmPo4CrllKo1RqNOUdLNqzT76WXUKKKK2PJCiiigAooooA+WfjV+yr4a+Onie38S+KvEes2wsrUWtpZ2ht1t4EJ3Oyh4XYvI2CxJ6BR0Arx7/h3R8Lf+hm8Q/99Wv/AMj1+g9FedVynCVZupUgm36n3WXeJfE+Aw0MHg8ZKFOCsklGyX3f8Ofnx/w7o+Fv/QzeIf8Avq1/+R6P+HdHwt/6GbxD/wB9Wv8A8j1+g9FZ/wBiYH/n2vx/zO3/AIi7xh/0Hz/8l/8AkT8+P+HdHwt/6GbxD/31a/8AyPR/w7o+Fv8A0M3iH/vq1/8Akev0Hoo/sTA/8+1+P+Yf8Rd4w/6D5/8Akv8A8ifnx/w7o+Fv/QzeIf8Avq1/+R6P+HdHwt/6GbxD/wB9Wv8A8j1+g9FH9iYH/n2vx/zD/iLvGH/QfP8A8l/+RPz4/wCHdHwt/wChm8Q/99Wv/wAj0f8ADuj4W/8AQzeIf++rX/5Hr9B6KP7EwP8Az7X4/wCYf8Rd4w/6D5/+S/8AyJ+fH/Duj4W/9DN4h/76tf8A5Ho/4d0fC3/oZvEP/fVr/wDI9foPRR/YmB/59r8f8w/4i7xh/wBB8/8AyX/5E/Pj/h3R8Lf+hm8Q/wDfVr/8j0f8O6Phb/0M3iH/AL6tf/kev0Hoo/sTA/8APtfj/mH/ABF3jD/oPn/5L/8AIn58f8O6Phb/ANDN4h/76tf/AJHo/wCHdHwt/wChm8Q/99Wv/wAj1+g9FH9iYH/n2vx/zD/iLvGH/QfP/wAl/wDkT8+P+HdHwt/6GbxD/wB9Wv8A8j0f8O6Phb/0M3iH/vq1/wDkev0Hoo/sTA/8+1+P+Yf8Rd4w/wCg+f8A5L/8ifnx/wAO6Phb/wBDN4h/76tf/kej/h3R8Lf+hm8Q/wDfVr/8j1+g9FH9iYH/AJ9r8f8AMP8AiLvGH/QfP/yX/wCRPz4/4d0/C3/oZvEP/fVr/wDI9fXXwj+Gdt8IvBVt4F0/Vr7V7Cxkka0fUDGZYYpDu8lTGiAorFiuRkA4zgAD02it8PluGoS56MLM8bPOPM+znDrC5niXUgnezS3V9dk+rCiiiu4+RCiiigAooooA/9T9/KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP/1f38ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//W/fyiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD/9k=';
		var accountName = component.get('v.accountName');
		var sapCompanyCode = component.get('v.sapCompanyCode') != null ? component.get('v.sapCompanyCode') : '';

		var that = this;
		var header = function (data) {
			pdf.addImage(logo, 'PNG', data.settings.margin.left - 13, 20, 175, 80);

			pdf.setFontType('bold');
			pdf.text(230, 65, 'Account: ');
			pdf.setFontType('normal');
			pdf.setTextColor(80);
			pdf.text(300, 65, accountName);
			pdf.setFontType('bold');
			pdf.setTextColor(0);
			pdf.text(650, 65, 'SAP CompanyCode: ');
			pdf.setFontType('normal');
			pdf.setTextColor(80);
			pdf.text(820, 65, sapCompanyCode);
			pdf.setFontType('bold');
			pdf.setTextColor(0);
			pdf.text(1080, 65, 'Selected Months: ');
			pdf.setFontType('normal');
			pdf.setTextColor(80);
			pdf.text(1220, 65, that.getSelectedMonthNames(component));
		};
		return header;
	},

	createFooter: function(pdf, totalPagesExp)
	{
		var today = new Date();
		var month = today.getMonth() + 1;
		var formattedDate = '' + today.getDate() + '.' + (month < 10 ? '0' + month : month) + '.' + today.getFullYear();
		var footer = function (data) {
			var str = "Page " + data.pageCount;
			// Total page number plugin only available in jspdf v1.0+
			if (typeof pdf.putTotalPages === 'function') {
				str = str + " of " + totalPagesExp;
			}
			//pdf.text(str, data.settings.margin.left, pdf.internal.pageSize.height - 30);
			pdf.setFontType('bold');
			pdf.text(pdf.internal.pageSize.width - 310, pdf.internal.pageSize.height - 30, 'Document created on: ');
			pdf.setFontType('normal');
			pdf.setTextColor(80);
			pdf.text(pdf.internal.pageSize.width - 125, pdf.internal.pageSize.height - 30, formattedDate);
		};
		return footer;
	},

	getDescriptionColumnStyles: function(greyedColumns)
	{
		var columnStyles = {};
		for ( var k = 0; k < greyedColumns; k++ )
		{
			columnStyles[k] = { columnWidth: 'auto', halign: 'left' };
		}
		return columnStyles;
	},

	getMetricColumnWidth: function(reportType)
	{
		var columnWidth = 100;
		if ( reportType == 'articleMix' || reportType == 'articleMixBySalesReps' || reportType == 'articleMixBySalesReps2')
		{
			columnWidth = 110;
		}
		return columnWidth;
	},

	getSelectedMonthNames: function(component)
	{
		var allMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		var selectedMonths = component.get('v.selectedMonths');
		var selectedMonthsNames = [];
		selectedMonths.forEach(function(month)
		{
			selectedMonthsNames.push(allMonths[month - 1]);
		});
		return selectedMonthsNames.join(', ');
	},

	showHideMaterialMarginColumns: function(show)
	{
		var that = this;
		var columns = document.querySelectorAll('.mmColumn');
		if ( columns )
		{
			for ( var i = 0; i < columns.length; i++ )
			{
				if ( show )
				{
					that.showColumn(columns[i]);
				}
				else
				{
					that.hideColumn(columns[i]);
				}
			}
		}
	},

	hideColumn: function(column)
	{
		if ( column )
		{
			column.style.display = 'none';
		}
	},

	showColumn: function(column)
	{
		if ( column )
		{
			column.style.display = 'table-cell';
		}
	}
})