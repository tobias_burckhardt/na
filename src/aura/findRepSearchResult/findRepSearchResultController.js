({
	sendResultSelected: function(component) {
		var evt = component.getEvent('selectResult');
		var result = component.get('v.result');
		evt.setParams({
			'result': result
		});
		evt.fire();
	}
})