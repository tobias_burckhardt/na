({
    fireDateChanged : function(cmp, newDateVal) {
        var evt = cmp.getEvent('dateChanged');
        evt.setParams({
            dateVal: newDateVal,
            label: cmp.get('v.label')
        });
        evt.fire();
    },
    populateCalendarRows : function(cmp, dateInNewMonth) {
        var calendarArr = [];

        var daysToMilliseconds = function(numDays) {
            return numDays*24*60*60*1000;
        };
        var firstDay = new Date(dateInNewMonth - daysToMilliseconds(dateInNewMonth.getDate()-1));
        // subtract day of week to get beginning of week
        var currentDay = new Date(firstDay - daysToMilliseconds(firstDay.getDay()));

        var thisMonth = dateInNewMonth.getMonth();
        while (currentDay.getMonth() <= thisMonth) {
            var weekArr = [];
            for (var j=1; j<=7; j+=1) {
                weekArr.push({
                    dayOfMonth : currentDay.getDate(),
                    currentMonth : currentDay.getMonth() === thisMonth
                });

                currentDay.setDate(currentDay.getDate() + 1);
            }
            calendarArr.push(weekArr);
        }
        cmp.set('v.weeksInMonth', calendarArr);
    },
    setDisplayedMonth : function(cmp, dateInNewMonth) {
        cmp.set('v.dateInDisplayedMonth', dateInNewMonth);
        var stringifiedDateArr = dateInNewMonth.toString().split(' ');
        cmp.set('v.displayedMonth', stringifiedDateArr[1]);
        cmp.set('v.displayedYear', stringifiedDateArr[3]);
        this.populateCalendarRows(cmp, dateInNewMonth);
    },
    setRelativeDisplayedMonth : function(cmp, increment) {
        var dateInDisplayedMonth = cmp.get('v.dateInDisplayedMonth');
        var monthVal = dateInDisplayedMonth.getMonth();
        var JANUARY = 0, DECEMBER = 11;
        if (monthVal === JANUARY && !increment) {
            dateInDisplayedMonth.setYear(dateInDisplayedMonth.getFullYear() - 1);
            dateInDisplayedMonth.setMonth(DECEMBER, 1);
        } else if (monthVal === DECEMBER && increment) {
            dateInDisplayedMonth.setYear(dateInDisplayedMonth.getFullYear() + 1);
            dateInDisplayedMonth.setMonth(JANUARY, 1);
        } else {
            dateInDisplayedMonth.setMonth(dateInDisplayedMonth.getMonth() + (increment? 1 : -1), 1);
        }
        this.setDisplayedMonth(cmp, dateInDisplayedMonth);
    },
    initializeSelector : function(cmp) {
        var initialCalendarDate = cmp.get('v.value');
        if ($A.util.isEmpty(initialCalendarDate)) {
            initialCalendarDate = new Date();
        } else if (typeof initialCalendarDate == 'string') {
            initialCalendarDate = new Date(initialCalendarDate);
        }
        // provide a day of the month that all months have
        initialCalendarDate.setMonth(initialCalendarDate.getMonth(), 1);
        this.setDisplayedMonth(cmp, initialCalendarDate);
    }
})