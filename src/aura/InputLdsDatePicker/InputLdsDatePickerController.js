({
    calDaySelected : function(cmp, e, helper) {
        var newValue = cmp.get('v.dateInDisplayedMonth');
        window.g = newValue;
        newValue.setHours(1);
        newValue.setMonth(newValue.getMonth(),
            e.srcElement.getAttribute('data-day-of-month'));
        var newValueIsoDate = newValue.toISOString().slice(0, 10);
        cmp.set('v.value', newValueIsoDate);
        helper.fireDateChanged(cmp, newValueIsoDate);
        cmp.set('v.hasMouseover', false);
    },
    calMonthDecr : function(cmp, e, helper) {
        helper.setRelativeDisplayedMonth(cmp, false);
    },
    calMonthIncr : function(cmp, e, helper) {
        helper.setRelativeDisplayedMonth(cmp, true);
    },
    calYearChanged : function(cmp, e, helper) {
        var dateInDisplayedMonth = cmp.get('v.dateInDisplayedMonth');
        dateInDisplayedMonth.setYear(e.getSource().get('v.value'));
        helper.setDisplayedMonth(cmp, dateInDisplayedMonth);
    },
    mouseover : function(cmp) {
        cmp.set('v.hasMouseover', true);
    },
    mouseleave : function(cmp) {
        cmp.set('v.hasMouseover', false);
    },
    searchBoxBlur : function(cmp) {
        cmp.set('v.hasFocus', false);
    },
    searchBoxFocus : function(cmp, e, helper) {
        if ($A.util.isEmpty(cmp.get('v.dateInDisplayedMonth'))) {
            helper.initializeSelector(cmp);
        }
        cmp.set('v.hasFocus', true);
    }
})