({
    displayContents: function(cmp) {
        cmp.set('v.contentsDisplayed', true);
        window.setTimeout($A.getCallback(function() {
            cmp.set('v.contentsDisplayed', false);
        }), 5000);
    }
})