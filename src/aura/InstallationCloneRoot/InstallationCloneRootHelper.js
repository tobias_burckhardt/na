({

    fetchInstallationInfo : function(cmp, installationId, callback) {
        this.serverSideRead(
            cmp,
            'c.getInstallationCloneInformation',
            {
                installationId : installationId
            },
            callback
       );
    },
    completeClone : function(cmp, installation, assignments, assets, callback) {
        var install = this.cleanupObject(installation);
        delete install.Account;
        console.log(install);
        this.serverSideMayWrite(
            cmp,
            'c.cloneInstallation',
            {
                installation : install,
                assignments : assignments.map(this.cleanupObject),
                assets : assets.map(this.cleanupObject)
            },
            callback
       );
    },
    fetchAvailableRecords : function(cmp, callback) {
        this.serverSideRead(
            cmp,
            'c.getLookupSelections',
            {},
            callback
        );
    },
    accountsLookup : function(cmp, searchTerm, callback) {
        this.serverSideRead(
            cmp,
            'c.getMatchingAccounts',
            { nameFilter : searchTerm },
            callback
        );
    },
    productsLookup : function(cmp, searchTerm, callback) {
        this.serverSideRead(
            cmp,
            'c.getMatchingProducts',
            { nameFilter : searchTerm },
            callback
        );
    },
    genericSave : function(cmp, successFunction) {
        var selectedAssignments = cmp.get('v.selectedAssignments');
        var relatedAssets = selectedAssignments.map(function(x) {
            return x.Sika_Asset__r;
        });
        this.completeClone(cmp, cmp.get('v.installationRecord'), selectedAssignments, relatedAssets,
            function(clonedInstallationId, error) {
                if (error !== null) {
                    cmp.set('v.errorMessage', error);
                    cmp.set('v.saving', false);
                } else {
                    successFunction(clonedInstallationId);
                }
            });
    },
    searchCallbackFactory : function(cmp, fnName) {
        var t = this;
        return function(searchTerm, callback) {
            t[fnName](cmp, searchTerm, function(records, error) {
                if (error !== null) {
                    cmp.set('v.errorMessage', error);
                } else {
                    callback(records);
                }
            });
        };
    },
    buildSearchCallbacks : function(cmp) {
        var searchCallbacks = {};
        searchCallbacks.accounts = this.searchCallbackFactory(cmp, 'accountsLookup');
        searchCallbacks.products = this.searchCallbackFactory(cmp, 'productsLookup');
        cmp.set('v.searchCallbacks', searchCallbacks);
    },
    cleanupObject : function(oldObj, skipKeys) {
        var newObj = {};
        Object.keys(oldObj).forEach(function(key) {
            newObj[key] = oldObj[key];
        });
        return newObj;
    }
})