({
    init: function(cmp, event, helper) {
        helper.fetchInstallationInfo(cmp, cmp.get('v.recordId'),
            function(installationInfo, error) {
                if(error !== null) {
                    cmp.set('v.errorMessage', error);
                } else {
                    installationInfo.sobjects.assigns.forEach(function(assign){
                        assign.Sika_Asset__r.ID_Serial__c = '';
                    });
                    installationInfo.sobjects.install[0].sobjectType = 'Sika_Asset_Installation__c';
                    var install = helper.cleanupObject(installationInfo.sobjects.install[0]);
                    install.Account = { Name: install.Account_Name__c };
                    cmp.set('v.installationRecord', install);
                    cmp.set('v.assignments', installationInfo.sobjects.assigns);
                    cmp.set('v.describe', installationInfo.describe);
                    cmp.set('v.installationInfoFetched', true);
                }
            });
        helper.buildSearchCallbacks(cmp);
    },
    saveAndGoToNew: function(cmp, e, helper) {
        helper.genericSave(cmp, function(clonedInstallationId) {
            window.location.href = '/' + clonedInstallationId;
        });
    },
    saveAndStayOnPage: function(cmp, e, helper) {
        helper.genericSave(cmp, function() {
            cmp.set('v.saving', false);
        });
    },
    cancel: function(cmp) {
        window.location.href = '/' + cmp.get('v.recordId');
    },
    fieldUpdate : function(cmp, e) {
        var update = e.getParams();
        console.log(update);
        if (update.sobjectType === 'Sika_Asset__c' || update.sobjectType === 'Sika_Assignment__c') {
            var assignments = cmp.get('v.assignments');
            var assetUpdate = update.sobjectType === 'Sika_Asset__c';
            for (var i=0; i<assignments.length; i+=1) {
                var record = assetUpdate? assignments[i].Sika_Asset__r : assignments[i];
                if (record.Id === update.recordId) {
                    record[update.fieldName] = update.fieldVal;
                    break;
                }
            }
            cmp.set('v.assignments', assignments);
        } else if (update.sobjectType === 'Sika_Asset_Installation__c') {
            var installationRecord = cmp.get('v.installationRecord');
            installationRecord[update.fieldName] = update.fieldVal;
            cmp.set('v.installationRecord', installationRecord);
        }
    },
    updateSelectedAssignments: function(cmp, e) {
        var params = e.getParams();
        cmp.set('v.selectedAssignmentIds', params.containedIdents);
        cmp.set('v.selectedAssignments', params.containedObjects);
    }
})