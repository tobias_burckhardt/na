({
    updateFieldValue : function(cmp, isAsset, fieldName, formInput) {
        var attr = isAsset? 'v.asset' : 'v.assignment';
        var record = cmp.get(attr);
        record[fieldName] = formInput;
        cmp.set(attr, record);
        var inputChanged = cmp.getEvent('inputUsed');
        inputChanged.setParams({
            recordId : record.Id,
            sobjectType : isAsset? 'Sika_Asset__c' : 'Sika_Assignment__c',
            fieldName : fieldName,
            fieldVal : formInput
        });
        inputChanged.fire();
    },
    updateAssetValue : function(cmp, fieldName, formInput) {
        this.updateFieldValue(cmp, true, fieldName, formInput);
    },
    updateAssignmentValue : function(cmp, fieldName, formInput) {
        this.updateFieldValue(cmp, false, fieldName, formInput);
    },
    getLocalId : function(e) {
        if ('srcElement' in e) {
            return e.srcElement.getAttribute('data-local-id');
        }
        return e.getSource().getLocalId();
    },
    getInputValue : function(e) {
        if ('srcElement' in e) {
            return e.srcElement.value;
        }
        return e.getSource().get('v.value');
    }
})