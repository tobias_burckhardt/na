({
    assetInput : function(cmp, e, helper) {
        helper.updateAssetValue(cmp,
            helper.getLocalId(e),
            helper.getInputValue(e));
    },
    assignmentInput : function(cmp, e, helper) {
        helper.updateAssignmentValue(cmp,
            helper.getLocalId(e),
            helper.getInputValue(e));
    },
    changeAssociatedProduct : function(cmp, e, helper) {
        var val = e.getParams().selectedRecord;
        if (val !== null) {
            val = val.Id;
        }
        e.stopPropagation();
        helper.updateAssignmentValue(cmp,
            'Product__c',
            val);
    },
    changePurchaseDate : function(cmp, e, helper) {
        helper.updateAssetValue(cmp,
            'Purchase_Date__c',
            e.getParams().dateVal);
    }
})