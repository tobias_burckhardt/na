({
    init : function(cmp) {
        var assetDescriptions = cmp.get('v.describe').objects.Sika_Asset__c.fields;
        cmp.set('v.headers', [
            'Include',
            assetDescriptions.Asset_Type__c.label,
            assetDescriptions.Asset_Size__c.label,
            assetDescriptions.Manufacturer__c.label,
            assetDescriptions.Status__c.label,
            assetDescriptions.Asset_Description__c.label,
            assetDescriptions.ID_Serial__c.label,
            assetDescriptions.Purchase_Date__c.label,
            'Product'
        ]);
    }
})