({
    init : function(cmp, e, helper) {
        helper.assertValidMappingFunc(cmp);
        helper.assertValidDerefFields(cmp);
    },
    mappingFuncProvided : function(cmp, e, helper) {
        helper.assertValidMappingFunc(cmp);
    },
    derefFieldsProvided : function(cmp, e, helper) {
        helper.assertValidDerefFields(cmp);
    }
})