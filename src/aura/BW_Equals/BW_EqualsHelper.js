({
    ERROR_PREFIX: function() {
        return 'Please contact an admin about this error: Bw equals extender error:';
    },
    errorOut : function(msg) {
        throw new Error(this.ERROR_PREFIX() + msg);
    },
    errorDerefsAndMappingFunc : function() {
        this.errorOut('please provide either a deref fields or a mapping func, not both');
    },
    errorDerefsNotList : function() {
        this.errorOut('an identityDerefFields attribute was provided, but it was not a list. This is an invalid value.');
    },
    errorDerefsNotStringList : function() {
        this.errorOut('an identityDerefFields attribute was provided, and it was a list, but not all of its elements were strings. This is an invalid value.');
    },
    errorMappingFuncNotFunc : function() {
        this.errorOut('an identityMappingFunc attribute was provided, but it was not a function. This is an invalid value.');
    },
    errorCantDeref : function(fieldsListStr, fieldName, objectStr) {
        this.errorOut('was asked to dereference, for identification, fields ' + fieldsListStr + ' but field ' + fieldName + ' is not undefined on object ' + objectStr);
    },
    assertValidDerefFields : function(cmp) {
        var derefFields = cmp.get('v.identityDerefFields');
        if (derefFields !== null) {
            //checked here too because they might change deref fields later
            if (cmp.get('v.identityMappingFunc') !== null) {
                this.errorDerefsAndMappingFunc();
            }
            if (typeof derefFields === 'string') {
                try {
                    derefFields = JSON.parse(derefFields);
                    cmp.set('v.identityDerefFields', derefFields);
                } catch (e) {
                    this.errorDerefsNotList();
                }
            }
            if (!('length' in derefFields)) {
                this.errorDerefsNotList();
            } else {
                for (var i=0; i<derefFields.length; i+=1) {
                    if ( typeof derefFields[i] !== 'string') {
                        this.errorDerefsNotStringList();
                    }
                }
            }
        }
    },
    assertValidMappingFunc : function(cmp) {
        var mappingFunc = cmp.get('v.identityMappingFunc');
        if (mappingFunc !== null) {
            if (cmp.get('v.identityDerefFields') !== null) {
                this.errorDerefsAndMappingFunc();
            }
            if (typeof mappingFunc !== 'function') {
                this.errorMappingFuncNotFunc();
            }
        }
    },
    isEqual : function(cmp, a, b) {
        return this.mapForEquality(cmp, a) === this.mapForEquality(cmp, b);
    },
    mapForEquality: function(cmp, a) {
        var mappingFunc = cmp.get('v.identityMappingFunc');
        if (mappingFunc !== null) {
            return mappingFunc(a);
        }
        var currentVal = a;
        var derefFields = cmp.get('v.identityDerefFields');
        if (derefFields !== null) {
            var t = this;
            derefFields.forEach(function(fieldName) {
                if (!(currentVal.hasOwnProperty(fieldName))) {
                    t.errorCantDeref(JSON.stringify(derefFields), fieldName, JSON.stringify(a));
                }
                currentVal = currentVal[fieldName];
            });
        }
        return currentVal.Id;
    }
})