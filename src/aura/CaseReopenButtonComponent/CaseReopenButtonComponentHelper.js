({
	updateRecordStatus : function(cmp) {
		var action = cmp.get("c.updateCase");
		var toastEvent = $A.get("e.force:showToast");
		
		action.setParams({
			caseId : cmp.get("v.recordId")
		});

		toastEvent.setParams({
	        "title": "Success!",
	        "message": "The record has been updated successfully.",
	        "type": "success"
    	});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state === "SUCCESS") {
				$A.get('e.force:refreshView').fire();
				toastEvent.fire();
			}
		});

		$A.enqueueAction(action);
	}
})