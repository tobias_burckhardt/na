({
	goToRegionalManager: function(component) {
		var evt = $A.get('e.force:navigateToSObject');
		evt.setParams({
			'recordId': component.get('v.result.regionalManagerUserId')
		});
		evt.fire();
	}
})