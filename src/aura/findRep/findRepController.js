({
	initialize: function(component) {
		component.set('v.searchResults', []);
	},

	handleSearch: function(component, event) {
		var search = component.get('c.search');
		search.setParams(event.getParams());
		search.setCallback(this, function(response) {
			if (component.isValid() && response.getState() === 'SUCCESS') {
				component.set('v.searchResults', response.getReturnValue());
				if (response.getReturnValue().length === 0) {
					FindRepUtils.showToast('info', 'Your search returned no results');
				}
			}
		});
		$A.enqueueAction(search);
	},

	clearResults: function(component) {
		component.set('v.searchResults', []);
		component.set('v.selectedSearchResult', null);
	},

	selectResult: function(component, event) {
		component.set('v.selectedSearchResult', event.getParam('result'));
		FindRepUtils.rescroll();
	},

	showSpinner: function(component) {
		component.set('v.spinnerVisible', true);
	},

	hideSpinner: function(component) {
		component.set('v.spinnerVisible', false);
	}
})