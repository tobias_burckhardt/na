({
    paginateTo : function(cmp, pageNum) {
        var pageSize = this.PAGE_SIZE();
        var startIndex = pageSize * (pageNum -1);
        var endIndex = pageSize * pageNum;
        var currentPage = cmp.get('v.searchResults').slice(startIndex, endIndex);
        cmp.set('v.currentPage', currentPage);
        cmp.set('v.currentPageNumber', pageNum);
    },
    PAGE_SIZE : function() {
        return 30;
    }
})