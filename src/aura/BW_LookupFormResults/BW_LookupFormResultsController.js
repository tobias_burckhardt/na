({
    paginateFirst : function(cmp, e, helper) {
        helper.paginateTo(cmp, 1);
    },
    paginateBack : function(cmp, e, helper) {
        helper.paginateTo(cmp, cmp.get('v.currentPageNumber')-1);
    },
    paginateForward : function(cmp, e, helper) {
        helper.paginateTo(cmp, cmp.get('v.currentPageNumber')+1);
    },
    selectRecord : function(cmp, e) {
        var selectedId = e.srcElement.getAttribute('data-record-id');
        var record = cmp.get('v.currentPage').filter(function(r){
            return r.Id === selectedId;
        })[0];
        cmp.get('v.onSelect')(record);
    },
    searchResultsChanged : function(cmp, e, helper) {
        helper.paginateTo(cmp, 1);
    }
})