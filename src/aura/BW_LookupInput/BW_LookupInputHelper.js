({
    recordChanged : function(cmp) {
        var selectedRecord = cmp.get('v.selectedRecord');
        if (selectedRecord === null) {
            cmp.set('v.displayedName', '--None--');
        } else {
            cmp.set('v.displayedName',
               selectedRecord[cmp.get('v.nameField')]);
        }
        var evt = cmp.getEvent('lookupChanged');
        evt.setParams({
            selectedRecord : selectedRecord,
            lookupLabel : cmp.get('v.label')
        });
        evt.fire();
    }
})