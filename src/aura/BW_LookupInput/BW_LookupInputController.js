({
    init : function(cmp) {
        var selectedRecord = cmp.get('v.selectedRecord');
        if (!($A.util.isEmpty(selectedRecord))) {
            cmp.set('v.displayedName',
               selectedRecord[cmp.get('v.nameField')]);
        }
    },
    inputClicked : function(cmp, e, helper) {
        var evt = cmp.getEvent('displayModal');
        evt.setParams({
            objectName : cmp.get('v.objectName'),
            displayedColumns : cmp.get('v.displayedColumns'),
            label : cmp.get('v.label'),
            callback : function(record) {
                cmp.set('v.selectedRecord', record);
                helper.recordChanged(cmp);
            }
        });
        evt.fire();
    },
    deleteSelected : function(cmp, e, helper) {
        cmp.set('v.selectedRecord', null);
        helper.recordChanged(cmp);
    }
})