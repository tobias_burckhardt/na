<aura:documentation>
    <!-- view this documentation @ https://{{mydomain}}.lightning.force.com/auradocs/reference.app#reference?descriptor=c:BW_Set&defType=component -->
    <aura:description>
        <h4>BW_Set</h4>

        <p>Have you ever been in this situation? A client wants a user of a component to be able to select multiple things in that component? Maybe they want a checkbox next to each row of a table. Maybe they have a grid of multiple elements and want the user to select multiple (whether through checkboxes or just clicking its entire element).</p>
        
        <p> Of course you have! This is a very frequent UX request! <code>BW_Set</code> was created to save you time having to solve the same problem again and again, and to save the client from having a codebase which solves the same problem ten different (inconsistent, error-prone) ways.</p>

        <p><code>BW_Set</code> has five core bundles that must be deployed with it, and optional documentation bundles that can be deployed with it.</p>

        <p> As a developer using BW_Set, in 95% of cases you will only need to know about these three core bundles:</p>

        <ul>
            <li><code>BW_Set</code> - a component bundle which manages the abstract logic of storing a set and notifying parents of changes to it</li>
            <li><code>BW_SetChangedEvent</code> - an event thrown by <code>BW_Set</code> to notify parents of changes to the set, including its new values</li>
            <li><code>BW_SetStatus</code> - a component bundle which indicates common use cases for a set, including indicating / toggling whether a provided element is isent in it</li>
        </ul>

        <p>Sometimes the user wants to go with an approach that is more "add" button and "remove" button than checkbox. In which case your component would probably be most elegant if you know about this fourth component</p>

        <ul>
            <li><code>BW_SetModifyEvent</code> - an event that can be fired from a component within BW_Set's body attribute to modify the set</li>
        </ul>

        <p>
            Finally, if your records are identified by a field other than Id, (e.g. they are wrappers of two or more objects, and the identifier field will be like myObj.Account.Id) or if you are dealing with objects that will never have the same contents as each other (strings, planned junction records), <code>BW_Set</code> is just as usable to you!</p>

        <p>Take a look at the attributes on the documentation page for the below component, and also review the example provided in this component's documentation. This component resource will also be useful to you if you're building your own reusable asset.
        </p>

        <ul>
            <li><code>BW_Equals</code> - allows customization of the dereferenced fields or function to determine the identity of a passed in object. Used by <code>BW_Set</code> and <code>BW_SetStatus</code></li>
        </ul>
    </aura:description>
    <aura:example name="ExampleBasic" ref="c:BW_SetExampleBasic" label="Simple example">
        <p>Several account objects are represented in the boxes below. Accounts which are contained in the set ("selected") are highlighted. Click them to toggle their set status, which will remove them from the set (will "unselect" them).</p>

        <p>You can click accounts that are not contained in the set ("unselected" accounts) to add them to the set</p>
    </aura:example>
    <aura:example name="ExampleTable" ref="c:BW_SetExampleTable" label="Non-toggleable indicators">
        <p>You can indicate whether an element is selected without automatically wrapping the child components in a click-to-toggle behavior.</p>

        <p>This makes it easy to support designs where, for easy visibility, an area larger than the toggle area itself should be highlighted when selected</p>

        <p>By default <code>BW_SetStatus</code> adds a class of <code>bw_selected</code> and <code>bw_unselected</code> to the top-level child components.</p>
    </aura:example>
</aura:documentation>