({
    fireSetChangedEvent : function(cmp, values) {
        var changedEvent = cmp.getEvent('setChanged');
        changedEvent.setParams({
            containedObjects: Object.keys(values).map(function(ident) {
                return values[ident];
            }),
            containedIdents: Object.keys(values)
        });
        changedEvent.fire();
    },
    asObject : function(cmp, valueArr) {
        var results = {};
        var t = this;
        valueArr.forEach(function(value) {
            results[t.mapForEquality(cmp, value)] = value;
        });
        return results;
    },
    addValues : function(currentValuesObject, newValuesObject, newValuesIdents) {
        var toAdd = newValuesIdents.filter(function(ident) {
            return !(ident in currentValuesObject);
        });
        var changed = false;
        toAdd.forEach(function(ident){
            if (!(ident in currentValuesObject)) {
                changed = true;
                currentValuesObject[ident] = newValuesObject[ident];
            }
        });
        return { hasChanged : changed, results : currentValuesObject };
    },
    removeValues : function(currentValuesObject, newValuesObject, newValuesIdents) {
        var resultingValuesObject = {};
        var changed = false;
        Object.keys(currentValuesObject).forEach(function(ident) {
            if (newValuesIdents.indexOf(ident) === -1) {
                resultingValuesObject[ident] = currentValuesObject[ident];
            } else {
                changed = true;
            }
        });
        return { hasChanged : changed, results : resultingValuesObject };
    },
    toggleValues : function(currentValuesObject, newValuesObject, newValuesIdents) {
        var resultingValuesObject = {};
        Object.keys(currentValuesObject).forEach(function(ident) {
            if (newValuesIdents.indexOf(ident) === -1) {
                resultingValuesObject[ident] = currentValuesObject[ident];
            }
        });
        newValuesIdents.forEach(function(ident) {
            if (!(ident in currentValuesObject)) {
                resultingValuesObject[ident] = newValuesObject[ident];
            }
        });
        return { hasChanged : newValuesIdents.length > 0, results : resultingValuesObject };
    },
    modifySet : function(cmp, params) {
        var operation = params.action;
        var newValuesObject = this.asObject(cmp, params.values);
        var currentValuesObject = cmp.get('v.values');
        var answer;
        if (operation === 'replace') {
            answer = {
                hasChanged : Object.keys(currentValuesObject) !== Object.keys(newValuesObject),
                results : newValuesObject
            };
        } else {
            var newValuesIdents = Object.keys(newValuesObject);
            if (operation === 'add') {
                answer = this.addValues(currentValuesObject, newValuesObject, newValuesIdents) ;
            } else if (operation === 'remove') {
                answer = currentValuesObject = this.removeValues(currentValuesObject, newValuesObject, newValuesIdents);
            } else if (operation === 'toggle') {
                answer = this.toggleValues(currentValuesObject, newValuesObject, newValuesIdents);
            }
        }
        if (answer.hasChanged) {
            cmp.set('v.values', answer.results);
            this.fireSetChangedEvent(cmp, answer.results);
        }
    }
})