({
    init: function(cmp, e, helper) {
        cmp.set('v.values', {});
        if (!$A.util.isEmpty(cmp.get('v.initialValues'))) {
            helper.modifySet(
                cmp,
                {
                    action: 'replace',
                    values: cmp.get('v.initialValues')
                }
            );
        }
        helper.fireSetChangedEvent(cmp, cmp.get('v.values'));
    },
    handleModifySet : function(cmp, e, helper) {
        helper.modifySet(cmp, e.getParams());
    },
    handleModifySetMethod : function(cmp, e, helper) {
        helper.modifySet(cmp, e.getParams().arguments);
    }
})