({
	initialize: function(component) {
		FindRepUtils.initializeAttributeFromMethod(component, 'v.crossSellTeamOptions', 'c.getCrossSellTeamOptions', function() {
			component.find('crossSellTeamInput').set('v.value', null);
		});
	},

	handleCreateReferral: function(component, event, helper) {
		var subject = component.find('subjectInput').get('v.value');
		var comments = component.find('commentsInput').get('v.value');

		if ($A.util.isEmpty(subject) && $A.util.isEmpty(comments)) {
			FindRepUtils.showToast('error', 'Subject and Comments are required');
		} else if ($A.util.isEmpty(subject)) {
			FindRepUtils.showToast('error', 'Subject is required');
		} else if ($A.util.isEmpty(comments)) {
			FindRepUtils.showToast('error', 'Comments are required');
		} else {
			helper.create(component);
		}
	}
})