({
	setFormDisabled: function(component, disabled) {
		component.find('createButton').set('v.disabled', disabled);
		component.find('subjectInput').set('v.disabled', disabled);
		component.find('commentsInput').set('v.disabled', disabled);
		component.find('crossSellTeamInput').set('v.disabled', disabled);
	},

	clearForm: function(component) {
		component.find('subjectInput').set('v.value', '');
		component.find('commentsInput').set('v.value', '');
		component.find('crossSellTeamInput').set('v.value', null);
	},

	getReferralCreateAction: function(component) {
		var action = component.get('c.createReferral');
		action.setParams({
			'targetMarket': component.get('v.result').targetMarket,
			'assignedTo': component.get('v.result').repUserId,
			'subject': component.find('subjectInput').get('v.value'),
			'comments': component.find('commentsInput').get('v.value'),
			'crossSellTeam': component.find('crossSellTeamInput').get('v.value')
		});
		return action;
	},

	navigateToRepInfo: function(component) {
		var navEvent = component.getEvent('selectTab');
		navEvent.setParams({
			'page': 'repInfo'
		});
		navEvent.fire();
	},

	create: function(component) {
		this.setFormDisabled(component, true);
		var action = this.getReferralCreateAction(component);
		action.setCallback(this, function(response) {
			if (response.getState() === 'SUCCESS' && component.isValid()) {
				this.setFormDisabled(component, false);
				if (response.getReturnValue()) {
					FindRepUtils.showToast('success', 'Referral created');
					this.navigateToRepInfo(component);
					this.clearForm(component);
				} else {
					FindRepUtils.showToast('error', 'There was a problem creating the referral');
				}
			}
		});
		$A.enqueueAction(action);
	}
})