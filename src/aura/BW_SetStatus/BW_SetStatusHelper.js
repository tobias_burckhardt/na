({
    updateIsSelected : function(cmp, isInit) {
        var selectedIdents = cmp.get('v.selectedIdents');
        var elementIdent = this.mapForEquality(cmp, cmp.get('v.element'));
        var isSelected = selectedIdents.indexOf(elementIdent) !== -1;
        var dualMode = this.isDualMode(cmp);
        if (isInit || isSelected !== cmp.get('v.isSelected')) {
            cmp.set('v.isSelected', isSelected);
            this.updateIndicators(cmp, dualMode, isSelected);
        }
        if (dualMode && isInit) {
            var selectedClass = this.SELECTED();
            var unselectedClass = this.UNSELECTED();
            cmp.get('v.body').forEach(function(bodyCmp) {
                $A.util.addClass(bodyCmp, selectedClass);
            });
            cmp.get('v.unselectedContents').forEach(function(unselectedCmp) {
                $A.util.addClass(unselectedCmp, unselectedClass);
            });
        }
    },
    isDualMode : function(cmp) {
        return !($A.util.isEmpty(cmp.get('v.unselectedContents')));
    },
    updateIndicators: function(cmp, dualMode, isSelected) {
        if (dualMode) {
            cmp.set('v.displayBody', isSelected);
        } else {
            this.updateBodyClass(cmp, isSelected);
        }
    },
    fireToggleSelected: function(cmp) {
        var modifySetEvent = cmp.getEvent('modifySet');
        modifySetEvent.setParams({
            action: 'toggle',
            values: [cmp.get('v.element')]
        });
        modifySetEvent.fire();
    },
    updateBodyClass: function(cmp, isSelected) {
        var oldClass = (isSelected)? this.UNSELECTED() : this.SELECTED();
        var newClass = (isSelected)? this.SELECTED() : this.UNSELECTED();
        cmp.get('v.body').forEach(function(bodyCmp) {
            $A.util.removeClass(bodyCmp, oldClass);
            $A.util.addClass(bodyCmp, newClass);
        });
    },
    SELECTED : function() {
        return 'bw_selected';
    },
    UNSELECTED: function() {
        return 'bw_unselected';
    }
})