({
    init: function(cmp, e, helper) {
        helper.updateIsSelected(cmp, true);
    },
    selectedIdentsChanged: function(cmp, e, helper) {
        helper.updateIsSelected(cmp, false);
    },
    toggleSelected: function(cmp, e, helper) {
        e.stopPropagation();
        var isSelected = !(cmp.get('v.isSelected'));
        helper.fireToggleSelected(cmp);
        cmp.set('v.isSelected', isSelected);
        helper.updateIndicators(cmp, helper.isDualMode(cmp), isSelected);
    }
})