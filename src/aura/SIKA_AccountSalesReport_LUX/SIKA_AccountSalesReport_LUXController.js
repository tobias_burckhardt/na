({
	doInit: function(component, event, helper) {
		console.log("init");
		var permAction = component.get("c.getCurrentUserInPermissionSet");
		permAction.setParams({
			"permissionSetName": "SAP_BI_Manager"
		});

		// Configure response handler
		permAction.setCallback(this, function(response) {
			var state = response.getState();
			if (component.isValid() && state === "SUCCESS") {
				console.log("success");
				component.set("v.hasPermission", response.getReturnValue());
			} else {
				console.log('Problem getting permissions, response state: ' + state);
			}
		});
		$A.enqueueAction(permAction);

		var action = component.get("c.getAccountFields");
		action.setParams({
			"accId": component.get("v.recordId")
		});

		// Configure response handler
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (component.isValid() && state === "SUCCESS") {
				component.set("v.account", response.getReturnValue());
			} else {
				console.log('Problem getting account, response state: ' + state);
			}
		});
		$A.enqueueAction(action);
		var userId = $A.get("$SObjectType.CurrentUser.Id");
		component.set('v.currentUserId', userId);
		component.find('userDataService').reloadRecord();
	},
	handleCancel: function(component, event, helper) {
		$A.get("e.force:closeQuickAction").fire();
	},
	navigateToReport1: function(component, event, helper) {
		console.log('=======' + component.get("v.account.SalesFiguresReportLink__c"));

		var n = component.get("v.account.SalesFiguresReportLink__c").indexOf("target");
		n = n - 2;
		console.log('=====substr==' + component.get("v.account.SalesFiguresReportLink__c").substring(10, n));
		window.location.href = 'https://sika-emea--emeadev01.cs86.my.salesforce.com/' + component.get("v.account.SalesFiguresReportLink__c").substring(10, n);

	},
	navigateToReport: function(component, event, helper) {
		var callSrc = event.currentTarget.dataset.valueid;
		var urlval = '';
		var bseUrl = $A.get("$Label.c.SIKA_ReportUrl");
		console.log('===callSrc==' + callSrc);
		var params = {};
		if (callSrc == 'SFR') {
			var n = component.get("v.account.SalesFiguresReportLink__c").indexOf("target");
			n = n - 2;
			console.log('=====substr==' + component.get("v.account.SalesFiguresReportLink__c").substring(10, n));
			//window.location.href = 'https://sika-emea--emeadev01.cs86.my.salesforce.com/'+component.get("v.account.SalesFiguresReportLink__c").substring(10,n);
			var urlval_temp = bseUrl + component.get("v.account.SalesFiguresReportLink__c").substring(10, n);
			urlval = urlval_temp.split("&amp;").join("&");
			params.reportType = 'salesFigures';
			//alert(urlval);
		}
		if (callSrc == 'AMR') {
			var n = component.get("v.account.ArticleMixReport__c").indexOf("target");
			n = n - 2;
			console.log('=====substr==' + component.get("v.account.ArticleMixReport__c").substring(10, n));
			//window.location.href = 'https://sika-emea--emeadev01.cs86.my.salesforce.com/'+component.get("v.account.ArticleMixReport__c").substring(10,n);
			var urlval_temp = bseUrl + component.get("v.account.ArticleMixReport__c").substring(10, n);
			urlval = urlval_temp.split("&amp;").join("&");
			params.reportType = 'articleMix';
		}
		if (callSrc == 'PMR') {
			var n = component.get("v.account.ProductMixLink__c").indexOf("target");
			n = n - 2;
			console.log('=====substr==' + component.get("v.account.ProductMixLink__c").substring(10, n));
			//window.location.href = 'https://sika-emea--emeadev01.cs86.my.salesforce.com/'+component.get("v.account.ProductMixLink__c").substring(10,n);
			var urlval_temp = bseUrl + component.get("v.account.ProductMixLink__c").substring(10, n);
			urlval = urlval_temp.split("&amp;").join("&");
			params.reportType = 'productMix';
		}
		if (callSrc == 'MySFR') {
			var n = component.get("v.account.MySalesFiguresReport__c").indexOf("target");
			n = n - 2;
			console.log('=====substr==' + component.get("v.account.MySalesFiguresReport__c").substring(10, n));
			// window.location.href = 'https://sika-emea--emeadev01.cs86.my.salesforce.com/'+component.get("v.account.MySalesFiguresReport__c").substring(10,n);
			var urlval_temp = bseUrl + component.get("v.account.MySalesFiguresReport__c").substring(10, n);
			urlval = urlval_temp.split("&amp;").join("&");
			params.reportType = 'salesFiguresBySalesReps';
			params.myReport = true;
			params.salesRepId = component.get('v.currentUserFields.ERPId__c');
		}
		if (callSrc == 'MyAMR') {
			var n = component.get("v.account.MyArticleMixReport__c").indexOf("target");
			n = n - 2;
			console.log('=====substr==' + component.get("v.account.MyArticleMixReport__c").substring(10, n));
			// window.location.href = 'https://sika-emea--emeadev01.cs86.my.salesforce.com/'+component.get("v.account.MyArticleMixReport__c").substring(10,n);
			var urlval_temp = bseUrl + component.get("v.account.MyArticleMixReport__c").substring(10, n);
			urlval = urlval_temp.split("&amp;").join("&");
			params.reportType = 'articleMixBySalesReps';
			params.myReport = true;
			params.salesRepId = component.get('v.currentUserFields.ERPId__c');
		}
		if (callSrc == 'MyPMR') {
			var n = component.get("v.account.MyProductMixReport__c").indexOf("target");
			n = n - 2;
			console.log('=====substr==' + component.get("v.account.MyProductMixReport__c").substring(10, n));
			// window.location.href = 'https://sika-emea--emeadev01.cs86.my.salesforce.com/'+component.get("v.account.MyProductMixReport__c").substring(10,n);
			var urlval_temp = bseUrl + component.get("v.account.MyProductMixReport__c").substring(10, n);
			urlval = urlval_temp.split("&amp;").join("&");
			params.reportType = 'productMixBySalesReps';
			params.myReport = true;
			params.salesRepId = component.get('v.currentUserFields.ERPId__c');
		}
		if (callSrc == 'SFSR') {
			var n = component.get("v.account.SalesFiguresBySalesRepsLink__c").indexOf("target");
			n = n - 2;
			console.log('=====substr==' + component.get("v.account.SalesFiguresBySalesRepsLink__c").substring(10, n));
			// window.location.href = 'https://sika-emea--emeadev01.cs86.my.salesforce.com/'+component.get("v.account.SalesFiguresBySalesRepsLink__c").substring(10,n);
			var urlval_temp = bseUrl + component.get("v.account.SalesFiguresBySalesRepsLink__c").substring(10, n);
			urlval = urlval_temp.split("&amp;").join("&");
			params.reportType = 'salesFiguresBySalesReps';
		}
		if (callSrc == 'AMSR') {
			var n = component.get("v.account.ArticleMixBySalesRepsLink__c").indexOf("target");
			n = n - 2;
			console.log('=====substr==' + component.get("v.account.ArticleMixBySalesRepsLink__c").substring(10, n));
			// window.location.href = 'https://sika-emea--emeadev01.cs86.my.salesforce.com/'+component.get("v.account.ArticleMixBySalesRepsLink__c").substring(10,n);
			var urlval_temp = bseUrl + component.get("v.account.ArticleMixBySalesRepsLink__c").substring(10, n);
			urlval = urlval_temp.split("&amp;").join("&");
			params.reportType = 'articleMixBySalesReps';
		}
		if (callSrc == 'PMSR') {
			var n = component.get("v.account.ProductMixBySalesRepsLink__c").indexOf("target");
			n = n - 2;
			console.log('=====substr==' + component.get("v.account.ProductMixBySalesRepsLink__c").substring(10, n));
			//window.location.href = 'https://sika-emea--emeadev01.cs86.my.salesforce.com/'+component.get("v.account.ProductMixBySalesRepsLink__c").substring(10,n);
			var urlval_temp = bseUrl + component.get("v.account.ProductMixBySalesRepsLink__c").substring(10, n);
			urlval = urlval_temp.split("&amp;").join("&");
			params.reportType = 'productMixBySalesReps';
		}
		console.log('=====urlval==' + urlval);
		var paramsKeyValues = urlval.substring(urlval.indexOf('?') + 1).split('&');

		for ( var i = 0; i < paramsKeyValues.length; i += 1 ) {
			var key = paramsKeyValues[i].split('=')[0]
			var value = paramsKeyValues[i].split('=')[1]
			params[key] = value;
		}
		params.accountId = params.Id;
		params.uiTheme = 'Theme4d';
		console.log('params', params);
		var evt = $A.get("e.force:navigateToComponent");
		evt.setParams({
			componentDef: "c:FinancialReports",
			componentAttributes: params
		});
		evt.fire();
		// console.log('fire navigate to component');
		// var urlEvent = $A.get("e.force:navigateToURL");
		// urlEvent.setParams({
		// 	"url": urlval
		// });
		// urlEvent.fire();
	}
})