({
    init : function(cmp, e, helper) {
        var objectName = cmp.get('v.objectName');
        var displayedColumns = cmp.get('v.displayedColumns');
        if ($A.util.isEmpty(displayedColumns)) {
            displayedColumns = ['Name'];
            cmp.set('v.displayedColumns', displayedColumns);
        } else if (typeof displayedColumns === 'string') {
            displayedColumns = JSON.parse(displayedColumns);
            cmp.set('v.displayedColumns', displayedColumns);
        }
        helper.fetchDescribe(
            cmp,
            objectName,
            function(describe, errorMessage) {
                if (errorMessage !== null) {
                    cmp.set('v.errorMessage', errorMessage);
                } else {
                    cmp.set('v.objectInfo', describe.objects[objectName]);
                    var fields = describe.objects[objectName].fields;
                    cmp.set('v.displayedColumnsDescribes', displayedColumns.map(function(apiName) {
                        return fields[apiName];
                    }));
                    cmp.set('v.describeFetched', true);
                }
            });
    },
    handleSearchEvent: function(cmp, e) {
        var params = e.getParams();
        if (params.searchEventType === 'started') {
            cmp.set('v.searchingNow', true);
        } else if (params.searchEventType === 'results') {
            cmp.set('v.searchingNow', false);
            cmp.set('v.searchResults', params.resultsList);
        }
    }
})