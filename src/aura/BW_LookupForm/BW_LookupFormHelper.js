({
    fetchDescribe : function(cmp, objectName, callback) {
        this.serverSideRead(cmp,
            'c.dfieldsAll',
            { objectName : objectName },
            callback);
    }
})