({
    serverSideAction : function(cmp, actionName, params, callback, opts) {
        var options = opts || {};
        var action = cmp.get(actionName);
        action.setParams(params);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid() && state === 'SUCCESS') {
                callback(response.getReturnValue(), null);
            } else if (state === 'INCOMPLETE' || state === 'ERROR') {
                var message = 'could not complete server side action ' + actionName + ' because: ';
                if (state === 'INCOMPLETE') {
                    message += 'request was incomplete';
                } else {
                    var errors = response.getError();
                    if (errors) {
                        message += ' there was an error: ';
                        errors.forEach(function(err) {
                            if (err.message) {
                                message += JSON.stringify(err) + ';';
                            }
                        });
                    } else {
                        message += ' there was an unknown error';
                    }
                }
                callback(null, message);
            }
        });
        if ('storable' in options && options.storable) {
            action.setStorable();
        }
        if ('abortable' in options && options.abortable) {
            action.setAbortable();
        }
        $A.enqueueAction(action);
    },
    serverSideRead : function(cmp, actionName, params, callback, opts) {
        var options = opts || {};
        options.storable = true;
        this.serverSideAction(cmp, actionName, params, callback, options);
    },
    serverSideMayWrite : function(cmp, actionName, params, callback, opts) {
        var options = opts || {};
        options.storable = false;
        this.serverSideAction(cmp, actionName, params, callback, options);
    }
})