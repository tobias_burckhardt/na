({
    init: function(cmp, event, helper) {
        cmp.set('v.tableRows', helper.buildTable(
            cmp.get('v.installationRecord'), cmp.get('v.describe')
            ));
    },
    installationInput : function(cmp, e, helper) {
        var value = e.srcElement.value;
        if (e.srcElement.getAttribute('type') == 'checkbox') {
            value = e.srcElement.checked;
        }
        helper.updateInstallationValue(cmp,
            e.srcElement.getAttribute('data-fieldname'),
            value);
    },
    changeInstallationAccount : function(cmp, e, helper) {
        helper.updateInstallationValue(cmp,
            'Account__c',
            e.getParams().selectedRecord.Id);
    }
})