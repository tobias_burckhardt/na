({
    buildTable : function(installationRecord, describe) {
        var fields = describe.objects.Sika_Asset_Installation__c.fields;

        var InfoRow = function(helpText, inputType, fieldName) {
            this.label = fields[fieldName].label;
            this.helpText = helpText;
            this.hasHelpText = (helpText !== undefined)? 'showHelpMarker' : '';
            this.inputType = inputType;
            this.isInput = (inputType !== undefined);
            this.value = installationRecord[fieldName];
            this.fieldName = fieldName;
        };
        return [
            new InfoRow(
                fields.Account_Name__c.inlineHelpText,
                'acctLookup',
                'Account_Name__c'
            ),
            new InfoRow(
                fields.Location_of_Equipment_on_Site__c.inlineHelpText,
                'text',
                'Location_of_Equipment_on_Site__c'
            ),
            new InfoRow(
                fields.Direct_Feed__c.inlineHelpText,
                'checkbox',
                'Direct_Feed__c'
            ),
            new InfoRow(
                fields.Estimated_Total_Equipment_Value__c.inlineHelpText,
                'number',
                'Estimated_Total_Equipment_Value__c'
            )
        ];
    },
    updateInstallationValue : function(cmp, fieldName, formInput) {
        var attr = 'v.installationRecord';
        var record = cmp.get(attr);
        record[fieldName] = formInput;
        cmp.set(attr, record);
        var inputChanged = cmp.getEvent('inputUsed');
        inputChanged.setParams({
            recordId : record.Id,
            sobjectType : 'Sika_Asset_Installation__c',
            fieldName : fieldName,
            fieldVal : formInput
        });
        inputChanged.fire();
    }
})