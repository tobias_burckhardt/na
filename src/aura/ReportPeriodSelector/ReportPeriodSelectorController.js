({
	init: function(cmp, event, helper) {
		var currMonth = new Date().getMonth() + 1//var currMonth = '12';
		helper.setYTDMonths(cmp, currMonth);
        cmp.set('v.yearLevel', helper.getSelectorYears());
/*
		$A.createComponents([
			["c:sldsCheckbox", {label:'Jan'}]
		],function(components,status,statusMessagesList) {
			console.log('checkbox1', components[0]);
			var periodSelectorContainer = cmp.find('periodSelector').get('v.body');
			periodSelectorContainer.push(components[0]);
			periodSelectorContainer.set('v.body', periodSelectorContainer)
		});
*/
	},

	selectFullYear: function(cmp, event, helper) {
        var currentyear = new Date().getFullYear();
        var selectedyear = cmp.find("selectedyear").set("v.value",currentyear);
		helper.setYTDMonths(cmp, 12, 'True');
	},

	calculate: function(cmp) {
		console.log("in calculate");
		var selectedMonths = [];
        var selectedyear = cmp.find("selectedyear").get("v.value");
        //alert(selected);
		var periodSelectorContainerComponents = cmp.find('periodSelector').get('v.body');
        //alert(periodSelectorContainerComponents.length);
		for ( var i = 0; i < periodSelectorContainerComponents.length; i++ ) {
			var monthCheckbox = periodSelectorContainerComponents[i];
			if ( monthCheckbox.get('v.checked') ) {
				var monthId = monthCheckbox.get('v.id');
				var monthNumber = parseInt(monthId.substring(monthId.indexOf('_') + 1, monthId.length), 10);
				selectedMonths.push(monthNumber);
			}
		}
		cmp.set('v.selectedMonths', selectedMonths);
		console.log('selectedMonths', selectedMonths);
        console.log('selectedyear', selectedyear);
		var recalculateEvent = $A.get('e.c:FinancialReports_ReloadReport');
		recalculateEvent.setParams({
			selectedMonths: selectedMonths,
            selectedYear : selectedyear
		});
		recalculateEvent.fire();
	},

    calculateyearchange: function(cmp) {
    	console.log("calculateyearchange");
		var selectedMonths = [];
        var selectedyear = cmp.find("selectedyear").get("v.value");
        //alert(selected);
		var periodSelectorContainerComponents = cmp.find('periodSelector').get('v.body');
        //alert(periodSelectorContainerComponents.length);
		for ( var i = 0; i < 12; i++ ) {
            var monthCheckbox = periodSelectorContainerComponents[i];
            monthCheckbox.set('v.checked', true);
			selectedMonths.push(i + 1);
		}
		cmp.set('v.selectedMonths', selectedMonths);
		console.log('selectedMonths', selectedMonths);
        console.log('selectedyear', selectedyear);
		var recalculateEvent = $A.get('e.c:FinancialReports_ReloadReport');
		recalculateEvent.setParams({
			selectedMonths: selectedMonths,
            selectedYear : selectedyear,
            isyearchange : 'True'
		});
		recalculateEvent.fire();
	},



	selectYTDMonths: function(cmp, event, helper) {
		console.log("in select YTD Months");
		var currMonth = new Date().getMonth() + 1;
        var currentyear = new Date().getFullYear();
        var selectedyear = cmp.find("selectedyear").get("v.value");
        if( currentyear != selectedyear )
		{
	        cmp.find("selectedyear").set("v.value", currentyear);
			helper.setYTDMonths(cmp, currMonth, 'True');
        }
		else
		{
            helper.setYTDMonths(cmp, currMonth, 'False');
        }
	}
})