({
	setYTDMonths: function(cmp, lastMonthIndex, isYearChanged) {
		var selectedMonths = [];
		var periodSelectorContainerComponents = cmp.find('periodSelector').get('v.body');
		for ( var i = 0; i < 12; i++ ) {
			if ( i < lastMonthIndex )
			{
				selectedMonths.push(i + 1);
			}
			var monthCheckbox = periodSelectorContainerComponents[i];
			if ( i < lastMonthIndex ) {
				monthCheckbox.set('v.checked', true);
				//selectedMonths.push(i + 1);
			} else {
				monthCheckbox.set('v.checked', false);
			}
		}
        //selectedMonths.push(lastMonthIndex);
       // alert(selectedMonths);
		cmp.set('v.selectedMonths', selectedMonths);
		var recalculateEvent = $A.get('e.c:FinancialReports_ReloadReport');
		recalculateEvent.setParams({
			selectedMonths: selectedMonths,
            selectedYear :new Date().getFullYear(),
            isyearchange : isYearChanged
		});
		recalculateEvent.fire();
	},

	getSelectorYears: function() {
		let currentYear = new Date().getFullYear();
		return [currentYear, currentYear -1, currentYear -2];
	}
    
})