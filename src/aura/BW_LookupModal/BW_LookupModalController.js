({
    displayModal : function(cmp, e) {
        var params = e.getParams();
        cmp.set('v.objectName', params.objectName);
        cmp.set('v.label', params.label);
        cmp.set('v.displayedColumns', params.displayedColumns);
        cmp.set('v.callback', function(record){
            cmp.set('v.modalDisplayed', false);
            params.callback(record);
        });
        cmp.set('v.modalDisplayed', true);
    },
    closeModal : function(cmp) {
        cmp.set('v.modalDisplayed', false);
    }
})