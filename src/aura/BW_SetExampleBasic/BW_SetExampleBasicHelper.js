({
    setupExampleAccounts : function() {
        var accts = [];
        var adjectives = ['Industrial', 'Mega', 'World', 'Quality', 'Global', 'Intl', 'Digital', 'Web', 'Local', 'Budget', 'Artisan'];
        var nouns = ['Systems', 'Dynamics', 'Conglomerate', 'Enterprises', 'Trading', 'Products', 'Tech', 'Commodities', 'Transactions', 'Mechanics'];
        var adjLength = adjectives.length;
        var nounLength = nouns.length;
        // work within the 100-200 range so that we can provide up to 100 examples
        // while providing realistic Ids
        for (var i=110; i<120; i+=1) {
            accts.push({
                Id : '001000000000000' + i,
                Name : adjectives[i%adjLength] + ' ' + nouns[i % nounLength] + ' Inc.',
                sObjectType : 'Account'
            });
        }
        return accts.sort(function(acctLeft, acctRight) {
            return acctLeft.Name.localeCompare(acctRight.Name);
        });
    }
})