({
    init: function(cmp, e, helper) {
        cmp.set('v.accounts', helper.setupExampleAccounts());
    },
    updateSelected: function(cmp, e) {
        var params = e.getParams();
        cmp.set('v.selected', params.containedObjects);
        cmp.set('v.selectedIdents', params.containedIdents);
    }
})