<apex:page controller="sika_SCCZoneTestResultsController" action="{!loadAction}" cache="false" sidebar="false" ShowHeader="false" standardStylesheets="false">
    <style>
        .bump{
            position: relative;
            top: 7px;
        }
    </style>
    <apex:variable var="t" value="{!result}"/>
    <apex:composition template="sika_SCCZoneBaseTemplate">
        <apex:define name="pageContent">
            <div class="container">
                <div class="row">
                    <c:sika_SCZoneSideNavigation />
                    <div class="column-wrapper">
                        <apex:form >
                            <h1>
                                <span><i><apex:outputText style="font-size:15px" value="Test Results"/></i></span><br/>
                                <apex:outputText value="{!theMix.Mix_Name__c}"/>
                                <div>
                                    <apex:commandButton value="Return To Mix" action="{!returnMix}" styleClass="utility"/>
                                    <apex:commandButton value="Delete" action="{!showPopup}" rerender="tstpopup" disabled="{!theMix.OwnerID != userID}" styleClass="utility {!IF(theMix.OwnerID != userID, 'disabled', '')}"/>
                                    <apex:commandButton value="Edit" action="{!editPage}" disabled="{!theMix.OwnerID != userID}" styleClass="utility {!IF(theMix.OwnerID != userID, 'disabled', '')}"/>
                                    
                                </div>
                            </h1>
                            <apex:pageBlock >
                                <div class="info-wrapper">
                                    <apex:pageBlockSection title="General Test Details" columns="2">
                                        <apex:pageBlockSectionItem >
                                            <apex:outputLabel value="Tester:" for="tester"/>
                                            <apex:outputText value="{!t.Tester__c}" id="tester"/> 
                                        </apex:pageBlockSectionItem> 
                                  
                                        <apex:pageBlockSectionItem >
                                            <apex:outputLabel value="Initial Test Date:" for="initial_date"/>
                                            <apex:outputText value="{!t.Initial_Test_date__c}" id="initial_date"/> 
                                        </apex:pageBlockSectionItem>
                                   
                                        <apex:pageBlockSectionItem >
                                            <apex:outputLabel value="Test Location:" for="test_location"/>
                                            <apex:outputText value="{!t.Test_Location__c}" id="test_location"/> 
                                        </apex:pageBlockSectionItem> 
                                  
                                        <apex:pageBlockSectionItem >
                                            <apex:outputLabel value="Mixing Date:" for="mix_date"/>
                                            <apex:outputText value="{!t.Mixing_Date__c}" id="mix_date"/> 
                                        </apex:pageBlockSectionItem> 
                                    
                                        <apex:pageBlockSectionItem >
                                            <apex:outputLabel value="Initial Set Time: (hrs)" for="set_time"/>
                                            <apex:outputText value="{!t.Initial_Set_Time__c} hrs" id="set_time"/> 
                                        </apex:pageBlockSectionItem> 
                                     
                                        <apex:pageBlockSectionItem >
                                            <apex:outputLabel value="Final Set Time: (hrs)" for="final_set_time"/>
                                            <apex:outputText value="{!t.Final_Set_Time__c} hrs" id="final_set_time" styleClass="bump"/> 
                                        </apex:pageBlockSectionItem> 
                                  
                                
                                    </apex:pageBlockSection>
                                </div>

                                 <div class="info-wrapper">

                            <apex:pageBlockSection title="Test Details" columns="2">
                             <apex:pageBlockSectionItem >
                                            <apex:outputLabel value="Unit Weight:"/>
                                            <apex:outputField value="{!t.Unit_Weight__c}"/> 
                                        </apex:pageBlockSectionItem> 

                                         <apex:pageBlockSectionItem >
                                            <apex:outputLabel value="Concrete Temp:" />
                                            <apex:outputField value="{!t.Concrete_Temp__c}"/> 
                                        </apex:pageBlockSectionItem> 

                                         <apex:pageBlockSectionItem >
                                            <apex:outputLabel value="Ambient Temp:"/>
                                            <apex:outputField value="{!t.Ambient_Temp__c}" styleClass="bump"/> 
                                        </apex:pageBlockSectionItem> 

                                         <apex:pageBlockSectionItem >
                                            <apex:outputLabel value="Air Content:"/>
                                            <apex:outputField value="{!t.Air_Content__c}" styleClass="bump"/> 
                                        </apex:pageBlockSectionItem> 

                                          <apex:pageBlockSectionItem >
                                            <apex:outputLabel value="Slump Flow:"/>
                                            <apex:outputField value="{!t.Slump_Flow__c}" styleClass="bump"/>
                                        </apex:pageBlockSectionItem> 

                                          <apex:pageBlockSectionItem >
                                            <apex:outputLabel value="VSI (Visual Stability Index):" />
                                            <apex:outputField value="{!t.VSI__c}" styleClass="bump"/> 
                                        </apex:pageBlockSectionItem> 

                                           <apex:pageBlockSectionItem >
                                            <apex:outputLabel value="T-20:"/>
                                            <apex:outputField value="{!t.T20__c}" styleClass="bump"/> 
                                        </apex:pageBlockSectionItem> 
                                         <apex:pageBlockSectionItem >
                                            <apex:outputLabel value="Static Segregation:"/>
                                            <apex:outputField value="{!t.Static_segregation__c}" styleClass="bump"/> 
                                        </apex:pageBlockSectionItem> 

                                        <apex:pageBlockSectionItem >
                                            <apex:outputLabel value="Column Segregation, Top:"/>
                                            <apex:outputField value="{!t.Col_Segregation_Top__c}" styleClass="bump"/> 
                                        </apex:pageBlockSectionItem> 
                                       
                                       
                                     
                                        <apex:pageBlockSectionItem >
                                            <apex:outputLabel value="J Ring Pass/Fail:"/>
                                            <apex:outputField value="{!t.JRing_Test__c}" styleClass="bump"/> 
                                        </apex:pageBlockSectionItem> 
                                         <apex:pageBlockSectionItem >
                                            <apex:outputLabel value="Column Segregation, Bottom:"/>
                                            <apex:outputField value="{!t.Col_Segregation_Bottom__c}" styleClass="bump"/> 
                                        </apex:pageBlockSectionItem> 
                                      
                                        <apex:pageBlockSectionItem >
                                            <apex:outputLabel value="J Ring Size:"/>
                                            <apex:outputField value="{!t.JRing_Size__c}" styleClass="bump"/> 
                                        </apex:pageBlockSectionItem> 
                                      
                                       

                            </apex:pageBlockSection>
                                </div>
                                
                                <div class="section-wrapper">
                                    <apex:pageBlockSection title="Compressive Strength ({!chartLabel})">
                                        <table cellpadding="0" cellspacing="0" border="0" class="dataGrid">
                                            <thead>
                                                <tr>
                                                    <th>Time Elapsed</th>
                                                    <th>Cylinder 1</th>
                                                    <th>Cylinder 2</th>
                                                    <th>Cylinder 3</th>
                                                    <th>Average</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="gridRowNormal">
                                                    <td>{!t.Strength_Test_1_label__c}</td>
                                                    <td>{!t.Strength_16hr_1__c}</td>
                                                    <td>{!t.Strength_16hr_2__c}</td>
                                                    <td>{!t.Strength_16hr_3__c}</td>
                                                    <td>{!t.Strength_Test_16h_avg__c}</td>
                                                </tr>
                                                <tr class="gridRowAlternate">
                                                    <td>{!t.Strength_Test_2_label__c}</td>
                                                    <td>{!t.Strength_1d_1__c}</td>
                                                    <td>{!t.Strength_1d_2__c}</td>
                                                    <td>{!t.Strength_1d_3__c}</td>
                                                    <td>{!t.Strength_Test_1d_avg__c}</td>
                                                </tr>
                                                <tr class="gridRowNormal">
                                                    <td>{!t.Strength_Test_3_label__c}</td>
                                                    <td>{!t.Strength_3day_1__c}</td>
                                                    <td>{!t.Strength_3day_2__c}</td>
                                                    <td>{!t.Strength_3day_3__c}</td>
                                                    <td>{!t.Strength_Test_3d_avg__c}</td>
                                                </tr>
                                                <tr class="gridRowAlternate">
                                                    <td>{!t.Strength_Test_4_label__c}</td>
                                                    <td>{!t.Strength_7day_1__c}</td>
                                                    <td>{!t.Strength_7day_2__c}</td>
                                                    <td>{!t.Strength_7day_3__c}</td>
                                                    <td>{!t.Strength_Test_7d_avg__c}</td>
                                                </tr>
                                                <tr class="gridRowNormal">
                                                    <td>{!t.Strength_Test_5_label__c}</td>
                                                    <td>{!t.Strength_28day_1__c}</td>
                                                    <td>{!t.Strength_28day_2__c}</td>
                                                    <td>{!t.Strength_28day_3__c}</td>
                                                    <td>{!t.Strength_Test_28d_avg__c}</td>
                                                </tr>
                                                <tr class="gridRowAlternate">
                                                    <td>{!t.Strength_Test_6_label__c}</td>
                                                    <td>{!t.Strength_56day_1__c}</td>
                                                    <td>{!t.Strength_56day_2__c}</td>
                                                    <td>{!t.Strength_56day_3__c}</td>
                                                    <td>{!t.Strength_Test_56d_avg__c}</td>
                                                </tr>
                                                <tr class="gridRowNormal">
                                                    <td>{!t.Strength_Test_7_label__c}</td>
                                                    <td>{!t.Strength_90day_1__c}</td>
                                                    <td>{!t.Strength_90day_2__c}</td>
                                                    <td>{!t.Strength_90day_3__c}</td>
                                                    <td>{!t.Strength_Test_90d_avg__c}</td>
                                                </tr>
                                                <tr class="gridRowAlternate">
                                                    <td>{!t.Strength_Test_8_label__c}</td>
                                                    <td>{!t.Strength_open_1__c}</td>
                                                    <td>{!t.Strength_open_2__c}</td>
                                                    <td>{!t.Strength_open_3__c}</td>
                                                    <td>{!t.Strength_Test_open_avg__c}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </apex:pageBlockSection>
                                </div>
                            </apex:pageBlock>
                             <apex:outputPanel id="tstpopup">
        <apex:outputPanel styleClass="popupBackground" layout="block" rendered="{!displayPopUp}"/>
     
            <apex:outputPanel styleClass="custPopup" layout="block" rendered="{!displayPopUp}"><center>
            <u><apex:outputText style="font-weight:800" value="Delete Test Result"/></u><br/><br/>
                <apex:outputLabel value="Are you sure you want to delete this test result?">
                </apex:outputLabel>
                <br/><br/>
                
                <apex:actionStatus id="SaveButtonStatus">
                              <apex:facet name="stop">
                               <apex:outputPanel >
                                <apex:commandButton action="{!deleteThisTest}" styleclass="utility" value="Delete" status="SaveButtonStatus" rerender="errormsg" />
                                <apex:commandButton action="{!closePopup}" value="Cancel" styleclass="utility" rerender="tstpopup" immediate="true" />
                               </apex:outputPanel> 
                              </apex:facet>
                              <apex:facet name="start">
                               <apex:outputPanel >
                                <apex:commandButton styleclass="utility" value="Deleting..." disabled="true" />
                                <apex:commandButton styleclass="utility" value="Deleting..." disabled="true" />
                               </apex:outputPanel>
                              </apex:facet>
                             </apex:actionStatus>
                </center>
            </apex:outputPanel>
        </apex:outputPanel>

                        </apex:form>
                    </div>
                </div>
            </div>
        </apex:define>
    </apex:composition>
        <style type="text/css">
        .custPopup{
            background-color: white;
            border-width: 2px;
            border-style: solid;
            z-index: 9999;
            left: 50%;
            padding:10px;
            position: fixed;
            /* These are the 3 css properties you will need to change so the popup 
            displays in the center of the screen. First set the width. Then set 
            margin-left to negative half of what the width is. You can add 
            the height property for a fixed size pop up if you want.*/
            width: 500px;
            margin-left: -250px;
            top:100px;
        }
        .popupBackground{
            background-color:black;
            opacity: 0.70;
            filter: alpha(opacity = 20);
            position: fixed;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: 9998;
        }

    </style>
</apex:page>