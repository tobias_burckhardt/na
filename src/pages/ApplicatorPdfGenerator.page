<apex:page standardController="Account" applyBodyTag="false" extensions="ApplicatorPdfGeneratorController" renderAs="pdf">
    <head>
        <style type="text/css" media="print">
            @page {
                size:portrait;
                margin-left: 1.5cm; 
                margin-right: 1.5cm;
                margin-top: 2cm; 
                margin-bottom: 3cm;
            
                @top-center {
                    content: element(header);
                }
                @bottom-left {
                    content: element(footer);
                }
            }
                
            .header {
                position: running(header);
                display: block;
            }
            
            .header-left {
                border: 1px solid white;
                float: left;
                display:inline-block;
                width: 55%;
                padding-top: 50px;
            }            
            
            .header-right {
                border: 1px solid white;
                float: left;
                display:inline-block;
                width: 40%;
                padding-top: 50px;
            }
            
            .clear {
                clear: both;
            }
            
            .content {
                padding-top: 80px;
                font-family: sans-serif;
                font-size: 10pt;
            }
            
            .sheet th {
                text-align: rigth;
                font-weight: normal;
                padding: 4px 8px 4px 0;
                vertical-align: top;
            } 
            
            .sheet td {
                padding: 4px 0;
            }
            
            .sheet .subject>* {
                
            }
            
            .contentHeader{
                padding-top: 25px;
            }

            .contentBody{
                padding-top: 25px;
            }

            .contentSignature{
                padding-top: 25px;
            }

            .footer {
                position: running(footer);
                font-family: sans-serif;
                font-size: 8pt;
            }
            
            .footer p {
                margin: 0;
            }
            
            .pagenumber:before {
                content: counter(page);
            }
            
            .pagecount:before {
                content: counter(pages);
            }
        </style>
    </head>
    <body>
        <div class="header">
            <div class="header-left">
                <apex:image value="{!$Resource.ApplicatorLetterPartnersClub}" height="80" rendered="{!elite}"/>
            </div>
            <div class="header-right">
                <apex:image url="{!$Resource.ApplicatorLetterLogo}" height="50"/>
            </div>
            <!--<div class="clear">&nbsp;</div>-->
        </div>
        <div class="content">
            <p>{!todayFormatted}</p>
            <div class="contentHeader">
                <table class="sheet">
                    <tr>  
                        <th>Company:</th>
                        <td>{!acc.Name}</td>
                    </tr>
                    <tr>
                        <th>Location:</th>
                        <td>{!acc.BillingStreet}<br/>
                            {!acc.BillingCity}, {!acc.BillingState} {!acc.BillingPostalCode}</td>
                    </tr>
                    
                    <tr class="subject">
                        <th>Re:</th>
                        <td>Authorized Applicator Status</td>
                    </tr>
                </table>
            </div>
            <div class="contentBody">
                <p>To Whom It May Concern:</p>
                
                <p>This letter is to certify that {!acc.Name} is in good standing as an authorized applicator of Sarnafil {!agreements} systems and qualifies for warranties provided by the Sika Corporation – Roofing division.</p>
                
                <apex:outputText rendered="{!elite}">
                    <p>{!acc.Name} has earned the status of Partners Club Elite Applicator, our highest overall rating relative to quality installations, technical expertise, payment history and integrity.</p>
                </apex:outputText>
                
                <p>If you have any questions, please contact our office.</p>
            </div>
            <div class="contentSignature">
                <p>Kind Regards,</p>
                
                <p>Sika Corporation - Roofing</p>
            </div>
        </div>
        <div class="footer">
            <p style="margin-bottom: 40px !important;"><strong>SIKA CORPORATION - ROOFING</strong><br/>
                <apex:outputPanel rendered="{! !ISBLANK(roofing_Rep_Tech_Mgr.Name) }">
                    {!roofing_Rep_Tech_Mgr.Name}<br/>
                </apex:outputPanel>
                <apex:outputPanel rendered="{! !ISBLANK(roofing_Rep_Tech_Mgr.Title) }">
                    {!roofing_Rep_Tech_Mgr.Title}<br/>
                </apex:outputPanel>
                <apex:outputPanel rendered="{! !ISBLANK(roofing_Rep_Tech_Mgr.Street) }">
                    {!roofing_Rep_Tech_Mgr.Street}<br/>
                </apex:outputPanel>
                <apex:outputPanel rendered="{! !ISBLANK(roofing_Rep_Tech_Mgr.City) }">
                    {!roofing_Rep_Tech_Mgr.City},
                </apex:outputPanel>
                <apex:outputPanel rendered="{! !ISBLANK(roofing_Rep_Tech_Mgr.State) }">
                    {!roofing_Rep_Tech_Mgr.State}&nbsp;
                </apex:outputPanel>
                <apex:outputPanel rendered="{! !ISBLANK(roofing_Rep_Tech_Mgr.PostalCode) }">
                    {!roofing_Rep_Tech_Mgr.PostalCode}<br/>
                </apex:outputPanel>
                <apex:outputPanel rendered="{! !ISBLANK(roofing_Rep_Tech_Mgr.Phone) }">
                    {!roofing_Rep_Tech_Mgr.Phone}<br/>
                </apex:outputPanel>
                <apex:outputPanel rendered="{! !ISBLANK(roofing_Rep_Tech_Mgr.Email) }">
                    {!roofing_Rep_Tech_Mgr.Email}<br/>
                </apex:outputPanel>
                <!--{!reg.Address__c} {!reg.City__c}, {!reg.Zip__c} {!reg.State__c}<br/>-->
            </p>
        </div>        
    </body>
</apex:page>