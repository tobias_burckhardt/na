<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>GLOBAL</label>
    <protected>false</protected>
    <values>
        <field>SikaCMP__ConnectionID__c</field>
        <value xsi:type="xsd:string">04P1W0000008u1rUAA</value>
    </values>
    <values>
        <field>SikaCMP__OrgConnection__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
