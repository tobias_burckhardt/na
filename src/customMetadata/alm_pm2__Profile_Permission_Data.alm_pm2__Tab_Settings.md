<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Tab Settings</label>
    <protected>false</protected>
    <values>
        <field>alm_pm2__Category__c</field>
        <value xsi:type="xsd:string">Tab Settings</value>
    </values>
    <values>
        <field>alm_pm2__Has_Sub_Sections__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>alm_pm2__Help_Text__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>alm_pm2__Is_Category__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>alm_pm2__Option_Label__c</field>
        <value xsi:type="xsd:string">Tab Settings</value>
    </values>
    <values>
        <field>alm_pm2__Option_Value__c</field>
        <value xsi:type="xsd:string">tabVisibilities</value>
    </values>
    <values>
        <field>alm_pm2__Order__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
</CustomMetadata>
