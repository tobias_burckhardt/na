<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default</label>
    <protected>false</protected>
    <values>
        <field>Warranty_Self_Reg_Owner_UserName__c</field>
        <value xsi:type="xsd:string">estes.mike@us.sika.com</value>
    </values>
</CustomMetadata>
