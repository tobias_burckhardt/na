public without sharing class sika_SCCZoneMaterialsHomePageController {
    public Id userID { get; set; }
    public Id materialID { get; set; }
    public Id userAccountId { get; set; }
    public list<Material__c> theMaterials { get; set; }
    public list<Material__c> myMaterials { get; set; }
    public list<Material__c> cementList { get; set; }
    public list<Material__c> scmList { get; set; }
    public list<Material__c> sandList { get; set; }
    public list<Material__c> coarseList { get; set; }
    public list<Material__c> adList { get; set; }
    public list<Material__c> waterList { get; set; }
    public list<Material__c> otherList { get; set; }
    public Material__c theMaterial { get; set; }
    public list<Mix_Material__c> mixMaterialList { get; set; }
    public list<Test_Result__c> testResultsList { get; set; }
    public String view { get; set; }
    public String selectOption { get; set; }
    public String unit { get; set; }
    public String type { get; set; }
    public Boolean displayPopUp { get; set; }
    public Boolean displayPopUp2 { get; set; }
    public list<Material__c> displayList { get; set; }

    public Material__c clonedMaterial { get; set; }

    public String unitSystem { get; set; }
    public list<SelectOption> unitSystemList { get; set; }

    public list<SelectOption> unitOptionList { get; set; }
    public String unitString { get; set; }
    public list<SelectOption> typeList { get; set; }
    public String typeString { get; set; }
    public User theUser { get; set; }
    public list<Admixture_Price__c> theAdmixturePrices { get; set; }
    public map<ID, String> adMap { get; set; }

    //public map<ID, String> adUnitMap {get; set;}
    public sika_SCCZoneMaterialsHomePageController() {
        sika_SCCZoneCheckAuth.setAuthStatus(UserInfo.getProfileId());

        userID = UserInfo.getUserId();
        materialID = Apexpages.currentPage().getParameters().get('id');    // for edit material page
        view = Apexpages.currentPage().getParameters().get('view');            // for materials homepage -- determines whether we'll show all material types, or filter for a specific type
        selectOption = Apexpages.currentPage().getParameters().get('state');// for materials homepage -- determines whether we'll show "my materials" or "my company's materials"
        unit = Apexpages.currentPage().getParameters().get('unit');            // for new material page -- tells the system of measure for the material we're creating
        type = Apexpages.currentPage().getParameters().get('type');            // for new material page -- tells what type of material we're creating
        clonedMaterial = new Material__c();
        unitOptionList = new list<SelectOption>();
        typeList = new list<SelectOption>();
        typeList.add(new SelectOption('None', '--None--'));
        typeList.add(new SelectOption('Cement', 'Cement'));
        typeList.add(new SelectOption('SCM', 'SCM'));
        typeList.add(new SelectOption('Sand', 'Sand'));
        typeList.add(new SelectOption('Coarse Aggregate', 'Coarse Aggregate'));
        typeList.add(new SelectOption('Water', 'Water'));
        typeList.add(new SelectOption('Other', 'Other'));
        unitSystemList = new list<SelectOption>();
        unitSystemList.add(new SelectOption('None', '--None--'));
        unitSystemList.add(new SelectOption('Metric (SI)', 'Metric (SI)'));
        unitSystemList.add(new SelectOption('US Customary', 'US Customary'));

        //adMap = new map<ID, String>();        
        //adUnitMap = new map<ID, String>();

        theUser = [
            Select Id,
                AccountID,
                ContactID, System_of_measure__c
            FROM User
            WHERE Id = :userID
            Limit 1
        ];

        theMaterials = [
            SELECT ID,
                Unit__c,
                isLocked__c,
                Account__c,
                Unit_System__c,
                Material_Name__c,
                Type__c,
                Sieve_1__c,
                Source__c,
                Owner.Name, Cost__c,
                isActive__c
            FROM Material__c
            WHERE (OwnerID = :userID
            OR Account__c = :theUser.AccountID
            OR Type__c = 'Admixture'
            )
            AND isActive__c = true
            ORDER BY Material_Name__c
        ];

        userAccountId = theUser.AccountId;
        displayPopUp = false;
        displayPopUp2 = false;
        cementList = new list<Material__c>();
        scmList = new list<Material__c>();
        sandList = new list<Material__c>();
        coarseList = new list<Material__c>();
        adList = new list<Material__c>();
        waterList = new list<Material__c>();
        otherList = new list<Material__c>();
        myMaterials = new list<Material__c>();

        set<ID> adSet = new set<ID>();

        list<Material__c> tempList = new list<Material__c>();
        for (Material__c m : theMaterials) {
            if (m.OwnerID == userID) {
                myMaterials.add(m);
            }

            if (m.Type__c == 'Admixture') {
                adList.add(m);
                adSet.add(m.ID);
            } else {
                tempList.add(m);
            }
        }

        displayList = new list<Material__c>();
        if (selectOption == 'me') {
            displayList = myMaterials;
        } else {
            displayList = tempList;
        }

        for (Material__c m : displayList) {
            if (m.Type__c == 'Cement') {
                cementList.add(m);
            } else if (m.Type__c == 'SCM') {
                scmList.add(m);
            } else if (m.Type__c == 'Sand') {
                sandList.add(m);
            } else if (m.Type__c == 'Coarse Aggregate') {
                coarseList.add(m);
            }

            else if (m.Type__c == 'Water') {
                waterList.add(m);
            } else {
                otherList.add(m);
            }
        }

        theAdmixturePrices = [
            Select Id,
                OwnerId,
                Admixture_Material__c,
                Price__c,
                Unit__c
            FROM Admixture_Price__c
            WHERE OwnerId = :userId
            AND Admixture_Material__c IN:adSet
        ];

        // get the price and unit info for any admixture materials that have a related Admixture_Price object.  (I.E., admixtures for which the user has saved a price.)
        addAdmixtureInfo(adList);

        if (materialID != null) {
            String qString = 'Select ID, Material_Name__c, Type__c, Source__c, Owner.Name, Description__c, Cost__c, ';
            qString += 'Unit__c, Unit_System__c,Abs__c, CreatedById, Spec_Gravity__c, CreatedDate, isActive__c, isLocked__c, SCM_Effective_Factor__c, ';
            qString += 'PDS_Link__c, Account__c, SDS_Link__c, ';

            qString += sieveQueryHelper();

            qString += 'FROM Material__c WHERE ID = \'' + materialID + '\'';
            system.debug('********CTRL: qString = ' + qString);

            theMaterial = Database.query(qString);

            unitString = String.valueOf(theMaterial.Unit__c);
            if (theMaterial.Unit_System__c == 'Metric (SI)') {
                unitOptionList.add(new SelectOption('', '-- None --'));
                unitOptionList.add(new SelectOption('kgs', 'kgs'));
                unitOptionList.add(new SelectOption('tonne', 'tonne'));
                unitOptionList.add(new SelectOption('liter', 'liter'));

            } else {
                unitOptionList.add(new SelectOption('', '-- None --'));
                unitOptionList.add(new SelectOption('lbs', 'lbs'));
                unitOptionList.add(new SelectOption('ton', 'ton'));
                unitOptionList.add(new SelectOption('gallon', 'gallon'));

            }
        } else {
            theMaterial = new Material__c(OwnerID = userID, Type__c = type, Unit_System__c = unit, isActive__c = true, Account__c = theUser.AccountID);
            if (type == 'Water') {
                theMaterial.Spec_Gravity__c = 1.0;
                theMaterial.Abs__c = '0';
            }
            if (unit == 'Metric (SI)') {
                unitOptionList.add(new SelectOption('', '-- None --'));
                unitOptionList.add(new SelectOption('kgs', 'kgs'));
                unitOptionList.add(new SelectOption('tonne', 'tonne'));
                unitOptionList.add(new SelectOption('liter', 'liter'));
            } else {
                unitOptionList.add(new SelectOption('', '-- None --'));
                unitOptionList.add(new SelectOption('lbs', 'lbs'));
                unitOptionList.add(new SelectOption('ton', 'ton'));
                unitOptionList.add(new SelectOption('gallon', 'gallon'));
            }
        }

        // if the material is an admixture, see whether the user has saved a (custom) price for the material. (Admixtures are not editable by the user -- only by the SysAdmin -- but the user can save a price by creating an Admixture_Price record.  This happens behind-the-scenes when the user tries to "edit" an admixture.)
        if (theMaterial.Type__c == 'Admixture') {
            addAdmixtureInfo(theMaterial);
        }
    }

    // check user auth status
    public PageReference loadAction() {
        PageReference authCheck;

        // because this controller runs "without sharing" we need to manually confirm that the user should have access. (A user whose session has timed-out, or an otherwise unauthenticated user who accessed this page directly, would be able to view the page otherwise, albeit without any data displayed.)
        authCheck = sika_SCCZoneCheckAuth.doAuthCheck();
        if (authCheck != null) {
            return authCheck;
        }

        return null;
    }


    private void addAdmixtureInfo(Material__c theMaterial) {
        list<Material__c> tempList = new list<Material__c>{
            theMaterial
        };
        addAdmixtureInfo(tempList);
    }

    private void addAdmixtureInfo(list<Material__c> theMaterials) {
        map<Id, Decimal> adId_to_adPrice = new map<Id, Decimal>();
        map<Id, String> adId_to_unit = new map<Id, String>();

        for (Admixture_Price__c p : theAdmixturePrices) {
            adId_to_adPrice.put(p.Admixture_Material__c, p.Price__c);
            adId_to_unit.put(p.Admixture_Material__c, p.Unit__c);
        }

        for (Material__c m : theMaterials) {
            if (adId_to_adPrice.get(m.Id) != null) {
                m.Cost__c = adId_to_adPrice.get(m.Id);
                m.Unit__c = adId_to_unit.get(m.Id);
            }
        }
    }

    public String sieveQueryHelper() {  // ...there are 72 sieve-related fields after all  ;)
        list<String> sieveFields = new list<String>();
        String sieveQueryString = '';

        // build a map of field names to field tokens
        map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get('material__c').getDescribe().Fields.getMap();

        // build a list of all fields that start with 'sieve_'
        if (fieldMap != null) {
            for (Schema.SObjectField sField : fieldMap.values()) {
                Schema.DescribeFieldResult result = sField.getDescribe();
                if (result.getName().toLowercase().startsWith('sieve_')) {
                    sieveFields.add(result.getName());
                }
            }
        }

        for (integer i = 0; i < sieveFields.size(); i++) {
            sieveQueryString += sieveFields[i];
            if (i != sieveFields.size() - 1) {
                sieveQueryString += ', ';
            } else {
                sieveQueryString += ' ';
            }
        }
        return sieveQueryString;
    }

    public PageReference cloneMaterial() {
        material__c cloneMaterial = theMaterial.clone(false, false);
        cloneMaterial.Material_Name__c = theMaterial.Material_Name__c + ' (copy)';
        cloneMaterial.OwnerID = userID;
        cloneMaterial.isActive__c = true;
        cloneMaterial.isLocked__c = false;
        insert cloneMaterial;

        PageReference newocp = new PageReference('/SCCZone/apex/sika_SCCZoneCreateEditMaterialPage?id=' + cloneMaterial.ID);
        newocp.setRedirect(true);
        return newocp;
    }

    public PageReference optionUpdate() {
        PageReference newocp = new PageReference('sika_SCCZoneMaterialsHomePage?state=me&view=' + view);

        if (selectOption == 'comp') {
            newocp = new PageReference('/SCCZone/apex/sika_SCCZoneMaterialsHomePage?state=comp&view=' + view);
        } else {
            newocp = new PageReference('/SCCZone/apex/sika_SCCZoneMaterialsHomePage?state=me&view=' + view);
        }

        newocp.setRedirect(true);
        return newocp;
    }

    public void closePopup() {
        displayPopup = false;
    }

    public void showPopup() {
        theMaterial.Unit_System__c = theUser.System_of_measure__c;
        unitSystem = theUser.System_of_measure__c;
        displayPopup = true;
    }

    public void closePopup2() {
        displayPopup2 = false;
    }

    public void showPopup2() {
        displayPopup2 = true;
    }

    public PageReference createNewMaterial() {
        PageReference newocp;
        if (materialID != null) {
            newocp = new PageReference('/SCCZone/apex/sika_SCCZoneCreateEditMaterialPage?id=' + materialID);
            newocp.setRedirect(true);
            return newocp;
        } else if (typeString == 'None' || typeString == null) {

            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Material Type: You must enter a value'));
            return null;
        } else {
            newocp = new PageReference('/SCCZone/apex/sika_SCCZoneCreateEditMaterialPage' + '?type=' + typeString + '&unit=' + theMaterial.Unit_System__c);
            newocp.setRedirect(true);
            return newocp;
        }
    }

    public PageReference editMaterial() {
        PageReference newocp;

        if (theMaterial.Type__c == 'Admixture') {
            newocp = new PageReference('/SCCZone/apex/sika_SCCZoneAdmixturePricePage?id=' + theMaterial.ID);
        } else {
            newocp = new PageReference('/SCCZone/apex/sika_SCCZoneCreateEditMaterialPage?id=' + theMaterial.ID);
        }
        newocp.setRedirect(true);
        return newocp;
    }

    public PageReference editAdmixture() {
        PageReference newocp = new PageReference('/SCCZone/apex/sika_SCCZoneAdmixturePricePage?id=' + theMaterial.ID);
        newocp.setRedirect(true);
        return newocp;
    }

    public PageReference cancelNewMaterial() {
        PageReference newocp;

        if (materialID != null) {
            newocp = new PageReference('/SCCZone/apex/sika_SCCZoneMaterialDetailPage?id=' + materialID);
        } else {
            newocp = new PageReference('/SCCZone/apex/sika_SCCZoneMaterialsHomePage?view=all');
        }

        newocp.setRedirect(true);
        return newocp;
    }

    // deactivate (don't delete) a material when the "delete" link is clicked.  
    public String deleteThisMaterial { get; set; }

    public PageReference deleteMaterial() {
        list<Material__c> theMat = [SELECT Id, isActive__c FROM Material__c WHERE Id = :deleteThisMaterial Limit 1];

        try {
            theMat[0].isActive__c = false;

            sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = true;  // in case the material is used in a locked mix.  (Update to change isActive value would fail otherwise.)
            sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = true;

            update(theMat[0]);
            PageReference newocp = new PageReference('/SCCZone/apex/sika_SCCZoneMaterialsHomePage?state=' + selectOption + '&view=' + view);
            newocp.setRedirect(true);
            return newocp;

        } catch (DmlException e) {
            // Process exception here
            Apexpages.addMessage(new Apexpages.message(ApexPages.Severity.Error, e.getMessage()));
            return null;
        }
    }

    // deactivate (don't delete) a material when the "delete" link is clicked.
    public PageReference deleteMaterialDetail() {
        try {
            theMaterial.isActive__c = false;

            sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = true;  // in case the material is used in a locked mix.  (Update to change isActive value would fail otherwise.)
            sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = true;
            update(theMaterial);

            PageReference newocp = new PageReference('/SCCZone/apex/sika_SCCZoneMaterialsHomePage?state=me&view=all');
            newocp.setRedirect(true);
            return newocp;

        } catch (DmlException e) {
            // Process exception here
            Apexpages.addMessage(new Apexpages.message(ApexPages.Severity.Error, e.getMessage()));
            return null;
        }
    }

    public PageReference unDeleteMaterial() {
        theMaterial.isActive__c = true;
        try {
            update(theMaterial);
            PageReference newocp = new PageReference('/SCCZone/apex/sika_SCCZoneMaterialDetailPage');
            newocp.getParameters().put('id', theMaterial.Id);
            newocp.setRedirect(true);
            return newocp;

        } catch (DmlException e) {
            // Process exception here
            Apexpages.addMessage(new Apexpages.message(ApexPages.Severity.Error, e.getMessage()));
            return null;
        }
    }

    public list<Mix_Material__c> getMixMaterials() {
        list<Mix_Material__c> theMixMaterials = [
            SELECT Id,
                Mix__r.Id,
                Mix__r.Mix_Name__c,
                Mix__r.Contractor__c,
                Mix__r.Project__c,
                Mix__r.OwnerID,
                Mix__r.CreatedDate,
                Material__r.Id
            FROM Mix_Material__c
            WHERE Material__r.Id = :materialID
            AND Mix__r.OwnerID = :userId
        ];
        if (theMixMaterials.size() == 0) {
            theMixMaterials = new list<Mix_Material__c>();
        }
        return theMixMaterials;
    }

    public PageReference createNewAfterSave() {
        if (typeString == 'None' || typeString == null) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Material Type: You must enter a value'));
            return null;
        }

        else if (unitSystem == 'None' || unitSystem == null) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'System of Measure: You must enter a value'));
            return null;
        } else {
            PageReference newocp = new PageReference('/SCCZone/apex/sika_SCCZoneCreateEditMaterialPage' + '?type=' + typeString + '&unit=' + unitSystem); //
            newocp.setRedirect(true);
            return newocp;
        }
    }

    public String sortField { get; set; }
    private String nameSortDir = 'DESC';  // this will sort the name column alphabetically in ASCending order on the first click.  (This value gets switched to the inverse before the sort is performed each time, so an initial setting of DESC means the first sort will be in ASCending order.)
    private String typeSortDir = 'DESC';
    private String sourceSortDir = 'DESC';
    private String costSortDir = 'ASC';  // seems more intuitive to sort numbers descending initially.
    private String unitSortDir = 'DESC';
    @testVisible private list<Material__c> tempSortList;

    public void tableSort() {
        tempSortList = new list<Material__c>();

        if (view == 'all') {
            tempSortList.addAll(displayList);
            displayList.clear();
            displayList.addAll((list<Material__c>) sika_SCCZoneSortListHelper.sortList(tempSortList, sortField, sortDirTracker()));
        } else if (view == 'ad') {
            tempSortList.addAll(adList);
            adList.clear();
            adList.addAll((list<Material__c>) sika_SCCZoneSortListHelper.sortList(tempSortList, sortField, sortDirTracker()));
        } else if (view == 'cement') {
            tempSortList.addAll(cementList);
            cementList.clear();
            cementList.addAll((list<Material__c>) sika_SCCZoneSortListHelper.sortList(tempSortList, sortField, sortDirTracker()));
        } else if (view == 'sand') {
            tempSortList.addAll(sandList);
            sandList.clear();
            sandList.addAll((list<Material__c>) sika_SCCZoneSortListHelper.sortList(tempSortList, sortField, sortDirTracker()));
        } else if (view == 'scm') {
            tempSortList.addAll(scmList);
            scmList.clear();
            scmList.addAll((list<Material__c>) sika_SCCZoneSortListHelper.sortList(tempSortList, sortField, sortDirTracker()));
        } else if (view == 'coarse') {
            tempSortList.addAll(coarseList);
            coarseList.clear();
            coarseList.addAll((list<Material__c>) sika_SCCZoneSortListHelper.sortList(tempSortList, sortField, sortDirTracker()));
        } else if (view == 'water') {
            tempSortList.addAll(waterList);
            waterList.clear();
            waterList.addAll((list<Material__c>) sika_SCCZoneSortListHelper.sortList(tempSortList, sortField, sortDirTracker()));
        } else if (view == 'other') {
            tempSortList.addAll(otherList);
            otherList.clear();
            otherList.addAll((list<Material__c>) sika_SCCZoneSortListHelper.sortList(tempSortList, sortField, sortDirTracker()));
        }
    }

    public String sortDirTracker() {
        if (sortField == 'Material_Name__c') {
            nameSortDir = sortDir(nameSortDir);
            return nameSortDir;
        } else if (sortField == 'Type__c') {
            typeSortDir = sortDir(typeSortDir);
            return typeSortDir;
        } else if (sortField == 'Source__c') {
            sourceSortDir = sortDir(sourceSortDir);
            return sourceSortDir;
        } else if (sortField == 'Cost__c') {
            costSortDir = sortDir(costSortDir);
            return costSortDir;
        } else if (sortField == 'Unit__c') {
            unitSortDir = sortDir(unitSortDir);
            return unitSortDir;
        }
        return 'ASC';

    }

    private String sortDir(String dir) {
        if (dir == 'ASC') {
            return 'DESC';
        }
        return 'ASC';
    }
}