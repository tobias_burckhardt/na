@isTest(seeAllData=true)

public class awsUpdateVersionTest {

	static ContentVersion testContentVersion;
	public static List<ContentWorkSpace> existingContentWorkSpace{
		get{
			if(existingContentWorkspace == null)
			{
				existingContentWorkspace = [SELECT id FROM ContentWorkspace WHERE name = 'PublicMarketingDocuments' LIMIT 1];
			}
			return existingContentWorkspace;
		}
		set;
	}

	@isTest
	static void updateNewVersion() {
				List<ContentWorkspace> libObj = [SELECT id,Name FROM ContentWorkspace WHERE Name = 'Attachment Archive' LIMIT 1];
		//List<ContentWorkspace> libObj = [SELECT id FROM ContentWorkspace WHERE name = 'PublicMarketingDocuments' LIMIT 1];
		String libId = libObj.get(0).id;

		ContentVersion contentVersion_1 = new ContentVersion(
			Title = 'Penguins',
			PathOnClient = 'Penguins.jpg',
			VersionData = Blob.valueOf('Test Content'),
			Description = 'This is a tet description',
			Document_Type__c = 'Specifications',
			Sub_Type_Level_1__c = 'Guide Specifications',
			Sub_Type_Level_2__c = 'Roofing Specs',
			Sub_Type_Level_3__c = 'Adhered',
			IsMajorVersion = true
		);

		insert contentVersion_1;

		ContentVersion fileVersion = [SELECT Id, Title, Document_Type__c, Sub_Type_Level_1__c, Sub_Type_Level_2__c, Sub_Type_Level_3__c, FileExtension, ContentDocumentId, VersionData, ContentSize FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];

		contentVersion_1 = new ContentVersion(
			Id = fileVersion.Id,
			Title = 'Penguins',
			PathOnClient = 'Penguins.jpg',
			VersionData = Blob.valueOf('Test Content'),
			Description = 'This is a tet description',
			Document_Type__c = 'Specifications',
			Sub_Type_Level_1__c = 'Guide Specifications',
			Sub_Type_Level_2__c = 'Roofing Specs',
			Sub_Type_Level_3__c = 'Green Roof',
			IsMajorVersion = true
		);
		update contentVersion_1;

		system.debug('content document id: ' + fileVersion.contentDocumentId);
		ContentDocumentLink contentlink = new ContentDocumentLink();
		contentlink.LinkedEntityId = libId;
		contentlink.contentdocumentid=fileVersion.contentDocumentId;
		contentlink.ShareType= 'I';


		//if ( !Test.isRunningTest() ) {
			 insert contentlink;
		//}

		List<ContentDocumentLink> files=[SELECT Id, ContentDocumentId, ContentDocument.LatestPublishedVersionId, LinkedEntityId, ContentDocument.Title
										 from ContentDocumentLink where ContentDocumentId= :fileVersion.ContentDocumentId];
		System.assertEquals(1, files.size());
	}

	@isTest
	static void deleteContentDocument(){
		List<ContentWorkspace> existingContentWorkspace = [SELECT id,Name FROM ContentWorkspace WHERE Name = 'Attachment Archive' LIMIT 1];

		testContentVersion  = createContentVersions(1,'Test Content',false)[0];
		testContentVersion.FirstPublishLocationId = existingContentWorkspace[0].Id;
		insert testContentVersion;

		testContentVersion = [Select ContentDocumentId,FirstPublishLocationId From ContentVersion Where Id =: testContentVersion.Id];
		ContentDocument doc = [Select Id, parentId, title from ContentDocument Where Id =: testContentVersion.ContentDocumentId];

		Test.startTest();
		delete doc;
		Test.stopTest();

		List<Content_Header__c> contentHeader = [Select Id From Content_Header__c Where Content_Document__c =: testContentVersion.contentDocumentId];
		System.assert(contentHeader.isEmpty());
	}

	public static List<ContentVersion> createContentVersions(Integer quantity, String title, Boolean doInsert) {
		List<ContentVersion> documents = new List<ContentVersion>();
		for (Integer i = 0; i < quantity; i++) {
			ContentVersion document = new ContentVersion(
				Title = title,
				VersionData= Blob.valueOf('Test Content'),
				PathOnClient= 'Penguins.jpg'
			);
			documents.add(document);
		}

		if (doInsert) {
			insert documents;
		}

		return documents;
	}


}