public class ExportQuoteLineItems{

  public Integer count{get;set;}
  public list<QuoteLineItem> quotelinelist{get;set;}
  public ExportQuoteLineItems(ApexPages.StandardController controller){
   	quotelinelist = new list<QuoteLineItem>();
   	quotelinelist = [select id,Quote.name,Product2.name,LineNumber,Quoteid,Product2Id,Article__c,Quantity,ListPrice,UnitPrice,Scale_Quantity_1__c,Scale_Quantity_2__c,Scale_Quantity_3__c,Scale_Quantity_4__c,Scale_Rate_2__c,Scale_Rate_3__c,Scale_Rate_4__c,Base_Unit__c from QuoteLineItem where Quoteid =: ApexPages.currentPage().getParameters().get('id')]; 
  }
   public pagereference opensame(){
   	count = 0;
	//count--;
   	pagereference pg = new pagereference('/'+ApexPages.currentPage().getParameters().get('Id'));
   	return null;
   }
}