@isTest
private class EmailRestrictUpDelTriggerTest {

    //Creates and returns the specified number of email messages related to the given case
    private static List<EmailMessage> createTestEmails(Id caseId, Integer num) {
        List<EmailMessage> emails = new List<EmailMessage>();
        for (Integer i = 0; i < num; i++) {
            EmailMessage em = new EmailMessage(Subject='Test Subject ' + i, ParentId=caseId);
            emails.add(em);
        }
        insert emails;
        return emails;
    }

    @testSetup
    static void setUpTests() {
        RestrictedCaseTestHelper.createCustomSettings();
    }

    @isTest
    static void testDeleteUnrestrictedEmail() {
        //Trying to delete an email message related to a case with an unrestricted record type should succeed
        Case testCase = RestrictedCaseTestHelper.createTestCase(false);
        List<EmailMessage> testEmails = createTestEmails(testCase.Id, 1);

        Test.startTest();
        Database.DeleteResult deleteResult = Database.delete(testEmails[0], false);
        System.assert(deleteResult.isSuccess());
        Test.stopTest();
    }

    @isTest
    static void testDeleteEmailRestricted() {
        //Trying to delete an email message related to a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<EmailMessage> testEmails = createTestEmails(testCase.Id, 1);

        Test.startTest();
        Database.DeleteResult deleteResult = Database.delete(testEmails[0], false);
        System.assert(!deleteResult.isSuccess());
        System.assertEquals(1, deleteResult.getErrors().size());
        System.assertEquals(System.Label.Restricted_Object_Delete, deleteResult.getErrors()[0].getMessage());
        Test.stopTest();
    }

    @isTest
    static void testBulkDeleteEmailsRestricted() {
        //Trying to delete email messages related to a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<EmailMessage> testEmails = createTestEmails(testCase.Id, 10);

        Test.startTest();
        //Try to delete all tasks
        List<Database.DeleteResult> results = Database.delete(testEmails, false);
        for (Database.DeleteResult res : results) {
            System.assert(!res.isSuccess());
            System.assertEquals(1, res.getErrors().size());
            System.assertEquals(System.Label.Restricted_Object_Delete, res.getErrors()[0].getMessage());
        }
        Test.stopTest();
    }
}