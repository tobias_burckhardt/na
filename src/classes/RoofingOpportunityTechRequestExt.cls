public class RoofingOpportunityTechRequestExt {
	
	public Opportunity opp;

	public PageReference flowFinishLocation {
		get {
			Set<Id> communityProfileIds = new Set<Id>(Pluck.ids(UserDao.getUserPartnerProfileIds()));
			String url = '/' + opp.Id;
            if (communityProfileIds.contains(UserInfo.getProfileId())) {
                url = '/PTS' + url;
            }
			return new PageReference(url);
		}
	}
	
	public RoofingOpportunityTechRequestExt (ApexPages.StandardController std) {
		this.opp = (Opportunity)std.getRecord();
	}
	
	public Boolean doFlow{
		get{
			if(doFlow == null)
				doFlow = false;
				
			return doFlow;
		} set;
	}
	
	public Boolean isAccepted{
		get{
			if(isAccepted == null)
				isAccepted = false;
				
			return isAccepted;
		} set;
	}
	
	
	
	public PageReference proceed() {
		if(isAccepted == true) {
			doFlow = true;
			save();
		}	
		return null;
	}
	
	public PageReference save() {
		try {
			update opp;
			return null;
		}
		
		catch(DMLException e) {
			ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'There was an error updating the opportunity: ' + e);
		}
		
		return null;
	}
}