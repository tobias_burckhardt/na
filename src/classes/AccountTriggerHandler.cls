public with sharing class AccountTriggerHandler {

    public static Boolean onAfterUpdate_FirstRun = true;
    public static Boolean onAfterInsert_FirstRun = true;
    public static Boolean skipTrigger = false;

    public  Boolean isTriggerContext { get; private set; }

    private Integer batchSize = 0;

    private Profile userProfile {
        get {
            if (userProfile == null) {
                Id userProfileId = UserInfo.getProfileId();
                userProfile = [SELECT UserLicense.Name FROM Profile WHERE Id = :userProfileId];
            }
            return userProfile;
        }

        private set;
    }

    private Boolean isInternalUser {
        get {
            return !userProfile.UserLicense.Name.contains('Community');
        }
    }

    /**
     * @param Boolean isTriggerContextParam - indicates whether the handler is being executed within a trigger context
     * @param Integer batchSizeParam - indicates the number of records to be processed on the thread
     */
    public AccountTriggerHandler(Boolean isTriggerContextParam, Integer batchSizeParam) {
        isTriggerContext = (isTriggerContextParam == null) ? false : isTriggerContextParam;
        batchSize = batchSizeParam;
    }

    public void onAfterInsert(List<Account> newList, map<Id, Account> newMap) {
        if (onAfterInsert_FirstRun) {
            setAccountSharing(newList, newMap);
            onAfterUpdate_FirstRun = false;
        }

        if (isInternalUser) {
            addMemberRecordsAfterInsert(newList, newMap, null, true, false);
        }
    }

    public void onAfterUpdate(List<Account> newList, Map<Id, Account> newAccountsById,
            Map<Id, Account> oldMap) {
        if (onAfterUpdate_FirstRun && onAfterInsert_FirstRun) {
            onAfterUpdate_FirstRun = false;
        }

        if (isInternalUser) {
            addMemberRecordsAfterInsert(newList, newAccountsById, oldMap, false, true);
        }
    }

    public void onBeforeUpdate(List<Account> newAccounts, Map<Id, Account> newAccountsById,
            Map<Id, Account> oldAccountsById) {
        if (isInternalUser) {
            addMemberRecordsBeforeUpdate(newAccounts, newAccountsById, oldAccountsById);
        }
    }

    /**
     * Method is used to add members records after Insert or after update
     */
    private void addMemberRecordsAfterInsert(List<Account> newList, Map<Id, Account> newMap,
            Map<Id, Account> oldMap, Boolean isInsert, Boolean isUpdate) {

        if (skipTrigger) {
            return;
        }

        List<AccountTeamMember> members = new List<AccountTeamMember>();
        AccountTeamMember member = null;
        Map<Id, List<AccountTeamMember>> accMembersMapTemp = AccountTriggerHelper.accMembersMap;

        if (accMembersMapTemp == null) {
            accMembersMapTemp = new Map<Id, List<AccountTeamMember>>();
        }

        String userId = UserInfo.getUserId();
        Set<Id> accIds = new Set<Id>();

        for (Account acc : newList) {
            if (acc.Id != null) {
                accIds.add(acc.Id);
            }
        }

        List<List<AccountTeamMember>> listOfmembersExisting = accMembersMapTemp.values();
        Map<String, String> userIdRoleMap = new Map<String, String>();

        for (List<AccountTeamMember> membersExisting : listOfmembersExisting) {
            for (AccountTeamMember atm : membersExisting) {
                if (userIdRoleMap.get(atm.AccountId + '' + atm.UserId) == null) {
                    userIdRoleMap.put(atm.AccountId + '' + atm.UserId, atm.TeamMemberRole);
                }
            }
        }

        //to get users as Active or Inactive
        Map<Id, Boolean> mapUserIdIsActive = getMapUserIdAndIsActive(newList, oldMap);

        Savepoint sp = Database.setSavepoint();

        for (Account acc : newList) {
            member = new AccountTeamMember();
            member.AccountId = acc.Id;
            member.UserId = UserInfo.getUserId();

            if (isInsert || (isUpdate && acc.OwnerId != oldMap.get(acc.Id).OwnerId)) {
                if (accMembersMapTemp != null && accMembersMapTemp.get(acc.Id) != null) {
                    for (AccountTeamMember atm : accMembersMapTemp.get(acc.Id)) {
                        if (mapUserIdIsActive.get(atm.UserId)) {
                            members.add(atm);
                        }
                    }
                }
            }

            if (userId.startsWith('005')) {
                if (isInsert) {
                    member.TeamMemberRole = 'Account Created';
                    if (mapUserIdIsActive.containsKey(member.UserId) && mapUserIdIsActive.get(member.UserId)) {
                        members.add(member);
                    }
                } else if (isUpdate && acc.OwnerId != oldMap.get(acc.Id).OwnerId && acc.OwnerId != UserInfo.getUserId()) {
                    member.TeamMemberRole = 'Account Edited';

                    if (userIdRoleMap.get(acc.Id + '' + member.UserId) != null) {
                        member.TeamMemberRole = userIdRoleMap.get(acc.Id + '' + member.UserId);
                    }

                    if (mapUserIdIsActive.containsKey(member.UserId) && mapUserIdIsActive.get(member.UserId)) {
                        members.add(member);
                    }

                    AccountTeamMember member1 = new AccountTeamMember();
                    member1.AccountId = acc.Id;
                    member1.UserId = acc.OwnerId;
                    member1.TeamMemberRole = 'Account Owner';

                    if (mapUserIdIsActive.containsKey(acc.OwnerId) && mapUserIdIsActive.get(acc.OwnerId)) {
                        members.add(member1);
                    }
                } else if (acc.OwnerId == UserInfo.getUserId()) {
                    member.TeamMemberRole = 'Account Owner';
                    if (mapUserIdIsActive.containsKey(oldMap.get(acc.Id).OwnerId) && mapUserIdIsActive.get(oldMap.get(acc.Id).OwnerId)) {
                        members.add(member);
                    }
                } else if (acc.OwnerId != UserInfo.getUserId()) {
                    AccountTeamMember member1 = new AccountTeamMember();
                    if (mapUserIdIsActive.containsKey(acc.OwnerId) && mapUserIdIsActive.get(acc.OwnerId)) {
                        member1.AccountId = acc.Id;
                        member1.UserId = acc.OwnerId;
                        member1.TeamMemberRole = 'Account Owner';
                        members.add(member1);
                    }

                    if (mapUserIdIsActive.containsKey(member.UserId) && mapUserIdIsActive.get(member.UserId)) {
                        members.add(member);
                    }
                    
                    member.TeamMemberRole = 'Account Edited';

                    if (userIdRoleMap.get(acc.Id + '' + member.UserId) != null) {
                        member.TeamMemberRole = userIdRoleMap.get(acc.Id + '' + member.UserId);
                    }
                }
            }
        }

        //To Insert Account Team Members
        try {
            AccountServicesWithoutSharing.insertAccountTeamMembers(members);
        } catch (DmlException e) {
            String errorTemplate = 'Error inserting Account Team Member {0}: {1}';
            for (Integer i = 0; i < e.getNumDml(); i++) {
                Id accountId = members.get(e.getDmlIndex(i)).AccountId;
                List<String> params = new List<String> {e.getDmlId(i), e.getDmlMessage(i)};
                String error = String.format(errorTemplate, params);
                newMap.get(accountId).addError(error);

            }
            Database.RollBack(sp);
            throw e;

        }

        //To update the Account Sharing Rules
        AccountShare[] accShares = [
            Select UserOrGroupId, RowCause, AccountId, AccountAccessLevel, Id
            From AccountShare
            where AccountId in :newMap.keySet()
            and rowcause = 'Team'
            and AccountAccessLevel = 'Read'
        ];

        for (AccountShare accshare : accShares) {
            accshare.AccountAccessLevel = 'Edit';
        }

        if (accShares.size() > 0) {
            try {
                AccountServicesWithoutSharing.updateAccountShares(accShares);
            } catch (DmlException e) {
                String errorTemplate = 'Error updating Account Team Member {0}: {1}';
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    Id accountId = accShares.get(e.getDmlIndex(i)).AccountId;
                    List<String> params = new List<String> {e.getDmlId(i), e.getDmlMessage(i)};
                    String error = String.format(errorTemplate, params);
                    newMap.get(accountId).addError(error);

                }
                Database.RollBack(sp);
                throw e;
            }
        }
    }

    /**
     * Method is used to add members records before Update
     */
    private void addMemberRecordsBeforeUpdate(List<Account> newList, map<Id, Account> newMap,
            map<Id, Account> oldMap) {

        Set<Id> accIds = new Set<Id>();

        for (Account a : newList) {
            accIds.add(a.Id);
        }

        List<AccountTeamMember> members = [
            SELECT Id, AccountId , UserId, TeamMemberRole
            FROM AccountTeamMember
            WHERE AccountId in : accIds
        ];

        if (members.size() > 0 && !members.isEmpty()) {
            List<AccountTeamMember> accMembers = members.deepClone();
            Map<Id, List<AccountTeamMember>> accMembersMapTemp =
                new Map<Id, List<AccountTeamMember>>();

            for (AccountTeamMember atm : accMembers) {
                if (accMembersMapTemp.get(atm.AccountId) == null) {
                    accMembersMapTemp.put(atm.AccountId, new List<AccountTeamMember>());

                }
                accMembersMapTemp.get(atm.AccountId).add(atm);

            }

            for (AccountTeamMember atm : accMembers) {
                if (newMap.get(atm.AccountId).OwnerId != oldMap.get(atm.AccountId).OwnerId) {
                    if (atm.TeamMemberRole == 'Account Owner') {
                        atm.TeamMemberRole = 'Former Owner';
                    }
                }
            }

            AccountTriggerHelper.accMembersMap = accMembersMapTemp;
        }
    }

    /**
     * Method is used to get User as Active or Inactive
     */
    private Map<Id, Boolean> getMapUserIdAndIsActive(List<Account> newList,
            Map<Id, Account> oldAccountsById) {

        Map<Id, List<AccountTeamMember>> accMembersMapTemp = AccountTriggerHelper.accMembersMap;
        Set<Id> sUserIds = new Set<Id>();
        Map<Id, Boolean> mapUserIdIsActive = new map<Id, Boolean>();

        if (accMembersMapTemp == null) {
            accMembersMapTemp = new Map<Id, List<AccountTeamMember>>();
        }

        for (Account acc : newList) {
            sUserIds.add(acc.OwnerId);
            
            if (oldAccountsById != null) {
                sUserIds.add(oldAccountsById.get(acc.Id).OwnerId);
            }
            
            if (accMembersMapTemp.containsKey(acc.Id)) {
                for ( AccountTeamMember atm : accMembersMapTemp.get(acc.Id)) {
                    sUserIds.add(atm.UserId);
                }
            }
        }

        List<User> usr = new List<user> ();
        usr = [select Id, IsActive from user where Id in : sUserIds];

        for (user u : usr) {
            mapUserIdIsActive.put(u.id, u.IsActive);
        }
        return mapUserIdIsActive;
    }

    /**
     * Method is used to add members records before Updation
     */
    private void setAccountSharing(List<Account> newAccts, map<Id, Account> newMap) {
        Map<Id, AccountShare> newAccShare = new Map<Id, AccountShare>();
        Map<Id, AccountShare> newFinalAccShare = new Map<Id, AccountShare>();

        for (Account acc : newAccts) {
            if (acc.Id != null) {
                newAccShare.put(acc.Id, new AccountShare());
            }
        }

        if (newAccShare != null) {
            for (User usr : [
                        SELECT AccountId FROM User
                        WHERE AccountId IN :newAccShare.keySet() AND IsActive = true
                    ]) {

                AccountShare accShare = new AccountShare();
                accShare.AccountId = usr.AccountId;
                accShare.UserOrGroupId = usr.Id;
                accShare.AccountAccessLevel = 'Edit';
                accShare.RowCause = Schema.AccountShare.RowCause.Manual;

                newFinalAccShare.put(usr.AccountId, accShare);

            }

            // add to insertable datastructure
            List<AccountShare> accShareToInsert = new List<AccountShare>();
            for (Id newAccShareId : newFinalAccShare.keySet()) {
                accShareToInsert.add(newFinalAccShare.get(newAccShareId));
            }

            if (!accShareToInsert.isEmpty()) {
                try {
                    AccountServicesWithoutSharing.insertAccountShares(accShareToInsert);
                } catch (DmlException e) {
                    String errorTemplate = 'Error updating Account Team Member {0}: {1}';
                    for (Integer i = 0; i < e.getNumDml(); i++) {
                        Id accountId = accShareToInsert.get(e.getDmlIndex(i)).AccountId;
                        List<String> params = new List<String> {e.getDmlId(i), e.getDmlMessage(i)};
                        String error = String.format(errorTemplate, params);
                        newMap.get(accountId).addError(error);
                    }
                }
            }
        }
    }
}