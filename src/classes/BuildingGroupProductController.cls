public with sharing class BuildingGroupProductController {
	public Id buildingGroupId {get;set;}
	public String categoryHierarchy {get;set;}
	public User currentUser {get;set;}
	public List<Apttus_Config2__ClassificationName__c> categories {get;set;}
	public List<Apttus_Config2__ClassificationHierarchy__c> classifications {get;set;}
	public List<Apttus_Config2__ProductClassification__c> productClassifications {get;set;}

	public List<SelectOption> categorySO {get;set;}
	public List<SelectOption> classificationSO {get;set;}
	public List<SelectOption> productClassificationSO {get;set;}

	public Id selectedCategory {get;set;}
	public Id selectedClassification {get;set;}
	public Id selectedProduct {get;set;}

	public BuildingGroupProductController() {
		buildingGroupId 	= ApexPages.currentPage().getParameters().get('id');
		categoryHierarchy 	= ApexPages.currentPage().getParameters().get('cat');
		currentUser 		= [SELECT Id, Name, ProfileId, Profile.Name FROM User WHERE Id =: UserInfo.getUserId()];
		//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'buildingGroupId: ' + buildingGroupId));
		//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'categoryHierarchy: ' + categoryHierarchy));
		if(string.isNotBlank(categoryHierarchy)){
			selectedCategory = categoryHierarchy;
		}

		GetCategory();
		GetClassification();
	}

	/*
	https://sikana--pts.cs19.my.salesforce.com/01I290000008gui?setupid=CustomObjects
	Apttus_Config2__ClassificationName__c
	//Apttus_Config2__Active__c, Apttus_Config2__HierarchyLabel__c, Apttus_Config2__GuidePage__c, Apttus_Config2__Type__c
	*/
	public void GetCategory(){
		categories = new List<Apttus_Config2__ClassificationName__c>();
		categorySO = new List<SelectOption>();
		categorySO.add(new SelectOption('', 'None'));

		for(Apttus_Config2__ClassificationName__c cat :[SELECT Id, Name
			FROM Apttus_Config2__ClassificationName__c
			ORDER BY Name ASC]){

			categories.add(cat);
			categorySO.add(new SelectOption(cat.Id, cat.Name));
		}
		//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'selectedCategory: ' + selectedCategory));
		//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'selectedClassification: ' + selectedClassification));
		//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'selectedProduct: ' + selectedProduct));
	}

	/*
	https://sikana--pts.cs19.my.salesforce.com/01I290000008guh?setupid=CustomObjects
	Apttus_Config2__ClassificationHierarchy__c
	*/
	public void GetClassification(){
		classifications 	= new List<Apttus_Config2__ClassificationHierarchy__c>();
		classificationSO 	= new List<SelectOption>();
		classificationSO.add(new SelectOption('', 'None'));
		String cat;
		for(SelectOption option : categorySO){
			system.debug(option);
			system.debug(option.getValue());
			system.debug(selectedCategory);
			if(String.isNotBlank(option.getValue()) && option.getValue() == selectedCategory){
				cat = option.getLabel();
				break;
			}
		}
		List<String> classificationsToRemove = new List<String>{'Accessory|'+cat,
																	'Custom Parts|'+cat,
																	'Fees|'+cat,
																	'Flashing Adhesives|'+cat,
																	'Flashing Membrane|'+cat,
																	'Flashing Metals|'+cat,
																	'Flashing|'+cat,
																	'Prefab Parts|'+cat,
																	'Recycle Candidate|'+cat,
																	'Sealants|'+cat,
																	'Tools|'+cat,
																	'Walkway|'+cat};
		if(string.isNotBlank(selectedCategory)){
			for(Apttus_Config2__ClassificationHierarchy__c cl :[SELECT Id, Name
				FROM Apttus_Config2__ClassificationHierarchy__c
				WHERE Apttus_Config2__HierarchyId__c = :selectedCategory
				AND (NOT Name LIKE '%Warranty%')
				AND Name NOT IN :classificationsToRemove
				ORDER BY Name ASC]){

					classifications.add(cl);
					classificationSO.add(new SelectOption(cl.Id, cl.Name));
			}
		}
		//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'selectedCategory: ' + selectedCategory));
		//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'selectedClassification: ' + selectedClassification));
		//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'selectedProduct: ' + selectedProduct));
	}

	/*
	https://sikana--pts.cs19.my.salesforce.com/01I290000008gw7?setupid=CustomObjects
	https://sikana--pts.cs19.my.salesforce.com/p/setup/layout/LayoutFieldList?type=Product2&setupid=CustomObjects
	Apttus_Config2__ProductClassification__c
	Apttus_Config2__ProductClassification__r.Apttus_Config2__ProductId__c
	*/
	public void GetProductClassifications(){
		productClassifications 	= new List<Apttus_Config2__ProductClassification__c>();
		productClassificationSO = new List<SelectOption>();
		productClassificationSO.add(new SelectOption('', 'None'));


		if(currentUser.Profile.Name == 'PTS Partner Community'){
			for (Apttus_Config2__ProductClassification__c pcl : [
				SELECT Id, Name, Product_Name__c, Apttus_Config2__ProductId__c,
					   Apttus_Config2__ProductId__r.APTS_Roofing_Description__c
				FROM Apttus_Config2__ProductClassification__c
				WHERE Apttus_Config2__ClassificationId__c = :selectedClassification
				AND Apttus_Config2__ProductId__r.APTS_Applicator_Active__c = true
				AND Apttus_Config2__ProductId__r.NOAActive__c = true
				ORDER BY Product_Name__c ASC
			]) {
				productClassifications.add(pcl);
				productClassificationSO.add(new SelectOption(pcl.Apttus_Config2__ProductId__c,
					pcl.Apttus_Config2__ProductId__r.APTS_Roofing_Description__c));
			}
		}
		else{
			for (Apttus_Config2__ProductClassification__c pcl : [
				SELECT Id, Name, Product_Name__c, Apttus_Config2__ProductId__c,
					   Apttus_Config2__ProductId__r.APTS_Roofing_Description__c
				FROM Apttus_Config2__ProductClassification__c
				WHERE Apttus_Config2__ClassificationId__c = :selectedClassification
				AND Apttus_Config2__ProductId__r.NOAActive__c = true
				ORDER BY Product_Name__c ASC
			]){
				productClassifications.add(pcl);
				productClassificationSO.add(new SelectOption(pcl.Apttus_Config2__ProductId__c,
					pcl.Apttus_Config2__ProductId__r.APTS_Roofing_Description__c));
			}
		}
	}

	public pageReference Save(){
		try{
			Product2 tempProd = new Product2();
			for(Product2 p2 : [SELECT Id, Attachment_Group__c FROM Product2 WHERE Id =:selectedProduct]){
				tempProd = p2;
			}

			Integer count = [
	            SELECT Id
	            FROM Building_Group_Product__c
	            WHERE Building_Group__c=:buildingGroupId
	        ].size();

			Building_Group_Product__c buildingGroupProduct 	= new Building_Group_Product__c();
			buildingGroupProduct.Building_Group__c 			= buildingGroupId;
			buildingGroupProduct.Category_Hierarchy__c		= selectedClassification;
			buildingGroupProduct.Product_lookup__c 			= selectedProduct;
			buildingGroupProduct.Attachment_Group__c 		= tempProd.Attachment_Group__c;
			buildingGroupProduct.Sort_Order__c				= count;
			insert buildingGroupProduct;

			if(string.isBlank(categoryHierarchy) && string.isNotBlank(selectedCategory)){
				Building_Group__c bgToUpdate = new Building_Group__c(Id=buildingGroupId, Category__c=selectedCategory, Category_Hierarchy__c=selectedClassification);
				update bgToUpdate;
			}

			PageReference pageRef = new PageReference('/apex/BuildingGroupProducts');
			pageRef.setRedirect(true);
			pageRef.getParameters().put('id',buildingGroupId);
			return pageRef;
		}
		catch (DmlException dmle){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'DmlException: ' + dmle.getMessage()));
		}
		return null;
		//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'selectedCategory: ' + selectedCategory));
		//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'selectedClassification: ' + selectedClassification));
		//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'selectedProduct: ' + selectedProduct));
	}

	public PageReference Cancel(){
		PageReference pageRef = new PageReference('/apex/BuildingGroupProducts');
		pageRef.setRedirect(true);
		pageRef.getParameters().put('id',buildingGroupId);
		return pageRef;
	}
}