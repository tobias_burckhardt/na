@isTest
private class sika_PTSValidationHelperTest {
	
	@isTest static void testEmailValidation(){

		system.assertEquals(sika_PTSValidationHelper.validateEmail(''), false);
		system.assertEquals(sika_PTSValidationHelper.validateEmail('s'), false);
		system.assertEquals(sika_PTSValidationHelper.validateEmail('s@'), false);
		system.assertEquals(sika_PTSValidationHelper.validateEmail('s@steinf'), false);
		system.assertEquals(sika_PTSValidationHelper.validateEmail('@mail.com'), false);
		system.assertEquals(sika_PTSValidationHelper.validateEmail('mail@.com'), false);
		system.assertEquals(sika_PTSValidationHelper.validateEmail('@.'), false);

		system.assertEquals(sika_PTSValidationHelper.validateEmail('estesmike@us.sika.com'), true);
		system.assertEquals(sika_PTSValidationHelper.validateEmail('estesmike@us.sika.fulltrain'), true);

	}

	@isTest static void testvalidatePriceField(){

		system.assertEquals(sika_PTSValidationHelper.validatePriceField(''), false);
		system.assertEquals(sika_PTSValidationHelper.validatePriceField('abc'), false);

		system.assertEquals(sika_PTSValidationHelper.validatePriceField('3'), true);
		system.assertEquals(sika_PTSValidationHelper.validatePriceField('3.00'), true);

	}

}