@isTest
private class PickApproversModelTest {
	static final Integer NUM_OF_APPROVERS = 8; // Adjust this if value in PickApproversModel changes

	static testMethod void bigFlowTest() {
		PickApproversModel testModel = new PickApproversModel();
		Integer nextPos = testModel.addAndGetNextUp(null);
		System.assertEquals(1, nextPos, 'The first available position should be 1 after 0');
		Integer prevPos = nextPos;
		nextPos = testModel.addAndGetNextUp(null);
		System.assertEquals(prevPos+1, nextPos, 'Add function did not increment nextUp properly');

		prevPos = nextPos;
		nextPos = testModel.addAndGetNextUp(null);
		System.assertEquals(prevPos+1, nextPos, 'Add function did not increment nextUp properly');

		prevPos = nextPos;
		nextPos = testModel.addAndGetNextUp(null);
		System.assertEquals(prevPos+1, nextPos, 'Add function did not increment nextUp properly');

		prevPos = nextPos;
		nextPos = testModel.addAndGetNextUp(null);
		System.assertEquals(prevPos+1, nextPos, 'Add function did not increment nextUp properly');

		prevPos = nextPos;
		nextPos = testModel.addAndGetNextUp(null);
		System.assertEquals(prevPos+1, nextPos, 'Add function did not increment nextUp properly');

		prevPos = nextPos;
		nextPos = testModel.addAndGetNextUp(null);
		System.assertEquals(prevPos+1, nextPos, 'Add function did not increment nextUp properly');

		prevPos = nextPos;
		nextPos = testModel.addAndGetNextUp(null);
		System.assertEquals(-1, nextPos, 'Add function did not increment nextUp properly. Should be out of nextUps');

		try {
			testModel.addAndGetNextUp(null);
			System.assert(false, 'If no space is available, an exception should be thrown');
		}
		catch (PickApproversModel.PickApproversException pae) {}

		Integer removedPos = testModel.remove(0).pos;
		System.assertEquals(0, removedPos, 'Remove function did not remove object properly');
		System.assertEquals(0, testModel.getNextUp(), 'Next up was not determined properly');

		removedPos = testModel.remove(1).pos;
		System.assertEquals(1, removedPos, 'Remove function did not remove object properly');
		System.assertEquals(0, testModel.getNextUp(), 'Next up was not determined properly');

		removedPos = testModel.remove(2).pos;
		System.assertEquals(2, removedPos, 'Remove function did not remove object properly');
		System.assertEquals(0, testModel.getNextUp(), 'Next up was not determined properly');

		removedPos = testModel.remove(3).pos;
		System.assertEquals(3, removedPos, 'Remove function did not remove object properly');
		System.assertEquals(0, testModel.getNextUp(), 'Next up was not determined properly');

		removedPos = testModel.remove(4).pos;
		System.assertEquals(4, removedPos, 'Remove function did not remove object properly');
		System.assertEquals(0, testModel.getNextUp(), 'Next up was not determined properly');

		removedPos = testModel.remove(5).pos;
		System.assertEquals(5, removedPos, 'Remove function did not remove object properly');
		System.assertEquals(0, testModel.getNextUp(), 'Next up was not determined properly');

		removedPos = testModel.remove(6).pos;
		System.assertEquals(6, removedPos, 'Remove function did not remove object properly');
		System.assertEquals(0, testModel.getNextUp(), 'Next up was not determined properly');

		removedPos = testModel.remove(7).pos;
		System.assertEquals(7, removedPos, 'Remove function did not remove object properly');
		System.assertEquals(0, testModel.getNextUp(), 'Next up was not determined properly');

		try {
			testModel.remove(0);
			System.assert(false, 'Already removed position cannot be removed again');
		}
		catch (PickApproversModel.PickApproversException pae) {}

		System.assert(testModel.getApprovers().isEmpty(), 'Removing all approvers did not result in an empty list');
	}

	static testMethod void testLoadingExistingApprovers_FullInputAndRemoveOne() {
		List<User> testUsers5 = TestingUtils.createMultipleTestUsers(8, 'userTestPAM', 'System Administrator', true);
		Map<Integer, User> testInput = generateInputMap(testUsers5, new List<Integer>{0,1,2,3,4,5,6,7});
		Test.startTest();
		PickApproversModel testModel = new PickApproversModel(testInput);
		testModel.remove(4);
		Test.stopTest();
		System.assertEquals(7, testModel.getApprovers().size(), 'Constructing with 8 users with 1 removed results in 7');
		System.assertEquals(4, testModel.getNextUp(), 'Next one available is the last one removed');
	}

	static testMethod void testLoadingExistingApprovers_EmptyInput() {
		Map<Integer, User> testInput = new Map<Integer, User>();
		Test.startTest();
		PickApproversModel testModel = new PickApproversModel(testInput);
		Test.stopTest();
		System.assert(testModel.getApprovers().isEmpty(), 'Constructing with no users did not result in an empty list');
		System.assertEquals(0, testModel.getNextUp(), 'Constructing with no users did not result in the expected nextUp value');
	}

	static testMethod void testLoadingExistingApprovers_FullInput() {
		List<User> testUsers5 = TestingUtils.createMultipleTestUsers(8, 'userTestPAM', 'System Administrator', true);
		Map<Integer, User> testInput = generateInputMap(testUsers5, new List<Integer>{0,1,2,3,4,5,6,7});
		Test.startTest();
		PickApproversModel testModel = new PickApproversModel(testInput);
		Test.stopTest();
		System.assertEquals(8, testModel.getApprovers().size(), 'Constructing with 8 users did not result in a list of 8 approvers');
		System.assertEquals(-1, testModel.getNextUp(), 'Constructing with 8 users did not result in full state');
	}

	static testMethod void testLoadingExistingApprovers_HalfInput() {
		List<User> testUsers5 = TestingUtils.createMultipleTestUsers(5, 'userTestPAM', 'System Administrator', true);
		Map<Integer, User> testInput = generateInputMap(testUsers5, new List<Integer>{0,3,4,5,6});
		Test.startTest();
		PickApproversModel testModel = new PickApproversModel(testInput);
		Test.stopTest();
		System.assertEquals(5, testModel.getApprovers().size(), 'Constructing with 8 users did not result in a list of 8 approvers');
		Set<Integer> acutalPositions = new Set<Integer>();
		for (PickApproversModel.ApproverWrapper wrapper : testModel.getApprovers()) {
			acutalPositions.add(wrapper.pos);
		}
		System.assertEquals(new Set<Integer>{0,3,4,5,6}, acutalPositions, 'Inputted users were not put into the right positions');
		System.assertEquals(1, testModel.getNextUp(), 'Constructing with given input did not result in the proper nextUp vaule');
	}

	static testMethod void testRefresh() {
		List<User> testUsers5 = TestingUtils.createMultipleTestUsers(5, 'userTestPAM', 'System Administrator', true);
		Map<Integer, User> testInput = generateInputMap(testUsers5, new List<Integer>{0,3,4,5,6});
		Test.startTest();
		PickApproversModel testModel = new PickApproversModel(testInput);
		testModel.approvers[0].pos = 1;
		testModel.refresh();
		Test.stopTest();
		System.assertEquals(5, testModel.getApprovers().size(), 'Constructing with 5 users did not result in a list of 5 approvers');
		Set<Integer> acutalPositions = new Set<Integer>();
		for (PickApproversModel.ApproverWrapper wrapper : testModel.getApprovers()) {
			acutalPositions.add(wrapper.pos);
		}
		System.assertEquals(new Set<Integer>{1,3,4,5,6}, acutalPositions, 'Inputted users were not put into the right positions');
		System.assertEquals(0, testModel.getNextUp(), 'Refreshing with given input did not result in the proper nextUp vaule');
	}

	static testMethod void testRefreshWithDuplicate() {
		List<User> testUsers5 = TestingUtils.createMultipleTestUsers(5, 'userTestPAM', 'System Administrator', true);
		Map<Integer, User> testInput = generateInputMap(testUsers5, new List<Integer>{0,3,4,5,6});
		Test.startTest();
		PickApproversModel testModel = new PickApproversModel(testInput);
		testModel.approvers[0].pos = 3;
		testModel.refresh();
		Test.stopTest();
		System.assertEquals(5, testModel.getApprovers().size(), 'Constructing with 5 users did not result in a list of 5 approvers. Duplicate numbers should be ignored.');
		Set<Integer> acutalPositions = new Set<Integer>();
		for (PickApproversModel.ApproverWrapper wrapper : testModel.getApprovers()) {
			acutalPositions.add(wrapper.pos);
		}
		System.assertEquals(new Set<Integer>{3,4,5,6}, acutalPositions, 'Inputted users were not put into the right positions');
		System.assertEquals(0, testModel.getNextUp(), 'Refreshing with given input did not result in the proper nextUp vaule');
	}

	static testMethod void testGetOuputMap() {
		List<User> testUsers5 = TestingUtils.createMultipleTestUsers(5, 'userTestPAM', 'System Administrator', true);
		Map<Integer, User> testInput = generateInputMap(testUsers5, new List<Integer>{0,3,4,5,6});
		PickApproversModel testModel = new PickApproversModel(testInput);

		Test.startTest();
		Map<Integer, Id> outputMap = testModel.outputApproversAsMap();
		Test.stopTest();

		Map<Integer, Id> expectedMap = new Map<Integer, Id>{ 0=>testInput.get(0).Id, 3=>testInput.get(3).Id, 4=>testInput.get(4).Id, 5=>testInput.get(5).Id, 6=>testInput.get(6).Id };
		System.assertEquals(expectedMap, outputMap, 'Output map does not hold the current state of the model');
	}

	static testMethod void testGetOuputMap_HasError() {
		List<User> testUsers5 = TestingUtils.createMultipleTestUsers(5, 'userTestPAM', 'System Administrator', true);
		Map<Integer, User> testInput = generateInputMap(testUsers5, new List<Integer>{0,3,4,5,6});
		PickApproversModel testModel = new PickApproversModel(testInput);
		testModel.addAndGetNextUp(null);
		testModel.addAndGetNextUp(null);

		Test.startTest();
		try {
			Map<Integer, Id> outputMap = testModel.outputApproversAsMap();
			System.assert(false, 'Calling this function knowing an error exists is not allowed');
		}
		catch (PickApproversModel.PickApproversException pae) {}
		Test.stopTest();
	}

	static testMethod void testGetHasErrors_DataError() {
		List<User> testUsers5 = TestingUtils.createMultipleTestUsers(5, 'userTestPAM', 'System Administrator', true);
		Map<Integer, User> testInput = generateInputMap(testUsers5, new List<Integer>{0,3,4,5,6});
		PickApproversModel testModel = new PickApproversModel(testInput);
		testModel.addAndGetNextUp(null);
		testModel.addAndGetNextUp(null);

		System.assert(testModel.hasDataError(), 'Current state of model has data errors');
	}

	static testMethod void testGetHasNoErrors() {
		List<User> testUsers5 = TestingUtils.createMultipleTestUsers(5, 'userTestPAM', 'System Administrator', true);
		Map<Integer, User> testInput = generateInputMap(testUsers5, new List<Integer>{0,3,4,5,6});
		PickApproversModel testModel = new PickApproversModel(testInput);
		System.assert(!testModel.hasDuplicateName() && !testModel.hasDuplicateNum() && !testModel.hasDataError(), 'Current state of model has no errors');
	}

	static testMethod void testGetErrors_DuplicateName() {
		List<User> testUsers5 = TestingUtils.createMultipleTestUsers(5, 'userTestPAM', 'System Administrator', true);
		Map<Integer, User> testInput = generateInputMap(testUsers5, new List<Integer>{0,3,4,5,6});
		PickApproversModel testModel = new PickApproversModel(testInput);
		testModel.addAndGetNextUp(null);
		testModel.approvers[5].placeholderRequest.Approver_1__c = testUsers5[0].Id; // duplicate approver
		System.assert(testModel.hasDuplicateName(), 'Current state of model has all three errors');
	}

	static testMethod void testGetErrors_AllThree() {
		List<User> testUsers5 = TestingUtils.createMultipleTestUsers(5, 'userTestPAM', 'System Administrator', true);
		Map<Integer, User> testInput = generateInputMap(testUsers5, new List<Integer>{0,3,4,5,6});
		PickApproversModel testModel = new PickApproversModel(testInput);
		testModel.addAndGetNextUp(null); // unfilled approver
		testModel.addAndGetNextUp(null);
		testModel.approvers[6].placeholderRequest.Approver_1__c = testUsers5[0].Id; // duplicate approver
		testModel.approvers[1].pos = 0; // duplicate pos
		System.assert(testModel.hasDuplicateName() && testModel.hasDuplicateNum() && testModel.hasDataError(), 'Current state of model has all three errors');
	}

	static testMethod void testGetErrors_AllThreeAfterRefresh() {
		List<User> testUsers5 = TestingUtils.createMultipleTestUsers(5, 'userTestPAM', 'System Administrator', true);
		Map<Integer, User> testInput = generateInputMap(testUsers5, new List<Integer>{0,3,4,5,6});
		PickApproversModel testModel = new PickApproversModel(testInput);
		testModel.addAndGetNextUp(null); // unfilled approver
		testModel.addAndGetNextUp(null);
		testModel.approvers[6].placeholderRequest.Approver_1__c = testUsers5[0].Id; // duplicate approver
		testModel.approvers[1].pos = 0; // duplicate pos
		testModel.refresh();
		System.assert(testModel.hasDuplicateName() && testModel.hasDuplicateNum() && testModel.hasDataError(), 'Current state of model has all three errors');
	}

	static Map<Integer, User> generateInputMap(List<User> userList, List<Integer> approverPos) {
		System.assertEquals(userList.size(), approverPos.size());
		Map<Integer, User> inputMap = new Map<Integer, User>();
		for (Integer i=0; i<userList.size(); i++) {
			inputMap.put(approverPos[i], userList[i]);
		}
		return inputMap;
	}

}