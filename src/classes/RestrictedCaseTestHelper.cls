@isTest
public class RestrictedCaseTestHelper {
    
    private static final List<String> RECORD_TYPE_NAMES = new List<String> {'RFMigrated', 'Internal_IT_Support_IT_Staff'};

    /**
     * Initializes the required custom settings (restricted record type and fields) for use in tests
     */
    public static void createCustomSettings() {
        Integer index = 0;
        List<RestrictedCaseRecordTypes__c> customSettings = new List<RestrictedCaseRecordTypes__c>();
        for (String name : RECORD_TYPE_NAMES) {
            RestrictedCaseRecordTypes__c rcrt = new RestrictedCaseRecordTypes__c();
            rcrt.Name = 'Test ' + (index++);
            rcrt.Record_Type_Name__c = name;
            customSettings.add(rcrt);
        }
        insert customSettings;
        
        List<RestrictedFields__c> restrictedFields = new List<RestrictedFields__c>();
        restrictedFields.add(new RestrictedFields__c(Name='Attachment', Field_Names__c='Body,BodyLength,ContentType,Description,IsPrivate,Name,OwnerId,ParentId'));
        restrictedFields.add(new RestrictedFields__c(Name='CaseComment', Field_Names__c='CommentBody,IsPublished,ParentId'));
        restrictedFields.add(new RestrictedFields__c(Name='ContentVersion', Field_Names__c='Description,Title'));
        
        insert restrictedFields;
    }
    
    /**
     * Shortcut method to create an open case that is either restricted or not
     */
    public static Case createTestCase(boolean restricted) {
        return createTestCase(restricted, false);
    }
    
    /**
     * Creates and returns a test Case with the specified record type and status
     * Params:
     *   restricted - true if the record type should be in the custom setting; false otherwise
     *   closed - true if the case should have a status of closed; false will set the status to New
     * Returns:
     *   the new Case that was created and inserted into the database
     */
    public static Case createTestCase(boolean restricted, boolean closed) {
        String recordTypeDevName = restricted ? 'RFMigrated' : 'Technical_Services_Case';
        String status = closed ? 'Closed' : 'New';
        Id rTypeId = [SELECT Id
                      FROM RecordType
                      WHERE sObjectType = 'Case' AND DeveloperName = :recordTypeDevName].Id;
        
        Case newCase = new Case(
            Subject = 'Test Case',
            Due_Date__c = System.now(),
            Origin = 'Web',
            Status = status,
            Project_Building_Name__c = 'Project Building Name',
            Master_Category__c = 'Account Administration',
            Category__c = 'Change Employee Role'
        );
        newCase.RecordTypeId = rTypeId;

        insert newCase;
        return newCase;
    }
}