@IsTest
private class UserTriggerHandlerTest {
	static final List<String> PROFILE_NAMES = Label.PartnerUserProfileNames.split(',');
	static final String USERNAME1 = 'BW-IBM-01@test.com';
	static final String USERNAME2 = 'BW-IBM-02@test.com';

	@TestSetup
	static void setup() {
		Account acc = (Account) SObjectFactory.create(Account.SObjectType, new Map<SObjectField, Object> {
				Account.Name => 'Account1'
		});
		Contact con = (Contact) SObjectFactory.create(Contact.SObjectType, new Map<SObjectField, Object> {
				Contact.AccountId => acc.Id
		});
		SObjectFactory.create(User.SObjectType, new Map<SObjectField, Object> {
				User.Username => USERNAME1, User.ContactId => con.Id, User.CommunityNickname => 'Nick',
				User.ProfileId => [SELECT Id FROM Profile WHERE Name = :PROFILE_NAMES[0] LIMIT 1].Id
		});
		Opportunity opp = (Opportunity) SObjectFactory.create(Opportunity.SObjectType, new Map<SObjectField, Object>{
				Opportunity.StageName => 'Quote', Opportunity.CloseDate => Date.today()+5,
				Opportunity.AccountId => acc.Id
		});
		SObjectFactory.create(Applicator_Quote__c.SObjectType, new Map<SObjectField, Object>{
				Applicator_Quote__c.Contact__c => con.Id, Applicator_Quote__c.Account__c => con.AccountId,
				Applicator_Quote__c.Opportunity__c => opp.Id
		});
	}

	@IsTest
	static void testSharingAQandOpp_insert() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		Contact con2 = (Contact) SObjectFactory.create(Contact.SObjectType, new Map<SObjectField, Object> {
				Contact.AccountId => acc.Id
		});

		Test.startTest();
		User u = (User) SObjectFactory.create(User.SObjectType, new Map<SObjectField, Object> {
				User.Username => USERNAME2, User.ContactId => con2.Id,
				User.ProfileId => [SELECT Id FROM Profile WHERE Name = :PROFILE_NAMES[1] LIMIT 1].Id
		});
		Test.stopTest();

		System.runAs(u) {
			System.assertEquals(1, [SELECT COUNT() FROM Opportunity], 'Test User should have visibility to Opportunity');
			System.assertEquals(1, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to Applicator Quote');
		}
	}

	@IsTest
	static void testSharingAQandOpp_enable() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		Contact con2 = (Contact) SObjectFactory.create(Contact.SObjectType, new Map<SObjectField, Object> {
				Contact.AccountId => acc.Id
		});
		User u = (User) SObjectFactory.create(User.SObjectType, new Map<SObjectField, Object> {
				User.Username => USERNAME2, User.ContactId => con2.Id, User.IsActive => false,
				User.ProfileId => [SELECT Id FROM Profile WHERE Name = :PROFILE_NAMES[1] LIMIT 1].Id
		});

		Test.startTest();
		System.runAs(new User(Id = UserInfo.getUserId())) {
			u.IsActive = true;
			update u;
		}
		Test.stopTest();

		System.runAs(u) {
			System.assertEquals(1, [SELECT COUNT() FROM Opportunity], 'Test User should have visibility to Opportunity');
			System.assertEquals(1, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to Applicator Quote');
		}
	}
}