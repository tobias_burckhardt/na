/**
* @author Garvit Totuka
* @date 18 Sep 2017
*
* @group user Sharing Management
*
* @description User Helper Test class to functionalities in User Dao class
*/
@isTest
private class UserDaoTest {
    
    static final Integer RECORD_COUNT = 1;
    static final String PROFILE_NAME = 'PTS Partner Community';
    static final String OPPORTUNITY_ACCESS_TYPE = 'Edit';
    
    static List<Account> accounttList;
    static List<Apttus_Config2__PriceList__c> priceList;
    static List<Opportunity> opportunityList;
    static List<Contact> contactList;
    static User testUser;
    
    @isTest static void getUsersByIdsTest() {
        setupData();
        Set<ID> userIds = new Set<ID>();
        userIds.add(testUser.Id);
        List<User> userList;
        Test.startTest();
            userList = UserDao.getUsersByIds(userIds);
        Test.stopTest();
        System.assert(!userList.isEmpty(), 'Return  user List');
    }
    
    @isTest static void getUserPartnerProfileIdsTest() {
        setupData();
        List<Profile> profileList;
        Test.startTest();
            profileList = UserDao.getUserPartnerProfileIds();
        Test.stopTest();
        System.assert(!profileList.isEmpty(), 'Return  profile List');
    }

    @isTest static void getAdminUsersByUserNameTest() {
        setupData();
        List<User> userList;
        Test.startTest();
            userList = UserDao.getAdminUsersByUserName();
        Test.stopTest();
        System.assert(!userList.isEmpty(), 'Return  profile List');
    }
    
    @isTest static void getAccountAndOpptyByUserListTest() {
        setupData();
        Set<ID> userIds = new Set<ID>();
        userIds.add(testUser.Id);
        List<User> users = UserDao.getUsersByIds(userIds);
        List<Account> accountList;
        Test.startTest();
            accountList = UserDao.getAccountAndOpptyByUserList(users);
        Test.stopTest();
        System.assert(!accountList.isEmpty(), 'Return  account List along with Opportunities');
    }
    
    @isTest static void createUsersByProfileIdTest() {
        setupData();
        List<User> users = new List<User>();
        users.add(testUser);
        Map<Id,List<User>> usersByProfileId;
        Test.startTest();
            usersByProfileId = UserDao.createUsersByProfileId(users);
        Test.stopTest();
        System.assert(!usersByProfileId.isEmpty(), 'Return  map of Profile id to list of users');
    }
    
    @isTest static void createSetOfUserIdsByAccountIdTest() {
        setupData();
        Set<ID> userIds = new Set<ID>();
        userIds.add(testUser.Id);
        List<User> users = UserDao.getUsersByIds(userIds);
        Map<Id,Set<Id>> userIdsByAccountId;
        Test.startTest();
            userIdsByAccountId = UserDao.createSetOfUserIdsByAccountId(users);
        Test.stopTest();
        System.assert(!userIdsByAccountId.isEmpty(), 'Return  map of Account Id to set of user ids');
    }

    @isTest static void sendEmailToUsersTest() {
        setupData();
        List<user> adminUsers = UserDao.getAdminUsersByUserName();
        String subject = 'Test Email';
        String htmlBody = '<html lang="en"><body>'+
                            '<br><br>'+
                            'This is a test email.  '+
                            '<br><br>'+
                            '</body></html>';
        Test.startTest();
            UserDao.sendEmailToUsers(adminUsers,subject,htmlBody);
            Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
        system.assertEquals(1, invocations, 'An email should be sent');
    }
    
    @isTest static void exceptionMailToAdminUsersTest() {
        setupData();
        Boolean expectedExceptionThrown;
        Integer invocations;
        Test.startTest();
        try {
            delete contactList;
        }
        catch (DMLException ex) {
        system.debug('#####'+ex);
            expectedExceptionThrown =  ex.getMessage().contains('failed') ? true : false;
            UserDao.exceptionMailToAdminUsers(ex);
            invocations = Limits.getEmailInvocations();
        }   
        Test.stopTest();
        System.AssertEquals(true, expectedExceptionThrown);
        system.assertEquals(1, invocations, 'An email should be sent');
    }
    
    static void setupData() {
        accounttList = TestingUtils.createAccounts('test Account',RECORD_COUNT, true);
        priceList = TestingUtils.createPriceList('Sika Test Price List',RECORD_COUNT,true);
        opportunityList = TestingUtils.createOpportunities(RECORD_COUNT, accounttList[0].Id, false);
            opportunityList[0].Price_List__c = priceList[0].Id;
            insert opportunityList;
        contactList = TestingUtils.createContacts(accounttList[0].Id, RECORD_COUNT, true);
        testUser = TestingUtils.createTestUser('garvittest',PROFILE_NAME);
            testUser.ContactId = contactList[0].Id;
            insert testUser;
    }
}