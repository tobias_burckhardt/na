public without sharing class TaskServicesWithoutSharing {
    public static void insertOpportunityTeamMembers(List<OpportunityTeamMember> members) {
        insert members;
    }

    public static void insertAccountTeamMembers(List<AccountTeamMember> members) {
        insert members;
    }

    public static void updateOpportunityShares(List<OpportunityShare> shares) {
        update shares;
    }

    public static void updateAccountShares(List<AccountShare> shares) {
        update shares;
    }
}