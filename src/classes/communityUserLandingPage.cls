// *
// * Controller for the sika_SCCZoneUserHomePage
// * Trifecta Technologies, 2014-12-24 (SS)
// *

public without sharing class communityUserLandingPage {

    private Id userID {get; set;}
    public Boolean displayPopUp {get; set;}
    public Boolean displayPopUp2 {get; set;}
    
    public communityUserLandingPage() { 
        sika_SCCZoneCheckAuth.setAuthStatus(UserInfo.getProfileId());

        // get current User Id
        userID = UserInfo.getUserId();
        displayPopUp= false;
        displayPopUp2= false;

    }

    // because this controller runs "without sharing" we need to manually confirm that the user should have access. (A user whose session has timed-out, or an otherwise unauthenticated user who accessed this page directly, would be able to view the page otherwise, albeit without any data displayed.)
    public PageReference loadAction(){
        PageReference authCheck;
        sika_SCCZoneCheckAuth.setAuthStatus(UserInfo.getProfileId());

        authCheck = sika_SCCZoneCheckAuth.doAuthCheck();

        if(authCheck != null){
            return authCheck;
        }
        return null;
    }

    // query to find the five most-recently created Mixes associated with the user's contact account
    public list<Mix__c> getMixes(){
        list<Mix__c> theMixes = new list<Mix__c>();
        theMixes = [SELECT Id, 
                           OwnerID, 
                           Mix_Name__c, 
                           Contractor__c, 
                           Project__c, isLocked__c, Water_Cement_Ratio_Calculated__c,
                           Cement_Comp_Strength__c, 
                           Calculated_Raw_Material_Cost__c 
                      FROM Mix__c 
                     WHERE OwnerID = :userID ORDER BY CreatedDate DESC
                     Limit 5];
		if(theMixes.size() > 0){
    		return theMixes;
		}
		return new list<Mix__c>();
    }
        
    // query to find the five most-recently created Materials associated with the user's contact account    
    public list<Material__c> getMaterials(){
        list<Material__c> theMaterials = new list<Material__c>();
        theMaterials = [SELECT Id,
                               OwnerID, isLocked__c,
                               Material_Name__c,
                               Type__c,
                               Source__c,
                               ABS__c,
                               Spec_Gravity__c, isActive__c,
                               Cost__c
                          FROM Material__c 
                         WHERE OwnerID = :userID AND isActive__c = true ORDER BY CreatedDate DESC
                         Limit 5];
		if(theMaterials.size() > 0){
        	return theMaterials;
		}
		return new list<Material__c>();
		
    }
    public void closePopup() {        
        displayPopup = false;    
    } 
        
    public void showPopup() {        
        displayPopup = true;    
    }
    
    public void closePopup2() {        
        displayPopup2 = false;    
    } 
        
    public void showPopup2() {        
        displayPopup2 = true;    
    }

    // delete a mix or a material when the correspinding delete link is clicked
    public String deleteThisMix {get; set;}

    public PageReference deleteMix(){
        list<Mix__c> theMix = [SELECT Id FROM Mix__c WHERE Id = :deleteThisMix];
        delete(theMix[0]);
        
        PageReference newocp = new PageReference('/SCCZone/apex/sika_SCCZoneUserHomePage');
            newocp.setRedirect(true);
            return newocp;
        
    }

    public String deleteThisMaterial {get; set;}

    public PageReference deleteMaterial(){
        list<Material__c> theMat = [SELECT Id FROM Material__c WHERE Id = :deleteThisMaterial];
        theMat[0].isActive__c = false;

        update(theMat[0]);
        
        PageReference newocp = new PageReference('/SCCZone/apex/sika_SCCZoneUserHomePage');
            newocp.setRedirect(true);
            return newocp;
    }

}