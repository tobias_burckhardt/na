@isTest
public class RequestForProjectApprovalTriHandlerTest {
    private static final String RPA = 'RPA';
	private static final Integer TEST_AMOUNT_NUMBER = 50;
	private static final String RPA_APPROVAL_PROCESS_NAME = 'RPA_Standard_Approval';

    @testSetup
    static void setup() {
        AWS_Settings__c awsSetting = new AWS_Settings__c(AWS_Key__c = 'Test', S3_Bucket_Name__c = 'Test', SF_Library_Name__c = 'Test');
        insert awsSetting;

        User sysAdmin = TestingUtils.createTestUser('TestRPA', 'System Administrator');
        User testUser1 = TestingUtils.createTestUser('TestRPA1', 'Standard User');
        User testUser2 = TestingUtils.createTestUser('TestRPA2', 'Standard User');
        User testUser3 = TestingUtils.createTestUser('TestRPA3', 'Standard User');
        insert new List<User>{testUser1, testUser2, testUser3, sysAdmin};

        // Run as System Admin to avoid Mixed DML Exception
        System.runAs(sysAdmin) {
            Employee__c testEmployee1 = TestingUtils.createEmployee('TestRPA1', RPA);
            testEmployee1.SFDC_User_Account__c = testUser1.Id;
            testEmployee1.SFDC_User__c = true;
            Employee__c testEmployee2 = TestingUtils.createEmployee('TestRPA2', RPA);
            testEmployee2.SFDC_User_Account__c = testUser2.Id;
            testEmployee2.SFDC_User__c = true;
            Employee__c testEmployee3 = TestingUtils.createEmployee('TestRPA3', RPA);
            testEmployee3.SFDC_User_Account__c = testUser3.Id;
            testEmployee3.SFDC_User__c = true;
            insert new List<Employee__c>{testEmployee1, testEmployee2, testEmployee3};

            Request_for_Project_Approval__c approval = TestingUtils.createTestRequestForPA(Date.today(), false);
            approval.Project_Manager__c = testEmployee1.Id;
            approval.Local_Controller__c = testEmployee2.Id;
            insert approval;
        }
    }

    static testmethod void testUpdateProjectManager() {
        Request_for_Project_Approval__c approval = [
            SELECT Local_Controller__c, Local_Controller__r.SFDC_User_Account__c
            FROM Request_for_Project_Approval__c
            LIMIT 1
        ];

        ContentVersion version = initializeDocument(approval);

        Employee__c newProjectManager = [SELECT SFDC_User_Account__c FROM Employee__c WHERE Legal_First_Name__c = 'TestRPA3'];

        Test.startTest();
        approval.Project_Manager__c = newProjectManager.Id;
        update approval;
        Test.stopTest();

        Set<Id> userIds = new Set<Id>{
            newProjectManager.SFDC_User_Account__c,
            approval.Local_Controller__r.SFDC_User_Account__c
        };

        List<ContentDocumentLink> links = [
            SELECT ContentDocumentId, LinkedEntityId
            FROM ContentDocumentLink
            WHERE LinkedEntityId IN :userIds
        ];

        System.assertEquals(2, links.size(), 'It should have created two links.');
        for (ContentDocumentLink link : links) {
            System.assert(userIds.contains(link.LinkedEntityId),
                'It should have created one link per user.');
            System.assertEquals(version.ContentDocumentId, link.ContentDocumentId,
                'It should have created a link to the right ContentDocument record.');
        }
    }

    static testmethod void testUpdateLocalController() {
        Request_for_Project_Approval__c approval = [
            SELECT Project_Manager__c, Project_Manager__r.SFDC_User_Account__c
            FROM Request_for_Project_Approval__c
            LIMIT 1
        ];

        ContentVersion version = initializeDocument(approval);

        Employee__c newLocalController = [SELECT SFDC_User_Account__c FROM Employee__c WHERE Legal_First_Name__c = 'TestRPA3'];

        Test.startTest();
        approval.Local_Controller__c = newLocalController.Id;
        update approval;
        Test.stopTest();

        Set<Id> userIds = new Set<Id>{
            newLocalController.SFDC_User_Account__c,
            approval.Project_Manager__r.SFDC_User_Account__c
        };

        List<ContentDocumentLink> links = [
            SELECT ContentDocumentId, LinkedEntityId
            FROM ContentDocumentLink
            WHERE LinkedEntityId IN :userIds
        ];

        System.assertEquals(2, links.size(), 'It should have created two links.');
        for (ContentDocumentLink link : links) {
            System.assert(userIds.contains(link.LinkedEntityId),
                'It should have created one link per user.');
            System.assertEquals(version.ContentDocumentId, link.ContentDocumentId,
                'It should have created a link to the right ContentDocument record.');
        }
    }

	static testmethod void updateCurrentApprovers(){
		List<Request_for_Project_Approval__c> rpaList = (List<Request_for_Project_Approval__c>) new SObjectBuilder(Request_for_Project_Approval__c.SObjectType)
			.put(Request_for_Project_Approval__c.Current_Approver__c, 'Test')
			.count(TEST_AMOUNT_NUMBER).create().getRecords();

		Test.startTest();
        Map<Id, User> rpaIdToApprover = setupApprovals(rpaList);
		Test.stopTest();

        Map<Id, String> rpaIdToApproverName = new Map<Id, String>();
        for(Id rpaId : rpaIdToApprover.keySet()){
            rpaIdToApproverName.put(rpaId, rpaIdToApprover.get(rpaId).Name);
        }

		for(Request_for_Project_Approval__c rpa : [SELECT Current_Approver__c FROM Request_for_Project_Approval__c WHERE Id IN :Pluck.Ids(rpaList)]){
			System.assertEquals(rpaIdToApproverName.get(rpa.Id), rpa.Current_Approver__c, 'Unexpected value set to Current Approver field on RPA of Id ' + rpa.Id);
		}
	}

    static testmethod void approvalNextStep(){
        List<Request_for_Project_Approval__c> rpaList = (List<Request_for_Project_Approval__c>) new SObjectBuilder(Request_for_Project_Approval__c.SObjectType)
            .put(Request_for_Project_Approval__c.Current_Approver__c, 'Test')
            .count(TEST_AMOUNT_NUMBER).create().getRecords();

        Map<Id, User> rpaIdToApprover = setupApprovals(rpaList);
        Map<Id, String> rpaIdToApproverName = new Map<Id, String>();

        List<User> actorList = (List<User>) new SObjectBuilder(User.SObjectType)
            .put(User.FirstName, new SObjectFieldProviders.UniqueStringProvider('jane'))
            .count(rpaList.size()).create().getRecords();

        actorList = [SELECT Name FROM User WHERE Id IN :actorList];

        Test.startTest();
        Map<Id, Id> rpaIdToWorkItemId = new Map<Id, Id>();
        for(ProcessInstanceWorkItem pwr : [SELECT ProcessInstance.TargetObjectId FROM ProcessInstanceWorkItem WHERE ProcessInstance.TargetObjectId IN : rpaIdToApprover.keySet()]){
            rpaIdToWorkItemId.put(pwr.ProcessInstance.TargetObjectId, pwr.Id);
        }

        // Move approval to next stage
        List<Approval.ProcessWorkItemRequest> workItems = new List<Approval.ProcessWorkItemRequest>();
        for(Integer i = 0; i < rpaList.size(); i++){
            Approval.ProcessWorkItemRequest pwr = new Approval.ProcessWorkItemRequest();
            pwr.setNextApproverIds(new List<Id>{actorList[i].Id});
            pwr.setAction('Approve');
            pwr.setWorkItemId(rpaIdToWorkItemId.get(rpaList[i].Id));
            workItems.add(pwr);
            rpaIdToApproverName.put(rpaList[i].Id, actorList[i].Name);
            rpaList[i].Approver_2__c = actorList[i].Id;
        }
        update rpaList;
        Approval.process(workItems);            
        Test.stopTest();

        for(Request_for_Project_Approval__c rpa : [SELECT Current_Approver__c FROM Request_for_Project_Approval__c WHERE Id IN :Pluck.Ids(rpaList)]){
            System.assertEquals(rpaIdToApproverName.get(rpa.Id), rpa.Current_Approver__c, 'Unexpected value set to Current Approver field on RPA of Id ' + rpa.Id);
        }
    }

	static Map<Id, User> setupApprovals(List<Request_for_Project_Approval__c> rpaList){
		List<User> actorList = (List<User>) new SObjectBuilder(User.SObjectType)
			.put(User.FirstName, new SObjectFieldProviders.UniqueStringProvider('john'))
			.count(rpaList.size()).create().getRecords();

        actorList = [SELECT Name FROM User WHERE Id IN :actorList];

		Map<Id, User> rpaIdToApprover = new Map<Id, User>();

		// Submit RPAs for Approval
		List<Approval.ProcessSubmitRequest> reqs = new List<Approval.ProcessSubmitRequest>();
		for(Integer i = 0; i < rpaList.size(); i++){
			Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
			req.setComments('Approve');
			req.setNextApproverIds(new List<Id>{actorList[i].Id});
			req.setObjectId(rpaList[i].Id);
			req.setProcessDefinitionNameOrId(RPA_APPROVAL_PROCESS_NAME);
			req.setSkipEntryCriteria(true);
			reqs.add(req);
			rpaIdToApprover.put(rpaList[i].Id, actorList[i]);
            rpaList[i].Approver_1__c = actorList[i].Id;
        }
        update rpaList;
		Approval.process(reqs);
		return rpaIdToApprover;
	}

    private static ContentVersion initializeDocument(Request_for_Project_Approval__c approval) {
        ContentVersion version = new ContentVersion(
            Title = 'TestRPA',
            PathOnClient = 'test.rpa',
            VersionData = Blob.valueOf('TestRPA'),
            IsMajorVersion = true
        );
        insert version;

        version = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :version.Id];

        ContentDocumentLink rpaLink = new ContentDocumentLink(
            LinkedEntityId = approval.Id,
            ContentDocumentId = version.ContentDocumentId,
            ShareType = 'V',
            Visibility = 'InternalUsers'
        );

        insert rpaLink;
        // For some weird reason Triggers do not run on the ContentDocumentLink in tests, so we will
        // just force the after insert manually
        ContentDocumentLinkTriggerHandler.handleAfterInsert(new List<ContentDocumentLink>{rpaLink});

        return version;
    }
}