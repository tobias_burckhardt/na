@isTest
private class sika_SCCZoneUnlockMaterialsHelperTest {
	private static Integer i;
	private static list<Mix__c> theMixes;
	private static list<Material__c> theMaterials;
	
	private static map<Integer, Mix__c> UID_to_Mix;
	private static map<Integer, Material__c> UID_to_Material;
	
	private static void init(){
		theMixes = null;
		theMaterials = null;
		UID_to_Mix = null;
		UID_to_Material = null;
		
		sika_SCCZoneMaterialTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = true;
		sika_SCCZoneMaterialTriggerHelper.bypassPreventDelete = true;
		sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = true;
		
		sika_SCCZoneMixTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMixTriggerHelper.bypassMixOwnerTransferCheck = true;
		sika_SCCZoneMixTriggerHelper.bypassMixChatterAlert = true;
		sika_SCCZoneMixTriggerHelper.bypassUpdateSCMQuantity = true;
		
		sika_SCCZoneMixMaterialTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMixMaterialTriggerHelper.bypassMixOptimizationCheck = true;
		sika_SCCZoneMixMaterialTriggerHelper.bypassSetSCMWeightAndUnit = true;
		sika_SCCZoneMixMaterialTriggerHelper.bypassConcreteCurveCalc = true;
		
		// create five materails
		theMaterials = new list<Material__c>();
		String materialName;
		for(i=5; i > 0; i--){
			materialName = 'Material_' + i;
			theMaterials.add(sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'type' => 'Sand', 'name' => materialName}));
		}
		insert(theMaterials);
		
		// create a map of UIDs to materials to make it easier to keep track of this stuff
		UID_to_Material = new map<Integer, Material__c>();
		for(Material__c m : theMaterials){
			if(m.Material_Name__c == 'Material_1'){ 
				UID_to_Material.put(1, m); 
				continue;
			}
			if(m.Material_Name__c == 'Material_2'){ 
				UID_to_Material.put(2, m); 
				continue;
			}
			if(m.Material_Name__c == 'Material_3'){ 
				UID_to_Material.put(3, m); 
				continue;
			}
			if(m.Material_Name__c == 'Material_4'){ 
				UID_to_Material.put(4, m); 
				continue;
			}
			if(m.Material_Name__c == 'Material_5'){ 
				UID_to_Material.put(5, m); 
			}
			
		}
		
		// create two mixes -- one locked, one not locked
		theMixes = new list<Mix__c>();
		theMixes.add(sika_SCCZoneTestFactory.createMix(new map<String, String>{'mixName' => 'Mix_1'}));
		theMixes.add(sika_SCCZoneTestFactory.createMix(new map<String, String>{'mixName' => 'Mix_2'}));
		insert(theMixes);
		
		UID_to_Mix = new map<Integer, Mix__c>();
		for(Mix__c m : theMixes){
			if(m.Mix_Name__c == 'Mix_1'){ 
				UID_to_Mix.put(1, m); 
			} else {
				UID_to_Mix.put(2, m);
			}
		}
		
	}


	@isTest static void testSimple() {
		init();
		
		// add some materials to one of the mixes.  (Doesn't matter which here. We're just going to pass a mix ID into the "unlock" method.)
		
		// put Material 1, 2, 3 in Mix_1
		Mix_Material__c material1_mix1 = sika_SCCZoneTestFactory.createMixMaterial(UID_to_Mix.get(1), UID_to_Material.get(1));
		Mix_Material__c material2_mix1 = sika_SCCZoneTestFactory.createMixMaterial(UID_to_Mix.get(1), UID_to_Material.get(2));
		Mix_Material__c material3_mix1 = sika_SCCZoneTestFactory.createMixMaterial(UID_to_Mix.get(1), UID_to_Material.get(3));
		list<Mix_Material__c> tempList = new list<Mix_Material__c>{material1_mix1, material2_mix1, material3_mix1};
		insert(tempList);
		
		// lock the materials associated with Mix_1.  (Manually.  We aren't testing the test_results trigger that ACTUALLY locks mixes here.)
		list<Material__c> materialsToLock = new list<Material__c>();
		materialsToLock.add(UID_to_Material.get(1));
		materialsToLock.add(UID_to_Material.get(2));
		materialsToLock.add(UID_to_Material.get(3));
		
		set<Id> lockedMaterialsSet = new set<Id>();
		for(Material__c m : materialsToLock){
			m.isLocked__c = true;
			lockedMaterialsSet.add(m.Id);
		}
		update(materialsToLock);
		
		// confirm that these materials in Mix_1 are now locked (and that no other materials are locked)
		list<Material__c> testForLocked = new list<Material__c>();
		testForLocked = [SELECT Id, isLocked__c FROM Material__c];
		for(Material__c m : testForLocked){
			if(lockedMaterialsSet.contains(m.Id)){
				system.assert(m.isLocked__c == true);
			} else {
				system.assert(m.isLocked__c == false);
			}
		}
		
		// pass Mix_1's ID to the unlock method
		list<Mix__c> tempMixList = new list<Mix__c>();
		tempMixList = new list<Mix__c>();
		tempMixList.add(UID_to_Mix.get(1));
		sika_SCCZoneUnlockMaterialsHelper.unlockMaterials(tempMixList);
	
		// confirm that all Mix_1's materials are now unlocked		
		list<Material__c> testForUnlocked = new list<Material__c>();
		testForUnlocked = [SELECT Id, Material_Name__c, isLocked__c FROM Material__c];
		for(Material__c m : testForUnlocked){
			system.debug('********X ' + m.Material_Name__c + ' isLocked__c = ' + m.isLocked__c + ' :: is in lockedMaterialsSet = ' + lockedMaterialsSet.contains(m.Id));
			if(lockedMaterialsSet.contains(m.Id)){
				system.assert(m.isLocked__c == false);
			}
		}

	}
	
	
	@isTest static void testWithMaterialsInOtherLockedMixes(){
		init();
		
		// add some materials to each of the mixes
		
		// put Material 1, 2, 3, and 4 in Mix_1
		Mix_Material__c material1_mix1 = sika_SCCZoneTestFactory.createMixMaterial(UID_to_Mix.get(1), UID_to_Material.get(1));
		Mix_Material__c material2_mix1 = sika_SCCZoneTestFactory.createMixMaterial(UID_to_Mix.get(1), UID_to_Material.get(2));
		Mix_Material__c material3_mix1 = sika_SCCZoneTestFactory.createMixMaterial(UID_to_Mix.get(1), UID_to_Material.get(3));
		Mix_Material__c material4_mix1 = sika_SCCZoneTestFactory.createMixMaterial(UID_to_Mix.get(1), UID_to_Material.get(4));
		
		// put Materials 3, 4, and 5 in Mix_2
		Mix_Material__c material3_mix2 = sika_SCCZoneTestFactory.createMixMaterial(UID_to_Mix.get(2), UID_to_Material.get(3));
		Mix_Material__c material4_mix2 = sika_SCCZoneTestFactory.createMixMaterial(UID_to_Mix.get(2), UID_to_Material.get(4));
		Mix_Material__c material5_mix2 = sika_SCCZoneTestFactory.createMixMaterial(UID_to_Mix.get(2), UID_to_Material.get(5));
		
		// insert the mix_material records
		list<Mix_Material__c> tempList = new list<Mix_Material__c>{material1_mix1, 
																   material2_mix1, 
																   material3_mix1,
																   material4_mix1,
																   material3_mix2,
																   material4_mix2,
																   material5_mix2};
		insert(tempList);
		
		// lock both mixes and all materials
		for(Mix__c m : theMixes){
			m.isLocked__c = true;
		}
		for(Material__c m : theMaterials){
			m.isLocked__c = true;
		}
		update(theMixes);
		update(theMaterials);
		list<Material__c> testForLocked;
		
		// confirm that all materials are locked
		testForLocked = new list<Material__c>();
		testForLocked = [SELECT Id, isLocked__c FROM Material__c];
		for(Material__c m : testForLocked){
			system.assert(m.isLocked__c == true);
		}
		
		// pass Mix_1's ID to the unlock method
		list<Mix__c> tempMixList = new list<Mix__c>();
		tempMixList = new list<Mix__c>();
		tempMixList.add(UID_to_Mix.get(1));
		sika_SCCZoneUnlockMaterialsHelper.unlockMaterials(tempMixList);
		
		// confirm that Materials 1 & 2 are unlocked, and that all other materials are locked
		testForLocked = new list<Material__c>();
		testForLocked = [SELECT Id, Material_Name__c, isLocked__c FROM Material__c];
		
		for(Material__c m : testForLocked){
			if(m.Id == UID_to_Material.get(1).Id || m.Id == UID_to_Material.get(2).Id){
				system.assert(m.isLocked__c == false);
			} else {
				system.assert(m.isLocked__c == true);
			}
		}
		
		// unlock both mixes.  (Mix_1.isLocked__c is still set to true.  We just passed its ID in to the unlock method. We want to ensure that ALL materials get unlocked now.)
		for(Mix__c m : theMixes){
			m.isLocked__c = false;
		}
		update(theMixes);
		
		// pass Mix_2's ID to the unlock method
		tempMixList = new list<Mix__c>();
		tempMixList.add(UID_to_Mix.get(2));
		sika_SCCZoneUnlockMaterialsHelper.unlockMaterials(tempMixList);
		
		// confirm that all Materials are unlocked
		testForLocked = new list<Material__c>();
		testForLocked = [SELECT Id, Material_Name__c, isLocked__c FROM Material__c];
		
		for(Material__c m : testForLocked){
			system.assert(m.isLocked__c == false);
		}
		
	}

}