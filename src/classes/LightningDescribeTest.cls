@isTest
private class LightningDescribeTest {
    // this test class follows a tests-as-documentation format
    // most tests are more verbose than they strictly need to be to cover
    // what they are testing, but showcase how you would build a describe
    // using the tested functionality in a typical scenario.

    // there is no cross-platform, org independent way to cover the ObjectInfo addFieldSets
    // functions as FieldSets cannot be constructed in tests as of Winter 17

    class FlagPair {
        public final String name;
        public final Boolean expected;
        public final Boolean actual;
        public FlagPair(String name, Boolean expected, Boolean actual) {
            this.name = name;
            this.expected = expected;
            this.actual = actual;
        }
    }

    static TestMethod void testObjectInfo_addObjectProps() {
        Test.startTest();
            final LightningDescribe describe = new LightningDescribe()
                .addObject(new LightningDescribe.ObjectInfo(Contact.SobjectType,
                    new Set<LightningDescribe.ObjectProp>{
                        LightningDescribe.ObjectProp.KEY_PREFIX,
                        LightningDescribe.ObjectProp.LABEL,
                        LightningDescribe.ObjectProp.LABEL_PLURAL
                    })
                );
        Test.stopTest();

         System.assertEquals(Contact.SobjectType.getDescribe().getKeyPrefix(), describe.objects.get('Contact').keyPrefix,
            'we expect that that when the KEY_PREFIX prop is requested, the keyPrefix value is correctly populated');
         System.assertEquals(Contact.SobjectType.getDescribe().getLabel(), describe.objects.get('Contact').label,
            'we expect that that when the LABEL prop is requested, the label value is correctly populated');
         System.assertEquals(Contact.SobjectType.getDescribe().getLabelPlural(), describe.objects.get('Contact').labelPlural,
            'we expect that that when the LABEL_PLURAL prop is requested, the labelPlural value is correctly populated');
    }

    static TestMethod void testObjectInfo_addObjectCapabilitiesProps() {
        Test.startTest();
            final LightningDescribe describe = new LightningDescribe()
                .addObject(new LightningDescribe.ObjectInfo(Contact.SobjectType,
                    LightningDescribe.OBJECT_CAPABILITIES)
                );
        Test.stopTest();

        DescribeSobjectResult contactToken = Contact.SobjectType.getDescribe();
        LightningDescribe.ObjectInfo contactInfo = describe.objects.get('Contact');
        for (FlagPair flag : new List<FlagPair>{
                new FlagPair('IS_ACCESSIBLE', contactToken.isAccessible(), contactInfo.isAccessible),
                new FlagPair('IS_CREATEABLE', contactToken.isCreateable(), contactInfo.isCreateable),
                new FlagPair('IS_DELETABLE', contactToken.isDeletable(), contactInfo.isDeletable),
                new FlagPair('IS_FEED_ENABLED', contactToken.isFeedEnabled(), contactInfo.isFeedEnabled),
                new FlagPair('IS_MERGEABLE', contactToken.isMergeable(), contactInfo.isMergeable),
                new FlagPair('IS_QUERYABLE', contactToken.isQueryable(), contactInfo.isQueryable),
                new FlagPair('IS_SEARCHABLE', contactToken.isSearchable(), contactInfo.isSearchable),
                new FlagPair('IS_UNDELETABLE', contactToken.isUndeletable(), contactInfo.isUndeletable),
                new FlagPair('IS_UPDATEABLE', contactToken.isUpdateable(), contactInfo.isUpdateable)
            }) {
            System.assertEquals(flag.expected, flag.actual,
                'we expect that when the flag ' + flag.name + ' is requested, the value is correctly populated');
        }
    }

    static TestMethod void testObjectInfo_StringConstructor() {
        Test.startTest();
            final LightningDescribe describe = new LightningDescribe()
                .addObject(new LightningDescribe.ObjectInfo('Contact',
                    new Set<LightningDescribe.ObjectProp>{
                        LightningDescribe.ObjectProp.LABEL
                    })
                );
        Test.stopTest();

        System.assertNotEquals(null, describe.objects.get('Contact').Label,
            'we expect that the sobjectType api name can be used to construct an ObjectInfo in lieu of a SobjectType');
   }

    static TestMethod void testObjectInfo_addFieldsReadProps() {

        Test.startTest();
            final LightningDescribe describe = new LightningDescribe()
                .addObject(
                    new LightningDescribe.ObjectInfo(Contact.SobjectType)
                        .addFields(new Set<SobjectField>{Contact.Email},
                            LightningDescribe.BASELINE_FIELD_READS)
                );
        Test.stopTest();

        LightningDescribe.FieldInfo emailInfo = describe.objects.get('Contact').fields.get('Email');
        System.assertEquals(Contact.Email.getDescribe().getLabel(), emailInfo.label,
            'we expect that when the LABEL prop is requested, the label value is correctly populated');
        System.assertEquals(Contact.Email.getDescribe().getInlineHelpText(), emailInfo.inlineHelpText,
            'we expect that when the INLINE_HELP_TEXT prop is requested, the inline help text value is correctly populated');
    }

    static TestMethod void testObjectInfo_addFieldsWriteProps() {

        Test.startTest();
            final LightningDescribe describe = new LightningDescribe()
                .addObject(
                    new LightningDescribe.ObjectInfo(Opportunity.SobjectType)
                        .addFields(new Set<SobjectField>{
                            Opportunity.StageName,
                            Opportunity.Name
                        },
                        LightningDescribe.BASELINE_FIELD_WRITES)
                );
        Test.stopTest();

        LightningDescribe.ObjectInfo opptyInfo = describe.objects.get('Opportunity');
        System.assertEquals(Opportunity.Name.getDescribe().getLength(), opptyInfo.fields.get('Name').length,
            'we expect that when the LENGTH prop is requested, the length value is correctly populated');
        LightningDescribe.FieldInfo stageNameInfo = opptyInfo.fields.get('StageName');
        DescribeFieldResult stageNameToken = Opportunity.StageName.getDescribe();
        List<Schema.PicklistEntry> stageNamePicklistEntries = stageNameToken.getPicklistValues();
        for (Integer i=0; i<stageNamePicklistEntries.size(); i++) {
            Schema.PicklistEntry ple = stageNamePicklistEntries[i];
            System.assertEquals(ple.getLabel(), stageNameInfo.picklistValues[i].label,
                'we expect that when the PICKLIST_VALUES prop is requested, the picklist entry labels are correctly populated');
            System.assertEquals(ple.getValue(), stageNameInfo.picklistValues[i].value,
                'we expect that when the PICKLIST_VALUES prop is requested, the picklist entry values are correctly populated');
            System.assertEquals(ple.isActive(), stageNameInfo.picklistValues[i].isActive,
                'we expect that when the PICKLIST_VALUES prop is requested, the picklist entry isActive flags are correctly populated');
            System.assertEquals(ple.isDefaultValue(), stageNameInfo.picklistValues[i].isDefaultValue,
                'we expect that when the PICKLIST_VALUES prop is requested, the picklist entry isDefaultValue flags are correctly populated');
        }
    }

    static TestMethod void testObjectInfo_addFieldsFlagProps() {

        Test.startTest();
            final LightningDescribe describe = new LightningDescribe()
                .addObject(
                    new LightningDescribe.ObjectInfo(Opportunity.SobjectType)
                        .addFields(new Set<SobjectField>{
                            Opportunity.StageName,
                            Opportunity.Name
                        },
                        LightningDescribe.BASELINE_FIELD_WRITES)
                );
        Test.stopTest();

        LightningDescribe.ObjectInfo opptyInfo = describe.objects.get('Opportunity');
        LightningDescribe.FieldInfo stageNameInfo = opptyInfo.fields.get('StageName');
        DescribeFieldResult stageNameToken = Opportunity.StageName.getDescribe();
        for (FlagPair flag : new List<FlagPair>{
                new FlagPair('IS_ACCESSIBLE', stageNameToken.isAccessible(), stageNameInfo.isAccessible),
                new FlagPair('IS_CASE_SENSITIVE', stageNameToken.isCaseSensitive(), stageNameInfo.isCaseSensitive),
                new FlagPair('IS_CREATEABLE', stageNameToken.isCreateable(), stageNameInfo.isCreateable),
                new FlagPair('IS_DEFAULTED_ON_CREATE', stageNameToken.isDefaultedOnCreate(), stageNameInfo.isDefaultedOnCreate),
                new FlagPair('IS_DEPENDENT_PICKLIST', stageNameToken.isDependentPicklist(), stageNameInfo.isDependentPicklist),
                new FlagPair('IS_NILLABLE', stageNameToken.isNillable(), stageNameInfo.isNillable),
                new FlagPair('IS_RESTRICTED_DELETE', stageNameToken.isRestrictedDelete(), stageNameInfo.isRestrictedDelete),
                new FlagPair('IS_UNIQUE', stageNameToken.isUnique(), stageNameInfo.isUnique),
                new FlagPair('IS_UPDATEABLE', stageNameToken.isUpdateable(), stageNameInfo.isUpdateable)
            }) {
            System.assertEquals(flag.expected, flag.actual,
                'we expect that when the flag ' + flag.name + ' is requested, the value is correctly populated');
        }
    }

    static TestMethod void testObjectInfo_addFieldsStringSignature() {
        Test.startTest();
            final LightningDescribe describe = new LightningDescribe()
                .addObject(
                    new LightningDescribe.ObjectInfo(Contact.SobjectType)
                        .addFields(new Set<String>{'Email'},
                            LightningDescribe.BASELINE_FIELD_READS)
                );
        Test.stopTest();

        System.assertNotEquals(null, describe.objects.get('Contact').fields.get('Email').label,
            'we expect to be able to add field props for an object by providing their api name');
    }

    static TestMethod void testAddObjects() {
        Test.startTest();
            final LightningDescribe describe = new LightningDescribe()
                .addObjects(new List<LightningDescribe.ObjectInfo>{new LightningDescribe.ObjectInfo(Contact.SobjectType)
                    .addProps(new Set<LightningDescribe.ObjectProp>{LightningDescribe.ObjectProp.LABEL})
                });
        Test.stopTest();

        System.assertNotEquals(null, describe.objects.get('Contact'),
            'we expect that addObjects allows for bulk adding of objectInfos similar to addObject');
    }

    static TestMethod void testMergeDescribe() {

        final SobjectField someSobjectField = Account.Name;
        Set<LightningDescribe.FieldProp> someFieldProps = new Set<LightningDescribe.FieldProp>{
            LightningDescribe.FieldProp.LABEL
        };
        Set<LightningDescribe.FieldProp> otherFieldProps = new Set<LightningDescribe.FieldProp>{
            LightningDescribe.FieldProp.INLINE_HELP_TEXT
        };

        Test.startTest();
            final LightningDescribe supplementaryDescribe = new LightningDescribe()
                .addObject(new LightningDescribe.ObjectInfo(Contact.SobjectType)
                    .addProps(new Set<LightningDescribe.ObjectProp>{LightningDescribe.ObjectProp.LABEL})
                    .addFields(new Set<SobjectField>{Contact.Name, Contact.Email}, otherFieldProps)
                );
            final LightningDescribe describe = new LightningDescribe()
                .addObject(new LightningDescribe.ObjectInfo(Contact.SobjectType)
                    .addFields(new Set<SobjectField>{Contact.Name, Contact.Email}, someFieldProps)
                ).mergeDescribe(supplementaryDescribe);
        Test.stopTest();

        System.assertEquals(Contact.SobjectType.getDescribe().getLabel(), describe.objects.get('Contact').label,
            'we expect that that merges will merge optional properties of objects');
        Set<LightningDescribe.FieldProp> mergedFieldProps = someFieldProps.clone();
        mergedFieldProps.addAll(otherFieldProps);
        System.assertEquals(mergedFieldProps, describe.objects.get('Contact').fields.get('Email').hasProps,
            'we expect that that merges will merge optional field properties of objects');
    }


}