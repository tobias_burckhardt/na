public without sharing class AccountServicesWithoutSharing {
    public static void insertAccountTeamMembers(List<AccountTeamMember> members) {
        insert members;
    }

    public static void updateAccountShares(List<AccountShare> shares) {
        update shares; 
    }

    public static void insertAccountShares(List<AccountShare> shares) {
        insert shares;
    }
}