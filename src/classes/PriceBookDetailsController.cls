public class PriceBookDetailsController {
    public String selectedLang{get;set;}
    public Boolean isMobile { get; set; }
   	public PriceBookDetailsController(){
       selectedLang = UserInfo.getLanguage();
       isMobile = UserInfo.getUiTheme() == 'Theme4t';
   	}
    @RemoteAction
    public static String getSAPRelatedPriceBooks(String dataList){
        List<Pricebook2> pricebooks = [Select Id,Name from Pricebook2 where isSAPRelated__c=true LIMIT 100];
        Map<String,Object> dataToBeSentBack = new Map<String,Object>();
        dataToBeSentBack.put('pricebooks',pricebooks);
        dataToBeSentBack.put('status',true);
    	return JSON.serializePretty(dataToBeSentBack); 
    }
    @RemoteAction
    public static String updateExcludedPBEntries(String dataList) {
        Map<String,Object> dataToBeSentBack = new Map<String,Object>();
        Map<String,Object> requestData = (Map<String,Object>)JSON.deserializeUntyped(dataList);
        System.debug('req data--'+requestData);
        String selectedEntry = (String)requestData.get('selectedEntry');
        System.debug('selectedEntry--'+selectedEntry);
		String unSelectedEntry = (String)requestData.get('unSelectedEntry');
        System.debug('unSelectedEntry--'+unSelectedEntry);
        
        if(String.isNotBlank(selectedEntry)){
            try{
                List<PricebookEntry> selectedPricebookEntries = [Select Id,Name,isToIncludeInPriceBookUpload__c from PricebookEntry where Id = :selectedEntry.trim()];
                for(PricebookEntry pbEntry:selectedPricebookEntries){
                    pbEntry.isToIncludeInPriceBookUpload__c = true;
                }
                update selectedPricebookEntries;
                System.debug('selectedPricebookEntries--'+selectedPricebookEntries);
                dataToBeSentBack.put('status',true);
                dataToBeSentBack.put('message',Label.price_book_entry_status_success);             
            }
            catch(Exception e){
                System.debug('Exception caught : '+e);
                dataToBeSentBack.put('status',false);
                dataToBeSentBack.put('message',Label.price_book_entry_status_error);            
            }
        }   
        
        if(String.isNotBlank(unSelectedEntry)){
            try{
                List<PricebookEntry> unselectedPricebookEntries = [Select Id,Name,isToIncludeInPriceBookUpload__c from PricebookEntry where Id = :unSelectedEntry.trim()];
                System.debug('unselectedPricebookEntries--'+unselectedPricebookEntries);
                for(PricebookEntry pbEntry:unselectedPricebookEntries){
                    pbEntry.isToIncludeInPriceBookUpload__c = false;
                }
                update unselectedPricebookEntries;
                System.debug('unselectedPricebookEntries--'+unselectedPricebookEntries);
                dataToBeSentBack.put('status',true);
                dataToBeSentBack.put('message',Label.price_book_entry_status_success);
            }
            catch(Exception e){
                System.debug('Exception caught : '+e);
                dataToBeSentBack.put('status',false);
                dataToBeSentBack.put('message',Label.price_book_entry_status_error);
            }
        }
        return JSON.serializePretty(dataToBeSentBack);
       
    }
    @RemoteAction
    public static String getPriceBookEntriesBySelectedPriceBook(String dataList) {
         List<PriceBookEntry> pricebookEntries = new List<PriceBookEntry>();
         List<PriceBookEntryWrapper> pricebookEntriesWrapperList = new List<PriceBookEntryWrapper>();
         Map<String,Object> dataToBeSentBack = new Map<String,Object>();
         List<string> preConditions = new List<String>();
         List<string> dynamicConditions = new List<String>();
         Integer LimitSize= 14;
         String emptyString = '';
         Map<String,Object> request = (Map<String,Object>)JSON.deserializeUntyped(dataList);
         String productNameOrArticleCode = (String)request.get('productNameOrArticleCode');
         String selectedPricebookId = (String)request.get('selectedPricebookId');
         Integer OffsetSize = (Integer)request.get('OffsetSize');
		 String countSoqlQuery = 'select COUNT() from PricebookEntry';
         String soqlQuery = 'select Id, Pricebook2Id, IsActive,Start_Date__c,End_Date__c,Pricebook2.LastModifiedDate,Pricebook2.Name,Scale_UOM__c,Price_UOM__c,Pricebook2.lastModifiedBy.Name,Pricebook2.Application__c,Pricebook2.Condition_Table__c,Pricebook2.Condition_Type__c,Pricebook2.Distribution__c,Pricebook2.Division__c,Pricebook2.Price_List_Type__c,Pricebook2.Sales_Org__c,Pricebook2.isSAPRelated__c,Pricebook2.Validity_Start_Date__c,Pricebook2.Validity_End_Date__c,isToIncludeInPriceBookUpload__c,Product2.Id,Product2.Name,Product2.Description, Product2.IsActive, Product2.Sales_Unit__c,Product2.Article_code__c,Scale_Quantity_1__c,Scale_Rate_1__c,Scale_Rate_2__c,Currency__c,Base_Scale_Quantity__c,Base_Price__c from PricebookEntry';         
         preConditions.add(' IsActive=true');
         preConditions.add(' Product2.IsActive=true');
         preConditions.add(' Pricebook2Id = \'' + selectedPricebookId + '\'');
        if(String.isNotBlank(productNameOrArticleCode)){
            String productNameOrArticleCodeSearchString = '%'+String.escapeSingleQuotes(productNameOrArticleCode)+'%';
            dynamicConditions.add(' (Product2.Name Like :productNameOrArticleCodeSearchString OR Product2.Article_code__c Like :productNameOrArticleCodeSearchString)');// or Product2.Description Like :productNameSearchString
        }
        if(preConditions.size() > 0) {
           soqlQuery += '  WHERE ' + preConditions[0];
           countSoqlQuery += '  WHERE ' + preConditions[0];
           for(Integer i = 1; i < preConditions.size(); i++){
              soqlQuery += '  AND ' + preConditions[i];
              countSoqlQuery += '  AND ' + preConditions[i];
           }
        }
        if(dynamicConditions.size() > 0) {
           for(Integer i = 0; i < dynamicConditions.size(); i++){
              soqlQuery += '  AND ' + dynamicConditions[i];
              countSoqlQuery += '  AND ' + dynamicConditions[i];
           }
        }
        soqlQuery+= ' order by Product2.Name LIMIT :LimitSize OFFSET :OffsetSize';
        Integer totalRecs = Database.countquery(countSoqlQuery);
        try{
            System.debug('LimitSize--'+LimitSize);
            System.debug('OffsetSize--'+OffsetSize);
            pricebookEntries = Database.query(soqlQuery);
            System.debug('pricebookEntries--'+pricebookEntries);
            if(pricebookEntries!=null){
                for(PricebookEntry pricebookEntryObj:pricebookEntries){
                    PriceBookEntryWrapper pricebookEntryWrapperObj = new PriceBookEntryWrapper();
                    pricebookEntryWrapperObj.productId = pricebookEntryObj.Product2.Id;
                    pricebookEntryWrapperObj.priceBookEntryId = pricebookEntryObj.Id;
                    pricebookEntryWrapperObj.priceBookId = pricebookEntryObj.Pricebook2Id;
                    pricebookEntryWrapperObj.priceBookName = pricebookEntryObj.Pricebook2.Name;
                    pricebookEntryWrapperObj.productName = pricebookEntryObj.Product2.Name;
                    pricebookEntryWrapperObj.productCode = pricebookEntryObj.Product2.Article_code__c!=null && pricebookEntryObj.Product2.Article_code__c!='' ? pricebookEntryObj.Product2.Article_code__c.substringAfter('_'):'NA';
                    pricebookEntryWrapperObj.productDesc = String.isNotBlank(pricebookEntryObj.Product2.Description) ? pricebookEntryObj.Product2.Description : 'NA';
                    pricebookEntryWrapperObj.baseScaleQty = pricebookEntryObj.Base_Scale_Quantity__c ==null ?0: pricebookEntryObj.Base_Scale_Quantity__c;
                    pricebookEntryWrapperObj.basePrice = pricebookEntryObj.Base_Price__c==null ?0:pricebookEntryObj.Base_Price__c;
                    pricebookEntryWrapperObj.isToIncludeInPriceBookUpload = pricebookEntryObj.isToIncludeInPriceBookUpload__c == null ? false : pricebookEntryObj.isToIncludeInPriceBookUpload__c;
                    pricebookEntryWrapperObj.lastModifiedPerson = pricebookEntryObj.Pricebook2.lastModifiedBy.Name;
                    pricebookEntryWrapperObj.conditionType = pricebookEntryObj.Pricebook2.Condition_Type__c;
                    pricebookEntryWrapperObj.salesOrg = pricebookEntryObj.Pricebook2.Sales_Org__c;
                    pricebookEntryWrapperObj.pricelistType = pricebookEntryObj.Pricebook2.Price_List_Type__c;
                    Date startDate = pricebookEntryObj.Pricebook2.Validity_Start_Date__c;
                    if(startDate!=null){
                        String startDay = startDate.day()>9 ? ''+startDate.day():'0'+startDate.day();
                    	String startMonth = startDate.month()>9 ? ''+startDate.month():'0'+startDate.month();
                    	pricebookEntryWrapperObj.pbValidityStartDate = startDay+'/'+ startMonth +'/'+startDate.year();
                    }else{
                        pricebookEntryWrapperObj.pbValidityStartDate = 'NA';
                    }
                    Date endDate = pricebookEntryObj.Pricebook2.Validity_End_Date__c;
                    if(endDate!=null){
                    	String endDay = endDate.day()>9 ? ''+endDate.day():'0'+startDate.day();
                    	String endMonth = endDate.month()>9 ? ''+endDate.month():'0'+endDate.month();
                    	pricebookEntryWrapperObj.pbValidityEndDate  = endDay+'/'+endMonth+'/'+endDate.year();  
                    }else{
                        pricebookEntryWrapperObj.pbValidityEndDate  = 'NA';  
                    }
                    DateTime lastModDate = pricebookEntryObj.Pricebook2.LastModifiedDate;
                    if(lastModDate!=null){
                    	String endModDay = lastModDate.day()>9 ? ''+lastModDate.day():'0'+lastModDate.day();
                    	String endModMonth = lastModDate.month()>9 ? ''+lastModDate.month():'0'+lastModDate.month();
                    	pricebookEntryWrapperObj.lastModifiedDateString = endModDay+'/'+endModMonth+'/'+lastModDate.year();  
                    }else{
                        pricebookEntryWrapperObj.lastModifiedDateString = 'NA';  
                    }
                    
                    pricebookEntryWrapperObj.isTieredPricingAvailable = pricebookEntryObj.Scale_Rate_1__c !=null && pricebookEntryObj.Scale_Rate_1__c != 0.00 ? true : false;
                    pricebookEntryWrapperObj.currencyRate = String.isNotBlank(pricebookEntryObj.Currency__c) ? pricebookEntryObj.Currency__c:'';
                    pricebookEntryWrapperObj.scaleUOM = String.isNotBlank(pricebookEntryObj.Scale_UOM__c) ? pricebookEntryObj.Scale_UOM__c:'NA' ;
                    pricebookEntryWrapperObj.priceUOM = 0.0;//pricebookEntryObj.Price_UOM__c !=null && pricebookEntryObj.Price_UOM__c!=0.00 ? pricebookEntryObj.Price_UOM__c : 0.00;
                    pricebookEntryWrapperObj.priceUOMScale = String.isNotBlank(pricebookEntryObj.Price_UOM__c)?pricebookEntryObj.Price_UOM__c:'NA';
                    Date startValDate = pricebookEntryObj.Start_Date__c;
                    if(startValDate!=null){
                    	String startValDay = startValDate.day()>9 ? ''+startValDate.day():'0'+startValDate.day();
                    	String startValMonth = startValDate.month()>9 ? ''+startValDate.month():'0'+startValDate.month();
                    	pricebookEntryWrapperObj.startDate = startValDay+'/'+startValMonth+'/'+startValDate.year();  
                    }else{
                        pricebookEntryWrapperObj.startDate = 'NA';  
                    }
                    Date endValDate = pricebookEntryObj.End_Date__c;
                    if(endValDate!=null){
                    	String endValDay = endValDate.day()>9 ? ''+endValDate.day():'0'+endValDate.day();
                    	String endValMonth = endValDate.month()>9 ? ''+endValDate.month():'0'+endValDate.month();
                    	pricebookEntryWrapperObj.endDate = endValDay+'/'+endValMonth+'/'+endValDate.year(); 
                    }else{
                        pricebookEntryWrapperObj.endDate = 'NA'; 
                    }
					pricebookEntriesWrapperList.add(pricebookEntryWrapperObj);
                }
            }
			dataToBeSentBack.put('pricebookEntriesWrapperList',pricebookEntriesWrapperList);
        	dataToBeSentBack.put('status',true);
        	dataToBeSentBack.put('totalRecs',totalRecs);
        }catch(Exception e){
            System.debug('exception--'+e.getStackTraceString()+'---'+e.getCause()+'--'+e.getTypeName()+'--'+e.getMessage());
            dataToBeSentBack.put('status',false);
        }  
        
         return JSON.serializePretty(dataToBeSentBack); 
    }
}