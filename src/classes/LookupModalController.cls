public with sharing class LookupModalController {


	@AuraEnabled
	public static String getRelatedData(String parentApiName, String childApiName, String fieldNames, String filterFieldNames){

		String pluralObjName = Schema.getGlobalDescribe().get(childApiName).getDescribe().getLabelPlural();
		List<sObject> sobjList = getAllRecords(childApiName,fieldNames);
		for(sObject s : sobjList){
			System.debug('obj =  ' + s);
		}

		Map<String,Schema.SObjectField> fieldToSchema = createFieldToLabelMap(childApiName);

		Map<String, List<String>> fieldToPicklistEntries = createFieldToSelectOptionsMap(childApiName,filterFieldNames);
		Map<String,String> fieldMap = mapApiNameToLabel(fieldToSchema);
		Map<String,String> fieldTypeMap = createFieldToTypeMap(fieldToSchema);
		LookUpDataEnvelope envelope = new LookUpDataEnvelope(sobjList,fieldMap, fieldTypeMap,fieldToPicklistEntries);

		 return JSON.serialize(envelope);
	}


	@AuraEnabled
	public static void handleSaving(List<String> childIds, String junctionApiName, String parentApiName, 
		String childApiName, String parentId){

		List<sObject> junctionObjects = new List<sObject>();
		for(String id: childIds){
			sObject sObj = Schema.getGlobalDescribe().get(junctionApiName).newSObject();
			sObj.put(childApiName,id);
			sObj.put(parentApiName,parentId);

			junctionObjects.add(sObj);
System.debug('sObj = ' + sObj);
		}

		saveJunctionObject(junctionObjects);

	}

	@AuraEnabled
	public static String getObjectNameFromApiName(String apiName){

		String label = Schema.getGlobalDescribe().get(apiName).getDescribe().getLabel();
		return label;

	}

	public static void saveJunctionObject(List<sObject> junctionObjects){
		Database.insert(junctionObjects, false);
	}


	public static Map<String, Schema.SObjectField> createFieldToLabelMap(String objectName){
		Map<String, Schema.SObjectField> fieldMap =Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
		return fieldMap;
	}

	public static List<sObject> getAllRecords(String apiName, String fieldNames){
		String query = 'SELECT  ' + fieldNames +' FROM ' + apiName;
		return Database.query(query);
	}


	public static Map<String,String> mapApiNameToLabel(Map<String, Schema.SObjectField> fieldMap){
		Map<String, String> newMap = new Map<String, String>();

		for(String key: fieldMap.keySet()){
			newMap.put(key, fieldMap.get(key).getDescribe().getlabel());
		}
		
		return newMap;
	}

	public static Map<String,String> createFieldToTypeMap(Map<String, Schema.SObjectField> fieldMap){
		Map<String, String> newMap = new Map<String, String>();

		for(String key: fieldMap.keySet()){
			String keyType = ''+fieldMap.get(key).getDescribe().getType();
			newMap.put(key, keyType);
		}
		
		return newMap;
	}




	public static Map<String, List<String>> createFieldToSelectOptionsMap(String objectApiName, String filterFields){
		List<String> fields = filterFields.split(',');
		Map<String, List<String>> fieldToPicklistEntries = new Map<String, List<String>>();

		for(String field : fields){
			DescribeFieldResult fieldResult = Schema.getGlobalDescribe()
                    .get(objectApiName).getDescribe().fields.getMap()
    				.get(field).getDescribe();

			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
			List<String> options = new List<String>();

			for( Schema.PicklistEntry pickListVal : ple){
	            options.add( pickListVal.getValue());
	        }  
	        fieldToPicklistEntries.put(field,options);

		}
		return fieldToPicklistEntries;

	}



	public class LookUpDataEnvelope{
		public List<sObject>  records {get; set;}
		public Map<String, String> childFieldMap {get; set;}
		public Map<String, String> fieldTypeMap {get; set;}
		public Map<String, List<String>> fieldToPicklistEntries  {get;set;}
		public LookUpDataEnvelope(List<sObject> records, Map<String, String> childFieldMap, 
		Map<String,String> fieldTypeMap, Map<String, List<String>> fieldToPicklistEntries  ){
			this.records = records;
			this.childFieldMap = childFieldMap;
			this.fieldTypeMap = fieldTypeMap;
			this.fieldToPicklistEntries = fieldToPicklistEntries;
		}
	}



}