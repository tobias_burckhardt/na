@isTest
private class SoqlUtilsTest {
	private static Integer idNum = 1;

	private static Id getFakeProductId() {
		String myIdNum = String.valueOf(idNum++);
		return Id.valueOf(Product2.SObjectType.getDescribe().getKeyPrefix() + '0'.repeat(12 - myIdNum.length()) + myIdNum);
	}

	@isTest static void testGetSelect() {
		String query = SoqlUtils.getSelect(new List<String> {
			'Id',
			'Name'
		}, 'Product2').toString();

		System.assertEquals('SELECT Id, Name FROM Product2', query);
	}

	@isTest static void testGetEq() {
		String eqClause = SoqlUtils.getEq('My_Field__c', 'Test Val\'ue 123').toString();

		System.assertEquals(eqClause, 'My_Field__c = \'Test Val\\\'ue 123\'');
	}

	@isTest static void testGetIncludesAllList() {
		String includesClause = SoqlUtils.getIncludesAll('Something__c', new List<String> {
			'value 1',
			'value 2',
			'value 3'
		}).toString();

		System.assertEquals('Something__c INCLUDES (\'value 1;value 2;value 3\')', includesClause);
	}

	@isTest static void testGetIncludesAllListSingle() {
		String includesClause = SoqlUtils.getIncludesAll('Something__c', new List<String> {
			'value 1'
		}).toString();

		System.assertEquals('Something__c INCLUDES (\'value 1\')', includesClause);
	}

	@isTest static void testGetIncludesAllListEmpty() {
		String includesClause = SoqlUtils.getIncludesAll('Something__c', new List<String>()).toString();

		System.assertEquals('Id = null', includesClause);
	}

	@isTest static void testQuery() {
		String query = SoqlUtils.getSelect(new List<String> {
				'Id',
				'Name'
			}, 'Product2')
			.withCondition(SoqlUtils.getEq('Name', 'x'))
			.toString();


		System.assertEquals('SELECT Id, Name FROM Product2 WHERE Name = \'x\'', query);
	}

	@isTest static void testBooleans() {
		System.assertEquals('a = true', SoqlUtils.getEq('a', true).toString());
		System.assertEquals('a = false', SoqlUtils.getEq('a', false).toString());
	}

	@isTest static void testGetAnd() {
		Id id1 = getFakeProductId();
		Id id2 = getFakeProductId();
		Id id3 = getFakeProductId();
		String andClause = SoqlUtils.getAnd(new List<SoqlUtils.SoqlCondition> {
			SoqlUtils.getEq('My_Field__c', '1'),
			SoqlUtils.getEq('Other_Field__c', '2')
		}).toString();
		System.assertEquals(andClause, '(My_Field__c = \'1\' AND Other_Field__c = \'2\')');
	}
}