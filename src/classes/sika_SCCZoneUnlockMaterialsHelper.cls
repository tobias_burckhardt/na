public without sharing class sika_SCCZoneUnlockMaterialsHelper {

	// trigger helper method that unlocks material records when the last mix they're associated with has been unlocked or deleted. (Called by the test_result trigger and by the mix trigger)
    public static void unlockMaterials(list<Mix__c> unlockedOrDeletedMixes){
        set<Id> unlockedOrDeletedMixesSet = new set<Id>();
		
		// add the IDs of the mixes passed to the controller to unlockedOrDeletedMixesSet
        for(Mix__c m : unlockedOrDeletedMixes){
            unlockedOrDeletedMixesSet.add(m.Id);
        }

		// get the Mix_Materials associated with the mixes passed to the controller
        list<Mix_Material__c> originalMixMaterialsList = [SELECT Id, Mix__r.Id, Material__r.Id, Material_Name__c FROM Mix_Material__c WHERE Mix__r.Id IN :unlockedOrDeletedMixesSet];

		// put these Mix_Material's IDs in a set for reference later.  (Could do this in the for loop below, but...trying to keep this easy to follow!)
		set<Id> originalMixMaterialsSet = new set<Id>();
		for(Mix_Material__c mm : originalMixMaterialsList){
			originalMixMaterialsSet.add(mm.Id);
		}
		
		// materials that might be unlock-able
		set<Id> materialsForConsideration = new set<Id>();
		for(Mix_Material__c mm : originalMixMaterialsList){
			materialsForConsideration.add(mm.Material__c);
		}
		
		// see what other LOCKED Mixes these Materials are in. (Exclude mix_material records in the unlockedOrDeletedMixes that were passed in to this method. The trigger needs to run before delete, so those materials would otherwise still be in the database, with isLocked == true!)
		list<Mix_Material__c> otherLockedMixesMaterials = [SELECT Id, 
														  Material__c, 
														  Material__r.Material_Name__c,
														  Mix__r.isLocked__c 
													 FROM Mix_Material__c 
													WHERE Material__c IN :materialsForConsideration
													  And Mix__r.isLocked__c = true 
													  AND Id NOT IN :originalMixMaterialsSet];
		
		// remove any otherMixMaterial records that are in locked mixes from the materialsForConsideration
		set<Id> materialsThatAreNotInLockedMixes = new set<Id>();
		for(Mix_Material__c mm : otherLockedMixesMaterials){
			materialsForConsideration.remove(mm.Material__c);
		}
		
		list<Material__c> materialsToUnlock = [SELECT Id, Material_Name__c, isLocked__c FROM Material__c WHERE Id IN :materialsForConsideration];
		for(Material__c m : materialsToUnlock){
			m.isLocked__c = false;
		}
		
		sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = true;
        sika_SCCZoneMaterialTriggerHelper.bypassAccountTeamCheck = true;
        sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = true;

        update(materialsToUnlock);

        sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = false;
        sika_SCCZoneMaterialTriggerHelper.bypassAccountTeamCheck = false;
        sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = false;

    }

}