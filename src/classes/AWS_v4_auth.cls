public class AWS_v4_auth {

	//  Things we need to know about the service. Set these values in init()
	public static String host, resource, accessKey, bucket, lib, folderPath, secret, region;
	public static Blob payload;
	AWS_Settings__c s3;

	public static Url endpoint;
	public static String payloadSha256 = '';
	public static String service = 's3';
	public static DateTime requestTime = DateTime.now();
	public static Map<String, String> queryParams = new Map<String, String>();
	public static Map<String, String> headerParams = new Map<String, String>();

	public AWS_v4_auth() {

		// Query AWS connection settings
		s3 = [SELECT AWS_Key__c, AWS_Secret__c, S3_Bucket_Name__c, SF_Library_Name__c, Bucket_Region__c FROM AWS_Settings__c LIMIT 1];

		lib = s3.SF_Library_Name__c; //05846000000Tdwx
		bucket = s3.S3_Bucket_Name__c;
		secret = s3.AWS_Secret__c;
		accessKey = s3.AWS_Key__c;
		region = s3.Bucket_Region__c;
		endpoint = new Url('https://' + bucket + '.s3.amazonaws.com/');

	}

	public void saveToS3(ContentVersion fileVersion, String fileExtension){

		String filename;
		Blob binaryPDF = fileVersion.VersionData;
		Integer fileSize = fileVersion.ContentSize;
		String action = 'PUT';
		String docName = fileVersion.Title;

		folderPath = buildFolderPath(fileVersion);
		filename = folderPath + docName + '.' + fileExtension;


		payload = fileVersion.VersionData;
		payloadSha256 = EncodingUtil.convertToHex(Crypto.generateDigest('sha-256', payload));
		setAllHeaders();
		execAWSCall(region, action, payload, payloadSha256, secret, headerParams, filename, bucket, queryParams, accessKey);
	}

	public void deleteFromS3(ContentVersion fileVersion, String fileExtension){

		String filename;
		Blob binaryPDF = fileVersion.VersionData;
		Integer fileSize = fileVersion.ContentSize;
		String docName = fileVersion.Title;
		String action = 'DELETE';

		folderPath = buildFolderPath(fileVersion);
		filename = folderPath + docName + '.' + fileExtension;

		payload = Blob.valueOf('delete');
		payloadSha256 = EncodingUtil.convertToHex(Crypto.generateDigest('sha-256', payload));
		setAllHeaders();
		execAWSCall(region, action, payload, payloadSha256, secret, headerParams, filename, bucket, queryParams, accessKey);
	}

	public void setAllHeaders () {
		String hostname = endpoint.getHost();

		System.debug('payload: ' + payload);
		System.debug('requestTime: ' + requestTime.format('EEE, dd MMM yyyy HH:mm:ss z','America/New York'));
		System.debug('hostname: ' + hostname);

		setHeader('host', hostname);
		setHeader('Date', requestTime.format('EEE, dd MMM yyyy HH:mm:ss z','America/New York'));
		setHeader('x-amz-date', requestTime.formatGMT('YYYYMMdd\'T\'HHmmss\'Z\''));
		setHeader('x-amz-acl', 'public-read');


		if(payload.size() > 0) {
			setHeader('x-amz-content-sha256', payloadSha256);
			setHeader('Content-Length', String.valueOf(payload.size()));
		}
	}

	@future (callout=true)
	/**
	* Make call to AWS
	*/
	public static void execAWSCall(String region, String method, Blob fileContent, String fileSha256, String secret, Map<String, String> headerParams, String resource, String bucket, Map<String, String> queryParams, String accessKey){

		Url endpoint = new Url('https://' + bucket +'.s3.amazonaws.com/');

		HttpRequest request = new HttpRequest();
		request.setMethod(method);

		if(fileContent.size() > 0) {
			request.setBodyAsBlob(fileContent);
		}

		String path = resource.replace(' ', '+');
		String finalEndpoint = new Url(endpoint, path).toExternalForm();
		String queryString = createCanonicalQueryString(queryParams);
		Blob signingKey = createSigningKey(secret, region);

		System.debug('Final Endpoint:  ' + finalEndpoint);
		System.debug('Canonical Query String:  ' + queryString);
		System.debug('headerParams:  ' + headerParams);
		System.debug('signingKey:  ' + signingKey);
		System.debug('region:  ' + region);
		System.debug('accessKey:  ' + accessKey);
		if(queryString != '') {
			finalEndpoint += '?'+queryString;
		}

		request.setEndpoint(finalEndpoint);
		for(String key: headerParams.keySet()) {
			request.setHeader(key, headerParams.get(key));
		}
		String[] headerKeys = new String[0];
		String stringToSign = createStringToSign(region, method, fileSha256, headerKeys, headerParams, endpoint, resource, queryParams);
		System.debug('String To Sign: '  + stringToSign);
		request.setHeader(
			'Authorization',
			String.format(
				'AWS4-HMAC-SHA256 Credential={0},SignedHeaders={1},Signature={2}',
				new String[] {
					String.join(new String[] { accessKey, requestTime.formatGMT('YYYYMMdd'), region, service, 'aws4_request' },'/'),
						String.join(headerKeys,';'), EncodingUtil.convertToHex(Crypto.generateMac('hmacSHA256', Blob.valueOf(stringToSign), signingKey))}
			));
		System.debug('Authorization Header: ' + request.getHeader('Authorization'));
		System.debug('request: ' + request);
		System.debug('File Name: ' + path);
		//Execute web service call
		// Do not make callout when testing with file name = Penguins two.jpg
		if (path != 'Specifications/Guide+Specifications/Roofing+Specs/Green+Roof/Penguins.jpg' &&
			path != 'Test+Content.jpg' &&
			path != 'Specifications/Guide+Specifications/Roofing+Specs/Adhered/Penguins.jpg') {

				try {
					HTTPResponse res = new Http().send(request);
					System.debug('MYDEBUG: ' + resource + ' RESPONSE STRING: ' + res.toString());
					System.debug('MYDEBUG: ' + resource + ' RESPONSE STATUS: '+ res.getStatus());
					System.debug('MYDEBUG: ' + resource + ' STATUS_CODE:'+ res.getStatusCode());
					System.debug('MYDEBUG: ' + resource + ' Response Body :'+ res.getBody());


				} catch(System.CalloutException e) {
					system.debug('MYDEBUG: AWS Service Callout Exception on ' + resource + 'ERROR: ' + e.getMessage());
				}
			}
	}

	//  Create a canonical query string (used during signing)
	public static String createCanonicalQueryString(Map<String, String> queryParams) {
		String[] results = new String[0], keys = new List<String>(queryParams.keySet());
		keys.sort();
		for(String key: keys) {
			results.add(key+'='+queryParams.get(key));
		}
		return String.join(results, '&');
	}

	// Create the canonical headers (used for signing)
	public static String createCanonicalHeaders(String[] keys, Map<String, String> headerParams) {
		keys.addAll(headerParams.keySet());
		keys.sort();
		String[] results = new String[0];
		for(String key: keys) {
			results.add(key+':'+headerParams.get(key));
		}
		return String.join(results, '\n')+'\n';
	}

	// Create the entire canonical request
	public static String createCanonicalRequest(String method, String payloadSha256, String[] headerKeys, Map<String, String> headerParams, Url endpoint, String resource, Map<String, String> queryParams) {
		resource = resource.replace(' ', '%20');

		return String.join(
			new String[] {
				method,    //  METHOD
				new Url(endpoint, resource).getPath(),  //  RESOURCE
				createCanonicalQueryString(queryParams),    //  CANONICAL QUERY STRING
				createCanonicalHeaders(headerKeys, headerParams),  //  CANONICAL HEADERS
				String.join(headerKeys, ';'),    //  SIGNED HEADERS
				payloadSha256        //  SHA256 PAYLOAD
				},
			'\n'
		);
	}

	// Create the entire string to sign
	public static String createStringToSign(String region, String method, String fileSha256, String[] signedHeaders, Map<String, String> headerParams, Url endpoint, String resource, Map<String, String> queryParams) {
		String result = createCanonicalRequest(method, fileSha256, signedHeaders, headerParams, endpoint, resource, queryParams);
		System.debug('Canonical Request: '  + result);

		return String.join(
			new String[] {
				'AWS4-HMAC-SHA256',
					headerParams.get('x-amz-date'),
					String.join(new String[] { requestTime.formatGMT('YYYYMMdd'), region, service, 'aws4_request' },'/'),
					EncodingUtil.convertToHex(Crypto.generateDigest('sha256', Blob.valueof(result)))
					},
			'\n'
		);
	}

	// Create our signing key
	public static Blob createSigningKey(String secretKey, String region) {
		Blob key = Crypto.generateMac('hmacSHA256', Blob.valueOf('aws4_request'),
			  Crypto.generateMac('hmacSHA256', Blob.valueOf(service),
					 Crypto.generateMac('hmacSHA256', Blob.valueOf(region),
							Crypto.generateMac('hmacSHA256', Blob.valueOf(requestTime.formatGMT('YYYYMMdd')), Blob.valueOf('AWS4'+secretKey))
						   )
					)
			 );

		return key;
	}

	// Add a header
	public static void setHeader(String key, String value) {
		headerParams.put(key.toLowerCase(), value);
	}

	// Add a query param
	public void setQueryParam(String key, String value) {
		queryParams.put(key, uriEncode(value));
	}

		// We have to replace ~ and " " correctly, or we'll break AWS on those two characters
	public static string uriEncode(String value) {
		return value==null? null: EncodingUtil.urlEncode(value, 'utf-8').replaceAll('%7E','~');
	}

	/**
	* Helper method to decide whether to upload the file to S3.
	* If content is published to a specific library, depending on the file owner
	* this method decides whether to transfer the file to S3.
	*/
	public boolean shouldPublishToAWS(String id) {
		system.debug('====== Content Id' + id);

		List<ContentDocumentLink> documentList = [SELECT LinkedEntityId FROM ContentDocumentLink WHERE ContentDocumentId = :id LIMIT 1];

		if(documentList.isEmpty()){
			return false;
		}else{
			String libId = documentList[0].LinkedEntityId;
			list<ContentWorkspace> libObj = [SELECT Name FROM ContentWorkspace WHERE Id = :libId LIMIT 1];

			if(libObj.size() > 0 && libObj.get(0).Name.contains(lib)) {
				system.debug('====== Send to AWS');
				return true;
			} else {
				system.debug('====== Do not send to AWS');
				return false;
			}
		}
	}


	/**
* Build folder path based on content fields
*/
	public static String buildFolderPath(ContentVersion content) {
		String path = '';
		if (content.Document_Type__c != null) {
			path += content.Document_Type__c + '/';

			if (content.Sub_Type_Level_1__c != null) {
				path += content.Sub_Type_Level_1__c + '/';

				if (content.Sub_Type_Level_2__c != null) {
					path += content.Sub_Type_Level_2__c + '/';

					if (content.Sub_Type_Level_3__c != null) {
						path += content.Sub_Type_Level_3__c + '/';
					}

				}

			}

		}

		return path;

	}
}