@isTest
private class SAPIntegrationServiceTest {

    @testSetup
    static void testSetup() {
        Opportunity oppty = TestUtilities.createOpportunities(TestUtilities.createAccounts(1, true), 1, false).get(0);
        oppty.Language__c = 'EN';
        oppty.SAP_Name_40__c = 'SAP Name 40';
        oppty.Project__c = TestUtilities.createProjects(1, true).get(0).Id;
        insert oppty;

        Id standardUserId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id;

        User salespersonUser1 = TestUtilities.createUser(standardUserId, 'test1', false);
        salespersonUser1.Business_Unit__c = 'Roofing';
        User salespersonUser2 = TestUtilities.createUser(standardUserId, 'test2', false);
        salespersonUser2.Business_Unit__c = 'Roofing';
        insert new List<User>{salespersonUser1, salespersonUser2};

        //Credit_Split__c split1 = TestUtilities.createCreditSplits(oppty, salespersonUser1, 'Applicator', false);
        //Credit_Split__c split2 = TestUtilities.createCreditSplits(oppty, salespersonUser2, 'Location', false);
        //insert new List<Credit_Split__c>{split1, split2};
    }

    static testmethod void testCreateContractInSAP() {
        Opportunity oppty = queryOpportunity();

        Test.setMock(WebServiceMock.class, new SAPContractIntegrationMockImpl());

        Test.startTest();
        String contractNumber = SAPIntegrationService.createContractInSAP(oppty);
        Test.stopTest();

        System.assertEquals(SAPContractIntegrationMockImpl.CONTRACT_NUMBER, contractNumber,
            'It should return the contract number.');
    }

    static testmethod void testCreateContractInSAPMissingField() {
        Opportunity oppty = queryOpportunity();
        oppty.SAP_Name_40__c = null;
        update oppty;

        Test.setMock(WebServiceMock.class, new SAPContractIntegrationMockImpl());

        Exception catchedException;
        Test.startTest();
        try {
            String contractNumber = SAPIntegrationService.createContractInSAP(oppty);
        } catch(SAPIntegrationService.SAPIntegrationServiceException e) {
            catchedException = e;
        }
        Test.stopTest();

        System.assertNotEquals(null, catchedException, 'It should have thrown an exception.');
        System.assert(catchedException.getMessage().contains(SAPContractIntegrationMockImpl.JOB_NAME_MISSING),
            'It should have thrown the right exception.');
    }

    private static Opportunity queryOpportunity() {
        return [
            SELECT SAP_Name_40__c, Opportunity_Number__c, SAP_Name_20__c, Street_Address__c, Zip__c,
                   City__c, Country__c, State__c, Transportation_Zone__c, Language__c, SAP_Industry_Key__c, Industry_Code__c,
                   Sales_Organization__c, Distribution__c, Division__c, Shipping_Condition__c, Warehouse__c, Incoterm1__c,
                   Incoterm2__c, Tax_Country_MX__c, Tax_Category_MX__c, Tax_Classification_MX__c, Tax_Country_US__c,
                   Tax_Category_US__c, Tax_Classification_US__c, Project_AccountId__c, ShipTo_AccountId__c,
                   Customer_Group__c, Account.SAP_Account_Code__c, SSI__c, SAP_Order_Reason__c, SAP_Account_Assign__c, RecordType.Name,
                   SAP_Vertical_Market_Code__c, SAP_Primary_System_Code__c, SAP_Primary_System_Area__c, SAP_Primary_System_UOM__c
            FROM Opportunity LIMIT 1
        ];
    }
}