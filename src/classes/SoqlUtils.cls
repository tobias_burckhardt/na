/**
 * Provides methods to generate dynamic SOQL using an object-oriented Builder pattern
 * inspired by the Hibernate criteria API.
 * @author brendan.conniff
 */
public with sharing class SoqlUtils {

	/**
	 * Creates a new SELECT query.
	 * @param fields  list of field names to query
	 * @param table   sObject name to query from 
	 * @return  a SELECT query
	 */
	public static SoqlQuery getSelect(List<String> fields, String table) {
		String soql = 'SELECT ';
		return new SoqlQuery('SELECT ' + String.join(fields, ', ') + ' FROM ' + table);
	}

	/**
	 * Creates an AND condition by combining a list of child conditions.
	 * @param conditions  list of conditions to combine with AND
	 * @return  an AND condition
	 */
	public static SoqlCondition getAnd(List<SoqlCondition> conditions) {
		List<SoqlCondition> keptConditions = new List<SoqlCondition>();
		Boolean anyAlwaysTrue = false;

		for (SoqlCondition condition: conditions) {
			if (condition.isAlwaysTrue()) {
				anyAlwaysTrue = true;
				continue; // skip conditions that are always true
			}
			if (condition.isAlwaysFalse()) {
				return new SoqlConditionAlwaysFalse(); // if any condition is always false, so is the AND
			}
			keptConditions.add(condition);
		}

		// if all conditions are always true, so is the AND
		if (keptConditions.isEmpty() && anyAlwaysTrue) {
			return new SoqlConditionAlwaysTrue();
		}

		String soql = '(';
		String sep = '';
		for (SoqlCondition condition: keptConditions) {
			soql += sep + condition.toString();
			sep = ' AND ';
		}
		return new SoqlCondition(soql+')');
	}

	/**
	 * Creates an equality condition.
	 * @param field  field name to test in the comparison
	 * @param value  value to compare the field to
	 * @return  an equality condition
	 */
	public static SoqlCondition getEq(String field, Boolean value) {
		return new SoqlCondition(field + ' = ' + String.valueOf(value));
	}

	/**
	 * Creates an equality condition.
	 * @param field  field name to test in the comparison
	 * @param value  value to compare the field to
	 * @return  an equality condition
	 */
	public static SoqlCondition getEq(String field, String value) {
		return new SoqlCondition(field + ' = \'' + String.escapeSingleQuotes(value) + '\'');
	}

	/**
	 * Creates an INCLUDES condition to test if a field contains all the given values.
	 * @param field   field name to test in the condition
	 * @param values  list of values to check for
	 * @return  an INCLUDES condition
	 */
	public static SoqlCondition getIncludesAll(String field, List<String> values) {
		if (values.isEmpty()) {
			return new SoqlConditionAlwaysFalse();
		}
		String sep = '';
		String escaped = '';
		for (String value : values) {
			escaped += sep + String.escapeSingleQuotes(value);
			sep = ';';
		}
		return new SoqlCondition(field + ' INCLUDES (\'' + escaped + '\')');
	}

	/**
	 * Provides methods to generate dynamic SOQL queries using an object-oriented Builder pattern
	 * inspired by the Hibernate criteria API. A SoqlQuery can't be instantiated directly, but can
	 * be created using the factory method <code>SoqlUtils.getSelect(...)</code>.
	 * @author brendan.conniff
	 * @see SoqlUtils.getSelect
	 */
	public virtual class SoqlQuery {
		private final String q;
		private SoqlCondition whereClause;

		private SoqlQuery(String q) {
			this.q = q;
		}

		/**
		 * Sets the WHERE clause of this query to check for the given condition.
		 * @param whereClause  condition to use in WHERE clause for this query
		 * @return this SoqlQuery
		 */
		public SoqlQuery withCondition(SoqlCondition whereClause) {
			this.whereClause = whereClause;
			return this;
		}

		/**
		 * Retrieves the SOQL for this query as a String, suitible for passing to <code>Database.query(...)</code>.
		 * @return the SOQL String for this query.
		 */
		public virtual override String toString() {
			String soql = q;
			if (whereClause != null && !whereClause.isAlwaysTrue()) {
				soql += ' WHERE '+whereClause.toString();
			}
			System.debug('[SoqlUtils] ' + soql);
			return soql;
		}
	}

	/**
	 * Represents a condition which may be used as part of a SOQL WHERE clause. A SoqlCondition can't be
	 * instantiated directly, but can be created using any of the factory methods in SoqlUtils with a return
	 * type of SoqlCondition.
	 * @author brendan.conniff
	 */
	public virtual class SoqlCondition {
		private final String q;

		private SoqlCondition(String q) {
			this.q = q;
		}

		public virtual Boolean isAlwaysTrue() {
			return false;
		}

		public virtual Boolean isAlwaysFalse() {
			return false;
		}

		/**
		 * Converts this condition to the SOQL String it represents.
		 * This should not be used directly, as it will not generate a full, valid query.
		 * @return the SOQL String for this condition
		 */
		public override String toString() {
			return q;
		}
	}

	private class SoqlConditionAlwaysTrue extends SoqlCondition {
		private SoqlConditionAlwaysTrue() {
			super('Id != null');
		}

		public override Boolean isAlwaysTrue() {
			return true;
		}
	}

	private class SoqlConditionAlwaysFalse extends SoqlCondition {
		private SoqlConditionAlwaysFalse() {
			super('Id = null');
		}

		public override Boolean isAlwaysFalse() {
			return true;
		}
	}
}