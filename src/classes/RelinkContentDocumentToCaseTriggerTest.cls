@isTest
public class RelinkContentDocumentToCaseTriggerTest {

    @testSetup
    static void setUpTests() {
        AWS_Settings__c awsSetting = new AWS_Settings__c(AWS_Key__c = 'Test', S3_Bucket_Name__c = 'Test', SF_Library_Name__c = 'Test');
        insert awsSetting;
        
        RestrictedCaseTestHelper.createCustomSettings();
    }
    
    @isTest
    static void testDeleteChatterFilePost_UnrestrictedCase() {
        //Trying to delete a FeedItem with a ContentVersion attached to a case with an unrestricted record type should succeed
        Case testCase = RestrictedCaseTestHelper.createTestCase(false);
        List<FeedItem> testFI = CaseEmailChatterFileTestHelper.createTestEmailAttachment(testCase.Id, 1);
        ContentVersion cv = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :testFI[0].RelatedRecordId];

        Test.startTest();
        List<ContentDocumentLink> beforeDeleteLinks = [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId = :testCase.Id AND ContentDocumentId = :cv.ContentDocumentId]; 
        System.assert(beforeDeleteLinks.size() == 1);
        delete testFI[0];
        
        List<ContentDocumentLink> afterDeleteLinks = [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId = :testCase.Id AND ContentDocumentId = :cv.ContentDocumentId]; 
        System.assert(afterDeleteLinks.size() == 0);

        Test.stopTest();
    }
    
     @isTest
    static void testDeleteChatterFilePost_RestrictedCase() {
        //Trying to delete a FeedItem with a ContentVersion attached to a case with an restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<FeedItem> testFI = CaseEmailChatterFileTestHelper.createTestEmailAttachment(testCase.Id, 1);
        ContentVersion cv = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :testFI[0].RelatedRecordId];

        Test.startTest();
        List<ContentDocumentLink> beforeDeleteLinks = [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId = :testCase.Id AND ContentDocumentId = :cv.ContentDocumentId]; 
        System.assert(beforeDeleteLinks.size() == 1);
        delete testFI[0];
        
        List<ContentDocumentLink> afterDeleteLinks = [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId = :testCase.Id AND ContentDocumentId = :cv.ContentDocumentId]; 
        System.assert(afterDeleteLinks.size() == 1);

        Test.stopTest();
    }
    
}