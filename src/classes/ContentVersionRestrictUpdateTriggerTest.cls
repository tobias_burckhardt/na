@isTest
public class ContentVersionRestrictUpdateTriggerTest {
    
    @testSetup
    static void setUpTests() {
        AWS_Settings__c awsSetting = new AWS_Settings__c(AWS_Key__c = 'Test', S3_Bucket_Name__c = 'Test', SF_Library_Name__c = 'Test');
        insert awsSetting;

        RestrictedCaseTestHelper.createCustomSettings();
    }

    @isTest
    static void testUpdateUnrestrictedOnCase() {
        //Trying to update a ContentVersion attached to a case with an unrestricted record type should succeed
        Case testCase = RestrictedCaseTestHelper.createTestCase(false);
        List<FeedItem> testFI = CaseEmailChatterFileTestHelper.createTestEmailAttachment(testCase.Id, 1);
        List<ContentVersion> testCV = [SELECT Id, Title, Description FROM ContentVersion WHERE Id = :testFI[0].RelatedRecordId];

        Test.startTest();
        testCV[0].Title = 'New Title';
        Database.SaveResult updateResult = Database.update(testCV[0], false);
        System.assert(updateResult.isSuccess());

        Test.stopTest();
    }
    
    @isTest
    static void testUpdateRestrictedOnCase() {
        //Trying to update a ContentVersion attached to a case with an restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<FeedItem> testFI = CaseEmailChatterFileTestHelper.createTestEmailAttachment(testCase.Id, 1);
        List<ContentVersion> testCV = [SELECT Id, Title, Description FROM ContentVersion WHERE Id = :testFI[0].RelatedRecordId];

        Test.startTest();
        testCV[0].Title = 'New Title';
        Database.SaveResult updateResult = Database.update(testCV[0], false);
        System.assert(!updateResult.isSuccess());

        Test.stopTest();
    }
    
    @isTest
    static void testUpdateRestrictedOnCase_Description() {
        //Trying to update a ContentVersion attached to a case with an restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<FeedItem> testFI = CaseEmailChatterFileTestHelper.createTestEmailAttachment(testCase.Id, 1);
        List<ContentVersion> testCV = [SELECT Id, Title, Description FROM ContentVersion WHERE Id = :testFI[0].RelatedRecordId];


        Test.startTest();
        testCV[0].Description = 'Description';
        Database.SaveResult updateResult = Database.update(testCV[0], false);
        System.assert(!updateResult.isSuccess());

        Test.stopTest();
    }
    
    @isTest
    static void testUpdateRestrictedOnCaseNoChange() {
        //Trying to update (with no change) a ContentVersion attached to a case with an restricted record type should succeed
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<FeedItem> testFI = CaseEmailChatterFileTestHelper.createTestEmailAttachment(testCase.Id, 1);
        List<ContentVersion> testCV = [SELECT Id, Title, Description FROM ContentVersion WHERE Id = :testFI[0].RelatedRecordId];


        Test.startTest();
        
        Database.SaveResult updateResult = Database.update(testCV[0], false);
        System.assert(updateResult.isSuccess());

        Test.stopTest();
    }
}