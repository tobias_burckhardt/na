@isTest
private class ContentFilesControllerTest {

	private final static Integer CREATE_RECORDS = 5;

	@TestSetup
	static void setupContentVersions() {
		AWS_Settings__c awsSetting = new AWS_Settings__c(AWS_Key__c = 'Test', S3_Bucket_Name__c = 'Test', SF_Library_Name__c = 'Test');
      	insert awsSetting;
      	
		TestingUtils.createContentVersions(CREATE_RECORDS, 'Title', true);
	}

	static testmethod void contentFilesControllerTest() {
		Test.startTest();
		ContentFilesController ctrl = new ContentFilesController();
		Test.stopTest();

		System.assert(!ctrl.hasNextPage, 'It should not have a next page.');
		System.assert(!ctrl.hasPreviousPage, 'It should not have a previous page.');
		System.assertEquals(CREATE_RECORDS, ctrl.numberOfRecords,
			'It should have retrieved the right number of records.');
		System.assertEquals('1 - 5 of 5', ctrl.recordsDisplayed, 'It should display the right message.');
	}

	//static testmethod void nextPageTest() {
	//    ContentFilesController ctrl = new ContentFilesController();

	//	Test.startTest();
	//	ctrl.nextPage();
	//	Test.stopTest();

	//	System.assert(!ctrl.hasNextPage, 'It should not have a next page.');
	//	System.assert(ctrl.hasPreviousPage, 'It should have a previous page.');
	//	System.assertEquals('501 - 501 of ' + CREATE_RECORDS, ctrl.recordsDisplayed,
	//		'It should display the right message.');
	//}

	//static testmethod void previousPageTest() {
	//    ContentFilesController ctrl = new ContentFilesController();
	//	ctrl.nextPage();

	//	Test.startTest();
	//	ctrl.previousPage();
	//	Test.stopTest();

	//	System.assert(ctrl.hasNextPage, 'It should have a next page.');
	//	System.assert(!ctrl.hasPreviousPage, 'It should not have a previous page.');
	//	System.assertEquals('1 - ' + ContentFilesController.PAGE_RECORDS + ' of ' + CREATE_RECORDS,
	//		ctrl.recordsDisplayed, 'It should display the right message.');
	//}
}