@isTest
private class sika_SCCZoneSortListHelperTest {

	private static list<Case> casesToSort = new list<Case>();
	private static list<Case> sortedCases;

	private static list<Mix__c> mixesToSort = new list<Mix__c>();
	private static list<Mix__c> sortedMixes = new list<Mix__c>();

	private static Integer i;
	
	// static initializer
	static {
        Case testCaseRecord;

        // create 5 test Cases
        for(i=5; i > 0; i--){
	        testCaseRecord = sika_SCCZoneTestFactory.createCase(
        		new map<String, String>{'dueDateYear' => String.valueOf(2010 + i),
										'dueDateMonth' => '01',							// dateTime
										'dueDateDay' => '01',
										'origin' => String.valueOf('Origin ' + i),   	// picklist
										'noAutoResponse' => i > 2 ? 'true' : 'false' ,	// boolean
										'subject' => String.valueOf('Subject ' + i) 	// string
			});
	        casesToSort.add(testCaseRecord);
    	}

    	Mix__c testMixRecord;

    	// create 5 test Mixes
    	for(i=5; i > 0; i--){
    		testMixRecord = sika_SCCZoneTestFactory.createMix(
    			new map<String, String>{'cementCompStrength' => string.valueOf(i),  					// integer
    									'aggEffFactor' => string.valueOf(i) + '.' + string.valueOf(i),	// decimal
    									'targetAir' => string.valueOf(i) + '.' + string.valueOf(i)		// percent   
    		});
    		mixesToSort.add(testMixRecord);    		
    	}

	}

    @isTest static void testStringSort(){

    	// sort strings descending
    	sortedCases = (list<Case>) sika_SCCZoneSortListHelper.sortList(casesToSort, 'subject', 'DESC');
    	system.assertEquals(sortedCases.size(), 5);
    	system.assertEquals(sortedCases[0].subject, 'Subject 5');
    	system.assertEquals(sortedCases[4].subject, 'Subject 1');
    	
    	// sort strings ascending
    	sortedCases = (list<Case>) sika_SCCZoneSortListHelper.sortList(casesToSort, 'subject'); // not passing a sort direction parameter will default the sort to
    	system.assertEquals(sortedCases[0].subject, 'Subject 1');
    	system.assertEquals(sortedCases[4].subject, 'Subject 5');

    	// include two null values and test again
    	system.assertEquals(casesToSort[0].subject, 'Subject 5');
    	casesToSort[0].subject = null;
    	casesToSort[4].subject = null;
    	sortedCases = (list<Case>) sika_SCCZoneSortListHelper.sortList(casesToSort, 'subject', 'DESC');
		// For strings, it makes sense for null values to be after the last alphanumeric. (A, B, ..., Z, null)...so that means null will be first in the list when we sort DESC
    	system.assertEquals(sortedCases[0].subject, null);
    	system.assertEquals(sortedCases[1].subject, null);
    	system.assertEquals(sortedCases[2].subject, 'Subject 4');
    	system.assertEquals(sortedCases[3].subject, 'Subject 3');
    	system.assertEquals(sortedCases[4].subject, 'Subject 2');

    	sortedCases = (list<Case>) sika_SCCZoneSortListHelper.sortList(casesToSort, 'subject', 'ASC');
    	system.assertEquals(sortedCases[4].subject, null);
    	system.assertEquals(sortedCases[3].subject, null);
    	system.assertEquals(sortedCases[2].subject, 'Subject 4');
    	system.assertEquals(sortedCases[1].subject, 'Subject 3');
    	system.assertEquals(sortedCases[0].subject, 'Subject 2');

    }

    @isTest static void testPicklistSort(){
    	
    	// sort picklist values descending
    	sortedCases = (list<Case>) sika_SCCZoneSortListHelper.sortList(casesToSort, 'origin', 'DESC');
    	system.assertEquals(sortedCases.size(), 5);
    	system.assertEquals(sortedCases[0].origin, 'Origin 5');
    	system.assertEquals(sortedCases[4].origin, 'Origin 1');
    	
    	// sort picklist values ascending
    	sortedCases = (list<Case>) sika_SCCZoneSortListHelper.sortList(casesToSort, 'origin', 'ASC');
    	system.assertEquals(sortedCases[0].origin, 'Origin 1');
    	system.assertEquals(sortedCases[4].origin, 'Origin 5');

    	// include some null values and test again
    	system.assertEquals(casesToSort[0].origin, 'Origin 5');
    	casesToSort[0].origin = null;
        casesToSort[1].origin = null;
    	sortedCases = (list<Case>) sika_SCCZoneSortListHelper.sortList(casesToSort, 'origin', 'DESC');
    	system.assertEquals(sortedCases[0].origin, null);
    	system.assertEquals(sortedCases[4].origin, 'Origin 1');

    	sortedCases = (list<Case>) sika_SCCZoneSortListHelper.sortList(casesToSort, 'origin', 'ASC');
    	system.assertEquals(sortedCases[0].origin, 'Origin 1');
    	system.assertEquals(sortedCases[4].origin, null);

    }

    @isTest static void testIntegerSort(){
    	
    	// sort integers descending
    	sortedMixes = (list<Mix__c>) sika_SCCZoneSortListHelper.sortList(mixesToSort, 'Cement_Comp_Strength__c', 'DESC');
    	system.assertEquals(sortedMixes.size(), 5);
    	system.assertEquals(sortedMixes[0].Cement_Comp_Strength__c, 5);
    	system.assertEquals(sortedMixes[4].Cement_Comp_Strength__c, 1);
    	
    	// sort integers ascending
    	sortedMixes = (list<Mix__c>) sika_SCCZoneSortListHelper.sortList(mixesToSort, 'Cement_Comp_Strength__c', 'ASC');    	system.assertEquals(sortedMixes[0].Cement_Comp_Strength__c, 1);
    	system.assertEquals(sortedMixes[4].Cement_Comp_Strength__c, 5);

    	// include some null values and test again
    	mixesToSort[0].Cement_Comp_Strength__c = null;
        mixesToSort[1].Cement_Comp_Strength__c = null;
    	sortedMixes = (list<Mix__c>) sika_SCCZoneSortListHelper.sortList(mixesToSort, 'Cement_Comp_Strength__c', 'DESC');
    	system.assertEquals(sortedMixes[0].Cement_Comp_Strength__c, 3);
    	system.assertEquals(sortedMixes[4].Cement_Comp_Strength__c, null);

    	sortedMixes = (list<Mix__c>) sika_SCCZoneSortListHelper.sortList(mixesToSort, 'Cement_Comp_Strength__c', 'ASC');
    	system.assertEquals(sortedMixes[0].Cement_Comp_Strength__c, null);
    	system.assertEquals(sortedMixes[4].Cement_Comp_Strength__c, 3);
    }

    @isTest static void testPercentSort(){
    	
    	// sort integers descending
    	sortedMixes = (list<Mix__c>) sika_SCCZoneSortListHelper.sortList(mixesToSort, 'Target_Air_Percent__c', 'DESC');
    	system.assertEquals(sortedMixes.size(), 5);
    	system.assertEquals(sortedMixes[0].Target_Air_Percent__c, 5.5);
    	system.assertEquals(sortedMixes[4].Target_Air_Percent__c, 1.1);
    	
    	// sort integers ascending
    	sortedMixes = (list<Mix__c>) sika_SCCZoneSortListHelper.sortList(mixesToSort, 'Target_Air_Percent__c', 'ASC');
    	system.assertEquals(sortedMixes[0].Target_Air_Percent__c, 1.1);
    	system.assertEquals(sortedMixes[4].Target_Air_Percent__c, 5.5);

    	// include some null values and test again
    	mixesToSort[0].Target_Air_Percent__c = null;
        mixesToSort[1].Target_Air_Percent__c = null;
    	sortedMixes = (list<Mix__c>) sika_SCCZoneSortListHelper.sortList(mixesToSort, 'Target_Air_Percent__c', 'DESC');
    	system.assertEquals(sortedMixes[0].Target_Air_Percent__c, 3.3);
    	system.assertEquals(sortedMixes[4].Target_Air_Percent__c, null);

    	sortedMixes = (list<Mix__c>) sika_SCCZoneSortListHelper.sortList(mixesToSort, 'Target_Air_Percent__c', 'ASC');
    	system.assertEquals(sortedMixes[0].Target_Air_Percent__c, null);
    	system.assertEquals(sortedMixes[4].Target_Air_Percent__c, 3.3);

    }

    @isTest static void testBooleanSort(){
    	
    	// sort boolean values descending (False before True)
    	sortedCases = (list<Case>) sika_SCCZoneSortListHelper.sortList(casesToSort, 'Do_not_send_auto_response__c', 'DESC');
    	system.assertEquals(sortedCases.size(), 5);
    	system.assertEquals(sortedCases[0].Do_not_send_auto_response__c, false);
    	system.assertEquals(sortedCases[1].Do_not_send_auto_response__c, false);
    	system.assertEquals(sortedCases[2].Do_not_send_auto_response__c, true);
    	system.assertEquals(sortedCases[3].Do_not_send_auto_response__c, true);
    	system.assertEquals(sortedCases[4].Do_not_send_auto_response__c, true);
    	
    	// sort boolean values ascending (True before False)
    	sortedCases = (list<Case>) sika_SCCZoneSortListHelper.sortList(casesToSort, 'Do_not_send_auto_response__c', 'ASC');
    	system.assertEquals(sortedCases[0].Do_not_send_auto_response__c, true);
    	system.assertEquals(sortedCases[1].Do_not_send_auto_response__c, true);
    	system.assertEquals(sortedCases[2].Do_not_send_auto_response__c, true);
    	system.assertEquals(sortedCases[3].Do_not_send_auto_response__c, false);
    	system.assertEquals(sortedCases[4].Do_not_send_auto_response__c, false);

    	// include some null values and test again
    	casesToSort[0].Do_not_send_auto_response__c = null;
        casesToSort[1].Do_not_send_auto_response__c = null;
    	sortedCases = (list<Case>) sika_SCCZoneSortListHelper.sortList(casesToSort, 'Do_not_send_auto_response__c', 'DESC');
		system.assertEquals(sortedCases[0].Do_not_send_auto_response__c, false);
    	system.assertEquals(sortedCases[1].Do_not_send_auto_response__c, false);
    	system.assertEquals(sortedCases[2].Do_not_send_auto_response__c, true);
    	system.assertEquals(sortedCases[3].Do_not_send_auto_response__c, false);
    	system.assertEquals(sortedCases[4].Do_not_send_auto_response__c, false);  // for booleans, null == false

    	sortedCases = (list<Case>) sika_SCCZoneSortListHelper.sortList(casesToSort, 'Do_not_send_auto_response__c', 'ASC');	
		system.assertEquals(sortedCases[0].Do_not_send_auto_response__c, false);
    	system.assertEquals(sortedCases[1].Do_not_send_auto_response__c, false);
    	system.assertEquals(sortedCases[2].Do_not_send_auto_response__c, true);
    	system.assertEquals(sortedCases[3].Do_not_send_auto_response__c, false);
    	system.assertEquals(sortedCases[4].Do_not_send_auto_response__c, false);

    }

    @isTest static void testDateTimeSort(){
    	
    	// sort boolean values descending (False before True)
    	sortedCases = (list<Case>) sika_SCCZoneSortListHelper.sortList(casesToSort, 'Due_Date__c', 'DESC');
    	system.assertEquals(sortedCases.size(), 5);
    	system.assertEquals(sortedCases[0].Due_Date__c, DateTime.valueOf('2015-01-01 12:00:00'));  // uses the default hour, minute, and second from the SCCZoneTestFactory class
    	system.assertEquals(sortedCases[4].Due_Date__c, DateTime.valueOf('2011-01-01 12:00:00'));
    	
    	// sort boolean values ascending (True before False)
    	sortedCases = (list<Case>) sika_SCCZoneSortListHelper.sortList(casesToSort, 'Due_Date__c', 'ASC');
    	system.assertEquals(sortedCases[0].Due_Date__c, DateTime.valueOf('2011-01-01 12:00:00'));
    	system.assertEquals(sortedCases[4].Due_Date__c, DateTime.valueOf('2015-01-01 12:00:00'));

    	// include one null value and test again
    	casesToSort[0].Due_Date__c = null;
        casesToSort[1].Due_Date__c = null;
    	sortedCases = (list<Case>) sika_SCCZoneSortListHelper.sortList(casesToSort, 'Due_Date__c', 'DESC');	
		system.assertEquals(sortedCases[0].Due_Date__c, null);
    	system.assertEquals(sortedCases[4].Due_Date__c, DateTime.valueOf('2011-01-01 12:00:00'));

    	sortedCases = (list<Case>) sika_SCCZoneSortListHelper.sortList(casesToSort, 'Due_Date__c', 'ASC');
    	system.assertEquals(sortedCases[0].Due_Date__c, DateTime.valueOf('2011-01-01 12:00:00'));
    	system.assertEquals(sortedCases[4].Due_Date__c, null);

    }

}