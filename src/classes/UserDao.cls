/**
* @author Garvit Totuka
* @date 18 Sep 2017
*
* @group user Sharing Management
*
* @description User Helper class to make generic mathods related to User Object
*/
public with sharing class UserDao {
    
    
    
    /**
    * @description Fetch Users as a list based on incoming User Ids
    * @param userIds Set of User Ids
    * @return list of users. 
    * @Condition User.IsActive : true AND User.IsPortalEnabled : true  User should be Active and have Portal Enabled
    */  
    public static List<User> getUsersByIds(Set<Id> userIds) {
        
        List<User> users = [SELECT Id, ContactId, IsActive, AccountId, ProfileId 
                                FROM User 
                                WHERE Id IN :userIds AND IsActive = :true AND IsPortalEnabled = :true
                           ];        
        return users;
        
    }
    
    /**
    * @description Fetch Partner Profiles as a list based on incoming profile names from Custom Label
    * Custome Label "PartnerUserProfileNames" should have comma (,) separator
    * (for eg. Partner Community User,PTS Partner Community,...)
    * @param Nill
    * @return list of profiles
    */
    public static List<Profile> getUserPartnerProfileIds() {
        return userPartnerProfileIds;
    }

    public static List<Profile> userPartnerProfileIds {
        get {
            if (userPartnerProfileIds == null) {
                Set<String> profileName = new Set<String>();
                for(String str: Label.PartnerUserProfileNames.split(',')) {
                    profileName.add(str);
                }
                userPartnerProfileIds = [SELECT Id, Name FROM Profile WHERE Name IN :profileName];
            }
            return userPartnerProfileIds;
        }
        set;
    }
    
    /**
    * @description Fetch Admin Users as a list based on incoming profile names from Custom Label
    * Custome Label "AdminUserNamesForSendingMail" should have comma (,) separator
    * (for eg. Bluewolf Admin,Mike Estes,...)
    * @param Nill
    * @return list of profiles
    */
    public static List<User> getAdminUsersByUserName() {
        Set<String> userName = new Set<String>();
        for(String str: Label.AdminUserNamesForSendingMail.split(',')) {
            userName.add(str);
        }
        List<User> users = [SELECT Id, Name , Email, IsActive
                                    FROM User
                                    WHERE UserName IN :userName AND IsActive = :true
                                 ];
        return users;
    } 
    
    /**
    * @description Fetch Accounts as a list based on incoming Users List
    * @param users List of User
    * @return list of Account having one or more opportunities
    */
    public static List<Account> getAccountAndOpptyByUserList(List<User> users ) {
        
        Set<Id> accountIds = new Set<Id>();
        for(User usr:users) {
            if(usr.ContactId != NULL && usr.AccountId != NULL) {
                accountIds.add(usr.AccountId);
            }
        }
        
        List<Account> accountList = new List<Account>();
        if(accountIds != NULL ) {
            accountList = [SELECT Id, Name, (SELECT Id, Name, AccountId FROM Opportunities) 
                            FROM Account 
                            WHERE Id IN:accountIds
                          ];
        }
        
        return accountList;     
    }
    
    /**
    * @description creates a map of list of users and profile id based on incoming user list using custom label for Profiles
    * @param 
    * @return map of list of Users based on profile ids
    */
    public static Map<Id, List<User>> createUsersByProfileId(List<User> users) {
        List<Profile> profiles = UserDao.getUserPartnerProfileIds();
        Map<Id, List<User>> userByprofileId = new Map<Id, List<User>>();
        for(Profile pr :profiles) {
            for(User usr :users) {
                if(pr.Id == usr.ProfileId) {
                    if(!userByprofileId.containsKey(usr.ProfileId)) {
                        userByprofileId.put(usr.ProfileId, new List<User>());
                    }
                    userByprofileId.get(usr.ProfileId).add(usr);
                }
            }
            
        }
        return userByprofileId;
    }
    
    /**
    * @description Create set of User Ids By Account Id as a map based on incoming Users List
    * @param users List of User
    * @return Map of set of Users By Accounts Key : Account Id Value: Set of User Id. 
    * @Ccondition User.ContactId : NOT NULL AND User.AccountId : NOT NULL User should have ContactId and AccountId
    */
    public static Map<Id,set<Id>> createSetOfUserIdsByAccountId(List<User> users ) { 
 
        Map<Id,set<Id>> userIdsByAccountId = new Map<Id,set<Id>>();
        for(User usr: users) {
            if(usr.ContactId != NULL && usr.AccountId != NULL) {
                if(!userIdsByAccountId.containsKey(usr.AccountId)) {
                    userIdsByAccountId.put(usr.AccountId, new Set<Id>());
                }
                userIdsByAccountId.get(usr.AccountId).add(usr.Id);
            }
        }
        
        return userIdsByAccountId;
    }
    
    /**
    * @description send mail to admin users when exception occurs 
    * @param exception any type of exception to come
    * @return send a mail to Admin users 
    */
    public static void exceptionMailToAdminUsers(Exception ex) {
        List<User> adminUsers = UserDao.getAdminUsersByUserName();
        String subject = 'An Exception has Occurred : '+ex.getTypeName();
        String errorMessage;
        errorMessage += 'ERROR: ' + 'An exception has occurred. -- '+ ex.getTypeName() + ':'+ex.getMessage() + ':' + ex.getLineNumber() + ':' + ex.getStackTraceString();
        String htmlBody = '<html lang="en"><body>'+
                            '<br><br>'+
                            'This email alert is to bring to your notice that exception occured  '+
                            '<br><br>'+
                            '<b>'+
                            'Here is detail of Exception '+
                            '</b>'+
                            '<br><br>'+ errorMessage +
                            '</body></html>';
        
        sendEmailToUsers(adminUsers,subject,htmlBody);                     
        
    }
    
    /**
    * @description send mail to users based on profiles, subject and body
    * @param profiles list of profiles
    * @param subject determine the subject line of mail
    * @param htmlBody determine the body of mail in HTML format
    * @return send a mail to Admins 
    */
    public static void sendEmailToUsers(List<User> users, String subject, String htmlBody) {
        
        //List<User> adminUsers = UserDao.getUsersByProfiles(profiles);
        List<String> toAddresses = new List<String>();
        for(User usr :users) {
            if (usr.IsActive == true && usr.Email != NULL) {
            toAddresses.add(usr.Email);
            }
        }
    
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage() ;
            mail.setToAddresses(toAddresses) ;
            mail.setSubject(subject);
            mail.setHtmlBody(htmlBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}