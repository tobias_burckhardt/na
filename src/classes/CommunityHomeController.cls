/* **********************************************************
Name        :CommunityHomeController
Description :An apex page controller that takes the user to 
            the right start page based on credentials or lack thereof
Author      :Shradha Bansal
Created On  :9/January/2015
Modified On :17/February/2015
*********************************************************** -->
 */
public without sharing class CommunityHomeController {
    public List<Warranty__c> warrList {get; set;}
    public List<Project__c> projList {get; set;}
    public List<Account> acctList {get; set;}
    // Code we will invoke on page load.
    public PageReference forwardToCustomAuthPage() {
        if(UserInfo.getUserType() == 'Guest'){
            return new PageReference('/CommunityLogin');
        }
        else{
            return null;
        }
    }

    public CommunityHomeController() {
        string userId = UserInfo.getUserId();
        
        //get all warranties for this user
        string warrFieldList = Utilities.GetAllObjectFields('Warranty__c');
        //append relational project fields to field list
        warrFieldList += ',Project__r.Name,Project__r.Id';
        string queryFormat = 'select {0} from Warranty__c  where Community_User_ID__c = :userId ORDER BY LastModifiedDate DESC Limit 10';
        string warrQuery = String.format(queryFormat, new List<string>{warrFieldList});
        
        try{
            warrList = Database.query(warrQuery);
        }
        catch(Exception e){
            system.debug(e.getMessage());
            warrList = new List<Warranty__c>();
        }
        //get all project for this user
        string projFieldList = Utilities.GetAllObjectFields('Project__c');
        queryFormat = 'select {0} from Project__c where Community_User_ID__c = :userId ORDER BY LastModifiedDate DESC Limit 10';
        string projQuery = String.format(queryFormat, new List<string>{projFieldList});
        
        try{
            projList = Database.query(projQuery);
        }
        catch(Exception e){
            projList = new List<Project__c>();
        }
        //get all account for this user
        string acctFieldList = Utilities.GetAllObjectFields('Account');
        queryFormat = 'select {0} from Account where Community_User_ID__c = :userId ORDER BY LastModifiedDate DESC Limit 10';
        string acctQuery = String.format(queryFormat, new List<string>{acctFieldList});
        try{
            acctList = Database.query(acctQuery);
        }
        catch(Exception e){
            acctList = new List<Account>();
        }
        
    }
    
    public void deleteProjects(){
            string index = ApexPages.currentPage().getParameters().get('ProjNum');
            if (projList != null && projList.size() > 0){
                Project__c ProjtoDelete = projList.get(integer.valueof(index));
                delete ProjtoDelete;
                projList.remove(integer.valueof(index));
            }
        }
        
    public void deleteAccount(){
            string index = ApexPages.currentPage().getParameters().get('AccRecordNum');
            if (acctList != null && acctList.size() > 0){
                Account AcctoDelete = acctList.get(integer.valueof(index));
                delete AcctoDelete;
                acctList.remove(integer.valueof(index));
            }
        }
        
    public void deleteWarranties(){
            string index = ApexPages.currentPage().getParameters().get('WarrNum');
            if (warrList != null && warrList.size() > 0){
                Warranty__c WartoDelete = warrList.get(integer.valueof(index));
                delete WartoDelete;
                warrList.remove(integer.valueof(index));
           }
        }
}