global class cloneIncidentAttachments implements Database.Batchable<SObject> {
    
    // To run, use:
    // Database.executeBatch(new cloneIncidentAttachments(Datetime.newInstanceGmt(2016, 2, 5), false, false), 50);
    // modify the max datetime value as necessary
    // true to skip Case records that already have attachments
    // true to rollback each batch, for testing purposes

    private final Datetime gmtCloseDateUpperBound;
    private final Boolean skipCasesWithAttachments;
    private final Boolean isTest;
    
    global cloneIncidentAttachments(Datetime gmtCloseDateUpperBound, Boolean skipCasesWithAttachments, Boolean isTest) {
        this.gmtCloseDateUpperBound = gmtCloseDateUpperBound;
        this.skipCasesWithAttachments = skipCasesWithAttachments;
        this.isTest = isTest;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT Id FROM bmcservicedesk__incident__c WHERE bmcservicedesk__closedatetime__c < :gmtCloseDateUpperBound ORDER BY bmcservicedesk__closedatetime__c DESC');
    }
    
    global void execute(Database.BatchableContext bc, List<SObject> scope) {
        Savepoint sp;
        if (isTest) {
            sp = Database.setSavepoint();
        }
        
        Map<Id, Case> caseMap = new Map<Id, Case>([
            SELECT Id, incident_id__c
            FROM Case
            WHERE incident_id__c IN :new Map<Id, SObject>(scope).keySet()
                AND RecordType.DeveloperName = 'RFMigrated'
        ]);
        
        if (skipCasesWithAttachments) {
            for (Attachment a : [SELECT ParentId FROM Attachment WHERE ParentId IN :caseMap.keySet()]) {
                caseMap.remove(a.ParentId);
            }
        }
        
        Map<Id, Id> incidentIdToCaseId = new Map<Id, Id>();
        for (Case c : caseMap.values()) {
            incidentIdToCaseId.put(c.incident_id__c, c.Id);
        }
        
        for (List<Attachment> attachments : [
            SELECT Name, Body, ContentType, Description, IsPrivate, ParentId, OwnerId,CreatedById, CreatedDate,LastModifiedById, LastModifiedDate
            FROM Attachment
            WHERE ParentId IN :incidentIdToCaseId.keySet()
        ]) {
            List<Attachment> newList = attachments.deepClone(false,true);
            for (Attachment a : newList) {
                a.ParentId = incidentIdToCaseId.get(a.ParentId);
            }
            Database.insert(newList);
        }
        
        if (isTest) {
            Database.rollback(sp);
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        AsyncApexJob a = [SELECT NumberOfErrors, TotalJobItems FROM AsyncApexJob WHERE Id = :bc.getJobId()];

        String recipientEmail = UserInfo.getUserEmail();
    
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] { recipientEmail });
        mail.setSubject('Incident Attachment Migration Complete');
        mail.setPlainTextBody('Batch Process has completed.\nText Context: ' + isTest + '\nProcessed: ' + a.TotalJobItems + '\nFailures: ' + a.NumberOfErrors);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}