@isTest
private class sika_SCCZoneTestResultsCtrlTest {
	private static sika_SCCZoneTestResultsController ctrl;
	private static PageReference pageRef;

	private static Mix__c theMix; 
	private static User SCCZoneUser;


	private static void init(){
		map<String, sObject> objects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
        SCCZoneUser = (User) objects.get('SCCZoneUser');

        System.runAs(SCCZoneUser){
	        theMix = sika_SCCZoneTestFactory.createMix();
	        insert(theMix);
	    }

	}


	@isTest static void testDoSave(){
		Integer testResultCount = [SELECT COUNT() FROM Test_Result__c];
		system.assertEquals(testResultCount, 0);

		init();
		pageRef = new PageReference('/sika_SCCZoneTestNewEditPage');
    	pageRef.getParameters().put('mixID', theMix.Id);
        Test.setCurrentPage(pageRef);

        System.runAs(SCCZoneUser){
        	Test.StartTest();

			ctrl = new sika_SCCZoneTestResultsController();
			PageReference p = ctrl.doSave();

			Test.StopTest();

			list<Test_Result__c> testResults = [SELECT Id FROM Test_Result__c WHERE Test_Result__c.Mix__c = :theMix.Id];
			system.assertEquals(testResults.size(), 1);

			String theLoc = '/apex/sika_scczonetestdetailspage?id=' + testResults[0].Id + '&mixID=' + theMix.Id;
			system.assert(String.valueOf(p).contains(theLoc));
			
		}
	}


	@isTest static void testEditButton(){
		init();
        Test_Result__c testResult = new Test_Result__c();
        testResult.Mix__c = theMix.Id;

        system.runAs(SCCZoneUser){
        	insert(testResult);

        	pageRef = new PageReference('/sika_SCCZoneTestDetailsPage');
        	pageRef.getParameters().put('id', testResult.Id);
	    	pageRef.getParameters().put('mixID', theMix.Id);
	        Test.setCurrentPage(pageRef);

	        ctrl = new sika_SCCZoneTestResultsController();
        	
        	PageReference p = ctrl.editPage();

        	String theLoc = '/apex/sika_scczonetestneweditpage?id=' + testResult.Id + '&mixID=' + theMix.Id;
			system.assert(String.valueOf(p).contains(theLoc));
		
		}
	}


	@isTest static void testDoCancelNew(){
		init();
        pageRef = new PageReference('/sika_SCCZoneTestDetailsPage');
    	pageRef.getParameters().put('mixID', theMix.Id);
        Test.setCurrentPage(pageRef);

        ctrl = new sika_SCCZoneTestResultsController();

        PageReference p = ctrl.doCancel();
        String theLoc = '/sika_SCCZoneMixDetailsPage?mixID=' + theMix.Id;
		system.assert(String.valueOf(p).contains(theLoc));

	}

	@isTest static void testDoCancelExisting(){
		init();
		Test_Result__c testResult = new Test_Result__c();
        testResult.Mix__c = theMix.Id;

        system.runAs(SCCZoneUser){
        	insert(testResult);
        }

        pageRef = new PageReference('/sika_SCCZoneTestDetailsPage');
        pageRef.getParameters().put('id', testResult.Id);
    	pageRef.getParameters().put('mixID', theMix.Id);
        Test.setCurrentPage(pageRef);

        ctrl = new sika_SCCZoneTestResultsController();

        PageReference p = ctrl.doCancel();
        String theLoc = '/apex/sika_scczonetestdetailspage?id=' + testResult.Id + '&mixID=' + theMix.Id;
		system.assert(String.valueOf(p).contains(theLoc));

	}


	@isTest static void testDeleteTest(){
		init();
		Test_Result__c testResult = new Test_Result__c();
        testResult.Mix__c = theMix.Id;

        system.runAs(SCCZoneUser){
        	insert(testResult);
        }

        Integer testResultCount = [SELECT COUNT() FROM Test_Result__c];
		system.assertEquals(testResultCount, 1);

		pageRef = new PageReference('/sika_SCCZoneMixDetailsPage');
        pageRef.getParameters().put('id', testResult.Id);
    	pageRef.getParameters().put('mixID', theMix.Id);
        Test.setCurrentPage(pageRef);

		ctrl = new sika_SCCZoneTestResultsController();
		ctrl.deleteThisTest = testResult.Id;
		
		system.runAs(SCCZoneUser){
			PageReference p = ctrl.deleteTest();

			String theLoc = '/sika_SCCZoneMixDetailsPage?mixID=' + theMix.Id;
			system.assert(String.valueOf(p).contains(theLoc));
		}
		testResultCount = [SELECT COUNT() FROM Test_Result__c];
		system.assertEquals(testResultCount, 0);

	}


	@isTest static void testDeleteThisTest(){
		init();
		Test_Result__c testResult = new Test_Result__c();
        testResult.Mix__c = theMix.Id;

        system.runAs(SCCZoneUser){
        	insert(testResult);
        }

        Integer testResultCount = [SELECT COUNT() FROM Test_Result__c];
		system.assertEquals(testResultCount, 1);

		pageRef = new PageReference('/sika_SCCZoneMixDetailsPage');
        pageRef.getParameters().put('id', testResult.Id);
    	pageRef.getParameters().put('mixID', theMix.Id);
        Test.setCurrentPage(pageRef);

		ctrl = new sika_SCCZoneTestResultsController();

		system.runAs(SCCZoneUser){
			PageReference p = ctrl.deleteThisTest();

			String theLoc = '/sika_SCCZoneMixDetailsPage?mixID=' + theMix.Id;
			system.assert(String.valueOf(p).contains(theLoc));
		}
		testResultCount = [SELECT COUNT() FROM Test_Result__c];
		system.assertEquals(testResultCount, 0);

	}


	@isTest static void returnToMixTest(){
		init();
		pageRef = new PageReference('/sika_SCCZoneTestDetailsPage');
    	pageRef.getParameters().put('mixID', theMix.Id);
        Test.setCurrentPage(pageRef);

		ctrl = new sika_SCCZoneTestResultsController();
		PageReference p = ctrl.returnMix();
		String theLoc = '/sika_SCCZoneMixDetailsPage?mixID=' + theMix.Id;
		system.assert(String.valueOf(p).contains(theLoc));

	}


	@isTest static void testLoadAction(){
		init();

		// test the doAuthCheck portion of loadAction()
        User guestUser = sika_SCCZoneTestFactory.createGuestUser();
        insert(guestUser);
        PageReference p;

        System.runAs(guestUser){
            ctrl = new sika_SCCZoneTestResultsController();
            p = ctrl.loadAction();
            system.assert(String.valueOf(p).contains('/SCCZone/sika_SCCZoneLoginPage'));

        }

        // test the "check for Mix Id" portion of loadAction()
        pageRef = new PageReference('/sika_SCCZoneTestDetailsPage');
        Test.setCurrentPage(pageRef);

        System.runAs(SCCZoneUser){
	        ctrl = new sika_SCCZoneTestResultsController();
	        p = ctrl.loadAction();
	        system.assert(String.valueOf(p).contains('/SCCZone/apex/sika_SCCZoneUserHomePage'));
    	}

    	pageRef = new PageReference('/sika_SCCZoneTestDetailsPage');
    	pageRef.getParameters().put('mixID', theMix.Id);
        Test.setCurrentPage(pageRef);

        System.runAs(SCCZoneUser){
	        ctrl = new sika_SCCZoneTestResultsController();
	        p = ctrl.loadAction();
	        system.assert(p == null);
    	}

    }


    @isTest static void andThePopupTests(){
    	ctrl = new sika_SCCZoneTestResultsController();
    	ctrl.closePopup();
    	system.assertEquals(ctrl.displayPopup, false);
    	ctrl.showPopup();
    	system.assertEquals(ctrl.displayPopup, true);

    }

}