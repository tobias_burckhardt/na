public without sharing class sika_SCCZoneSortListHelper {
	@testVisible private static String sortField {get; set;}
	@testVisible private static String sortType {get; set;}
	private static Integer more;
	private static Integer less;


	public static list<sObject> sortList(list<sObject> theList, String sortBy){

		list<sObject> listSorted = sortList(theList, sortBy, 'ASC');

		return listSorted;
	}

	public static list<sObject> sortList(list<sObject> theList, String sortBy, String sortDir){	
		sortField = sortBy;
		more = sortDir == 'ASC' ? 1 : -1;
		less = sortDir == 'ASC' ? -1 : 1;

		list<sortableObj> listSortable = new list<sortableObj>();
		// get the display type value for the field we want to sort by
		sortType = String.valueOf(theList.getSobjectType().getDescribe().fields.getMap().get(sortBy).getDescribe().getType());

		for(sObject s : theList){
			listSortable.add(new sortableObj(s));
		}
		
		listSortable.sort();

		list<sObject> listSorted = new list<sObject>();
		//listSorted.addAll(listSortable);

		for(sortableObj s : listSortable){
			listSorted.add(s.record);
		}

		return listSorted;
	}


	public class sortableObj implements Comparable {
		private sObject record {get; set;}
	
		sortableObj(sObject theObj){
			record = theObj;
		}

		public Integer compareTo(Object compareTo){
			sortableObj compareObj = (sortableObj) compareTo;

			if(sortType == 'STRING'){

				String thisOne = (String) record.get(sortField);
				String otherOne = (String) compareObj.record.get(sortField);

				// use the default String.compareTo() method if neither value is null
				if(thisOne == null){
					if(otherOne != null){
						return more;
					}
					return 0;
				}
				if(otherOne == null){
					if(thisOne != null){
						return less;
					}
					return 0;
				}
				// ...if sortOrder = 'ASC', more will be set to 1
				return more == 1 ? thisOne.compareTo(otherOne) : thisOne.compareTo(otherOne) * -1;  
			}

			if(sortType == 'PICKLIST'){
				String thisOne = (String) String.valueOf(record.get(sortField));
				String otherOne = (String) String.valueOf(compareObj.record.get(sortField));
				
				if(thisOne == null){
					if(otherOne != null){
						return more;
					}
					return 0;
				}
				if(otherOne == null){
					if(thisOne != null){
						return less;
					}
					return 0;
				}
				return more == 1 ? thisOne.compareTo(otherOne) : thisOne.compareTo(otherOne) * -1;  
			}

			if(sortType == 'INTEGER'){
				Integer thisOne = (Integer) record.get(sortField);
				Integer otherOne = (Integer) compareObj.record.get(sortField);
				
				if(thisOne == null){
					if(otherOne != null){
						return more;
					}
					return 0;
				}
				if(otherOne == null){
					if(thisOne != null){
						return less;
					}
					return 0;
				}

				if(thisOne == otherOne){
					return 0;
				} else if(thisOne > otherOne){
					return more;
				}
				return less;
			}

			// if getType returns the displayType value "Percent" or "Currency" treat the value like a decimal.  (There is no Percent or Currency data type in Apex)
			if(sortType == 'DECIMAL' || sortType == 'PERCENT' || sortType == 'CURRENCY'){
				Decimal thisOne = (Decimal) record.get(sortField);
				Decimal otherOne = (Decimal) compareObj.record.get(sortField);
				
				if(thisOne == null){
					if(otherOne != null){
						return less;
					}
					return 0;
				}
				if(otherOne == null){
					if(thisOne != null){
						return more;
					}
					return 0;
				}

				if(thisOne == otherOne){
					return 0;
				} else if(thisOne > otherOne){
					return more;
				}
				return less;
			}

			if(sortType == 'DOUBLE'){
				Double thisOne = (Double) record.get(sortField);
				Double otherOne = (Double) compareObj.record.get(sortField);
				
				if(thisOne == null){
					if(otherOne != null){
						return less;
					}
					return 0;
				}
				if(otherOne == null){
					if(thisOne != null){
						return more;
					}
					return 0;
				}

				if(thisOne == otherOne){
					return 0;
				} else if(thisOne > otherOne){
					return more;
				}
				return less;
			}

			if(sortType == 'BOOLEAN'){  // true > false
				Boolean thisOne = (Boolean) record.get(sortField);
				Boolean otherOne = (Boolean) compareObj.record.get(sortField);

				if(thisOne == otherOne){
					return 0;
				} else if(thisOne == true &&  otherOne == false){
					return less;
				}
				return less;
			}
/*
			if(sortType == 'DATE'){
				Date thisOne = (Date) record.get(sortField);
				Date otherOne = (Date) compareObj.record.get(sortField);
				
				if(thisOne == null){
					if(otherOne != null){
						return more;
					}
					return 0;
				}
				if(otherOne == null){
					if(thisOne != null){
						return less;
					}
					return 0;
				}

				if(thisOne == otherOne){
					return 0;
				} else if(thisOne > otherOne){
					return more;
				}
				return less;
			}
*/
			if(sortType == 'DATETIME'){
				Datetime thisOne = (Datetime) record.get(sortField);
				Datetime otherOne = (Datetime) compareObj.record.get(sortField);
				
				if(thisOne == null){
					if(otherOne != null){
						return more;
					}
					return 0;
				}
				if(otherOne == null){
					if(thisOne != null){
						return less;
					}
					return 0;
				}

				if(thisOne == otherOne){
					return 0;
				} else if(thisOne > otherOne){
					return more;
				}
				return less;
			}

			return null;
		}

	}

}