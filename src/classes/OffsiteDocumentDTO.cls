public with sharing class OffsiteDocumentDTO {

	public String name {get;set;}
	public String id {get;set;}
	public String type {get;set;}
	public String createdDate {get;set;}
	public Integer size {get;set;}

	public OffsiteDocumentDTO(String name, String id){

		this.name = name;
		this.id = id;
	}
}