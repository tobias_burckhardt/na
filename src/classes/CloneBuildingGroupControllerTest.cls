@isTest
private class CloneBuildingGroupControllerTest {

    @isTest
    public static void testCloneBuildingGroup() {
        
        Decimal productsPerimeter = 1000;
        
        Building_Group__c bg = createBuildingGroupWithProducts(productsPerimeter);
        ApexPages.StandardController sc = new ApexPages.StandardController(bg);        
        CloneBuildingGroupController cgc = new CloneBuildingGroupController(sc);
        
        PageReference page = cgc.cloneBuildingGroup();
		System.assertNotEquals(null, page, ApexPages.getMessages());        
        System.assertEquals('/' + cgc.clonedBg.Id, page.getUrl());
                
        System.assertEquals(bg.Color__c, cgc.clonedBg.Color__c);
        System.assertEquals(bg.Length__c, cgc.clonedBg.Length__c);
        System.assertEquals(bg.on_a_hill__c, cgc.clonedBg.on_a_hill__c);
        
        // get childs for cloned building group        
        List<Building_Group_Product__c> bgps = [select Perimeter__c from Building_Group_Product__c where Building_Group__c = :cgc.clonedBg.Id];
        System.assertEquals(3, bgps.size());
        for (Building_Group_Product__c bgp : bgps) {
        	System.assertEquals(productsPerimeter, bgp.Perimeter__c);    
        }
    }
    
    private static Building_Group__c createBuildingGroupWithProducts(Decimal productsPerimeter) {
        Building_Group__c bg = (Building_Group__c)TestFactory.createSObject(new Building_Group__c(Color__c = 'red', Length__c = 233, on_a_hill__c = true), true);
        
        // child products
        Building_Group_Product__c  bgp = 
            (Building_Group_Product__c )TestFactory.createSObject(new Building_Group_Product__c (Building_Group__c = bg.Id, Perimeter__c = productsPerimeter), true);
        Building_Group_Product__c  bgp2 = 
            (Building_Group_Product__c )TestFactory.createSObject(new Building_Group_Product__c (Building_Group__c = bg.Id, Perimeter__c = productsPerimeter), true);
        Building_Group_Product__c  bgp3 = 
            (Building_Group_Product__c )TestFactory.createSObject(new Building_Group_Product__c (Building_Group__c = bg.Id, Perimeter__c = productsPerimeter), true);        
        
        return bg;
    }
}