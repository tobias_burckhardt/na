@isTest
public class ApplicatorPdfGeneratorControllerTests {

    @isTest
    public static void testController(){
        Account a = new Account(Name='TestAcc'); insert a;
        
        ApexPages.StandardController sc = new ApexPages.standardController(a);
        ApplicatorPdfGeneratorController cont = new ApplicatorPdfGeneratorController(sc);
        
        System.assertEquals(a, cont.acc);
    }
    
    @isTest
    public static void testStringConcat(){
        Account a = new Account(Name='TestAcc'); insert a;
        
        ApexPages.StandardController sc = new ApexPages.standardController(a);
        ApplicatorPdfGeneratorController cont = new ApplicatorPdfGeneratorController(sc);
        
        String s = cont.getAgreements('text1;');
        
        System.assertEquals('text1', s);
        
        s = cont.getAgreements('text1;text2;');
        
        System.assertEquals('text1 and text2', s);
        
        s = cont.getAgreements('text1;text2;text3');
        
        System.assertEquals('text1, text2 and text3', s);
    }
    
    @isTest
    public static void testRegionSelect(){
        Account a = new Account(Name='TestAcc'); insert a;
        
        ApexPages.StandardController sc = new ApexPages.standardController(a);
        ApplicatorPdfGeneratorController cont = new ApplicatorPdfGeneratorController(sc);
        
        Region__c r = cont.selectRegionAddress();
        
        System.assertEquals(new Region__c(), r);
        
        Region__c rNew = new Region__c(Address__c = '12345 TestStreet', City__c='TestCity', State__c='TestState', Zip__c='12345', Sales_Person__c = UserInfo.getUserId());
        insert rNew;
        
        r = cont.selectRegionAddress();
        
        System.assertEquals(rNew.Id, r.Id);
    }
}