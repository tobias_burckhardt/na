public with sharing class CommunityProjectEditController {
        
        public Project__c proj {get; set;}
        public Boolean addWarranty {get; set;}
        public List<Warranty__c> warrantyList {get; set;}
        public Boolean showWarranties {get; set;}
        
        public CommunityProjectEditController(){
            //determine if url parameters were passed in
            Map<string, string> pageParams = ApexPages.currentPage().getParameters();
            
            showWarranties = false;
            //validateForm();
            if(pageParams.size() > 0){
                if(pageParams.get('pid') != null){
                        string pid = pageParams.get('pid');
                        string projectFields = Utilities.GetAllObjectFields('Project__c');
                        string queryFormat = 'select {0} from {1} where {2}';
                        string projQuery = String.format(queryFormat, new List<string>{projectFields, 'Project__c', 'Id = :pid'});
                        proj = Database.query(projQuery);
                        
                        //check to see if any warranties exist
                        string warrantyFields = Utilities.GetAllObjectFields('Warranty__c');
                        string warrQuery = String.format(queryFormat, new List<string>{warrantyFields, 'Warranty__c', 'Project__c = :pid'});
                                        
                                        warrantyList = Database.query(warrQuery);
                                        
                                        if(warrantyList.size() > 0){
                                                showWarranties = true;
                                        }
                }
            }
            
            if(proj == null){
                    RecordType custEntered = [select Id from RecordType where SObjectType = 'Project__c' and DeveloperName = 'Customer_Entered'];
                    proj = new Project__c(RecordTypeId = custEntered.Id);
            }
            
            if(warrantyList == null){
                warrantyList = new List<Warranty__c>();
            }
            
            addWarranty = false;
        }
        
        public PageReference SaveProject(){
            try{
                //ensure project has the community user tied in
                if(proj.Community_User_ID__c == null){
                   proj.Community_User_ID__c = UserInfo.getUserId();
                }                        
                Database.upsertResult res = Database.upsert(proj);
                        
                if(res.IsSuccess()){
                            PageReference pr = null;
                    if(addWarranty){
                        pr = new PageReference('/apex/CommunityWarrantyEdit?pid=' + res.getId());
                                    }
                                        else{
                            pr = new PageReference('/apex/CommunityProjectDetail?pid=' + res.getId());
                                        }
                            pr.setRedirect(true);
                    return pr;
                                }
                else{
                                        system.debug(res);
                            return null;
                    }
                        }
            catch(Exception e){
                        system.debug('exception:' + e.getMessage());
                    return null;
            }
        }
        
        public PageReference SaveAndCreateWarranty(){
                addWarranty = true;
                return SaveProject();
        }
        
        private string validateForm(){
                string errors = '';
                //validate that required fields were entered
                if(proj.Name == null || proj.Name == ''){
                        errors += 'Project Name<br/>';
                }
                if(proj.City__c == null || proj.City__c == ''){
                        errors += 'City<br/>';
                }
                if(proj.ProjectValue__c == null ){
                        errors += 'Project Value<br/>';
                }
                if(proj.BidDate__c == null){
                        errors += 'Bid Date<br/>';
                }
                
                if(errors.length() > 0){
                        return 'Fail|' + errors;
                }
                return 'Success';
        }
        
        //to delete warranties
        public void deleteWarranties(){
            string index = ApexPages.currentPage().getParameters().get('WarrNum');
            Warranty__c WartoDelete = warrantyList.get(integer.valueof(index));
            delete WartoDelete;
           warrantyList.remove(integer.valueof(index));
        }

}