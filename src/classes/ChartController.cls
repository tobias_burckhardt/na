public class ChartController {
 
    public List<ChartData> ClosedIncidentsByStaffThisWeek
    {
        get
        {
            ClosedIncidentsByStaffThisWeek= new List<ChartData>();
            for(AggregateResult res : [Select BMCServiceDesk__FKClosedBy__r.Name ClosedBy, Count(Id) Total
                                       From BMCServiceDesk__Incident__c
                                       Where BMCServiceDesk__state__c = False AND BMCServiceDesk__isServiceRequest__c = False AND BMCServiceDesk__closeDateTime__c = THIS_WEEK
                                       Group By BMCServiceDesk__FKClosedBy__r.Name
                                       Order By Count(Id) ASC, BMCServiceDesk__FKClosedBy__r.Name ASC])
            {
                ClosedIncidentsByStaffThisWeek.add(new ChartData(
                    (String)res.get('ClosedBy'),
                    (Decimal)res.get('Total'))
                );
            }
            return ClosedIncidentsByStaffThisWeek;
        }
        set;
    }

    public List<ChartData> ClosedIncidentsByStaffLast14Days
    {
        get
        {
            date d = system.today().addDays(-14);
            ClosedIncidentsByStaffLast14Days= new List<ChartData>();
            for(AggregateResult res : [Select BMCServiceDesk__FKClosedBy__r.Name ClosedBy, Count(Id) Total
                                       From BMCServiceDesk__Incident__c
                                       Where BMCServiceDesk__state__c = False AND BMCServiceDesk__isServiceRequest__c = False AND BMCServiceDesk__closeDateTime__c >= :d
                                       Group By BMCServiceDesk__FKClosedBy__r.Name
                                       Order By Count(Id) ASC, BMCServiceDesk__FKClosedBy__r.Name ASC])
            {
                ClosedIncidentsByStaffLast14Days.add(new ChartData(
                    (String)res.get('ClosedBy'),
                    (Decimal)res.get('Total'))
                );
            }
            return ClosedIncidentsByStaffLast14Days;
        }
        set;
    }
    
    public List<ChartData> OpenIncidentsByStaffLast7Days
    {
        get
        {
            date d = system.today().addDays(-7);
            OpenIncidentsByStaffLast7Days= new List<ChartData>();
            for(AggregateResult res : [Select BMCServiceDesk__FKClosedBy__r.Name Owner, Count(Id) Total
                                       From BMCServiceDesk__Incident__c
                                       Where BMCServiceDesk__state__c = True AND BMCServiceDesk__isServiceRequest__c = False AND BMCServiceDesk__openDateTime__c >= :d
                                       Group By BMCServiceDesk__FKClosedBy__r.Name
                                       Order By Count(Id) ASC, BMCServiceDesk__FKClosedBy__r.Name ASC])
            {
                OpenIncidentsByStaffLast7Days.add(new ChartData(
                    (String)res.get('Owner'),
                    (Decimal)res.get('Total'))
                );
            }
            return OpenIncidentsByStaffLast7Days;
        }
        set;
    }

    public List<ChartData> OpenIncidentsByStaffLast30Days
    {
        get
        {
            date d = system.today().addDays(-30);
            OpenIncidentsByStaffLast30Days= new List<ChartData>();
            for(AggregateResult res : [Select BMCServiceDesk__FKClosedBy__r.Name Owner, Count(Id) Total
                                       From BMCServiceDesk__Incident__c
                                       Where BMCServiceDesk__state__c = True AND BMCServiceDesk__isServiceRequest__c = False AND BMCServiceDesk__openDateTime__c >= :d
                                       Group By BMCServiceDesk__FKClosedBy__r.Name
                                       Order By Count(Id) ASC, BMCServiceDesk__FKClosedBy__r.Name ASC])
            {
                OpenIncidentsByStaffLast30Days.add(new ChartData(
                    (String)res.get('Owner'),
                    (Decimal)res.get('Total'))
                );
            }
            return OpenIncidentsByStaffLast30Days;
        }
        set;
    }

    public void refreshCharts()
    {
        ClosedIncidentsByStaffThisWeek = null;
        ClosedIncidentsByStaffLast14Days = null;
        OpenIncidentsByStaffLast7Days = null;
        OpenIncidentsByStaffLast30Days = null;
    }
 
    public class ChartData
    { 
        public String name { get; set; } 
        public Decimal data { get; set; } 
 
        public ChartData(String name, Decimal data)
        { 
            this.name = name; 
            this.data = data; 
        } 
    }  
 
}