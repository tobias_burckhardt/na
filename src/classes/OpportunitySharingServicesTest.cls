@IsTest
private class OpportunitySharingServicesTest {

	static final List<String> PROFILE_NAMES = Label.PartnerUserProfileNames.split(',');
	static final String USERNAME1 = 'BW-IBM-01@test.com';
	static final String USERNAME2 = 'BW-IBM-02@test.com';

	@TestSetup
	static void setup() {
		//Account acc1 = (Account) SObjectFactory.create(Account.SObjectType, new Map<SObjectField, Object> {
		//		Account.Name => 'Account1'
		//});
		//Account acc2 = (Account) SObjectFactory.create(Account.SObjectType, new Map<SObjectField, Object> {
		//		Account.Name => 'Account2'
		//});
		//Contact con1 = (Contact) SObjectFactory.create(Contact.SObjectType, new Map<SObjectField, Object> {
		//		Contact.AccountId => acc1.Id
		//});
		//Contact con2 = (Contact) SObjectFactory.create(Contact.SObjectType, new Map<SObjectField, Object> {
		//		Contact.AccountId => acc2.Id
		//});
		//SObjectFactory.create(User.SObjectType, new Map<SObjectField, Object> {
		//		User.Username => USERNAME1, User.ContactId => con1.Id,
		//		User.ProfileId => [SELECT Id FROM Profile WHERE Name = :PROFILE_NAMES[0] LIMIT 1].Id
		//});
		//SObjectFactory.create(User.SObjectType, new Map<SObjectField, Object> {
		//		User.Username => USERNAME2, User.ContactId => con2.Id,
		//		User.ProfileId => [SELECT Id FROM Profile WHERE Name = :PROFILE_NAMES[1] LIMIT 1].Id
		//});
		//Opportunity opp1 = (Opportunity) SObjectFactory.create(Opportunity.SObjectType, new Map<SObjectField, Object> {
		//		Opportunity.Name => 'Opportunity1'
		//});
		//Opportunity opp2 = (Opportunity) SObjectFactory.create(Opportunity.SObjectType, new Map<SObjectField, Object> {
		//		Opportunity.Name => 'Opportunity2'
		//});
		//SObjectFactory.create(Applicator_Quote__c.SObjectType, new Map<SObjectField, Object> {
		//		Applicator_Quote__c.Contact__c => con1.Id, Applicator_Quote__c.Account__c => acc1.Id, Applicator_Quote__c.Opportunity__c => opp1.Id
		//});
		//SObjectFactory.create(Applicator_Quote__c.SObjectType, new Map<SObjectField, Object> {
		//		Applicator_Quote__c.Contact__c => con1.Id, Applicator_Quote__c.Account__c => acc1.Id, Applicator_Quote__c.Opportunity__c => opp2.Id
		//});
		//SObjectFactory.create(Applicator_Quote__c.SObjectType, new Map<SObjectField, Object> {
		//		Applicator_Quote__c.Contact__c => con2.Id, Applicator_Quote__c.Account__c => acc2.Id, Applicator_Quote__c.Opportunity__c => opp1.Id
		//});

		Id profileId = [SELECT Id FROM Profile WHERE Name = :PROFILE_NAMES[0] LIMIT 1].Id;
		List<Account> accountList = new List<Account>();
		Account acc1 = new Account(Name = 'Account1');
		accountList.add(acc1);
		insert accountList;

		List<Contact> contactList = new List<Contact>();
		Contact cont1 = new Contact(LastName = 'lastName', AccountId = acc1.Id);
		contactList.add(cont1);
		insert contactList;

		List<User> userList = new List<User>();
		User usr1 = new User(Username = 'testUser@testuser.sika.com', ContactId = cont1.id, ProfileId = profileId, LastName = 'lastname', Email = 'testuser@testuser.com', Alias='tst', TimeZoneSidKey = 'America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
		userList.add(usr1);
		insert userList;

		List<Opportunity> oppList = new List<Opportunity>();
		Opportunity opp1 = new Opportunity(Name = 'Opportunity', StageName = 'Quote', CloseDate = Date.today()+5);
		Opportunity opp2 = new Opportunity(Name = 'Opportunity1',AccountId = acc1.id, StageName = 'Quote', CloseDate = Date.today()+5);
		oppList.add(opp1);
		oppList.add(opp2);
		insert oppList;

	}

	//@IsTest
	//static void testOppFromNullToAcc() {
	//	Test.startTest();

		//User testUser = [SELECT Id FROM User WHERE Username = :USERNAME1 LIMIT 1];
		//System.runAs(testUser) {
		//    System.assertEquals(2, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to both Applicator Quotes associated to their Account');
		//    System.assertEquals(2, [SELECT COUNT() FROM Opportunity], 'Test User should have visibility to both Opportunities associated to the above Applicator Quotes');
		//}
		//User testUser2 = [SELECT Id FROM User WHERE Username = :USERNAME2 LIMIT 1];
		//System.runAs(testUser2) {
		//    System.assertEquals(1, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to only the Applicator Quote that points to the same Account');
		//    System.assertEquals(1, [SELECT COUNT() FROM Opportunity], 'Test User should have visibility to only the Opportunity that has the applicator quote with the correct Account');
		//}

		//Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'Opportunity1' LIMIT 1];
		//Account acc = [SELECT Id FROM Account WHERE Name = 'Account1' LIMIT 1];
		//opp.AccountId = acc.Id;
		//update opp;
		//OpportunitySharingServices.shareOppsWithPTS(new List<Opportunity>{opp});

		//System.runAs(testUser) {
		//    System.assertEquals(2, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to both Applicator Quotes associated to their Account');
		//    System.assertEquals(2, [SELECT COUNT() FROM Opportunity], 'Test User should have visibility to both Opportunities associated to the above Applicator Quotes');
		//}
		//System.runAs(testUser2) {
		//    System.assertEquals(0, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should no longer have visibility to Applicator Quote with Opp lookup up to another Account');
		//    System.assertEquals(0, [SELECT COUNT() FROM Opportunity], 'Test User should no longer have visibility to Opp looking up to another Account');
		//}

	//	Test.stopTest();
	//}

	/*@IsTest
	static void testOppFromAccToNull() {
		Test.startTest();

		Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'Opportunity1' LIMIT 1];
		Account acc = [SELECT Id FROM Account WHERE Name = 'Account1' LIMIT 1];
		opp.AccountId = acc.Id;
		update opp;
		OpportunitySharingServices.shareOppsWithPTS(new List<Opportunity>{opp});

		User testUser = [SELECT Id FROM User WHERE Username = :USERNAME1 LIMIT 1];
		System.runAs(testUser) {
			System.assertEquals(2, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to both Applicator Quotes associated to their Account');
			System.assertEquals(2, [SELECT COUNT() FROM Opportunity], 'Test User should have visibility to both Opportunities associated to the above Applicator Quotes');
		}
		User testUser2 = [SELECT Id FROM User WHERE Username = :USERNAME2 LIMIT 1];
		System.runAs(testUser2) {
			System.assertEquals(0, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should not have visibility to Applicator Quote with Opp lookup up to another Account');
			System.assertEquals(0, [SELECT COUNT() FROM Opportunity], 'Test User should not have visibility to Opp looking up to another Account');
		}

		opp.AccountId = null;
		update opp;
		OpportunitySharingServices.shareOppsWithPTS(new List<Opportunity>{opp});

		System.runAs(testUser) {
			System.assertEquals(2, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to both Applicator Quotes associated to their Account');
			System.assertEquals(2, [SELECT COUNT() FROM Opportunity], 'Test User should have visibility to both Opportunities associated to the above Applicator Quotes');
		}
		System.runAs(testUser2) {
			System.assertEquals(1, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to only the Applicator Quote that points to the same Account');
			System.assertEquals(1, [SELECT COUNT() FROM Opportunity], 'Test User should have visibility to only the Opportunity that has the applicator quote with the correct Account');
		}

		Test.stopTest();
	}

	@IsTest
	static void testOppFromAccToAcc() {
		Test.startTest();

		Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'Opportunity1' LIMIT 1];
		Account acc1 = [SELECT Id FROM Account WHERE Name = 'Account1' LIMIT 1];
		Account acc2 = [SELECT Id FROM Account WHERE Name = 'Account2' LIMIT 1];
		opp.AccountId = acc1.Id;
		update opp;
		OpportunitySharingServices.shareOppsWithPTS(new List<Opportunity>{opp});

		User testUser1 = [SELECT Id FROM User WHERE Username = :USERNAME1 LIMIT 1];
		System.runAs(testUser1) {
			System.assertEquals(2, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to both Applicator Quotes associated to their Account');
			System.assertEquals(2, [SELECT COUNT() FROM Opportunity], 'Test User should have visibility to both Opportunities associated to the above Applicator Quotes');
		}
		User testUser2 = [SELECT Id FROM User WHERE Username = :USERNAME2 LIMIT 1];
		System.runAs(testUser2) {
			System.assertEquals(0, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should not have visibility to Applicator Quote with Opp lookup up to another Account');
			System.assertEquals(0, [SELECT COUNT() FROM Opportunity], 'Test User should not have visibility to Opp looking up to another Account');
		}

		opp.AccountId = acc2.Id;
		update opp;
		OpportunitySharingServices.shareOppsWithPTS(new List<Opportunity>{opp});

		System.runAs(testUser1) {
			System.assertEquals(1, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should not have visibility to Applicator Quote with Opp lookup up to another Account');
			System.assertEquals(1, [SELECT COUNT() FROM Opportunity], 'Test User should not have visibility to Opp looking up to another Account');
		}
		System.runAs(testUser2) {
			System.assertEquals(1, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should not have visibility to Applicator Quote with Opp lookup up to another Account');
			System.assertEquals(1, [SELECT COUNT() FROM Opportunity], 'Test User should not have visibility to Opp looking up to another Account');
		}

		Test.stopTest();
	}

	@IsTest
	static void testOppWithNoAQsWithAcc() {
		Test.startTest();
		User testUser1 = [SELECT Id FROM User WHERE Username = :USERNAME1 LIMIT 1];
		Account acc1 = [SELECT Id FROM Account WHERE Name = 'Account1' LIMIT 1];
		Opportunity opp3 = (Opportunity)SObjectFactory.create(Opportunity.SObjectType, new Map<SObjectField, Object> {
				Opportunity.Name => 'Opportunity 3', Opportunity.AccountId => acc1.Id
		});
		System.runAs(testUser1) {
			System.assertEquals(2, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to both Applicator Quotes associated to their Account');
			System.assertEquals(2, [SELECT COUNT() FROM Opportunity], 'Test User should have visibility to both Opportunities associated to the above Applicator Quotes');
		}

		OpportunitySharingServices.shareOppsWithPTS(new List<Opportunity>{opp3});

		System.runAs(testUser1) {
			System.assertEquals(2, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to both Applicator Quotes associated to their Account');
			System.assertEquals(3, [SELECT COUNT() FROM Opportunity], 'Test User should have visibility to all 3 Opportunities, even though one opp isn\'t associated with an Applicator Quote');
		}
		Test.stopTest();
	}*/

	@isTest
	static void addOpportunitySharesToCommunityUsers() {
		List<User> userList = [SELECT Id FROM User WHERE Username = 'testUser@testuser.sika.com'];
		List<Opportunity> oppList = [SELECT Id FROM Opportunity];
		List<Id> oppIds = new List<Id>();
		for(Opportunity opp : oppList){
			oppIds.add(opp.id);
		}

		Map<Id,List<Id>> userToOpportunityMap = new Map<Id,List<Id>>();
		userToOpportunityMap.put(userList[0].Id,oppIds);

		Test.startTest();
			OpportunitySharingServices.addOpportunitySharesToCommunityUsers(userToOpportunityMap);
		Test.stopTest();

		List<OpportunityShare> oppShares = [SELECT id FROM OpportunityShare];

		System.assertNotEquals(0,oppShares.size(),'There should be one opportunity share created');
	}

	@isTest
	static void shareOppsWithPTS_OppHasNoAccount() {
		List<User> userList = [SELECT Id FROM User WHERE Username = 'testUser@testuser.sika.com'];
		List<Opportunity> oppList = [SELECT Id FROM Opportunity WHERE Name = 'Opportunity'];

		Test.startTest();
			OpportunitySharingServices.shareOppsWithPTS(oppList);
		Test.stopTest();

		List<OpportunityShare> oppShares = [SELECT id FROM OpportunityShare];

		System.assertNotEquals(0,oppShares.size(),'There should be one opportunity share created');
	}

	@isTest
	static void shareOppsWithPTS_OppHasSameAccount() {
		List<User> userList = [SELECT Id FROM User WHERE Username = 'testUser@testuser.sika.com'];
		List<Opportunity> oppList = [SELECT Id FROM Opportunity WHERE Name = 'Opportunity1'];

		Test.startTest();
			OpportunitySharingServices.shareOppsWithPTS(oppList);
		Test.stopTest();

		List<OpportunityShare> oppShares = [SELECT id FROM OpportunityShare];

		System.assertNotEquals(0,oppShares.size(),'There should be one opportunity share created');
	}
}