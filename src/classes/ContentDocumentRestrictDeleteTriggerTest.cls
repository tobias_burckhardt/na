@isTest
public class ContentDocumentRestrictDeleteTriggerTest {

    @testSetup
    static void setUpTests() {
        RestrictedCaseTestHelper.createCustomSettings();
        AWS_Settings__c awsSetting = new AWS_Settings__c(AWS_Key__c = 'Test', S3_Bucket_Name__c = 'Test', SF_Library_Name__c = 'Test');
        insert awsSetting;
    }

    @isTest
    static void testDeleteUnrestrictedOnCase() {
        //Trying to delete a ContentDocument attached to a case with an unrestricted record type should succeed
        Case testCase = RestrictedCaseTestHelper.createTestCase(false);
        List<FeedItem> testFI = CaseEmailChatterFileTestHelper.createTestEmailAttachment(testCase.Id, 1);
        List<ContentVersion> cvs = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :testFI[0].RelatedRecordId];
        
        List<ContentDocument> cds = [SELECT Id FROM ContentDocument WHERE Id = :cvs[0].ContentDocumentId];
        
        Test.startTest();

        Database.DeleteResult deleteResult = Database.delete(cds[0], false);
        System.assert(deleteResult.isSuccess());
        
        Test.stopTest();
    }
    
    @isTest
    static void testDeleteRestrictedOnCase() {
        //Trying to delete a ContentDocument attached to a case with an restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
		List<FeedItem> testFI = CaseEmailChatterFileTestHelper.createTestEmailAttachment(testCase.Id, 1);
        List<ContentVersion> cvs = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :testFI[0].RelatedRecordId];
        
        List<ContentDocument> cds = [SELECT Id FROM ContentDocument WHERE Id = :cvs[0].ContentDocumentId];
        
        Test.startTest();

        Database.DeleteResult deleteResult = Database.delete(cds[0], false);
        System.assert(!deleteResult.isSuccess());
        
        Test.stopTest();
    }
}