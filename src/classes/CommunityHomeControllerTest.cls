@isTest 
public class CommunityHomeControllerTest{
    static testMethod void TestCommunityHomeController() {
    
        Account a1 = new Account();
        a1.name='test';
        a1.Community_User_ID__c=userinfo.getuserid();
        insert a1;
           
        Project__c p1 = new Project__c();
        p1.name='test pro';
        p1.Community_User_ID__c=userinfo.getuserid();
        insert p1;
        
        Warranty__c w1 = new Warranty__c();
        //w1.name='test warr';
        w1.Project__c = p1.id;
        w1.Community_User_ID__c=userinfo.getuserid();
        insert w1;
        
        PageReference pageRef = Page.CommunityHome; //replace with your VF page name 
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('ProjNum','0');
       CommunityHomeController ch=new CommunityHomeController();
        
        ch.forwardToCustomAuthPage();
        ch.deleteProjects();
        test.StartTest();
        
        apexpages.currentpage().getparameters().put('AccRecordNum' , '0');
        ch.deleteAccount();
        apexpages.currentpage().getparameters().put('WarrNum' , '0');
        ch.deleteWarranties();
        
        test.stopTest();
        
    }
}