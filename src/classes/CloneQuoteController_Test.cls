@isTest
public class CloneQuoteController_Test {
    static testMethod void createquote() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('SoldTo_Account').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByDeveloperName().get('SPA_Pricing').getRecordTypeId();
        Id quaRecordTypeId = Schema.SObjectType.quote.getRecordTypeInfosByDeveloperName().get('SPA_Pricing').getRecordTypeId();
  		List<Quote> lstQuote = new List<Quote>();

        Account objAccount = new Account(Name = 'Test Acc1', BillingCity = 'Test City', BillingState = 'Test State', 
                                                                    BillingStreet = 'Test Street', BillingPostalCode = '12345', 
                                                                    BillingCountry = 'Test Country', Phone = '123456');
        objAccount.RecordTypeId = accRecordTypeId;
        insert objAccount;
        
        contact con = new contact();
        con.FirstName = 'test';
        con.LastName = 'sample';
        con.AccountId = objAccount.Id;
        insert con;
        
        Product2 objProduct = new Product2(Name = 'Test product1', family = 'Cafe',Standard_Cost1__c=1);                                           
        insert objProduct;
        
        Id pricebookId = Test.getStandardPricebookId();
		PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = objProduct.Id,UnitPrice = 10000, IsActive = true);
		insert standardPrice;
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
		insert customPB;
        PriceBookEntry objPBE = new PriceBookEntry(UnitPrice = 300,Pricebook2Id = customPB.Id,
                                                        Product2Id = objProduct.Id, IsActive = true);
        insert objPBE;
       
        Opportunity objOpp = new Opportunity(Name = 'Test Opp', AccountId = objAccount.Id, StageName = 'Verbal Confirmation', CloseDate = Date.today()+1);
        objOpp.recordtypeid = oppRecordTypeId;
        insert objOpp;
        for(Integer i=0; i< 10 ; i++){
			lstQuote.add(new Quote(OpportunityId = objOpp.Id, Name = 'Test Quote' + i, Status = 'Review Pending',contactid = con.Id,recordtypeid = quaRecordTypeId,Pricebook2Id =customPB.id));
		}
        insert lstQuote;
        //QuoteLineItem newqli = new QuoteLineItem(Product2Id =objProduct.id,QuoteId=lstQuote[0].id,Quantity=1,UnitPrice=2,PricebookEntryId=objPBE.id);
        //insert newqli;
        CloneQuoteController.cloneParent(lstQuote[0].id);
    }
}