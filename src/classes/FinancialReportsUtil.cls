/**
 * Shared code between Financial Reports and Report Cockpit
 *
 * @author mto
 * @copyright PARX
 */
public with sharing class FinancialReportsUtil {

    /**
     * Converts a comma separated string to a list, sanitize the string
     * It assumes the string parameter is non null and non empty
     */
    public static List<String> stringToList(String input)
    {
        // if input string is not comma separated return array with only that element
        if(!input.contains(',')) {
            return new List<String> { String.escapeSingleQuotes(input).trim() };
        }

        final List<String> idList = input.split(',');
        final List<String> resultList = new List<String>();

        for(String id: idList)
        {
            resultList.add(String.escapeSingleQuotes(id).trim());
        }

        return resultList;
    }

    /**
     * @description getUserRoleId()
     */
    public static List<Id> getSalesRepIdsOfDirectReportsByRoleHierarchy(String parentRoleId)
    {
        List<Id> salesRepIds = new List<Id>();

        if(parentRoleId != null && parentRoleId != '') {
            // Query the direct child roles
            Map<Id, UserRole> childRoles = new Map<Id, UserRole>([select id, name from UserRole where ParentRoleId = :parentRoleId]);

            System.debug('===== childRoles: ' + childRoles.keySet());

            if(childRoles.keySet().size() > 0) {
                // Query all the users which are in those child roles
                List<User> directReports = [select id, ERPId__c, name from User where UserRoleId in :childRoles.keySet()];

                System.debug('===== directReports: ' + directReports);

                for (User directReport : directReports) {
                    if (directReport.ERPId__c != null) {
                        salesRepIds.add(directReport.Id);
                    }
                }

                System.debug('===== directReports with ERP id: ' + salesRepIds);
            }
        }

        return salesRepIds;
    }

    /**
     * @description Returns a List of Sales Rep Erp Ids of the direct rerports for a given manager Erp id
     */
    public static List<String> getSalesRepIdsOfDirectReports(String managerUserId)
    {
        System.debug('===== getSalesRepIdsOfDirectReports(' + managerUserId + ')');
        List<User> directReports = [select id , ERPId__c, name from User where  ManagerId =:managerUserId];

        List<String> salesRepIds = new List<String>();
        for (User directReport : directReports)
        {
            if(directReport.ERPId__c != null)
            {
                salesRepIds.add(directReport.Id);
            }
        }

        return salesRepIds;
    }
}