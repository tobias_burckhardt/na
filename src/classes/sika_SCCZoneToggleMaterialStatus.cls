global class sika_SCCZoneToggleMaterialStatus {
	
	// provides a way for internal Sika users to activate/deactivate material records that are used in locked mixes.
	// this method is called from a pair of custom buttons that may be used in an admin-specific layout, or may be made available to other user types, as business needs dictate.
	Webservice static void activateDeactivateMaterial(String materialID, String action){
		Material__c theMaterial = [SELECT Id, isActive__c FROM Material__c WHERE Id = :materialID Limit 1];

		if(action == 'activate'){
			theMaterial.isActive__c = true;
		} else if(action == 'deactivate') {
			theMaterial.isActive__c = false;
		} else {
			system.debug('******** action string not recognized. Value must be \'activate\' or \'deactivate\'. Actual value: \'' + action + '\'');
		}

		sika_SCCZoneMaterialTriggerHelper.bypassPreventDelete = true;
		sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = true;
		sika_SCCZoneMaterialTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = true;

		update(theMaterial);

		// no need to reset bypasses, since the execution context will end here.
	}
}