@isTest
public class SendEmailWithSFAttachmentsTest
{
   
     public static Applicator_Quote__c appQuote;
     public static Opportunity opt;
     public static List<Account> act;
     public static List<Contact> con;
     public static Attachment attach;
    public static void testData(){
         act = TestingUtils.createAccounts( 'testAct', 1, true );
        con = TestingUtils.createContacts(act[0].id, 1, true);
        opt = TestingUtils.createOpportunities( 1, act[0].Id, false )[0];
        Opportunity negativeOpt = TestingUtils.createOpportunities( 1, null, false )[0];
        Apttus_Config2__PriceList__c  pl = new Apttus_Config2__PriceList__c();
        pl.name ='testpl';
        INSERT pl;
        Apttus_Proposal__Proposal__c tProposal= new Apttus_Proposal__Proposal__c();
        tProposal.Apttus_Proposal__Proposal_Name__c= 'testProposal';
        tProposal.Apttus_Proposal__Opportunity__c = opt.id;
        tProposal.Apttus_QPConfig__PriceListId__c = pl.id;
        INSERT tProposal;
        appQuote = new Applicator_Quote__c();
        appQuote.Contact__c = con[0].id;
        appQuote.Account__c =act[0].id;
        appQuote.Opportunity__c = opt.id;
        appQuote.Quote_Apttus__c= tProposal.id;
        INSERT appQuote;
         attach=new Attachment();     
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=appQuote.id;
        insert attach;
    }
    static testMethod void test_query()
    {
    /*    List<Account> act = TestingUtils.createAccounts( 'testAct', 1, true );
        List<Contact> con = TestingUtils.createContacts(act[0].id, 1, true);
        Opportunity opt = TestingUtils.createOpportunities( 1, act[0].Id, false )[0];
        Opportunity negativeOpt = TestingUtils.createOpportunities( 1, null, false )[0];
        Apttus_Config2__PriceList__c  pl = new Apttus_Config2__PriceList__c();
        pl.name ='testpl';
        INSERT pl;
        Apttus_Proposal__Proposal__c tProposal= new Apttus_Proposal__Proposal__c();
        tProposal.Apttus_Proposal__Proposal_Name__c= 'testProposal';
        tProposal.Apttus_Proposal__Opportunity__c = opt.id;
        tProposal.Apttus_QPConfig__PriceListId__c = pl.id;
        INSERT tProposal;
        Applicator_Quote__c appQuote = new Applicator_Quote__c();
        appQuote.Contact__c = con[0].id;
        appQuote.Account__c =act[0].id;
        appQuote.Opportunity__c = opt.id;
        appQuote.Quote_Apttus__c= tProposal.id;
        INSERT appQuote;
        Attachment attach=new Attachment();     
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=appQuote.id;
        insert attach;  */
        
        /* Folder[] folders  =[select id from Folder where name ='PTS Applicator Email Templates'];
        
        EmailTemplate e = new EmailTemplate (developerName = 'test', FolderID = folders[0].id, TemplateType= 'Text', Name = 'test'); // plus any other fields that you want to set
        insert e; */
         testData();
        SendEmailWithSFAttachments objSendEmailWithSFAttachments = new SendEmailWithSFAttachments();
       // objSendEmailWithSFAttachments.selectedTemplateId =  e.id;
        EmailTemplate eTemplate = objSendEmailWithSFAttachments.theTemplate;
        objSendEmailWithSFAttachments.ApplicatorQuoteId = appQuote.id;
        String srh = objSendEmailWithSFAttachments.getsearchCategory() ;
        objSendEmailWithSFAttachments.setsearchCategory('Template');
        Applicator_Quote__c obj = objSendEmailWithSFAttachments.parentQuoteProposalId;
        Id iDvalue = objSendEmailWithSFAttachments.accountSelectedId;
        String conSelected = objSendEmailWithSFAttachments.contactSelected;
        Id iDConvalue = objSendEmailWithSFAttachments.contactSelectedId;
        List<Attachment> lstAttachment = objSendEmailWithSFAttachments.attachments;
        List<Contact> bccContacts = objSendEmailWithSFAttachments.bccContacts;
        List<Contact> contactList = objSendEmailWithSFAttachments.contacts;
        //List<User> userList = objSendEmailWithSFAttachments.users;
        objSendEmailWithSFAttachments.cancel();
        
        List<SendEmailWithSFAttachments.WhereClause> clauses = new List<SendEmailWithSFAttachments.WhereClause>();
        SendEmailWithSFAttachments.WhereClause stageNameClause = new SendEmailWithSFAttachments.WhereClause();
        stageNameClause.whereColumn = 'AccountId';
        stageNameClause.whereValue = opt.AccountId;
        stageNameClause.eval = 'EQUALS';
        clauses.add( stageNameClause );

        SendEmailWithSFAttachments.WhereClause nameLikeClause = new SendEmailWithSFAttachments.WhereClause();
        nameLikeClause.whereColumn = 'Name';
        String likeName = 'Test';
        nameLikeClause.whereValue = likeName;
        nameLikeClause.eval = 'LIKE';
        clauses.add( nameLikeClause );

        Test.startTest();
            List<sObject> objs = SendEmailWithSFAttachments.query( new List<String>{ 'AccountId' }, 'Opportunity', clauses );
        Test.stopTest();

        System.assertEquals( 0, objs.size(), 'We expect only the Optys existing for this account to be returned' );
     //   System.assert( objs[0] instanceOf Opportunity, 'Because the tableName is "Opportunity" we expect those types to be returned');
        System.assertEquals( act[0].Id, opt.AccountId, 'We expect only the Optys existing for this account to be returned' );
    }

    static testMethod void testSendEmail()
    {
      //  ApexPages.StandardController std = new ApexPages.StandardController( new Certification__c() );
        SendEmailWithSFAttachments cont = new SendEmailWithSFAttachments(  );
        SendEmailWithSFAttachments.ApexSingleEmailMessage message = setup();

        Test.startTest();
            
            Messaging.SingleEmailMessage sentMsg = SendEmailWithSFAttachments.sendEmail( message );
        Test.stopTest();

        System.assertEquals( message.targetObjectId, sentMsg.getTargetObjectId(), 'the relevant fields should be passed over' );
    } 
 
    static SendEmailWithSFAttachments.ApexSingleEmailMessage setup()
    {     testData();
        final String EMAIL = 'test@test.com';
        final String TO_ID = [Select Id, Email from User where Email != null limit 1].Id;// Bluewolf Beyond
        final String TARGET_OBJECT = '';

        SendEmailWithSFAttachments.ApexSingleEmailMessage message = new SendEmailWithSFAttachments.ApexSingleEmailMessage( new SendEmailWithSFAttachments());
        List<id> idAttachments = new List<id>();
        idAttachments.add(attach.id);
        message.attachmentIds = idAttachments; 
        message.bcc = new List<String>{EMAIL};
        message.cc = new List<String>{EMAIL};
        message.documentIds = new List<String>();
        message.targetObjectId = TO_ID;
        message.Subject = 'The Subject';
        message.Body = 'hello';
        message.freeText = 'Test';
        message.freeTextSubject = 'Test Subject';
        
        return message;
    } 
}