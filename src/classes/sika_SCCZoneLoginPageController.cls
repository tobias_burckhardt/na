global with sharing class sika_SCCZoneLoginPageController {
	
	global sika_SCCZoneLoginPageController() { }

	global String username {get; set;}
    global String password {get; set;}

    public Boolean alertValue {get; set;}
    public String errorMsg {get; set;}
    
    // if the login page is accessed by an already-authenticated user, redirect the user to the userHomePage.
    public Pagereference loadAction(){
    	sika_SCCZoneCheckAuth.setAuthStatus(UserInfo.getProfileId());
		PageReference isAuthenticated = sika_SCCZoneCheckAuth.doAuthCheck();
		PageReference pageRef;
		
		if(isAuthenticated == null){
			pageRef = new PageReference('/SCCZone/sika_SCCZoneUserHomePage');
	        pageRef.setRedirect(true);
	        return pageRef; 
		}
		return null;
	}

	// if the user comes right to /SCCZone  or to /SCCZone/sika_SCCZoneLoginPage, upon authentication, go to the Disclaimer page;
	// otherwise, go to the page referenced in the URL.  (e.g., .../SCCZone/sika_SCCZoneMixDetailsPage)
    global PageReference login() {

        String startUrl = System.currentPageReference().getParameters().get('startURL');
        if(startUrl == null){
        	startUrl = '/sika_SCCZoneDisclaimer';
        }
        return Site.login(username, password, startUrl);

    }  
}