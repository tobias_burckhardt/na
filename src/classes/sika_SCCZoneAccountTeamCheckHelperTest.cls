@isTest
private class sika_SCCZoneAccountTeamCheckHelperTest {
	private static Account acct;
	private static User salesUser;
	private static User SCCZoneUser;
	private static User adminUser;

	private static void init(){
		sika_SCCZoneMixTriggerHelper.bypassUpdateSCMQuantity = true;
		sika_SCCZoneMixTriggerHelper.bypassMixOwnerTransferCheck = true;
		sika_SCCZoneMixTriggerHelper.bypassMixChatterAlert = true;
		
		sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = true;
		sika_SCCZoneMaterialTriggerHelper.bypassPreventDelete = true;
		sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = true;
		
		sika_SCCZoneMixMaterialTriggerHelper.bypassMixOptimizationCheck = true;
		sika_SCCZoneMixMaterialTriggerHelper.bypassSetSCMWeightAndUnit = true;
		sika_SCCZoneMixMaterialTriggerHelper.bypassConcreteCurveCalc = true;
		
		sika_SCCZoneTestResultTriggerHelper.bypassLockOrUnlockMixAndMaterials = true;
		
		// we need the AccountTeamCheck trigger to run with every DML operation in order for this test to run correctly as written.
		sika_SCCZoneAccountTeamCheckHelper.doNotLimit = true;
		
		acct = null;
		salesUser = null;
		SCCZoneUser = null;
		adminUser = null;
	
		map<String, sObject> objects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
		acct = (Account) objects.get('theAccount');
		salesUser = (User) objects.get('salesUser');
		SCCZoneUser = (User) objects.get('SCCZoneUser');
		adminUser = (User) objects.get('adminUser');
		
	}
	
	private static Id createAccountTeamMember(Id user, Id acctId, String role){
		AccountTeamMember atm = new AccountTeamMember();
		atm.userId = user;
		atm.accountId = acctId;
		atm.teamMemberRole = role;
		system.runAs(adminUser){
			insert(atm);
		}
		return atm.Id;
	}

	private static map<String, sObject> createAllObjects(Id ownerId, Boolean setAccountId){
		Id acctId = setAccountId == true ? acct.Id : null;

		Mix__c mix = sika_SCCZoneTestFactory.createMix(new map<String, String>{'accountId' => acctId});
		mix.OwnerId = ownerId;
		Database.saveResult mixSR = Database.insert(mix);
		system.assertEquals(mixSR.isSuccess(), true);
		
		Material__c material = sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'accountId' => acctId, 'type' => 'SCM'});
		Database.saveResult materialSR = Database.insert(material);
		material.OwnerId = ownerId;
		system.assertEquals(materialSR.isSuccess(), true);
		
		Mix_Material__c mm = sika_SCCZoneTestFactory.createMixMaterial(mix, material);
		// mix_materials are owned by their related mix's owner
		Database.saveResult mmSR = Database.insert(mm);
		system.assertEquals(mmSR.isSuccess(), true);
		
		Test_Result__c testResult = new Test_Result__c();
		// test results are owned by their related mix's owner
		testResult.Mix__c = mix.Id;
		Database.saveResult testResultSR = Database.insert(testResult);
		system.assertEquals(testResultSR.isSuccess(), true);
		
		map<String, sObject> theObjects = new map<String, sObject>();
		theObjects.put('mix', mix);
		theObjects.put('material', material);
		theObjects.put('mm', mm);
		theObjects.put('testResult', testResult);
		return theObjects;
	}


	// confirm SCC Zone users can create, edit, and delete mixes, materials, mix_materials, and test_results (a.k.a., 'SCC Objects') 
	@isTest static void testCreateAllObjectsSCCUser(){
		init();
		Test.startTest();
		System.runAs(SCCZoneUser){
			
			map<String, sObject> objectMap = createAllObjects(SCCZoneUser.Id, true);
			
			Database.saveResult editMix = Database.update(objectMap.get('mix'));
			Database.saveResult editMaterial = Database.update(objectMap.get('material'));
			Database.saveResult editMM = Database.update(objectMap.get('mm'));
			Database.saveResult editTR = Database.update(objectMap.get('testResult'));
			
			system.assertEquals(editMix.isSuccess(), true);
			system.assertEquals(editMaterial.isSuccess(), true);
			system.assertEquals(editMM.isSuccess(), true);
			system.assertEquals(editTR.isSuccess(), true);
			
			Database.deleteResult deleteMix = Database.delete(objectMap.get('mix'));
			Database.deleteResult deleteMaterial = Database.delete(objectMap.get('material'));
			
			system.assertEquals(deleteMix.isSuccess(), true);
			system.assertEquals(deleteMaterial.isSuccess(), true);
		
		}
		Test.stopTest();
		
	}



	// confirm salespeople with the SCC Zone Object Access permissionSet can create, edit, and delete SCC Objects (that they own)
	@isTest static void testCreateAllObjectsSalesUser(){
		init();
		
		Test.startTest();
		System.runAs(salesUser){
			map<String, sObject> objectMap = createAllObjects(salesUser.Id, false);

			Database.saveResult editMix = Database.update(objectMap.get('mix'));
			Database.saveResult editMaterial = Database.update(objectMap.get('material'));
			Database.saveResult editMM = Database.update(objectMap.get('mm'));
			Database.saveResult editTR = Database.update(objectMap.get('testResult'));
			
			system.assertEquals(editMix.isSuccess(), true);
			system.assertEquals(editMaterial.isSuccess(), true);
			system.assertEquals(editMM.isSuccess(), true);
			system.assertEquals(editTR.isSuccess(), true);
			
			Database.deleteResult deleteMix = Database.delete(objectMap.get('mix'));
			Database.deleteResult deleteMaterial = Database.delete(objectMap.get('material'));
			// (mix_materials are deleted when their master (mix) record is deleted)
			// (test_results are deleted when their master (mix record is deleted)
			
			system.assertEquals(deleteMix.isSuccess(), true);
			system.assertEquals(deleteMaterial.isSuccess(), true);
			
		}
		Test.stopTest();
		
	}


	// confirm that SCC salespeople can edit and delete SCC Objects associated with accounts where they are designated as 'SCC Zone Sales' teamMembers. (TeamMember.Role = the value set in Sika_SCCZone_Common.SCCZoneSalesRepATMRole)
	@isTest static void testAccountTeamMemberEditSCCAccountObjects(){
		init();
		map<String, sObject> theObjects;
		
		System.runAs(SCCZoneUser){
			theObjects = createAllObjects(SCCZoneUser.Id, true);
		}
		
		// assign the salesUser to the SCC Zone user's account team, with an accountTeamMember role that designates the user is an SCC Zone Sales Rep
		createAccountTeamMember(salesUser.Id, acct.Id, Sika_SCCZone_Common.SCCZoneSalesRepATMRole);
		
		Mix__c testMix = (Mix__c) theObjects.get('mix');
		Material__c testMaterial = (Material__c) theObjects.get('material');
		Mix_Material__c testMM = (Mix_Material__c) theObjects.get('mm');
		Test_Result__c testTR = (Test_Result__c) theObjects.get('testResult');
		
		Test.startTest();
		
		System.runAs(salesUser){
			
			// update the objects
			Database.saveResult mixSR = Database.update(testMix);
			Database.saveResult materialSR = Database.update(testMaterial);
			Database.saveResult mmSR = Database.update(testMM);
			Database.saveResult trSr = Database.update(testTR);
			
			// confirm success
			system.assertEquals(mixSR.isSuccess(), true);
			system.assertEquals(materialSR.isSuccess(), true);
			system.assertEquals(mmSR.isSuccess(), true);
			system.assertEquals(trSR.isSuccess(), true);
			
			// delete the objects
			Database.deleteResult mixDR = Database.delete(testMix);
			Database.deleteResult materialDR = Database.delete(testMaterial);
			
			// confirm success
			system.assertEquals(mixDR.isSuccess(), true);
			system.assertEquals(materialDR.isSuccess(), true);
			
		}
		Test.stopTest();
		
	}
	
	
	// confirm that accountTeamMembers that aren't designated as 'SCC Zone Sales' team members on an account can't edit or delete SCC objects associated with the account
	@isTest static void testNonAccountTeamMemberEditSCCAccountObjects(){
		init();
		map<String, sObject> theObjects;
		
		System.runAs(SCCZoneUser){
			theObjects = createAllObjects(SCCZoneUser.Id, true);
		}
		
		// assign the salesUser to the SCC Zone user's account team, with a role that precludes the user from accessing that account's SCC objects. (The user has the SCC Zone Object Access permissionSet, but is not an SCC team member on THIS account.)
		createAccountTeamMember(salesUser.Id, acct.Id, 'Not-an-SCC-type-team-member');
		
		Mix__c testMix = (Mix__c) theObjects.get('mix');
		Material__c testMaterial = (Material__c) theObjects.get('material');
		Mix_Material__c testMM = (Mix_Material__c) theObjects.get('mm');
		Test_Result__c testTR = (Test_Result__c) theObjects.get('testResult');
		
		Test.startTest();
		
		System.runAs(salesUser){
			Boolean mixException = false;
			Boolean materialException = false;
			Boolean mmException = false;
			Boolean testResultException = false;
			
			// try updating...
			try {
				update(testMix);
			} catch (Exception e) {
				mixException = true;
				
				// confirm that the expected exception was thrown
				system.assertEquals(e.getDMLType(0), StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION);
			}
			// confirm that an exception was thrown
			system.assertEquals(mixException, true);
			
			try {
				update(testMaterial);
			} catch (Exception e) {
				materialException = true;
				
				system.assertEquals(e.getDMLType(0), StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION);
			}
			system.assertEquals(materialException, true);
			
			try {
				update(testMM);
			} catch (Exception e) {
				mmException = true;
				
				system.assertEquals(e.getDMLType(0), StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION);
			}
			system.assertEquals(mmException, true);
			
			try {
				update(testTR);
			} catch (Exception e) {
				testResultException = true;
				
				system.assertEquals(e.getDMLType(0), StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION);
			}
			system.assertEquals(testResultException, true);
			
			// try deleting...
			try {
				delete(testMix);
			} catch (Exception e) {
				mixException = true;
				
				// confirm that the expected exception was thrown
				system.assertEquals(e.getDMLType(0), StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION);
			}
			// confirm that an exception was thrown
			system.assertEquals(mixException, true);
			
			try {
				delete(testMaterial);
			} catch (Exception e) {
				materialException = true;
				
				system.assertEquals(e.getDMLType(0), StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION);
			}
			system.assertEquals(materialException, true);
			
		}
		Test.stopTest();
		
	}
	
}