/**********************************************************************
 * Author: Sohini
 * Vendor Name : CTS Created Date: Jun 2017
 * Modified Date: Jul 2017
 * Purpose: Test class to handle the Account fields query
 =======================================================================
*/
@isTest
public class SIKA_QueryAccountFieldsTest {
    public static testMethod void testGetAccountFields() {
        Account a = new Account(Name='aa');
        insert a;
        
        test.startTest();
        Account acc = SIKA_QueryAccountFields.getAccountFields(a.Id);
        System.assertEquals(a.name,acc.name);
        test.stopTest();
    }    
}