public with sharing class SendOpportunityToSAP {
    public static final String EMPTY_PROJECT_ACCOUNT_ERROR =
        'Please populate Project_AccountId__c before integrating with SAP.';
    public static final String EMPTY_SHIP_TO_ERROR = 'Please populate ShipTo_AccountId__c before integrating with SAP.';
    public static final String ERROR_TEMPLATE = 'There was an error ({0}), please contact your administrator.';
    public static final String CUSTOM_SETTING_ERROR =
        'Please make sure the custom setting Sarnafil Services Inc Account Id is correctly ' +
        'configured.';

    private Opportunity oppty;

    public SendOpportunityToSAP(ApexPages.StandardController stdController) {
        oppty = [
            SELECT SAP_Name_40__c, Opportunity_Number__c, SAP_Name_20__c, Street_Address__c, Zip__c, SSI__c,
                   City__c, Country__c, State__c, Transportation_Zone__c, Language__c, SAP_Industry_Key__c, SAP_Industry_Code__c,
                   Sales_Organization__c, Distribution__c, Division__c, Shipping_Condition__c, Warehouse__c, Incoterm1__c,
                   Incoterm2__c, Tax_Country_MX__c, Tax_Category_MX__c, Tax_Classification_MX__c, Tax_Country_US__c,
                   Tax_Category_US__c, Tax_Classification_US__c, Project_AccountId__c, ShipTo_AccountId__c,
                   Customer_Group__c, Account.SAP_Account_Code__c, SAP_Order_Reason__c, SAP_Account_Assign__c, SAP_Vertical_Market_Code__c,
                   SAP_Primary_System_Code__c, SAP_Primary_System_Area__c, SAP_Primary_System_UOM__c
            FROM Opportunity
            WHERE Id = :stdController.getId()
        ];
    }

    public PageReference createProjectInSAP() {
        PageReference ref;

        try {
            oppty.Project_AccountId__c = SAPIntegrationService.createProjectInSAP(oppty);
            update oppty;
            ref = redirectToOpportunity();
        } catch(DmlException e) {
            addErrorMessage(e);
        } catch(CalloutException e) {
            addErrorMessage(e);
        } catch(SAPIntegrationService.SAPIntegrationServiceException e) {
            addErrorMessage(e);
        }

        return ref;
    }

    public PageReference createShipToInSAP() {
        PageReference ref;

        if (oppty.Project_AccountId__c != null) {
            try {
                oppty.ShipTo_AccountId__c = SAPIntegrationService.createShipToInSAP(oppty);
                update oppty;
                ref = redirectToOpportunity();
            } catch(DmlException e) {
                addErrorMessage(e);
            } catch(CalloutException e) {
                addErrorMessage(e);
            } catch(SAPIntegrationService.SAPIntegrationServiceException e) {
                addErrorMessage(e);
            }
        } else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, EMPTY_PROJECT_ACCOUNT_ERROR));
        }

        return ref;
    }

    public PageReference createContractInSAP() {
        PageReference ref;

        if (oppty.ShipTo_AccountId__c != null) {
            try {
                oppty.SAP_ContractId__c = SAPIntegrationService.createContractInSAP(oppty);
                update oppty;
                ref = redirectToOpportunity();
            } catch(DmlException e) {
                addErrorMessage(e);
            } catch(CalloutException e) {
                addErrorMessage(e);
            } catch(QueryException e) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,
                    CUSTOM_SETTING_ERROR));
            } catch(SAPIntegrationService.SAPIntegrationServiceException e) {
                addErrorMessage(e);
            }
        } else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, EMPTY_SHIP_TO_ERROR));
        }

        return ref;
    }

    public PageReference redirectToOpportunity() {
        return new PageReference('/' + oppty.Id);
    }

    private void addErrorMessage(Exception e) {
        String logId = (String) ExceptionLogger.logError(e, oppty.Id);
        String errorMessage = String.format(ERROR_TEMPLATE, new List<String>{logId});
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, errorMessage));
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, e.getMessage()));
    }
}