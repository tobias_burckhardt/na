@IsTest
private class RoofingOpportunityTechRequestExtTest {

    @TestSetup
    static void setup() {
        User internalUser = TestingUtils.createTestUser('BluewolfInternal', 'System Administrator');
        UserRole internalRole = (UserRole) SObjectFactory.create(UserRole.SObjectType, new Map<SObjectField, Object> {
                UserRole.Name => 'InternalRole'
        });
        internalUser.UserRoleId = internalRole.Id;
        insert internalUser;

        // Creating the Account in runAs to avoid mixed DML exception
        Account acc;
        Contact c;
        System.runAs(internalUser) {
            SObjectFactory.create(Opportunity.SObjectType);

            acc = (Account) SObjectFactory.create(Account.SobjectType);
            c = (Contact) SObjectFactory.create(Contact.SobjectType, new Map<SobjectField, Object> {
                    Contact.AccountId => acc.Id
            });
        }

        System.runAs(internalUser) {
            User communityUser = TestingUtils.createTestUser('BluewolfCommunity', sika_PTS_Common.PTSUserProfileName);
            communityUser.ContactId = c.Id;
            insert communityUser;
        }
    }

    @IsTest
    static void testControllerControlVars() {
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

        ApexPages.StandardController standardController = new ApexPages.StandardController(opp);
        RoofingOpportunityTechRequestExt controller = new RoofingOpportunityTechRequestExt(standardController);

        Test.startTest();
        System.assert(!controller.doFlow);
        System.assert(!controller.isAccepted);

        controller.isAccepted = true;
        controller.proceed();
        System.assert(controller.doFlow);
        Test.stopTest();
    }

    @IsTest
    static void testFlowForCommunity() {
        User communityUser = [SELECT Id FROM User WHERE Username = 'BluewolfCommunity@test.com' LIMIT 1];
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

        ApexPages.StandardController standardController = new ApexPages.StandardController(opp);
        RoofingOpportunityTechRequestExt controller = new RoofingOpportunityTechRequestExt(standardController);

        Test.startTest();
        System.runAs(communityUser) {
            System.assertEquals('/PTS/' + opp.Id, controller.flowFinishLocation.getUrl());
        }
        Test.stopTest();
    }

    @IsTest
    static void testFlowForInternal() {
        User internalUser = [SELECT Id FROM User WHERE Username = 'BluewolfInternal@test.com' LIMIT 1];
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

        ApexPages.StandardController standardController = new ApexPages.StandardController(opp);
        RoofingOpportunityTechRequestExt controller = new RoofingOpportunityTechRequestExt(standardController);

        Test.startTest();
        System.runAs(internalUser) {
            System.assertEquals('/' + opp.Id, controller.flowFinishLocation.getUrl());
        }
        Test.stopTest();
    }
}