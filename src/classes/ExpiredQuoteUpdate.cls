public with sharing class ExpiredQuoteUpdate extends ExpiredQuotesBatch{

    private final Apttus_Proposal__Proposal__c quote;

    public ExpiredQuoteUpdate(ApexPages.StandardController stdController) {
        super();
        this.quote = (Apttus_Proposal__Proposal__c)stdController.getRecord();
    }

    public void updateQuote(){
        List<Apttus_Proposal__Proposal__c> theQuote = new List<Apttus_Proposal__Proposal__c>{this.quote};
        updateExpiredQuotes(theQuote);
        List<Apttus_Config2__ProductConfiguration__c> configs = updateProductConfiguration(theQuote);

        Savepoint sp = Database.setSavepoint();
        try{
            update theQuote;
            update configs;
        }
        catch(DMLException dmle){
            Database.rollback(sp);
            ApexPages.addMessages(dmle);
            return;
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The update succeeded'));
    }

}