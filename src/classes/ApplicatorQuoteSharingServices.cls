/**
 * Collection of services methods used for sharing logic on the Applicator Quote and related
 * Opportunity
 */
public without sharing class ApplicatorQuoteSharingServices {


  /**
   * Shares the new Applicator Quote and the related Opportunity to all the PTS Community users
   * that are related 
   * @param newQuotes [description]
   */
  public static void shareApplicatorQuoteWithPTS(List<Applicator_Quote__c> newQuotes) {
    newQuotes = [SELECT Id, Opportunity__c, Account__c, Contact__c, OwnerId FROM Applicator_Quote__c WHERE Id IN :newQuotes];

    Set<Id> accountIds = new Set<Id>();
    Set<Id> oppIds = new Set<Id>();
    Map<Id, List<Applicator_Quote__c>> quotesByOpptyId = new Map<Id, List<Applicator_Quote__c>>();
    for (Applicator_Quote__c newQuote : newQuotes) {
      Id opptyId = newQuote.Opportunity__c;
      Id accountId = newQuote.Account__c;
      if (opptyId != null && accountId != null && newQuote.Contact__c != null) {
        accountIds.add(accountId);
        oppIds.add(opptyId);
        if (quotesByOpptyId.containsKey(opptyId)) {
          quotesByOpptyId.get(opptyId).add(newQuote);
        } else {
          quotesByOpptyId.put(opptyId, new List<Applicator_Quote__c>{newQuote});
        }
      }
    }
    List<User> users = getPtsUsers(accountIds);
    
    if (!users.isEmpty()) {
      createOpptyShares(users,quotesByOpptyId);
      createApplQuoteShares(users,quotesByOpptyId);

    }

  }


  public static void handleDeleteSharing(List<Applicator_Quote__c> deletedQuotes, Map<Id, Applicator_Quote__c> oldAppQ){

    //Get all quotes where NOA was awarded,
    List<Applicator_Quote__c> appQuotesToShare = new List<Applicator_Quote__c>();
    Set<Id> oppIds = new Set<Id>();
    Set<Id> accountIds = new Set<Id>();
    //Set<Id> oppIdsToRemoveSharing = new Set<Id>();
    for(Applicator_Quote__c appQ : deletedQuotes){
      Id opptyId = appQ.Opportunity__c;
      if(oldAppQ.containsKey(appQ.Id) && oldAppQ.get(appQ.Id).Award_NOA__c == true){
        //appQuotesToShare.add(appQ);
        oppIds.add(opptyId);
      }
    }

    System.debug('MSA oppIds.size() = ' + oppIds.size());
    if(oppIds.size() > 0){
      removeOldShareRecords(oppIds);
    }
        
  

  }

  /*
    ASSUMPTIONS: We will delete all old share records on the opportunites and application quote then
    create new share records for the new Applicator quote awarded NOA

    START NEW STUFF
  */
 
  public static void handleUpdateSharing(List<Applicator_Quote__c> newQuotes, Map<Id, Applicator_Quote__c> oldAppQ) {
    //Get all quotes where NOA was awarded,
    List<Applicator_Quote__c> appQuotesToShare = new List<Applicator_Quote__c>();
    Set<Id> oppIds = new Set<Id>();
    Set<Id> accountIds = new Set<Id>();
    //Set<Id> oppIdsToRemoveSharing = new Set<Id>();
    Map<Id, List<Applicator_Quote__c>> quotesByOpptyId = new Map<Id, List<Applicator_Quote__c>>();
    for(Applicator_Quote__c appQ : newQuotes){
      Id opptyId = appQ.Opportunity__c;
      if(oldAppQ.containsKey(appQ.Id) && oldAppQ.get(appQ.Id).Award_NOA__c == false && appQ.Award_NOA__c == true){
        //appQuotesToShare.add(appQ);
        oppIds.add(opptyId);
        if(appQ.Account__c != null){
          accountIds.add(appQ.Account__c);
          if (quotesByOpptyId.containsKey(opptyId)) {
            quotesByOpptyId.get(opptyId).add(appQ);
          } else {
            quotesByOpptyId.put(opptyId, new List<Applicator_Quote__c>{appQ});
          }
        }
        
      }
      /*
      if(oldAppQ.containsKey(appQ.Id) && oldAppQ.get(appQ.Id).Award_NOA__c == true && appQ.Award_NOA__c == false){
        oppIdsToRemoveSharing.add(appQ.Opportunity__c);
      }*/
    }

    List<User> users = getPtsUsers(accountIds);
    if(oppIds.size() > 0){
      removeOldShareRecords(oppIds);
    }
        
        System.debug('quotesByOpptyId = ' + quotesByOpptyId.size());
    if(users.size() > 0  && quotesByOpptyId.size() > 0){
      createOpptyShares(users,quotesByOpptyId);
    }

    //we now have all newly awarded NOA quotes.
    //remove all sharing(community only)
    
  }

//private static void createOpptyAndApplQuoteShares(List<User> users, Map<Id, List<Applicator_Quote__c>> quotesByOpptyId
  public static void removeOldShareRecords( Set<id> oppIds ){
    List<OpportunityShare> oppShares = getOpportunityShareRecords(oppIds);
    if(oppShares.size() > 0){
      deleteOppShares(oppShares);
    }
  }
  private static List<OpportunityShare> getOpportunityShareRecords(Set<id> oppIds) {
    return[SELECT Id
    FROM OpportunityShare
    WHERE OpportunityId IN :oppIds];
    
  }

  private static List<Applicator_Quote__c> getApplicationQuoteByOppoIds(Set<id> oppIds) {
    return[SELECT Id,Opportunity__c
      FROM Applicator_Quote__c
      WHERE Opportunity__c IN :oppIds];
      
  }




  private static void deleteOppShares(List<OpportunityShare> oppShares){
        Database.DeleteResult[] DR_Dels = Database.delete(oppShares,false);

  }

  private static void deleteApplQuoteShares(List<Applicator_Quote__Share> applQuoteShares){
        Database.DeleteResult[] DR_Dels = Database.delete(applQuoteShares,false);

  }


  //END NEW STUFF


  private static List<User> getPtsUsers(Set<Id> accountIds) {
    return [
      SELECT Contact.AccountId
      FROM User
      WHERE Contact.AccountId IN :accountIds
      AND IsActive = TRUE
      AND Profile.Name IN :Label.PartnerUserProfileNames.split(',')
    ];
  }


  private static void createOpptyShares(List<User> users, Map<Id, List<Applicator_Quote__c>> quotesByOpptyId) {
    Map<Id, List<User>> usersByAccountId = (Map<Id, List<User>>) GroupBy.ids('Contact.AccountId', users);

    List<OpportunityShare> opptyShares = new List<OpportunityShare>();
    List<Applicator_Quote__Share> quoteShares = new List<Applicator_Quote__Share>();

    // Set to hold the unique combinations of User Id and Oppty Id since a User can be related
    // to the same Oppty through different Applicator Quotes
    Set<String> opptyUserUniqueKeys = new Set<String>();
    List<Opportunity> opps = [SELECT Id, AccountId, OwnerId FROM Opportunity WHERE Id IN :quotesByOpptyId.keySet()];
        System.debug('opps size = ' + opps.size());
    for (Opportunity opp : opps) {
      System.debug('opp = ' + opp);
      System.debug('opp.Id = ' + opp.Id);
      System.debug('quotesByOpptyId = ' + quotesByOpptyId);
      for (Applicator_Quote__c quote : quotesByOpptyId.get(opp.Id)) {
        Id accountId = quote.Account__c;
        System.debug('quote = ' + quote);
        if (usersByAccountId.get(accountId) != null) {

          for (User u : usersByAccountId.get(accountId)) {
           System.debug('in users');
            if (opp.AccountId == quote.Account__c || opp.AccountId == null) {
                             System.debug('accountId is not null');
                            
              String opptyUserUniqueKey = String.valueOf(opp.Id) + String.valueOf(u.Id);
                            System.debug('opptyUserUniqueKey = ' + opptyUserUniqueKey);
              if (!opptyUserUniqueKeys.contains(opptyUserUniqueKey)) {
                opptyUserUniqueKeys.add(opptyUserUniqueKey);
                                
                if (opp.OwnerId != u.Id) {
                  OpportunityShare opptyShare = createOpptyShare(opp.Id, u.Id, 'Edit');
                  opptyShares.add(opptyShare);
                }
              }
            }
          }
        }
      }
    }
           System.debug('opptyShares.size()' + opptyShares.size());
    if (!opptyShares.isEmpty()) {
      insert opptyShares;
    }

  }


  private static void createApplQuoteShares(List<User> users, Map<Id, List<Applicator_Quote__c>> quotesByOpptyId) {
    Map<Id, List<User>> usersByAccountId = (Map<Id, List<User>>) GroupBy.ids('Contact.AccountId', users);

    List<Applicator_Quote__Share> quoteShares = new List<Applicator_Quote__Share>();

    // Set to hold the unique combinations of User Id and Oppty Id since a User can be related
    // to the same Oppty through different Applicator Quotes
    Set<String> opptyUserUniqueKeys = new Set<String>();
    List<Opportunity> opps = [SELECT Id, AccountId, OwnerId FROM Opportunity WHERE Id IN :quotesByOpptyId.keySet()];
    for (Opportunity opp : opps) {
      for (Applicator_Quote__c quote : quotesByOpptyId.get(opp.Id)) {
        Id accountId = quote.Account__c;
        if (usersByAccountId.get(accountId) != null) {
          for (User u : usersByAccountId.get(accountId)) {
            if (quote.OwnerId != u.Id) {
              Applicator_Quote__Share quoteShare = createApplQuoteShare(quote.Id, u.Id, 'Edit');
              quoteShares.add(quoteShare);
            }
          }
        }
      }
    }
        

    if (!quoteShares.isEmpty()) {
      insert quoteShares;
    }
  }

  private static Applicator_Quote__Share createApplQuoteShare(Id quoteId, Id userId, String access) {
    Applicator_Quote__Share quoteShare = new Applicator_Quote__Share(
      ParentId = quoteId,
      UserOrGroupId = userId,
      AccessLevel = access,
      RowCause = Schema.Applicator_Quote__Share.RowCause.Manual
    );
    return quoteShare;
  }

  private static OpportunityShare createOpptyShare(Id opptyId, Id userId, String access) {
    OpportunityShare opptyShare = new OpportunityShare(
      OpportunityId = opptyId,
      UserOrGroupId = userId,
      OpportunityAccessLevel = access,
      RowCause = Schema.OpportunityShare.RowCause.Manual
    );
    return opptyShare;
  }


  //NEW RANDY CODE, DELETE ABOVE WHEN NO REFERENCES LEFT
  public static void addApplicatorQuoteSharesToCommunityUsers(Map<Id,List<Id>> userToApplicatorQuoteMap){
    List<Applicator_Quote__Share> quoteShares = new List<Applicator_Quote__Share>();
    for(Id usrId : userToApplicatorQuoteMap.keySet()){
      List<Id> quoteIds = userToApplicatorQuoteMap.get(usrId);
      for(Id quoteId : quoteIds){
        Applicator_Quote__Share quoteShare = createApplQuoteShare(quoteId, usrId, 'Edit');
        quoteShares.add(quoteShare);
      }
    }
    insert quoteShares;
  }

}