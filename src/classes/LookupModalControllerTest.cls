@isTest
private class LookupModalControllerTest {
	
	@testSetup
    public static void setup() {
    	 Account acc1 = (Account) SObjectFactory.create(Account.SObjectType, new Map<SObjectField, Object> {
                Account.Name => 'Account1'
        });

    	 Contact con1 = TestingUtils.createContact('Con1 LastName', true);

    	 Test_Method__c tm1 = new Test_Method__c(Name='Test Method Name 1');
    	 insert tm1;

    	 CPD_Technical_Service_Request__c tsr1 = new CPD_Technical_Service_Request__c();
    	 insert tsr1;

    	 
    }


	@isTest 
	static void getRelatedDataUnitTest() {
		// Implement test code
		String lookupDataJSON = '';
		Test.startTest();
			lookupDataJSON = LookupModalController.getRelatedData('Account','Contact','LastName','LastName');
		Test.stopTest();

		LookupModalController.LookUpDataEnvelope envelope = (LookupModalController.LookUpDataEnvelope)JSON.deserialize(lookupDataJSON, LookupModalController.LookUpDataEnvelope.class);

		System.assertEquals(1, envelope.records.size(),'There should be 1 user record');
	}

/*
	@isTest
	static void handleSavingTest(){

		
		List<Required_Test__c> beforeTests = [SELECT ID FROM Required_Test__c];
		System.assertEquals(beforeTests.size(), 0 ,'There should be no required tests in the system.');



		List<CPD_Technical_Service_Request__c> testRequests = [SELECT Id FROM CPD_Technical_Service_Request__c];
		List<Test_Method__c> testMethods = [SELECT Id, Name FROM Test_Method__c];
		List<String> ids = new List<String>();
		ids.add(testMethods[0].Id);
		String junctionApiName = 'Required_Test__c';
		String parentApiName = 'Technical_Service_Request__c';


		String childApiName = 'Test_Method__c';

		String parentId = testRequests[0].Id;

		Test.startTest();
			LookupModalController.handleSaving(ids,junctionApiName,parentApiName,childApiName,parentId);
		Test.stopTest();

		List<Required_Test__c> afterTests = [SELECT ID FROM Required_Test__c];
		System.assertEquals(afterTests.size(), 1 ,'There should be one required tests in the system.');

	}
*/

	@isTest 
	static void givenApiName_shouldReturnLabel(){
		String label = '';
		Test.startTest();
			label = LookupModalController.getObjectNameFromApiName('Account');
		Test.stopTest();

		System.assertNotEquals('', label ,'There should be a label of from the account.');

	}

	@isTest
	static void givenObject_ShouldInsert(){
		

		 sObject sObj = Schema.getGlobalDescribe().get('Account').newSObject();
		 sObj.put('Name', 'Test Account 2');
		 List<sObject> accounts = new List<sObject>();
		 accounts.add(sObj);

		
		Test.startTest();
			 LookupModalController.saveJunctionObject(accounts);
		Test.stopTest();

		List<Account> afterAccounts = [SELECT ID FROM Account];
		System.assertEquals(afterAccounts.size(), 2, 'There should be two accounts that exist');
	}

	
}