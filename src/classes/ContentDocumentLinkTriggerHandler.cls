public class ContentDocumentLinkTriggerHandler {

    public static void handleBeforeDelete(List<ContentDocumentLink> documentLinks) {
        restrictDelete(documentLinks);
    }

    public static void handleAfterInsert(List<ContentDocumentLink> documentLinks) {
        createSharingForRPADocuments(documentLinks);
        sendToAWS(documentLinks);
    }

    public static void handleAfterUpdate(List<ContentDocumentLink> documentLinks) {
        sendToAWS(documentLinks);
    }

    private static void restrictDelete(List<ContentDocumentLink> documentLinks) {
        for (ContentDocumentLink cdl : documentLinks) {
            List<Case> linkedCases =
                [SELECT RecordType.DeveloperName from Case where Id = :cdl.LinkedEntityId];
            if (!linkedCases.isEmpty()) {
                Boolean isRestricted = RestrictedCaseRecordsUtil.isRestrictedCaseRecordType(
                    linkedCases[0].Id, linkedCases[0].RecordType.DeveloperName);
                if (isRestricted) {
                    cdl.addError(System.Label.Restricted_Object_Delete);
                }
            }
        }
    }

    private static void createSharingForRPADocuments(List<ContentDocumentLink> links) {
        List<ContentDocumentLink> rpaLinks = ContentDocumentLinkServices.filterRPARelated(links);
        if (!rpaLinks.isEmpty()) {
            Map<Id, Request_for_Project_Approval__c> approvals =
                    ContentDocumentLinkServices.queryRPAs(rpaLinks);
            ContentDocumentLinkServices.createNewSharingForRPA(rpaLinks, approvals);
        }
    }

    private static void sendToAWS(List<ContentDocumentLink> links) {
        AWS_v4_auth awsS3 = new AWS_v4_auth();
        Map<Id, ContentWorkspace> contentWorkspacesById = new Map<Id, ContentWorkspace>([SELECT Id, Name FROM ContentWorkspace WHERE Id IN :Pluck.ids('LinkedEntityId', links)]);
        Set<Id> contentDocumentIdsToPublish = new Set<Id>();

        for (ContentDocumentLink doc : links) {
            if (contentWorkspacesById.containsKey(doc.LinkedEntityId) &&
                    contentWorkspacesById.get(doc.LinkedEntityId).Name.contains(AWS_v4_auth.lib)) {
                contentDocumentIdsToPublish.add(doc.ContentDocumentId);
            }
        }

        List<ContentVersion> contentVersions = [SELECT Id, Title, Document_Type__c, Sub_Type_Level_1__c, Sub_Type_Level_2__c, Sub_Type_Level_3__c, FileExtension, ContentDocumentId, VersionData, ContentSize FROM ContentVersion WHERE ContentDocumentId IN :contentDocumentIdsToPublish];
        for (ContentVersion fileVersion : contentVersions) {
            awsS3.saveToS3(fileVersion, fileVersion.FileExtension);
        }
    }
}