@isTest 
public class CommunityProjectEditControllerTest {
    static testMethod void TestCommunityProjectEditController () {

        Project__c p1 = new Project__c();
        //p1.Community_User_ID__c = UserInfo.getUserId();
        p1.name = 'test';
        p1.City__c = 'city';
        p1.ProjectValue__c = 10.4;
        p1.BidDate__c = Date.Today();
        insert p1;
        
        Warranty__c w1 = new Warranty__c();
        w1.Project__c = p1.id;
        insert w1;

        test.startTest();
        CommunityProjectEditController cpec1 = new CommunityProjectEditController();

        ApexPages.currentPage().getParameters().put('WarrNum','0');
        ApexPages.currentPage().getParameters().put('pid', p1.id);
        
        CommunityProjectEditController cpec2 = new CommunityProjectEditController();
        cpec2.deleteWarranties();
        cpec2.SaveProject();
        cpec2.SaveAndCreateWarranty();
        
        test.stopTest();
    }
}