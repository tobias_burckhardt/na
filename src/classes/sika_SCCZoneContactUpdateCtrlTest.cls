@isTest
private class sika_SCCZoneContactUpdateCtrlTest {
	
	private Static User theUser;
	private Static User adminUser;
	private Static Account theAccount;
	private Static Contact theContact;

	// create a map so we can more easily test the Communities user and the Communities user's Contact with the same params
	private Static map<String, String> user_and_contact_values = new map<String, String>{
			'firstName' => 'Test',
			'lastName' => 'SCCZoneUser',
			'title' => 'My Title',
			'email' => 'testSCCZoneUser@mail.com',
			'phone' => '202-222-2222',
			'mailingStreet' => '123 N. Main St',
			'mailingCity' => 'Anytown',
			//'mailingStateCode' => 'PA',
			'mailingState' => 'PA',
			'mailingPostalCode' => '12345XYZ',
			//'mailingCountryCode' => 'US'
			'mailingCountry' => 'US'};

	private static void doInitialSetUp() {  

		map<String, String> params = new map<String, String>{
			'account_name' => 'Test Account',
			'account_type' => 'Test Account Type',
			'contact_firstName' => user_and_contact_values.get('firstName'),
			'contact_lastName' => user_and_contact_values.get('lastName'),
			'contact_title' => user_and_contact_values.get('title'),
			'contact_email' => user_and_contact_values.get('email'),
			'contact_phone' => user_and_contact_values.get('phone'),
			'contact_mailingStreet' => user_and_contact_values.get('mailingStreet'),
			'contact_mailingCity' => user_and_contact_values.get('mailingCity'),
			'contact_mailingState' => user_and_contact_values.get('mailingState'),
			'contact_mailingPostalCode' => user_and_contact_values.get('mailingPostalCode'),
			'contact_mailingCountry' => user_and_contact_values.get('mailingCountry'),
			'commsUser_userName' => user_and_contact_values.get('email')};

		map<String, sObject> theObjects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount(params);
		theAccount = (Account) theObjects.get('theAccount');
		theContact = (Contact) theObjects.get('theContact');
		theUser = (User) theObjects.get('SCCZoneUser');
		
	}

	// ensure everything works correctly when it should
	@isTest static void test_happy_path() {
		doInitialSetUp();

		// set page we'll be testing
		PageReference pageRef = new PageReference('/sika_SCCZoneContactUpdatePage');
		Test.setCurrentPage(pageRef);

		// run as SCC Zone communities user
		System.runAs(theUser){

			// create an instance of the controller
			sika_SCCZoneContactUpdateCtrl ctrl = new sika_SCCZoneContactUpdateCtrl();

			// confirm that the page loaded. (Didn't redirect to an error page)
			system.assertEquals(pageRef.getURL(), ApexPages.CurrentPage().getURL());

			// confirm that the page is displaying the current user's info (and the current User's associated Contact record info)
			Id currentUserId = UserInfo.getUserId();
			system.assertEquals(currentUserId, theUser.Id);

			system.assertEquals(ctrl.firstName, user_and_contact_values.get('firstName'));
			system.assertEquals(ctrl.lastName, user_and_contact_values.get('lastName'));
			system.assertEquals(ctrl.jobTitle, user_and_contact_values.get('title'));
			system.assertEquals(ctrl.emailAddress, user_and_contact_values.get('email').toLowercase()); // Salesforce automatically converts email addresses lowercase on insert. 
			system.assertEquals(ctrl.streetAddress, user_and_contact_values.get('mailingStreet'));
			system.assertEquals(ctrl.city, user_and_contact_values.get('mailingCity'));
			//system.assertEquals(ctrl.theUser.stateCode, user_and_contact_values.get('mailingStateCode'));
			system.assertEquals(ctrl.theUser.state, user_and_contact_values.get('mailingState'));
			//system.assertEquals(ctrl.theUser.countryCode, user_and_contact_values.get('mailingCountryCode'));
			system.assertEquals(ctrl.theUser.country, user_and_contact_values.get('mailingCountry'));
			system.assertEquals(ctrl.postalCode, user_and_contact_values.get('mailingPostalCode'));
			system.assertEquals(ctrl.companyName, theAccount.Name);
			system.assertEquals(ctrl.companyType, theAccount.Type);

			// make one change
			ctrl.phone = '202-222-3333';

			// cancel & confirm that we're taken to the user home page
			system.assertEquals(String.valueOf(ctrl.cancel()), String.valueOf(new PageReference('/sika_SCCZoneUserHomePage')));

			// re-load the update contact info page. Confirm change has not been saved
			ctrl = new sika_SCCZoneContactUpdateCtrl();
			system.assertEquals(ctrl.phone, user_and_contact_values.get('phone'));

			// change all writeable fields to new (and valid) values
			map<String, String> newValues = new map<String, String>{
				'jobTitle' => 'newTitle',
				'email' => 'newEmail@mail.com',
				//'countryCode' => 'CA', // Canada
				'country' => 'CA',
				'phone' => '202-222-4444',
				'address' => 'New Address',
				'city' => 'Newtown',
				//'stateCode' => 'ON', // Ontario
				'state' => 'ON',
				'postalCode' => 'L5T 1L5'};

			ctrl.jobTitle = newValues.get('jobTitle');
			ctrl.emailAddress = newValues.get('email');
			//ctrl.theUser.countryCode = newValues.get('countryCode');
			ctrl.country = newValues.get('country');
			ctrl.phone = newValues.get('phone');
			ctrl.streetAddress = newValues.get('address');
			ctrl.city = newValues.get('city');
			//ctrl.theUser.stateCode = newValues.get('stateCode');
			ctrl.state = newValues.get('state');
			ctrl.postalCode = newValues.get('postalCode');

			// validate the new form field values
			//ctrl.stateRequired = true;
			system.assertEquals(ctrl.validateForm(), true);
			system.assertEquals(ctrl.hasErrors, false);

			// save the changes and confirm that the user has been redirected to the user home page
			test.startTest();
			system.assertEquals(String.valueOf(ctrl.save()), String.valueOf(new PageReference('/sika_SCCZoneUserHomePage')));
			test.stopTest();

			// confirm that Contact and Account DML writes were successful
			system.assert(ctrl.userSaveResult.isSuccess());
			system.assert(ctrl.contactSaveResult.isSuccess());
			
			// query User record & confirm changes have been made
			User updatedUser = [SELECT accountId, 
									   contactId, 
									   firstName, 
									   lastName, 
									   title, 
									   email, 
									   phone, 
									   street, 
									   city, 
									   //stateCode, 
									   state,
									   postalCode, 
									   //countryCode
									   country 
								  FROM User 
								 WHERE Id = :theUser.Id];

			// the following fields should not have changed:
			system.assertEquals(updatedUser.accountId, theAccount.Id);
			system.assertEquals(updatedUser.contactId, theContact.Id);
			system.assertEquals(updatedUser.firstName, user_and_contact_values.get('firstName'));
			system.assertEquals(updatedUser.lastName, user_and_contact_values.get('lastName'));

			// the following fields should have been updated:
			system.assertEquals(updatedUser.title, newValues.get('jobTitle'));
			system.assertEquals(updatedUser.email, newValues.get('email').toLowercase());
			system.assertEquals(updatedUser.phone, newValues.get('phone'));
			system.assertEquals(updatedUser.street, newValues.get('address'));
			system.assertEquals(updatedUser.city, newValues.get('city'));
			//system.assertEquals(updatedUser.stateCode, newValues.get('stateCode'));
			system.assertEquals(updatedUser.state, newValues.get('state'));
			system.assertEquals(updatedUser.postalCode, newValues.get('postalCode'));
			//system.assertEquals(updatedUser.countryCode, newValues.get('countryCode'));
			system.assertEquals(updatedUser.country, newValues.get('country'));

			// query Contact recorrd & confirm changes have been made
			Contact updatedContact = [SELECT lastName, 
											 firstName, 
											 title, 
											 email, 
											 phone, 
											 mailingStreet, 
											 mailingCity, 
											 //mailingStateCode, 
											 mailingState,
											 mailingPostalCode, 
											 //mailingCountryCode
											 mailingCountry 
										FROM Contact 
									   WHERE Id = :theContact.Id];

			// the following fields should not have changed:
			system.assertEquals(updatedContact.lastName, user_and_contact_values.get('lastName'));
			system.assertEquals(updatedContact.lastName, user_and_contact_values.get('lastName'));

			//the following fields should have been updated:
			system.assertEquals(updatedContact.title, newValues.get('jobTitle'));
			system.assertEquals(updatedContact.email, newValues.get('email').toLowercase());
			system.assertEquals(updatedContact.phone, newValues.get('phone'));
			system.assertEquals(updatedContact.mailingStreet, newValues.get('address'));
			system.assertEquals(updatedContact.mailingCity, newValues.get('city'));
			//system.assertEquals(updatedContact.mailingStateCode, newValues.get('stateCode'));
			system.assertEquals(updatedContact.mailingState, newValues.get('state'));
			system.assertEquals(updatedContact.mailingPostalCode, newValues.get('postalCode'));
			//system.assertEquals(updatedContact.mailingCountryCode, newValues.get('countryCode'));
			system.assertEquals(updatedContact.mailingCountry, newValues.get('country'));

		}
		
	}


	// ensure things fail when they should. (Invalid data entered, etc.)
	@isTest static void test_sad_path() {
		doInitialSetUp();

		// set page we'll be testing
		PageReference pageRef = new PageReference('/sika_SCCZoneContactUpdatePage');
		Test.setCurrentPage(pageRef);

		// run as SCC Zone communities user
		System.runAs(theUser){

			// create an instance of the controller
			sika_SCCZoneContactUpdateCtrl ctrl = new sika_SCCZoneContactUpdateCtrl();

			// clear all field values
			ctrl.jobTitle = null;
			ctrl.emailAddress = null;
			ctrl.phone = null;
			ctrl.streetAddress = null;
			ctrl.city = null;
			//ctrl.theUser.stateCode = null;
			ctrl.state = null;
			ctrl.postalCode = null;
			//ctrl.theUser.countryCode = null;
			ctrl.country = null;

			//confirm that submission returns the expected errors
			//ctrl.stateRequired = true;
			system.assert(ctrl.save() == null);
			system.assertEquals(ctrl.stateError, true);

			system.assertEquals(ctrl.hasErrors, true);
			system.assertEquals(ctrl.emailIsBlank, true);
			system.assertEquals(ctrl.emailIsInvalid, false);
			system.assertEquals(ctrl.jobTitleError, true);
			system.assertEquals(ctrl.streetAddressError, true);
			system.assertEquals(ctrl.cityError, true);
			system.assertEquals(ctrl.postalCodeError, true);
			system.assertEquals(ctrl.countryError, true);

			// cancel & confirm that we're taken to the user home page
			system.assertEquals(String.valueOf(ctrl.cancel()), String.valueOf(new PageReference('/sika_SCCZoneUserHomePage')));
			// re-load the update contact info page.
			ctrl = new sika_SCCZoneContactUpdateCtrl();

			// enter only whitespace in one field and make sure it doesn't validate
			ctrl.jobTitle = '    ';
			ctrl.validateForm();
			system.assert(ctrl.jobTitleError);

			// enter an invalid email address and ensure that it doesn't validate
			ctrl.emailAddress = 'e@mail';
			ctrl.validateForm();
			system.assertEquals(ctrl.emailIsBlank, false);
			system.assertEquals(ctrl.emailIsInvalid, true);

			// (Other invalid email address patterns have been tested against the email validation method used in the controller. No need to repeat here.)
		
			// enter valid values in all fields and confirm that validation errors are cleared
			ctrl.jobTitle = 'That guy';
			ctrl.emailAddress = 'thisGuy@email.com';
			system.assertEquals(ctrl.validateForm(), true);
		}
	}


	// ensure the system handles errors that shouldn't occur. (System errors, governor limits exceeded, permissions errors, etc -- not user interaction-related errors)
	@isTest static void test_bad_path() {
		doInitialSetUp();

		// ensure the page loads correctly when the User's record, and User's Contact record, have null values in displayed fields
		theUser.firstName = null;
		theContact.email = null;

		update(theUser);

		System.runAs(theUser){ // ...to avoid MIXED_DML_OPERATION error
			update(theContact);
		}

		PageReference pageRef = new PageReference('/sika_SCCZoneContactUpdatePage');
		Test.setCurrentPage(pageRef);

		System.runAs(theUser){
			// create an instance of the controller
			sika_SCCZoneContactUpdateCtrl ctrl = new sika_SCCZoneContactUpdateCtrl();

			// confirm that the page loaded. (Didn't redirect to an error page)
			system.assertEquals(pageRef.getURL(), ApexPages.CurrentPage().getURL());

		}
	}


	@isTest static void testLoadAction(){
		sika_SCCZoneContactUpdateCtrl ctrl;
        User guestUser = sika_SCCZoneTestFactory.createGuestUser();
        insert(guestUser);

        PageReference p;

        doInitialSetUp();
        System.runAs(theUser){
            ctrl = new sika_SCCZoneContactUpdateCtrl();
            p = ctrl.loadAction();
            system.assertEquals(p, null);
        }
    }

}