public with sharing class WaranttyValidationHelper {
  
  // email validation method. (*Note - returns TRUE if it's passed a VALID email address.)
    public static Boolean validateEmail(String theAddress){
        //String emailPattern = '^[a-zA-Z]+[A-Za-z0-9_.]*@[A-Za-z0-9.]+\\.[A-Za-z]+$';
        //// ** NOTE ** all backslashes have to be escaped...by placing a backslash in front of them...in order to avoid an invalid string literal error.
        String emailPattern = '^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$';
        Pattern myPattern = Pattern.compile(emailPattern);
        Matcher emailPatternMatch = myPattern.matcher(theAddress);
        return emailPatternMatch.matches();
    }


    // test price fields
    public static Boolean validatePriceField(String theField){
        if(theField == null){
            return false;
        }
        if(!String.isBlank(theField)){
            if(theField.isAlpha()){
                return false;
            }
            if(theField.contains('.') && theField.length() < 2){
              return false;
            }
            return true;
        }
        return false;
    }

}