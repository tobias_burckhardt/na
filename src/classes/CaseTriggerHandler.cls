public with sharing class CaseTriggerHandler {

    @TestVisible
    static Map<String, Open_Order__c> orderMap = new Map<String, Open_Order__c>();
    @TestVisible
    static Map<String, Account> customerToAccountMap = new Map<String, Account>(); 
    @TestVisible
    static Map<String, Invoice__c> invoiceMap = new Map<String, Invoice__c>();
    @TestVisible
    static Map<String, Contact> emailToContactMap = new Map<String, Contact>(); 
    @TestVisible
    static Set<Case> allCases = new Set<Case>();
    
	public static final String WEB2CASENAME = 'Web_to_Case';

	public static void filterWebToCase(List<Case> cases)
	{
		Id web2CaseId = [SELECT Id, DeveloperName 
							FROM RecordType 
							WHERE DeveloperName =: WEB2CASENAME][0].Id;
		for(Case c : cases)
		{
			
			if(c.RecordTypeId == web2CaseId)
			{
				
				if(c.Order_Number__c != null || c.Invoice_Number__c != null || c.Customer_Number__c != null) 
				{
					allCases.add(c);
					
				}
				if(c.Customer_Email__c != null)	
				{
					allCases.add(c);
				}
				
			}
			
		}
		
	}
	
	public static void populateAccountandContactMaps()
	{
		
		if(allCases.isEmpty())
			return;

		List<Case> caseList = new List<Case>(allCases);	
		
		Set<String> customerNumbers = Pluck.strings('Customer_Number__c', caseList);
		for(Account acc : [SELECT Id, SAP_Account_Code__c  
								FROM Account 
								WHERE SAP_Account_Code__c IN :customerNumbers
								AND SAP_Account_Code__c != NULL]){
			customerToAccountMap.put(acc.SAP_Account_Code__c, acc);
		}

		Set<String> invoiceNumbers = Pluck.strings('Invoice_Number__c', caseList);
		for(Invoice__c inv : [SELECT Id, Name, Account__c 
								FROM Invoice__c 
								WHERE Name IN :invoiceNumbers
								AND Account__c != NULL AND Name != NULL]){
			invoiceMap.put(inv.Name, inv);
		} 

		Set<String> orderNumbers = Pluck.strings('Order_Number__c', caseList);
		for(Open_Order__c oo : [SELECT Id, SAP_Order_Code__c, Account__c 
										FROM Open_Order__c 
										WHERE SAP_Order_Code__c IN :orderNumbers
										AND Account__c != NULL AND SAP_Order_Code__c != NULL]){
			orderMap.put(oo.SAP_Order_Code__c, oo);
		}

		Set<String> contactEmails = Pluck.strings('Customer_Email__c', caseList);
		for(Contact con : [SELECT Id, Email 
										FROM Contact 
										WHERE Email IN :contactEmails]){
			emailToContactMap.put(con.Email, con);
		}

	}

	public static void caseFieldMapping() 
	{
		if(allCases.isEmpty())
			return;
		
		for(Case cs : allCases)
		{
			if( cs.Customer_Email__c != NULL && emailToContactMap.containsKey(cs.Customer_Email__c) )
			{
				cs.ContactId = emailToContactMap.get(cs.Customer_Email__c).Id;
			}
			
			if( cs.Invoice_Number__c != NULL && invoiceMap.containsKey(cs.Invoice_Number__c) )
			{
				cs.AccountId = invoiceMap.get(cs.Invoice_Number__c).Account__c;
			}
			else if( cs.Order_Number__c != NULL && orderMap.containsKey(cs.Order_Number__c) )
			{
				cs.AccountId = orderMap.get(cs.Order_Number__c).Account__c;
			}
			else if( cs.Customer_Number__c != NULL && customerToAccountMap.containsKey(cs.Customer_Number__c) )
			{
				cs.AccountId = customerToAccountMap.get(cs.Customer_Number__c).Id;
			}
		}
	}
}