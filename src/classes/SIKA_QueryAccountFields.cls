/**************************************************************************************************************************************
-------------------------------------------------------------------------DEVELOPED BY COGNIZANT TECHNOLOGY SOLUTIONS  -------------------------------------------------------------------------
Class Name: SIKA_QueryAccountFields

Purpose: Apex Class to return Account Financial Information Fields

History of Changes:
-----------------------------------------------------------------------------------------------------------------------------------
Date : 06/16/2017                          Developer: Sohini Banerjee             Comments
-----------------------------------------------------------------------------------------------------------------------------------
***************************************************************************************************************************************/
public class SIKA_QueryAccountFields {

	@AuraEnabled
	public static Account getAccountFields(String accId)
	{
		List<Account> accList = new List<Account>();
		Account retAcc = null;
		 String message ='';
		try{
		if(!string.isBlank(accId))
				{
					accList = [Select Id,Name,Receivables__c,Overdue__c,Open_Credit_Notes__c,Open_Order_Value__c,
							   Dunning_Level_1__c,Dunning_Level_2__c,Dunning_Level_3__c,Dunning_Level_4__c,SalesFiguresReportLink__c,
							  ArticleMixReport__c,ProductMixLink__c,MySalesFiguresReport__c,MyArticleMixReport__c,
							  MyProductMixReport__c,SalesFiguresBySalesRepsLink__c,ArticleMixBySalesRepsLink__c,
							  ProductMixBySalesRepsLink__c from Account where Id =: accId];
					if(!(accList.isEmpty())){
						retAcc = accList[0];
					}
				}
		}catch(Exception dmEx)
		{
			message= dmEx.getMessage();
			return null;
		}
		return retAcc;
	}

	@AuraEnabled
	public static Boolean getCurrentUserInPermissionSet(String permissionSetName) {
		List<PermissionSetAssignment> userPermissions = [
														SELECT PermissionSetId
														FROM PermissionSetAssignment
														WHERE AssigneeId = :UserInfo.getUserId()
															AND PermissionSet.Name = :permissionSetName
	];

	return userPermissions.size() > 0;
}

}