public with sharing class ProposalToBuildingGroupController {
	private Id recordId;
	public Opportunity oppRecord {get;set;}
	public Id selectedBuildingGroupId {get;set;}
	public List<Building_Group_Product__c> bgpList {get;set;}
	public List<LineItemWrapper> lineItems {get;set;}

	public ProposalToBuildingGroupController(ApexPages.StandardController stdController) {
		recordId = stdController.getId();
		if(string.isBlank(recordId)){
			recordId = ApexPages.currentPage().getParameters().get('id');
		}
		oppRecord = OpportunityServices.GetOpportunity(recordId);
	}


	/*
	Apttus_Proposal__Proposal_Line_Item__c
	https://sikana--pts.cs19.my.salesforce.com/01I290000008gyB?setupid=CustomObjects
	*/
	public void GetApttusProposalLineItems(){
		lineItems = new List<LineItemWrapper>();
		if(string.isNotBlank(recordId)){
			for(Apttus_Proposal__Proposal_Line_Item__c apli : [SELECT Id, Name, SystemID__c, Apttus_Proposal__Proposal__c,
				Apttus_QPConfig__ClassificationId__c, Apttus_QPConfig__ClassificationId__r.Name, Apttus_QPConfig__ClassificationId__r.Apttus_Config2__HierarchyId__c, Apttus_Proposal__Product__c 
				FROM Apttus_Proposal__Proposal_Line_Item__c 
				WHERE Apttus_Proposal__Proposal__r.Apttus_Proposal__Opportunity__c = :recordId]){

				lineItems.add(new LineItemWrapper(apli));
			}
		}
	}

 	public List<SelectOption> GetBuldingGroups() {
 	 	List<SelectOption> options = new List<SelectOption>();
 	 	options.add(new SelectOption('','None'));
 	 	if(string.isNotBlank(recordId)){
			for(Building_Group__c bg :[SELECT Id, Name FROM Building_Group__c WHERE Opportunity__c=:recordId]){
				options.add(new SelectOption(bg.Id,bg.Name));
			} 	 	
		}
 	 	return options;
  	}

  	public void GetSelectedBuildingGroupProduct(){
  		//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'selectedBuildingGroupId: ' + selectedBuildingGroupId));
  		bgpList = new List<Building_Group_Product__c>();
  		if(string.isNotBlank(selectedBuildingGroupId)){
			for(Building_Group_Product__c bg :[SELECT Id, Name, Category_Hierarchy__c, Product_lookup__c FROM Building_Group_Product__c WHERE Building_Group__c=:selectedBuildingGroupId]){
				bgpList.add(bg);
			}
		}
  	}

  	public PageReference Save(){
  		//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'selectedBuildingGroupId: ' + selectedBuildingGroupId));
  		if(string.isNotBlank(selectedBuildingGroupId)){
			List<Building_Group_Product__c> bgpToInsert = new List<Building_Group_Product__c>();
			for(LineItemWrapper liw :lineItems){
				if(liw.isSelected){
					Building_Group_Product__c tempBGP = new Building_Group_Product__c();
					tempBGP.Building_Group__c 			= selectedBuildingGroupId;
					tempBGP.Proposal_Line_Item__c 		= liw.apttusProposalLineItem.Id;
					tempBGP.Category_Hierarchy__c		= liw.apttusProposalLineItem.Apttus_QPConfig__ClassificationId__c;
					tempBGP.Product_lookup__c 			= liw.apttusProposalLineItem.Apttus_Proposal__Product__c;
					bgpToInsert.add(tempBGP);
				}
			}  		

			if(!bgpToInsert.isEmpty()){
				try{
					insert bgpToInsert;
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Saved!'));

					PageReference pageRef = new PageReference('/'+ recordId);
					pageRef.setRedirect(true);
					return pageRef;
				}
				catch(DmlException dmle){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'DmlException: ' + dmle.getMessage()));
				}
			}
		}
		else{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'You must select a bulding group before you can save.'));
		}
		return null;
  	}

	public class LineItemWrapper{
		public Boolean isSelected {get;set;}
		public Apttus_Proposal__Proposal_Line_Item__c apttusProposalLineItem {get;set;}
		public Building_Group_Product__c buildingGroupProduct {get;set;}
		/*
		public LineItemWrapper(){
			this.isSelected 				= false;
			this.apttusProposalLineItem 	= new Apttus_Proposal__Proposal_Line_Item__c();
			this.buildingGroupProduct 		= new Building_Group_Product__c();
		}
		*/
		public LineItemWrapper(Apttus_Proposal__Proposal_Line_Item__c appli){
			this.isSelected 				= false;
			this.apttusProposalLineItem 	= appli;
			this.buildingGroupProduct 		= new Building_Group_Product__c();
		}		
	}
}