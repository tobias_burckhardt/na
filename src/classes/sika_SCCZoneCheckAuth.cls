public without sharing class sika_SCCZoneCheckAuth {
	private static Boolean authenticated;
	private static List<String> sccIds;

	public static void setAuthStatus(Id currentUserProfileId) {
		if(sccIds == null){
            sccIds = new List<String>();
            List<Profile> profiles = [SELECT Name, Id FROM Profile WHERE Name IN :Sika_SCCZone_Common.SCCZoneUserProfileNames];    
            for(Profile p : profiles){
                sccIds.add(p.Id);
            }
        }

        
        // cast profile as string for SF known issue W-4884673
		if(sccIds.contains((String)currentUserProfileId)){
            authenticated = true;
        } else {
            authenticated = false;
        }
	}

	public static PageReference doAuthCheck(){
		if(authenticated != true){
            PageReference pageRef = new PageReference('/SCCZone/sika_SCCZoneLoginPage');
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
	}

}