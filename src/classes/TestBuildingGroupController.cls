@IsTest
private class TestBuildingGroupController {
	public static final Id PRICEBOOK_ID = Test.getStandardPricebookId();

    static List<Account> testAcc;
    static List<Contact> testCon;
    static List<Opportunity> testOpp;
    static Building_Group__c testBuildingGroup;
    static Building_Group_Product__c testBuildingGroupProduct;
    static List<Product2> pList;

    static void setup() {
        testAcc = TestUtilities.createAccounts(1, true);
        testCon = TestUtilities.createContacts(testAcc, 1, true);
        testOpp = TestUtilities.createOpportunities(testAcc, 1, true);

        Apttus_Proposal__Proposal__c testProposal =
            new Apttus_Proposal__Proposal__c(Apttus_Proposal__Opportunity__c=testOpp[0].Id);
        insert testProposal;

        testBuildingGroup = new Building_Group__c(Opportunity__c=testOpp[0].Id);
        insert testBuildingGroup;

        Pricebook2 pb = new Pricebook2(
            Name = 'Standard Price Book',
            Description = 'Price Book Products',
            IsActive = true
        );
        insert pb;

        pList = new List<Product2>();
        pList.add(new Product2(Name='Membrane', Family='Best Practices', IsActive=true));
        pList.add(new Product2(Name='Warranty', Family='Best Practices', IsActive=true));
        pList.add(new Product2(Name='Warranty Extension', Family='Best Practices', IsActive=true));
        insert pList;

        List<PricebookEntry> peList = new List<PricebookEntry>();
        for(Product2 p :pList){
            peList.add(new PricebookEntry(
                Pricebook2Id=PRICEBOOK_ID,
                Product2Id=p.Id,
                UnitPrice=10000,
                IsActive=true,
                UseStandardPrice=false
            ));
        }

        for(Product2 p :pList){
            peList.add(new PricebookEntry(
                Pricebook2Id=pb.Id,
                Product2Id=p.Id,
                UnitPrice=10000,
                IsActive=true,
                UseStandardPrice=false
            ));
        }
        insert peList;

        Apttus_Config2__ClassificationName__c testCategory =
            new Apttus_Config2__ClassificationName__c(Name='TestCategory',
            Apttus_Config2__HierarchyLabel__c='TestLabel');
        insert testCategory;

        Apttus_Config2__ClassificationHierarchy__c testClassification =
            new Apttus_Config2__ClassificationHierarchy__c(Name='Warranty',
            Apttus_Config2__HierarchyId__c=testCategory.Id,
            Apttus_Config2__Label__c='TestLabel');
        insert testClassification;
        Apttus_Config2__ClassificationHierarchy__c testClassification1 =
            new Apttus_Config2__ClassificationHierarchy__c(Name='Warranty Extension',
            Apttus_Config2__HierarchyId__c=testCategory.Id,
            Apttus_Config2__Label__c='TestLabel');
        insert testClassification1;
        Apttus_Config2__ClassificationHierarchy__c testClassification2 =
            new Apttus_Config2__ClassificationHierarchy__c(Name='Membrane',
            Apttus_Config2__HierarchyId__c=testCategory.Id,
            Apttus_Config2__Label__c='TestLabel');
        insert testClassification2;

        List<Apttus_Config2__ProductClassification__c> pcList =
            new List<Apttus_Config2__ProductClassification__c>();
        for (Product2 p : pList) {
            pcList.add(new Apttus_Config2__ProductClassification__c(
                Apttus_Config2__ClassificationId__c = testClassification.Id,
                Apttus_Config2__ProductId__c = p.Id
            ));
        }
        insert pcList;                

        List<Apttus_Proposal__Proposal_Line_Item__c> ppList =
            new List<Apttus_Proposal__Proposal_Line_Item__c>();
        for(Product2 p :pList){
            Apttus_Proposal__Proposal_Line_Item__c testLineItem =
                new Apttus_Proposal__Proposal_Line_Item__c();
            testLineItem.Apttus_Proposal__Product__c = p.Id;
            testLineItem.Apttus_Proposal__Proposal__c = testProposal.Id;
            ppList.add(testLineItem);
        }
        insert ppList;

        List<Building_Group_Product__c> bgpList = new List<Building_Group_Product__c>();
        for (Integer i = 0; i < ppList.Size(); i++) {
            testBuildingGroupProduct = new Building_Group_Product__c();
            testBuildingGroupProduct.Building_Group__c = testBuildingGroup.Id;
            testBuildingGroupProduct.Proposal_Line_Item__c = ppList[i].Id;
            if (i == 0) {
                testBuildingGroupProduct.Category_Hierarchy__c = testClassification.Id;
            }
            else if (i == 1) {
                testBuildingGroupProduct.Category_Hierarchy__c = testClassification1.Id;
            }
            else if(i==2){
                testBuildingGroupProduct.Category_Hierarchy__c = testClassification2.Id;
            }
            testBuildingGroupProduct.Product_lookup__c = ppList[i].Apttus_Proposal__Product__c;
            bgpList.add(testBuildingGroupProduct);
        }
        insert bgpList;

        Test.setCurrentPageReference(new PageReference('/apex/NewOpportunityFirm')); 
        System.currentPageReference().getParameters().put('id', testOpp[0].Id);         
    }

    @IsTest
    static void TestBuildingGroupController_Constructor_ValidParameters() {
        setup();
        ApexPages.StandardController standardController = new ApexPages.StandardController(testBuildingGroup);
        BuildingGroupController controller 	= new BuildingGroupController(standardController);

        system.assertEquals(controller.buildingGroup.Id, testBuildingGroup.Id);
    }

    @IsTest
    static void TestBuildingGroupController_GetBuildingGroupProducts_ValidParameters() {
        setup();
        ApexPages.StandardController standardController = new ApexPages.StandardController(testBuildingGroup);
        BuildingGroupController controller 	= new BuildingGroupController(standardController);
        controller.GetBuildingGroupProducts();

      	system.assertEquals(1, controller.bgpList.size());        
    }

    @IsTest
    static void TestBuildingGroupController_NewLayer_ValidParameters() {
        setup();
        ApexPages.StandardController standardController = new ApexPages.StandardController(testBuildingGroup);
        BuildingGroupController controller 	= new BuildingGroupController(standardController);
        PageReference pageRef = controller.NewLayer();

      	System.assertNotEquals(null,pageRef);     
      	system.assertEquals('/apex/AddBuildingGroupProduct?id='+testBuildingGroup.Id, pageRef.getUrl());
    }

    @IsTest
    static void TestBuildingGroupController_Save_ValidParameters() {
        setup();
        ApexPages.StandardController standardController = new ApexPages.StandardController(testBuildingGroup);
        BuildingGroupController controller 	= new BuildingGroupController(standardController);
        controller.GetBuildingGroupProducts();
        controller.Save();

        List<ApexPages.Message> messages = ApexPages.getMessages();
        System.assertEquals('Saved!', messages.get(0).getDetail());
    }

    @IsTest
    static void TestBuildingGroupController_DeleteSelected_ValidParameters() {
        setup();
        ApexPages.StandardController standardController = new ApexPages.StandardController(testBuildingGroup);
        BuildingGroupController controller  = new BuildingGroupController(standardController);
        controller.GetBuildingGroupProducts();
        controller.selectedProductToDelete = testBuildingGroupProduct.Id;
        controller.DeleteSelected();

        List<ApexPages.Message> messages = ApexPages.getMessages();
        System.assert(messages.get(0).getDetail().startsWith('Deleted!'));
    }

    @IsTest
    static void TestBuildingGroupController_CloneSelected_ValidParameters() {
        setup();
        ApexPages.StandardController standardController = new ApexPages.StandardController(testBuildingGroup);
        BuildingGroupController controller  = new BuildingGroupController(standardController);
        controller.GetBuildingGroupProducts();
        controller.selectedProductToCloneIndex = 0;
        Integer listSize = controller.bgpList.size();
        controller.cloneSelectedProduct();

        System.assertEquals(listSize + 1, controller.bgpList.size());
    }
}