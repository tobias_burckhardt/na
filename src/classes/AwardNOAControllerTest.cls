@IsTest
private class AwardNOAControllerTest {

	@TestSetup
	static void setup() {
		Opportunity oppty = TestingUtils.createOpportunity('Parent', 'Prospective', Date.today(), true);
		Apttus_Proposal__Proposal__c proposal = TestingUtils.createApptusProposal(false);
		proposal.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
		insert proposal;
		Applicator_Quote__c quote = TestingUtils.createApplicatorQuote(false);
		quote.Quote_Apttus__c = proposal.Id;
		quote.Opportunity__c = oppty.Id;
		quote.Award_NOA__c = false;
		insert quote;
	}

    @IsTest
    static void testAwardNOAControllerShowsError() {
		List<Applicator_Quote__c> quotes = new List<Applicator_Quote__c>();
		quotes.add(new Applicator_Quote__c());
		quotes.add(new Applicator_Quote__c());

		ApexPages.StandardSetController stdController = new ApexPages.StandardSetController(quotes);
		stdController.setSelected(quotes);

		Test.startTest();
		new AwardNOAController(stdController);
		Test.stopTest();

		System.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR), 'The page should have an error message.');
    }

    @IsTest
    static void testAwardNOAControllerShowsNoError() {
		List<Applicator_Quote__c> quotes = [SELECT Id FROM Applicator_Quote__c LIMIT 1];

		ApexPages.StandardSetController stdController = new ApexPages.StandardSetController(quotes);
		stdController.setSelected(quotes);

		Test.startTest();
		new AwardNOAController(stdController);
		Test.stopTest();

		System.assert(!ApexPages.hasMessages(ApexPages.Severity.ERROR), 'The page should have an error message.');
    }

    @IsTest
    static void testAwardNOA() {
		List<Applicator_Quote__c> quotes = [SELECT Id FROM Applicator_Quote__c LIMIT 1];

		ApexPages.StandardSetController stdController = new ApexPages.StandardSetController(quotes);
		stdController.setSelected(quotes);
		AwardNOAController controller = new AwardNOAController(stdController);

		Test.startTest();
		PageReference ref = controller.awardNOA();
		Test.stopTest();

		Applicator_Quote__c quote = [
			SELECT Opportunity__c
			FROM Applicator_Quote__c
			WHERE Id = :quotes.get(0).Id
		];

		System.assert(ref.getUrl().contains('/apex/AwardQuote'),
			'It should redirect to the AwardQuote page.');
		System.assertEquals(quote.Opportunity__c, ref.getParameters().get('Id'),
			'It should have the right Id parameter.');
    }
}