public with sharing class AwardNOAController {

	private final Applicator_Quote__c quote;

    public AwardNOAController(ApexPages.StandardSetController stdController) {
        List<Applicator_Quote__c> quotes = (List<Applicator_Quote__c>) stdController.getSelected();

		if (quotes.isEmpty()) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select one quote to award NOA.'));
		} else if (quotes.size() > 1) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Only one quote can be selected to award NOA.'));
		} else {
			this.quote = [SELECT Id, Opportunity__c FROM Applicator_Quote__c WHERE Id = :quotes.get(0).Id];
		}
    }

	public PageReference awardNOA() {
        PageReference ref;
        if (this.quote != null) {
            ref = new PageReference('/apex/AwardQuote');
            ref.getParameters().put('Id', quote.Opportunity__c);
            ref.getParameters().put('qId', quote.Id);
        }
        return ref;
	}
}