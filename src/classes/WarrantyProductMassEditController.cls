public with sharing class WarrantyProductMassEditController {
    
    public string warrantyId {get; set;}
    public List<Warranty_Line__c> warrantyLineList {get; set;}
    
    public WarrantyProductMassEditController(){
        warrantyLineList = new List<Warranty_Line__c>();
        warrantyId = ApexPages.currentPage().getParameters().get('wid');
        if(warrantyId == null){
            return;
        }
        try{
        warrantyLineList = [select name, Warranty_Term_Years__c, Quantity__c,Product__r.name, Article_Cod__c, Sales_Unit_Of_Measure__c, Substrate_Area__c, Substrate_Unit_of_Measure__c, Product__r.Max_Warranty_Term__c from Warranty_Line__c where Warranty__r.id =: warrantyId];
        }catch(Exception e){system.debug('exception messages: ' + e.getMessage()); }
    }
    public Pagereference clearvalues(){
    warrantyLineList.clear();
    Pagereference pg=new Pagereference('/apex/CommunityWarrantyEdit?wid='+warrantyId);
     pg.setRedirect(true);
    return pg;
    }
    public PageReference saveWarrantyLines(){
        try{
            //validate against max warranty term for each line item
            string linesToChange = '';
            for(Warranty_Line__c wl : warrantyLineList){
                Integer selectedTerm = Integer.valueOf(wl.Warranty_Term_Years__c);
                if(selectedTerm > wl.Product__r.Max_Warranty_Term__c){
                    linesToChange += wl.Product__r.Name + ', ';
                }
            }
            
            if(linesToChange.length() > 0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The following lines have exceeded the maximum allowed warranty term: ' + linesToChange));
                return null;
            }
            update warrantyLineList;
            return new PageReference('/apex/CommunityWarrantyDetail?wid=' + warrantyId);
        }catch(Exception e){system.debug('exception messages: ' + e.getMessage()); return null; }
    }
}