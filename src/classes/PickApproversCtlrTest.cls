@isTest
private class PickApproversCtlrTest {
	static Request_for_Project_Approval__c testRPA;
	static PickApproversCtlr testC;
	static User inserter;

	static void setup() {
		testRPA = TestingUtils.createTestRequestForPA(Date.newInstance(2016, 3, 3), true);
		ApexPages.StandardController testSC = new ApexPages.StandardController(testRPA);
		testC = new PickApproversCtlr(testSC);
	}

	static void createSysAdminInserter() {
		inserter = TestingUtils.createTestUser('InserterTestPAM', 'System Administrator');
		insert inserter;		
	}
	
	static testMethod void testRemoveAndSave() {	
		createSysAdminInserter();
		List<User> testUsers = TestingUtils.createMultipleTestUsers(8, 'userTestPAM', 'System Administrator', true);
		System.runAs(inserter) {
			testRPA = TestingUtils.createTestRequestForPA(Date.newInstance(2016, 3, 3), true);
			testRPA.Approver_1__c = testUsers[0].Id;
			testRPA.Approver_2__c = testUsers[1].Id;
			testRPA.Approver_3__c = testUsers[2].Id;
			testRPA.Approver_4__c = testUsers[3].Id;
			testRPA.Approver_5__c = testUsers[4].Id;
			testRPA.Approver_6__c = testUsers[5].Id;
			testRPA.Approver_7__c = testUsers[6].Id;
			testRPA.Approver_8__c = testUsers[7].Id;
			update testRPA;
			ApexPages.StandardController testSC = new ApexPages.StandardController(testRPA);
			testC = new PickApproversCtlr(testSC);

			Test.startTest();
			testC.remove(4);
			testC.savePage();
			Test.stopTest();
		}
		System.assert(testC.errorMessages == null || testC.errorMessages.isEmpty(), 'There should be no errors in this test. Found error: ' + testC.errorMessages);
	}
	
	static testMethod void testSaveAndHandle_NoError() {	
		createSysAdminInserter();	
		List<User> testUsers3 = TestingUtils.createMultipleTestUsers(3, 'userTestPAM', 'System Administrator', true);
		Map<Integer, Id> testInput = generateInputMap(testUsers3, new List<Integer>{0,3,4});

		System.runAs(inserter) {
			setup(); 

			Test.startTest();
			testC.saveAndHandleDMLError(testInput);
			Test.stopTest();
		}

		System.assert(testC.errorMessages == null || testC.errorMessages.isEmpty(), 'There should be no errors in this test. Found error: ' + testC.errorMessages);

		Request_for_Project_Approval__c updatedRPA = 
			[SELECT Id, Approver_1__c, Approver_2__c, Approver_3__c, Approver_4__c, Approver_5__c, Approver_6__c, Approver_7__c, Approver_8__c 
			FROM Request_for_Project_Approval__c WHERE Id = :testRPA.Id];

		System.assertEquals(testInput.get(0), updatedRPA.Approver_1__c, 'Given the input map, save function should populate the right Approver field');
		System.assertEquals(testInput.get(3), updatedRPA.Approver_4__c, 'Given the input map, save function should populate the right Approver field');
		System.assertEquals(testInput.get(4), updatedRPA.Approver_5__c, 'Given the input map, save function should populate the right Approver field');

		System.assert(String.isBlank(updatedRPA.Approver_2__c), 'Given the input map, save function should empty out the right Approver field');
		System.assert(String.isBlank(updatedRPA.Approver_3__c), 'Given the input map, save function should empty out the right Approver field');
		System.assert(String.isBlank(updatedRPA.Approver_6__c), 'Given the input map, save function should empty out the right Approver field');
		System.assert(String.isBlank(updatedRPA.Approver_7__c), 'Given the input map, save function should empty out the right Approver field');
		System.assert(String.isBlank(updatedRPA.Approver_8__c), 'Given the input map, save function should empty out the right Approver field');
	}
	
	static testMethod void testSaveAndHandle_DataError() {
		setup();
		
		Test.startTest();
		testC.saveAndHandleDataError(new Set<PickApproversModel.ErrorCode>{PickApproversModel.ErrorCode.DATA_ERROR});
		Test.stopTest();

		System.assertEquals(1, testC.errorMessages.size(), 'There should be an error message');
		System.assertEquals(testC.DATA_ERR_MSG, testC.errorMessages[0], 'Error message should be the model error message');
	}
		
	static testMethod void testSaveAndHandle_DupNameError() {
		setup();
		
		Test.startTest();
		testC.saveAndHandleDataError(new Set<PickApproversModel.ErrorCode>{PickApproversModel.ErrorCode.DUPLICATE_NAME_ERROR});
		Test.stopTest();

		System.assertEquals(1, testC.errorMessages.size(), 'There should be an error message');
		System.assertEquals(testC.DUPL_NAME_ERR_MSG, testC.errorMessages[0], 'Error message should be the duplicate number message');
	}

	static testMethod void testSaveAndHandle_DupNumError() {
		setup();
		
		Test.startTest();
		testC.saveAndHandleDataError(new Set<PickApproversModel.ErrorCode>{PickApproversModel.ErrorCode.DUPLICATE_NUM_ERROR});
		Test.stopTest();

		System.assertEquals(1, testC.errorMessages.size(), 'There should be an error message');
		System.assertEquals(testC.DUPL_NUM_ERR_MSG, testC.errorMessages[0], 'Error message should be the duplicate number message');
	}
	
	static testMethod void testSaveAndHandle_DMLError() {
		setup();
		Map<Integer, Id> testInput = new Map<Integer, Id>();
		
		Test.startTest();
		testC.mysObject.Date__c = null; // Required
		testC.saveAndHandleDMLError(testInput);
		Test.stopTest();

		System.assertEquals(1, testC.errorMessages.size(), 'There should be an error message');
		System.assert(testC.errorMessages[0].contains('REQUIRED_FIELD_MISSING'), 'There should be a DML error message');
	}

	static testMethod void testDisplayMessage() {
		setup();
		Map<Integer, Id> testInput = new Map<Integer, Id>();
		
		Test.startTest();
			testC.errorMessages = new List<String>{'err'};
			testC.displayMessages();
		Test.stopTest();

		System.assertEquals(null, testC.errorMessages, 'After display a message, the list of errors should be reset since the errors only apply once');
		System.assert(ApexPages.hasMessages(), 'The message should be put on the page');
	}

	static testMethod void shortFlowTest() {
		createSysAdminInserter();

		List<User> testUsers3 = TestingUtils.createMultipleTestUsers(3, 'userTestPAM', 'System Administrator', true);
		Map<Integer, Id> testInput = generateInputMap(testUsers3, new List<Integer>{0,1,2});

		System.runAs(inserter) {
			testRPA = TestingUtils.createTestRequestForPA(Date.newInstance(2016, 3, 3), true);
			testRPA.Approver_1__c = testInput.get(0);
			update testRPA;

			ApexPages.StandardController testSC = new ApexPages.StandardController(testRPA);
			testC = new PickApproversCtlr(testSC);

			Test.startTest();
			testC.add();
			testC.paModel.approvers[1].placeholderRequest.Approver_1__c = testInput.get(1);
			testC.add();
			testC.paModel.approvers[2].placeholderRequest.Approver_1__c = testInput.get(2);
			testC.remove(1);
			testC.savePage();
			Test.stopTest();		
		}

		System.assert(!ApexPages.hasMessages(), 'There should be no errors in this test. Found error: ' + ApexPages.getMessages());

		Request_for_Project_Approval__c updatedRPA = 
			[SELECT Id, Approver_1__c, Approver_2__c, Approver_3__c, Approver_4__c, Approver_5__c, Approver_6__c, Approver_7__c, Approver_8__c 
			FROM Request_for_Project_Approval__c WHERE Id = :testRPA.Id];

		System.assertEquals(testInput.get(0), updatedRPA.Approver_1__c, 'The existing approver should be untouched');
		System.assertEquals(testInput.get(2), updatedRPA.Approver_3__c, 'The new approver 2 should be saved');
	}

	static testMethod void removePublicTest() {
		createSysAdminInserter();

		List<User> testUsers3 = TestingUtils.createMultipleTestUsers(3, 'userTestPAM', 'System Administrator', true);
		Map<Integer, Id> testInput = generateInputMap(testUsers3, new List<Integer>{0,1,2});

		System.runAs(inserter) {
			testRPA = TestingUtils.createTestRequestForPA(Date.newInstance(2016, 3, 3), true);
			testRPA.Approver_1__c = testInput.get(0);
			update testRPA;

			ApexPages.StandardController testSC = new ApexPages.StandardController(testRPA);
			testC = new PickApproversCtlr(testSC);

			Test.setCurrentPage(Page.PickApprovers);
			Test.startTest();
			ApexPages.currentPage().getParameters().put('removalNumber', '0');
			testC.removeRow();
			Test.stopTest();		
		}

		System.assert(ApexPages.hasMessages(), 'There should be an info message on the page about the removal.');
	}

	static testMethod void updateStatusCoverage() {
		setup();

		Test.startTest();
		testC.updateState();
		Test.stopTest();
	}

	static Map<Integer, Id> generateInputMap(List<User> userList, List<Integer> approverPos) {
		System.assertEquals(userList.size(), approverPos.size());
		Map<Integer, Id> inputMap = new Map<Integer, Id>();
		for (Integer i=0; i<userList.size(); i++) {
			inputMap.put(approverPos[i], userList[i].Id);
		}
		return inputMap;
	}
}