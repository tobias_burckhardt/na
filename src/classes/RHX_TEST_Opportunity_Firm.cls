@isTest(SeeAllData=true)
public class RHX_TEST_Opportunity_Firm {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM Opportunity_Firm__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new Opportunity_Firm__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}