public with sharing class CommunityMyWarrantiesController {
    
    public List<Warranty__c> warrList {get; set;}
    
    public CommunityMyWarrantiesController(){
        string userId = UserInfo.getUserId();
        
        //get all warranties for this user
        string warrFieldList = Utilities.GetAllObjectFields('Warranty__c');
        //append relational project fields to field list
        warrFieldList += ',Project__r.Name,Project__r.Id';
        string queryFormat = 'select {0} from Warranty__c where Community_User_ID__c = :userId';
        string warrQuery = String.format(queryFormat, new List<string>{warrFieldList});
        
        try{
            warrList = Database.query(warrQuery);
        }
        catch(Exception e){
            system.debug(e.getMessage());
            warrList = new List<Warranty__c>();
        }
    }
    
    public void deleteWarranties(){
            string index = ApexPages.currentPage().getParameters().get('WarrNum');
            Warranty__c WartoDelete = warrList.get(integer.valueof(index));
            delete WartoDelete;
           warrList.remove(integer.valueof(index));
        }

}