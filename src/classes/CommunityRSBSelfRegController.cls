public without sharing class CommunityRSBSelfRegController {
    public String firstName {get; set;}
    public String lastName {get; set;}
    public String companyName {get; set;}
    public String companyType {get; set;}
    public String jobTitle {get; set;}
    public String emailAddress {get; set;}
    public String country {get; set;}
    public String phoneNo {get; set;}
    public String streetAddress {get; set;}
    public String city {get; set;}
    public String state {get; set;}
    public String postalCode {get; set;}
    public String salesRep {get; set;}

    public Boolean hasErrors {get; private set;}
    public Boolean firstNameError {get; private set;}
    public Boolean lastNameError {get; private set;}
    public Boolean companyNameError {get; private set;}
    public Boolean emailIsInvalid {get; private set;}
    public Boolean emailIsBlank {get; private set;}
    public Boolean jobTitleError {get; private set;}
    public Boolean streetAddressError {get; private set;}
    public Boolean cityError {get; private set;}
    public Boolean stateError {get; private set;}
    public Boolean countryError {get; private set;}
    public Boolean postalCodeError {get; private set;}
    public Boolean duplicateUserFound {get; set;}

    public List<String> companyTypeList = new List<String>{
            'Architect',
            'Certified Applicator',
            'Distributor',
            'General Contractor',
            'Owner',
            'Other'
    };
    public List<SelectOption> getCompanyTypes(){
        List<SelectOption> theSelects = new List<SelectOption>();

        theSelects.add(new SelectOption('', '--Company Type--'));
        for(String s : companyTypeList){
            theSelects.add(new SelectOption(s, s));
        }
        return theSelects;

    }

    public Boolean validateForm(){
        // initialize all error Booleans to false. (Ensures we don't reference any null objects, or get tricked into thinking "not false" equals "true"!)
        hasErrors = false;
        firstNameError = false;
        lastNameError = false;
        emailIsBlank = false;
        emailIsInvalid = false;
        companyNameError = false;
        jobTitleError = false;
        streetAddressError = false;
        cityError = false;
        stateError = false;
        postalCodeError = false;
        countryError = false;

        if(String.isBlank(firstName)){
            hasErrors = true;
            firstNameError = true;
        }

        if(String.isBlank(lastName)){
            hasErrors = true;
            lastNameError = true;
        }
        if(String.isBlank(emailAddress)){
            hasErrors = true;
            emailIsBlank = true;
        } else {
            emailIsBlank = false;
            if(WaranttyValidationHelper.validateEmail(emailAddress) == false){
                hasErrors = true;
                emailIsInvalid = true;
            }
        }
        if(String.isBlank(companyName)){
            hasErrors = true;
            companyNameError = true;
        }
        if(String.isBlank(jobTitle)){
            hasErrors = true;
            jobTitleError = true;
        }
        if(String.isBlank(streetAddress)){
            hasErrors = true;
            streetAddressError = true;
        }
        if(String.isBlank(city)){
            hasErrors = true;
            cityError = true;
        }
        if(String.isBlank(country)){
            hasErrors = true;
            countryError = true;
        }
        if(String.isBlank(state)){
            hasErrors = true;
            stateError = true;
        }
        if(String.isBlank(String.valueOf(postalCode))){
            hasErrors = true;
            postalCodeError = true;
        }
        if(hasErrors){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill in all of the fields highlighted below.'));
            if(emailIsInvalid == true){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'A valid email address is required.'));
            }
            return false;
        }
        return true;
    }

    public PageReference signUp() {
        if (validateForm()) {
            Savepoint sp = Database.setSavepoint();
            try {
                List<User> existingUsers = [SELECT Id, Username FROM User WHERE Email = :emailAddress AND IsActive = TRUE LIMIT 1];
                if (existingUsers.size() > 0) {
                    duplicateUserFound = true;
                    return null;
                }

                List<Account> existingAccounts = [SELECT Id FROM Account WHERE Name = :companyName AND BillingCity = :city AND BillingState = :state AND BillingPostalCode = :postalCode];
                Account myAcc;
                if (existingAccounts.size() != 1) {
                    Warranty_Community_Setting__mdt setting = [SELECT Warranty_Self_Reg_Owner_UserName__c FROM Warranty_Community_Setting__mdt WHERE DeveloperName = 'Default' LIMIT 1];
                    String warrantySelfRegOwner = setting.Warranty_Self_Reg_Owner_UserName__c;
                    if ([SELECT IsSandbox FROM Organization].IsSandbox) {
                        warrantySelfRegOwner = warrantySelfRegOwner + '.' + UserInfo.getUserName().substringAfterLast('.');
                    }
                    User accOwner = [SELECT Id FROM User WHERE Username = :warrantySelfRegOwner];
                    RecordType marketingRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Marketing'];
                    myAcc = new Account(Name = companyName, RecordTypeId = marketingRecordType.Id, Pardot_Share__c = true, Type = companyType, BillingStreet = streetAddress,
                            OwnerId = accOwner.Id, BillingCity = city, BillingState = state, BillingPostalCode = postalCode, BillingCountry = country);
                } else {
                    myAcc = new Account(Id = existingAccounts.get(0).Id, Pardot_Share__c = true, Type = companyType, BillingCountry = country);
                }

                List<Contact> existingContacts = [SELECT Id, AccountId FROM Contact WHERE FirstName = :firstName AND LastName = :lastName AND Email = :emailAddress ORDER BY LastModifiedDate DESC];
                Contact myContact;
                if (existingContacts.size() > 0) {
                    myContact = new Contact(Id = existingContacts.get(0).Id, AccountId = existingContacts.get(0).AccountId, Title = jobTitle, Phone = phoneNo);
                } else {
                    myContact = new Contact(FirstName = firstName, LastName = lastName, Email = emailAddress, Title = jobTitle, Phone = phoneNo);
                }

                upsert myAcc;
                myContact.AccountId = myAcc.Id;
                upsert myContact;

                Profile warrantyCommunityProfile = [SELECT Id FROM Profile WHERE Name = 'Warranty Customer Community Login' LIMIT 1];
                User newUser = new User(FirstName = firstName, LastName = lastName, Email = emailAddress, Target_Market__c = 'RSB Commercial', ContactId = myContact.Id,
                        CommunityNickname = firstName.substring(0, 1) + lastName, ProfileId = warrantyCommunityProfile.Id, Username = emailAddress);
                Id userId = createExternalUser(newUser, myAcc.Id, 1, 1);

                PageReference loginPage = Page.CommunityRSBSelfRegistrationSuccess;
                loginPage.setRedirect(true);
                return loginPage;
            } catch (Exception e) {
                Database.rollback(sp);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An exception occurred: ' + e.getMessage()));
                return null;
            }
        }
        return null;
    }

    private Id createExternalUser(User newUser, Id accountId, Integer nickNameStack, Integer usernameStack) {
        try {
            return Site.createExternalUser(newUser, accountId);
        } catch (Exception e) {
            if (e.getMessage().contains('That nickname already exists')) {
                newUser.CommunityNickname = newUser.firstName.substring(0, 1) + newUser.lastName + nickNameStack;
                return createExternalUser(newUser, accountId, nickNameStack + 1, usernameStack);
            } else if (e.getMessage().contains('User already exists')) {
                newUser.Username = newUser.Username + usernameStack;
                return createExternalUser(newUser, accountId, nickNameStack, usernameStack + 1);
            }
            throw e;
        }
    }
}