public class APTS_ProposalTriggerExecutor {
	
      /**
     *  method to check whether the opportunity related to the proposal hdoes not have any other proposals linked to it
     */
     
             public void ValidateProposalOpportunity(Map<Id, Apttus_Proposal__Proposal__c> newProposalMap) {
        
        for(String key : newProposalMap.keySet()) {
            if(newProposalMap.get(key).Apttus_Proposal__Opportunity__c != null)
            {
                List<Apttus_Proposal__Proposal__c> oppRelatedProposals = [select id from Apttus_Proposal__Proposal__c where Apttus_Proposal__Opportunity__c = :newProposalMap.get(key).Apttus_Proposal__Opportunity__c and id <> :newProposalMap.get(key).Id];
              system.debug(oppRelatedProposals);
                if(oppRelatedProposals != null && oppRelatedProposals.size() >= 1){
                    newProposalMap.get(key).Apttus_Proposal__Opportunity__c.addError('Opportunity already has a proposal associated to it.');
                              return;
                }
            }
        }
    }


    
}