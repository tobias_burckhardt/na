public without sharing class sika_SCCZoneAdmixturePricePageController {
	public Id userID {get; set;}
	public Id materialID {get; set;}

	public Material__c theMaterial {get; private set;}
	public list<Mix_Material__c> theMixMaterials {get; private set;}
	public list<Admixture_Price__c> theAdmixturePrices {get; set;}
	public Admixture_Price__c theAdmixturePrice {get; set;}

	public Boolean renderPDS {get; set;}
	public Boolean renderSDS {get; set;}

	public Boolean hasErrors {get; set;}

	public sika_SCCZoneAdmixturePricePageController() {
		sika_SCCZoneCheckAuth.setAuthStatus(UserInfo.getProfileId());

		userID = UserInfo.getUserId();
		materialID = Apexpages.currentPage().getParameters().get('id');	

		theMaterial = [SELECT Id, 
							  Unit_System__c, 
							  Material_Name__c, 
							  Type__c, 
							  Source__c, 
							  Description__c,
							  PDS_Link__c, 
							  SDS_Link__c,
							  isActive__c 
						 FROM Material__c 
						WHERE Type__c = 'Admixture'
						  AND Id = :materialID LIMIT 1];
		
		// used to populate the "mixes where this material is used" table on the page
		theMixMaterials = [SELECT Id, 
                                  Mix__r.Id,
                                  Mix__r.Mix_Name__c, 
                                  Mix__r.Contractor__c, 
                                  Mix__r.Project__c,
                                  Mix__r.OwnerID,
                                  Mix__r.CreatedDate,
                                  Material__r.Id
                             FROM Mix_Material__c 
                            WHERE Material__r.Id = :materialID 
                              AND Mix__r.OwnerID = :userId];

		theAdmixturePrices = [Select Id, 
									OwnerId, 
									Admixture_Material__c, 
									Price__c,
									Unit__c
							   FROM Admixture_Price__c 
							  WHERE OwnerId = :userId 
							    AND Admixture_Material__c = :materialID];

		if(theAdmixturePrices.size() == 0){
			theAdmixturePrice = new Admixture_Price__c(Admixture_Material__c = materialID);
		} else if(theAdmixturePrices.size() == 1){
			theAdmixturePrice = theAdmixturePrices[0];
		} else {
			system.debug('********ERROR: More than one Admixture_Price record was returned.');
		}

		renderPDS = theMaterial.PDS_Link__c == '' ? false : true;
		renderSDS = theMaterial.SDS_Link__c == null ? false : true;
		
	}

	// check user auth status
    public PageReference loadAction(){
        PageReference authCheck;

        // because this controller runs "without sharing" we need to manually confirm that the user should have access. (A user whose session has timed-out, or an otherwise unauthenticated user who accessed this page directly, would be able to view the page otherwise, albeit without any data displayed.)
        authCheck = sika_SCCZoneCheckAuth.doAuthCheck();
        if(authCheck != null){
            return authCheck;
        }
        return null;
    }
    

	public PageReference doSave(){
		if(validate()){
			upsert(theAdmixturePrice);

			PageReference pageRef = new PageReference('/SCCZone/apex/sika_SCCZoneMaterialDetailPage?id=' + materialID);
			pageRef.setRedirect(true);
			return pageRef;
		} 

		return null;
	}

	public PageReference doCancel(){
		PageReference pageRef = new PageReference('/SCCZone/apex/sika_SCCZoneMaterialDetailPage?id=' + materialID);
		pageRef.setRedirect(true);
		return pageRef;
	}


	public Boolean priceIsInvalid {get; set;}
	public Boolean unitIsBlank {get; set;}

    @testVisible private Boolean validate(){
    	String errorString;

    	priceIsInvalid = false;
    	unitIsBlank = false;
    	hasErrors = false;

    	if(!sika_SCCZoneValidationHelper.validatePriceField(String.valueOf(theAdmixturePrice.Price__c))){
    		priceIsInvalid = true;
    		hasErrors = true;
    		errorString = 'Please enter a valid price. (Exclude the currency symbol.)';
    	}
    	if(String.isBlank(String.valueOf(theAdmixturePrice.Unit__c))){
    		unitIsBlank = true;
    		hasErrors = true;
    		errorString = 'Please select a unit.';
    	}
    	if(priceIsInvalid || unitIsBlank){
    		ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, errorString));
    		return false;
    	}
    	return true;
    }

}