public with sharing class CommunityForgotPasswordController {

    public String userName {get; set;}
    public String success {get; set;}
    public Boolean submitted {get; set;}
    public String boxText {get; set;}
    public Boolean displayPopup {get; set;}
    public String caseEmail{get; set;}
    public String caseDesc {get; set;}

    public Boolean hasError {get; private set;}
    public String errorText {get; private set;}
    
    @TestVisible private static String usernameNotRecognizedErrorMsg = 'The username you entered was not recognized. Please try again.';
    @TestVisible private static String invalidEmailErrorMsg = 'The username you entered was not recognized. Please try again.)';
    private static final List<String> PROFILES = new List<String>{'Warranty Community User', 'Warranty Customer Community Login'};

    public CommunityForgotPasswordController() {
        success = ApexPages.currentPage().getParameters().get('success');
        displayPopup = false;

        if(success != null && Boolean.valueOf(success) == true){
            submitted = true;
            boxText = 'Thank you! You should receive an email with a link to reset your password shortly.';
        } else {
            submitted = false;
            boxText = 'Please enter your user name below:';
        }

    }

    public PageReference submitRequest(){
        if(validateForm()){
            Site.forgotPassword(userName);
            
            PageReference thePage = new PageReference(ApexPages.currentPage().getUrl());
            thePage.getParameters().put('success', 'true');
            thePage.setRedirect(true);
            return thePage;
        }
        return null;
    }
    
    public void closePopup() {        
       displayPopup = false;    
    } 
    
    public void showPopup() {        
        displayPopup = true;    
    }

    public void submitCase(){
        Case theCase = new Case();
        theCase.Description = caseDesc;
        theCase.Status = 'New';
        theCase.Due_Date__c = Datetime.now();
        theCase.Origin = 'Web';
        theCase.Type = 'Website Help';
        theCase.Priority = 'Medium';
        theCase.Origin = ' Website';
        theCase.Subject = 'Website Help' + ' ticket created by ' + caseEmail;
        theCase.Customer_Email__c = caseEmail;
        theCase.Do_not_send_auto_response__c = true;

        List<Contact> u = [SELECT ID, Name, Email FROM Contact WHERE Email =: caseEmail LIMIT 1];
        if(u.size() == 1){
            theCase.ContactID = u[0].ID;
        }

        Database.DMLOptions options = new Database.DMLOptions();
        options.assignmentRuleHeader.useDefaultRule = true;
        theCase.setOptions(options);

        try {
            insert(theCase);
        } catch (DmlException e) {
            System.debug('******** ERROR submitting case: ' + e);
        }
        displayPopup = false;   
    }

    public Boolean validateForm(){
        hasError = false;
        errorText = '';
        
        if(!WaranttyValidationHelper.validateEmail(userName) || userName == null){
            hasError = true;
            errorText = invalidEmailErrorMsg;
            return false;
        }
        
        List<Profile> warrantyCommProfiles = [SELECT Id FROM Profile WHERE Name IN :PROFILES];
        
        Integer u = [SELECT COUNT() FROM User WHERE Username = :userName AND ProfileId IN :warrantyCommProfiles];
        if(u == 0) {
            hasError = true;
            errorText = usernameNotRecognizedErrorMsg;
            return false;
        }
        return true;
    } 
}