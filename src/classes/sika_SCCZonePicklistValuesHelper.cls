public without sharing class sika_SCCZonePicklistValuesHelper {
	
	// Method for getting picklist values from any picklist field on any object
    public static list<SelectOption> getPicklistValues(SObject obj, String field){
        list<SelectOption> options = new list<SelectOption>();
  
        // Get the object type of the SObject
        Schema.sObjectType objType = obj.getSObjectType(); 
  
        // Describe the SObject using its object type
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
  
        // Get a map of fields for the SObject
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
  
        // Get the list of picklist values for the requested field
        list<Schema.PicklistEntry> values = fieldMap.get(field).getDescribe().getPickListValues();
      
        // Add these values to the selectoption list
        for (Schema.PicklistEntry a : values) { 
         options.add(new SelectOption(a.getLabel(), a.getValue())); 
        }
        if(!options.isEmpty()){
            return options;
        } else {
            return new list<SelectOption>();
        }
    }
}