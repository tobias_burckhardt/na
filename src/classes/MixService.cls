public class MixService {
    public static void updateMixMaterialMoistureAdjustment(List<Mix__c> newMixes, Map<Id, Mix__c> oldMixesById) {
        List<Mix__c> filteredMixes = filterUpdateMoistureAdjustment(newMixes, oldMixesById);

        List<Mix_Material__c> mixMaterials = getRelatedMixMaterials(filteredMixes);
        Map<Id, Material__c> materialsById = new Map<Id, Material__c>(MixMaterialService.getRelatedMaterials(mixMaterials));
        Map<Id, Mix__c> mixesById = new Map<Id, Mix__c>(filteredMixes);

        List<Mix_Material__c> mixMaterialsToUpdate = new List<Mix_Material__c>();
        for (Mix_Material__c mixMaterial : mixMaterials) {
            Material__c material = materialsById.get(mixMaterial.Material__c);
            Mix__c mix = mixesById.get(mixMaterial.Mix__c);
            if (material != null && mix != null) {
                MixMaterialService.updateMoistureAdjustment(mixMaterial, material, mix);
                mixMaterialsToUpdate.add(mixMaterial);
            }
        }

        updateRelatedMixMaterials(mixMaterialsToUpdate, mixesById);
    }

    private static void updateRelatedMixMaterials(List<Mix_Material__c> mixMaterials, Map<Id, Mix__c> mixesById) {
        if (!mixMaterials.isEmpty()) {
            try {
                update mixMaterials;
            } catch(DmlException e) {
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    String errorMessage = 'Unable to update Moisture Adjustment of related Mix Material records: ' +
                        e.getDmlMessage(i);
                    mixesById.get(mixMaterials.get(e.getDmlIndex(i)).Mix__c).addError(errorMessage);
                }
            }
        }
    }

    private static List<Mix__c> filterUpdateMoistureAdjustment(List<Mix__c> newMixes, Map<Id, Mix__c> oldMixesById) {
        List<Mix__c> filteredMixes = new List<Mix__c>();
        for (Mix__c newMix : newMixes) {
            Mix__c oldMix = oldMixesById.get(newMix.Id);
            if (newMix.Water_Adjustment_kg__c != oldMix.Water_Adjustment_kg__c) {
                filteredMixes.add(newMix);
            }
        }
        return filteredMixes;
    }

    private static List<Mix_Material__c> getRelatedMixMaterials(List<Mix__c> mixes) {
        return [
            SELECT Quanity_kg__c, Moisture__c, Material__c, Mix__c
            FROM Mix_Material__c
            WHERE Mix__c IN :mixes
        ];
    }
}