@IsTest
public class ContentServiceTest {

    @testSetup
    static void setup(){
        AWS_Settings__c awsSetting = new AWS_Settings__c(AWS_Key__c = 'Test', S3_Bucket_Name__c = 'Test', SF_Library_Name__c = 'Test');
        insert awsSetting;

        ContentVersion contentVersion = new ContentVersion(
            Title = 'Test Document',
            VersionData = Blob.valueOf('Test Content'),
            PathOnClient= 'Test',
            Document_Type__c = 'Specifications',
            Sub_Type_Level_1__c = 'CSI Certified',
            IsMajorVersion = true
        );
        insert contentVersion;
    }
     
    @IsTest
    static void test_createTree() {

        List<Node> tree = ContentService.createTree();

        System.assert(tree.size() > 0);

    }

    @IsTest
    static void test_getMetadata(){

        List<ContentVersion> docs = ContentService.getDocumentsMetadata('/Specifications/CSI Certified');

        system.assert(docs.size() == 1);
        system.assertEquals(docs[0].Title, 'Test Document');
    }
    
    @isTest 
    static void test_searchDocument(){

        List<ContentVersion> docs = ContentService.getDocumentsByNameLike('Test');

        system.assert(docs.size() == 1);
        system.assertEquals(docs[0].Title, 'Test Document');

    }

}