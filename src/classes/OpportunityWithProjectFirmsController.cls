public class OpportunityWithProjectFirmsController
{
    private Opportunity o;
    public List<ProjectFirm__c> projectFirms {get;private set;}
    
    public OpportunityWithProjectFirmsController(ApexPages.StandardController controller) 
    {
        this.o = (Opportunity)controller.getRecord();
        
        Opportunity opps = [Select id, Project__c FROM Opportunity where id =: o.id];
        
        if (opps.Project__c == null)
        {
        return;
        }
        
        if (opps.Project__c != null)
        {
        projectFirms = [SELECT Name, Account__c, Account__r.Name, Account_City__c, Account_State__c, Contact_Phone__c, Contact__r.Name, Project_Role__c, Comments__c
                        FROM ProjectFirm__c
                        WHERE Project__c =: opps.Project__c
                        ];
        }
        
        if (projectFirms.size() == 0)
        {
        projectFirms = null;
        }
    }
}