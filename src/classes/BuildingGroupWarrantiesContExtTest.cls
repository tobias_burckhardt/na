/**
* @author Pavel Halas
* @company Bluewolf, an IBM Company
* @date 6/2016
*
* Test for BuildingGroupWarrantiesContExt.
*/
@IsTest
class BuildingGroupWarrantiesContExtTest {

    @TestSetup
    static void setup() {
        Account account = (Account) SObjectFactory.create(Account.SObjectType);
        Contact contact = (Contact) SObjectFactory.create(Contact.SObjectType, new Map<SObjectField, Object>{
                Contact.FirstName => 'First',
                Contact.LastName => 'Last',
                Contact.Email => 'someEmail@email.com',
                Contact.AccountId => account.Id
        });
        Opportunity opportunity = (Opportunity) SObjectFactory.create(Opportunity.SObjectType, new Map<SObjectField, Object>{
                Opportunity.AccountId => account.Id,
                Opportunity.Amount => 5,
                Opportunity.Roofing_Area_SqFt__c => 5,
                Opportunity.Estimated_Ship_Date__c => Date.today(),
                Opportunity.Roofing_Primary_Contact__c => contact.Id
        });
        Id roofingRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Roofing' AND SobjectType = 'Warranty__c'].Id;
        SObjectFactory.create(Warranty__c.SObjectType, new Map<SObjectField, Object> {
                Warranty__c.RecordTypeId => roofingRecordTypeId
        });
    }
	
	/**
	 * No warranty object is created when there is no building group without a warranty issued.
	 */
	@IsTest
	static void testContructor_NoWarrantyCreated() {
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
        Warranty__c warranty = [SELECT Id FROM Warranty__c LIMIT 1];

        SObjectFactory.create(2, Building_Group__c.SObjectType, new Map<SObjectField, Object> {
                Building_Group__c.Building_Group_name__c => 'Test',
                Building_Group__c.Opportunity__c => opportunity.Id,
                Building_Group__c.Warranty__c => warranty.Id
        });
		
		Test.startTest();
		BuildingGroupWarrantiesContExt ext = new BuildingGroupWarrantiesContExt(new ApexPages.StandardController(opportunity));
		Test.stopTest();

		System.assert(ext.warranty == null);
	}

	/**
	 * If there is at least one building group without a warranty issued, warranty object is created.
	 */
	@IsTest
	static void testConstructor_WarrantyCreated() {
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
        Warranty__c warranty = [SELECT Id FROM Warranty__c LIMIT 1];

        SObjectFactory.create(Building_Group__c.SObjectType, new Map<SObjectField, Object> {
                Building_Group__c.Building_Group_name__c => 'Test',
                Building_Group__c.Opportunity__c => opportunity.Id,
                Building_Group__c.Warranty__c => warranty.Id
        });
        SObjectFactory.create(2, Building_Group__c.SObjectType, new Map<SObjectField, Object> {
                Building_Group__c.Building_Group_name__c => 'Test',
                Building_Group__c.Opportunity__c => opportunity.Id
        });
        
		Test.startTest();
		
		BuildingGroupWarrantiesContExt ext = new BuildingGroupWarrantiesContExt(new ApexPages.StandardController(opportunity));
		List<BuildingGroupService.BuildingGroupWrapper> wrappers = ext.getBuildingGroupWrappers();
		
		Test.stopTest();
		
		System.assert(ext.warranty != null);
		
		System.assertEquals(3, wrappers.size());
		System.assertEquals(true, wrappers[0].warrantyExists);
		System.assertEquals(false, wrappers[1].warrantyExists);
		System.assertEquals(false, wrappers[2].warrantyExists);
	}
	
	/**
	 * Building groups marked to issue a warranty will have both reference the newly created warranty object.
	 */
	@IsTest
	static void testConstructor_Save() {
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
        Warranty__c warranty = [SELECT Id FROM Warranty__c LIMIT 1];

        SObjectFactory.create(Building_Group__c.SObjectType, new Map<SObjectField, Object> {
                Building_Group__c.Building_Group_name__c => 'Test',
                Building_Group__c.Opportunity__c => opportunity.Id,
                Building_Group__c.Warranty__c => warranty.Id
        });
        List<Building_Group__c> buildingGroups = (List<Building_Group__c>) SObjectFactory.create(2, Building_Group__c.SObjectType, new Map<SObjectField, Object> {
                Building_Group__c.Building_Group_name__c => 'Test',
                Building_Group__c.Opportunity__c => opportunity.Id
        });

		Test.startTest();
		
		BuildingGroupWarrantiesContExt ext = new BuildingGroupWarrantiesContExt(new ApexPages.StandardController(opportunity));
		ext.getBuildingGroupWrappers()[1].issueWarranty = true;
		ext.getBuildingGroupWrappers()[2].issueWarranty = true;
		ext.save();
		
		Test.stopTest();

		System.assert(ext.warranty != null);
		System.assert(ext.warranty.Id != null);

        Building_Group__c bg1 = [SELECT Id, Building_Group_Name__C, Warranty__c FROM Building_Group__c WHERE Id = :buildingGroups.get(0).Id LIMIT 1];
        Building_Group__c bg2 = [SELECT Id, Building_Group_Name__C, Warranty__c FROM Building_Group__c WHERE Id = :buildingGroups.get(1).Id LIMIT 1];
		System.assertEquals(ext.warranty.Id, bg1.Warranty__c);
		System.assertEquals(ext.warranty.Id, bg2.Warranty__c);
	}

    /**
     * Building groups marked to issue a warranty will have both reference the newly created warranty object.
     */
    @IsTest
    static void testConstructor_NoValidBuildingGroups() {
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];

        SObjectFactory.create(Building_Group__c.SObjectType, new Map<SObjectField, Object> {
                Building_Group__c.Building_Group_name__c => 'Test',
                Building_Group__c.Opportunity__c => opportunity.Id
        });

        Test.startTest();
        BuildingGroupWarrantiesContExt ext = new BuildingGroupWarrantiesContExt(new ApexPages.StandardController(opportunity));
        ext.getBuildingGroupWrappers()[0].issueWarranty = false;
        ext.save();
        Test.stopTest();

        System.assert(ApexPages.getMessages().size() > 0, 'No valid building groups, so we should get error messages');
    }

	/**
	 * Testing rollback functionality
	 */
	@IsTest
	static void testConstructor_rollback() {
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];

		Building_Group__c buildingGroup1 = (Building_Group__c) SObjectFactory.create(Building_Group__c.SObjectType, new Map<SObjectField, Object> {
                Building_Group__c.Building_Group_name__c => 'Test',
                Building_Group__c.Opportunity__c => opportunity.Id
        });

		Test.startTest();
        System.Savepoint savepoint = Database.setSavepoint();
        Warranty__c warranty = (Warranty__c) SObjectFactory.create(Warranty__c.SObjectType);
        BuildingGroupWarrantiesContExt.rollbackRecords(savepoint, warranty, new List<Building_Group__c> {buildingGroup1});
		Test.stopTest();

        System.assertEquals(null, warranty.Id, 'Warranty Should not have an Id');
	}
}