@isTest
private class skia_SCCZoneCheckAuthTest {

	@isTest static void checkAuthTestHappy() {
		map<String, sObject> objectMap = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
		User sccUser = (User) objectMap.get('SCCZoneUser');

		sika_SCCZoneCheckAuth.setAuthStatus(sccUser.ProfileId);
		PageReference ref = sika_SCCZoneCheckAuth.doAuthCheck();

		system.assert(ref == null);

	}

	@isTest static void checkAuthTestSad() {
		User guestUser = sika_SCCZoneTestFactory.createGuestUser();
		insert(guestUser);

		sika_SCCZoneCheckAuth.setAuthStatus(guestUser.Id);
		PageReference ref = sika_SCCZoneCheckAuth.doAuthCheck();

 		system.assert(ref.getURL() == '/SCCZone/sika_SCCZoneLoginPage' || ref.getURL() == '/SCCZone');
	}
}