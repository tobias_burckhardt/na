@isTest
private class OpportunityFirmTriggerTest {
    private static final String ROLE = 'Specifier';

    private static Opportunity opportunity;
    private static Contact contact;

    private static void setUp() {
        Account account = (Account) TestFactory.createSObject(new Account(), true);
        opportunity = (Opportunity) TestFactory.createSObject(new Opportunity(
            AccountId = account.Id,
            Amount = 5,
            Roofing_Area_SqFt__c = 5,
            Estimated_Ship_Date__c = Date.today()
        ), true);
        contact = (Contact) TestFactory.createSObject(new Contact(AccountId = account.Id), true);
    }

    @isTest static void createFirm() {
        setUp();
        Integer expectedContactRoleCreatedCount = 1;
        Opportunity_Firm__c firm = (Opportunity_Firm__c) TestFactory.createSObject(new Opportunity_Firm__c(
            Opportunity__c = opportunity.Id,
            Contact__c = contact.Id,
            Firm_Role__c = ROLE,
            Account__c = opportunity.AccountId
        ));

        Test.startTest();
            insert firm;
        Test.stopTest();

        List<OpportunityContactRole> opportunityContactRole = [SELECT OpportunityId, ContactId, Role FROM OpportunityContactRole WHERE OpportunityId =: opportunity.Id];
        
        System.assert(!opportunityContactRole.isEmpty(), 'OpportunityContactRole record doesn\'t created');
        System.assert(opportunityContactRole.size() == expectedContactRoleCreatedCount, 'More then one record created');
        System.assertEquals(firm.Opportunity__c, opportunityContactRole[0].OpportunityId, 'Opportunity Id is incorrect');
        System.assertEquals(firm.Contact__c, opportunityContactRole[0].ContactId, 'Contact Id is incorrect');
        System.assertEquals(firm.Firm_Role__c, opportunityContactRole[0].Role, 'Role is incorrect');
    }
}