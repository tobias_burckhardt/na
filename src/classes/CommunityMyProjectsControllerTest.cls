@isTest 
public class CommunityMyProjectsControllerTest {
    static testMethod void TestCommunityMyProjectsController () {
        Project__c p1 = new Project__c();
        p1.Community_User_ID__c = UserInfo.getUserId();
        insert p1;
        
        CommunityMyProjectsController cmpc = new CommunityMyProjectsController();
        test.startTest();
        apexpages.currentpage().getparameters().put('ProjNum' , '0');
        cmpc.deleteProjects();
        test.stopTest();
    }
}