@isTest
private class newSCCZoneRequestTest {

    // ensure everything works correctly when it should
    @isTest static void test_happy_path() {
        // set page we'll be testing
        PageReference pageRef = Page.sika_SCCZoneSelfRegistrationPage;
        Test.setCurrentPage(pageRef);

        // run as user.userType = 'guest' (non-authenticated) user
        User guestUser = sika_SCCZoneTestFactory.createGuestUser();
        insert(guestUser);
        System.runAs(guestUser){

            // instantiate controller class
            newSCCZoneRequest ctrl = new newSCCZoneRequest();

            // confirm the getCompanyTypes method returns results
            list<SelectOption> companyTypesTest = ctrl.getCompanyTypes();
            system.assert(companyTypesTest.size() > 0);

            // populate only the required form field values
            ctrl.firstName = 'FirstName';
            ctrl.lastName = 'LastName';
            ctrl.companyName = 'CompanyName';
            ctrl.jobTitle = 'JobTitle';
            ctrl.emailAddress = 'email@mail.com';
            ctrl.streetAddress = '123 N. Main Street';
            ctrl.city = 'Anytown';
            ctrl.country = 'United States';
            ctrl.state = 'Florida';
            ctrl.postalCode = '32222';

            // confirm that form validation passes
            ctrl.validateForm();
            system.assertEquals(ctrl.hasErrors, false);

            // populate the non-required form field values
            ctrl.phoneNo = '202-222-2222';
            ctrl.salesRep = 'Sales Rep';
            ctrl.companyType = 'CompanyType';

            // confirm that form validation passes
            ctrl.validateForm();
            system.assertEquals(ctrl.hasErrors, false);

            // submit and confirm we're on the "thank you" page
            //String nextPage = ctrl.createCase().getURL();
            //system.assertEquals(nextPage, '/apex/sika_SCCZoneNewRegistrationThankyouPage?success=true');

        }
    }

    // ensure things fail when they should. (Invalid data entered, etc.)
    @isTest static void test_sad_path() {
        // set page we'll be testing
        PageReference pageRef = Page.sika_SCCZoneSelfRegistrationPage;

        // run as user.userType = 'guest' (non-authenticated) user
        User guestUser = sika_SCCZoneTestFactory.createGuestUser();
        insert(guestUser);
        System.runAs(guestUser){

            // instantiate controller class
            newSCCZoneRequest ctrl = new newSCCZoneRequest();

            // submit the form without entering any values, and confirm that we are not redirected away from the page
            system.assert(ctrl.createCase() == null);

            // confirm that validation has failed
            system.assertEquals(ctrl.validateForm(), false);
            system.assert(ctrl.hasErrors);

            // confirm that no case has been created
            list<Case> cases = [SELECT ID from Case Limit 1];
            system.assert(cases.size() == 0);

            // enter values, but leave the email address field blank.
            ctrl.firstName = 'FirstName';
            ctrl.lastName = 'LastName';
            ctrl.companyType = 'CompanyType';
            ctrl.jobTitle = 'JobTitle';
            ctrl.streetAddress = '22 Dayrell\'s Road';
            ctrl.city = 'St. Michael';
            ctrl.state = '';
            ctrl.country= 'Barbados';
            ctrl.postalCode = 'BB14030';

            // confirm that the error returned is the "email is blank" and not the "email is invalid" error
            ctrl.validateForm();
            system.assert(ctrl.emailIsBlank == true && ctrl.emailIsInvalid == false);

            // enter whitespace in the email address field and confirm that "email is blank" is returned
            ctrl.emailAddress = '    ';
            ctrl.validateForm();
            system.assert(ctrl.emailIsBlank == true && ctrl.emailIsInvalid == false);

            // enter an invalid email address
            ctrl.emailAddress = '@mail.com';

            // validate & confirm that "email is invalid" error is returned
            ctrl.validateForm();
            system.assert(ctrl.emailIsBlank == false && ctrl.emailIsInvalid == true);

            // try a different invalid pattern
            ctrl.emailAddress = 'e@mail.';

            // validate & confirm that "email is invalid" error is returned
            ctrl.validateForm();
            system.assert(ctrl.emailIsBlank == false && ctrl.emailIsInvalid == true);

            // ...one more time
            ctrl.emailAddress = 'e@.com';

            // validate & confirm that "email is invalid" error is returned
            ctrl.validateForm();
            system.assert(ctrl.emailIsBlank == false && ctrl.emailIsInvalid == true);

            // enter a valid email address
            ctrl.emailAddress = 'i@me.bb.gov';

            // confirm that no email error is returned
            ctrl.validateForm();
            system.assert(ctrl.emailIsBlank == false && ctrl.emailIsInvalid == false);

        }
    }


    // bad case type test
    @isTest(SeeAllData=true)
    static void caseTypeTest(){
        // instantiate controller class
        newSCCZoneRequest ctrl = new newSCCZoneRequest();

        // query the schema to find all case types
        Schema.DescribeFieldResult caseTypeQuery = Case.SCC_Zone_Case_Types__c.getDescribe();
        List<Schema.PicklistEntry> caseTypes = caseTypeQuery.getPicklistValues();

        // confirm that the caseType hard-coded in the newSCCZoneRequest class matches one of the case types in this org
        system.assert(ctrl.caseType != null);

        Boolean matchFound = false;
        for(Schema.PicklistEntry p : caseTypes){
            if(ctrl.caseType == p.getValue()){
                matchFound = true;
                break;
            }
        }
        system.assert(matchFound);
        
    }
}