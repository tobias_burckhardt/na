@istest
public with sharing class testattachmentsample {    
    static testMethod void attachmentsample(){  
		Account acc = new Account();
        acc.Name='Test';
		insert acc;
        
        Attachment a = new Attachment();
        a.parentid=acc.id;
        a.name='photo';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        a.body=bodyBlob;
//        a.body='test';
        
        try{
         insert a;   
        }
        catch (Exception e) {
            System.debug('Error occurred');
        }
        
        List<Attachment> attachments = [SELECT Id FROM Attachment WHERE parent.id=:acc.id];
        System.assertEquals(1, attachments.size());
    }

}