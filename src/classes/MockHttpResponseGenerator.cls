@isTest
                        
global class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"Water": "a5Ve0000000Ckf5EAC", "Water_Variable":"0", "Cement": "a5Ve0000000CkPQEA0", "Cement_Variable": "56", "Sand1": "a5Ve0000000CkPSEA0", "Sand1_Variable": "0", "Sand2": "a5Ve0000000CkZ7EAK", "Sand2_Variable": "1370", "Sand3": null, "Sand3_Variable": "0", "Sand4": null, "Sand4_Variable": "0", "Sand5": null, "Sand5_Variable": "0", "Coarse1": "a5Ve0000000CkPTEA0", "Coarse1_Variable": "0", "Coarse2": null, "Coarse2_Variable": "0", "Coarse3": null, "Coarse3_Variable": "0", "Coarse4": null, "Coarse4_Variable": "0", "Coarse5": null, "Coarse5_Variable": "0", "Other1": null, "Other2": null, "Admixture1": null, "Admixture2": null, "Admixture3": null, "Admixture4": null, "Admixture5": null, "SCM": "a5Ve0000000CkPREA0", "Objective_Sieve": "67476.690005", "Objective_WC": null, "Calculated_WC_Ratio": "0", "Objective_Strength": "12.021976", "Calculated_Strength": "19.532728", "Measure": "metric", "ErrorMsg": null}');
        res.setStatusCode(200);
        return res;
    }
}