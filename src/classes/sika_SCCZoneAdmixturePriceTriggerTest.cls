@isTest
private class sika_SCCZoneAdmixturePriceTriggerTest {
	private static Material__c theAdmixtureMaterial;
	private static list<Mix__c> theMixes;
	private static map<Integer, Mix__c> UID_to_Mix;


	private static void init(){
		
		sika_SCCZoneMaterialTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = true;
		sika_SCCZoneMaterialTriggerHelper.bypassPreventDelete = true;
		sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = true;
		
		sika_SCCZoneMixTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMixTriggerHelper.bypassMixOwnerTransferCheck = true;
		sika_SCCZoneMixTriggerHelper.bypassMixChatterAlert = true;
		sika_SCCZoneMixTriggerHelper.bypassUpdateSCMQuantity = true;
		
		sika_SCCZoneMixMaterialTriggerHelper.mixMaterialTriggerBypassSwitch(true);
		
		// create three mixes -- one locked, two unlocked
		theMixes = new list<Mix__c>();
		Mix__c tempMix;
		
		tempMix = sika_SCCZoneTestFactory.createMix(new map<String, String>{'mixName' => 'Mix_1'});
		tempMix.isLocked__c = true;
		theMixes.add(tempMix);
		
		tempMix = sika_SCCZoneTestFactory.createMix(new map<String, String>{'mixName' => 'Mix_2'});
		tempMix.isLocked__c = false;
		theMixes.add(tempMix);
		
		tempMix = sika_SCCZoneTestFactory.createMix(new map<String, String>{'mixName' => 'Mix_3'});
		tempMix.isLocked__c = false;
		theMixes.add(tempMix);

		insert(theMixes);
		
		UID_to_Mix = new map<Integer, Mix__c>();
		for(Mix__c m : theMixes){
			if(m.Mix_Name__c == 'Mix_1'){ 
				UID_to_Mix.put(1, m); 
			} else if(m.Mix_Name__c == 'Mix_2'){ 
				UID_to_Mix.put(2, m); 
			} else {
				UID_to_Mix.put(3, m);
			}
		}
		
		// create an admixture material
		theAdmixtureMaterial = sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'type' => 'Admixture'});
		theAdmixtureMaterial.Cost__c = null;
		theAdmixtureMaterial.Unit__c = null;
		insert(theAdmixtureMaterial);
		
		// add the admixture material to all three mixes
		Mix_Material__c admixtureOnMix1 = sika_SCCZoneTestFactory.createMixMaterial(UID_to_Mix.get(1), theAdmixtureMaterial);
		Mix_Material__c admixtureOnMix2 = sika_SCCZoneTestFactory.createMixMaterial(UID_to_Mix.get(2), theAdmixtureMaterial);
		Mix_Material__c admixtureOnMix3 = sika_SCCZoneTestFactory.createMixMaterial(UID_to_Mix.get(3), theAdmixtureMaterial);
		
		list<Mix_Material__c> theMMs = new list<Mix_Material__c>{admixtureOnMix1, 
																 admixtureOnMix2, 
																 admixtureOnMix3};
		insert(theMMs);
	
	}
	
	
	@isTest static void test(){
		init();
	
		// create and insert an admixture price record
		Admixture_Price__c axp = new Admixture_Price__c();
		axp.Admixture_Material__c = theAdmixtureMaterial.Id;
		axp.Price__c = 10.00;
		axp.Unit__c = 'lb';
		
		insert(axp);
		
		list<Mix_Material__c> theMMs = [SELECT Mix__c, Material__c, Admixture_Cost__c FROM Mix_Material__c];
		
		// confirm that the mm record linking the admixture material to the two unlocked mixes has its Admixture_Cost__c field set, and the mm associated with the locked mix doesn't
		for(Mix_Material__c mm : theMMs){
			if(mm.Mix__c == UID_to_Mix.get(1).Id){
				system.assert(mm.Admixture_Cost__c == 0 || mm.Admixture_Cost__c == null);
			} else {
				system.assertEquals(mm.Admixture_Cost__c, 10.00);
			}
		}
		
		// unlock the locked mix, and lock one of the previously unlocked mixes (Unlock 1, lock 2, leave 3 unlocked.)
		for(Mix__c m : theMixes){
			if(m.Id == UID_to_Mix.get(1).Id){
				m.isLocked__c = false;
				continue;
			}
			if(m.Id == UID_to_Mix.get(2).Id){
				m.isLocked__c = true;
				continue;
			}
		}
		update(theMixes);
		
		// edit the Ax$ cost record
		axp.Price__c = 5.00;
		update(axp);
		
		// confirm that the previously LOCKED mix now has a value -- the new value -- in its Admixture_Cost__c field. 
		// confirm that the previously unlocked, but now locked mm retained the old value in Admixture_Cost__c
		// confirm that the (always) unlocked mm record has the new value
		
		// (1 = 5.00, 2 = 10.00, 3 = 5.00)
		list<Mix_Material__c> updatedMMs = [SELECT Id, Admixture_Cost__c, Mix__r.Mix_Name__c FROM Mix_Material__c];

		for(Mix_Material__c mm : updatedMMs){
			if(mm.Mix__c == UID_to_Mix.get(2).Id){
				system.assertEquals(mm.Admixture_Cost__c, 10.00);
			} else {
				system.assertEquals(mm.Admixture_Cost__c, 5.00);
			}
		}
	}

}