@IsTest
private class ApplicatorQuoteSharingServicesTest {

    /*static final List<String> PROFILE_NAMES = Label.PartnerUserProfileNames.split(',');
    static final String USERNAME1 = 'BW-IBM-01@test.com';
    static final String USERNAME2 = 'BW-IBM-02@test.com';

    @TestSetup
    static void setup() {
        Account acc1 = (Account) SObjectFactory.create(Account.SObjectType, new Map<SObjectField, Object> {
                Account.Name => 'Account1'
        });
        Account acc2 = (Account) SObjectFactory.create(Account.SObjectType, new Map<SObjectField, Object> {
                Account.Name => 'Account2'
        });
        Contact con1 = (Contact) SObjectFactory.create(Contact.SObjectType, new Map<SObjectField, Object> {
                Contact.AccountId => acc1.Id
        });
        Contact con2 = (Contact) SObjectFactory.create(Contact.SObjectType, new Map<SObjectField, Object> {
                Contact.AccountId => acc2.Id
        });
        SObjectFactory.create(User.SObjectType, new Map<SObjectField, Object> {
                User.Username => USERNAME1, User.ContactId => con1.Id,
                User.ProfileId => [SELECT Id FROM Profile WHERE Name = :PROFILE_NAMES[0] LIMIT 1].Id
        });
        SObjectFactory.create(User.SObjectType, new Map<SObjectField, Object> {
                User.Username => USERNAME2, User.ContactId => con2.Id,
                User.ProfileId => [SELECT Id FROM Profile WHERE Name = :PROFILE_NAMES[1] LIMIT 1].Id
        });
        SObjectFactory.create(Opportunity.SObjectType);
    }

    @IsTest
    static void testSharingAQandOpp_insert() {
        User testUser = [SELECT Id FROM User WHERE Username = :USERNAME1 LIMIT 1];
        User testUser2 = [SELECT Id FROM User WHERE Username = :USERNAME2 LIMIT 1];
        Id oppId = [SELECT Id FROM Opportunity LIMIT 1].Id;
        Contact con = [SELECT Id, AccountId FROM Contact WHERE Account.Name = 'Account1'];
        System.runAs(testUser) {
            System.assertEquals(0, [SELECT COUNT() FROM Opportunity], 'Test User should not have visibility to Opportunity');
        }

        System.runAs(testUser2) {
            System.assertEquals(0, [SELECT COUNT() FROM Opportunity], 'Test User should not have visibility to Opportunity');
        }

        Test.startTest();
        SObjectFactory.create(Applicator_Quote__c.SObjectType, new Map<SObjectField, Object>{
                Applicator_Quote__c.Contact__c => con.Id, Applicator_Quote__c.Account__c => con.AccountId,
                Applicator_Quote__c.Opportunity__c => oppId
        });
        Test.stopTest();

        System.runAs(testUser) {
            System.assertEquals(1, [SELECT COUNT() FROM Opportunity], 'Test User should have visibility to Opportunity');
            System.assertEquals(1, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to Applicator Quote');
        }

        System.runAs(testUser2) {
            System.assertEquals(0, [SELECT COUNT() FROM Opportunity], 'Test User should not have visibility to Opportunity');
            System.assertEquals(0, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should not have visibility to Applicator Quote');
        }
    }

    @IsTest
    static void testSharingAQandOpp_update() {
        User testUser = [SELECT Id FROM User WHERE Username = :USERNAME1 LIMIT 1];
        User testUser2 = [SELECT Id FROM User WHERE Username = :USERNAME2 LIMIT 1];
        Id oppId = [SELECT Id FROM Opportunity LIMIT 1].Id;
        Contact con = [SELECT Id, AccountId FROM Contact WHERE Account.Name = 'Account1'];
        System.runAs(testUser) {
            System.assertEquals(0, [SELECT COUNT() FROM Opportunity], 'Test User should not have visibility to Opportunity');
        }

        System.runAs(testUser2) {
            System.assertEquals(0, [SELECT COUNT() FROM Opportunity], 'Test User should not have visibility to Opportunity');
        }

        Applicator_Quote__c aq = (Applicator_Quote__c) SObjectFactory.create(Applicator_Quote__c.SObjectType, new Map<SObjectField, Object>{
                Applicator_Quote__c.Contact__c => con.Id, Applicator_Quote__c.Account__c => con.AccountId
        });

        System.runAs(testUser) {
            System.assertEquals(0, [SELECT COUNT() FROM Opportunity], 'Test User should not have visibility to Opportunity');
            System.assertEquals(0, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should not have visibility to Applicator Quote');
        }

        System.runAs(testUser2) {
            System.assertEquals(0, [SELECT COUNT() FROM Opportunity], 'Test User should not have visibility to Opportunity');
            System.assertEquals(0, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should not have visibility to Applicator Quote');
        }

        Test.startTest();
        aq.Opportunity__c = oppId;
        update aq;
        Test.stopTest();

        System.runAs(testUser) {
            System.assertEquals(1, [SELECT COUNT() FROM Opportunity], 'Test User should have visibility to Opportunity');
            System.assertEquals(1, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to Applicator Quote');
        }

        System.runAs(testUser2) {
            System.assertEquals(0, [SELECT COUNT() FROM Opportunity], 'Test User should not have visibility to Opportunity');
            System.assertEquals(0, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should not have visibility to Applicator Quote');
        }
    }

    @IsTest
    static void testSharingMultiAQS() {
        User testUser1 = [SELECT Id FROM User WHERE Username = :USERNAME1 LIMIT 1];
        User testUser2 = [SELECT Id FROM User WHERE Username = :USERNAME2 LIMIT 1];
        Contact con1 = [SELECT Id, AccountId FROM Contact WHERE Account.Name = 'Account1'];
        Contact con2 = [SELECT Id, AccountId FROM Contact WHERE Account.Name = 'Account2'];
        Id oppId = [SELECT Id FROM Opportunity LIMIT 1].Id;

        SObjectFactory.create(Applicator_Quote__c.SObjectType, new Map<SObjectField, Object>{
                Applicator_Quote__c.Contact__c => con1.Id, Applicator_Quote__c.Account__c => con1.AccountId,
                Applicator_Quote__c.Opportunity__c => oppId
        });

        System.runAs(testUser1) {
            System.assertEquals(1, [SELECT COUNT() FROM Opportunity], 'Test User should have visibility to Opportunity');
            System.assertEquals(1, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to Applicator Quote');
        }

        System.runAs(testUser2) {
            System.assertEquals(0, [SELECT COUNT() FROM Opportunity], 'Test User should not have visibility to Opportunity');
            System.assertEquals(0, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should not have visibility to Applicator Quote');
        }

        SObjectFactory.create(Applicator_Quote__c.SObjectType, new Map<SObjectField, Object>{
                Applicator_Quote__c.Contact__c => con2.Id, Applicator_Quote__c.Account__c => con2.AccountId,
                Applicator_Quote__c.Opportunity__c => oppId
        });

        System.runAs(testUser1) {
            System.assertEquals(1, [SELECT COUNT() FROM Opportunity], 'Test User should have visibility to Opportunity');
            System.assertEquals(1, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to Applicator Quote');
        }

        System.runAs(testUser2) {
            System.assertEquals(1, [SELECT COUNT() FROM Opportunity], 'Test User should have visibility to Opportunity');
            System.assertEquals(1, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to Applicator Quote');
        }
    }

    @IsTest
    static void testSharingOppWithAccount() {
        User testUser1 = [SELECT Id FROM User WHERE Username = :USERNAME1 LIMIT 1];
        User testUser2 = [SELECT Id FROM User WHERE Username = :USERNAME2 LIMIT 1];
        Contact con1 = [SELECT Id, AccountId FROM Contact WHERE Account.Name = 'Account1'];
        Contact con2 = [SELECT Id, AccountId FROM Contact WHERE Account.Name = 'Account2'];
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        opp.AccountId = con1.AccountId;
        update opp;

        SObjectFactory.create(Applicator_Quote__c.SObjectType, new Map<SObjectField, Object>{
                Applicator_Quote__c.Contact__c => con1.Id, Applicator_Quote__c.Account__c => con1.AccountId,
                Applicator_Quote__c.Opportunity__c => opp.Id
        });

        System.runAs(testUser1) {
            System.assertEquals(1, [SELECT COUNT() FROM Opportunity], 'Test User should have visibility to Opportunity');
            System.assertEquals(1, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to Applicator Quote');
        }

        System.runAs(testUser2) {
            System.assertEquals(0, [SELECT COUNT() FROM Opportunity], 'Test User should not have visibility to Opportunity');
            System.assertEquals(0, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should not have visibility to Applicator Quote');
        }

        SObjectFactory.create(Applicator_Quote__c.SObjectType, new Map<SObjectField, Object>{
                Applicator_Quote__c.Contact__c => con2.Id, Applicator_Quote__c.Account__c => con2.AccountId,
                Applicator_Quote__c.Opportunity__c => opp.Id
        });

        System.runAs(testUser1) {
            System.assertEquals(1, [SELECT COUNT() FROM Opportunity], 'Test User should have visibility to Opportunity');
            System.assertEquals(1, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to Applicator Quote');
        }

        System.runAs(testUser2) {
            System.assertEquals(0, [SELECT COUNT() FROM Opportunity], 'Test User should not have visibility to Opportunity');
            System.assertEquals(1, [SELECT COUNT() FROM Applicator_Quote__c], 'Test User should have visibility to Applicator Quote');
        }
    }
}*/


    static final List<String> PROFILE_NAMES = Label.PartnerUserProfileNames.split(',');
    static final String USERNAME1 = 'BW-IBM-01@test.com';
    static final String USERNAME2 = 'BW-IBM-02@test.com';

    @TestSetup
    static void setup() {

        Id profileId = [SELECT Id FROM Profile WHERE Name = :PROFILE_NAMES[0] LIMIT 1].Id;
        List<Account> accountList = new List<Account>();
        Account acc1 = new Account(Name = 'Account1');
        accountList.add(acc1);
        insert accountList;

        List<Contact> contactList = new List<Contact>();
        Contact cont1 = new Contact(LastName = 'lastName', AccountId = acc1.Id);
        contactList.add(cont1);
        insert contactList;

        List<User> userList = new List<User>();
        User usr1 = new User(Username = 'testUser@testuser.sika.com', ContactId = cont1.id, ProfileId = profileId, LastName = 'lastname', Email = 'testuser@testuser.com', Alias='tst', TimeZoneSidKey = 'America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
        userList.add(usr1);
        insert userList;

        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity opp1 = new Opportunity(Name = 'Opportunity', StageName = 'Quote', CloseDate = Date.today()+5);
        Opportunity opp2 = new Opportunity(Name = 'Opportunity1',AccountId = acc1.id, StageName = 'Quote', CloseDate = Date.today()+5);
        oppList.add(opp1);
        oppList.add(opp2);
        insert oppList;

        List<Applicator_Quote__c> quoteList = new List<Applicator_Quote__c>();
        Applicator_Quote__c quote1 = new Applicator_Quote__c(Contact__c = cont1.id,Account__c = acc1.id,Opportunity__c= opp1.id,
            Award_NOA__c = true);
        Applicator_Quote__c quote2 = new Applicator_Quote__c(Contact__c = cont1.id,Opportunity__c= opp1.id);
        quoteList.add(quote1);
        quoteList.add(quote2);
        insert quoteList;
    }
/*
    @isTest
    static void addApplicatorQuoteSharesToCommunityUsers() {
        List<User> userList = [SELECT Id FROM User WHERE Username = 'testUser@testuser.sika.com'];
        List<Applicator_Quote__c> quoteList = [SELECT Id FROM Applicator_Quote__c];
        List<Id> quoteIds = new List<Id>();
        for(Applicator_Quote__c quote : quoteList){
            quoteIds.add(quote.id);
        }

        Map<Id,List<Id>> userToQuoteMap = new Map<Id,List<Id>>();
        userToQuoteMap.put(userList[0].Id,quoteIds);

        Test.startTest();
            ApplicatorQuoteSharingServices.addApplicatorQuoteSharesToCommunityUsers(userToQuoteMap);
        Test.stopTest();

        List<Applicator_Quote__share> quoteShares = [SELECT id FROM Applicator_Quote__share];

        System.assertNotEquals(0,quoteShares.size(),'There should be one quote share created');
    }

    @isTest
    static void shareApplicatorQuoteWithPTS_QuoteHasNoAccount() {
        List<User> userList = [SELECT Id FROM User WHERE Username = 'testUser@testuser.sika.com'];
        List<Applicator_Quote__c> quoteList = [SELECT Id FROM Applicator_Quote__c WHERE Account__c = null];

        Test.startTest();
            ApplicatorQuoteSharingServices.shareApplicatorQuoteWithPTS(quoteList);
        Test.stopTest();

        List<Applicator_Quote__share> quoteShares = [SELECT id FROM Applicator_Quote__share];

        System.assertNotEquals(0,quoteShares.size(),'There should be one quote share created');
    }
*/
    @isTest
    static void shareApplicatorQuoteWithPTS_QuoteHasSameAccount() {
        List<User> userList = [SELECT Id FROM User WHERE Username = 'testUser@testuser.sika.com'];
        List<Applicator_Quote__c> quoteList = [SELECT Id FROM Applicator_Quote__c WHERE Account__c != null];

        Test.startTest();
            ApplicatorQuoteSharingServices.shareApplicatorQuoteWithPTS(quoteList);
        Test.stopTest();

        List<Applicator_Quote__share> quoteShares = [SELECT id FROM Applicator_Quote__share];

        System.assertNotEquals(0,quoteShares.size(),'There should be one quote share created');
    }

}