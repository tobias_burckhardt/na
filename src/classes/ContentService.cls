/**
 * @author Pavel Halas
 * @company Bluewolf, an IBM Company
 * @date 6/2016
 * 
 * Business logic related to document content.
 */
public class ContentService {

    /**
     * Returns all documents related to a given Building Group.
     */
    public static List<ContentVersion> getDocumentMetadataForBuildingGroup(String buildingGroupId){

        List<Building_Group_Product__c> buildingGroupProducts = [select Product_Lookup__c from Building_Group_Product__c where Building_Group__c = :buildingGroupId];
        Set<Id> productLookups = Pluck.ids('Product_Lookup__c', buildingGroupProducts);

        List<Content_Header_Content_Version__c> junctions = [Select Content_Header__c from Content_Header_Content_Version__c where Product2__c in: productLookups];
        Set<Id> contentHeaderIds = Pluck.ids('Content_Header__c', junctions);

        List<Content_Header__c> contentHeaders = [Select Content_Document__c from Content_Header__c Where Id in: contentHeaderIds];
        Set<Id> contentDocumentIds = Pluck.ids('Content_Document__c', contentHeaders);

        List<ContentVersion> contentVersions = [select Id, ContentDocumentId, Title, FileExtension, Document_Type__c, Related_Product__c,
                   ContentDocument.ContentSize, ContentDocument.ContentModifiedDate, ContentUrl
            from ContentVersion
            where IsLatest = true and ContentDocumentId in: contentDocumentIds];
  
        return contentVersions;
    }

    /**
     * Provides list of document contents for a given list of document IDs.
     * The content is encoded with Base 64 encoding and is ready to be sent as a result of remove call.
     */
    public static List<ContentVersionWrapper> getDocumentsContent(List<String> documentIds) {
        System.debug('Retrieving document content for Docuemnts: ' + documentIds);
        List<ContentVersionWrapper> wrappers = new List<ContentVersionWrapper>();
        
        for (ContentVersion version : [select Id, ContentDocumentId, Title, FileExtension, VersionData from ContentVersion where ContentdocumentId in :documentIds and IsLatest = true]) {
            ContentVersionWrapper wrapper = new ContentVersionWrapper();
            wrapper.Id = version.Id;
            wrapper.ContentDocumentId = version.ContentDocumentId;
            wrapper.Title = version.Title;
            wrapper.FileExtension = version.FileExtension;
            wrapper.VersionData = EncodingUtil.base64Encode(version.VersionData);
            wrappers.add(wrapper);
        }
        
        return wrappers;
    }

    /**
    * Gets lists of dependent picklists and create tree structure using recursive Node class
    * Maps are independent
    */
    public static List<Node> createTree(){

        List<Node> tree = new List<Node>();

        Map<String,List<String>> firstLevel = Utilities.GetDependentOptions('ContentVersion', 'Document_Type__c', 'Sub_Type_Level_1__c');
        Map<String,List<String>> secondLevel = Utilities.GetDependentOptions('ContentVersion', 'Sub_Type_Level_1__c', 'Sub_Type_Level_2__c');
        Map<String,List<String>> thirdLevel = Utilities.GetDependentOptions('ContentVersion', 'Sub_Type_Level_2__c', 'Sub_Type_Level_3__c');

        //root level
        for(String first : firstLevel.keySet()){

            // if is empty skip iteration
            if(String.isBlank(first)) continue;

            Node n = new Node();
            n.title = first;
            n.path = '/' + first;
            //second level
            for(String child : firstLevel.get(first)){

                Node childNode = new Node();
                childNode.title = child;
                childNode.path = n.path + '/' + child;

                n.children.add(childNode);

                if(secondLevel.containsKey(child) && secondLevel.get(child).size() > 0){
                    //third level
                    for(String child2 : secondLevel.get(child)){

                        Node child2Node = new Node();
                        child2Node.title = correctTitle(child2);
                        child2Node.path = childNode.path + '/' + child2; 

                        childNode.children.add(child2Node);

                        if(thirdLevel.containsKey(child2) && thirdLevel.get(child2).size() > 0){
                            //fourth level
                            for(String child3 : thirdLevel.get(child2)){

                                Node child3Node = new Node();
                                child3Node.title = child3;
                                child3Node.path = child2Node.path + '/' + child3;

                                child2Node.children.add(child3Node);
                            }
                        }
                    }
                }
            }

            tree.add(n);

        }

        return tree;
    }
    /**
    * Small hack for handling situations when there are same titles on same level. They should have picklists values with number postfix.
    **/
    private static String correctTitle(String title){

        if(title.contains('Roofing')) return 'Roofing';
        if(title.contains('Waterproofing')) return 'Waterproofing';

        return title;
    }

    /**
     * Creates dynamic SOQL query and returns Content file metadata. 
     * Input parameter is path with sub type levels delimetered with shlashes
     */
    public static List<ContentVersion> getDocumentsMetadata(String path){
        List<String> params = path.removeStart('/').split('/');

        String query = 'SELECT Id,ContentDocumentId, Title,FileExtension, CreatedDate, ContentSize, Category__c ' +
            'FROM ContentVersion WHERE IsLatest = true AND Document_Type__c = \'' + params[0] + '\'';

        for (Integer i = 1; i < params.size(); i++) {
                query += ' AND Sub_Type_Level_' + String.valueOf(i) + '__c = \'' + params[i] + '\'';
        }

        return Database.query(query);
    }

    public static List<ContentVersion> getDocumentsByNameLike(String name){

        String s = '%' + name + '%';

        return [SELECT Id, ContentDocumentId, Title, CreatedDate, FileExtension, ContentSize, Category__c FROM ContentVersion WHERE Title LIKE :s];
    }

    public with sharing class ContentVersionWrapper {
        public String Id {get; set;}
        public String ContentDocumentId {get; set;}
        public String Title {get; set;}
        public String FileExtension {get; set;}
        public String VersionData {get; set;}
    }
}