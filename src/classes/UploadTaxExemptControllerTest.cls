/*
* Author: Martin Kona
* Company: Bluewolf
* Date: April 29, 2016
* Description: Test class for UploadTaxExemptController
* 
*/

@isTest
private class UploadTaxExemptControllerTest {
    
    @isTest
	public static void testController()
	{
        Opportunity opp = (Opportunity)TestFactory.createSObject(new Opportunity(name='Oppa gangman style',Amount = 5,Roofing_Area_SqFt__c = 5,Estimated_Ship_Date__c = Date.today()), true);

		UploadTaxExemptController controller=new UploadTaxExemptController();
		controller.sobjId=opp.id;
		
		System.assertEquals(0, controller.getContents().size());
		
		System.assertEquals(1, controller.newContents.size());
		
		controller.addMore();
		
		System.assertEquals(1 + UploadTaxExemptController.NUM_CONTENTS_TO_ADD, controller.newContents.size());
		
		// populate the first and third new attachments
		List<Attachment> newAtts = controller.newContents;
		newAtts[0].Name='Unit Test 1';
		newAtts[0].Description='Unit Test 1';
		newAtts[0].Body=Blob.valueOf('Unit Test 1');

		newAtts[2].Name='Unit Test 2';
		newAtts[2].Description='Unit Test 2';
		newAtts[2].Body=Blob.valueOf('Unit Test 2');
		
		controller.save();
		
		System.assertEquals(2, controller.getContents().size());
		System.assertNotEquals(null, controller.done());
    }
    
    @isTest
    public static void testDeleteAttachment() {
		
        Opportunity opp = (Opportunity)TestFactory.createSObject(new Opportunity(name='Oppa gangman style',Amount = 5,Roofing_Area_SqFt__c = 5,Estimated_Ship_Date__c = Date.today()), true);
        
		UploadTaxExemptController controller = new UploadTaxExemptController();
		controller.sobjId = opp.id;
        
        List<Attachment> newAtts = controller.newContents;
       	newAtts[0].Name='Unit Test 1';
		newAtts[0].Description='Unit Test 1';
		newAtts[0].Body=Blob.valueOf('Unit Test 1');
		controller.save();
        
        System.assertEquals(1, controller.getContents().size());
        
        ApexPages.currentPage().getParameters().put('contentId', controller.contents[0].Id);
        controller.deleteContent();
        // NULL contents to query again for records
        controller.contents = NULL;
        System.assertEquals(0, controller.getContents().size());                    
    }
    
    @isTest
    public static void testSmoke() {
        Opportunity opp = (Opportunity)TestFactory.createSObject(new Opportunity(name='Oppa gangman style',Amount = 5,Roofing_Area_SqFt__c = 5,Estimated_Ship_Date__c = Date.today()), true);
        
		UploadTaxExemptController controller = new UploadTaxExemptController();
		controller.sobjId = opp.id;
        
        Opportunity opp2 = controller.opp;
		controller.done();
		//controller.viewContentVersion();        
    }

}