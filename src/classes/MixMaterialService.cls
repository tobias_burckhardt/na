public class MixMaterialService {
    public static final String MATERIAL_TYPE_SAND = 'Sand';
    public static final String MATERIAL_TYPE_COARSE_AGGREGATE = 'Coarse Aggregate';
    public static final String MATERIAL_TYPE_ADMIXTURE = 'Admixture';
    public static final String MATERIAL_TYPE_WATER = 'Water';

    public static void updateMoistureAdjustment(List<Mix_Material__c> newMixMaterials,
            Map<Id, Mix_Material__c> oldMixMaterialsById) {
        List<Mix_Material__c> filteredMixMaterials = filterUpdateMoistureAdjustment(newMixMaterials, oldMixMaterialsById);
        updateMoistureAdjustment(filteredMixMaterials);
    }

    public static void updateMoistureAdjustment(List<Mix_Material__c> mixMaterials) {
        Map<Id, Material__c> materialsById = new Map<Id, Material__c>(getRelatedMaterials(mixMaterials));
        Map<Id, Mix__c> mixesById = new Map<Id, Mix__c>(getRelatedMixes(mixMaterials));
        for (Mix_Material__c mixMaterial : mixMaterials) {
            Material__c material = materialsById.get(mixMaterial.Material__c);
            Mix__c mix = mixesById.get(mixMaterial.Mix__c);
            if (material != null && mix != null) {
                updateMoistureAdjustment(mixMaterial, material, mix);
            }
        }
    }

    public static List<Mix__c> getRelatedMixes(List<Mix_Material__c> mixMaterials) {
        Set<Id> mixIds = Pluck.ids('Mix__c', mixMaterials);
        return [SELECT Water_Adjustment_kg__c FROM Mix__c WHERE Id IN :mixIds];
    }

    public static List<Material__c> getRelatedMaterials(List<Mix_Material__c> mixMaterials) {
        Set<Id> materialIds = Pluck.ids('Material__c', mixMaterials);
        return [SELECT Type__c, Abs__c, Solid_Content__c FROM Material__c WHERE Id IN :materialIds];
    }

    public static void updateMoistureAdjustment(Mix_Material__c mixMaterial, Material__c material, Mix__c mix) {
        if (mixMaterial.Quanity_kg__c != null && material.Abs__c != null && mixMaterial.Moisture__c != null &&
                (MATERIAL_TYPE_SAND.equalsIgnoreCase(material.Type__c) ||
                 MATERIAL_TYPE_COARSE_AGGREGATE.equalsIgnoreCase(material.Type__c))) {
            mixMaterial.Moisture_Adjustment_2__c =
                mixMaterial.Quanity_kg__c * (Decimal.valueOf(material.Abs__c) - mixMaterial.Moisture__c) / 100;
        } else if (mixMaterial.Quanity_kg__c != null && material.Solid_Content__c != null &&
                MATERIAL_TYPE_ADMIXTURE.equalsIgnoreCase(material.Type__c)) {
            mixMaterial.Moisture_Adjustment_2__c = mixMaterial.Quanity_kg__c * (1 - material.Solid_Content__c);
        } else if (mixMaterial.Quanity_kg__c != null && mix.Water_Adjustment_kg__c != null &&
                MATERIAL_TYPE_WATER.equalsIgnoreCase(material.Type__c)) {
            mixMaterial.Moisture_Adjustment_2__c = -mix.Water_Adjustment_kg__c;
        }
    }

    private static List<Mix_Material__c> filterUpdateMoistureAdjustment(List<Mix_Material__c> newMixMaterials,
            Map<Id, Mix_Material__c> oldMixMaterialsById) {
        List<Mix_Material__c> filteredMixMaterials = new List<Mix_Material__c>();
        for (Mix_Material__c newMixMaterial : newMixMaterials) {
            Mix_Material__c oldMixMaterial = oldMixMaterialsById.get(newMixMaterial.Id);
            if (newMixMaterial.Quanity_kg__c != oldMixMaterial.Quanity_kg__c ||
                    newMixMaterial.Moisture__c != oldMixMaterial.Moisture__c ||
                    newMixMaterial.Material__c != oldMixMaterial.Material__c ||
                    newMixMaterial.Mix__c != oldMixMaterial.Mix__c) {
                filteredMixMaterials.add(newMixMaterial);
            }
        }
        return filteredMixMaterials;
    }
}