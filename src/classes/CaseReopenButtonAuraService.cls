public with sharing class CaseReopenButtonAuraService {
	
	@AuraEnabled
	public static void updateCase(Id caseId) {
		Case caseRecord = CaseReopenButtonDataAccessor.queryCase(caseId);
		caseRecord.RecordTypeId = CaseReopenButtonDataAccessor.queryCaseInternalItRecordTypeId();
		caseRecord.Status = 'New';
		update caseRecord;
	}
}