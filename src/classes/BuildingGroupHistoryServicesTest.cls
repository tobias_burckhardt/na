@IsTest public with sharing class BuildingGroupHistoryServicesTest {

    static final String TEST_NAME_1 = 'TEST_NAME 1';
    static final String TEST_NAME_2 = 'TEST_NAME 2';

    static Building_Group_Product__c buildingGroupProduct;
    static Product2 poduct;

    static void setup() {
        poduct = (Product2)TestFactory.createSObject(new Product2(Name = TEST_NAME_1), true);
        buildingGroupProduct = (Building_Group_Product__c)TestFactory.createSObject(new Building_Group_Product__c(Product_lookup__c = poduct.Id), true);
    }
    
    @IsTest
    static void saveBuildingGroupHistoryTrue() {
        setup();

        poduct = (Product2)TestFactory.createSObject(new Product2(Name = TEST_NAME_2), true);

        buildingGroupProduct.Product_lookup__c = poduct.Id;
        Test.startTest();
            update buildingGroupProduct;
        Test.stopTest();
        List<Building_Group_History__c> result = [SELECT Id, Modified_From__c, Modified_To__c FROM Building_Group_History__c];

        System.assertEquals(result.size(), 1);
        System.assertEquals(result.get(0).Modified_From__c, TEST_NAME_1);
        System.assertEquals(result.get(0).Modified_To__c, TEST_NAME_2);
    }
    
    @IsTest
    static void saveBuildingGroupHistoryFalse() {
        setup();
        buildingGroupProduct.New__c = true;
        Test.startTest();
            update buildingGroupProduct;
        Test.stopTest();
        List<Building_Group_History__c> result = [SELECT Id FROM Building_Group_History__c];
        System.assert(result.isEmpty());
    }

}