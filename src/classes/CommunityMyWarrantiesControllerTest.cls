@isTest 
public class CommunityMyWarrantiesControllerTest {
    static testMethod void TestCommunityMyWarrantiesController() {

        Warranty__c w1 = new Warranty__c();
        w1.Community_User_ID__c = UserInfo.getUserId();
        insert w1;
        
        CommunityMyWarrantiesController cmwc = new CommunityMyWarrantiesController();
        test.startTest();
        apexpages.currentpage().getparameters().put('WarrNum' , '0');
        cmwc.deleteWarranties();
        test.stopTest();
        
    }
}