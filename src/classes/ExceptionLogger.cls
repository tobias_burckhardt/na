public with sharing class ExceptionLogger {
    public static final String ERROR_CREATING_LOG = 'Error creating log: {0}. Please contact your Administrator.';

    public static Id logError(Exception ex, Id recordId) {
        ExLog__c log = new ExLog__c(
            Exception_Description__c = ex.getMessage(),
            Stack_Trace__c = ex.getStackTraceString(),
            Record_Id__c = recordId,
            Who__c = UserInfo.getUserName(),
            Organization_Id__c = UserInfo.getOrganizationId(),
            Organization_Name__c = UserInfo.getOrganizationName(),
            Session_Id__c = UserInfo.getSessionId()
        );

        try {
            insert log;
        } catch (DmlException e) {
            String message = String.format(ERROR_CREATING_LOG, new List<String>{e.getMessage()});
            System.debug(System.LoggingLevel.ERROR, message);
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, message));
        }

        return log.Id;
    }
}