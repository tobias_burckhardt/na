/* **********************************************************
Name        :CommunityChangePasswordController
Description :to change use password
             
Author      :Shradha Bansal
Created On  :2/25/2015
Modified On :
********************************************************** 
*/
global with sharing class CommunityChangePasswordController {
    global String oldpassword{get;set;}
    global String newpassword{get;set;}
    global String confirmpassword{get;set;}
    global CommunityChangePasswordController () {}
    
    global PageReference changePassword() {
        return Site.changePassword(newpassword, confirmpassword, oldpassword);
    }     

}