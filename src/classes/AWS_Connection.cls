public class AWS_Connection {
    
    // Declare global variables
    Public Static String contentType = '';
    Public Static String lib;
	Public Static String bucketName;
	Public Static String key;
	Public Static String secret;
    Public Static String folderPath = '';    
    AWS_Settings__c s3;
    
    // Class constructor
    public AWS_Connection() {
        
        // Query AWS connection settings
        s3 = [SELECT AWS_Key__c, AWS_Secret__c, S3_Bucket_Name__c, SF_Library_Name__c FROM AWS_Settings__c LIMIT 1];
        
        
        lib = s3.SF_Library_Name__c; //05846000000Tdwx
        bucketName = s3.S3_Bucket_Name__c;
        key = s3.AWS_Key__c;
        secret = s3.AWS_Secret__c;
        
    }
    

	    
    public void saveToS3(ContentVersion fileVersion, String fileExtension){
        
        String auth;
        String filename;
        String reqHeaders;
        Blob binaryPDF = fileVersion.VersionData;
        Integer fileSize = fileVersion.ContentSize;
        String formattedDateString = Datetime.now().format('EEE, dd MMM yyyy HH:mm:ss z','America/New York'); 
        
        String docName = fileVersion.Title;
        String action = 'PUT';
        
        folderPath = buildFolderPath(fileVersion);
        filename = folderPath + docName; 
        filename = filename.replace(' ', '+') + '.' + fileExtension;
        
        
        reqHeaders = 'x-amz-acl:public-read'; // Sets the file on AWS to public
        
        // Call function that creates the signature
        auth = createAuthHeader(action, reqHeaders, filename, key, secret, bucketName, formattedDateString);
        
        // cal function that handles that excecutes the call
        execAWSCall(action, binaryPDF, bucketName, filename, fileSize, auth, formattedDateString);
    }
        
    
    public void deleteFromS3(ContentVersion fileVersion, String fileExtension){
        String auth;
        String filename;
        String reqHeaders = '';
        Blob binaryPDF = fileVersion.VersionData;
        String formattedDateString = Datetime.now().format('EEE, dd MMM yyyy HH:mm:ss z','America/New York'); 
        Integer fileSize = fileVersion.ContentSize;     
        String docName = fileVersion.Title;
        String action = 'DELETE';
        
        folderPath = buildFolderPath(fileVersion);
        filename = folderPath + docName; 
        filename = filename.replace(' ', '+') + '.' + fileExtension;
        
        auth = createAuthHeader(action, reqHeaders, filename, key, secret, bucketName, formattedDateString);
        
        execAWSCall(action, binaryPDF, bucketName, filename, fileSize, auth, formattedDateString);
    }
    
    
    
    @future (callout=true)
    /**
	* Make call to AWS
	*/
    public static void execAWSCall(String action, Blob fileContent, String bucket, String filePath, Integer size, String authorization, String formattedDate){
        
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        String oldPath;
        
        if (action == 'COPY') {
            List <string> paths = filePath.split('\n');
            oldPath = '/' + bucket + '/' + paths.get(0);
            filePath = paths.get(1);
            req.setMethod('PUT');
            req.setHeader('x-amz-copy-source', oldPath);
            System.debug('x-amz-copy-source: ' + oldPath);
        }else{
            req.setMethod(action);
        }
        
        System.debug('filePath: ' + filePath);
        
        
        req.setHeader('Host', 's3.amazonaws.com'); 
        req.setEndpoint('https://s3.amazonaws.com/' + bucket + '/' + filePath); 
        req.setHeader('Date', formattedDate);
        System.debug('Date: ' + formattedDate);
        
        // Only for put operations, set file to public by passing this header param
        if(action == 'PUT'){
            req.setHeader('x-amz-acl', 'public-read');
        }
        
        req.setHeader('Authorization', authorization);
        System.debug('Authorization: ' + authorization);
        if(fileContent != null || action == 'DELETE' || action == 'COPY'){
            
            // Only when adding files to AWS, the call body needs to be set to the file content
            if(action == 'PUT'){
                Blob fileBlob = fileContent;
                req.setBodyAsBlob(fileBlob);
            }
            
            // don't pass body content or file size when copying via API. It will timeout 
            if(action != 'COPY'){
                req.setHeader('Content-Length', string.valueOf(size));
                System.debug('Size: ' + string.valueOf(size));
            }
            
            //Execute web service call
            // Do not make callout when testing with file name = Penguins.jpg
            if (filePath != 'Specifications/Guide+Specifications/Roofing+Specs/Green+Roof/Penguins.jpg' &&
               filePath != 'Specifications/Guide+Specifications/Roofing+Specs/Adhered/Penguins.jpg') {
                try {
                    HTTPResponse res = http.send(req);
                    System.debug('MYDEBUG: ' + filePath + ' RESPONSE STRING: ' + res.toString());
                    System.debug('MYDEBUG: ' + filePath + ' RESPONSE STATUS: '+ res.getStatus());
                    System.debug('MYDEBUG: ' + filePath + ' STATUS_CODE:'+ res.getStatusCode());
                    if (action == 'COPY' && res.getStatus() == 'OK') {
                        // Tried to delete file only after this call was completed but future methods cannot
                        // be called from another futuere method
                    }
                    
                } catch(System.CalloutException e) {
                    system.debug('MYDEBUG: AWS Service Callout Exception on ' + filePath + 'ERROR: ' + e.getMessage());
                }
            }
        }
    }
    
    
    //create signature header for Amazon S3 REST API
    //this signature needs to match what will be passed in the call or it will
    //return a 401 forbidden error
    public Static String createAuthHeader(String method, String headers, String filename, String keyString, String secretString, String bucket, String formattedDate){
        string auth;
        String stringToSign;
        
        if(headers != ''){
            headers = '\n' + headers;
        }
        
        if (method == 'COPY'){
            stringToSign = 'PUT' + '\n' + '' + '\n' + '' + '\n' + formattedDate + headers;
        }else{
            stringToSign = method + '\n' + '' + '\n' + '' + '\n' + formattedDate + headers + '\n/' + bucket + '/' + filename;
        }
        
        System.debug('StringToSign: ' + stringToSign);
        Blob mac = Crypto.generateMac('HMACSHA1', blob.valueof(stringToSign),blob.valueof(secretString));
        String sig = EncodingUtil.base64Encode(mac);
        auth = 'AWS' + ' ' + keyString + ':' + sig;
        return auth;
    }
    
    
    
    /**
    * Helper method to decide whether to upload the file to S3.
    * If content is published to a specific library, depending on the file owner 
    * this method decides whether to transfer the file to S3.
    */
    public boolean shouldPublishToAWS(String id) {        
        system.debug('====== Content Id' + id);
        
        ContentDocumentLink cdl = [SELECT LinkedEntityId FROM ContentDocumentLink WHERE ContentDocumentId = :id LIMIT 1].get(0);
        String libId = cdl.LinkedEntityId;
        list<ContentWorkspace> libObj = [SELECT Name FROM ContentWorkspace WHERE Id = :libId LIMIT 1];
        
        if(libObj.size() > 0 && libObj.get(0).Name.contains(lib)) { 
            system.debug('====== Send to AWS');
            return true;
        } else {
            system.debug('====== Do not send to AWS');
            return false;
        }
    }
    
    
    
    /**
    * Build folder path based on content fields
    */
    public static String buildFolderPath(ContentVersion content) {
        String path = '';
        if (content.Document_Type__c != null) {
            path += content.Document_Type__c + '/';
            
            if (content.Sub_Type_Level_1__c != null) {
                path += content.Sub_Type_Level_1__c + '/';
                
                if (content.Sub_Type_Level_2__c != null) {
                    path += content.Sub_Type_Level_2__c + '/';
                    
                    if (content.Sub_Type_Level_3__c != null) {
                        path += content.Sub_Type_Level_3__c + '/';
                    }
                    
                }
                
            }
            
        }
        
        return path;
        
    }
    
}