@isTest
private class MixServiceTest {
    private static final String ABS = '5';
    private static final Integer QUANTITY = 1;
    private static final Decimal MOISTURE = 5.8;
    private static final Decimal SOLID_CONTENT = 0.25;
    private static final Decimal WATER_ADJUSTMENT = 5.15;

    @TestSetup
    static void testSetup() {
        skipUnwantedMixMaterialTriggers();
        
        Material__c material = TestingUtils.createMaterial(false);
        material.Type__c = MixMaterialService.MATERIAL_TYPE_WATER;
        material.Abs__c = ABS;
        material.Solid_Content__c = SOLID_CONTENT;
        insert material;

        Mix__c mix = TestingUtils.createMix(5, 'Test Mix', 0.5, 0.5, 0.5, false);
        mix.Water_Adjustment_kg__c = WATER_ADJUSTMENT;
        insert mix;

        Mix_Material__c mixMaterial = TestingUtils.createMixMaterial(mix, material, false);
        mixMaterial.Moisture__c = MOISTURE;
        mixMaterial.Quantity__c = QUANTITY;
        mixMaterial.Unit__c = 'kgs';
        insert mixMaterial;
    }

    static testmethod void testUpdateMixMaterialMoistureAdjustment() {
        skipUnwantedMixMaterialTriggers();

        Mix__c mix = [SELECT Id FROM Mix__c WHERE Mix_Name__c = 'Test Mix' LIMIT 1];

        mix.Water_Adjustment_kg__c = 3;

        Test.startTest();
        update mix;
        Test.stopTest();

        Mix_Material__c mixMaterial =
            [SELECT Moisture_Adjustment_2__c FROM Mix_Material__c WHERE Mix__c = :mix.Id LIMIT 1];

        Decimal expected =  -3;//QUANTITY - 3;
        System.assertEquals(expected, mixMaterial.Moisture_Adjustment_2__c,
            'Moisture adjustment should be calculated correctly.');
    }

    private static void skipUnwantedMixMaterialTriggers() {
        Sika_SCCZoneMixMaterialTriggerHelper.bypassAccountTeamCheck = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassPopulateMaterialTypeCounts = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassSetSCMWeightAndUnit = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculatePasteVolume = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculateTotalSolidsVolumeLiters = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculateTotalMixVolumeLiters = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculateWaterAdjustment = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassPopulateMaterialTypeWeights = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassConcreteCurveCalc = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculateTotalMaterialCost = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassAddOverLimitErrors = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassMixOptimizationCheck = true;
        Sika_SCCZoneMixTriggerHelper.bypassAccountTeamCheck = true;
        Sika_SCCZoneMixTriggerHelper.bypassUpdateSCMQuantity = true;
        Sika_SCCZoneMixTriggerHelper.bypassMixOwnerTransferCheck = true;
        Sika_SCCZoneMixTriggerHelper.bypassMixChatterAlert = true;
    }
}