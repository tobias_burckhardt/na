@isTest
private class sika_SCCZoneTestResultTriggerTest {
	private static sika_SCCZoneTestResultsController ctrl;
	private static PageReference pageRef;

	private static Mix__c theMix; 
	private static list<Material__c> theMaterials;
	private static list<Mix_Material__c> theMMs;
	private static User SCCZoneUser;
	//private static User adminUser;
	private static User adminUser2;

	@isTest static void testLockAndUnlock(){

		map<String, sObject> objects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
		SCCZoneUser = (User) objects.get('SCCZoneUser');
		//adminUser = (User) objects.get('adminUser');
		adminUser2 = sika_SCCZoneTestFactory.createAdminUser();
		insert(adminUser2);

		theMaterials = new list<Material__c>();
		theMMs = new list<Mix_Material__c>();

		Material__c admixtureMaterial;

		System.runAs(adminUser2){

	        theMix = sika_SCCZoneTestFactory.createMix();
	        insert(theMix);

	        Material__c waterMaterial = sika_SCCZoneTestFactory.createMaterialWater();
	        theMaterials.add(waterMaterial);

	        admixtureMaterial = sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'type' => 'Admixture'});
			admixtureMaterial.Cost__c = null;
			admixtureMaterial.Unit__c = null;
			admixtureMaterial.Solid_Content__c = 1;
			theMaterials.add(admixtureMaterial);

			insert(theMaterials);

			set<Id> theMaterialIDs = new set<Id>();
			for(Material__c m : theMaterials){
				theMaterialIDs.add(m.Id);
			}

	        theMMs = new list<Mix_Material__c>();
	        theMMs.add(sika_SCCZoneTestFactory.createMixMaterial(theMix, waterMaterial));
	        theMMs.add(sika_SCCZoneTestFactory.createMixMaterial(theMix, admixtureMaterial));
	        
	        // insert the Mix_Materials, bypassing all Mix_Material trigger methods
	        sika_SCCZoneMixMaterialTriggerHelper.mixMaterialTriggerBypassSwitch(true);
	        insert(theMMs);


		    Test.startTest();

	        Test_Result__c testResult = new Test_Result__c();
	        testResult.Mix__c = theMix.Id;
	        insert(testResult);

	        list<Mix__c> testMixes = [SELECT Id, isLocked__c FROM Mix__c WHERE Id = :theMix.Id];
	        system.assert(testMixes[0].isLocked__c);

	        list<Material__c> testMaterials = [SELECT Id, Type__c, isLocked__c FROM Material__c WHERE Id IN :theMaterialIDs];

	        for(Material__c m : testMaterials){
	        	if(m.Type__c == 'Admixture'){
	        		system.assert(m.isLocked__c == false);
	        	} else {
	        		system.assert(m.isLocked__c);
	        	}
	        }

	        delete(testResult);

	        testMixes = [SELECT Id, isLocked__c FROM Mix__c WHERE Id = :theMix.Id];
	        system.assert(testMixes[0].isLocked__c == false);

	        testMaterials = [SELECT Id, Type__c, isLocked__c FROM Material__c WHERE Id IN :theMaterialIDs];

	        for(Material__c m : testMaterials){
	        	system.assert(m.isLocked__c == false);
	        }

		    Test.stopTest();
		}

	}

}