@isTest
private class APTS_CloneQuotesPageControllerTest {
	
	testMethod static void test() {
		Apttus_Proposal__Proposal__c proposal = new Apttus_Proposal__Proposal__c();
		insert proposal;

		ApexPages.currentPage().getParameters().put('id', proposal.Id);
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(proposal);
		APTS_CloneQuotesPageController ctrl = new APTS_CloneQuotesPageController(stdCtrl);
		ctrl.cloneItems();
	}
	
}