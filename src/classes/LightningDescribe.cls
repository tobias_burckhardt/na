public class LightningDescribe {

    public class FieldInfo {
        //type is a reserved word in apex
        @AuraEnabled public String displayType { get; set; }
        @AuraEnabled public String inlineHelpText { get; set; }
        @AuraEnabled public Boolean isAccessible { get; set; }
        @AuraEnabled public Boolean isCaseSensitive { get; set; }
        @AuraEnabled public Boolean isCreateable { get; set; }
        @AuraEnabled public Boolean isDefaultedOnCreate { get; set; }
        @AuraEnabled public Boolean isDependentPicklist { get; set; }
        @AuraEnabled public Boolean isNillable { get; set; }
        @AuraEnabled public Boolean isRestrictedDelete { get; set; }
        @AuraEnabled public Boolean isRestrictedPicklist { get; set; }
        @AuraEnabled public Boolean isUnique { get; set; }
        @AuraEnabled public Boolean isUpdateable { get; set; }
        @AuraEnabled public String label { get; set; }
        @AuraEnabled public Integer length { get; set; }
        @AuraEnabled public String name { get; set; }
        @AuraEnabled public List<PicklistEntryInfo> picklistValues { get; set; }

        public DescribeFieldResult descToken;
        public Set<FieldProp> hasProps;
    }

    public Enum FieldProp {
        //type is  a reserved word in apex
        DISPLAY_TYPE,
        INLINE_HELP_TEXT,
        IS_ACCESSIBLE,
        IS_CASE_SENSITIVE,
        IS_CREATEABLE,
        IS_DEFAULTED_ON_CREATE,
        IS_DEPENDENT_PICKLIST,
        IS_NILLABLE,
        IS_RESTRICTED_DELETE,
        IS_RESTRICTED_PICKLIST,
        IS_UNIQUE,
        IS_UPDATEABLE,
        LABEL,
        LENGTH,        
        PICKLIST_VALUES
    }

    public class FieldSetInfo {
        @AuraEnabled public String description { get; set; }
        @AuraEnabled public List<FieldSetMemberInfo> fields { get; set; }
        @AuraEnabled public String label { get; set; }
        @AuraEnabled public String name { get; set; }
        @AuraEnabled public String namespace { get; set; }
    }

    public class FieldSetMemberInfo {
        @AuraEnabled public Boolean dbRequired { get; set; }
        @AuraEnabled public String fieldPath { get; set; }
        @AuraEnabled public String label { get; set; }
        @AuraEnabled public Boolean required { get; set; }
        @AuraEnabled public String type { get; set; }
    }

    public class ObjectInfo {

        @AuraEnabled public Map<String, FieldInfo> fields { get {
            if (fields == null) {
                fields = new Map<String, FieldInfo>();
            }
            return fields;
        } set; }
        @AuraEnabled public Map<String, FieldSetInfo> fieldSets { get {
            if (fieldSets == null) {
                fieldSets = new Map<String, FieldSetInfo>();
            }
            return fieldSets;
        } set; }
        @AuraEnabled public Boolean isAccessible { get; set; }
        @AuraEnabled public Boolean isCreateable { get; set; }
        @AuraEnabled public Boolean isDeletable { get; set; }
        @AuraEnabled public Boolean isFeedEnabled { get; set; }
        @AuraEnabled public Boolean isMergeable { get; set; }
        @AuraEnabled public Boolean isQueryable { get; set; }
        @AuraEnabled public Boolean isSearchable { get; set; }
        @AuraEnabled public Boolean isUndeletable { get; set; }
        @AuraEnabled public Boolean isUpdateable { get; set; }
        @AuraEnabled public String keyPrefix { get; set; }
        @AuraEnabled public String label { get; set; }
        @AuraEnabled public String labelPlural { get; set; }
        @AuraEnabled public String name { get; set; }

        public Set<ObjectProp> hasProps;
        private DescribeSobjectResult descToken;

        public ObjectInfo(String objectName) {
            this(Schema.getGlobalDescribe().get(objectName), new Set<ObjectProp>());
        }
        public ObjectInfo(SobjectType sobjType) {
            this(sobjType, new Set<ObjectProp>());
        }
        public ObjectInfo(String objectName, Set<ObjectProp> toDescribe) {
            this(Schema.getGlobalDescribe().get(objectName), toDescribe);
        }
        public ObjectInfo(SobjectType sobjType, Set<ObjectProp> toDescribe) {
            this.descToken = sobjType.getDescribe();
            this.name = this.descToken.getName();
            this.hasProps = new Set<ObjectProp>();
            this.addProps(toDescribe);
        }
        public ObjectInfo addFields(Set<String> fieldNames, Set<FieldProp> toDescribe) {
            Set<SobjectField> sobjFields = new Set<SobjectField>();
            for (String fieldName : fieldNames) {
                sobjFields.add(descToken.fields.getMap().get(fieldName));
            }
            return this.addFields(sobjFields, toDescribe);
        }
        public ObjectInfo addFields(Set<SobjectField> sobjFields, Set<FieldProp> toDescribe) {
            for (SobjectField sobjField : sobjFields) {
                try {
                    FieldInfo field = fieldInfoFactory(sobjField);
                    FieldInfo existingField = this.fields.get(field.Name);
                    if (existingField == null) {
                        fieldInfoAddProps(field, toDescribe);
                        this.fields.put(field.Name, field);
                    } else {
                        fieldInfoAddProps(existingField, toDescribe);
                    }
                } catch (Exception e) {
                    continue;
                }
            }
            return this;
        }
        public ObjectInfo addFieldSet(String fieldSetName) {
            this.fieldSets.put(fieldSetName,
                fieldSetInfoFactory(descToken.fieldSets.getMap().get(fieldSetName)));
            return this;
        }
        public ObjectInfo addFieldSet(String fieldSetName, Set<FieldProp> toDescribe) {
            this.addFieldSet(fieldSetName);
            Set<String> fieldNames = new Set<String>();
            for (FieldSetMemberInfo fSetMemberInfo : this.fieldSets.get(fieldSetName).fields) {
                fieldNames.add(fSetMemberInfo.fieldPath);
            }
            return this.addFields(fieldNames, toDescribe);
        }
        public ObjectInfo addFieldSets(Set<String> fieldSetNames) {
            for (String fieldSetName: fieldSetNames) {
                this.addFieldSet(fieldSetName);
            }
            return this;
        }
        public ObjectInfo addFieldSets(Set<String> fieldSetNames, Set<FieldProp> toDescribe) {
            for (String fieldSetName: fieldSetNames) {
                this.addFieldSet(fieldSetName, toDescribe);
            }
            return this;
        }
        public ObjectInfo addProps(Set<ObjectProp> requestedToDescribe) {
            Set<ObjectProp> toDescribe = this.missingProps(requestedToDescribe);
            if (toDescribe.contains(ObjectProp.IS_ACCESSIBLE)) { this.isAccessible = this.descToken.isAccessible(); }
            if (toDescribe.contains(ObjectProp.IS_CREATEABLE)) { this.isCreateable = this.descToken.isCreateable(); }
            if (toDescribe.contains(ObjectProp.IS_DELETABLE)) { this.isDeletable = this.descToken.isDeletable(); }
            if (toDescribe.contains(ObjectProp.IS_FEED_ENABLED)) { this.isFeedEnabled = this.descToken.isFeedEnabled(); }
            if (toDescribe.contains(ObjectProp.IS_MERGEABLE)) { this.isMergeable = this.descToken.isMergeable(); }
            if (toDescribe.contains(ObjectProp.IS_QUERYABLE)) { this.isQueryable = this.descToken.isQueryable(); }
            if (toDescribe.contains(ObjectProp.IS_SEARCHABLE)) { this.isSearchable = this.descToken.isSearchable(); }
            if (toDescribe.contains(ObjectProp.IS_UNDELETABLE)) { this.isUndeletable = this.descToken.isUndeletable(); }
            if (toDescribe.contains(ObjectProp.IS_UPDATEABLE)) { this.isUpdateable = this.descToken.isUpdateable(); }
            if (toDescribe.contains(ObjectProp.KEY_PREFIX)) { this.keyPrefix = this.descToken.getKeyPrefix(); }
            if (toDescribe.contains(ObjectProp.LABEL)) { this.label = this.descToken.getLabel(); }
            if (toDescribe.contains(ObjectProp.LABEL_PLURAL)) { this.labelPlural = this.descToken.getLabelPlural(); }
            this.hasProps.addAll(toDescribe);
            return this;
        }
        public Set<ObjectProp> missingProps(Set<ObjectProp> toDescribe) {
            Set<ObjectProp> missing = new Set<ObjectProp>();
            for (ObjectProp prop : toDescribe) {
                if (!this.hasProps.contains(prop)) {
                    missing.add(prop);
                }
            }
            return missing;
        }
        private void mergeFields(List<FieldInfo> infos) {
            for (FieldInfo info : infos) {
                FieldInfo existingInfo = fields.get(info.Name);
                if (existingInfo == null) {
                    this.fields.put(info.Name, info);
                } else {
                    fieldInfoAddProps(existingInfo, info.hasProps);
                }
            }
        }
        private void mergeObj(ObjectInfo sameObject) {
            this.addProps(sameObject.hasProps);
            this.mergeFields(sameObject.fields.values());
            // for fieldSets, all properties are automatically provided;
            // any FieldSetInfo is a complete fieldSetInfo (contrast with FieldInfo)
            for (String fieldSetName : sameObject.fieldSets.keySet()) {
                if (!this.fieldSets.containsKey(fieldSetName)) {
                    this.fieldSets.put(fieldSetName,
                        sameObject.fieldSets.get(fieldSetName));
                }
            }
        }
    }

    public Enum ObjectProp {
        IS_ACCESSIBLE,
        IS_CREATEABLE,
        IS_DELETABLE,
        IS_FEED_ENABLED,
        IS_MERGEABLE,
        IS_QUERYABLE,
        IS_SEARCHABLE,
        IS_UNDELETABLE,
        IS_UPDATEABLE,
        KEY_PREFIX,
        LABEL,
        LABEL_PLURAL
    }

    public class PicklistEntryInfo {
        @AuraEnabled public String label { get; set; }
        @AuraEnabled public Boolean isActive { get; set; }
        @AuraEnabled public Boolean isDefaultValue { get; set; }
        @AuraEnabled public String value { get; set; }
    }

    @AuraEnabled public Map<String, ObjectInfo> objects { get {
        if (objects == null) {
            objects = new Map<String, ObjectInfo>();
        }
        return objects;
    } set; }

    public static Set<FieldProp> BASELINE_FIELD_READS {
        get {
            if (BASELINE_FIELD_READS == null) {
                BASELINE_FIELD_READS = new Set<FieldProp>();
                BASELINE_FIELD_READS.addAll(new Set<FieldProp>{
                    FieldProp.DISPLAY_TYPE,
                    FieldProp.INLINE_HELP_TEXT,
                    FieldProp.IS_ACCESSIBLE,
                    FieldProp.LABEL,
                    FieldProp.PICKLIST_VALUES
                });
            }
            return BASELINE_FIELD_READS;
        }
        set;
    }
    public static Set<FieldProp> BASELINE_FIELD_WRITES {
        get {
            if (BASELINE_FIELD_WRITES == null) {
                BASELINE_FIELD_WRITES = BASELINE_FIELD_READS.clone();
                BASELINE_FIELD_WRITES.addAll(new Set<FieldProp>{
                    FieldProp.IS_CASE_SENSITIVE,
                    FieldProp.IS_CREATEABLE,
                    FieldProp.IS_DEFAULTED_ON_CREATE,
                    FieldProp.IS_DEPENDENT_PICKLIST,
                    FieldProp.IS_NILLABLE,
                    FieldProp.IS_RESTRICTED_DELETE,
                    FieldProp.IS_RESTRICTED_PICKLIST,
                    FieldProp.IS_UNIQUE,
                    FieldProp.IS_UPDATEABLE,
                    FieldProp.LENGTH
                });
            }
            return BASELINE_FIELD_WRITES;
        }
        set;
    }

    public static Set<ObjectProp> OBJECT_CAPABILITIES {
        get {
            if (OBJECT_CAPABILITIES == null) {
                OBJECT_CAPABILITIES = new Set<ObjectProp>{
                    ObjectProp.IS_ACCESSIBLE,
                    ObjectProp.IS_CREATEABLE,
                    ObjectProp.IS_DELETABLE,
                    ObjectProp.IS_FEED_ENABLED,
                    ObjectProp.IS_MERGEABLE,
                    ObjectProp.IS_QUERYABLE,
                    ObjectProp.IS_SEARCHABLE,
                    ObjectProp.IS_UNDELETABLE,
                    ObjectProp.IS_UPDATEABLE
                };
            }
            return OBJECT_CAPABILITIES;
        }
        set;
    }
    public LightningDescribe() {
    }
    public LightningDescribe mergeDescribe(LightningDescribe otherDescribe) {
        this.addObjects(otherDescribe.objects.values());
        return this;
    }
    public LightningDescribe addObjects(List<ObjectInfo> objInfos) {
       for (ObjectInfo objInfo : objInfos) {
           this.addObject(objInfo);
       }
       return this;
    }
    public LightningDescribe addObject(ObjectInfo objInfo) {
        ObjectInfo existingInfo = this.objects.get(objInfo.Name);
        if (existingInfo == null) {
            this.objects.put(objInfo.Name, objInfo);
        } else {
            existingInfo.mergeObj(objInfo);
        }
        return this;
    }

    // because the idiomatic way to return schema info is within
    // the hierarchy of a LightningDescribe instance,
    // exposing these constructors could be confusing
    // as end users would not know whether they were expected to have a use
    // case for them.

    private static FieldInfo fieldInfoFactory(SobjectField sobjField) {
        FieldInfo fInfo = new FieldInfo();
        fInfo.descToken = sobjField.getDescribe();
        fInfo.name = fInfo.descToken.getName();
        fInfo.hasProps = new Set<FieldProp>();
        return fInfo;
    }
    private static void fieldInfoAddProps(FieldInfo fInfo, Set<FieldProp> requestedToDescribe) {
        Set<FieldProp> toDescribe = fieldInfoMissingProps(fInfo, requestedToDescribe);
        if (toDescribe.contains(FieldProp.DISPLAY_TYPE)) { fInfo.displayType = fInfo.descToken.getType().Name(); }
        if (toDescribe.contains(FieldProp.LABEL)) { fInfo.label = fInfo.descToken.getLabel(); }
        if (toDescribe.contains(FieldProp.INLINE_HELP_TEXT)) { fInfo.inlineHelpText = fInfo.descToken.getInlineHelpText(); }
        if (toDescribe.contains(FieldProp.PICKLIST_VALUES) && fInfo.descToken.getType() == Schema.DisplayType.Picklist) {
            fInfo.picklistValues = new List<PicklistEntryInfo>();
            for (Schema.PicklistEntry ple : fInfo.descToken.getPicklistValues()) {
                fInfo.picklistValues.add(picklistEntryInfoFactory(ple));
            }
        }
        if (toDescribe.contains(FieldProp.Length) && fInfo.descToken.getType() == Schema.DisplayType.String) {
            fInfo.length = fInfo.descToken.getLength();
        }
        if (toDescribe.contains(FieldProp.IS_ACCESSIBLE)) { fInfo.isAccessible = fInfo.descToken.isAccessible(); }
        if (toDescribe.contains(FieldProp.IS_CASE_SENSITIVE)) { fInfo.isCaseSensitive = fInfo.descToken.isCaseSensitive(); }
        if (toDescribe.contains(FieldProp.IS_CREATEABLE)) { fInfo.isCreateable = fInfo.descToken.isCreateable(); }
        if (toDescribe.contains(FieldProp.IS_DEFAULTED_ON_CREATE)) { fInfo.isDefaultedOnCreate = fInfo.descToken.isDefaultedOnCreate(); }
        if (toDescribe.contains(FieldProp.IS_DEPENDENT_PICKLIST)) { fInfo.isDependentPicklist = fInfo.descToken.isDependentPicklist(); }
        if (toDescribe.contains(FieldProp.IS_NILLABLE)) { fInfo.isNillable = fInfo.descToken.isNillable(); }
        if (toDescribe.contains(FieldProp.IS_RESTRICTED_DELETE)) { fInfo.isRestrictedDelete = fInfo.descToken.isRestrictedDelete(); }
        if (toDescribe.contains(FieldProp.IS_RESTRICTED_PICKLIST)) { fInfo.isRestrictedPicklist = fInfo.descToken.isRestrictedPicklist(); }
        if (toDescribe.contains(FieldProp.IS_UNIQUE)) { fInfo.isUnique = fInfo.descToken.isUnique(); }
        if (toDescribe.contains(FieldProp.IS_UPDATEABLE)) { fInfo.isUpdateable = fInfo.descToken.isUpdateable(); }
        fInfo.hasProps.addAll(toDescribe);
    }
    private static Set<FieldProp> fieldInfoMissingProps(FieldInfo fInfo, Set<FieldProp> toDescribe) {
        Set<FieldProp> missing = new Set<FieldProp>();
        for (FieldProp prop : toDescribe) {
            if (!fInfo.hasProps.contains(prop)) {
                missing.add(prop);
            }
        }
        return missing;
    }
    private static FieldSetInfo fieldSetInfoFactory(Schema.FieldSet fieldSetVal) {
        FieldSetInfo fSetInfo = new FieldSetInfo();
        fSetInfo.name = fieldSetVal.getName();
        fSetInfo.label = fieldSetVal.getLabel();
        fSetInfo.description = fieldSetVal.getDescription();
        fSetInfo.namespace = fieldSetVal.getNameSpace();
        fSetInfo.fields = new List<FieldSetMemberInfo>();
        for (Schema.FieldSetMember fSetMember : fieldSetVal.getFields()) {
            fSetInfo.fields.add(fieldSetMemberInfoFactory(fSetMember));
        }
        return fSetInfo;
    }
    private static FieldSetMemberInfo fieldSetMemberInfoFactory(Schema.FieldSetMember fSetMember) {
        FieldSetMemberInfo fSetMemberInfo = new FieldSetMemberInfo();
        fSetMemberInfo.label = fSetMember.getLabel();
        fSetMemberInfo.dbRequired = fSetMember.getDBRequired();
        fSetMemberInfo.required = fSetMember.getRequired();
        fSetMemberInfo.fieldPath = fSetMember.getFieldPath();
        fSetMemberInfo.type = fSetMember.getType().name();
        return fSetMemberInfo;
    }
    private static PicklistEntryInfo picklistEntryInfoFactory(Schema.PicklistEntry ple) {
        PicklistEntryInfo pleInfo = new PicklistEntryInfo();
        pleInfo.label = ple.getLabel();
        pleInfo.isActive = ple.isActive();
        pleInfo.isDefaultValue = ple.isDefaultValue();
        pleInfo.value = ple.getValue();
        return pleInfo;
    }
}