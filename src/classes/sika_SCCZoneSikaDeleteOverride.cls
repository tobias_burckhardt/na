public without sharing class sika_SCCZoneSikaDeleteOverride {
		
	private String retURL;
	private Mix_Material__c theRecord;

	public sika_SCCZoneSikaDeleteOverride(ApexPages.StandardController stdCtrl){
//		ctrl = stdCtrl;
		retURL = ApexPages.currentPage().getParameters().get('retURL');

		Id recordId = stdCtrl.getRecord().Id;
		theRecord = [SELECT Id, Mix__c, Material_Type__c FROM Mix_Material__c WHERE Id = :recordId];

	}
	
	public PageReference doDelete(){
		list<Mix_Material__c> theMMs;

		// if the record being deleted is a cement, set the SCM (if any) quantity to 0.
		if(theRecord.Material_Type__c == 'Cement'){

			// Get the SCM MM from the Mix, if any. (There should be at most 1 SCM per Mix.)
			theMMs = [SELECT Quantity__c, Material_Type__c FROM Mix_Material__c WHERE Material_Type__c = 'SCM' AND Mix__c = :theRecord.Mix__c]; 			
			if(theMMs.size() == 1){
				theMMs[0].Quantity__c = 0;
				update(theMMs[0]);
			} else if(theMMs.size() == 0){
				system.debug('******** This Mix contains no SCM Mix Materials. Skipping the SCM Quantity calculation.');
			} else {
				system.debug(loggingLevel.ERROR, '********ERROR This Mix contains ' + theMMs.size() + ' SCM Mix Materials. The max allowed is 1.');
			}
		}

		// delete the reocrd
		delete(theRecord);
			
		// return the user to the page from which the delete action was started
		PageReference p = new PageReference(retURL);
		p.setRedirect(true);
		return p;
	}

}