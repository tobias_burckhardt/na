@isTest
public class PricebookSharingDetailsControllerTest {
    static testMethod void testFunctionalities(){
        
        Id accountId = PricingTestDataFactory.createAccount();
        Id contactId = PricingTestDataFactory.createPrimaryContactForAccount(accountId);
        List<Product2> products = PricingTestDataFactory.createProducts();
        System.debug('products--'+products);
        List<String> productIds = new List<String>();
        for(Product2 product:products){
            productIds.add(product.Id);
        }
        Id customPricebookId = PricingTestDataFactory.createCustomPricebook();
        //System.debug('customPricebookId--'+customPricebookId);
        Boolean areProductsAddedToStdPriceBook = PricingTestDataFactory.addProductsToStandardPriceBook(productIds);
        //System.debug('areProductsAddedToStdPriceBook--'+areProductsAddedToStdPriceBook);
        List<PricebookEntry> insertedPricebookEntries = PricingTestDataFactory.addProductsToCustomPriceBook(productIds,customPricebookId);
        
        Id accountPriceBookEntryId = PricingTestDataFactory.createAccountPriceBookEntry(accountId,contactId,customPricebookId);
        //System.debug('accountPriceBookEntryId--'+accountPriceBookEntryId);
        PricingTestDataFactory.updatePriceBookEntry(insertedPricebookEntries.get(0).Id);
        Test.startTest();
        	PricebookSharingDetailsController controllerObj = new PricebookSharingDetailsController();
        	System.assertNotEquals(null, controllerObj);
            String ajaxData = '{"productNameOrArticleCode":"'+products.get(0).Name+'","selectedPricebookId":"'+customPricebookId+'","accountPriceBookEntryId":"'+accountPriceBookEntryId+'","OffsetSize":0}';
            System.assertNotEquals(null, PricebookSharingDetailsController.getPriceBookEntriesBySelectedPriceBook(ajaxData));
        	ajaxData = '{"deselectedArray":[],"selectedPricebookId":"'+customPricebookId+'","accountPriceBookEntryId":"'+accountPriceBookEntryId+'","isDataRetrievalFromPBEntry":true}';
            System.assertNotEquals(null, PricebookSharingDetailsController.savePriceBookEntriesWithStatus(ajaxData));
        	ajaxData = '{"deselectedArray":["'+insertedPricebookEntries.get(0).Id+'"],"selectedPricebookId":"'+customPricebookId+'","accountPriceBookEntryId":"'+accountPriceBookEntryId+'","isDataRetrievalFromPBEntry":false}';
            System.assertNotEquals(null, PricebookSharingDetailsController.savePriceBookEntriesWithStatus(ajaxData));
            ajaxData = '{"productNameOrArticleCode":"'+products.get(0).Name+'","selectedPricebookId":"'+customPricebookId+'","accountPriceBookEntryId":"'+accountPriceBookEntryId+'","OffsetSize":0}';
            System.assertNotEquals(null, PricebookSharingDetailsController.getPriceBookEntriesBySelectedPriceBook(ajaxData));
        	ajaxData = '{"selectedPricebookId":"'+customPricebookId+'","accountPriceBookEntryId":"'+accountPriceBookEntryId+'"}';
            System.assertNotEquals(null, PricebookSharingDetailsController.getUpdatedOrNewlyCreatedEntries(ajaxData));
        	ajaxData = '{"selectedPricebookId":"'+customPricebookId+'","accountPriceBookEntryId":"'+accountPriceBookEntryId+'"}';
            System.assertNotEquals(null, PricebookSharingDetailsController.refreshPriceBookEntriesWithStatus(ajaxData));
        	
        Test.stopTest();
    }
}