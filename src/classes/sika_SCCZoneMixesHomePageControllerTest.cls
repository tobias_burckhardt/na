@isTest
private class sika_SCCZoneMixesHomePageControllerTest {
	private static sika_SCCZoneMixesHomePageController ctrl;
	private static PageReference pageRef;

	private static User SCCZoneUser1;
	private static User SCCZoneUser2;
	private static Account theAccount;
	
	private static list<Mix__c> theMixes;
	private static list<Mix__c> user1Mixes;
	private static list<Mix__c> user2Mixes;

	private static Integer i;


	private static void init(){
		// bypass the triggers we won't need to have firing during these tests
		sika_SCCZoneMixTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMixTriggerHelper.bypassMixOwnerTransferCheck = true;
		sika_SCCZoneMixTriggerHelper.bypassMixChatterAlert = true;
		sika_SCCZoneMixTriggerHelper.bypassUpdateSCMQuantity = true;
	
		// create two SCC Zone users, both associated with the same account
		map<String, sObject> objects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
        SCCZoneUser1 = (User) objects.get('SCCZoneUser');
        theAccount = (Account) objects.get('theAccount');
        																		
        map<String, sObject> moreObjects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
        SCCZoneUser2 = (User) moreObjects.get('SCCZoneUser');
        
        // create a few mixes for each user. (No need to create mix_materials at this point.)
        theMixes = new list<Mix__c>();
        user1Mixes = new list<Mix__c>();
        user2Mixes = new list<Mix__c>();
        
        
        system.runAs(SCCZoneUser1){
	        for(i=3; i > 0; i--){
	        	user1Mixes.add(sika_SCCZoneTestFactory.createMix(new map<String, String>{'ownerId' => SCCZoneUser1.Id,
	        																		   'mixName' => 'Mix_' + i,
	        																		   'accountId' => theAccount.Id}));
	        }
        	insert(user1Mixes);
        }
        
        system.runAs(SCCZoneUser1){
	        for(i=3; i > 0; i--){
	        	user2Mixes.add(sika_SCCZoneTestFactory.createMix(new map<String, String>{'ownerId' => SCCZoneUser2.Id,
	        																		   'mixName' => 'Mix_' + i,
	        																		   'accountId' => theAccount.Id}));
	        }
        	insert(user2Mixes);
        }
        theMixes.addAll(user1Mixes);
        theMixes.addAll(user2Mixes);
        
	
		// set page we'll be testing
        pageRef = new PageReference('/sika_SCCZoneMixesHomePage');
        pageRef.getParameters().put('view', 'me');
        Test.setCurrentPage(pageRef);
        
        // create an instance of the controller
        ctrl = new sika_SCCZoneMixesHomePageController();
	}
	
	
//TODO: Update this test after we make the MixesHomePage default to "view my mixes" instead of...blowing up.	
/*
	@isTest static void testLoadActionBad(){
		// set page we'll be testing, without setting a value for 'view'
        pageRef = new PageReference('/sika_SCCZoneMixesHomePage');
        Test.setCurrentPage(pageRef);
        
        // create an instance of the controller
        ctrl = new sika_SCCZoneMixesHomePageController();

        ctrl..........
	}
*/	
	
	
	@isTest static void testUpdateListView(){
		init();
		system.runAs(SCCZoneUser1){
			// affirm that the page view parameter is set to "me" instead of "comp" (i.e., "my mixes" instead of "my company's mixes")
			system.assertEquals(ApexPages.CurrentPage().getParameters().get('view'), 'me');
			
			// change the picklist value to "my company's mixes"
			ctrl.selectOption = 'comp';
			pageRef = ctrl.optionUpdate();
			
			// affirm that the pagereference returned has the view set to show my company's mixes
			system.assertEquals(pageRef.getParameters().get('view'), 'comp');
			system.assertEquals(ctrl.selectOption, 'comp');
		}
		
	}
	
	
	@isTest static void testMixListDisplay(){
		init();
		system.runAs(SCCZoneUser1){
			// instantiate the page controller
		    ctrl = new sika_SCCZoneMixesHomePageController();
		    
			// confirm that the myMixes list contains only the currrent user (SCCZoneUser1)'s mixes
		    for(Mix__c m : ctrl.myMixes){
		    	system.assert(m.OwnerId == SCCZoneUser1.Id);
		    }
		}
		system.runAs(SCCZoneUser2){
			ctrl = new sika_SCCZoneMixesHomePageController();
		    
			// confirm that the myMixes list contains only the currrent user (SCCZoneUser2)'s mixes
		    for(Mix__c m : ctrl.myMixes){
		    	system.assert(m.OwnerId == SCCZoneUser2.Id);
		    }
		}
	}
	
	
	@isTest static void testSortTable(){
		init();
		String tempString;
		Decimal tempDecimal;
		Boolean tempBoolean;
		
		test.startTest();
		
		System.runAs(SCCZoneUser1){
			// instantiate the page controller
		    ctrl = new sika_SCCZoneMixesHomePageController();

			// sort the list by name. (Should default to sort ascending)
			ctrl.sortField = 'Mix_Name__c';
			ctrl.tableSort();

			// sort by name again. Affirm that the list is now sorted in the other direction. (Descending)
			tempString = ctrl.myMixes[0].Mix_Name__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.myMixes[ctrl.myMixes.size() - 1].Mix_Name__c, tempString);

			// repeat for the other table columns
			ctrl.sortField = 'Project__c';
			ctrl.tableSort();
			tempString = ctrl.myMixes[0].Project__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.myMixes[ctrl.myMixes.size() - 1].Project__c, tempString);

			ctrl.sortField = 'Water_Cement_Ratio_Calculated__c';
			ctrl.tableSort();
			tempDecimal = ctrl.myMixes[0].Water_Cement_Ratio_Calculated__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.myMixes[ctrl.myMixes.size() - 1].Water_Cement_Ratio_Calculated__c, tempDecimal);

			ctrl.sortField = 'Concrete_Compressive_Strength_DSP__c';
			ctrl.tableSort();
			tempDecimal = ctrl.myMixes[0].Concrete_Compressive_Strength_DSP__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.myMixes[ctrl.myMixes.size() - 1].Concrete_Compressive_Strength_DSP__c, tempDecimal);
			
			ctrl.sortField = 'Calculated_Raw_Material_Cost__c';
			ctrl.tableSort();
			tempDecimal = ctrl.myMixes[0].Calculated_Raw_Material_Cost__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.myMixes[ctrl.myMixes.size() - 1].Calculated_Raw_Material_Cost__c, tempDecimal);

			ctrl.sortField = 'isLocked__c';
			ctrl.tableSort();
			tempBoolean = ctrl.myMixes[0].isLocked__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.myMixes[ctrl.myMixes.size() - 1].isLocked__c, tempBoolean);

		}
		test.stopTest();

	}
	
	
	@isTest static void testNewMixButton(){
		init();
		system.runAs(SCCZoneUser1){
			// instantiate the page controller
			ctrl = new sika_SCCZoneMixesHomePageController();
		
			// confirm that we're on the Mixes homepage
			system.assert(ApexPages.CurrentPage().getURL().contains('/sika_SCCZoneMixesHomePage'));
			
			// set the system-of-measure (SOM) for the new mix. (This would be selected by the user in the modal div ("popup") that's displayed before redirecting to the new mix page.)
			ctrl.newMix.System_Of_Measure__c = 'Metric (SI)';
			
			PageReference newMixPageRef = ctrl.createNewMix();

			// confirm ...
			system.assert(newMixPageRef.getURL().contains('/sika_SCCZoneNewMixPage'));
			system.assertEquals(newMixPageRef.getParameters().get('metric'), 'Metric (SI)');
			
			// re-load the mixes homepage
			system.assert(ApexPages.CurrentPage().getURL().contains('/sika_SCCZoneMixesHomePage'));
			
			ctrl.mixID = ctrl.myMixes[0].Id;
			
			newMixPageRef = ctrl.createNewMix();

			// confirm ...
			system.assert(newMixPageRef.getURL().contains('/sika_SCCZoneNewMixPage'));
			
		}
		
	}
	//cancelNewMix
	
	@isTest static void testCancelMixButton(){
		init();
		system.runAs(SCCZoneUser1){
			// instantiate the page controller
			ctrl = new sika_SCCZoneMixesHomePageController();
		
			// confirm that we're on the Mixes homepage
			system.assert(ApexPages.CurrentPage().getURL().contains('/sika_SCCZoneMixesHomePage'));
			
			// set the mixId of the current mix
			ctrl.mixID = ctrl.myMixes[0].Id;
			
			PageReference newMixPageRef = ctrl.cancelNewMix();

			// confirm ...
			system.assert(newMixPageRef.getURL().contains('/sika_SCCZoneMixDetailsPage'));
			
			// re-load the mixes homepage
			system.assert(ApexPages.CurrentPage().getURL().contains('/sika_SCCZoneMixesHomePage'));
			
			ctrl.mixID = null;
			
			newMixPageRef = ctrl.cancelNewMix();

			// confirm ...
			system.assert(newMixPageRef.getURL().contains('/sika_SCCZoneMixesHomePage'));
			
		}
		
	}
	
	
	@isTest static void testDeleteMix(){
		init();
		system.runAs(SCCZoneUser1){
			ctrl = new sika_SCCZoneMixesHomePageController();
			Id mixToDelete = ctrl.myMixes[0].Id;
			ctrl.deleteThisMix = mixToDelete;
			Test.startTest();
			pageRef = ctrl.deleteMix();
			Test.stopTest();
			
			// re-load the page
			ctrl = new sika_SCCZoneMixesHomePageController();
			
			// confirm that the Mix is no longer in the list
			set<Id> remainingMixes = new set<Id>();
			for(Mix__c m : ctrl.theMixes){
				remainingMixes.add(m.Id);
			}
			system.assertEquals(remainingMixes.contains(mixToDelete), false);
			
		}
	}
	

	@isTest static void testLoadAction(){
        User guestUser = sika_SCCZoneTestFactory.createGuestUser();
        insert(guestUser);

        PageReference p;

        System.runAs(guestUser){
            ctrl = new sika_SCCZoneMixesHomePageController();
            p = ctrl.loadAction();
            system.assert(String.valueOf(p).contains('/SCCZone/sika_SCCZoneLoginPage'));
        }
    }

}