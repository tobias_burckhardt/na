/**
* @author Garvit Totuka
* @date 18 Sep 2017
*
* @group user Sharing Management
*
* @description User Trigger Test class to functionalities in Full User Trigger and User Trigger Handler class
*/
@isTest
private class FullUserTriggerTest {

	static final Integer RECORD_COUNT = 1;
	static final String PROFILE_NAME = 'PTS Partner Community';
	static final String OPPORTUNITY_ACCESS_TYPE = 'Edit';

	static List<Account> accounttList;
	static List<Apttus_Config2__PriceList__c> priceList;
	static List<Opportunity> opportunityList;
	static List<Contact> contactList;
	static User testUser;


	@isTest static void insertUserTest() {
		setupData();
		testUser = TestingUtils.createTestUser('garvittest',PROFILE_NAME);
			testUser.ContactId = contactList[0].Id;
		Test.startTest();
			insert testUser;
		Test.stopTest();
		List<OpportunityShare> opptyShare = [SELECT Id FROM OpportunityShare WHERE OpportunityId = :opportunityList[0].Id];
		System.assert(!opptyShare.isEmpty(), 'OpportunityShare should be created');
	}

	@isTest static void updateUserToInactiveTest() {
		setupData();
		List<Profile> adminProf = [SELECT id,Name FROM Profile WHERE Profile.Name = 'System Administrator'];
		List<User> adminUser = [SELECT Id FROM User WHERE ProfileId IN :adminProf AND isActive=true];

		testUser = TestingUtils.createTestUser('garvittest',PROFILE_NAME);
			testUser.ContactId = contactList[0].Id;
			insert testUser;

		Test.startTest();
			//stop test used to trigger future calls
		Test.stopTest();

		System.runAs(adminUser[0]){
				testUser.IsActive = false;
				update testUser;
			}
		List<OpportunityShare> opptyShare = [SELECT Id FROM OpportunityShare WHERE UserOrGroupId = :testUser.Id AND RowCause = :'Manual'];
		System.assert(opptyShare.isEmpty(), 'OpportunityShare should be deleted');
	}

	static void setupData() {
		accounttList = TestingUtils.createAccounts('test Account',RECORD_COUNT, true);
		priceList = TestingUtils.createPriceList('Sika Test Price List',RECORD_COUNT,true);
		opportunityList = TestingUtils.createOpportunities(RECORD_COUNT, accounttList[0].Id, false);
			opportunityList[0].Price_List__c = priceList[0].Id;
			insert opportunityList;
		contactList = TestingUtils.createContacts(accounttList[0].Id, RECORD_COUNT, true);
	}

}