@isTest
private class ContentDocumentLinkTriggerTest {
    private static final String RPA = 'RPA';

    @testSetup
    static void setup() {
        AWS_Settings__c awsSetting = new AWS_Settings__c(AWS_Key__c = 'Test', S3_Bucket_Name__c = 'Test', SF_Library_Name__c = 'Test');
        insert awsSetting;
        
        User sysAdmin = TestingUtils.createTestUser('TestRPA', 'System Administrator');
        User testUser1 = TestingUtils.createTestUser('TestRPA1', 'Standard User');
        User testUser2 = TestingUtils.createTestUser('TestRPA2', 'Standard User');
        insert new List<User>{testUser1, testUser2, sysAdmin};

        // Run as System Admin to avoid Mixed DML Exception
        System.runAs(sysAdmin) {
            Employee__c testEmployee1 = TestingUtils.createEmployee('TestRPA1', RPA);
            testEmployee1.SFDC_User_Account__c = testUser1.Id;
            testEmployee1.SFDC_User__c = true;
            Employee__c testEmployee2 = TestingUtils.createEmployee('TestRPA2', RPA);
            testEmployee2.SFDC_User_Account__c = testUser2.Id;
            testEmployee2.SFDC_User__c = true;
            insert new List<Employee__c>{testEmployee1, testEmployee2};

            Request_for_Project_Approval__c approval = TestingUtils.createTestRequestForPA(Date.today(), false);
            approval.Project_Manager__c = testEmployee1.Id;
            approval.Local_Controller__c = testEmployee2.Id;
            insert approval;
        }

    }

    static testmethod void testCreateContentDocumentLink() {
        ContentVersion version = new ContentVersion(
            Title = 'TestRPA',
            PathOnClient = 'test.rpa',
            VersionData = Blob.valueOf('TestRPA'),
            IsMajorVersion = true
        );
        insert version;

        Request_for_Project_Approval__c approval = [
            SELECT Project_Manager__c, Local_Controller__c, Project_Manager__r.SFDC_User_Account__c,
                   Local_Controller__r.SFDC_User_Account__c
            FROM Request_for_Project_Approval__c
            LIMIT 1
        ];

        version = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :version.Id];

        ContentDocumentLink rpaLink = new ContentDocumentLink(
            LinkedEntityId = approval.Id,
            ContentDocumentId = version.ContentDocumentId,
            ShareType = 'V',
            Visibility = 'InternalUsers'
        );

        Test.startTest();
        insert rpaLink;
        // For some weird reason Triggers do not run on the ContentDocumentLink in tests, so we will
        // just force the after insert manually
        ContentDocumentLinkTriggerHandler.handleAfterInsert(new List<ContentDocumentLink>{rpaLink});
        Test.stopTest();

        Set<Id> employeeIds = new Set<Id>{
            approval.Project_Manager__r.SFDC_User_Account__c,
            approval.Local_Controller__r.SFDC_User_Account__c
        };

        List<ContentDocumentLink> links = [
            SELECT ContentDocumentId, LinkedEntityId
            FROM ContentDocumentLink
            WHERE LinkedEntityId IN :employeeIds
        ];

        System.assertEquals(2, links.size(), 'It should have created two links.');
        for (ContentDocumentLink link : links) {
            System.assert(employeeIds.contains(link.LinkedEntityId),
                'It should have created one link per user.');
            System.assertEquals(version.ContentDocumentId, link.ContentDocumentId,
                'It should have created a link to the right ContentDocument record.');
        }
    }
}