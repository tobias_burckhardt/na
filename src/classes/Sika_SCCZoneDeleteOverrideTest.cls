@isTest
private class Sika_SCCZoneDeleteOverrideTest {
	private static sika_SCCZoneSikaDeleteOverride ctrl;
	private static PageReference pageRef;
	private static User salesUser;
	private static User adminUser;
	private static Mix__c theMix;
	private static list<Mix_Material__c> theMMs;

	private static void init(){

        String cementQty = '100';
        String cementUnit = 'lbs';
        Decimal mixSCMRatio = 5;

    	// create materials...
        list<Material__c> theMaterials = new list<Material__c>();
        theMaterials.add(sika_SCCZoneTestFactory.createMaterialWater());
        theMaterials.add(sika_SCCZoneTestFactory.createMaterialSand());
        theMaterials.add(sika_SCCZoneTestFactory.createMaterialCement());
        theMaterials.add(sika_SCCZoneTestFactory.createMaterialSCM());
        
        insert(theMaterials);
    	
    	// create a mix...
    	theMix = sika_SCCZoneTestFactory.createMix();//new map<String, String>{'ownerId' => salesUser.Id});
    	theMix.Target_SCM_Ratio__c = mixSCMRatio;
    	insert(theMix);

    	// add materials to the mix
    	theMMs = new list<Mix_Material__c>();
    	
    	for(Material__c m : theMaterials){
    		if(m.Type__c == 'Cement'){
    			theMMs.add(sika_SCCZoneTestFactory.createMixMaterial(theMix, m, new map<String, String>{'quantity' => cementQty, 'unit' => cementUnit}));
    		} else {
    			theMMs.add(sika_SCCZoneTestFactory.createMixMaterial(theMix, m));
    		}
    	}
    	insert(theMMs);

	}


	@isTest static void testDelete(){
		init();
		theMMs = [SELECT Material_Type__c FROM Mix_Material__c];

		pageRef = new PageReference('/sika_SCCZoneDeleteOverride');
        pageRef.getParameters().put('retURL', theMix.Id);
        Test.setCurrentPage(pageRef);
		Test.startTest();

		// confirm that there are 4 MMs in the database
		system.assertEquals(theMMs.size(), 4);

		// delete one non-cement mix material -- the Sand mix material, in this case.
		for(Mix_Material__c mm : theMMs){
			if(mm.Material_Type__c == 'Sand'){
				// instantiate the standard Mix_Material page controller, passing in the Sand Mix_Material record
				ApexPages.StandardController stdCtrl = new ApexPages.StandardController(mm);
				// instantiate the controller extension
				ctrl = new sika_SCCZoneSikaDeleteOverride(stdCtrl);
				// run the delete method
				ctrl.doDelete();
				break;
			}
		}

		Boolean noSand = true;
		Decimal scmQty;

		// confirm that the Sand MM has been deleted. Also get the quantity of the SCM MM.
		theMMs = [SELECT Quantity__c, Material_Type__c FROM Mix_Material__c];

		for(Mix_Material__c mm : theMMs){
			if(mm.Material_Type__c == 'Sand'){
				noSand = false;
			}
			if(mm.Material_Type__c == 'SCM'){
				scmQty = mm.Quantity__c;
			}
		}
		system.assertEquals(noSand, true);

		// confirm that the SCM MM is something other than 0
		system.assert(scmQty != 0);

		
		// delete the cement MM
		for(Mix_Material__c mm : theMMs){
			if(mm.Material_Type__c == 'Cement'){
				ApexPages.StandardController stdCtrl = new ApexPages.StandardController(mm);
				ctrl = new sika_SCCZoneSikaDeleteOverride(stdCtrl);
				ctrl.doDelete();
				break;
			}
		}

		Boolean noCement = true;

		// confirm that the Cement MM has been deleted
		theMMs = [SELECT Quantity__c, Material_Type__c FROM Mix_Material__c];

		for(Mix_Material__c mm : theMMs){
			if(mm.Material_Type__c == 'Cement'){
				noCement = false;
			}
			if(mm.Material_Type__c == 'SCM'){
				scmQty = mm.Quantity__c;
			}
		}
		system.assertEquals(noCement, true);

		// confirm that the SCM mix material now has a quantity of 0
		system.assertEquals(scmQty, 0);

		Test.stopTest();
	}

}