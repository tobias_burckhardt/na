@isTest
private class sika_SCCZoneMixMaterialsController2Test {
	private static sika_SCCZoneMixMaterialsController2 ctrl;
	private static PageReference pageRef;

	private static User SCCZoneUser;
	private static User adminUser;
	private static User admixtureOwner;
	private static Account theAccount;
	private static Mix__c theMix;
	private static list<Material__c> theMaterials;
	
	private static map<String, String> materialNames_to_IDs = new map<String, String>();
	private static map<String, String> materialType_to_IDs = new map<String, String>();
	
	private static Integer i;


	private static void init(){

		map<String, sObject> objects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
        SCCZoneUser = (User) objects.get('SCCZoneUser');
     	admixtureOwner = (User) objects.get('salesUser');
        adminUser = (User) objects.get('adminUser');
        theAccount = (Account) objects.get('theAccount');
        
		// bypass the triggers that we don't need to fire during these tests
		bypassAllTriggers();

		system.runAs(SCCZoneUser){
			// create a mix
			theMix = sika_SCCZoneTestFactory.createMix();
			insert(theMix);
	
			// create some materials
			Material__c tempMaterial;
			list<Material__c> materialsToInsert = new list<Material__c>();
			String makeThisType;

			// three of each...except admixtures
			makeThisType = 'Cement';
			for(i=3; i > 0; i--){
				if(makeThisType == 'Cement'){
					tempMaterial = sika_SCCZoneTestFactory.createMaterialCement();
					if(i == 1) { 
						makeThisType = 'SCM';
						i = 4; 
					}
				} else if(makeThisType == 'SCM'){
					tempMaterial = sika_SCCZoneTestFactory.createMaterialSCM();
					if(i == 1) { 
						makeThisType = 'Sand';
						i = 4; 
					}
				} else if(makeThisType == 'Sand'){
					tempMaterial = sika_SCCZoneTestFactory.createMaterialSand();
					if(i == 1) { 
						makeThisType = 'Coarse Aggregate';
						i = 4; 
					}
				} else if(makeThisType == 'Coarse Aggregate'){
					tempMaterial = sika_SCCZoneTestFactory.createMaterialCoarseAgg();
					if(i == 1) { 
						makeThisType = 'Water';
						i = 4; 
					}
				} else if(makeThisType == 'Water'){
					tempMaterial = sika_SCCZoneTestFactory.createMaterialWater();
					if(i == 1) { 
						makeThisType = 'Other';
						i = 4; 
					}
				} else if(makeThisType == 'Other'){
					tempMaterial = sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'type' => 'Other'});
				}
				tempMaterial.Material_Name__c = makeThisType + ' ' + (i-1);
				materialsToInsert.add(tempMaterial);
	
			}
			insert(materialsToInsert);
			
		}

		// create a couple admixture materials.  (These are owned by Sika (admin, in this case), and available to anybody; they aren't associated with any one SCC Zone user)
		system.runAs(adminUser){
			Material__c tempMaterial;
			Material__c admixtureMaterial1 = tempMaterial = sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'type' => 'Admixture', 'material_name' => 'Admixture 1'});
			admixtureMaterial1.Cost__c = null;
			admixtureMaterial1.Unit__c = null;
			
			Material__c admixtureMaterial2 = tempMaterial = sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'type' => 'Admixture', 'material_name' => 'Admixture 2'});
			admixtureMaterial2.Cost__c = null;
			admixtureMaterial2.Unit__c = null;
			
			insert(admixtureMaterial1);
			insert(admixtureMaterial2);
		}
		
		// to make it easier to keep track of the materials we're wokring with, we'll create a map<Material Name => Material Id> and another map<Material_type => Material ID>
		map<Id, Material__c> materialIDs_to_Materials = new map<Id, Material__c>();
		list<Material__c> tempMatList = [SELECT Id, Material_Name__c, Cost__c, Type__c FROM Material__c];
		for(Material__c m : tempMatList){
			materialIDs_to_Materials.put(m.Id, m);
		}
		materialNames_to_IDs = new map<String, String>();
		materialType_to_IDs = new map<String, String>();

		for(String matId : materialIDs_to_Materials.keySet()){
			materialNames_to_IDs.put(materialIDs_to_Materials.get(matId).Material_Name__c, matId);
			materialType_to_IDs.put(materialIDs_to_Materials.get(matId).Type__c, matId);
		}

		// associate some materials with the mix. (We'll go with Water 1, Sand 1, and Coarse Aggregate 1)
		system.runAs(SCCZoneUser){
			list<Mix_Material__c> theMMs = new list<Mix_Material__c>();
			theMMs.add(sika_SCCZoneTestFactory.createMixMaterial(theMix, materialIDs_to_Materials.get(materialNames_to_IDs.get('Water 1'))));
			theMMs.add(sika_SCCZoneTestFactory.createMixMaterial(theMix, materialIDs_to_Materials.get(materialNames_to_IDs.get('Sand 1'))));
			theMMs.add(sika_SCCZoneTestFactory.createMixMaterial(theMix, materialIDs_to_Materials.get(materialNames_to_IDs.get('Coarse Aggregate 1'))));
		
			insert(theMMs);
		}
		
		// set page we'll be testingselectedMixMaterials
        pageRef = new PageReference('/sika_SCCZoneManageMixMaterialsPage2');
        pageRef.getParameters().put('mixId', theMix.Id);
        Test.setCurrentPage(pageRef);

	}


	@isTest static void testLoad(){
		init();
		test.startTest();

		System.runAs(SCCZoneUser){
			// set page we'll be testing
	        pageRef = new PageReference('/sika_SCCZoneManageMixMaterialsPage2');
	        pageRef.getParameters().put('mixId', theMix.Id);
	        Test.setCurrentPage(pageRef);
        
			// instantiate the page controller
		    ctrl = new sika_SCCZoneMixMaterialsController2();

			// affirm that the page loaded. (Didn't redirect to an error page)
			system.assert(ApexPages.CurrentPage().getURL().contains('/sika_SCCZoneManageMixMaterialsPage2'));
			
			// affirm that the Mix's current Mix_Materials are displayed in the "current mix materials" list
			system.assertEquals(ctrl.selectedMixMaterials.size(), 3);
			
			// confirm that all other materials associated with the current user or current user's company are displayed in the bottom table, if they aren't displayed in the top. (This excludes admixture materials.)
			system.assertEquals(ctrl.wrappedMaterials.size(), 15);
			
			// "click the Cancel button" and return from whence you came!
			system.assert(ctrl.doCancel().getURL().contains('/sika_SCCZoneMixDetailsPage'));

		}
		test.stopTest();

	}
	
	@isTest static void testAddSelectedMaterialHappy(){
		init();
		test.startTest();

		System.runAs(SCCZoneUser){
	        pageRef = new PageReference('/sika_SCCZoneManageMixMaterialsPage2');
	        pageRef.getParameters().put('mixId', theMix.Id);
	        Test.setCurrentPage(pageRef);
        
		    ctrl = new sika_SCCZoneMixMaterialsController2();
		    
		    // add a material to the mix. (We'll add a Coarse Aggregate, since we know adding one won't put us over the allowed limit)
			Id tempId;
			for(sika_SCCZoneMixMaterialsController2.materialWrapper mw : ctrl.wrappedMaterials){
				if(mw.theMaterial.Type__c == 'Coarse Aggregate'){
					mw.pickMe = true;
					tempId = mw.theMaterial.Id;
					break;
				}
			}
		    PageReference p = ctrl.addMixMaterials();
		    
		    // confirm that the material was added to the selectedMixMaterials list
			system.assertEquals(ctrl.selectedMixMaterials.size(), 4);
			Boolean isHere = false;
			for(sika_SCCZoneMixMaterialsController2.mmWrapper m : ctrl.selectedMixMaterials){  // ** Note that we need to reference the wrapper class as a static class; can't use the controller object (ctrl)
				if(m.theMM.Material__c == tempId){
					isHere = true;
				}
			}
		}
		
	}


	@isTest static void testChangeViewFilter(){
		init();
		test.startTest();

		System.runAs(SCCZoneUser){
	        pageRef = new PageReference('/sika_SCCZoneManageMixMaterialsPage2');
	        pageRef.getParameters().put('mixId', theMix.Id);
	        Test.setCurrentPage(pageRef);
        
		    ctrl = new sika_SCCZoneMixMaterialsController2();
		    
		   	Integer count;
			set<String> theTypes = new set<String>{'Cement', 'Water', 'SCM', 'Coarse Aggregate', 'Sand', 'Other'};
			for(String t : theTypes){
				count = 0;
			    ctrl.selectOption = t;
			    ctrl.setWrapper();
			    
				for(sika_SCCZoneMixMaterialsController2.materialWrapper m : ctrl.wrappedMaterials){
			    	if(m.theMaterial.Type__c == t){
			    		count++;
			    		if(count == 3){
			    			break;
			    		}
			    	}
				}
			}
			system.assertEquals(count, 3);
		}
		
	}

	@isTest static void testAddAndSaveMixMaterials(){
		init();
		test.startTest();

		System.runAs(SCCZoneUser){
	        pageRef = new PageReference('/sika_SCCZoneManageMixMaterialsPage2');
	        pageRef.getParameters().put('mixId', theMix.Id);
	        Test.setCurrentPage(pageRef);
        
		    ctrl = new sika_SCCZoneMixMaterialsController2();
		    
		    // add a material to the mix.
			Id tempId;
			for(sika_SCCZoneMixMaterialsController2.materialWrapper mw : ctrl.wrappedMaterials){
				if(mw.theMaterial.Material_Name__c.contains(' 3')){
					mw.pickMe = true;
				}
			}
		    PageReference p = ctrl.addMixMaterials();
		    
		    // try and save with no required fields filled in. (Fields default to zero, and will validate with zero values, so we need to clear the fields to confirm that the save will fail.)
		    for(sika_SCCZoneMixMaterialsController2.mmWrapper mw : ctrl.selectedMixMaterials){
		    	mw.theMM.Quantity__c = null;
		    	mw.theMM.Unit__c = null;
		    	mw.theMM.Moisture__c = null;
		    }
		    system.assertEquals(ctrl.saveMM(), null);
		    
		    // add required fields and try again
		    for(sika_SCCZoneMixMaterialsController2.mmWrapper mw : ctrl.selectedMixMaterials){
		    	if(mw.quantityRequired){
		    		mw.theMM.Quantity__c = 1;
		    	}	
		    	if(mw.moistureRequired) {
		    		mw.theMM.Moisture__c = 1;
		    	}
		    	if(mw.unitRequired){
		    		mw.theMM.Unit__c = 'testUnit';
		    	}
		    }
		    system.assert(ctrl.saveMM().getURL().contains('/SCCZone/apex/sika_SCCZoneMixDetailsPage'));
		}
		    
	}


	@isTest static void testRemoveNewMixMaterial(){
		init();
		test.startTest();

		System.runAs(SCCZoneUser){
	        pageRef = new PageReference('/sika_SCCZoneManageMixMaterialsPage2');
	        pageRef.getParameters().put('mixId', theMix.Id);
	        Test.setCurrentPage(pageRef);
        
		    ctrl = new sika_SCCZoneMixMaterialsController2();
		    
		    // add a material to the mix.
			Id tempId;
			for(sika_SCCZoneMixMaterialsController2.materialWrapper mw : ctrl.wrappedMaterials){
				if(mw.theMaterial.Type__c == 'Water'){
					mw.pickMe = true;
					tempId = mw.theMaterial.Id;
					break;
				}
			}
		    PageReference p = ctrl.addMixMaterials();
		    
		    // confirm that the material was added
		    Boolean isHere = false;
			for(sika_SCCZoneMixMaterialsController2.mmWrapper m : ctrl.selectedMixMaterials){  // ** Note that we need to reference the wrapper class as a static class; can't use the controller object (ctrl)
				if(m.theMM.Material__c == tempId){
					isHere = true;
				}
			}
			
			// remove the material and save
		    ctrl.removeThisMaterial = tempId;
		    ctrl.removeMixMaterial();
		    ctrl.saveMM();
		    
		    // confirm that the material has been removed
			for(sika_SCCZoneMixMaterialsController2.mmWrapper m : ctrl.selectedMixMaterials){
				if(m.theMM.Material__c == tempId){
					isHere = true;
				}
			}
		}
		
	}
	

	@isTest static void testDeleteMixMaterial(){
		init();
		test.startTest();

		System.runAs(SCCZoneUser){
	        pageRef = new PageReference('/sika_SCCZoneManageMixMaterialsPage2');
	        pageRef.getParameters().put('mixId', theMix.Id);
	        Test.setCurrentPage(pageRef);
        
		    ctrl = new sika_SCCZoneMixMaterialsController2();
		    
		    // remove a mix_material that was already associated with the mix.
			Id removeMaterialId = ctrl.selectedMixMaterials[0].theMM.Material__c;
			Id removeMixMaterialId = ctrl.selectedMixMaterials[0].theMM.Id;

			ctrl.removeThisMaterial = removeMaterialId;
			ctrl.removeMixMaterial();
			
			// confirm that the material has been removed from the mmWrapper list
			Boolean isHere = false;
			for(sika_SCCZoneMixMaterialsController2.mmWrapper m : ctrl.selectedMixMaterials){
				if(m.theMM.Material__c == removeMaterialId){
					isHere = true;
				}
			}
			
			system.assertEquals(isHere, false);
			
			// save changes
			PageReference p = ctrl.saveMM();
			system.assert(p.getURL().contains('/SCCZone/apex/sika_SCCZoneMixDetailsPage'));
			system.assertEquals(p.getParameters().get('mixId'), theMix.Id);
			
			list<Mix_Material__c> butNotHere = [SELECT Id FROM Mix_Material__c WHERE Id = :removeMixMaterialId];
			system.assertEquals(butNotHere.size(), 0);
		}
	
	}


	@isTest static void testLoadAction(){

		// test the doAuthCheck portion of loadAction()
        User guestUser = sika_SCCZoneTestFactory.createGuestUser();
        insert(guestUser);
        PageReference p;

        System.runAs(guestUser){
            ctrl = new sika_SCCZoneMixMaterialsController2();
            p = ctrl.loadAction();
            system.assert(String.valueOf(p).contains('/SCCZone/sika_SCCZoneLoginPage'));

        }

        // test the "check for Mix Id" portion of loadAction()
        pageRef = new PageReference('/sika_SCCZoneManageMixMaterialsPage2');
        Test.setCurrentPage(pageRef);

        map<String, sObject> objects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
        SCCZoneUser = (User) objects.get('SCCZoneUser');

        System.runAs(SCCZoneUser){
	        ctrl = new sika_SCCZoneMixMaterialsController2();
	        p = ctrl.loadAction();
	        system.assert(String.valueOf(p).contains('/sika_SCCZoneMixesHomePage?view=me'));
    	}

    }

	
// -----------------------------------------------------------------------------------

//TODO: Check: Not used (yet).  May not need it.


	private static void doOverLimitTest(){
		list<Id> testingMWs = new list<Id>();
		map<String, Integer> theTypes = new map<String, Integer>{'Cement' => sika_SCCZoneMixMaterialsController2.cementMax,
														'Sand' => sika_SCCZoneMixMaterialsController2.sandMax,
														'SCM' => sika_SCCZoneMixMaterialsController2.scmMax, 
														'Coarse Aggregate' => sika_SCCZoneMixMaterialsController2.aggMax, 
														'Water' => sika_SCCZoneMixMaterialsController2.waterMax, 
														'Other' => sika_SCCZoneMixMaterialsController2.otherMax};
		integer counter;
		
		for(String type : theTypes.keySet()){
			counter = 0;
			
			for(sika_SCCZoneMixMaterialsController2.materialWrapper mw : ctrl.wrappedMaterials){
				if(mw.theMaterial.Type__c == type){
					counter++;
					mw.pickMe = true;
					testingMWs.add(mw.theMaterial.Id);
					if(counter == theTypes.get(type) + 1){
						counter = 0;
						break;
					}
				}
			}
			
			ctrl.addMixMaterials();
			system.assertEquals(counter-1, theTypes.get(type));
			
			PageReference p = ctrl.addMixMaterials();
			Boolean hasMsg = false;
			
			List<Apexpages.Message> msgs = ApexPages.getMessages();
			for(Apexpages.Message msg : msgs){
				String exceptionMsg = theTypes.get(type) + ' ' + type;
				if(theTypes.get(type) > 1){
					exceptionMsg += 's';
				}
			    if (msg.getDetail().contains(exceptionMsg)){
			    	hasMsg = true;
			    }
			}
			system.assert(hasMsg);
			
			for(Id mw : testingMWs){
				system.assert(ctrl.overLimit.contains(mw));
			}
		}
	}

	
	
	private static void bypassAllTriggers(){ //...well, all relevant triggers anyway
		sika_SCCZoneMixTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMixTriggerHelper.bypassMixOwnerTransferCheck = true;
		sika_SCCZoneMixTriggerHelper.bypassMixChatterAlert = true;
		sika_SCCZoneMixTriggerHelper.bypassUpdateSCMQuantity = true;
	
		sika_SCCZoneMaterialTriggerHelper.bypassPreventDelete = true;
		sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = true;
		sika_SCCZoneMaterialTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = true;
		
		sika_SCCZoneMixMaterialTriggerHelper.mixMaterialTriggerBypassSwitch(true);
		
	}

}