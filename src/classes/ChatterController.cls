public with sharing class ChatterController {
    public Opportunity oppRecord {get; set;}
    public List<FeedItem> fiList {get; set;}

    private static final String NO_EMAIL_ERROR =
        'Please check that the Opportunity\'s Account is related to a Roofing Rep CSR with a valid Email.';

    public ChatterController(ApexPages.StandardController stdController) {
        oppRecord   = [SELECT Id, Account.Roofing_Rep_CSR__c, Account.Roofing_Rep_CSR__r.Email FROM Opportunity WHERE Id = :stdController.getId()];
        fiList      = new List<FeedItem>();
        for (integer i = 0; i < 10; i++) {
            fiList.add(new FeedItem());
        }
    }

    // Add more Attachment action method
    public void AddRow() {
        fiList.add(new FeedItem());
    }

    public PageReference Save() {
        if (oppRecord.Id != null) {
            List<FeedItem> fiToInsert   = new List<FeedItem>();
            Boolean hasBody             = FALSE;
            for (FeedItem fi : fiList) {
                if (fi.ContentData != NULL) {
                    fi.parentId    = oppRecord.Id;
                    hasBody        = TRUE;
                    fiToInsert.add(fi);
                }
            }

            try {
                insert fiToInsert;
            } catch (DmlException e) {
                ApexPages.addMessages(e);
                return null;
            }

            // if at least one file is inserted, then opportunity “Tax doc uploaded” checkbox is checked
            if (hasBody) {
                Opportunity opp = new Opportunity(Id = oppRecord.Id);
                opp.Tax_doc_uploaded__c = TRUE;
                update opp;
            }

            if (oppRecord.Account == null || oppRecord.Account.Roofing_Rep_CSR__c == null ||
                    String.isEmpty(oppRecord.Account.Roofing_Rep_CSR__r.Email)) {

                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, NO_EMAIL_ERROR));
                return null;
            }

            fiList = new List<FeedItem>();

            Messaging.reserveSingleEmailCapacity(2);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {oppRecord.Account.Roofing_Rep_CSR__r.Email};
            mail.setToAddresses(toAddresses);
            mail.setReplyTo('support@acme.com');
            mail.setSenderDisplayName('Salesforce Support');
            mail.setSubject('New Case Created : ' + case.Id);
            mail.setBccSender(false);
            mail.setUseSignature(false);
            mail.setPlainTextBody('New chatter files have been loaded for Opportunity: ' + oppRecord.Id);
            mail.setHtmlBody('Your Opportunity:<b> ' + oppRecord.Id + ' </b>has new Chatter Files.<p>' +
                             'To view your Opportunity <a href=https://sikana--pts.cs19.my.salesforce.com/' + oppRecord.Id + '>click here.</a>');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

            PageReference pageRef = new PageReference('/' + oppRecord.Id);
            pageRef.setRedirect(true);
            return pageRef;
        }

        return null;
    }

    public PageReference Cancel() {
        PageReference pageRef = new PageReference('/' + oppRecord.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }
}