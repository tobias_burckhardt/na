public without sharing class sika_SCCZoneMixTriggerHelper {
	
	public static Boolean bypassAccountTeamCheck = false;
	public static Boolean bypassMixOwnerTransferCheck = false;
	public static Boolean bypassMixChatterAlert = false;
	public static Boolean bypassUpdateSCMQuantity = false;

	private static Boolean updateSCMQuantityHasRun = false;
	private static Boolean mixOwnerTransferCheckHasRun = false;
	private static Boolean mixChatterAlertHasRun = false;


	// prevents salespeople from editing or deleting Mix records associated with accounts on which they are not accountTeamMembers (or owners)
	// the SCC Zone permission set should be added to SCC Zone salespeople, and should provide view all & modify all access to the Mix object.
	// Will only run once per execution context.
	public static void accountTeamCheck(list<Mix__c> theMixes, String actionName){
		sika_SCCZoneAccountTeamCheckHelper.accountTeamCheck(theMixes, 'Mix', actionName);
	}















    // recalculates any SCM mix_material's quantity if the Mix.Target_SCM_Ratio field changes
    
    

// *************    
//TODO:
// need to recalculate the material weights on Mix when we do this.  (Replicate MixMaterialTriggerHelper's populateMaterialTypeWeights method.) 
// *************


    public static void updateSCMQuantity(list<Mix__c> theMixes){
    	// This should only fire once per execution context.
// No -- that would prevent it from running on delete if it already ran on update.  We need it to run in both contexts.

    	//if(updateSCMQuantityHasRun == true){
    	//	system.debug('******** updateSCMQuantity has already run once. Skipping.');
    	//} else {
	    //	updateSCMQuantityHasRun = true;

	    	set<Id> theMixIDs = new set<Id>();
	    	map<Id, Decimal> mixId_to_targetSCMRatio = new map<Id, Decimal>();
	    	map<Id, Decimal> mixID_to_cementQuantity = new map<Id, Decimal>();

	    	for(Mix__c m : theMixes){
	    		theMixIDs.add(m.Id);
	    		mixId_to_targetSCMRatio.put(m.Id, m.Target_SCM_Ratio__c);
	    	}

	    	list<Mix_Material__c> concretesAndSCMs = [SELECT Id, Mix__c, Quantity__c, Material__r.Type__c FROM Mix_Material__c WHERE Mix__c IN :theMixIDs AND (Material__r.Type__c = 'SCM' OR Material__r.Type__c = 'Cement')];

	    	for(Mix_Material__c mm : concretesAndSCMs){
	    		if(mm.Material__r.Type__c == 'Cement'){
	    			mixID_to_cementQuantity.put(mm.Mix__c, mm.Quantity__c);
	    		}
	    	}

	    	list<Mix_Material__c> SCMsToUpdate = new list<Mix_Material__c>();

	    	Decimal cementWeight;
	    	Decimal targetSCMRatio;
	    	Decimal SCMweight;
	    	for(Mix_Material__c mm : concretesAndSCMs){
	    		if(mm.Material__r.Type__c == 'SCM'){
	    			// calculate SCM weight based on the target (user-entered) ratio of SCM to cement
	    			cementWeight = mixID_to_cementQuantity.get(mm.Mix__c) == null ? 0 : mixID_to_cementQuantity.get(mm.Mix__c);
	    			targetSCMRatio = mixId_to_targetSCMRatio.get(mm.Mix__c);

map<String, String> SCMInfo = sika_SCCZoneCalculateSCMWeight.getSCMWeightAndUnit(cementWeight, targetSCMRatio);
mm.Quantity__c = Decimal.valueOf(SCMInfo.get('quantity'));
	    			
	    			// SCMWeight = cementWeight * (-1 * (targetSCMRatio / (targetSCMRatio -1)));
	    			
					// mm.Quantity__c = SCMWeight;
	    			SCMsToUpdate.add(mm);
	    		}
	    	}

			// make sure the record(s) aren't re-updated by the Mix Material trigger.
			// 
// TODO: Switch to new trigger bypass method:
sika_SCCZoneMixMaterialTriggerHelper.bypassSetSCMWeightAndUnit = true;
sika_SCCZoneMixMaterialTriggerHelper.bypassPopulateMaterialTypeCounts = true;
sika_SCCZoneMixMaterialTriggerHelper.bypassPopulateMaterialTypeWeights = true;
sika_SCCZoneMixMaterialTriggerHelper.bypassSetSCMWeightAndUnit = true;

	    	update(SCMsToUpdate);

sika_SCCZoneMixMaterialTriggerHelper.bypassSetSCMWeightAndUnit = false;
sika_SCCZoneMixMaterialTriggerHelper.bypassPopulateMaterialTypeCounts = false;
sika_SCCZoneMixMaterialTriggerHelper.bypassPopulateMaterialTypeWeights = false;
sika_SCCZoneMixMaterialTriggerHelper.bypassSetSCMWeightAndUnit = false;
//		}
    }


	// unlocks all material records associated with locked mixes that have been deleted, if the material isn't associated with another locked mix.
	// (When the last locked mix they're associated with is UNLOCKED (not deleted), the unlock action is fired by the Test Results (on delete) trigger.)
	public static void unlockMaterials(list<Mix__c> theMixes){
        list<Mix__c> lockedDeletedMixes = new list<Mix__c>();

        for(Mix__c m : theMixes){
        	if(m.isLocked__c){
        		lockedDeletedMixes.add(m);
        	}
        }

        sika_SCCZoneUnlockMaterialsHelper.unlockMaterials(lockedDeletedMixes);

    }


    // changes owner of attachments when a mix owner is changed.  (Note that Mix_Materials and Test Results change owners automatically; they're in a master:detail relationship with their Mix.) Use case: Salesperson creates mix & transfers ownership to SCC Zone user.
    // Also updates the Account associated with the mix when the mix owner is changed.
    public static void mixOwnerTransferCheck(list<Mix__c> newMixes, map<Id, Mix__c> oldMixMap){
    	if(mixOwnerTransferCheckHasRun == true){
    		system.debug('******** MixOwnerTransferCheck has already run once. Skipping.');
    	} else {

    		mixOwnerTransferCheckHasRun = true;

	    	set<Id> mixesChangingOwners = new set<Id>();
	    	set<Id> ownerIDs = new set<Id>();
	    	map<Id, Id> mixId_to_ownerId = new map<Id, Id>();
	    	map<Id, Id> ownerId_to_accountId = new map<Id, Id>();
	    	
	    	// create a set of IDs of the mixes with different OwnerIDs in Trigger.new vs Trigger.old
	    	// also create a map Mix.Id => OwnerId
	    	for(Mix__c m : newMixes){
	    		if(m.OwnerId != oldMixMap.get(m.Id).OwnerId){
	    			mixesChangingOwners.add(m.Id);
	    			ownerIDs.add(m.OwnerId);
	    			mixId_to_ownerId.put(m.Id, m.OwnerId);
	    		}
	    	}
	    	
	    	list<User> theUsers = [SELECT Id, AccountId FROM User WHERE Id IN :ownerIDs];
	    	for(User u : theUsers){
	    	    ownerId_to_accountId.put(u.Id, u.AccountId);
	    	}

	    	// get all the records related to the mixes that are changing owners. 
	    	if(mixesChangingOwners.size() > 0){
	    		system.debug('******** The following mix records have changed owners: ' + mixesChangingOwners);

	    		list<Attachment> attachmentsToUpdate = [SELECT ParentId, OwnerId FROM Attachment WHERE ParentId IN :mixesChangingOwners];

	    		// change the owner of all the records returned by the queries above, to their corresponding Mix's new owner
	    		for(Attachment a : attachmentsToUpdate){
	    			a.OwnerId = mixId_to_ownerId.get(a.ParentId);
	    		}
	    		
	    		for(Mix__c m : newMixes){
	    		    m.Account__c = ownerId_to_accountId.get(mixId_to_ownerId.get(m.Id));
	    		}
	    		
	    		update(attachmentsToUpdate);
	    		
	    	}
	    }
    }


	// alerts sales rep(s) via Chatter when an SCC Zone user associated with one of their accounts creates or optimixes a mix
	public static void mixChatterAlert(list<Mix__c> theMixes, map<Id, Mix__c> mixIDs_to_oldValues){
		if(mixChatterAlertHasRun == true){
			system.debug('******** mixChatterAlert has already run once. Skipping.');
		} else {

			mixChatterAlertHasRun = true;

			map<Id, set<Id>> accountId_to_repIDs = new map<Id, set<Id>>();
			map<Id, String> accountId_to_accountName = new map<Id, String>();
			set<Id> accountIDs = new set<Id>();
			Id accountID;
			String alertType;

			// create a set of Mix's Account's IDs
			for(Mix__c m : theMixes){
				accountIDs.add(String.valueOf(m.get('Account__c')));
			}
			system.debug('******** accountIDs = ' + accountIDs);

			// map each Account's ID to the IDs of its preferred sales rep(s)
			accountId_to_repIDs = sika_SCCZoneFindSalesRepHelper.getAccountIDsAndRepIDs(accountIDs);
			system.debug('******** accountId_to_repIDs = ' + accountId_to_repIDs);
			
			// create a map from AccountID to Account name
			list<Account> accounts = [SELECT Id, Name FROM Account WHERE Id IN :accountIDs];
			for(Account a : accounts){
				accountId_to_accountName.put(a.Id, a.Name);
			}
			system.debug('******** accountId_to_accountName = ' + accountId_to_accountName);
			
			list<FeedItem> posts = new list<FeedItem>();

			// for each mix, get the sales reps, and post the appropriate alert (new mix or optimized mix) to the reps' Chatter feeds
			for(Mix__c m : theMixes){
				alertType = null;
				if(mixIDs_to_oldValues != null){
					if(mixIDs_to_oldValues.get(m.Id) == null){
						alertType = 'NewMix';
						system.debug('******** A new mix has been created. Adding an alert to the Mix\'s Account\'s sales reps\' Chatter feed');
					
					} else if(m.get('isOptimized__c') == true && mixIDs_to_oldValues.get(m.Id).get('isOptimized__c') == false){
						alertType = 'MixOptimized';
						system.debug('******** A mix has been optimized.  Adding an alert to the  Mix\'s Account\'s sales reps\' Chatter feed');
					} else {
						system.debug('******** Mix updated, but not newly optimized. No Chatter alert will be created.');
						continue;
					}
				} else {
					alertType = 'NewMix';
					system.debug('******** A new mix has been created. Adding an alert to the Mix\'s Account\'s sales reps\' Chatter feed');
				}

				accountId = String.valueOf(m.get('Account__c'));
				system.debug('******** String.valueOf(m.get(Account__c) = ' + String.valueOf(m.get('Account__c')));
				
				if(accountId_to_repIDs.get(accountId) != null){
					set<Id> theReps = accountId_to_repIDs.get(accountId);
					system.debug('******** theReps = ' + theReps);
					String bodyStr;

					for(Id repId : theReps){
						bodyStr = '';
						// create an alert
						FeedItem post = new FeedItem();
						post.ParentId = repId;

						// set the alert body
						if(alertType == 'NewMix'){
							bodyStr += 'A new Mix has been created by ' + UserInfo.getName() + ' of ' + accountId_to_accountName.get(accountId) + '.\n';
							if(!String.isEmpty(String.valueOf(m.get('Project__c')))){
								bodyStr += 'Project Name: ' + String.valueOf(m.get('Project__c'));
							}
						}
						if(alertType == 'MixOptimized'){
							bodyStr += 'A mix has been optimized by ' + UserInfo.getName() + ' of ' + accountId_to_accountName.get(accountId) + '.\n';
							if(!String.isEmpty(String.valueOf(m.get('Project__c')))){
								bodyStr += 'Project Name: ' + String.valueOf(m.get('Project__c'));
							}
							
						}

						post.Body = bodyStr;
						post.Title = String.valueOf(m.get('Mix_Name__c'));
						post.LinkURL = '/' + m.Id;
						system.debug('******** post = ' + post);
						
						// add to a list of alerts	
						posts.add(post);
					}
				}
			}
			// post the alerts
			if(posts.size() > 0){
				insert(posts);
			}
		}
	}

}