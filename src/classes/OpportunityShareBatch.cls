public class OpportunityShareBatch implements Database.Batchable<SObject> {

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT Id FROM Opportunity]);
    }

    public void execute(Database.BatchableContext bc, List<Opportunity> scope) {
        OpportunitySharingServices.manageOpportunityTeamSharing(scope);
        OpportunitySharingServices.shareOppsWithPTS(scope);
    }

    public void finish(Database.BatchableContext bc) {}
}