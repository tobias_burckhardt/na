@isTest
private class OpportunityNOAServiceTest {

    /*
     * Insert Opportunity with NOA_Awarded__c = true 
     * and check if Take_Action__c is set to “NOA Created”  
     */
    @isTest
    public static void testAcceptProposals_insert_awarded() {

        Opportunity opp = createOppWithProposals(true);
        
        System.assertEquals('NOA Created', findOpportunity(opp.Id).Take_Action__c);

    }
    
    @isTest
    public static void testAcceptProposals_insert_not_awarded() {
        
        Opportunity opp = createOppWithProposals(false);
        
        System.assertEquals(null, findOpportunity(opp.Id).Take_Action__c);

    }    
    
    @isTest
    public static void testAcceptProposals_update_awarded() {
        
        Opportunity opp = createOppWithProposals(false);
        
        opp.NOA_Awarded__c = TRUE;
        update opp;        
        
        Opportunity finalOpp = findOpportunity(opp.Id);
                
        System.assertEquals('NOA Created', finalOpp.Take_Action__c); 
        System.assertEquals(1, finalOpp.Apttus_Proposal__R00N70000001yUfDEAU__r.size());
        System.assertEquals('Accepted', finalOpp.Apttus_Proposal__R00N70000001yUfDEAU__r[0].Apttus_Proposal__Approval_Stage__c);        
        System.assertEquals(finalOpp.AccountId, finalOpp.Apttus_Proposal__R00N70000001yUfDEAU__r[0].Apttus_Proposal__Account__c);        	        
        
    }    
    
    @isTest
    public static void testAcceptProposals_update_not_awarded() {
 
        Opportunity opp = createOppWithProposals(false);
                
        opp.NOA_Awarded__c = false;
        update opp;        
         
        Opportunity finalOpp = findOpportunity(opp.Id);
                
        System.assertEquals(null, finalOpp.Take_Action__c); 
        System.assertEquals(1, finalOpp.Apttus_Proposal__R00N70000001yUfDEAU__r.size());
        System.assertEquals('Draft', finalOpp.Apttus_Proposal__R00N70000001yUfDEAU__r[0].Apttus_Proposal__Approval_Stage__c);        
        System.assertNotEquals(finalOpp.AccountId, finalOpp.Apttus_Proposal__R00N70000001yUfDEAU__r[0].Apttus_Proposal__Account__c);        	        
        
    }      
    
    private static Opportunity createOppWithProposals(Boolean awarded) {
        Id roofingRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Roofing' AND SobjectType = 'Opportunity' limit 1].Id;

		Account acc = 
            (Account)TestFactory.createSObject(new Account(), true);
        
		Opportunity opp = (Opportunity) TestFactory.createSObject(new Opportunity(
            NOA_Awarded__c = awarded,
            AccountId = acc.Id,
            Amount = 5,
            Roofing_Area_SqFt__c = 5,
            Estimated_Ship_Date__c = Date.today(),
            RecordTypeId = roofingRecordTypeId
        ), true);
		// Opportunity can have just one proposal
		Apttus_Proposal__Proposal__c app = 
            (Apttus_Proposal__Proposal__c)TestFactory.createSObject(new Apttus_Proposal__Proposal__c(Apttus_Proposal__Opportunity__c = opp.Id), true);

        // not a child
		Apttus_Proposal__Proposal__c app3 = 
            (Apttus_Proposal__Proposal__c)TestFactory.createSObject(new Apttus_Proposal__Proposal__c(), true);  
        
        return opp;
    }
    
    private static Opportunity findOpportunity(Id oppId) {
        return [SELECT Id, NOA_Awarded__c, Take_Action__c, AccountId,
                                	(SELECT Apttus_Proposal__Approval_Stage__c, Apttus_Proposal__Account__c 
                                	 FROM Apttus_Proposal__R00N70000001yUfDEAU__r) 
                                FROM Opportunity 
                                WHERE Id = :oppId LIMIT 1];
    }
}