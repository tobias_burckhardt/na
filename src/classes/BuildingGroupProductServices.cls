public with sharing class BuildingGroupProductServices {

    public static void populateAttachmentGroup(List<Building_Group_Product__c> newList, Map<Id, Building_Group_Product__c> oldMap) {
        Map<Id, Building_Group_Product__c> bgpMap = new Map<Id, Building_Group_Product__c>();

        for(Building_Group_Product__c bgp : newList) {
            if(isChangeProduct(bgp, oldMap.get(bgp.Id))){
                bgpMap.put(bgp.Id, new Building_Group_Product__c());
            }
        }
        
        for(Building_Group_Product__c bgp :[SELECT Id, Name, Attachment_Group__c, Product_lookup__r.Attachment_Group__c 
        	FROM Building_Group_Product__c W
        	WHERE Id IN :bgpMap.KeySet()]){
        	if(string.isNotEmpty(bgp.Product_lookup__r.Attachment_Group__c)){
        		bgp.Attachment_Group__c = bgp.Product_lookup__r.Attachment_Group__c;
        		bgpMap.put(bgp.Id, bgp);
        	}
        }

        List<Building_Group_Product__c> bgpUpdate = new List<Building_Group_Product__c>();
        bgpUpdate.addAll(bgpMap.values());

        if(!bgpUpdate.isEmpty()) {
            insert bgpUpdate;
        }
    }
 
     public static void populateParentSystem(List<Building_Group_Product__c> newList) {
        Map<Id, List<Building_Group_Product__c>> bgpMap = new Map<Id, List<Building_Group_Product__c>>();

        for(Building_Group_Product__c bgp : newList) {
            bgpMap.put(bgp.Building_Group__c, new List<Building_Group_Product__c>());
        }
        
        for(Building_Group_Product__c bgp :[SELECT Id, Name, Building_Group__c, Attachment_Group__c, Product_lookup__r.Attachment_Group__c 
            FROM Building_Group_Product__c 
            WHERE Building_Group__c IN :bgpMap.KeySet()]){
            bgpMap.get(bgp.Building_Group__c).add(bgp);
        }

        /*
        List<Building_Group_Product__c> bgpUpdate = new List<Building_Group_Product__c>();
        bgpUpdate.addAll(bgpMap.values());

        if(!bgpUpdate.isEmpty()) {
            insert bgpUpdate;
        }
        */
    }

    private static Boolean isChangeProduct(Building_Group_Product__c newObject, Building_Group_Product__c oldObject) {
        return newObject.Product_lookup__c != oldObject.Product_lookup__c;
    }
}