public without sharing class sika_SCCZoneUserInfoDisplayComponentCtrl {

    public String repName {get; set;}
    public String repFirstName {get; set;}
    public String repEmail {get; set;}
    public String repPhone {get; set;}
    public String accountName {get; set;}

    public String name {get; set;}
    public String email {get; set;}
    public String phone {get; set;}

    public User theUser {get; private set;}
    public Contact theContact {get; private set;}

    public sika_SCCZoneUserInfoDisplayComponentCtrl() {
        // get current User Id
        Id userID = UserInfo.getUserId();

        // query for User info
        User theUser = [Select Id,
                         AccountID, 
                         ContactID 
                    FROM User 
                   WHERE Id = :userID 
                   Limit 1];

        // query for the User's Contact record info.  (We want to display the info from the Contact record 
        // though, in theory, the info on the User record and its related Contact record should match.)
        Contact theContact = [SELECT Id, 
                             Name, 
                             Email, 
                             Phone 
                        FROM Contact 
                       WHERE Id = :theUser.ContactID 
                       Limit 1];
                  
        // query for the user's Account record info
		Account theAccount = [SELECT Id,
									 Name
								FROM Account
							   WHERE Id = :theUser.AccountId];

        // set contact values for display
        name = theContact.Name;
        email = theContact.Email != null ? theContact.Email : '';
        phone = theContact.Phone != null ? theContact.Phone : '';


        // query accountTeamMembers associated with the user's account
        // if there is an account team member with a TeamMemberRole = [the SCC Zone sales rep's team member role], then that's the sales rep. (If > 1, use the first, until further notice.)
        // if there isn't an use w/ the "SCC Zone Sales" role, look for a user with TeamMemberRole = "Concrete BU," & assume that user is the rep.

        Id atmID = sika_SCCZoneFindSalesRepHelper.findOneSCCSalesRep(theUser.AccountID);
        list<User> u = [SELECT Id, Name, FirstName, Email, Phone FROM User WHERE Id = :atmID Limit 1];
        if(u.size() > 0) {
          repName = u[0].Name;
          repFirstName = u[0].FirstName;
          repEmail = u[0].Email;
          repPhone = u[0].Phone;
        }

        // make sure no values return null. (Blank is okay.)
        accountName = theAccount.Name != null ? theAccount.Name : '';
        repName = repName != null ? repName : '';
        repFirstName = repFirstName != null ? repFirstName : '';
        repEmail = repEmail != null ? repEmail : '';
        repPhone = repPhone != null ? repPhone : '';
    }

}