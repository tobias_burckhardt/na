@isTest
private class EventRestrictUpDelTriggerTest {

    private static final String MODIFIED_SUBJECT = 'Modified Subject';
    
    //Creates and inserts the specified number of events related to the given caseId
    private static List<Event> createTestEvent(Id caseId, Integer num) {
        List<Event> events = new List<Event>();
        for (Integer i = 0; i < num; i++) {
            Event ev = new Event(Subject='Test Subject ' + i, WhatId=caseId, DurationInMinutes=60, ActivityDateTime=System.now());
            events.add(ev);
        }
        insert events;
        return events;
    }

    @testSetup
    static void setUpTests() {
        RestrictedCaseTestHelper.createCustomSettings();
    }

    @isTest
    static void testUpdateDeleteUnrestrictedOpenEvent() {
        //Trying to update/delete an event related to an open case with an unrestricted record type should succeed
        Case testCase = RestrictedCaseTestHelper.createTestCase(false, false);
        List<Event> testEvents = createTestEvent(testCase.Id, 1);

        Test.startTest();
        testEvents[0].Subject = MODIFIED_SUBJECT;
        Database.SaveResult updateResult = Database.update(testEvents[0], false);
        System.assert(updateResult.isSuccess());

        Database.DeleteResult deleteResult = Database.delete(testEvents[0], false);
        System.assert(deleteResult.isSuccess());
        Test.stopTest();
    }

    @isTest
    static void testUpdateDeleteUnrestrictedClosedEvent() {
        //Trying to update/delete an event related to a closed case with an unrestricted record type should succeed
        Case testCase = RestrictedCaseTestHelper.createTestCase(false, true);
        List<Event> testEvents = createTestEvent(testCase.Id, 1);

        Test.startTest();
        testEvents[0].Subject = MODIFIED_SUBJECT;
        Database.SaveResult updateResult = Database.update(testEvents[0], false);
        System.assert(updateResult.isSuccess());

        Database.DeleteResult deleteResult = Database.delete(testEvents[0], false);
        System.assert(deleteResult.isSuccess());
        Test.stopTest();
    }

    @isTest
    static void testUpdateOpenEventRestricted() {
        //Trying to update an event related to an open case with a restricted record type should succeed
        Case testCase = RestrictedCaseTestHelper.createTestCase(true, false);
        List<Event> testEvents = createTestEvent(testCase.Id, 1);

        Test.startTest();
        testEvents[0].Subject = MODIFIED_SUBJECT;
        Database.SaveResult updateResult = Database.update(testEvents[0], false);
        System.assert(updateResult.isSuccess());
        System.assertEquals(0, updateResult.getErrors().size());
        Test.stopTest();
    }

    @isTest
    static void testReparentOpenEventRestricted() {
        //Trying to reparent an event related to an open case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true, false);
        Case newParent = RestrictedCaseTestHelper.createTestCase(true);
        List<Event> testEvents = createTestEvent(testCase.Id, 1);

        Test.startTest();
        testEvents[0].WhatId = newParent.Id;
        Database.SaveResult updateResult = Database.update(testEvents[0], false);
        System.assert(!updateResult.isSuccess());
        System.assertEquals(1, updateResult.getErrors().size());
        System.assertEquals(System.Label.Restricted_Object_Update, updateResult.getErrors()[0].getMessage());
        Test.stopTest();
    }

    @isTest
    static void testUpdateClosedEventRestricted() {
        //Trying to update an event related to a closed case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true, true);
        List<Event> testEvents = createTestEvent(testCase.Id, 1);

        Test.startTest();
        testEvents[0].Subject = MODIFIED_SUBJECT;
        Database.SaveResult updateResult = Database.update(testEvents[0], false);
        System.assert(!updateResult.isSuccess());
        System.assertEquals(1, updateResult.getErrors().size());
        System.assertEquals(System.Label.Restricted_Object_Update, updateResult.getErrors()[0].getMessage());
        Test.stopTest();
    }

    @isTest
    static void testDeleteOpenEventRestricted() {
        //Trying to delete an event related to an open case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true, false);
        List<Event> testEvents = createTestEvent(testCase.Id, 1);

        Test.startTest();
        Database.DeleteResult deleteResult = Database.delete(testEvents[0], false);
        System.assert(!deleteResult.isSuccess());
        System.assertEquals(1, deleteResult.getErrors().size());
        System.assertEquals(System.Label.Restricted_Object_Delete, deleteResult.getErrors()[0].getMessage());
        Test.stopTest();
    }

    @isTest
    static void testDeleteClosedEventRestricted() {
        //Trying to delete an event related to a closed case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true, true);
        List<Event> testEvents = createTestEvent(testCase.Id, 1);

        Test.startTest();
        Database.DeleteResult deleteResult = Database.delete(testEvents[0], false);
        System.assert(!deleteResult.isSuccess());
        System.assertEquals(1, deleteResult.getErrors().size());
        System.assertEquals(System.Label.Restricted_Object_Delete, deleteResult.getErrors()[0].getMessage());
        Test.stopTest();
    }
    
    @isTest
    static void testUpdateUnrelatedEvent() {
        //Trying to update an unrelated event (no WhatId) should succeed
        List<Event> testEvents = createTestEvent(null, 1);

        Test.startTest();
        testEvents[0].Subject = MODIFIED_SUBJECT;
        Database.SaveResult updateResult = Database.update(testEvents[0], false);
        System.assert(updateResult.isSuccess());
        System.assertEquals(0, updateResult.getErrors().size());
        Test.stopTest();
    }

    @isTest
    static void testDeleteUnrelatedEvent() {
        //Trying to delete an unrelated event (no WhatId) should succeed
        List<Event> testEvents = createTestEvent(null, 1);

        Test.startTest();
        Database.DeleteResult deleteResult = Database.delete(testEvents[0], false);
        System.assert(deleteResult.isSuccess());
        System.assertEquals(0, deleteResult.getErrors().size());
        Test.stopTest();
    }

    @isTest
    static void testBulkUpdateClosedEventsRestricted() {
        //Trying to update events related to a closed case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true, true);
        List<Event> testEvents = createTestEvent(testCase.Id, 10);

        Test.startTest();
        //Update all Events
        for (Event ev : testEvents) {
            ev.Subject = MODIFIED_SUBJECT;
        }

        //Try to save all Events
        List<Database.SaveResult> results = Database.update(testEvents, false);
        for (Database.SaveResult res : results) {
            System.assert(!res.isSuccess());
            System.assertEquals(1, res.getErrors().size());
            System.assertEquals(System.Label.Restricted_Object_Update, res.getErrors()[0].getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void testBulkDeleteClosedEventsRestricted() {
        //Trying to delete events related to a closed case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true, true);
        List<Event> testEvents = createTestEvent(testCase.Id, 10);

        Test.startTest();
        //Try to delete all events
        List<Database.DeleteResult> results = Database.delete(testEvents, false);
        for (Database.DeleteResult res : results) {
            System.assert(!res.isSuccess());
            System.assertEquals(1, res.getErrors().size());
            System.assertEquals(System.Label.Restricted_Object_Delete, res.getErrors()[0].getMessage());
        }
        Test.stopTest();
    }
}