public without sharing class sika_SCCZoneDisclaimerPageController {
	public PageReference acceptDisclaimer(){
      	PageReference ref = new PageReference('/SCCZone/apex/sika_SCCZoneUserHomePage');
        ref.setRedirect(true);
        return ref;
    }

    public PageReference rejectDisclaimer(){
    	PageReference ref = new PageReference('/SCCZone/secur/logout.jsp');
        ref.setRedirect(true);
        return ref;		
	}

    // check user auth status.  
    // if the user isn't authenticated, redirect to the login page
    public PageReference loadAction(){
        PageReference authCheck;
        sika_SCCZoneCheckAuth.setAuthStatus(UserInfo.getProfileId());

        authCheck = sika_SCCZoneCheckAuth.doAuthCheck();

        if(authCheck != null){
            return authCheck;
        }
        return null;
    }
}