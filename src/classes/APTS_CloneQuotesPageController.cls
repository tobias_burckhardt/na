/*************
@date 06/30/2015
Controller for CloneQuotesPage to clone Quote record with its child records
*************/

public class APTS_CloneQuotesPageController {
    
    String qId;
    
    public APTS_CloneQuotesPageController(ApexPages.StandardController controller) {
    
       qid= ApexPages.currentPage().getParameters().get('id');
    }
    
    public pageReference cloneItems(){
    
        System.debug('id $$$$$$$ '+qid);
        
        Set<String> proposalFields = new Set<String>(getCustomFieldNames(Apttus_Proposal__Proposal__c.getSObjectType()));

        String query = buildObjectQuery(proposalFields,
                                        null,
                                        'id=\''+qid+'\'',
                                        'Apttus_Proposal__Proposal__c');
                      
        List<Apttus_Proposal__Proposal__c> proposals =(List<Apttus_Proposal__Proposal__c>)Database.query(query);
        
         // Maps from Proposal Id to related objects

        Map<String,Apttus_Config2__ProductConfiguration__c> configurationMap = new Map<String,Apttus_Config2__ProductConfiguration__c>();
        Map<String,List<Apttus_Config2__LineItem__c>> lineItemMap = new Map<String,List<Apttus_Config2__LineItem__c>>();
        Map<String,List<Apttus_Proposal__Proposal_Line_Item__c>> proposalLineItemMap = new Map<String,List<Apttus_Proposal__Proposal_Line_Item__c>>();
        Map<String,List<Apttus_QPConfig__ProposalSummaryGroup__c>> proposalSummaryGroupMap = new Map<String,List<Apttus_QPConfig__ProposalSummaryGroup__c>>();
        Map<String,List<Apttus_Config2__SummaryGroup__c>> summaryGroupMap = new Map<String,List<Apttus_Config2__SummaryGroup__c>>();
        Map<String,List<Apttus_QPConfig__ProposalSummary__c>> proposalSummaryMap = new Map<String,List<Apttus_QPConfig__ProposalSummary__c>>();
        Map<String,List<Apttus_Config2__ProductAttributeValue__c>> productAttributeValueMap = new Map<String,List<Apttus_Config2__ProductAttributeValue__c>>();
       // Map<String,List<CPQ_PH2_Price_Model_Line__c>> priceModelLineValueMap = new Map<String,List<CPQ_PH2_Price_Model_Line__c>>();
       // Map<String,List<CPQ_PH2_Proposal_Price_Model_Line__c>> proposalPriceModelLineValueMap = new Map<String,List<CPQ_PH2_Proposal_Price_Model_Line__c>>();
        Map<String,List<Apttus_QPConfig__ProposalFootnote__c>> proposalFootNoteMap = new Map<String,List<Apttus_QPConfig__ProposalFootnote__c>>();
        Map<String,List<Apttus_Config2__UsagePriceTier__c>> usagePriceTierValueMap = new Map<String,List<Apttus_Config2__UsagePriceTier__c>>();
        Map<String,List<Apttus_QPConfig__ProposalUsagePriceTier__c>> proposalUsagePriceTierValueMap = new Map<String,List<Apttus_QPConfig__ProposalUsagePriceTier__c>>();
        Map<String,List<Apttus_QPConfig__ProposalProductAttributeValue__c>> proposalProductAttributeValueMap = new Map<String,List<Apttus_QPConfig__ProposalProductAttributeValue__c>>();
        
        // Get proposal Id list for queries
        Map<String,Apttus_Proposal__Proposal__c> proposalIdMap = new Map<String,Apttus_Proposal__Proposal__c>();
        for (Apttus_Proposal__Proposal__c proposal: proposals) {
            proposalIdMap.put(proposal.Id, proposal);
        }
        List<String> proposalIds = new List<String>(proposalIdMap.keySet());

        // **************************************************************************
        // Get records related to the Quote/Proposal to clone
        // **************************************************************************

        // Get product configurations and create map from proposal to product configuration
        Set<String> configurationFields = new Set<String>(getCustomFieldNames(Apttus_Config2__ProductConfiguration__c.getSObjectType()));
        String configurationQuery = buildObjectQuery(configurationFields,
                                                     null,
                                                     'Apttus_QPConfig__Proposald__c in :proposalIds',
                                                     'Apttus_Config2__ProductConfiguration__c');
        
        List<Apttus_Config2__ProductConfiguration__c> configurations = Database.query(configurationQuery);
        List<Id> configurationIds = new List<Id>();
        for (Apttus_Config2__ProductConfiguration__c configuration: configurations) {
            configurationMap.put(configuration.Apttus_QPConfig__Proposald__c, configuration);
            configurationIds.add(configuration.Id);
           // configurationIdToProposalIdMap.put(configuration.Id, configuration.Apttus_QPConfig__Proposald__c);
        }

        // Get Proposal Summary Groups and create map from proposal to List of Proposal Summary Groups
        Set<String> proposalSummaryGroupFields = new Set<String>(getCustomFieldNames(Apttus_QPConfig__ProposalSummaryGroup__c.getSObjectType()));
        String proposalSummaryGroupQuery = buildObjectQuery(proposalSummaryGroupFields,
                                                            null,
                                                            null,
                                                           'Apttus_QPConfig__ProposalSummaryGroups__r');
        proposalSummaryGroupQuery = 'Select Id, (' + proposalSummaryGroupQuery + ') From Apttus_Proposal__Proposal__c Where Id in :proposalIds';
        List<Apttus_Proposal__Proposal__c> proposalSummaryGroupProposals = Database.query(proposalSummaryGroupQuery);
        for (Apttus_Proposal__Proposal__c proposal: proposalSummaryGroupProposals ) {
            proposalSummaryGroupMap.put(proposal.Id, proposal.Apttus_QPConfig__ProposalSummaryGroups__r);
        }

        // Get Summary Groups and create map from config to List of Summary Groups
        Set<String> summaryGroupFields = new Set<String>(getCustomFieldNames(Apttus_Config2__SummaryGroup__c.getSObjectType()));
        String summaryGroupQuery = buildObjectQuery(summaryGroupFields,
                                                    null,
                                                    null,
                                                    'Apttus_Config2__SummaryGroups__r');
        summaryGroupQuery = 'Select Id, (' + summaryGroupQuery + ') From Apttus_Config2__ProductConfiguration__c Where Id in :configurationIds';
        List<Apttus_Config2__ProductConfiguration__c> summaryGroupConfigs = Database.query(summaryGroupQuery);
        for (Apttus_Config2__ProductConfiguration__c config: summaryGroupConfigs) {
            summaryGroupMap.put(config.Id, config.Apttus_Config2__SummaryGroups__r);
        }

        // Get Proposal Summaries and create map from proposal to List of Proposal Summaries
        Set<String> proposalSummaryFields = new Set<String>(getCustomFieldNames(Apttus_QPConfig__ProposalSummary__c.getSObjectType()));
        String proposalSummaryQuery = buildObjectQuery(proposalSummaryFields,
                                                       null,
                                                       null,
                                                       'Apttus_QPConfig__ProposalSummary__r');
        proposalSummaryQuery = 'Select Id, (' + proposalSummaryQuery + ') From Apttus_Proposal__Proposal__c Where Id in :proposalIds';
        List<Apttus_Proposal__Proposal__c > proposalSummaryProposals = Database.query(proposalSummaryQuery);
        for (Apttus_Proposal__Proposal__c proposal: proposalSummaryProposals ) {
            proposalSummaryMap.put(proposal.Id, proposal.Apttus_QPConfig__ProposalSummary__r);
        }

        // Get line items and create map from config to List of Line Items.
        Set<String> lineItemFields = new Set<String>(getCustomFieldNames(Apttus_Config2__LineItem__c.getSObjectType()));
        String lineItemQuery = buildObjectQuery(lineItemFields,
                                                null,
                                                null,
                                                'Apttus_Config2__LineItems__r');
        lineItemQuery = 'Select Id, (' + lineItemQuery + ') From Apttus_Config2__ProductConfiguration__c Where Id in :configurationIds';
        List<Apttus_Config2__ProductConfiguration__c> lineItemConfigs = Database.query(lineItemQuery);
        Map<String,Apttus_Config2__LineItem__c> lineItemToLineItemMap = new Map<String,Apttus_Config2__LineItem__c>();
        for (Apttus_Config2__ProductConfiguration__c config: lineItemConfigs) {
            lineItemMap.put(config.Id, config.Apttus_Config2__LineItems__r);
            for (Apttus_Config2__LineItem__c li: config.Apttus_Config2__LineItems__r) {
                lineItemToLineItemMap.put(li.Id, li);
            }
        }

        // Get product attribute values and create map from line item to List of Product Attribute Values.
        Set<String> productAttributeValueFields = new Set<String>(getCustomFieldNames(Apttus_Config2__ProductAttributeValue__c.getSObjectType()));
        String productAttributeValueQuery = buildObjectQuery(productAttributeValueFields,
                                                             null,
                                                             null,
                                                             'Apttus_Config2__ProductAttributeValues__r');
        productAttributeValueQuery = 'Select Id, (' + productAttributeValueQuery + ') From Apttus_Config2__LineItem__c Where Apttus_Config2__ConfigurationId__c in :configurationIds';
        List<Apttus_Config2__LineItem__c> productAttributeValueLineItems = Database.query(productAttributeValueQuery);
        for (Apttus_Config2__LineItem__c lineItem: productAttributeValueLineItems) {
            productAttributeValueMap.put(lineitem.Id, lineItem.Apttus_Config2__ProductAttributeValues__r);
        }
        // Get price model line values and create map from line item to List of Price Model Line Values.
       // Set<String> priceModelLineValueFields = new Set<String>(getCustomFieldNames(CPQ_PH2_Price_Model_Line__c.getSObjectType()));
        //String priceModelLineValueQuery = buildObjectQuery(priceModelLineValueFields,
                                //                             null,
                                  //                           null,
                                    //                         'Price_Model_Lines__r');
        //priceModelLineValueQuery = 'Select Id, (' + priceModelLineValueQuery + ') From Apttus_Config2__LineItem__c Where Apttus_Config2__ConfigurationId__c in :configurationIds';
       // List<Apttus_Config2__LineItem__c> priceModelLineValueLineItems = Database.query(priceModelLineValueQuery);
        //for (Apttus_Config2__LineItem__c liItem: priceModelLineValueLineItems) {
          //  priceModelLineValueMap.put(liItem.Id, liItem.Price_Model_Lines__r);
        //}
        
         // Get proposal price model line values and create map from proposal line item to List of Proposal Price Model Line Values.
      /*  Set<String> proposalPriceModelLineValueFields = new Set<String>(getCustomFieldNames(CPQ_PH2_Proposal_Price_Model_Line__c.getSObjectType()));
        String proposalPriceModelLineValueQuery = buildObjectQuery(proposalPriceModelLineValueFields,
                                                             null,
                                                             null,
                                                             'Proposal_Price_Model_Lines__r');
        proposalPriceModelLineValueQuery = 'Select Id, (' + proposalPriceModelLineValueQuery + ') From Apttus_Proposal__Proposal_Line_Item__c Where Apttus_QPConfig__ConfigurationId__c in :configurationIds';
        List<Apttus_Proposal__Proposal_Line_Item__c> proposalPriceModelLineValueLineItems = Database.query(proposalPriceModelLineValueQuery);
        for (Apttus_Proposal__Proposal_Line_Item__c pliItem: proposalPriceModelLineValueLineItems) {
            proposalPriceModelLineValueMap.put(pliItem.Id, pliItem.Proposal_Price_Model_Lines__r);
        }
*/
        // Get proposal line items and create map from config to List of Proposal Line Items.
        Set<String> proposalLineItemFields = new Set<String>(getCustomFieldNames(Apttus_Proposal__Proposal_Line_Item__c.getSObjectType()));
        String proposalLineItemQuery = buildObjectQuery(proposalLineItemFields,
                                                        null,
                                                        null,
                                                        'Apttus_Proposal__R00N70000001yUfBEAU__r');
        proposalLineItemQuery = 'Select Id, (' + proposalLineItemQuery + ') From Apttus_Proposal__Proposal__c Where Id in :proposalIds';
        List<Apttus_Proposal__Proposal__c> proposalLineItemProposals = Database.query(proposalLineItemQuery );
        Map<String,Apttus_Proposal__Proposal_Line_Item__c> lineItemToProposalLineItemMap = new Map<String,Apttus_Proposal__Proposal_Line_Item__c>();
        for (Apttus_Proposal__Proposal__c proposal: proposalLineItemProposals) {
            proposalLineItemMap.put(proposal.Id, proposal.Apttus_Proposal__R00N70000001yUfBEAU__r);
            for (Apttus_Proposal__Proposal_Line_Item__c pli: proposal.Apttus_Proposal__R00N70000001yUfBEAU__r) {
                lineItemToProposalLineItemMap.put(pli.Apttus_QPConfig__DerivedFromId__c, pli);
            }
        }
        
        
        // Get proposal product attribute values and create map from proposal  to proposal product attribute values.
        Set<String> proposalProductAttributeFields = new Set<String>(getCustomFieldNames(Apttus_QPConfig__ProposalProductAttributeValue__c.getSObjectType()));
        String proposalProductAttributeQuery = buildObjectQuery(proposalProductAttributeFields,
                                                        null,
                                                        null,
                                                        'Apttus_QPConfig__ProductAttributeValues__r');
        proposalProductAttributeQuery = 'Select Id, (' + proposalProductAttributeQuery + ') From Apttus_Proposal__Proposal_Line_Item__c Where Apttus_Proposal__Proposal__c in :proposalIds';
        List<Apttus_Proposal__Proposal_Line_Item__c> proposalProductAttributeValues = Database.query(proposalProductAttributeQuery);
        for (Apttus_Proposal__Proposal_Line_Item__c proposal: proposalProductAttributeValues) {
            proposalProductAttributeValueMap.put(proposal.Id, proposal.Apttus_QPConfig__ProductAttributeValues__r);  
        }
        
        
        
        // Get proposal usage price tier values and create map from proposal  to proposal usage price tier values.
        Set<String> proposalUsagePriceTierFields = new Set<String>(getCustomFieldNames(Apttus_QPConfig__ProposalUsagePriceTier__c.getSObjectType()));
        String proposalUsagePriceTierQuery = buildObjectQuery(proposalUsagePriceTierFields,
                                                        null,
                                                        null,
                                                        'Apttus_QPConfig__UsagePriceTiers__r');
        proposalUsagePriceTierQuery = 'Select Id, (' + proposalUsagePriceTierQuery + ') From Apttus_Proposal__Proposal_Line_Item__c Where Apttus_Proposal__Proposal__c in :proposalIds';
        List<Apttus_Proposal__Proposal_Line_Item__c> proposalUsagePriceTierValues = Database.query(proposalUsagePriceTierQuery);
        for (Apttus_Proposal__Proposal_Line_Item__c proposal: proposalUsagePriceTierValues) {
            proposalUsagePriceTierValueMap.put(proposal.Id, proposal.Apttus_QPConfig__UsagePriceTiers__r);  
        }
        
        
        
        
        // Get Footer Note values and create map from line item to List of Footer Note Values.
        Set<String> footerNoteValueFields = new Set<String>(getCustomFieldNames(Apttus_QPConfig__ProposalFootnote__c.getSObjectType()));
        String footerNoteValueQuery = buildObjectQuery(footerNoteValueFields,
                                                             null,
                                                             null,
                                                             'Apttus_QPConfig__QuoteFootnotes__r');
        footerNoteValueQuery = 'Select Id, (' + footerNoteValueQuery + ') From Apttus_Proposal__Proposal__c Where Id in :proposalIds';
        List<Apttus_Proposal__Proposal__c> footerNoteValueLineItems = Database.query(footerNoteValueQuery );
        for (Apttus_Proposal__Proposal__c  proposal: footerNoteValueLineItems ) {
            proposalFootNoteMap.put(proposal.Id, proposal.Apttus_QPConfig__QuoteFootnotes__r);
        }
        
        
        // Get Usage Price Tier values and create map from line item to List of Usage Price tier Values.
        Set<String> usagePriceTierValueFields = new Set<String>(getCustomFieldNames(Apttus_Config2__UsagePriceTier__c.getSObjectType()));
        String usagePriceTierValueQuery = buildObjectQuery(usagePriceTierValueFields ,
                                                             null,
                                                             null,
                                                             'Apttus_Config2__UsagePriceTiers__r');
        usagePriceTierValueQuery = 'Select Id, (' + usagePriceTierValueQuery + ') From Apttus_Config2__LineItem__c Where Apttus_Config2__ConfigurationId__c in :configurationIds';
        List<Apttus_Config2__LineItem__c> usagePriceTierValueLineItems = Database.query(usagePriceTierValueQuery);
        for (Apttus_Config2__LineItem__c lineItem: usagePriceTierValueLineItems) {
            usagePriceTierValueMap.put(lineitem.Id, lineItem.Apttus_Config2__UsagePriceTiers__r);
        }
        
        
        

        // **************************************************************************
        // Copy existing rows and insert new versions
        // **************************************************************************

        // Create new proposals, copy old proposal data over, and insert them
        List<Apttus_Proposal__Proposal__c> proposalsToInsert = proposals.deepClone(false, false, false);
        RecordType preApprovalRT = [Select Id From RecordType Where SobjectType = 'Apttus_Proposal__Proposal__c' /*And DeveloperName = 'Proposal_CPQ_Pre_Approval'*/ limit 1];
        for (Apttus_Proposal__Proposal__c proposal: proposalsToInsert) {
            proposal.Apttus_Proposal__Approval_Stage__c = 'Approval Required';
            proposal.RecordTypeId = preApprovalRT.Id;
        }
        insert proposalsToInsert;
        
        Map<String,String> oldProposalIdToNewIdMap = new Map<String,String>();
        for (Integer index = 0; index < proposals.size(); index++) {
            oldProposalIdToNewIdMap.put(proposals[index].Id, proposalsToInsert[index].Id);
        }

        // Create new product configurations, copy old configuration data over, and insert them
        List<Apttus_Config2__ProductConfiguration__c> configurationsToInsert = configurations.deepClone(false, false, false);
        List<Apttus_Proposal__Proposal__c> newProposalList=[select Id,Name from Apttus_Proposal__Proposal__c where id in:oldProposalIdToNewIdMap.values()];
       
        for (Apttus_Config2__ProductConfiguration__c configuration: configurationsToInsert) {
             configuration.Apttus_QPConfig__Proposald__c = oldProposalIdToNewIdMap.get(configuration.Apttus_QPConfig__Proposald__c);
             configuration.Apttus_Config2__BusinessObjectId__c = newProposalList.get(0).Id;
             configuration.Name = 'Product Config – '+newProposalList.get(0).Name;
        }
        insert configurationsToInsert;
        Map<Id,Id> oldProposalIdToNewConfigurationIdMap = new Map<Id,Id>();
        Map<Id,Id> oldConfigurationIdToNewConfigurationIdMap = new Map<Id,Id>();
        for (Integer index = 0; index < configurations.size(); index++) {
            oldProposalIdToNewConfigurationIdMap.put(configurations[index].Apttus_QPConfig__Proposald__c, configurationsToInsert[index].Id);
            oldConfigurationIdToNewConfigurationIdMap.put(configurations[index].Id, configurationsToInsert[index].Id);
        }

        // Create new proposal summary groups, copy data over, and insert them
        List<Apttus_QPConfig__ProposalSummaryGroup__c> proposalSummaryGroups = new List<Apttus_QPConfig__ProposalSummaryGroup__c>();
        for (List<Apttus_QPConfig__ProposalSummaryGroup__c> psgList: proposalSummaryGroupMap.values()) {
            proposalSummaryGroups.addAll(psgList);
        }
        List<Apttus_QPConfig__ProposalSummaryGroup__c> proposalSummaryGroupsToInsert = proposalSummaryGroups.deepClone(false, false, false);
        for (Apttus_QPConfig__ProposalSummaryGroup__c proposalSummaryGroup: proposalSummaryGroupsToInsert) {
            proposalSummaryGroup.Apttus_QPConfig__ConfigurationId__c = oldProposalIdToNewConfigurationIdMap.get(proposalSummaryGroup.Apttus_QPConfig__ProposalId__c);
            proposalSummaryGroup.Apttus_QPConfig__ProposalId__c = oldProposalIdToNewIdMap.get(proposalSummaryGroup.Apttus_QPConfig__ProposalId__c);
        }
        insert proposalSummaryGroupsToInsert;
        Map<String,String> oldProposalSummaryGroupIdToNewIdMap = new Map<String,String>();
        for (Integer index = 0; index < proposalSummaryGroupsToInsert.size(); index++) {
            oldProposalSummaryGroupIdToNewIdMap.put(proposalSummaryGroups[index].Id, proposalSummaryGroupsToInsert[index].Id);
        }

        // Create new summary groups, copy data over, and insert them
        List<Apttus_Config2__SummaryGroup__c> summaryGroups = new List<Apttus_Config2__SummaryGroup__c>();
        for (List<Apttus_Config2__SummaryGroup__c> sgList: summaryGroupMap.values()) {
            summaryGroups.addAll(sgList);
        }
        List<Apttus_Config2__SummaryGroup__c> summaryGroupsToInsert = summaryGroups.deepClone(false, false, false);
        for (Apttus_Config2__SummaryGroup__c summaryGroup: summaryGroupsToInsert) {
            summaryGroup.Apttus_Config2__ConfigurationId__c = oldConfigurationIdToNewConfigurationIdMap.get(summaryGroup.Apttus_Config2__ConfigurationId__c);
        }
        insert summaryGroupsToInsert;
        Map<String,String> oldSummaryGroupIdToNewIdMap = new Map<String,String>();
        for (Integer index = 0; index < summaryGroupsToInsert.size(); index++) {
            oldSummaryGroupIdToNewIdMap.put(summaryGroups[index].Id, summaryGroupsToInsert[index].Id);
        }

        // Create new proposal summaries, copy data over, and insert them
        List<Apttus_QPConfig__ProposalSummary__c> proposalSummaries = new List<Apttus_QPConfig__ProposalSummary__c>();
        for (List<Apttus_QPConfig__ProposalSummary__c> psList: proposalSummaryMap.values()) {
            proposalSummaries.addAll(psList);
        }
        List<Apttus_QPConfig__ProposalSummary__c> proposalSummariesToInsert = proposalSummaries.deepClone(false, false, false);
        for (Apttus_QPConfig__ProposalSummary__c proposalSummary: proposalSummariesToInsert) {
            proposalSummary.Apttus_QPConfig__ConfigurationId__c = oldProposalIdToNewConfigurationIdMap.get(proposalSummary.Apttus_QPConfig__ProposalId__c);
            proposalSummary.Apttus_QPConfig__ProposalId__c = oldProposalIdToNewIdMap.get(proposalSummary.Apttus_QPConfig__ProposalId__c);
            proposalSummary.Apttus_QPConfig__SummaryGroupId__c = oldSummaryGroupIdToNewIdMap.get(proposalSummary.Apttus_QPConfig__SummaryGroupId__c);
        }
        insert proposalSummariesToInsert;
        Map<String,String> oldProposalSummaryIdToNewIdMap = new Map<String,String>();
        for (Integer index = 0; index < proposalSummariesToInsert.size(); index++) {
            oldProposalSummaryIdToNewIdMap.put(proposalSummaries[index].Id, proposalSummariesToInsert[index].Id);
        }

        // Create new line items as clones, copy data over, and insert them
        List<Apttus_Config2__LineItem__c> lineItemsOriginal = lineItemToLineItemMap.values();
        List<Apttus_Config2__LineItem__c> lineItemsToInsert = LineItemsOriginal.deepClone(false, false, false);
        for (Apttus_Config2__LineItem__c lineItem: lineItemsToInsert) {
            lineItem.Apttus_Config2__ConfigurationId__c= oldConfigurationIdToNewConfigurationIdMap.get(lineItem.Apttus_Config2__ConfigurationId__c);
        }
        insert lineItemsToInsert;
        Map<String,String> oldLineItemIdToNewIdMap = new Map<String,String>();
        for (Integer index = 0; index < lineItemsOriginal.size(); index++) {
            oldLineItemIdToNewIdMap.put(lineItemsOriginal[index].Id, lineItemsToInsert[index].Id);
        }

        // Create new proposal line items as clones, copy old proposal line item data over, and insert them
        List<Apttus_Proposal__Proposal_Line_Item__c> proposalLineItems = new List<Apttus_Proposal__Proposal_Line_Item__c>();
        for (List<Apttus_Proposal__Proposal_Line_Item__c> pliList: proposalLineItemMap.values()) {
            proposalLineItems.addAll(pliList);
        }
        List<Apttus_Proposal__Proposal_Line_Item__c> proposalLineItemsToInsert = proposalLineItems.deepClone(false, false, false);
        for (Apttus_Proposal__Proposal_Line_Item__c pli: proposalLineItemsToInsert) {
            pli.Apttus_Proposal__Proposal__c = oldProposalIdToNewIdMap.get(pli.Apttus_Proposal__Proposal__c);
            pli.Apttus_QPConfig__ConfigurationId__c = oldConfigurationIdToNewConfigurationIdMap.get(pli.Apttus_QPConfig__ConfigurationId__c);
            pli.Apttus_QPConfig__DerivedFromId__c = oldLineItemIdToNewIdMap.get(pli.Apttus_QPConfig__DerivedFromId__c);
        }
        insert proposalLineItemsToInsert;
        Map<String,String> oldProposalLineItemIdToNewIdMap = new Map<String,String>();
        for (Integer index = 0; index < proposalLineItemsToInsert.size(); index++) {
            oldProposalLineItemIdToNewIdMap.put(proposalLineItems[index].Id, proposalLineItemsToInsert[index].Id);
        }
        
        
        // Create new proposal product attribute values as clones, copy old proposal product attribute data over, and insert them
        List<Apttus_QPConfig__ProposalProductAttributeValue__c> proposalProductAttributes = new List<Apttus_QPConfig__ProposalProductAttributeValue__c>();
        for (List<Apttus_QPConfig__ProposalProductAttributeValue__c> ppavList: proposalProductAttributeValueMap.values()) {
            proposalProductAttributes.addAll(ppavList);
        }
        List<Apttus_QPConfig__ProposalProductAttributeValue__c> proposalProductAttributeValuesToInsert = proposalProductAttributes.deepClone(false, false, false);
        for (Apttus_QPConfig__ProposalProductAttributeValue__c pli: proposalProductAttributeValuesToInsert) {
            pli.Apttus_QPConfig__LineItemId__c= oldProposalLineItemIdToNewIdMap.get(pli.Apttus_QPConfig__LineItemId__c);
        }
        insert proposalProductAttributeValuesToInsert;
        Map<String,String> oldProposalProductAttributeValueIdToNewIdMap = new Map<String,String>();
        for (Integer index = 0; index < proposalProductAttributeValuesToInsert.size(); index++) {
            oldProposalProductAttributeValueIdToNewIdMap.put(proposalProductAttributes[index].Id, proposalProductAttributeValuesToInsert[index].Id);
        }
        
        
         // Create new proposal usage price tier values as clones, copy old proposal usage price tier data over, and insert them
        List<Apttus_QPConfig__ProposalUsagePriceTier__c> proposalUsagePriceTier = new List<Apttus_QPConfig__ProposalUsagePriceTier__c>();
        for (List<Apttus_QPConfig__ProposalUsagePriceTier__c> ppavList: proposalUsagePriceTierValueMap.values()) {
            proposalUsagePriceTier.addAll(ppavList);
        }
        List<Apttus_QPConfig__ProposalUsagePriceTier__c> proposalUsagePriceTierValuesToInsert = proposalUsagePriceTier.deepClone(false, false, false);
        for (Apttus_QPConfig__ProposalUsagePriceTier__c pli: proposalUsagePriceTierValuesToInsert) {
            pli.Apttus_QPConfig__LineItemId__c= oldProposalLineItemIdToNewIdMap.get(pli.Apttus_QPConfig__LineItemId__c);
        }
        insert proposalUsagePriceTierValuesToInsert;
        
        

        // Create new product attribute values, copy old product attribute value data over, and insert them
        List<Apttus_Config2__ProductAttributeValue__c> productAttributeValues = new List<Apttus_Config2__ProductAttributeValue__c>();
        for (List<Apttus_Config2__ProductAttributeValue__c> pavList: productAttributeValueMap.values()) {
            productAttributeValues.addAll(pavList);
        }
        List<Apttus_Config2__ProductAttributeValue__c> productAttributeValuesToInsert = productAttributeValues.deepClone(false, false, false);
        for (Apttus_Config2__ProductAttributeValue__c pav: productAttributeValuesToInsert) {
            pav.Apttus_Config2__LineItemId__c = oldLineItemIdToNewIdMap.get(pav.Apttus_Config2__LineItemId__c);
        }
        insert productAttributeValuesToInsert;
        Map<String,String> oldProductAttributeValueIdToNewIdMap = new Map<String,String>();
        for (Integer index = 0; index < productAttributeValuesToInsert.size(); index++) {
            oldProductAttributeValueIdToNewIdMap.put(productAttributeValues[index].Id, productAttributeValuesToInsert[index].Id);
        }
        
        // Create new price model line values, copy old price model line value data over, and insert them
/*      List<CPQ_PH2_Price_Model_Line__c> priceModelLineValues = new List<CPQ_PH2_Price_Model_Line__c>();
        for (List<CPQ_PH2_Price_Model_Line__c> pmlList: priceModelLineValueMap.values()) {
            priceModelLineValues.addAll(pmlList);
        }
        List<CPQ_PH2_Price_Model_Line__c> priceModelLineValuesToInsert = priceModelLineValues.deepClone(false, false, false);
        for (CPQ_PH2_Price_Model_Line__c pml: priceModelLineValuesToInsert) {
            pml.CPQ_PH2_Price_Model_LineItem__c = oldLineItemIdToNewIdMap.get(pml.CPQ_PH2_Price_Model_LineItem__c);
        }
        insert priceModelLineValuesToInsert;
        Map<String,String> oldPriceModelLineValueIdToNewIdMap = new Map<String,String>();
        for (Integer index = 0; index < priceModelLineValuesToInsert.size(); index++) {
            oldPriceModelLineValueIdToNewIdMap.put(priceModelLineValues[index].Id, priceModelLineValuesToInsert[index].Id);
        }
       
        // Create new proposal price model line values, copy old proposal price model line value data over, and insert them
        List<CPQ_PH2_Proposal_Price_Model_Line__c> proposalPriceModelLineValues = new List<CPQ_PH2_Proposal_Price_Model_Line__c>();
        for (List<CPQ_PH2_Proposal_Price_Model_Line__c> ppmlList: proposalPriceModelLineValueMap.values()) {
            proposalPriceModelLineValues.addAll(ppmlList);
        }
        List<CPQ_PH2_Proposal_Price_Model_Line__c> proposalPriceModelLineValuesToInsert = proposalPriceModelLineValues.deepClone(false, false, false);
        for (CPQ_PH2_Proposal_Price_Model_Line__c ppml: proposalPriceModelLineValuesToInsert) {
            ppml.CPQ_PH2_Proposal_Line_Item__c = oldProposalLineItemIdToNewIdMap.get(ppml.CPQ_PH2_Proposal_Line_Item__c);
        }
        insert proposalPriceModelLineValuesToInsert;
        Map<String,String> oldProposalPriceModelLineValueIdToNewIdMap = new Map<String,String>();
        for (Integer index = 0; index < proposalPriceModelLineValuesToInsert.size(); index++) {
            oldProposalPriceModelLineValueIdToNewIdMap.put(proposalPriceModelLineValues[index].Id, proposalPriceModelLineValuesToInsert[index].Id);
        }
         */
        
        // Create new footer note values and insert them
        List<Apttus_QPConfig__ProposalFootnote__c> footerNoteValues = new List<Apttus_QPConfig__ProposalFootnote__c>();
        for (List<Apttus_QPConfig__ProposalFootnote__c> fooList: proposalFootNoteMap.values()) {
            footerNoteValues.addAll(fooList);
        }
        List<Apttus_QPConfig__ProposalFootnote__c> footerNoteValuesToInsert = footerNoteValues.deepClone(false, false, false);
        for (Apttus_QPConfig__ProposalFootnote__c foo: footerNoteValuesToInsert) {
            foo.Apttus_QPConfig__ProposalId__c = oldProposalIdToNewIdMap.get(foo.Apttus_QPConfig__ProposalId__c);
        }
        insert footerNoteValuesToInsert;
        
        
        
        // Create new usage price tier values, copy old price tier value data over, and insert them
        List<Apttus_Config2__UsagePriceTier__c> usagePriceTierValues = new List<Apttus_Config2__UsagePriceTier__c>();
        for (List<Apttus_Config2__UsagePriceTier__c> uptvList: usagePriceTierValueMap.values()) {
            usagePriceTierValues.addAll(uptvList);
        }
        List<Apttus_Config2__UsagePriceTier__c> usagePriceTierValuesToInsert = usagePriceTierValues.deepClone(false, false, false);
        for (Apttus_Config2__UsagePriceTier__c pav: usagePriceTierValuesToInsert) {
            pav.Apttus_Config2__LineItemId__c = oldLineItemIdToNewIdMap.get(pav.Apttus_Config2__LineItemId__c);
        }
        insert usagePriceTierValuesToInsert;
        Map<String,String> oldUsagePriceTierValueIdToNewIdMap = new Map<String,String>();
        for (Integer index = 0; index < usagePriceTierValuesToInsert.size(); index++) {
            oldUsagePriceTierValueIdToNewIdMap.put(usagePriceTierValues[index].Id, usagePriceTierValuesToInsert[index].Id);
        }
        
        

        // Update line items with product attribute Ids inserted
        for (Apttus_Config2__LineItem__c lineItem: lineItemsToInsert) {
                lineItem.Apttus_Config2__AttributeValueId__c = oldProductAttributeValueIdToNewIdMap.get(lineItem.Apttus_Config2__AttributeValueId__c);
        }
        if(lineItemsToInsert != null && lineItemsToInsert.size() >0){
            update lineItemsToInsert;
        }
        
        // Update line items with product attribute Ids inserted
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem: proposalLineItemsToInsert) {
            proposalLineItem.Apttus_QPConfig__AttributeValueId__c= oldProposalProductAttributeValueIdToNewIdMap.get(proposalLineItem.Apttus_QPConfig__AttributeValueId__c);
        }
        update proposalLineItemsToInsert;
        
        String newqId=null;
        if(proposalsToInsert!=null && proposalsToInsert.size()>0 && proposalsToInsert.get(0)!=null){
            newqId=proposalsToInsert.get(0).id;
            return new PageReference('/'+newqId);
        }
    
        return new PageReference('/'+qid);
    }
    
    public static List<String> getCustomFieldNames(Schema.SObjectType sobjectType) {
        // get custom field names from the object
        List<String> fieldNames = new List<String>();
        // get the field metadata
        Schema.DescribesObjectResult metadata = sobjectType.getDescribe();
        for (Schema.SObjectField field : metadata.fields.getMap().values()) {
            // use the fully qualified field name as the key
            Schema.DescribeFieldResult fldMetadata = field.getDescribe();
            if (fldMetadata.isCustom()) {
                // add the custom field
                fieldNames.add(fldMetadata.getName());
            }
        }
        return fieldNames;
    }
    
    public static String buildObjectQuery(Set<String> fieldNames, String specialFields, String whereClause, String sObjectName) {
        String queryStr = 'SELECT ';
        // standard fields
        queryStr += 'Id';
        queryStr += ', Name';
        if(specialFields != null) {
            queryStr += ','+specialFields;
        }
        // custom fields
        if (fieldNames != null) {
            for (String fieldName : fieldNames) {
                if (fieldName == 'Id' || fieldName == 'Name') {
                    // skip standard fields
                    continue;
                }
                queryStr += ',' + fieldName;
            }
        }
        queryStr += ' FROM ' + sObjectName;
        if(whereClause != null) {
            queryStr += ' WHERE ';
            queryStr += whereClause;
        }
        System.debug('queryStr : '+queryStr);
        return queryStr;
    }

}