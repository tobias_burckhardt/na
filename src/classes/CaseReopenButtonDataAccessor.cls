public with sharing class CaseReopenButtonDataAccessor {
	
	public static Case queryCase(Id caseId) {
		List<Case> cases = [SELECT Status, RecordTypeId  
					  		FROM Case
				      		WHERE Id = :caseId];
		return cases[0];
	}

	public static Id queryCaseInternalItRecordTypeId() {
		List<RecordType> caseRecordTypes = [SELECT Name, Id 
											FROM RecordType 
											WHERE DeveloperName = 'Internal_IT_Support_IT_Staff'];

		return caseRecordTypes[0].Id;
	}
}