@isTest(SeeAllData=true)
public class RHX_TEST_Status_Update {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM Status_Update__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new Status_Update__c()
            );
        }
    	Database.upsert(sourceList);
    }
}