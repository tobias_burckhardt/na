@isTest
private class GoogleChartsControllerTest {
	
	testMethod static void testLoadSievesMix1() {
		Mix__c mix = TestingUtils.createMix(5, 'mix', 0.5, 0.5, 0.5, false);
		mix.System_of_Measure__c = 'US Customary';
		insert mix;
		Sieve_ideal__c sieveIdeal = new Sieve_ideal__c(Name = 'name');
		sieveIdeal.Std_38_high__c = 0.1;
		sieveIdeal.Std_38_low__c = 0.01;
		insert sieveIdeal;

		GoogleChartsController.loadSievesMix(mix.Id);
	}

	testMethod static void testLoadSievesMaterial1() {
		Material__c material = TestingUtils.createMaterial(false);
		material.Unit_System__c = 'US Customary';
		insert material;

		GoogleChartsController.loadSievesMaterial(material.Id);
	}

	testMethod static void testLoadSievesMaterial2() {
		Material__c material = TestingUtils.createMaterial(false);
		insert material;

		GoogleChartsController.loadSievesMaterial(material.Id);
	}

	testMethod static void testWrapperSieve() {
		GoogleChartsController.WrapperSieve wrapperSieve = new GoogleChartsController.WrapperSieve('label', 0.1, 0.2, 0.3);
		wrapperSieve.sieveLabel = 'label';
		wrapperSieve.highValue = 0.1;
		wrapperSieve.lowValue = 0.2;
		wrapperSieve.calculatedValue = 0.3;
	}
	
}