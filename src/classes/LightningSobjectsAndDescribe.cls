public class LightningSobjectsAndDescribe {
    @AuraEnabled public Map<String, List<Sobject>> sobjects { get; set; }
    @AuraEnabled public LightningDescribe describe { get; set; }

    public LightningSobjectsAndDescribe() {
        this(new Map<String, List<Sobject>>(), new LightningDescribe());
    }

    public LightningSobjectsAndDescribe(Map<String, List<Sobject>> sobjects, LightningDescribe describe) {
        this.sobjects = sobjects;
        this.describe = describe;
    }
}