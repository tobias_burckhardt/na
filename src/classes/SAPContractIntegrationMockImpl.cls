@isTest
public class SAPContractIntegrationMockImpl implements WebServiceMock {
    public static final String CONTRACT_NUMBER = '1234';
    public static final String JOB_NAME_MISSING = 'JobName is missing.';
    
    public void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
            String requestName, String responseNS, String responseName, String responseType) {

        SAPContractIntegration.ContractResponse_element responseElement =
            new SAPContractIntegration.ContractResponse_element();

        responseElement.ResponseMessages = new List<SAPContractIntegration.ResponseMessages_element>();
        SAPContractIntegration.DT_ContractCreateRequest request_x =
            (SAPContractIntegration.DT_ContractCreateRequest) request;
        responseElement.ResponseMessages.addAll(addErrorMissingFields(request_x));

        if (responseElement.ResponseMessages.isEmpty()) {
            responseElement.ContractNumber = CONTRACT_NUMBER;
        }

        SAPContractIntegration.DT_ContractCreateResponse responseObject =
            new SAPContractIntegration.DT_ContractCreateResponse();
        responseObject.ContractResponse = responseElement;

        response.put('response_x', responseObject); 
    }

    private List<SAPContractIntegration.ResponseMessages_element> addErrorMissingFields(
            SAPContractIntegration.DT_ContractCreateRequest request) {

        List<SAPContractIntegration.ResponseMessages_element> errors =
            new List<SAPContractIntegration.ResponseMessages_element>();

        if (String.isBlank(request.ContractHeader.JobName)) {
            SAPContractIntegration.ResponseMessages_element error = new SAPContractIntegration.ResponseMessages_element();
            error.MessageType = SAPIntegrationService.ERROR_TYPE;
            error.MessageDescription = JOB_NAME_MISSING;
            errors.add(error);
        }

        return errors;
    }
}