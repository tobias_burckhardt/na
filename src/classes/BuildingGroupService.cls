/**
 * Shared business logic for Building_Group__c.
 * 
 * @author Pavel Halas
 * @company Bluewolf, an IBM Company
 * @date 6/2016
 */
public with sharing class BuildingGroupService {

    /**
     * Provides list of building groups for a given opportunity ID.
     */
    public static List<Building_Group__c> getBuildingGroupsForOpportunity(Id oppId) {
        return [
            SELECT Name, Opportunity__c, Building_Group_name__c, Accepted__c, Windspeed__c, Size__c, Membrane__c,
                    Warranty_Extended_Product__c, Warranty_Extended_Product__r.Name, Intended_Warranty_Product__c,
                    Intended_Warranty_Product__r.Name, Intended_Warranty_Product__r.Warranty_Type__c,
                    Intended_Warranty_Product__r.Warranty_Term__c, Intended_Warranty_Product__r.Warranty_Windspeed__c,
                    Intended_Warranty_Product__r.Roofing_Zero_Value_Warranty__c, Warranty__r.Entered_By__c,
                    Warranty__r.Title_of_Entered_By__c, Warranty__r.Date_of_Entered_By__c,
                    Warranty__r.Revision_Date__c, Bldg_Sq_Footage__c, Primary_Membrane__r.Name, Windspeed_MPH__c
            FROM Building_Group__c WHERE Opportunity__c = :oppId
        ];
    }

    /**
     * Persists provided building groups.
     */
    public static void updateBuildingGroups(List<Building_Group__c> buildingGroups) {
        update buildingGroups;
    }

    public static List<BuildingGroupWrapper> constructBuildingGroupWrappers(List<Building_Group__c> buildingGroups, Boolean issueWarranty) {
        List<BuildingGroupWrapper> buildingGroupWrappers = new List<BuildingGroupWrapper>();
        for (Building_Group__c bg : buildingGroups) {
            BuildingGroupWrapper bgw = new BuildingGroupWrapper();
            bgw.buildingGroup = bg;
            bgw.issueWarranty = issueWarranty;
            buildingGroupWrappers.add(bgw);
        }
        return buildingGroupWrappers;
    }

    public class BuildingGroupWrapper {
        public Building_Group__c buildingGroup {get; set;}
        public Boolean issueWarranty {get; set;}
        public Boolean warrantyExists {
            get {
                return buildingGroup.warranty__c != null;
            }
        }
    }
}