public class fieldnoteattachmentcontroller {

    public Field_Note__c fn {get; set;}
    public fieldnoteattachmentcontroller(ApexPages.StandardController controller) {
         this.fn = (Field_Note__c)controller.getRecord();
        }   
    public Attachment myfile;
    public Attachment getmyfile()
    {
        myfile = new Attachment();
        return myfile;
    }

    Public Pagereference Savedoc()
    { 
        String accid = System.currentPagereference().getParameters().get('id');
        Attachment a = new Attachment(parentId = accid, name=myfile.name, body = myfile.body, description=myfile.description);         
        insert a;  
        fn.Has_Attachment__c = true;         
        update fn;
        PageReference fieldnote = new PageReference('/'+fn.Id);
        fieldnote.setRedirect(true);
        return fieldnote;
        return NULL;
    }   

}