public with sharing class sika_SCCZoneCreateEditMaterialCtrlExt3 {

    @testVisible private Material__c theMaterial;
    @testVisible private String system_of_measure;
    private String materialType;
    private String theUserId {get; set;}
    
    public Boolean hasErrors {get; set;}
    public Boolean fillInMetric {get; set;}

    public Boolean errorsBoolean {get; private set;}
    public Boolean nameError {get; private set;}
    public Boolean absError {get; private set;}
    public Boolean specError {get; private set;}
    public Boolean unitError {get; private set;}
    public Boolean costError {get; private set;}
    public Boolean scmError {get; private set;}
    public Boolean newMatBool {get; set;}

    @testVisible private list<sieveWrapper> sieveWrappers;

    public sika_SCCZoneMaterialsHomePageController controller;


    // constructor
    public sika_SCCZoneCreateEditMaterialCtrlExt3(sika_SCCZoneMaterialsHomePageController ctrl) {
        theUserId = ctrl.userId;
        newMatBool = false;

        theMaterial = ctrl.theMaterial;
        controller = ctrl;
        
        system_of_measure = theMaterial.Unit_System__c;
        materialType = theMaterial.Type__c;

        fillInMetric = false;
        errorsBoolean = false;
        nameError = false;
        absError = false;
        specError = false;
        unitError = false;
        costError = false;
        scmError = false;
    }

    /**
     * The API field names of the Standard (US) fields, minus the "__c" suffix.
     */
    public static map <Integer, String> ordinal_to_field_std = new map<Integer, String>{
        1 => 'Sieve_2',
        2 => 'Sieve_1_5',
        3 => 'Sieve_1',
        4 => 'Sieve_3_4',
        5 => 'Sieve_1_2',
        6 => 'Sieve_3_8',
        7 => 'Sieve_4',
        8 => 'Sieve_8',
        9 => 'Sieve_10',
        10 => 'Sieve_16',
        11 => 'Sieve_20',
        12 => 'Sieve_30',
        13 => 'Sieve_40',
        14 => 'Sieve_100',
        15 => 'Sieve_150',
        16 => 'Sieve_200'
    };

    /**
     * Ordinals of the Standard (US) fields, mapped to aperture size in mm.
     */
    private map<Integer, Decimal> ordinal_to_std_in_mm = new map<Integer, Decimal>{
        1 => 50.8,
        2 => 38.1,
        3 => 25.4,
        4 => 19,
        5 => 12.7,
        6 => 9.51,
        7 => 4.76,
        8 => 2.38,
        9 => 2,
        10 => 1.19,
        11 => 0.841,
        12 => 0.595,
        13 => 0.420,
        14 => 0.149,
        15 => 0.112,
        16 => 0.075
    };

    public static map<Integer, String> ordinal_to_field_metric = new map<Integer, String>{
                    1 => 'Sieve_80mm',
                    2 => 'Sieve_63mm',
                    3 => 'Sieve_31_5mm',
                    4 => 'Sieve_25mm',
                    5 => 'Sieve_20mm',
                    6 => 'Sieve_16mm',
                    7 => 'Sieve_14mm',
                    8 => 'Sieve_12_5mm',
                    9 => 'Sieve_10mm',
                    10 => 'Sieve_5mm',
                    11 => 'Sieve_2_5mm',
                    12 => 'Sieve_1_25mm',
                    13 => 'Sieve_0_63mm',
                    14 => 'Sieve_0_315mm',
                    15 => 'Sieve_0_16mm',
                    16 => 'Sieve_0_08mm'
    };

    private map<Integer, Decimal> ordinal_to_size_metric = new map<Integer, Decimal>{
                    1 => 80,
                    2 => 63,
                    3 => 31.5,
                    4 => 25,
                    5 => 20,
                    6 => 16,
                    7 => 14,
                    8 => 12.5,
                    9 => 10,
                    10 => 5,
                    11 => 2.5,
                    12 => 1.25,
                    13 => 0.63,
                    14 => 0.315,
                    15 => 0.16,
                    16 => 0.08
    };


    public list<sieveWrapper> getSieveWrappers(){   
        System.debug('Welcome to the matrix!'); 
        Integer ord;
        String fieldName;
        String calculatedFieldName;
        String label;
        String value;
        String calculatedValue;
        Decimal apSize;

        if(sieveWrappers == null || fillInMetric == true){  // if the material is being measured in US Customary (rather than Metric), we will have to calculate the metric sieve sizes, based on the US Customary sizes entered by the user.  (Optimization and other calculations are all performed in metric.) So...we need to save the metric calculated sieve values regardless. When we run calculateValues() on save, we'll generate the metric sized sieveWrappers.   

            // create the sieveWrappers
            sieveWrappers = new list<sieveWrapper>();

            if(system_of_measure == 'Metric (SI)' || fillInMetric == true){
                for(Integer i = 0; i < ordinal_to_field_metric.size(); i++){
                    ord = i + 1;

                    fieldName = ordinal_to_field_metric.get(ord) + '__c';
                    calculatedFieldName = ordinal_to_field_metric.get(ord) + '_calculated__c';

                    // create new object so that we can access theMaterial's fields by the string value of their fieldNames.
                    object m = theMaterial.get(ordinal_to_field_metric.get(ord) + '__c');

                    value = String.valueOf(m);

                    map<String, sObjectField> fields_to_labels = theMaterial.getsObjectType().getDescribe().Fields.getMap();
                    label = fields_to_labels.get(ordinal_to_field_metric.get(ord) + '__c').getDescribe().getLabel();

                    apSize = ordinal_to_size_metric.get(ord);
                    sieveWrappers.add(new sieveWrapper(ord,                  // Unique key used to identify the same sieve in the maps above. (Standard maps are differentiated from Metric maps; the ord values don't flow across the two types.)
                                                       fieldName,            // API name of the field that stores the user-entered value of a given sieve, e.g., SieveName__c
                                                       calculatedFieldName,  // API name of the field that stores the calculated value of a given sieve, e.g.,   SieveName_calculated__c
                                                       label,
                                                       value,                // will be written back out to [SieveName]__c   (calculatedValue will be written out to [SieveName]_calculated__c)
                                                       apSize,               // the sieve's aperture (in mm)
                                                       'Metric (SI)'));          
                }

            } else { // if system-of-measure is not Metric (SI)

                for(Integer i = 0; i < ordinal_to_field_std.size(); i++){
                    ord = i + 1;
                    fieldName = ordinal_to_field_std.get(ord) + '__c';
                    calculatedFieldName = ordinal_to_field_std.get(ord) + '_calculated__c';

                    object s = theMaterial.get(ordinal_to_field_std.get(ord) + '__c');
                    value = String.valueOf(s);

                    map<String, sObjectField> fields_to_labels = theMaterial.getsObjectType().getDescribe().Fields.getMap();
                    label = fields_to_labels.get(ordinal_to_field_std.get(ord) + '__c').getDescribe().getLabel();

                    apSize = ordinal_to_std_in_mm.get(ord);

                    sieveWrappers.add(new sieveWrapper(ord,
                                                       fieldName, 
                                                       calculatedFieldName, 
                                                       label,
                                                       value, 
                                                       apSize,
                                                       system_of_measure));
                }
            }
            sortBy = 'apSize';
            sieveWrappers.sort();
            system.debug('******** Returning new sieveWrapper list: ' + sieveWrappers);
            return sieveWrappers;

        }
        system.debug('******** Returning existing sieveWrapper list: ' + sieveWrappers);
        return sieveWrappers;
    }


    public PageReference doSave(){
        
        // write the material details to the corresponding fields.  (We're using a generic sObject so we can reference fields by API name.  Will make sense in about 12 more lines...)
        SObject sObj = new Material__c();
        sObj.put('Id', theMaterial.Id); 
        sObj.put('Material_Name__c', theMaterial.Material_Name__c);
        sObj.put('Type__c', theMaterial.Type__c);
        sObj.put('Source__c', theMaterial.Source__c);
        sObj.put('Description__c', theMaterial.Description__c);
        sObj.put('Cost__c', theMaterial.Cost__c);
        sObj.put('Unit_System__c', theMaterial.Unit_System__c);
        sObj.put('Abs__c', theMaterial.Abs__c);
        sObj.put('Spec_Gravity__c', theMaterial.Spec_Gravity__c);
        sObj.put('Account__c', theMaterial.Account__c);
        
        ID tempID =[Select Id, SobjectType, Name From RecordType WHERE Name =: theMaterial.Type__c and SobjectType ='Material__c' limit 1].Id;
        theMaterial.RecordTypeID = tempID;
        sObj.put('RecordTypeID', theMaterial.RecordTypeID);

        sObj.put('Unit__c', controller.unitString);
        theMaterial.Unit__c = controller.unitString;

        if(theMaterial.Type__c == 'SCM' && theMaterial.SCM_Effective_Factor__c != null){
            sObj.put('SCM_Effective_Factor__c', theMaterial.SCM_Effective_Factor__c);
        }
        
        // validate the material details fields
        if(!detailsValidation()){
            return null;
        }

        if(materialType == 'Coarse Aggregate' || materialType == 'Sand'){
            // validate the user-entered sieve values
            if(sieveValidation(sieveWrappers)){ 
                
                // take the validated values, and interpolate/extrapolate any field values that were left blank.
                sieveWrappers = calculateValues(sieveWrappers);

                // write the user-entered sieve values to the user-entered fields; the calculated sieve values to the calculated fields.  (The user-entered values will remain editable as entered, including any field values that were left blank.)
                for(Integer i = 0; i < sieveWrappers.size(); i++){

                    // write the user-entered values to the [SieveName]__c fields
                    if(String.isNotBlank(sieveWrappers[i].theValue)){
                        sObj.put(sieveWrappers[i].theFieldName, Decimal.valueOf(sieveWrappers[i].theValue));
                    } else {
                        sObj.put(sieveWrappers[i].theFieldName, null);
                    }

                    // write the calculated values to the [SieveName]_calculated__c fields
                    if(String.isNotBlank(sieveWrappers[i].theCalculatedValue)){
                        sObj.put(sieveWrappers[i].theCalculatedFieldName, Decimal.valueOf(sieveWrappers[i].theCalculatedValue));
                    } else {
                        sObj.put(sieveWrappers[i].theCalculatedFieldName, null);
                    } 
                }
            } else {  // if sieve validation failed, ...
                return null;
            }
        }
            
        // cast the updted material__c object back into theMaterial, and upsert
        theMaterial = (Material__c) sObj;
        Database.UpsertResult sr = Database.upsert(theMaterial, false);
      
        system.debug('******** isSuccess = ' + sr.isSuccess());
        if(sr.isSuccess()){
            System.debug('Successfully inserted material. ID: ' + sr.getId());
            controller.displayPopUp = true;

        } else {
            errorsBoolean = true;
            for(Database.Error err : sr.getErrors()) {
                System.debug('The following error has occurred.');                    
                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                System.debug('Material fields that affected this error: ' + err.getFields());
                return null;
            }
        }

        PageReference newocp = new PageReference('/SCCZone/apex/sika_SCCZoneMaterialDetailPage?id=' + theMaterial.ID);
        newocp.setRedirect(true);
        return newocp;

    }

    
    @testVisible private Boolean detailsValidation(){
        // clear any previous errors
        errorsBoolean = false;
        nameError = false;
        absError = false;
        specError = false;
        unitError = false;
        costError = false;
        scmError = false;
        
        String errorString = 'Please resolve the following issues:';

        if(String.isBlank(theMaterial.Material_Name__c)) {
            errorsBoolean = true;
            nameError = true;
            errorString += '<BR/>* Material Name cannot be blank.'; 
        }
        if(String.isBlank(theMaterial.Abs__c) && theMaterial.Type__c != 'Water' && theMaterial.Type__c != 'SCM' && theMaterial.Type__c != 'Cement'){
            errorsBoolean = true;
            absError = true;
            errorString += '<BR/>* Max absorption cannot be blank.'; 
        } 
        if(String.isBlank(String.valueOf(theMaterial.Spec_Gravity__c))) {  // using isBlank on the string value of this field b/c it will return true if the field is null or whitespace
            errorsBoolean = true;
            specError = true;
            errorString += '<BR/>* Spec Gravity cannot be blank.'; 
        } 
        if(theMaterial.Unit__c == null){  // this tests the keys of the picklist key/value pairs.  The keys associated with the "-- none --" value are blank.
            errorsBoolean = true;
            unitError = true;
            errorString += '<BR/>* Unit cannot be blank.'; 
        } 
        if(String.isBlank(String.valueOf(theMaterial.Cost__c))){
            errorsBoolean = true;
            costError = true;
            errorString += '<BR/>* Cost cannot be blank.'; 
        }  
        if(theMaterial.Type__c == 'SCM' && (theMaterial.SCM_Effective_Factor__c < 0 || theMaterial.SCM_Effective_Factor__c > 2)){
            errorsBoolean = true;
            scmError = true;
            errorString += '<BR/>* SCM Effective Factor must be between 0 and 2.'; 
        }
        if(theMaterial.Type__c == 'SCM' && String.isBlank(String.valueOf(theMaterial.SCM_Effective_Factor__c))){
            errorsBoolean = true;
            scmError = true;
            errorString += '<BR/>* SCM Effective Factor cannot be blank.'; 
        }
        if(errorsBoolean == true){
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, errorString));
            return false;
        }
      
        return true;
    }


    @testVisible private Boolean sieveValidation(list<sieveWrapper> validateMe){
        hasErrors = false;

        map<Integer, String> workingMap = new map<Integer, String>();

        sortBy='apSize';
        validateMe.sort();

        Integer i;

        for(i=0; i < validateMe.size(); i++){
               workingMap.put(i, validateMe[i].theValue);
        }

        Integer fieldCount = 0;
        Integer x;        

        // make sure that at least two values have been entered
        for(i=0; i < workingMap.size(); i++){
            if(String.isNotBlank(String.valueOf(workingMap.get(i)))){
                fieldCount++;
            }
            if(fieldCount == 2){
                break;
            }
        }
        if(fieldCount < 2){
            hasErrors = true;
            validateMe[0].hasError = true;
            validateMe[0].errorMsg = 'Please fill in at least two sieve test values';
            return false;
        }

        // loop through the list looking for empty fields, filling each in with the value of the field above.
        for(i=0; i < workingMap.size(); i++){

            if(i == 0 && String.isBlank(workingMap.get(i))) {
                system.debug('********VALIDATION: largest sieve\'s % passing value is null...and that\'s okay! (Assume 100% passing for this test)'); 
                workingMap.put(i, '100');
                continue;
            }
            if(String.isBlank(workingMap.get(i))) {
                system.debug('********VALIDATION: No value was entered for sieve ' + validateMe[i].theLabel + '. Writing the last known larger sieve\'s value, ' + validateMe[i-1].theValue + ', to the workingList.');
                workingMap.put(i, workingMap.get(i-1));
            } 

            // also clear out any existing errors for the sieveWrappers, in preparation for the tests that follow
            validateMe[i].hasError = false;
            validateMe[i].errorMsg = '';
        }

        // if any field contains a non-numeric value, write the errors to the page and break here.  (Prevents us from trying to get decimal values of non-numerics in the next section.)
        Boolean isNAN = false;
        for(i=0; i < workingMap.size(); i++){
            if(String.isNotBlank(workingMap.get(i))){

                // if it isn't a number (excluding decimals), ...
                if(((workingMap.get(i))).remove('.').isNumeric() == false){
                    system.debug('********VALIDATION: sieve ' + validateMe[i].theLabel + '\'s value is NAN.');
                    hasErrors = true;
                    isNAN = true;
                    validateMe[i].hasError = true;
                    validateMe[i].errorMsg = 'Please enter a numeric value';
                }
            }
        }
        if(isNAN) {
            return false;
        }

        // be sure every smaller aperture sieve's % passing is < or == every larger aperteure sieve's % passing, and that no percentage has been entered that's > 100 or < 0.
        for(i=0; i < workingMap.size(); i++){
            system.debug('********VALIDATION: Now testing sieve ' + validateMe[i].theLabel + ' :: value = ' + validateMe[i].theValue);

            if(Decimal.valueOf(workingMap.get(i)) > 100 || Decimal.valueOf(workingMap.get(i)) < 0){
                system.debug('********VALIDATION: sieve ' + validateMe[i].theLabel + ' is either greater than 100% or less than 0%.');
                hasErrors = true;
                validateMe[i].hasError = true;
                validateMe[i].errorMsg = '% passing must be between 0 and 100';
            }

            // skip the largest sieve when we run the "this not greater than previous" test
            if(i == 0){
                system.debug('********VALIDATION: this is the largest sieve in the list');
                continue;
            }

            if(Decimal.valueOf(workingMap.get(i)) > Decimal.valueOf(workingMap.get(i-1))){
                system.debug('********VALIDATION: sieve ' + validateMe[i].theLabel + '\'s value is greater than the last largest known sieve\'s value. Setting an error.');
                hasErrors = true;
                validateMe[i].hasError = true;
                validateMe[i].errorMsg = '% passing cannot be greater than ' + validateMe[i-1].theValue + '.';
            }
        }

        if(hasErrors){
            return false;
        }
        return true;
    }


    @testVisible private list<sieveWrapper> calculateValues(list<sieveWrapper> userEntered){
        system.debug('********CALC: userEntered BEFORE addMetricSieves = ' + userEntered);
        list<sieveWrapper> outerUnknowns = new list<sieveWrapper>();
        list<sieveWrapper> innerUnknowns = new list<sieveWrapper>();
        list<sieveWrapper> calculatedWrappers = new list<sieveWrapper>();
        map<sieveWrapper, String> wrappers_to_values = new map<sieveWrapper, String>();

        // clear out all previously calculated values
        for(sieveWrapper s : userEntered){
            s.thecalculatedValue = null;
        }

        // if fewer than two sieve values have been entered, don't calculate any null values, but don't return an error.       
        Integer valueCount = 0;
        for(sieveWrapper s : userEntered){
            if(String.isNotBlank(s.theValue)){
                valueCount++;
                if(valueCount == 2) {
                    break;
                }
            }
        }
        if(valueCount < 2){
            system.debug('********CALC: No sieve values have been entered');
            return userEntered; 
        }

        // If the material is entered using Standard values (i.e., "US Customary"), we need both sets of sieves.  (All calculations are performed in metric, so we'll need to take the % passing values from the Standard sieves, and interpolate/extrapolate the values that would have passed through the Metric sieves, had they been used.)
        // Get the metric sized sieves and add them to the userEntered list so we can calculate (based on interpolation/extrapolation from the values for the sizes entered) their sizes as well.
        if(system_of_measure == 'US Customary'){
            fillInMetric = true;
            list<sieveWrapper> addMetricSieves = getSieveWrappers(); 
            userEntered.addAll(addMetricSieves);
        }

        Integer knownsCount = 0;
        Decimal lowSize = null;
        Decimal lowValue = null;
        Decimal highSize = null;
        Decimal highValue = null;

        // make sure the sieves in the list are ordered largest aperture to smallest aperture
        sortBy = 'apSize';
        userEntered.sort();
        system.debug('********CALC: userEntered BEFORE addMetricSieves = ' + userEntered);


        // if we haven't reached the end of the list, or if the list is empty (which it will be eventually)...
        for(integer i=0; i < userEntered.size() || userEntered.isEmpty() == false; i++){
            
            // if this sieveWrapper's % passing value is null... 
            if(String.isBlank(userEntered[i].theValue)){
                
                // if this is not the last sieveWrapper in the list...
                if(i+1 != userEntered.size()){
                    
                    // if we haven't found any % passing values yet...
                    if(knownsCount < 1){
                        
                        // add this to the list of sieveWrappers with values that we'll have to extrapolate
                        outerUnknowns.add(userEntered[i]);
                        userEntered.remove(i);  // remove this sieveWrapper from this list & decrement the index pointer
                        i--;
                        continue;
                    
                    } else {  // if we have at least one known % passing value
                        // add this to the list of sieveWrappers with values that we'll have to interpolate
                        innerUnknowns.add(userEntered[i]);
                        userEntered.remove(i);
                        i--;
                        continue;
                    }

                } else {  // if this is the last sieveWrapper in the list and its % passing value is null, we'll need to extrapolate its value (and the value of any sieveWrappers that are still in the innerUnknowns list)
                          // All sieveWrappers we could use as high & low points to extrapolate from are in the calculatedWrappers list now.  We'll loop backwards through the calculatedWrappers list to find high & low.  To keep the extrapolation as accurate as possble, we'll only look at user entered values.
                    Integer n = calculatedWrappers.size() - 1;
                    lowValue = null;
                    highValue = null;
                    system.debug('********CALC: lowValue = ' + lowValue + ' :: highValue = ' + highValue);

                    // find the two smallest-aperture sives larger than this sieve, with % passing values that were entered by the user.  (Not interpolated values.)
                    while(lowValue == null || highValue == null){
                        system.debug('********CALC: lowValue = ' + lowValue + ' :: highValue = ' + highValue);
                        system.debug('********CALC: calculatedWrappers[n].theValue = ' + calculatedWrappers[n].theValue + ' :: ' + calculatedWrappers[n].theApSize + ' :: ' + calculatedWrappers[n].theLabel);
                        
                        if(lowValue == null && !String.isBlank(calculatedWrappers[n].theValue)){
                            lowValue = Decimal.valueOf(calculatedWrappers[n].theValue);
                            lowSize = calculatedWrappers[n].theApSize;
                            n--;
                            continue;
                        }
                        if(highValue == null && !String.isBlank(calculatedWrappers[n].theValue)){
                            highValue = Decimal.valueOf(calculatedWrappers[n].theValue);
                            highSize = calculatedWrappers[n].theApSize;
                            break;
                        }
                        n--;
                    }

                    // move this sieveWrapper into the *innerUnknowns* list w/ any others that might still be there, and *extrapolate* to find their values
                    innerUnknowns.add(userEntered[i]);
                    userEntered.remove(i);

                    wrappers_to_values = doExtrapolation(innerUnknowns, highSize, highValue, lowSize, lowValue);

                    for(sieveWrapper s : wrappers_to_values.keySet()){
                        s.theCalculatedValue = wrappers_to_values.get(s);
                        calculatedWrappers.add(s);
                    }
                    system.debug('********CALC: Exiting the userEntered for loop, extrapolating for one or more null values at the low end;');
                    break;
                
                }  // (end logic for last sieveWrapper in the list)


            // if the sieveWrapper's % passing value is not null...
            } else {
                knownsCount ++; 

                // if this sieveWrapper's passing value is 100% and there are previous (larger) sieveWrappers with null values -- i.e., any in the outerUnknowns list -- set their values 
                // to 100 and move them (and this one) directly to the calculatedWrappers list.  (No calculation needed!)
                if(userEntered[i].theValue == '100'){
                    
                    // set the last known high value & high aperture size
                    highValue = Decimal.valueOf(userEntered[i].theValue);
                    highSize = userEntered[i].theApSize;

                    // move this sieveWrapper over to the outerUnknowns list, and...
                    outerUnknowns.add(userEntered[i]);
                    userEntered.remove(i);

                    // ...set its and all larger sieveWrappers' % passing values to 100
                    if(!outerUnknowns.isEmpty()){
                        for(sieveWrapper s : outerUnknowns){
                            s.thecalculatedValue = '100.00';
                        }
                    } 
                    // move all outerUnknowns to the calculatedWrappers list
                    calculatedWrappers.addAll(outerUnknowns);
                    outerUnknowns.clear();
                    i--;
                    system.debug('*******CALC: The first non-null sieveWrapper value was 100. Setting all previous (larger) sieveWrappers\' values to 100, moving them into the calculatedWrappers list, and continuing on to the next value in the userEntered list\'s for loop.');
                    continue;
                }
                
                // if there are no null values to calculate, or if there are null values to calculate, but we don't have enough (i.e., 2) known values to calculate them from yet...                
                if((outerUnknowns.isEmpty() && innerUnknowns.isEmpty()) || !(outerUnknowns.isEmpty() && innerUnknowns.isEmpty()) && knownsCount < 2){    

                    // move the last known high value & high aperture size down to this sieve's value & ap size
                    highValue = Decimal.valueOf(userEntered[i].theValue);
                    highSize = userEntered[i].theApSize;
                    
                    // set the sieve's calculated value equal to its user-entered value. (Nothing to calculate.)
                    userEntered[i].thecalculatedValue = userEntered[i].theValue;
                    
                    // move this sieve out of this list, and into the calculatedWrappers list. (We're all done with this one!)
                    calculatedWrappers.add(userEntered[i]);
                    userEntered.remove(i);
                    i--;
                    continue;

                } else { // if there are null values to calculate, and if we do have enough data points to calculate slope & distance to/from any unknown values...
                    
                    if(knownsCount > 1){

                        // set the low point for size & value to this sieve's size & value
                        lowValue = Decimal.valueOf(userEntered[i].theValue);
                        lowSize = userEntered[i].theApSize;
                     
                        // see if we need to extrapolate any sieve values
                        if(outerUnknowns.isEmpty() == false){
                            
                            // run the extrapolation.... & get e'm into the final list.
                            wrappers_to_values = doExtrapolation(outerUnknowns, highSize, highValue, lowSize, lowValue);                          

                            for(sieveWrapper s : wrappers_to_values.keySet()){
                                s.theCalculatedValue = wrappers_to_values.get(s);
                                calculatedWrappers.add(s);
                            }
                            // clear the high list
                            outerUnknowns.clear();
                        }

                        // see if we need to interpolate any sieve values
                        if(innerUnknowns.isEmpty() == false){

                            // run the interpolation.... & get e'm into the calcuatedWrappers list                  
                            wrappers_to_values = doInterpolation(innerUnknowns, highSize, highValue, lowSize, lowValue);                          

                            for(sieveWrapper s : wrappers_to_values.keySet()){
                                s.theCalculatedValue = wrappers_to_values.get(s);
                                calculatedWrappers.add(s);
                            }
                            // clear the low list
                            innerUnknowns.clear();
                        }

                        // now make this sieve's values the last-known high values...
                        highValue = Decimal.valueOf(userEntered[i].theValue);
                        highSize = userEntered[i].theApSize;


                        // if this sieveWrapper's % passing value is zero, then we know all remaining (smaller) sieves must have a 0% passing value.
                        if(Decimal.valueOf(userEntered[i].theValue) == 0){
                            system.debug('*******CALC: The current sieveWrapper, ' + userEntered[i].theLabel + ', has a 0% passing value.  All remaining (smaller) sieveWrappers\' % passing value must also be 0%. Setting all remaining sieveWrappers\' values to 0, moving them into the calculatedWrappers list, and breaking out of the userEntered list\'s for loop.');
                            
                            while(i < userEntered.size()){
                                userEntered[i].thecalculatedValue = '0.00';
                                i++;
                            }
                            calculatedWrappers.addAll(userEntered);
                            userEntered.clear();
                            break;
                        
                        } else { // if this sieveWrapper's % passing value is not zero...
                            // set this sieve's calculated value equal to its user-entered value. (Nothing to calculate.)
                            userEntered[i].thecalculatedValue = userEntered[i].theValue;
                            
                            // ...and move this sieve over to the final list.
                            calculatedWrappers.add(userEntered[i]);
                            userEntered.remove(i);
                            i--;
                        }
                    }
                }
            }
        }
        userEntered = null;
        outerUnknowns = null;
        innerUnknowns = null;
   
        return calculatedWrappers;
    } 


    private map<sieveWrapper, String> doInterpolation(list<sieveWrapper> theUnknowns, Decimal highAp, Decimal highValue, Decimal lowAp, Decimal lowValue){      
        map<sieveWrapper, String> wrapper_to_value = new map<sieveWrapper, String>();

        Decimal X;
        Decimal Y;

        Decimal y2 = highValue;
        Decimal y1 = lowValue;
        Decimal x2 = highAp;
        Decimal x1 = lowAp;

        // To interpolate the (unknown) % passing through a sieve between two known sieves:
        // x2 = the larger sieve's aperture size
        // x1 = the smaller sieve's aperture size
        // y2 = the larger sieve's % passing
        // y1 = the smaller sieve's % passing
        // X is the aperture of the sieve we're solving for
        // Y is what we're solving for -- the % passing through X

        // formula is:
        //       (y2 - y1)(X - x1)
        // Y =  -------------------  + y1
        //         (x2 - x1)
        //
        for(sieveWrapper s : theUnknowns){
            system.debug('*******INTERP: s.theLabel = ' + s.theLabel + ' :: s.apSize = ' + s.theApSize + ' :: highAp = ' + highAp + ' :: highValue = ' + highValue + ' :: lowAp = ' + lowAp + ' :: lowValue = ' + lowValue);

            X = s.theApSize;

            Y = ((y2 - y1) * (X - x1)/(x2 - x1)) + y1;

            system.debug('******** interpolatedPercentPassing value for ' + s.theLabel + ' = ' + Y);
            wrapper_to_value.put(s, String.valueOf(Y.setScale(2)));

        }

        return wrapper_to_value;
    }



    private map<sieveWrapper, String> doExtrapolation(list<sieveWrapper> theUnknowns, Decimal highAp, Decimal highValue, Decimal lowAp, Decimal lowValue){      
        map<sieveWrapper, String> wrapper_to_value = new map<sieveWrapper, String>();

        Decimal X;
        Decimal Y;

        Decimal y2 = highValue;
        Decimal y1 = lowValue;
        Decimal x2 = highAp;
        Decimal x1 = lowAp;

        // x2 = the larger sieve's aperture size
        // x1 = the smaller sieve's aperture size
        // y2 = the larger sieve's % passing
        // y1 = the smaller sieve's % passing
        // X is the aperture of the sieve we're solving for
        // Y is what we're solving for -- the % passing through X

        // formula is:
        //
        //             (y2 - y1)
        //  Y = y1 + ( --------- ) * (X - x1)
        //             (x2 - x1)
        //

        for(sieveWrapper s : theUnknowns){
            system.debug('*******EXTRAP: s.theLabel = ' + s.theLabel + ' :: s.apSize = ' + s.theApSize + ' :: highAp = ' + highAp + ' :: highValue = ' + highValue + ' :: lowAp = ' + lowAp + ' :: lowValue = ' + lowValue);

            X = s.theApSize;

            Y = y1 + ((y2 - y1)/(x2 - x1)) * (X - x1);
            
            // round to two decimal places before returning
            system.debug('******** extrapolatedPercentPassing value for ' + s.theLabel + ' = ' + Y + ' :: rounding to ' + Y.setScale(2));
            Y = Y.setScale(2);

            // percent passing values cannot be greater than 100% or less than 0%. Cut the calculations off there.
            if(Y > 100) {
                Y = 100;
            } 
            if(Y < 0){
                Y = 0;
            }

            wrapper_to_value.put(s, String.valueOf(Y));
        }
        return wrapper_to_value;
    }



    // wrapper class -- sortable by aperture size or "theOrd" value (the number designating the order of the sieves and their sizes, as defined in the ordinal_to_field and ordinal_to_size maps)
    public static String sortBy {get; set;}

    public class sieveWrapper implements Comparable {   
        public Integer theOrd {get; set;}
        public String theFieldName {get; set;}
        public String theCalculatedFieldName {get; set;}
        public String theLabel {get; set;}

        // salesforce treats blank decimal fields as zeroes, but we need to respect blank fields, not treat them as zeroes. We'll have to use strings instead.
        public String theValue {get; set;}
        public String theCalculatedValue {get; set;}
        public Decimal theApSize {get; set;}
        public String theSOM {get; set;}

        public Boolean hasError {get; set;}
        public String errorMsg {get; set;}

        // constructor
        public sieveWrapper(Integer ord, String fieldName, String calcFieldName, String label, String value, Decimal apSize, String systemOfMeasure){
            theOrd = ord;
            theFieldName = fieldName;
            theCalculatedFieldName = calcFieldName;
            theLabel = label;
            theValue = value;
            theApSize = apSize;
            theSOM = systemOfMeasure;
            
            theCalculatedValue = '';  // used by the vforce page when the system of measure = standard.  Prevents the sieve wrappers representing metric-sized sieves from being displayed, even though they are being calculated ...in secret!
            hasError = false;
            errorMsg = '';
        }

        // implement the compareTo() method
        public Integer compareTo(Object compareTo){
            sieveWrapper compareToSieveWrapper = (sieveWrapper) compareTo;
            if(sortBy == 'ord'){
                if(theOrd == compareToSieveWrapper.theOrd){
                    return 0;
                }
                if(theOrd > compareToSieveWrapper.theOrd){
                    return 1;
                }
                return -1;  
            } else {
                if(theApSize == compareToSieveWrapper.theApSize){
                    return 0;
                }
                if(theApSize > compareToSieveWrapper.theApSize){
                    return -1;
                }
                return 1;   
            }
        }
    }

}