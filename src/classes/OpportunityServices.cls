public with sharing class OpportunityServices {
    public static Opportunity GetOpportunity(Id oppId){
        Opportunity results = new Opportunity();
        if(oppId != null){
            results = [SELECT Id, Name,
            Account.Name,
            Account.SAP_Account_ID__c,
            AccountId,
            Bonded_Job__c,
            CreatedDate,
            EBW__c,
            Hit_With_Hail__c,
            Project_Address__c,
            Project_Name__c,
            Project_Status_lookup__c,
            PV_Installation__c,
            Quote_Id_Source_for_SAP__c,
            SAP_ContractId__c,
            SAP_Project_Number__c, 
            StartDate__c,
            Storm_Damage__c,
            Tax_Exempt__c,
            Warranty_Terminated__c,
            Warranty_Transfer__c
            FROM Opportunity 
            WHERE Id=:oppId];
        }
        return results;
    }
}