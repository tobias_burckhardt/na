/**
* @author Garvit Totuka
* @date 18 Sep 2017
*
* @group user Sharing Management
*
* @description Opportunity Share Helper class for creation and deletion of OpportunityShare Object through Opportunity, users and accounts
*/
public with Sharing class OpportunityShareDao {

    /**
    * @description create Opportunity Share for incoming accounts and users
    * @param accountIds Set of Account Ids
    * @param userIds Set of User Ids
    * @param accessType String value of Opportunity Access Level : Read, Edit, Full
    * @return void insert the Opportunity Share Object
    */
    public static void createOpportunityShare(Set<ID> accountIds, Set<ID> userIds, String accessType) {
        List<OpportunityShare> opptyShareToInsert = new List<OpportunityShare>();
        List<User> usrList = UserDao.getUsersByIds(userIds);
        Map<Id, Set<Id>> userIdsByAccountId = UserDao.createSetOfUserIdsByAccountId(usrList);

        List<Account> accountList = [
            SELECT Id, Name, (SELECT Id, Name, AccountId, OwnerId FROM Opportunities)
            FROM Account
            WHERE Id IN:accountIds
        ];

        for (Account acct : accountList) {
            for (Opportunity oppty : acct.Opportunities) {
                if (userIdsByAccountId.get(acct.Id) != NULL && userIdsByAccountId.containsKey(acct.Id)) {
                    for (Id usrOrGrpId : userIdsByAccountId.get(acct.Id)) {
                        if (usrOrGrpId != oppty.OwnerId) {
                            opptyShareToInsert.add(new OpportunityShare(
                                    OpportunityId = oppty.Id,
                                    UserOrGroupId = usrOrGrpId,
                                    OpportunityAccessLevel = accessType,
                                    RowCause = Schema.OpportunityShare.RowCause.Manual
                            ));
                        }
                    }
                }
            }
        }

        if (opptyShareToInsert != NULL && !opptyShareToInsert.isEmpty() && opptyShareToInsert.size() > 0)  {
            try {
                System.debug(opptyShareToInsert);
                insert opptyShareToInsert;
            } catch (DMLException ex) {
                System.debug(LoggingLevel.DEBUG, '\n$ ex: ' + ex + '\n');
                for (Integer i = 0; i < ex.getNumDml(); i++) {
                    ExceptionLogger.logError(ex, ex.getDmlId(i));
                }
                UserDao.exceptionMailToAdminUsers(ex);
                throw ex;
            }
        }

    }

    @future
    public static void createOpportunityShare_Future(Set<ID> accountIds, Set<ID> userIds, String accessType) {
        createOpportunityShare(accountIds, userIds, accessType);
    }

    /**
    * @description delete Opportunity Share for incoming opportunities
    * @param opportunityIds Set of Opportunity Ids
    * @return void delete the Opportunity Share Object based on Row Cause : Manual
    */
    public static void deleteOpportunityShare(Set<ID> opportunityIds) {
        List<OpportunityShare> opptyShareToDelete = new List<OpportunityShare>();
        for (OpportunityShare opptyShare : [SELECT OpportunityId, RowCause
                                            FROM OpportunityShare
                                            WHERE RowCause  = :'Manual' AND OpportunityId IN :opportunityIds
                                           ]) {
            opptyShareToDelete.add(opptyShare);
        }

        if (opptyShareToDelete != NULL && !opptyShareToDelete.isEmpty() && opptyShareToDelete.size() > 0) {
            try {
                delete opptyShareToDelete;
            } catch (DMLException ex) {
                for (Integer i = 0; i < ex.getNumDml(); i++) {
                    ExceptionLogger.logError(ex, ex.getDmlId(i));
                }
                UserDao.exceptionMailToAdminUsers(ex);
                throw ex;
            }
        }

    }

    @future
    public static void deleteOpportunityShare_Future(Set<ID> opportunityIds) {
        deleteOpportunityShare(opportunityIds);
    }

    /**
    * @description delete Opportunity Share for incoming users
    * @param userIds Set of User Ids
    * @return void delete the Opportunity Share Object based on Row Cause : Manual
    */
    public static void deleteOpportunityShareFromUser(Set<ID> userIds) {
        List<OpportunityShare> opptyShareToDelete = new List<OpportunityShare>();
        for (OpportunityShare opptyShare : [SELECT OpportunityId, RowCause
                                            FROM OpportunityShare
                                            WHERE RowCause  = :'Manual' AND  UserOrGroupId IN :userIds
                                           ]) {
            opptyShareToDelete.add(opptyShare);
        }

        if (opptyShareToDelete != NULL && !opptyShareToDelete.isEmpty() && opptyShareToDelete.size() > 0 ) {
            try {
                delete opptyShareToDelete;
            } catch (DMLException ex) {
                for (Integer i = 0; i < ex.getNumDml(); i++) {
                    ExceptionLogger.logError(ex, ex.getDmlId(i));
                }
                UserDao.exceptionMailToAdminUsers(ex);
                throw ex;
            }
        }

    }

    @future
    public static void deleteOpportunityShareFromUser_Future(Set<ID> userIds) {
        deleteOpportunityShareFromUser(userIds);
    }

}