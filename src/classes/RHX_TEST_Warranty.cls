@isTest(SeeAllData=true)
public class RHX_TEST_Warranty {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM Warranty__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new Warranty__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}