public with sharing class PickApproversModel {
	public enum ErrorCode { DUPLICATE_NAME_ERROR, DUPLICATE_NUM_ERROR, DATA_ERROR }
	@TestVisible final Integer NUM_OF_APPROVERS = 8;
	@TestVisible public List<ApproverWrapper> approvers;
	List<ApproverWrapper> nextUp;

	public PickApproversModel() {
		this.approvers = new List<ApproverWrapper>();
		this.nextUp = new List<ApproverWrapper>();
		for (Integer i=0; i<NUM_OF_APPROVERS; i++) {
			this.nextUp.add(new ApproverWrapper(null, i));
		}
	}

	/**
	 * @param existingApprovers key for this map represents the approver number
	 */
	public PickApproversModel(Map<Integer, User> existingApprovers) {
		System.debug('CONSTRUCTOR!!');
		buildLists(existingApprovers);
	}

	private void buildLists(Map<Integer, User> existingApprovers) {
		this.approvers = new List<ApproverWrapper>();
		this.nextUp = new List<ApproverWrapper>();
		for (Integer i=0; i<NUM_OF_APPROVERS; i++) {
			if (existingApprovers.containsKey(i)) {
				this.approvers.add(new ApproverWrapper(existingApprovers.get(i), i));				
			} else {
				this.nextUp.add(new ApproverWrapper(null, i));
			}
		}		
	}

	public List<ApproverWrapper> getApprovers() {
		return approvers;
	}

	public void tradePositions(Integer i, Integer j) {
		Id tempId = approvers[i].placeholderRequest.Approver_1__c;
		approvers[i].placeholderRequest.Approver_1__c = approvers[j].placeholderRequest.Approver_1__c;
		approvers[j].placeholderRequest.Approver_1__c = tempId;
	}

	public void refresh() {
		Set<Integer> existingApproverNumbers = new Set<Integer>();
		for (ApproverWrapper approver : approvers) {
			System.debug('(' + approver.placeholderRequest.Approver_1__c + ', '+approver.pos+')');
			existingApproverNumbers.add(approver.pos);
		}
		nextUp = new List<ApproverWrapper>();
		for (Integer i=0; i<NUM_OF_APPROVERS; i++) {
			if (!existingApproverNumbers.contains(i)) {
				nextUp.add(new ApproverWrapper(null, i));
			}
		}
	}

	public Map<Integer, Id> outputApproversAsMap() {
		Map<Integer, Id> currentStateOfApproverIds = new Map<Integer, Id>();
		for (Integer i=0; i<approvers.size(); i++) {
			if (approvers[i].placeholderRequest.Approver_1__c != null) {
				currentStateOfApproverIds.put(approvers[i].pos, approvers[i].placeholderRequest.Approver_1__c);
			} else {
				// This case should be caught with the hasError function
				throw new PickApproversException('There is an error. Why did you come here?');			
			}
		}
		return currentStateOfApproverIds;
	}

	public Set<ErrorCode> getErrors() {
		Set<ErrorCode> errorCodes = new Set<ErrorCode>();
		if (hasDuplicateName()) {
			errorCodes.add(ErrorCode.DUPLICATE_NAME_ERROR);
		}
		if (hasDuplicateNum()) {
			errorCodes.add(ErrorCode.DUPLICATE_NUM_ERROR);
		}
		if (hasDataError()) {
			errorCodes.add(ErrorCode.DATA_ERROR);
		}
		return errorCodes;
	}

	@TestVisible private Boolean hasDuplicateName() {
		Set<Id> visitedIds = new Set<Id>();
		for (ApproverWrapper approver : approvers) {
			if (approver.placeholderRequest.Approver_1__c != null) {
				if (visitedIds.contains(approver.placeholderRequest.Approver_1__c)) {
					return true;
				}
				visitedIds.add(approver.placeholderRequest.Approver_1__c);
			}
		}
		return false;
	}

	@TestVisible private Boolean hasDuplicateNum() {
		Set<Integer> visitedNums = new Set<Integer>();
		for (ApproverWrapper approver : approvers) {
			if (approver.pos != null) {
				if (visitedNums.contains(approver.pos) || approver.pos < 0 || approver.pos > NUM_OF_APPROVERS-1 ) {
					return true;
				}
				visitedNums.add(approver.pos);
			}
		}
		return false;
	}

	@TestVisible private Boolean hasDataError() {
		for (ApproverWrapper approver : approvers) {
			if (hasDataError(approver)) {
				System.debug(approvers);
				return true;
			}
		}
		return false;
	}

	private Boolean hasDataError(ApproverWrapper approver) {
		return approver.placeholderRequest.Approver_1__c == null || approver.pos == null;
	}

	public Integer getNextUp() {
		if (nextUp.isEmpty()) {
			return -1; // this means approvers list is full
		}
		return nextUp[0].pos;
	}

	/**
	 * @param posRemove specific approver number to remove (i.e. ApproverX)
	 */
	public ApproverWrapper remove(Integer posRemove) {
		Integer removeIndex; // different than posRemove
		for (Integer i=0 ;i<approvers.size(); i++) {
			if (approvers[i].pos == posRemove) {
				removeIndex = i;
				break;
			}
		}
		if (removeIndex == null) {
			throw new PickApproversException('Cannot find position to remove');
		}
		nextUp.add(new ApproverWrapper(null, posRemove));
		nextUp.sort();
		return approvers.remove(removeIndex);
	}

	public Integer addAndGetNextUp(User newApprover) {
		if (getNextUp() < 0) {
			throw new PickApproversException('No more room for to add approver');
		}
		approvers.add(new ApproverWrapper(newApprover, getNextUp()));
		nextUp.remove(0);
		return getNextUp();
	}

	public class ApproverWrapper implements Comparable {
		public Request_for_Project_Approval__c placeholderRequest {get;set;}
		public User approver {get;set;}
		public Integer pos {get;set;}
		public Integer posFromOne {
			get;
			set {
				posFromOne = value;
				pos = value-1;
			}
		}
		public ApproverWrapper(User approver, Integer positionFromZero) {
			this.approver = approver;
			this.pos = positionFromZero;
			this.posFromOne = positionFromZero+1;
			this.placeholderRequest = new Request_for_Project_Approval__c();
			if (approver != null) {
				this.placeholderRequest.Approver_1__c = approver.Id;
			}
		}
		public Integer compareTo(Object compareTo) {
	        ApproverWrapper compareToApprover = (ApproverWrapper)compareTo;
	        if (this.pos == compareToApprover.pos) return 0;
	        if (this.pos > compareToApprover.pos) return 1;
	        return -1;        
	    }
	}

	public class PickApproversException extends Exception {}
}