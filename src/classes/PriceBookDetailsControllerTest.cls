@isTest
public class PriceBookDetailsControllerTest {
    static testMethod void testFunctionalities(){
        List<Product2> products = PricingTestDataFactory.createProducts();
        Id customPricebookId = PricingTestDataFactory.createCustomPricebook();
        List<String> productIds = new List<String>();
        for(Product2 product:products){
            productIds.add(product.Id);
        }
        Boolean areProductsAddedToStdPriceBook = PricingTestDataFactory.addProductsToStandardPriceBook(productIds);
        //System.debug('areProductsAddedToStdPriceBook--'+areProductsAddedToStdPriceBook);
        List<PricebookEntry> insertedPricebookEntries = PricingTestDataFactory.addProductsToCustomPriceBook(productIds,customPricebookId);
        Test.startTest();
        	PriceBookDetailsController controllerObj = new PriceBookDetailsController();
        	System.assertNotEquals(null, controllerObj);
        	String ajaxData = '{}';
            System.assertNotEquals(null, PriceBookDetailsController.getSAPRelatedPriceBooks(ajaxData));
        	ajaxData = '{"productNameOrArticleCode":"'+products.get(0).Name+'","selectedPricebookId":"'+customPricebookId+'","OffsetSize":0}';
            System.assertNotEquals(null, PriceBookDetailsController.getPriceBookEntriesBySelectedPriceBook(ajaxData));
        	ajaxData = '{"selectedEntry":"'+insertedPricebookEntries.get(0).Id+'","unSelectedEntry":"'+insertedPricebookEntries.get(1).Id+'"}';
            System.assertNotEquals(null, PriceBookDetailsController.updateExcludedPBEntries(ajaxData));
        	ajaxData = '{"productNameOrArticleCode":"'+products.get(0).Name+'","selectedPricebookId":"'+customPricebookId+'","OffsetSize":0}';
            System.assertNotEquals(null, PriceBookDetailsController.getPriceBookEntriesBySelectedPriceBook(ajaxData));
        Test.stopTest();
    }
}