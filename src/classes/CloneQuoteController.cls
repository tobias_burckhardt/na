global class CloneQuoteController {
    webService static string cloneParent(string qid){
        try {
            Quote quo = [SELECT AccountId,Account__c,Application__c,BillingAddress,BillingCity,BillingCountry,BillingGeocodeAccuracy,
                     BillingLatitude,BillingLongitude,BillingName,BillingPostalCode,BillingState,BillingStreet,Business_Unit_Region__c,CanCreateQuoteLineItems,Conga_Template_Id__c,
                     Conga_Template_Parameters__c,ContactId,ContractId,CreatedById,CreatedDate,Description,Discount,Email,ExpirationDate,Fax,FOB_Point__c,Freight_Terms__c,
                     GrandTotal,Id,IsDeleted,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,LineItemCount,Margin_Indicator__c,
                     Name,Notes__c,OpportunityId,OwnerId,Phone,Presented_Date__c,Pricebook2Id,Price_Book__c,Price_Tiering_for_SPQ__c,Price_Tiering_for_SPA__c,Price_Tiering__c,
                     QuoteNumber,QuoteToAddress,QuoteToCity,QuoteToCountry,QuoteToGeocodeAccuracy,QuoteToLatitude,QuoteToLongitude,QuoteToName,
                     QuoteToPostalCode,QuoteToState,QuoteToStreet,RecordTypeId,Standard_Cost_Flag__c,
                     Subtotal,SystemModstamp,Tax,TotalPrice,Total_Cost__c,Total_Margin__c,Language__c FROM Quote  where  id=:qid] ;
            Quote newquo  = new Quote();
            newquo = quo.clone();
           newquo.Name = quo.Name + ' Revision '+ String.valueof(system.today())+' '+string.valueOf(system.now().hour())+':'+string.valueOf(system.now().minute());
            newquo.Status = 'Draft';
            insert newquo;
            /*list<QuoteLineItem> qlilist = [SELECT Article__c,Original_Creation_Date__c,Base_Price__c,Base_Scale_Quantity__c,Base_Unit__c,Base_Weight_Unit__c,CreatedById,CreatedDate,Currency__c,Description,Discount,Discount_Margin__c,Id,
                IsDeleted,LastModifiedById,LastModifiedDate,LineNumber,ListPrice,Margin_Indicator__c,Margin_Status_del__c,Margin__c,Maximum_Quantity__c,NetWeightPerUnit__c,Net_Cost__c,
                OpportunityLineItemId,Packaging__c,PricebookEntryId,Pricetiering__c,Price_Tiering_Available__c,Product2Id,Quantity,QuoteId,Scale_Quantity_1__c,Scale_Quantity_2__c,Scale_Quantity_3__c,
                Scale_Quantity_4__c,Scale_Quantity_5__c,Scale_Quantity_6__c,Scale_Quantity_7__c,Scale_Quantity_8__c,Scale_Quantity_9__c,Scale_Quantity_10__c,Scale_Rate_1__c,Scale_Rate_2__c,
                Scale_Rate_3__c,Scale_Rate_4__c,Scale_Rate_5__c,Scale_Rate_6__c,Scale_Rate_7__c,Scale_Rate_8__c,Scale_Rate_9__c,Scale_Rate_10__c,ServiceDate,SortOrder,Standard_Cost_Available__c,
                Standard_Cost_for_Roll_up__c,Standard_Cost_Override__c,Standard_Cost__c,Subtotal,SystemModstamp,TotalPrice,Total_Cost__c,Total_Line_Weight__c,UnitPrice FROM QuoteLineItem where QuoteId =:qid order by Original_Creation_Date__c desc];
            for(QuoteLineItem qli : qlilist){
                QuoteLineItem newqli = new QuoteLineItem();
                newqli = qli.clone();
                newqli.QuoteId = newquo.id;
                newqlilist.add(newqli);
            }
            insert newqlilist;*/
            return string.valueOf(newquo.Id);
        }
        catch(Exception e) {
            return e.getMessage();
        }
    }
    
}