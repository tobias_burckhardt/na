global without sharing class GoogleChartsController {
    @RemoteAction
    global static list<wrapperSieve> loadSievesMix(ID mixID) {
        Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Mix__c.fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();

        String theQuery = 'SELECT ';

        for (Schema.SObjectField s : fldObjMapValues) {
            String theName = s.getDescribe().getName();
            theQuery += theName + ',';
        }

        theQuery = theQuery.subString(0, theQuery.length() - 1);
        theQuery += ' FROM Mix__c where ID =: mixID order by CreatedDate DESC limit 1';

        list<Mix__c> mixList = Database.query(theQuery);

        if (mixList.isEmpty()) {
            system.debug('********GoogleChartsController: No Mix was found that matches the criteria. theQuery = ' + theQuery);
            return null;
        }

        list<wrapperSieve> resultList = new list<wrapperSieve>();

        Map<String, Schema.SObjectField> fldObjMap2 = schema.SObjectType.Sieve_ideal__c.fields.getMap();
        List<Schema.SObjectField> fldObjMapValues2 = fldObjMap2.values();

        String theQuery2 = 'SELECT ';

        for (Schema.SObjectField s : fldObjMapValues2) {
            String theName2 = s.getDescribe().getName();
            theQuery2 += theName2 + ',';
        }

        theQuery2 = theQuery2.subString(0, theQuery2.length() - 1);
        theQuery2 += ' FROM Sieve_ideal__c order by Order__c ASC';
        list<Sieve_ideal__c> idealList = Database.query(theQuery2);

        if (idealList.isEmpty()) {
            system.debug('********GoogleChartsController: No Sieve_ideal record were found that matche the criteria. theQuery2 = ' + theQuery2);
            return null;
        }

        List<Decimal> highPoints = new list<Decimal>();
        List<Decimal> lowPoints = new list<Decimal>();

        for (Mix__c m : mixList) {
            if (m.System_of_Measure__c == 'US Customary') {
                for (Sieve_ideal__c si : idealList) {
                    if (si.isMetric__c == false) {
                        if (m.Max_Agg_Diameter__c == '3/8"' && m.Reinforcement__c == 'Normal') {
                            highPoints.add(si.Std_38_high__c);
                            lowPoints.add(si.Std_38_low__c);
                        } else if (m.Max_Agg_Diameter__c == '3/8"' && m.Reinforcement__c == 'Highly Reinforced') {
                            highPoints.add(si.HR_38_high__c);
                            lowPoints.add(si.HR_38_low__c);
                        } else if (m.Max_Agg_Diameter__c == '1/2"' && m.Reinforcement__c == 'Normal') {
                            highPoints.add(si.Std_12_high__c);
                            lowPoints.add(si.Std_12_low__c);
                        } else if (m.Max_Agg_Diameter__c == '1/2"' && m.Reinforcement__c == 'Highly Reinforced') {
                            highPoints.add(si.HR_12_high__c);
                            lowPoints.add(si.HR_12_low__c);
                        } else if (m.Max_Agg_Diameter__c == '3/4"' && m.Reinforcement__c == 'Normal') {
                            highPoints.add(si.Std_34_high__c);
                            lowPoints.add(si.Std_34_low__c);
                        } else if (m.Max_Agg_Diameter__c == '3/4"' && m.Reinforcement__c == 'Highly Reinforced') {
                            highPoints.add(si.HR_34_high__c);
                            lowPoints.add(si.HR_34_low__c);
                        }
                    }
                }

                if (highPoints.isEmpty() || lowPoints.isEmpty()) {
                    system.debug('********GoogleChartsController: the highPoints list or lowPoints list was empty.');
                    system.debug('********GoogleChartsController: highPoints = ' + highPoints);
                    system.debug('********GoogleChartsController: lowPoints = ' + lowPoints);
                    return null;
                }

                resultList.add(new wrapperSieve('200', highPoints[0], lowPoints[0], m.Sieve_ord_16_US__c));
                resultList.add(new wrapperSieve('150', highPoints[1], lowPoints[1], m.Sieve_ord_15_US__c));
                resultList.add(new wrapperSieve('100', highPoints[2], lowPoints[2], m.Sieve_ord_14_US__c));
                resultList.add(new wrapperSieve('50', highPoints[3], lowPoints[3], m.Sieve_ord_13_US__c));
                resultList.add(new wrapperSieve('30', highPoints[4], lowPoints[4], m.Sieve_ord_12_US__c));
                resultList.add(new wrapperSieve('20', highPoints[5], lowPoints[5], m.Sieve_ord_11_US__c));
                resultList.add(new wrapperSieve('16', highPoints[6], lowPoints[6], m.Sieve_ord_10_US__c));
                resultList.add(new wrapperSieve('10', highPoints[7], lowPoints[7], m.Sieve_ord_9_US__c));
                resultList.add(new wrapperSieve('8', highPoints[8], lowPoints[8], m.Sieve_ord_8_US__c));
                resultList.add(new wrapperSieve('4', highPoints[9], lowPoints[9], m.Sieve_ord_7_US__c));
                resultList.add(new wrapperSieve('3/8', highPoints[10], lowPoints[10], m.Sieve_ord_6_US__c));
                resultList.add(new wrapperSieve('1/2', highPoints[11], lowPoints[11], m.Sieve_ord_5_US__c));
                resultList.add(new wrapperSieve('3/4', highPoints[12], lowPoints[12], m.Sieve_ord_4_US__c));
                resultList.add(new wrapperSieve('1', highPoints[13], lowPoints[13], m.Sieve_ord_3_US__c));
                resultList.add(new wrapperSieve('1 1/2', highPoints[14], lowPoints[14], m.Sieve_ord_2_US__c));
                resultList.add(new wrapperSieve('2', highPoints[15], lowPoints[15], m.Sieve_ord_1_US__c));

            } else {

                // if Max_Agg_Diameter__c = 8mm, 14mm, 20mm, 3/8", 1/2", 3/4"
                for (Sieve_ideal__c si : idealList) {
                    if (si.isMetric__c == true) {

                        if (m.Max_Agg_Diameter__c == '8mm' && m.Reinforcement__c == 'Normal') {
                            highPoints.add(si.Std_8mm_high__c);
                            lowPoints.add(si.Std_8mm_low__c);
                        } else if (m.Max_Agg_Diameter__c == '8mm' && m.Reinforcement__c == 'Highly Reinforced') {
                            highPoints.add(si.HR_8mm_high__c);
                            lowPoints.add(si.HR_8mm_low__c);
                        } else if (m.Max_Agg_Diameter__c == '14mm' && m.Reinforcement__c == 'Normal') {
                            highPoints.add(si.Std_14mm_high__c);
                            lowPoints.add(si.Std_14mm_low__c);
                        } else if (m.Max_Agg_Diameter__c == '14mm' && m.Reinforcement__c == 'Highly Reinforced') {
                            highPoints.add(si.HR_14mm_high__c);
                            lowPoints.add(si.HR_14mm_low__c);
                        } else if (m.Max_Agg_Diameter__c == '20mm' && m.Reinforcement__c == 'Normal') {
                            highPoints.add(si.Std_20mm_high__c);
                            lowPoints.add(si.Std_20mm_low__c);
                        } else if (m.Max_Agg_Diameter__c == '20mm' && m.Reinforcement__c == 'Highly Reinforced') {
                            highPoints.add(si.HR_20mm_high__c);
                            lowPoints.add(si.HR_20mm_low__c);
                        }
                    }
                }

                resultList.add(new wrapperSieve('0.08', highPoints[0], lowPoints[0], m.Sieve_ord_16__c));
                resultList.add(new wrapperSieve('0.16', highPoints[1], lowPoints[1], m.Sieve_ord_15__c));
                resultList.add(new wrapperSieve('0.315', highPoints[2], lowPoints[2], m.Sieve_ord_14__c));
                resultList.add(new wrapperSieve('0.63', highPoints[3], lowPoints[3], m.Sieve_ord_13__c));
                resultList.add(new wrapperSieve('1.25', highPoints[4], lowPoints[4], m.Sieve_ord_12__c));
                resultList.add(new wrapperSieve('2.5', highPoints[5], lowPoints[5], m.Sieve_ord_11__c));
                resultList.add(new wrapperSieve('5', highPoints[6], lowPoints[6], m.Sieve_ord_10__c));
                resultList.add(new wrapperSieve('10', highPoints[7], lowPoints[7], m.Sieve_ord_9__c));
                resultList.add(new wrapperSieve('12.5', highPoints[8], lowPoints[8], m.Sieve_ord_8__c));
                resultList.add(new wrapperSieve('14', highPoints[9], lowPoints[9], m.Sieve_ord_7__c));
                resultList.add(new wrapperSieve('16', highPoints[10], lowPoints[10], m.Sieve_ord_6__c));
                resultList.add(new wrapperSieve('20', highPoints[11], lowPoints[11], m.Sieve_ord_5__c));
                resultList.add(new wrapperSieve('25', highPoints[12], lowPoints[12], m.Sieve_ord_4__c));
                resultList.add(new wrapperSieve('31.5', highPoints[13], lowPoints[13], m.Sieve_ord_3__c));
                resultList.add(new wrapperSieve('63', highPoints[14], lowPoints[14], m.Sieve_ord_2__c));
                resultList.add(new wrapperSieve('80', highPoints[15], lowPoints[15], m.Sieve_ord_1__c));
            }

        }


        return resultList;
    }

    @RemoteAction
    global static list<wrapperSieve> loadSievesMaterial(ID materialID) {
        Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Material__c.fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();


        String theQuery = 'SELECT ';

        for (Schema.SObjectField s : fldObjMapValues) {
            String theName = s.getDescribe().getName();
            theQuery += theName + ',';
        }

        theQuery = theQuery.subString(0, theQuery.length() - 1);
        theQuery += ' FROM Material__c where ID =: materialID order by CreatedDate DESC limit 1';

        list<Material__c> matList = Database.query(theQuery);

        list<wrapperSieve> resultList = new list<wrapperSieve>();


        for (Material__c m : matList) {
            //Input in US
            if (m.Unit_System__c == 'US Customary') {
                resultList.add(new wrapperSieve('200', null, null, m.Sieve_200_calculated__c));
                resultList.add(new wrapperSieve('150', null, null, m.Sieve_150_calculated__c));
                resultList.add(new wrapperSieve('100', null, null, m.Sieve_100_calculated__c));
                resultList.add(new wrapperSieve('50', null, null, m.Sieve_40_calculated__c));
                resultList.add(new wrapperSieve('30', null, null, m.Sieve_30_calculated__c));
                resultList.add(new wrapperSieve('20', null, null, m.Sieve_20_calculated__c));
                resultList.add(new wrapperSieve('16', null, null, m.Sieve_16_calculated__c));
                resultList.add(new wrapperSieve('10', null, null, m.Sieve_10_calculated__c));
                resultList.add(new wrapperSieve('8', null, null, m.Sieve_8_calculated__c));
                resultList.add(new wrapperSieve('4', null, null, m.Sieve_4_calculated__c));
                resultList.add(new wrapperSieve('3/8', null, null, m.Sieve_3_8_calculated__c));
                //resultList.add(new wrapperSieve('1/2', null, null, m.Sieve_1_2_calculated__c));
                resultList.add(new wrapperSieve('3/4', null, null, m.Sieve_3_4_calculated__c));
                resultList.add(new wrapperSieve('1', null, null, m.Sieve_1_calculated__c));
                resultList.add(new wrapperSieve('1 1/2', null, null, m.Sieve_1_5_calculated__c));
                resultList.add(new wrapperSieve('2', null, null, m.Sieve_2_calculated__c));

            } else {
                resultList.add(new wrapperSieve('0.08', null, null, m.Sieve_0_08mm_calculated__c));
                resultList.add(new wrapperSieve('0.16', null, null, m.Sieve_0_16mm_calculated__c));
                resultList.add(new wrapperSieve('0.315', null, null, m.Sieve_0_315mm_calculated__c));
                resultList.add(new wrapperSieve('0.63', null, null, m.Sieve_0_63mm_calculated__c));
                resultList.add(new wrapperSieve('1.25', null, null, m.Sieve_1_25mm_calculated__c));
                resultList.add(new wrapperSieve('2.5', null, null, m.Sieve_2_5mm_calculated__c));
                resultList.add(new wrapperSieve('5', null, null, m.Sieve_5mm_calculated__c));
                resultList.add(new wrapperSieve('10', null, null, m.Sieve_10mm_calculated__c));
                resultList.add(new wrapperSieve('12.5', null, null, m.Sieve_12_5mm_calculated__c));
                resultList.add(new wrapperSieve('14', null, null, m.Sieve_14mm_calculated__c));
                resultList.add(new wrapperSieve('16', null, null, m.Sieve_16mm_calculated__c));
                resultList.add(new wrapperSieve('20', null, null, m.Sieve_20mm_calculated__c));
                resultList.add(new wrapperSieve('25', null, null, m.Sieve_25mm_calculated__c));
                resultList.add(new wrapperSieve('31.5', null, null, m.Sieve_31_5mm_calculated__c));
                resultList.add(new wrapperSieve('63', null, null, m.Sieve_63mm_calculated__c));
                resultList.add(new wrapperSieve('80', null, null, m.Sieve_80mm_calculated__c));


            }

        }


        return resultList;
    }

    global class wrapperSieve {

        global string sieveLabel { get; set; }
        global decimal highValue { get; set; }
        global decimal lowValue { get; set; }
        global decimal calculatedValue { get; set; }


        global wrapperSieve(String label, Decimal high, Decimal low, Decimal calc) {
            sieveLabel = label;
            highValue = high;
            lowValue = low;
            calculatedValue = calc;

        }

    }


}