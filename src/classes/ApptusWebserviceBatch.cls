global class ApptusWebserviceBatch implements Database.Batchable<sObject> {
	
	String query;
	
	global ApptusWebserviceBatch() {
		query = 'SELECT Id FROM Apttus_Proposal__Proposal__c ' +
				'WHERE Apptus_Initial_Configuration_Completed__c = false ' + 
				'AND Apptus_Initial_Configuration_Priority__c != null '+
				'ORDER BY Apptus_Initial_Configuration_Priority__c';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Apttus_Proposal__Proposal__c> scope) {
		try{
			ApttusAdhocWebserviceCalling.configurationFullProcess(scope[0].Id);
			scope[0].Apptus_Initial_Configuration_Completed__c = true;
			update scope[0];
		}catch(Exception e){}
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}

	global static void runBatch(){
		Database.executeBatch(new ApptusWebserviceBatch(), 1);
	}
	
}