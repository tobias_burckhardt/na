public with sharing class CommunityUpdateUserInfoController {
    
    public User Usr{get; set;}
    public Contact cont{get; set;}
    public string currentUser = UserInfo.getUserId();
    
    public CommunityUpdateUserInfoController(){
        try{
            Usr = [select name, email, phone, CompanyName, Street, Country, City, State, PostalCode, Department  from user where user.id =: currentUser];
        }catch(Exception e){System.debug(e.getMessage());}
        
    }
    
    public pageReference SaveUserInfo(){
        try{
            update Usr;
        }catch(Exception e){system.debug(e.getMessage());}
        pageReference p;
        p = new pageReference('/apex/CommunityUserSupport');
        p.setRedirect(true);
        return p;
        
    }
    
    public pageReference doCancel(){
        pageReference p;
        p = new pageReference('/apex/CommunityUserSupport');
        p.setRedirect(true);
        return p;
    }
}