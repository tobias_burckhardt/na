@isTest
public with sharing class ContentDocumentTriggerTest 
{
	static User adminUser;
	static ContentVersion testContentVersion;
	
	@testSetup static void setupCustomSetting() {
      AWS_Settings__c awsSetting = new AWS_Settings__c(AWS_Key__c = 'Test', S3_Bucket_Name__c = 'Test', SF_Library_Name__c = 'Test');
      insert awsSetting;
    }
    
	public static void setup()
	{
		TestingUtils.createValidLibrary(existingContentWorkspace[0].Name, true);
	}
	
	//Cannot create mock contentworkspaces, can only query for existing ones
	public static List<ContentWorkSpace> existingContentWorkSpace
	{  
		get
		{
			if(existingContentWorkspace == null)
			{
				existingContentWorkspace = [Select Id, Name from contentworkspace];
			}
			return existingContentWorkspace;
		}
		set;
	}
	
    static testMethod void test_createContentHeader() 
    {
		setup();
		    	
    	Test.startTest();
	    	testContentVersion  = TestingUtils.createContentVersions(1,'test',false)[0];
	    	testContentVersion.FirstPublishLocationId = existingContentWorkspace[0].Id;
	    	insert testContentVersion;
	    Test.stopTest();
	    
	    testContentVersion = [Select ContentDocumentId,FirstPublishLocationId From ContentVersion Where Id =: testContentVersion.Id];
	    ContentDocument doc = [Select Id, parentId, title from ContentDocument Where Id =: testContentVersion.ContentDocumentId];
	    
		List<Content_Header__c> contentHeader = [Select Id, Name From Content_Header__c Where Content_Document__c =: testContentVersion.contentDocumentId];
		System.assert(!contentHeader.isEmpty(), 'We expect a content header looking up to the created contentDocument to be created.');
		System.assertEquals(1, contentHeader.size(), 'We expect only one content header to be created');
		System.assertEquals(doc.title, contentHeader[0].Name, 'We expect the content header name to match the title of the content document');
    }
    
    static testMethod void test_createContentHeader_NoValidLibraryName() 
    {
		    	
    	Test.startTest();
	    	testContentVersion  = TestingUtils.createContentVersions(1,'test',false)[0];
	    	insert testContentVersion;
	    Test.stopTest();
	    
	    testContentVersion = [Select ContentDocumentId,FirstPublishLocationId From ContentVersion Where Id =: testContentVersion.Id];
	    ContentDocument doc = [Select Id, parentId, title from ContentDocument Where Id =: testContentVersion.ContentDocumentId];
	    
		List<Content_Header__c> contentHeader = [Select Id, Name From Content_Header__c Where Content_Document__c =: testContentVersion.contentDocumentId];
		System.assert(contentHeader.isEmpty(), 'We expect a content header looking up to the created contentDocument not to be created since there are no valid library names.');
    }

    static testMethod void test_deleteContentDocument()
    {
    	setup();

		testContentVersion  = TestingUtils.createContentVersions(1,'test',false)[0];
    	testContentVersion.FirstPublishLocationId = existingContentWorkspace[0].Id;
    	insert testContentVersion;
    	
    	testContentVersion = [Select ContentDocumentId,FirstPublishLocationId From ContentVersion Where Id =: testContentVersion.Id];
        ContentDocument doc = [Select Id, parentId, title from ContentDocument Where Id =: testContentVersion.ContentDocumentId];
    	
    	Test.startTest();
    		delete doc;
	    Test.stopTest();

	    List<Content_Header__c> contentHeader = [Select Id From Content_Header__c Where Content_Document__c =: testContentVersion.contentDocumentId];
		System.assert(contentHeader.isEmpty(), 'We expect a content header looking up to the created contentDocument to be deleted.');    
    }

}