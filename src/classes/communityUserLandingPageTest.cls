@isTest
private class communityUserLandingPageTest {
	private static communityUserLandingPage ctrl;
	private static User SCCZoneUser;
	private static User theOtherUser;
	private static map<String, String> params;
	private static list<Mix__c> mixes;
	private static list<Material__c> materials;
	
	
	private static void init(){
		// create an SCCZone User (and its required Contact and Account objects), and some other user to set as the owner of materials we shouldn't display
		map<String, sObject> objects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
		SCCZoneUser = (User) objects.get('SCCZoneUser');
		theOtherUser = (User) objects.get('salesUser');
		
		mixes = new list<Mix__c>();
		materials = new list<Material__c>();
		
		// generate four mix records owned by the SCCZoneUser
		params = new map<String, String>();
		for(integer i = 4; i > 0; i--){
			params.clear();
			params.put('mixName', 'Mix_' + i);
			params.put('ownerId', SCCZoneUser.Id);
			mixes.add(sika_SCCZoneTestFactory.createMix(params));
		}
		
		// generate six materials records owned by the SCCZoneUser
		params = new map<String, String>();
		for(integer i = 6; i > 0; i--){
			params.clear();
			params.put('materialName', 'Material_' + i);
			params.put('type', 'Sand');
			params.put('ownerId', SCCZoneUser.Id);
			materials.add(sika_SCCZoneTestFactory.createMaterial(params));
		}
		
		// generate two mix records owned by the other user
		params = new map<String, String>();
		for(integer i = 2; i > 0; i--){
			params.clear();
			params.put('mixName', 'Mix_' + i);
			params.put('ownerId', theOtherUser.Id);
			mixes.add(sika_SCCZoneTestFactory.createMix(params));
		}
		
		// generate two material records owned by the other user
		params = new map<String, String>();
		for(integer i = 2; i > 0; i--){
			params.clear();
			params.put('materialName', 'Material_' + i);
			params.put('type', 'Sand');
			params.put('ownerId', theOtherUser.Id);
			materials.add(sika_SCCZoneTestFactory.createMaterial(params));
		}
		
		system.runAs(SCCZoneUser){
			list<Database.saveResult> mixSR = Database.insert(mixes);
			for(Database.saveResult sr : mixSr){
				system.assert(sr.isSuccess());
			}
			
			list<Database.saveResult> materialSR = Database.insert(materials);
			for(Database.saveResult sr : materialSR){
				system.assert(sr.isSuccess());
			}
		}
		
		// set the page we'll be testing
		PageReference pageRef = Page.sika_SCCZoneUserHomePage;
		Test.setCurrentPage(pageRef);
		
		// need to bypass mix & material triggers
		sika_SCCZoneMixTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMixTriggerHelper.bypassMixOwnerTransferCheck = true;
		sika_SCCZoneMixTriggerHelper.bypassMixChatterAlert = true;
		sika_SCCZoneMixTriggerHelper.bypassUpdateSCMQuantity = true;
		
		sika_SCCZoneMaterialTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = true;
		sika_SCCZoneMaterialTriggerHelper.bypassPreventDelete = true;
		sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = true;
		
	}
	
	
	@isTest static void realTest(){
		init();
		Test.startTest();
		
		system.runAs(SCCZoneUser){
			ctrl = new communityUserLandingPage();
			
			// confirm that the getMixes query returns four records
			list<Mix__c> theMixes = ctrl.getMixes();
			system.assertEquals(theMixes.size(), 4);
	
			// confirm that the getMaterials query returns five records
			list<Material__c> theMaterials = ctrl.getMaterials();
			system.assertEquals(theMaterials.size(), 5);
	
			// confirm that all mixes and materials displayed belong to the user viewing the page
			for(Mix__c m : theMixes){
				system.assert(m.OwnerId == SCCZoneUser.Id);
			}
			for(Material__c m : theMaterials){
				system.assert(m.OwnerId == SCCZoneUser.Id);
			}
			
			// delete a mix
			Id deletingMixId = theMixes[0].Id;
			ctrl.deleteThisMix = deletingMixId;
			PageReference deleteMixResult = ctrl.deleteMix();
			system.assert(deleteMixResult.getUrl().contains('sika_SCCZoneUserHomePage'));
			
			// re-load the page and confirm that the mix was deleted
			ctrl = new communityUserLandingPage();
			
			theMixes.clear();
			theMixes = ctrl.getMixes();
			system.assertEquals(theMixes.size(), 3);
			for(Mix__c m : theMixes){
				system.assert(m.Id != deletingMixId);
			}
			
			
			// delete a material
			Id deletingMaterialId = theMaterials[0].Id;
			ctrl.deleteThisMaterial = deletingMaterialId;
			PageReference deleteMaterialResult = ctrl.deleteMaterial();
			system.assert(deleteMaterialResult.getUrl().contains('sika_SCCZoneUserHomePage'));
			
			// re-load the page and confirm that 5 materials are still being displayed, but the deleted material isn't.  (There were 6 in the database before we deleted one.)
			ctrl = new communityUserLandingPage();
			
			theMaterials.clear();
			theMaterials = ctrl.getMaterials();
			system.assertEquals(theMaterials.size(), 5);
			
			// make sure the deleted material isn't one of them...
			for(Material__c m : theMaterials){
				system.assert(m.Id != deletingMaterialId);
			}
			
			// the material shouldn't have actually been "deleted" -- it's just marked as deactivated.  Make sure this is the case. 
			Material__c deactivatedMaterialCheck = [SELECT Id, isActive__c FROM Material__c WHERE Id = :deletingMaterialId];
			system.assertEquals(deactivatedMaterialCheck.isActive__c, false);
			
			
			// delete all of the current user's mixes
			list<Mix__c> mixesToDelete = [SELECT OwnerId FROM Mix__c WHERE OwnerId = :SCCZoneUser.Id];
			list<Database.deleteResult> mixDR = Database.delete(mixesToDelete);
			for(Database.deleteResult dr : mixDR){
				system.assert(dr.isSuccess());
			}
			
			// delete (deactivate) all of the current user's materials
			list<Material__c> materialsToDeactivate = [SELECT OwnerId FROM Material__c WHERE OwnerId = :SCCZoneUser.Id];
			for(Material__c m : materialsToDeactivate){
				m.isActive__c = false;
			}
			list<Database.saveResult> materialSR = Database.update(materialsToDeactivate);
			for(Database.saveResult sr : materialSR){
				system.assert(sr.isSuccess());
			}
			
			// re-load page and confirm that it doesn't blow up.
			ctrl = new communityUserLandingPage();
			
			theMixes.clear();
			theMixes = ctrl.getMixes();
			system.assertEquals(theMixes.size(), 0);
			
			theMaterials.clear();
			theMaterials = ctrl.getMaterials();
			system.assertEquals(theMaterials.size(), 0);
			
		}
		Test.stopTest();
	}


	@isTest static void testPopups(){
		ctrl = new communityUserLandingPage();
		ctrl.closePopup();
		system.assertEquals(ctrl.displayPopUp, false);

		ctrl.showPopup();
		system.assertEquals(ctrl.displayPopUp, true);

		ctrl.closePopup2();
		system.assertEquals(ctrl.displayPopUp2, false);

		ctrl.showPopup2();
		system.assertEquals(ctrl.displayPopUp2, true);
	}

	@isTest static void testLoadAction(){
        User guestUser = sika_SCCZoneTestFactory.createGuestUser();
        insert(guestUser);

        PageReference p;

        System.runAs(guestUser){
            ctrl = new communityUserLandingPage();
            p = ctrl.loadAction();
            system.assert(String.valueOf(p).contains('/SCCZone/sika_SCCZoneLoginPage'));
        }
    }

}