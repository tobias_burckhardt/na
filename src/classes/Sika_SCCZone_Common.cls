public without sharing class Sika_SCCZone_Common {

    // Sales Rep Profile Names
    public static set<String> salesRepProfiles = new set<String>{'SCC Zone Sales Rep',
                                                                 'Sika Sales User',
                                                                 'SikaExternalRep'};                                                   
    // SCC Zone Sales Rep AccountTeamMember Role name
    public static String SCCZoneSalesRepATMRole = 'SCC Zone Sales';
    
    // SysAdmin UserRole name
    public static String adminUserRoleName = 'Sika Admin';

    // SCCZone User Profile name (authenticated users with SCC Zone access via a Communities license)
    public static String SCCZoneUserProfileUserName = 'SCC Zone Community User';
    public static String SCCZoneUserProfileLoginName = 'SCCZone Community Login';
    public static List<String> SCCZoneUserProfileNames = new List<String>{
        SCCZoneUserProfileUserName,
        SCCZoneUserProfileLoginName
    };

    // SCCZone User Profile Id (based on the above)
    private static Id sccId;
    public static Id SCCZoneUserProfileId(){
        if(sccId == null){
            Id theId = [SELECT Name, Id FROM Profile WHERE Name = :Sika_SCCZone_Common.SCCZoneUserProfileUserName Limit 1].Id;    
            sccId = theId;
        }
        return sccId;
    }

    // Guest (anonymous) user profile ID
    public static Id guestUserProfileID(){
        Id guestProfileId = [SELECT Id, UserType from User WHERE UserType = 'GUEST' Limit 1].Id;
        return guestProfileId;
    }

}