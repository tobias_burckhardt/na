public without sharing class sika_SCCZoneSCMWeightHelper {
    




/*


            DEPRECATED










	
	// Method for (re)calculating the SCM Weight, based on the Mix's SCM Ratio (A/(A+C)...more or less) and the weight of the cement in the Mix
    public static void calculate_SCM_Weight(Id mixID){
        Mix_Material__c theSCM;
        Integer cementIndex;
        String cementUnit;

        Integer SCMIndex;
        Decimal SCMQuantity;

        list<Mix_Material__c> cement_and_SCM = [SELECT Id, 
                                                       Mix__c, 
                                                       Quantity__c, 
                                                       Unit__c, 
                                                       Material__r.Type__c, 
                                                       Mix__r.Target_SCM_Ratio__c 
                                                  FROM Mix_Material__c 
                                                 WHERE Mix__c = :mixID 
                                                   AND (Material__r.Type__c = 'Cement' OR Material__r.Type__c = 'SCM')];
        
        // there should be at most one cement and at most one SCM in a Mix
        if(cement_and_SCM.size() > 0){
            for(integer i = 0; i < cement_and_SCM.size(); i++){
                if(cement_and_SCM[i].Material__r.Type__c == 'Cement'){
                    cementIndex = i;
                    continue;
                }
                if(cement_and_SCM[i].Material__r.Type__c == 'SCM'){
                    SCMIndex = i;
                }
            }
        }
//TODO: move this (& above) to the  the mix materials trigger
        if(cementIndex != null && SCMIndex != null){
            theSCM = cement_and_SCM[SCMIndex];
            cementUnit = cement_and_SCM[cementIndex].Unit__c;
            Decimal cementWeight = cement_and_SCM[cementIndex].Quantity__c;
            
            Decimal percentSCM = cement_and_SCM[0].Mix__r.Target_SCM_Ratio__c;
            theSCM.Quantity__c = cementWeight * (percentSCM/100);
            theSCM.Unit__c = cementUnit;
            update(theSCM);

        } else if(SCMIndex != null) {
            theSCM = cement_and_SCM[SCMIndex];
            theSCM.Quantity__c = 0;
            theSCM.Unit__c = 'none set';
            update(theSCM);
        } 
    }

*/
}