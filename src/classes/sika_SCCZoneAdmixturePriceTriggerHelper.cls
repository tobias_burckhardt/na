public with sharing class sika_SCCZoneAdmixturePriceTriggerHelper {
	public static Boolean bypassMixMaterialUpdate = false;

	public static void updateMixMaterialAdmixturePrice(list<Admixture_Price__c> admixturePrices){
		if(bypassMixMaterialUpdate == false){
			set<Id> materialIDs = new set<Id>();
			map<Id, Decimal> materialID_to_admixPrice = new map<Id, Decimal>();
			
			// create a set of material IDs from the list of Admixture_Price objects
			// also create a map from material (admixture) IDs to admixture_price price values
			for(Admixture_Price__c axp : admixturePrices){
				materialIDs.add(axp.Admixture_Material__c);
				materialID_to_admixPrice.put(axp.Admixture_Material__c, axp.Price__c);
			}

			// get all mix_materials that are not a part of a locked mix, and that reference the materials (admixtures) associated with the Admixture_Price records passed to the trigger
			list<Mix_Material__c> mixMaterials = [SELECT Id, 
														 Admixture_Cost__c, 
														 Mix__r.isLocked__c, 
														 Material__c 
												 	FROM Mix_Material__c 
												   WHERE Material__c IN: materialIDs 
												     AND Mix__r.isLocked__c = false];

			list<Mix_Material__c> mmsToUpdate = new list<Mix_Material__c>();

			for(Mix_Material__c mm : mixMaterials){
				if(materialID_to_admixPrice.get(mm.Material__c) != null){
					mm.Admixture_Cost__c = materialID_to_admixPrice.get(mm.Material__c);
					mmsToUpdate.add(mm);
				}
			}

			sika_SCCZoneMixMaterialTriggerHelper.bypassConcreteCurveCalc = true;
			sika_SCCZoneMixMaterialTriggerHelper.bypassMixOptimizationCheck = true;

			update mmsToUpdate;

			sika_SCCZoneMixMaterialTriggerHelper.bypassConcreteCurveCalc = false;
			sika_SCCZoneMixMaterialTriggerHelper.bypassMixOptimizationCheck = false;
		}
	}
}