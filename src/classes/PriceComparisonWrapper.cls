public class PriceComparisonWrapper {
	  public String quoteName{get;set;}
      public String quantity{get;set;}
      public String listPrice{get;set;}
      public String salesPrice{get;set;}
      public String totalPrice{get;set;}
      public String createdDate{get;set;}
      public String presentedDate{get;set;}
      public String expirationDate{get;set;}
      public String discount{get;set;}
      public String quoteOwner{get;set;}
      public String priceTierAvailable{get;set;}
      public String quoteStatus{get;set;}
      public String productName{get;set;}
      public String articleCode{get;set;}
      public String quoteLineItemId{get;set;}
      public String opportunityName{get;set;}
      public Boolean isSPAQuote{get;set;}
}