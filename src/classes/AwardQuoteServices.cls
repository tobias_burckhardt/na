public with sharing class AwardQuoteServices {

    private static final AwardQuoteControllerWithoutSharing TRAMPOLINE =
        new AwardQuoteControllerWithoutSharing();

    public static List<AwardQuoteController.ApttusProposalWrapper> filterProposals(Boolean isCommunityUser,
            List<AwardQuoteController.ApttusProposalWrapper> proposals) {

        List<AwardQuoteController.ApttusProposalWrapper> results =
            new List<AwardQuoteController.ApttusProposalWrapper>();

        for (AwardQuoteController.ApttusProposalWrapper proposal : proposals) {
            if (proposal.isSelected) {
                proposal.buildingGroup.Membrane__c = proposal.selectedMembrane;
                proposal.buildingGroup.Primary_Membrane__c = proposal.selectedMembrane;
                proposal.buildingGroup.Intended_Warranty_Product__c = proposal.selectedWarranty;
                proposal.buildingGroup.Warranty_Extended_Product__c = proposal.selectedExtension;

                Boolean isCommunityUserOrHasWarranty = isCommunityUser ||
                    proposal.buildingGroup.Intended_Warranty_Product__c != null;

                if (proposal.buildingGroup.Building_Group_name__c != null &&
                        proposal.buildingGroup.Bldg_Sq_Footage__c != null &&
                        proposal.buildingGroup.Membrane__c != null &&
                        isCommunityUserOrHasWarranty) {
                    results.add(proposal);
                } else {
                    throw new IncompleteBuildingGroupException();
                }
            }
        }

        return results;
    }

    public static void setApplicatorQuoteAwardedValuesOnOpp(User currentUser, Opportunity oppRecord, Applicator_Quote__c applicatorQuote) {
        if (currentUser.AccountId != null) {
            oppRecord.AccountId = currentUser.AccountId;
        } else {
            oppRecord.AccountId = applicatorQuote.Account__c;
            oppRecord.Roofing_Primary_Contact__c = applicatorQuote.Contact__c;
        }
        oppRecord.OwnerId = currentUser.Id;
        oppRecord.NOA_Awarded__c = true;
        oppRecord.NOA_Created__c = true;
        update oppRecord;
    }

    public static void createBuildingGroupsAndProducts(Boolean isCommunityUser,
            List<AwardQuoteController.ApttusProposalWrapper> proposals) {

        Map<Id, Building_Group__c> buildingGroupByCategory = new Map<Id, Building_Group__c>();
        List<Building_Group_Product__c> buildingGroupProducts = new List<Building_Group_Product__c>();

        for (AwardQuoteController.ApttusProposalWrapper proposal : proposals) {
            buildingGroupByCategory.put(proposal.buildingGroup.Category__c, proposal.buildingGroup);
        }

        insert buildingGroupByCategory.values();

        Integer sortNum = 0;
        List<Product2> deckProd = [SELECT Id FROM Product2 WHERE Name = 'DECK'];

        if (deckProd.isEmpty()) {
            throw new MissingDeckProductException();
        }

        for (AwardQuoteController.ApttusProposalWrapper proposal : proposals) {
            if (proposal.buildingGroup.Id != null) {

                for (AwardQuoteController.ApttusProposalLineItemWrapper apli : proposal.lineItems) {
                    if (apli.buildingGroupProduct.Product_lookup__c == proposal.selectedMembrane ||
                            !apli.apttusProposalLineItem.Apttus_Proposal__Product__r.PTS_IsMembrane__c) {

                        if (apli.buildingGroupProduct.Product_lookup__c == proposal.selectedMembrane) {
                            apli.buildingGroupProduct.Primary_Membrane__c = true;
                        }

                        apli.buildingGroupProduct.Building_Group__c = proposal.buildingGroup.Id;
                        Product2 proposalProduct = apli.apttusProposalLineItem.Apttus_Proposal__Product__r;
                        if (proposalProduct.APTS_Applicator_Active__c && isCommunityUser ||
                                proposalProduct.NOAActive__c && !isCommunityUser) {
                            apli.buildingGroupProduct.Sort_Order__c = sortNum;
                            sortNum++;
                            buildingGroupProducts.add(apli.buildingGroupProduct);
                        }
                    }
                }

                Building_Group_Product__c deckProduct = new Building_Group_Product__c();
                deckProduct.Building_Group__c = proposal.buildingGroup.Id;
                deckProduct.Product_lookup__c = deckProd[0].Id;
                deckProduct.Sort_Order__c = sortNum;
                sortNum++;
                buildingGroupProducts.add(deckProduct);
            }
        }

        TRAMPOLINE.insertBuildingGroupProducts(buildingGroupProducts);
    }

    public class IncompleteBuildingGroupException extends Exception {}

    public class MissingDeckProductException extends Exception {}
}