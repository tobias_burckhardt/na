public class APTS_ProposalTriggerHandler{

    public static void onBeforeInsert(List< Apttus_Proposal__Proposal__c> newProposalList) {
        Map<Id,Apttus_Proposal__Proposal__c > newProposalMap= new  Map<Id,Apttus_Proposal__Proposal__c >();
         for (Apttus_Proposal__Proposal__c prop : newProposalList)
         {
             newProposalMap.put(prop.Id,prop);
         }
        APTS_ProposalTriggerExecutor executor = new APTS_ProposalTriggerExecutor();
         system.debug(newProposalMap);
        executor.ValidateProposalOpportunity(newProposalMap);
    }
     public static void onBeforeUpdate(Map<Id, Apttus_Proposal__Proposal__c> oldProposalMap,
                                     Map<Id, Apttus_Proposal__Proposal__c> newProposalMap) {
        
        APTS_ProposalTriggerExecutor executor = new APTS_ProposalTriggerExecutor();
        executor.ValidateProposalOpportunity(newProposalMap);
       
    }


}