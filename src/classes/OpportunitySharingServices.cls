/**
 * Collection of services methods used for sharing logic on the Opportunity
 */
public without sharing class OpportunitySharingServices {

	/**
	 * Shares the new Opportunities to all the PTS Community users as well as removes sharing for Members that are not
	 * related to the Account on the Opportunity
	 * @param newOpps [description]
	 */
	public static void shareOppsWithPTS(List<Opportunity> newOppties) {
		// Grab necessary fields
		newOppties = [SELECT Id, AccountId, OwnerId FROM Opportunity WHERE Id IN :newOppties];

		// Group AQ's by Opportunity
		List<Applicator_Quote__c> aqsToRemoveShares = [SELECT Id, Account__c, Opportunity__c, Opportunity__r.AccountId,
				Contact__c, OwnerId FROM Applicator_Quote__c WHERE Opportunity__c IN :newOppties];
		Map<Id, List<Applicator_Quote__c>> quotesByOpptyId = (Map<Id, List<Applicator_Quote__c>>) GroupBy.ids('Opportunity__c', aqsToRemoveShares);

		// Get all accounts from AQ's
		Set<Id> accsToRemoveShare = new Set<Id>();
		for (Applicator_Quote__c aqToRemoveShares : aqsToRemoveShares) {
			accsToRemoveShare.add(aqToRemoveShares.Account__c);
		}

		// Get all accounts from opps that don't have any applicator quotes with a reference to the opp account
		List<Opportunity> oppsNoAqsWithAcc = new List<Opportunity>();
		for (Opportunity opp : newOppties) {
			if (opp.AccountId != null) {
				if (!quotesByOpptyId.keySet().contains(opp.Id)) {
					accsToRemoveShare.add(opp.AccountId);
					oppsNoAqsWithAcc.add(opp);
				} else {
					boolean aqReferencingOppAccExists = false;
					for (Applicator_Quote__c aq : (List<Applicator_Quote__c>) quotesByOpptyId.get(opp.Id)) {
						if (aq.Account__c == opp.AccountId) {
							aqReferencingOppAccExists = true;
							break;
						}
					}
					if (!aqReferencingOppAccExists) {
						accsToRemoveShare.add(opp.AccountId);
						oppsNoAqsWithAcc.add(opp);
					}
				}
			}
		}

		// Get list of PTS contacts that belong to those accounts
		Set<Id> aqIdsToRemoveShare = Pluck.ids(aqsToRemoveShares);
		List<User> users = getPtsUsers(accsToRemoveShare);

		// Remove ALL manual shares from applicator quotes and opportunities
		List<OpportunityShare> oppShareToDelete = [SELECT Id FROM OpportunityShare WHERE OpportunityId IN :newOppties AND UserOrGroupId IN :users AND RowCause = :Schema.OpportunityShare.RowCause.Manual];
		List<Applicator_Quote__Share> aQshareToDelete = [SELECT Id FROM Applicator_Quote__Share WHERE ParentId IN :aqIdsToRemoveShare AND UserOrGroupId IN :users AND RowCause = :Schema.Applicator_Quote__Share.RowCause.Manual];
		delete oppShareToDelete;
		delete aQshareToDelete;

		// Recreate the shares with the new account or if opp account was nullified, share with all AQ's under opp
		Map<Id, Opportunity> oppsById = new Map<Id, Opportunity>([SELECT Id, AccountId FROM Opportunity WHERE Id IN :quotesByOpptyId.keySet()]);
		List<Applicator_Quote__c> aqsToAddShares = new List<Applicator_Quote__c>();
		for (Id id : quotesByOpptyId.keySet()) {
			for (Applicator_Quote__c applicatorQuote : quotesByOpptyId.get(id)) {
				if (applicatorQuote.Account__c == oppsById.get(id).AccountId || oppsById.get(id).AccountId == null) {
					aqsToAddShares.add(applicatorQuote);
				}
			}
		}
		ApplicatorQuoteSharingServices.shareApplicatorQuoteWithPTS(aqsToAddShares);

		// Recreate the shares for the Opportunities with Account but no Applicator Quotes
		List<OpportunityShare> oppSharesToCreate = new List<OpportunityShare>();
		Map<Id, List<User>> usersByAccountId = (Map<Id, List<User>>) GroupBy.ids('Contact.AccountId', users);
		for (Opportunity opp : oppsNoAqsWithAcc) {
			if (usersByAccountId.get(opp.AccountId) != null) {
				for (User u : usersByAccountId.get(opp.AccountId)) {
					if (opp.OwnerId != u.Id) {
						OpportunityShare opptyShare = new OpportunityShare(OpportunityId = opp.Id, UserOrGroupId = u.Id,
								OpportunityAccessLevel = 'Edit', RowCause = Schema.OpportunityShare.RowCause.Manual);
						oppSharesToCreate.add(opptyShare);
					}
				}
			}
		}
		insert oppSharesToCreate;
	}

	public static void manageOpportunityTeamSharing(List<Opportunity> opps) {
		Set<Id> ptsProfiles = Pluck.ids(UserDao.getUserPartnerProfileIds());
		Set<Id> oppKeys = Pluck.ids(opps);
		List<OpportunityShare> oppShares = [
				SELECT UserOrGroupId, RowCause, OpportunityId, OpportunityAccessLevel, Id
				FROM OpportunityShare
				WHERE OpportunityId IN :oppKeys
				AND RowCause = 'Team'
				AND OpportunityAccessLevel = 'Read'
		];
		for (OpportunityShare oppshare : oppShares) {
			oppshare.OpportunityAccessLevel = 'Edit';
		}
		if (oppShares.size() > 0) {
			update oppShares;
		}

		// PTS Users should not be added as team members
		List<OpportunityTeamMember> teamMembers = [
				SELECT Id, UserId
				FROM OpportunityTeamMember
				WHERE OpportunityId IN :oppKeys
		];
		Set<String> teamMemberIds = Pluck.strings('UserId', teamMembers);
		Set<Id> ptsUserIds = Pluck.ids([
				SELECT Id FROM User
				WHERE Id IN :teamMemberIds
				AND ProfileId IN :ptsProfiles
		]);
		List<OpportunityTeamMember> teamMembersToDelete = new List<OpportunityTeamMember>();
		for (OpportunityTeamMember teamMember : teamMembers) {
			if (ptsUserIds.contains(teamMember.UserId)) {
				teamMembersToDelete.add(teamMember);
			}
		}
		if (teamMembersToDelete.size() > 0) {
			delete teamMembersToDelete;
		}
	}
	private static List<User> getPtsUsers(Set<Id> accountIds) {
		return [
				SELECT Contact.AccountId
				FROM User
				WHERE Contact.AccountId IN :accountIds
				AND IsActive = TRUE
				AND Profile.Name IN :Label.PartnerUserProfileNames.split(',')
		];
	}


	//NEW RANDY CODE, DELETE ABOVE IF NO REFERENCES WHEN DONE
	public static void addOpportunitySharesToCommunityUsers(Map<Id,List<Id>> userToOpportunityMap){
		List<OpportunityShare> opptyShares = new List<OpportunityShare>();
		for(Id usrId : userToOpportunityMap.keySet()){
			List<Id> oppIds = userToOpportunityMap.get(usrId);
			for(Id oppId : oppIds){
				OpportunityShare oppShare = createOpportunityShare(oppId, usrId, 'Edit');
				opptyShares.add(oppShare);
			}
		}
		insert opptyShares;
	}

	public static OpportunityShare createOpportunityShare (Id oppId, Id userId, String access){
		OpportunityShare opptyShare = new OpportunityShare(OpportunityId = oppId,
											UserOrGroupId = userId,
											OpportunityAccessLevel = 'Edit',
											RowCause = Schema.OpportunityShare.RowCause.Manual);
		return opptyShare;
	}
}