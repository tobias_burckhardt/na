public without sharing class sika_SCCZoneServiceRequestComponentCtrl {
	
	public String selectedType {get; set;}
	public String caseDesc {get; set;}
	private User theUser;
	private String acctName;
	public Boolean displayPopup {get; set;}

	public sika_SCCZoneServiceRequestComponentCtrl() {
		Id userID = UserInfo.getUserId();
		displayPopup = false;
		
		theUser = [SELECT Id, Name, Email, AccountId FROM User WHERE Id = :userId Limit 1];
		
		if(theUser.AccountId == null){
		    acctName = 'None';
		} else {
		    acctName = [SELECT Name FROM Account WHERE Id = :theUser.AccountId Limit 1].Name;
		}
		
		String type = Apexpages.currentPage().getParameters().get('type');
		if(type != '' && type != null){
			selectedType = 'Website Help';
		} 

	}

	public list<SelectOption> getRequestTypes(){

		// get the piclist values for Case.SCC_Zone_Case_Types__c. The SCC_Zone_Case_Types__c field is only used to populate this selectList. No data is ever stored in the field.  Just makes it easier to add more options to the list later.
		list<SelectOption> typeList = sika_SCCZonePicklistValuesHelper.getPicklistValues(new Case(), 'SCC_Zone_Case_Types__c');
		return typeList;
	}

	public void closePopup() {        
        displayPopup = false; 
        caseDescError = false;   
    } 
        
    public void showPopup() {        
        displayPopup = true;    
    }

	@testVisible private Database.SaveResult result;
    @testVisible private String subject;

	public pagereference submitCase(){

		if(validateForm()){
			// create the case
			Case theCase = new Case();
			theCase.Subject = theUser.Name + ', ' + acctName + ' -- ' + selectedType;
			theCase.Description = caseDesc;
			theCase.Status = 'New';
	        theCase.Due_Date__c = Datetime.now();
	        theCase.Origin = 'SCCZone Website';
	        //theCase.Type = selectedType;
	        theCase.Priority = 'Medium';
	        theCase.Customer_Email__c = theUser.Email;
	        theCase.SCC_Zone_Case_Types__c = selectedType;
	        theCase.Do_not_send_auto_response__c = true;

	        // make sure the auto case assignment rules are applied to this case when it's added to the database
	        AssignmentRule ar = new AssignmentRule();
            ar = [SELECT Id FROM AssignmentRule WHERE SobjectType = 'Case' AND Active = true Limit 1];

	        Database.DMLOptions options = new Database.DMLOptions();
			options.assignmentRuleHeader.assignmentRuleId= AR.id;
			theCase.setOptions(options);

			insert(theCase);

			pageReference commentDetailsPage = new pageReference('/sika_SCCZoneSupportTicket');
			commentDetailsPage.getParameters().put('id', theCase.Id);
			commentDetailsPage.setRedirect(true);

			return commentDetailsPage;
			//displayPopup = false;
		}
		return null;
		//displayPopup = true;
	}

	public Boolean hasErrors {get; private set;}
	public Boolean caseDescError {get; private set;}
	public String caseDescErrorText {get; private set;}

	private Boolean validateForm (){
		caseDescError = false;
		caseDescErrorText = '';

		if(String.isBlank(caseDesc)){
            caseDescErrorText = 'Please provide some details about your request.';
            caseDescError = true;

            return false;
        }
        return true;
	}

}