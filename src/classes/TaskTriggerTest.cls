@isTest
private class TaskTriggerTest {

    static testMethod void myUnitTest() {
        Opportunity opp = new Opportunity();
        opp.name = 'test opp 1';
        opp.StageName = 'Oferecer projeto/orçamento';
        opp.LeadSource = 'Google';
        opp.CloseDate = date.today();
        opp.OwnerId = UserInfo.getUserId();
                                
        insert opp;
        
        Task t = new Task();
        t.OwnerId = UserInfo.getUserId();
        t.Subject='Donni';
        t.Status='Not Started';
        t.Priority='Normal';
        t.whatId = opp.id;
        
        User u = [select id from User where id <> :UserInfo.getUserId() and profile.name = 'System Administrator' and isactive = true limit 1];
        
        Task t1 = new Task();
        t1.OwnerId = u.id;
        t1.Subject='Donni';
        t1.Status='Not Started';
        t1.Priority='Normal';
        t1.whatId = opp.id;
        
        insert t;
        insert t1;
    }
}