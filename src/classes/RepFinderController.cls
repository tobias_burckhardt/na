/**
 * Controller for Lightning components involved in the Rep Finder app
 *
 * @author ben.burwell@trifecta.com
 */
public without sharing class RepFinderController {
	/**
	 * Retrieve the possible Target Market options from Describe info
	 *
	 * @return a list of Target Market options
	 */
	@AuraEnabled
	public static List<String> getTargetMarketOptions() {
		return RepFinderUtils.getPicklistEntries(Region__c.Target_Market__c.getDescribe());
	}

	/**
	 * Retrieve the possible State options from Describe info
	 *
	 * @return a list of State options
	 */
	@AuraEnabled
	public static List<String> getStateOptions() {
		return RepFinderUtils.getPicklistEntries(Region__c.Region_State__c.getDescribe());
	}

	/**
	 * Retrieve the possible Cross Sell Team options from Describe info
	 *
	 * @return a list of Cross Sell Team options
	 */
	@AuraEnabled
	public static List<String> getCrossSellTeamOptions() {
		return RepFinderUtils.getPicklistEntries(Referral__c.Cross_Sell_Team__c.getDescribe());
	}

	/**
	 * Retrieve the possible Vertical options from Describe info
	 *
	 * @return a list of Vertical options
	 */
	@AuraEnabled
	public static List<String> getVerticalOptions() {
		return RepFinderUtils.getPicklistEntries(Region__c.Vertical__c.getDescribe());
	}

	/**
	 * Perform a rep search based on supplied criteria
	 *
	 * @param targetMarket the target market to look for reps in
	 * @param state the state to look for reps in
	 * @return a list of RepFinderSearchResult
	 */
	@AuraEnabled
	public static List<RepFinderSearchResult> search(String targetMarket, String state, String vertical) {
		return RepFinderUtils.doRepSearch(targetMarket, state, vertical);
	}

	/**
	 * Create a Referral
	 *
	 * Create a new referral with a few fields populated from the Rep Finder
	 *
	 * @param targetMarket the target market for the referral
	 * @param assignedTo the user ID to assign the referral to
	 * @param subject the referral subject
	 * @param comments the referral comments
	 * @param crossSellTeam an optional cross sell team for the referral
	 * @return true iff the creation succeeded
	 */
	@AuraEnabled
	public static Boolean createReferral(
		String targetMarket,
		Id assignedTo,
		String subject,
		String comments,
		String crossSellTeam
	) {
		Referral__c referral = new Referral__c();
		referral.Target_Market__c = targetMarket;
		referral.Assigned_To__c = assignedTo;
		referral.Subject__c = subject;
		referral.Referral_Comments__c = comments;
		referral.Cross_Sell_Team__c = crossSellTeam;
		try {
			insert referral;
		} catch (DmlException e) {
			System.debug('There was a problem creating the referral: ' + e.getMessage());
			return false;
		}
		return true;
	}
}