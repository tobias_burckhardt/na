public with sharing class sika_SCCZoneForgotPasswordController {

	public String userName {get; set;}
	public String success {get; set;}
	public Boolean submitted {get; set;}
	public String boxText {get; set;}
	public Boolean displayPopup {get; set;}
	public String caseEmail{get; set;}
    public String caseDesc {get; set;}

    @TestVisible private static String usernameNotRecognizedErrorMsg = 'The username you entered was not recognized. Please try again.';
    @TestVisible private static String invalidEmailErrorMsg = 'The username you entered was not recognized. Please try again.';

	public sika_SCCZoneForgotPasswordController() {
		success = ApexPages.currentPage().getParameters().get('success');
		displayPopup = false;

		if(success != null && Boolean.valueOf(success) == true){
			submitted = true;
			boxText = 'Thank you! You should receive an email with a link to reset your password shortly.';
		} else {
			submitted = false;
			boxText = 'Please enter your user name below:';
		}

	}

	public PageReference submitRequest(){
		if(validateForm()){
			Site.forgotPassword(userName);

			PageReference thePage = new PageReference(ApexPages.currentPage().getUrl());
			thePage.getParameters().put('success', 'true');
			thePage.setRedirect(true);
			return thePage;
		}

		return null;
	}

	public void closePopup() {        
        displayPopup = false;    
    } 
        
    public void showPopup() {        
        displayPopup = true;    
    }

	public void submitCase(){

		Case theCase = new Case();
		theCase.Description = caseDesc;
		theCase.Status = 'New';
		theCase.Due_Date__c = Datetime.now();
		theCase.Origin = 'Web';
		theCase.Type = 'Website Help';
		theCase.Priority = 'Medium';
		theCase.Origin = 'SCCZone Website';
		theCase.Subject = 'Website Help' + ' ticket created by ' + caseEmail;
		theCase.Customer_Email__c = caseEmail;
		theCase.SCC_Zone_Case_Types__c = 'Website Help';
		theCase.Do_not_send_auto_response__c = true;

		List<Contact> u = [SELECT ID, Name, Email FROM Contact WHERE Email =: caseEmail LIMIT 1];
		if(u.size() == 1){
			theCase.ContactID = u[0].ID;

		}
   
		Database.DMLOptions options = new Database.DMLOptions();
		options.assignmentRuleHeader.useDefaultRule = true;
		theCase.setOptions(options);

		try {
			insert(theCase);
		} catch (DmlException e) {
			system.debug('******** ERROR submitting case: ' + e);
		}
		displayPopup = false;   

	}


	public Boolean hasError {get; private set;}
	public String errorText {get; private set;}

	public Boolean validateForm(){
		hasError = false;
		errorText = '';
		String currentPage =  ApexPages.currentPage().getUrl().toLowerCase();
		if( currentPage.contains('?')){
			currentPage = currentPage.substringBefore('?');
		}
		Boolean isZonePage = currentPage == Page.sika_SCCZoneForgotPasswordPage.getUrl();
		if((!sika_SCCZoneValidationHelper.validateEmail(userName) && isZonePage) || (!sika_PTSValidationHelper.validateEmail(userName) && !isZonePage)|| userName == null){
			hasError = true;
			errorText = invalidEmailErrorMsg;

            return false;
		}

		List<Id> profileIds = new List<Id>();
		if (isZonePage) {
			profileIds = new List<Id>{Sika_SCCZone_Common.SCCZoneUserProfileId()};
		} else {
			profileIds = new List<Id>(Pluck.ids(UserDao.getUserPartnerProfileIds()));
		}
		Integer u = [SELECT COUNT() FROM User WHERE Username = :userName AND ProfileId IN :profileIds];
		if(u == 0){
			hasError = true;
			errorText = usernameNotRecognizedErrorMsg;
			
			return false;
		}

		return true;
	}



}