public with sharing class SAPIntegrationService {
    public static final String PROJECT_ACCOUNT_GROUP = 'YPRJ';
    public static final String SHIP_TO_ACCOUNT_GROUP = '0002';
    public static final String ERROR_TYPE = 'E';

    private static final String CUSTOMER_ENDPOINT = 'callout:SAP_Customer_Integration';
    private static final String CONTRACT_ENDPOINT = 'callout:SAP_Contract_Integration';
    private static final Integer CALLOUT_TIMEOUT = 120000;

    public static String createProjectInSAP(Opportunity oppty) {
        return createAccountInSAP(oppty, createProjectDataElementFromOppty(oppty));
    }

    public static String createShipToInSAP(Opportunity oppty) {
        return createAccountInSAP(oppty, createBaseDataElementFromOppty(oppty, SHIP_TO_ACCOUNT_GROUP));
    }

    public static String createContractInSAP(Opportunity oppty) {
        SAPContractIntegration.os_ContractCreatePort stub = new SAPContractIntegration.os_ContractCreatePort();
        stub.endpoint_x = CONTRACT_ENDPOINT;
        stub.timeout_x = CALLOUT_TIMEOUT;

        List<Credit_Split__c> creditSplits = [
            SELECT Split_Percentage__c, SAP_Yx__c, SAP_Sales_User__r.SAP_Sales_Code__c
            FROM Credit_Split__c WHERE Opportunity__c = :oppty.Id
        ];

        SAPContractIntegration.ContractHeader_element data = createContractHeaderFromOppty(oppty, creditSplits);
        List<SAPContractIntegration.ContractPartners_element> partners = createPartnersFromOppty(oppty, creditSplits);
        List<SAPContractIntegration.ContractLineItems_element> lineItems = createLineItemsFromOppty(oppty);

        SAPContractIntegration.ContractResponse_element response = stub.os_ContractCreate(data, partners, lineItems);

        List<String> errors = parserErrorsFromResponse(response);
        if (!errors.isEmpty()) {
            throw new SAPIntegrationServiceException(String.join(errors, '\n'));
        }

        return response.ContractNumber;
    }

    public class SAPIntegrationServiceException extends Exception {}

    private static SAPContractIntegration.ContractHeader_element createContractHeaderFromOppty(Opportunity oppty,
            List<Credit_Split__c> creditSplits) {

        List<Integer> splitPercentages = calculateSplitPercentages(creditSplits);

        SAPContractIntegration.ContractHeader_element header = new SAPContractIntegration.ContractHeader_element();
        header.SalesOrg = oppty.Sales_Organization__c;
        header.DistrChannel = oppty.Distribution__c;
        header.Division = oppty.Division__c;
        header.BillingDate = Date.today();
        header.PurchOrder = oppty.Opportunity_Number__c;
        header.PurchOrderDate = Date.today();
        header.JobName = oppty.SAP_Name_40__c;
        header.CustomerGrp = oppty.Customer_Group__c;
        header.ValidFrom = Date.today();
        header.ValidTo = Date.today().addYears(1);
        header.ShippingCondition = oppty.Shipping_Condition__c;
        header.SplitPecent1 = splitPercentages.get(0) == 0 ? null : splitPercentages.get(0);
        header.SplitPecent2 = splitPercentages.get(1) == 0 ? null : splitPercentages.get(1);
        header.SplitPecent3 = splitPercentages.get(2) == 0 ? null : splitPercentages.get(2);
        header.SplitPecent4 = splitPercentages.get(3) == 0 ? null : splitPercentages.get(3);
        header.SalesforceID = oppty.Id;
        header.OrderReason = oppty.SAP_Order_Reason__c;
        header.AccountAssignment = oppty.SAP_Account_Assign__c;
        return header;
    }

    private static List<Integer> calculateSplitPercentages(List<Credit_Split__c> creditSplits) {
        List<Integer> splitPercentages = new List<Integer>{0, 0, 0, 0};
        for (Credit_Split__c creditSplit : creditSplits) {
            if ('Y1'.equalsIgnoreCase(creditSplit.SAP_Yx__c) && creditSplit.Split_Percentage__c != null) {
                splitPercentages.set(0, splitPercentages.get(0) + Integer.valueOf(creditSplit.Split_Percentage__c));
            } else if ('Y2'.equalsIgnoreCase(creditSplit.SAP_Yx__c) && creditSplit.Split_Percentage__c != null) {
                splitPercentages.set(1, splitPercentages.get(1) + Integer.valueOf(creditSplit.Split_Percentage__c));
            } else if ('Y3'.equalsIgnoreCase(creditSplit.SAP_Yx__c) && creditSplit.Split_Percentage__c != null) {
                splitPercentages.set(2, splitPercentages.get(2) + Integer.valueOf(creditSplit.Split_Percentage__c));
            } else if ('Y4'.equalsIgnoreCase(creditSplit.SAP_Yx__c) && creditSplit.Split_Percentage__c != null) {
                splitPercentages.set(3, splitPercentages.get(3) + Integer.valueOf(creditSplit.Split_Percentage__c));
            }
        }

        return splitPercentages;
    }

    private static List<SAPContractIntegration.ContractPartners_element> createPartnersFromOppty(Opportunity oppty,
            List<Credit_Split__c> creditSplits) {
        List<SAPContractIntegration.ContractPartners_element> partners =
            new List<SAPContractIntegration.ContractPartners_element>();

        for (Credit_Split__c split : creditSplits) {
            String partnerNumber = split.SAP_Sales_User__r != null ? split.SAP_Sales_User__r.SAP_Sales_Code__c : null;
            partners.add(createPartner(split.SAP_Yx__c, partnerNumber));
        }

        if (oppty.Account != null) {
            String partnerValue;
            if (String.isNotBlank(oppty.Account.SAP_Account_Code__c)) {
                partnerValue = oppty.Account.SAP_Account_Code__c.replaceFirst('^US01_', '');
            }
            partners.add(createPartner('SP', partnerValue));
        }

        if (String.isNotBlank(oppty.ShipTo_AccountId__c)) {
            partners.add(createPartner('SH', oppty.ShipTo_AccountId__c));
        }

        if (String.isNotBlank(oppty.Project_AccountId__c)) {
            partners.add(createPartner('YP', oppty.Project_AccountId__c));
        }

        return partners;
    }

    private static String getSarnafilServicesIncSAPCode() {
        Sarnafil_Services_Inc_Account_Id__c setting =
            [SELECT Account_Id__c FROM Sarnafil_Services_Inc_Account_Id__c LIMIT 1];
        Account sarnafilAccount =
            [SELECT SAP_Account_Code__c FROM Account WHERE Id = :setting.Account_Id__c];
        return sarnafilAccount.SAP_Account_Code__c;
    }

    private static SAPContractIntegration.ContractPartners_element createPartner(String partnerFunction,
            String partnerNumber) {
        SAPContractIntegration.ContractPartners_element partner = new SAPContractIntegration.ContractPartners_element();
        partner.PartnerFunction = partnerFunction;
        partner.PartnerNumber = partnerNumber;
        return partner;
    }

    private static List<SAPContractIntegration.ContractLineItems_element> createLineItemsFromOppty(Opportunity oppty) {
        List<Apttus_Proposal__Proposal_Line_Item__c> proposalLineItems = [
            SELECT APTS_SAP_Item_Number__c, Apttus_QPConfig__Quantity2__c, Apttus_QPConfig__PriceUom__c,
                   Apttus_Proposal__Product__r.SAP_Part_Number__c, Apttus_QPConfig__NetPrice__c, APTS_SAP_Price_UOM__c, SAP_Sales_Unit__c
            FROM Apttus_Proposal__Proposal_Line_Item__c
            WHERE Apttus_Proposal__Proposal__r.Apttus_Proposal__Opportunity__c = :oppty.Id
        ];

        List<SAPContractIntegration.ContractLineItems_element> lineItems =
            new List<SAPContractIntegration.ContractLineItems_element>();

        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : proposalLineItems) {
            SAPContractIntegration.ContractLineItems_element lineItem =
                new SAPContractIntegration.ContractLineItems_element();
            lineItem.ItemNumber = Integer.valueOf(proposalLineItem.APTS_SAP_Item_Number__c);
            lineItem.MaterialNumber = proposalLineItem.Apttus_Proposal__Product__r == null ?
                null : proposalLineItem.Apttus_Proposal__Product__r.SAP_Part_Number__c;
            lineItem.TargetQty = proposalLineItem.Apttus_QPConfig__Quantity2__c;
            lineItem.TargetUOM = proposalLineItem.SAP_Sales_Unit__c;
            lineItem.Plant = oppty.Warehouse__c;
            lineItem.Conditions = createConditionsForLineItem(proposalLineItem);
            lineItems.add(lineItem);
        }

        return lineItems;
    }

    private static List<SAPContractIntegration.Conditions_element> createConditionsForLineItem(
            Apttus_Proposal__Proposal_Line_Item__c proposalLineItem) {
        SAPContractIntegration.Conditions_element condition = new SAPContractIntegration.Conditions_element();
        condition.condItem = Integer.valueOf(proposalLineItem.APTS_SAP_Item_Number__c);
        condition.condType = PROJECT_ACCOUNT_GROUP;
        condition.condPrice = proposalLineItem.Apttus_QPConfig__NetPrice__c;
        condition.condPriceUOM = proposalLineItem.APTS_SAP_Price_UOM__c;
        return new List<SAPContractIntegration.Conditions_element>{condition};
    }

    private static String createAccountInSAP(Opportunity oppty, SAPAccountIntegration.GeneralData_element data) {
        SAPAccountIntegration.os_CustomerCreatePort stub = new SAPAccountIntegration.os_CustomerCreatePort();
        stub.endpoint_x = CUSTOMER_ENDPOINT;
        stub.timeout_x = CALLOUT_TIMEOUT;

        List<SAPAccountIntegration.SalesAreaData_element> salesAreaDatas = createSalesAreaDataElementFromOppty(oppty);
        List<SAPAccountIntegration.TaxIndicators_element> taxIndicators = createTaxIndicatorFromOppty(oppty);

        SAPAccountIntegration.CustomerResponse_element response =
            stub.os_CustomerCreate(data, salesAreaDatas, taxIndicators);

        List<String> errors = parserErrorsFromResponse(response);
        if (!errors.isEmpty()) {
            throw new SAPIntegrationServiceException(String.join(errors, '\n'));
        }

        return response.CustomerNumber;
    }

    private static SAPAccountIntegration.GeneralData_element createBaseDataElementFromOppty(Opportunity oppty,
            String accountGroup) {

        SAPAccountIntegration.GeneralData_element data = new SAPAccountIntegration.GeneralData_element();
        data.AccountGroup = accountGroup;
        data.Name1 = oppty.SAP_Name_40__c;
        data.SearchTerm1 = oppty.SAP_Name_20__c;
        data.SearchTerm2 = oppty.Opportunity_Number__c;
        data.Street = oppty.Street_Address__c;
        data.PostalCode = oppty.Zip__c;
        data.City = oppty.City__c;
        data.Country = oppty.Country__c;
        data.Region = oppty.State__c;
        data.TransportationZone = oppty.Transportation_Zone__c;
        data.Language = oppty.Language__c;
        data.TaxJurisdiction = null;    // This is populated in SAP, no need to pass value
        data.SalesforceID = String.valueOf(oppty.Id) + accountGroup;    // SAP is expecting the concatenated Id and Group
        return data;
    }

    private static SAPAccountIntegration.GeneralData_element createProjectDataElementFromOppty(Opportunity oppty) {
        SAPAccountIntegration.GeneralData_element data = createBaseDataElementFromOppty(oppty, PROJECT_ACCOUNT_GROUP);
        data.Name2 = oppty.Opportunity_Number__c;
        data.HouseNumber = getHouseNumberFromStreetAddress(oppty.Street_Address__c);
        data.Industry = oppty.SAP_Industry_Key__c;
        data.IndustryCode1 = oppty.SAP_Industry_Code__c;
        return data;
    }

    private static String getHouseNumberFromStreetAddress(String streetAddress) {
        List<String> streetParts = streetAddress == null ? new List<String>() : streetAddress.split(' ');
        if (!streetParts.isEmpty() && streetParts.get(0).isNumeric()) {
            return streetParts.get(0);
        } else {
            return null;
        }
    }

    private static List<SAPAccountIntegration.SalesAreaData_element> createSalesAreaDataElementFromOppty(Opportunity oppty) {
        List<SAPAccountIntegration.SalesAreaData_element> salesAreaDatas =
            new List<SAPAccountIntegration.SalesAreaData_element>();
        SAPAccountIntegration.SalesAreaData_element salesAreaData = new SAPAccountIntegration.SalesAreaData_element();
        salesAreaData.SalesOrg = oppty.Sales_Organization__c;
        salesAreaData.DistrChannel = oppty.Distribution__c;
        salesAreaData.Division = oppty.Division__c;
        salesAreaData.ShipCondition = oppty.Shipping_Condition__c;
        salesAreaData.DeliveringPlant = oppty.Warehouse__c;
        salesAreaData.IncoTerm1 = oppty.Incoterm1__c;
        salesAreaData.IncoTerm2 = oppty.Incoterm2__c;
        salesAreaData.VerticalMarket = oppty.SAP_Vertical_Market_Code__c;
        salesAreaData.PrimarySystem = oppty.SAP_Primary_System_Code__c;
        salesAreaData.PrimaryArea = oppty.SAP_Primary_System_Area__c;
        salesAreaData.PrimaryAreaUom = oppty.SAP_Primary_System_UOM__c;
        salesAreaDatas.add(salesAreaData);
        return salesAreaDatas;
    }
 
    private static List<SAPAccountIntegration.TaxIndicators_element> createTaxIndicatorFromOppty(Opportunity oppty) {
        List<SAPAccountIntegration.TaxIndicators_element> taxIndicators =
            new List<SAPAccountIntegration.TaxIndicators_element>();

        SAPAccountIntegration.TaxIndicators_element taxIndicatorMX = new SAPAccountIntegration.TaxIndicators_element();
        taxIndicatorMX.TaxCountry = oppty.Tax_Country_MX__c;
        taxIndicatorMX.TaxCategory = oppty.Tax_Category_MX__c;
        taxIndicatorMX.TaxClasification = String.valueOf(oppty.Tax_Classification_MX__c);
        taxIndicators.add(taxIndicatorMX);

        SAPAccountIntegration.TaxIndicators_element taxIndicatorUS = new SAPAccountIntegration.TaxIndicators_element();
        taxIndicatorUS.TaxCountry = oppty.Tax_Country_US__c;
        taxIndicatorUS.TaxCategory = oppty.Tax_Category_US__c;
        taxIndicatorUS.TaxClasification = String.valueOf(oppty.Tax_Classification_US__c);
        taxIndicators.add(taxIndicatorUS);

        return taxIndicators;
    }

    private static List<String> parserErrorsFromResponse(SAPContractIntegration.ContractResponse_element response) {
        List<String> errors = new List<String>();
        if (!response.ResponseMessages.isEmpty()) {
            for (SAPContractIntegration.ResponseMessages_element respElement : response.ResponseMessages) {
                if (ERROR_TYPE.equalsIgnoreCase(respElement.MessageType)) {
                    errors.add(respElement.MessageDescription);
                }
            }
        }
        return errors;
    }

    private static List<String> parserErrorsFromResponse(SAPAccountIntegration.CustomerResponse_element response) {
        List<String> errors = new List<String>();
        if (!response.ResponseMessages.isEmpty()) {
            for (SAPAccountIntegration.ResponseMessages_element respElement : response.ResponseMessages) {
                if (ERROR_TYPE.equalsIgnoreCase(respElement.MessageType)) {
                    errors.add(respElement.MessageDescription);
                }
            }
        }
        return errors;
    }
}