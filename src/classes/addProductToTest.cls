@isTest(SeeAllData=true)
public class addProductToTest {
    static testMethod void testNonStaticMethods() {
        Test.StartTest();     
            Account account = addProductToTest.createAccount();
            String jsonString = '{"accountName":"'+account.Id+'"}';
            Opportunity opportunity = addProductToTest.createOpportunity(jsonString);
            addProductTo addProductToObj  = new addProductTo(new ApexPages.StandardController(opportunity));
            System.assertNotEquals(null,addProductToObj.getChosenCurrency());
        Test.StopTest();
    }
    static testMethod void testRemoteMethods() {
        Account account = addProductToTest.createAccount();
        String jsonString = '{"accountName":"'+account.Id+'"}';
        Opportunity opportunity = addProductToTest.createOpportunity(jsonString);
        List<Product2> products = addProductToTest.createProducts();
        List<Product2> productsToBeMappedToPricebook = new List<Product2>();
        for(Integer i=0;i<products.size()-10;i++){
            productsToBeMappedToPricebook.add(products[i]);
        }
        Pricebook2 pricebook = [select id from Pricebook2 where IsStandard = true limit 1];
        List<PricebookEntry> pricebookEntries = addProductToTest.addPricebookEntries(productsToBeMappedToPricebook,pricebook.Id);
        System.debug('pricebookEntries--'+pricebookEntries);
        OpportunityLineItem opportunityLineItemObj = addProductToTest.mapProductToOpportunity(opportunity.Id,pricebookEntries.get(0));
        List<Account> accounts = addProductToTest.createMultipleAccounts();
        Test.StartTest();     
        	
        	String searchableProductString = 'Sika Test Product';
        	String searchableArticleCode = 'CA01_101';
        	
        	String dataString = '{"oppId":"'+opportunity.id+'","productName":"'+searchableProductString+'","articleCode":"'+searchableArticleCode+'","OffsetSize":0,"scrollValue":0,"listSize":10,"loadMore":false,"isScrollAjaxInProcess":false,"isFirstTimePageLoad":"true"}';
        	System.assertNotEquals(null,addProductTo.getAvailableProducts(dataString));
        	
        	dataString = '{"oppId":"'+opportunity.id+'","product2Id":"'+products.get(0).Id+'","pricebookEntryId":"'+pricebookEntries.get(0).Id+'"}';
        	System.assertNotEquals(null,addProductTo.saveProductToLineItem(dataString));
        	
        	dataString = '{"oppId":"'+opportunity.id+'"}';
        	System.assertNotEquals(null,addProductTo.getLineItems(dataString));
        
        	dataString = '{"oppId":"'+opportunity.id+'","product2Id":"'+products.get(0).Id+'","pricebookEntryId":"'+pricebookEntries.get(0).Id+'"}';
        	System.assertNotEquals(null,addProductTo.removeProductFromLineItem(dataString));
        	
        	dataString = '[{"quantity":"1","oppLineItemId":"'+opportunityLineItemObj.Id+'"}]';
        	System.assertNotEquals(null,addProductTo.upsertOpportunityLineItems(dataString));
        	
        	String searchableAccountString = 'dummy';
        	dataString = '{"oppId":"'+opportunity.id+'","accountName":"'+searchableAccountString+'","OffsetSize":0,"scrollValue":0,"listSize":10,"loadMore":false,"isScrollAjaxInProcess":false,"isFirstTimePageLoad":"true"}';
        	System.assertNotEquals(null,addProductTo.getAccounts(dataString));
        
        	dataString = '{"oppId":"'+opportunity.id+'","accountId":"'+accounts.get(0).Id+'"}';
        	System.assertNotEquals(null,addProductTo.updateAccountToOpportunity(dataString));
        
        	dataString = '{"oppId":"'+opportunity.id+'"}';
        	System.assertNotEquals(null,addProductTo.getSelectedAccountDetail(dataString));
        
        	dataString = '{"oppId":"'+opportunity.id+'","accountId":"'+accounts.get(0).Id+'","pickup":"Prepaid","customerDeliveryAddress":"No","accBillingStreet":"abc","accBillingCity":"sxe","accBillingState":"esw","accBillingCountry":"qwd","accBillingPostalCode":"234wsa"}';
        	System.assertNotEquals(null,addProductTo.updateAccountAddressDetail(dataString));
        
        	dataString = '{"oppId":"'+opportunity.id+'","accountId":"'+accounts.get(0).Id+'","pickup":"Prepaid","customerDeliveryAddress":"Yes","accBillingStreet":"abc","accBillingCity":"sxe","accBillingState":"esw","accBillingCountry":"qwd","accBillingPostalCode":"234wsa"}';
        	System.assertNotEquals(null,addProductTo.updateAccountAddressDetail(dataString));
        
        Test.StopTest();
    }
    public static Account createAccount(){
        Account account = new Account();
        account.Name = 'Dummy Test Account';
        account.SAPAccountGroup__c = '0001';
        account.Country_Iso_Code__c = 'CA';
        insert account;
        Account accountQueried = [Select Id,Name from Account where Name like '%Dummy Test Account%' and CreatedDate=Today Limit 1];
        return accountQueried;
    }
    public static List<Account> createMultipleAccounts(){
        List<Account> accounts = new List<Account>();
        for(Integer i=0;i<20;i++){
            Account account = new Account();
            account.Name = 'Dummy Test Account'+i;
            account.SAPAccountGroup__c = '0001';
            account.Country_Iso_Code__c = 'CA'; 
            accounts.add(account);
        }
        insert accounts;
        List<Account> accountsQueried = [Select Id,Name,SAPAccountGroup__c,Country_Iso_Code__c from Account Where Name like '%Dummy Test Account%'];
        return accountsQueried;
    }
    public static Opportunity createOpportunity(String jsonString){
        JSONParser  parser = JSON.createParser(jsonString);
        String accountID;
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'accountName')){
                parser.nextToken();
                accountID=parser.getText();
            }
        }
        Account accountQueried = [Select Id,Name from Account where Id =:accountID  Limit 1];
        Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('CA Free Sample Requisition').getRecordTypeId();
        Opportunity opportunity = new Opportunity();
        opportunity.Name = 'Test Opportunity for Free Sample Requisition';
        opportunity.RecordTypeId = recordTypeId;
        opportunity.CloseDate = Date.newInstance(2020, 2, 17);
        opportunity.StageName = 'Draft';
        opportunity.Account= accountQueried;
        opportunity.is_there_a_CCR__c = 'No';
        opportunity.Pickup__c = 'Customer Pickup';
        opportunity.Account_Group__c = 'YPRJ';
        insert opportunity;
        Opportunity opportunityQueried = [Select Id from Opportunity where Name like '%Test Opportunity for Free Sample Requisition%' limit 1];
        return opportunityQueried;
    }
   
    public static List<Product2> createProducts(){
        List<Product2> products = new List<Product2>();
        for(Integer i=0;i<20;i++){
            Product2 product = new Product2();
            product.Name = 'Sika Test Product'+i;
            product.ProductCode = 'CA01_101'+i;
            product.IsActive = true;
            product.CA_Product__c = true;
            product.Article_Code__c = 'CA01_101'+i;
            product.Application_Field_Code__c = '07';
            product.Application_Field_Text__c = 'Waterproofing';
            product.Description = 'Sika Test Product';
            product.MTART__c = 'FERT';
            product.Sales_Unit__c = 'CAR';
            
           products.add(product);
        }
        insert products;
        List<Product2> productsQueried = [Select Id,Name,ProductCode,IsActive,CA_Product__c,Article_Code__c,Application_Field_Code__c,Application_Field_Text__c,Description,MTART__c,Sales_Unit__c from Product2 where Name like '%Sika Test Product%'];
        return productsQueried;
    }
    public static OpportunityLineItem mapProductToOpportunity(String oppId,PricebookEntry priceBookEntryObj){
        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem newOpportunityLineItem = new opportunityLineItem(OpportunityId=oppId, 
                                                                             PriceBookEntry=priceBookEntryObj, 
                                                                             PriceBookEntryId=priceBookEntryObj.Id, 
                                                                             UnitPrice=priceBookEntryObj.UnitPrice,
                                                                             Quantity=1);
        insert newOpportunityLineItem;
        OpportunityLineItem newOpportunityLineItemQueried = [Select Id,OpportunityId,PriceBookEntryId,UnitPrice,Quantity from opportunityLineItem where OpportunityId =:oppId and PriceBookEntryId=:priceBookEntryObj.Id];
        return newOpportunityLineItemQueried;
    }
    public static List<PricebookEntry> addPricebookEntries(List<Product2> products,String pricebook2Id){
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        List<String> productIds = new List<String>();
        for(Product2 product:products){
            PricebookEntry pricebookEntry = new PricebookEntry();
            pricebookEntry.Product2Id = product.Id;
            pricebookEntry.Pricebook2Id = pricebook2Id;
            pricebookEntry.UnitPrice = 10;
            pricebookEntry.IsActive = true;
            pricebookEntries.add(pricebookEntry);
            productIds.add(product.Id);
        }
        System.debug('pricebookEntries-1-'+pricebookEntries);
        insert pricebookEntries;
        System.debug('productIds--'+productIds);
        List<PricebookEntry> priceBookEntriesQueried = [Select Id,UnitPrice,Product2Id,Pricebook2Id,IsActive from PricebookEntry where Product2Id IN : productIds];
        System.debug('pricebookEntries-2-'+pricebookEntries);
        return priceBookEntriesQueried;
    }
}