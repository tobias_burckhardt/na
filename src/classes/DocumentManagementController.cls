public with sharing class DocumentManagementController {

    /**
     * Returns the 'contentDocumentId' URL parameter. If present the related VF page will
     * download the ContentVersion record related to the given ContentDocument automatically
     * when loading the page.
     */
    public String contentDocumentId {
        get {
            return ApexPages.currentPage().getParameters().get('contentDocumentId');
        }
    }

    @RemoteAction
    public static List<Node> getMenuTree(){
        return ContentService.createTree();
    }

    @RemoteAction
    public static List<ContentVersion> getDocumentsMetadata(String path){
        return ContentService.getDocumentsMetadata(path);
    }

    @RemoteAction
    public static List<ContentService.ContentVersionWrapper> getDocumentsData(List<String> documentIds){
        return ContentService.getDocumentsContent(documentIds);
    }

    @RemoteAction
    public static ContentVersionModel getDocumentData(Id documentId) {
        ContentVersion document = [
            SELECT Title, FileExtension, VersionData
            FROM ContentVersion
            WHERE ContentDocumentId = :documentId AND IsLatest = true
        ];
        return new ContentVersionModel(document);
    }

    @RemoteAction
    public static List<ContentVersion> searchDocuments(String name){
        return ContentService.getDocumentsByNameLike(name);
    }

    private class ContentVersionModel {
        public String title;
        public String fileExtension;
        public String versionData;
        public ContentVersionModel(ContentVersion document) {
            this.title = document.Title;
            this.fileExtension = document.FileExtension;
            this.versionData = EncodingUtil.base64Encode(document.VersionData);
        }
    }
}