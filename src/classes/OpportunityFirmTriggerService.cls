/**
* @description service for OpportunityFirmTrigger trigger 
*/
public class OpportunityFirmTriggerService {
    /**
	* @description Insert OpportunityContactRole record based on Opportunity_Firm__c fields
	* @param new list Opportunity_Firm__c
	*/
    public static void createOpportynityContactRole(List<Opportunity_Firm__c> firms) {
        List<OpportunityContactRole> opportunityContactRoles = new List<OpportunityContactRole>();

        for (Opportunity_Firm__c firm : firms) {
            opportunityContactRoles.add(new OpportunityContactRole(
                OpportunityId = firm.Opportunity__c, 
                ContactId = firm.Contact__c, 
                Role = firm.Firm_Role__c)
            );
        }

        try {
            if (!opportunityContactRoles.isEmpty()) insert opportunityContactRoles;
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
    }
}