public without sharing class sika_SCCZoneAccountTeamCheckHelper {
    private static Boolean hasRun = false;
    @testVisible private static Boolean doNotLimit = false; // used by AccountTeamCheckHelperTest to allow this method to fire more than once in an execution context. (Overrides hasRun)

    // abstracted trigger helper method for preventing salespeople from editing or deleting Mix, Material, and Test Result records associated with accounts on which they are not accountTeamMembers (or owners)
    // the SCC Zone permission set should be added to SCC Zone salespeople, and should provide view all & modify all access to the Mix, Material, and Test Result objects.
    // Objects making use of this method must have a formula field named "Account_ID", which contains the ID of the record owner's account.
    public static void accountTeamCheck(list<sObject> theObjects, String objectName, String actionName){
        if(hasRun == true && doNotLimit == false){
            system.debug('******** AccountTeamCheck trigger has already run once.  Skipping.');
        } else {
            hasRun = true;  
            
            system.debug('********TRG: firing the accountTeamCheck trigger from ' + objectName);

            String profileName = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId() Limit 1].Name;

            // if the user is not an admin or an SCC Zone user
            if(profileName == 'System Administrator' || profileName == Sika_SCCZone_Common.adminUserRoleName || 
                 profileName == Sika_SCCZone_Common.SCCZoneUserProfileLoginName ||  profileName == Sika_SCCZone_Common.SCCZoneUserProfileUserName){
                system.debug('******** User is either an Administrator or an SCC Zone user. Skipping accountTeamCheck.');
            
            } else {
                // for each object, if the object isn't owned by the current user, add the object to a list of objects of the same type.
                // (The only objects that get put into a list are objects that the current user does not own. We'll check these objects' accountTeams to see whether the current user is a member.)
                String currentUserId = UserInfo.getUserId();

                list<Mix__c> mixes = new list<Mix__c>();
                list<Material__c> materials = new list<Material__c>();
                list<Mix_Material__c> mms = new list<Mix_Material__c>();
                list<Test_Result__c> testResults = new list<Test_Result__c>();
        
                Integer count = 0;
                
                for(sObject s : theObjects){
                    System.debug('MSA sObjectType = ' +s.getSObjectType() );
                    if(s.getSObjectType() == Mix__c.sObjectType || s.getSObjectType() == Material__c.sObjectType){
                        System.debug('MSA sObjectType = '  + String.valueOf(s.get('OwnerId')) );
                        System.debug('MSA currentUserId = ' + currentUserId );

                        if (String.valueOf(s.get('OwnerId')) == String.valueOf(currentUserId)){
                            system.debug('******** User owns recrod ' + s.Id + ', and therefore passes accountTeamCheck.');
                            continue;
                        }
                    } else {
                        if(String.valueOf(s.get('Owner_Id__c')) == String.valueOf(currentUserId)){
                        system.debug('******** User owns recrod ' + s.Id + ', and therefore passes accountTeamCheck.');
                        continue;
                        }
                    }
                    
                    // ...otherwise, break the sObjects out into lists of like object types
                    Mix__c mix;
                    Material__c material;
                    Mix_Material__c mm;
                    Test_Result__c testResult;

                    if(s.getSObjectType() == Mix__c.sObjectType){
                        mix = (Mix__c) s;
                        mixes.add(mix);
                        count ++;
                        continue;
                    }
                    if(s.getSObjectType() == Material__c.sObjectType){
                        material = (Material__c) s;
                        materials.add(material);
                        count ++;
                        continue;
                    }
                    if(s.getSObjectType() == Mix_Material__c.sObjectType){
                        mm = (Mix_Material__c) s;
                        mms.add(mm);
                        count ++;
                        continue;
                    }
                    if(s.getSObjectType() == Test_Result__c.sObjectType){
                        testResult = (Test_Result__c) s;
                        testResults.add(testResult);
                        count ++;
                        continue;
                    }
                }

                if(count > 0){ // if there are any objects being operated on, that aren't owned by the current user, ...
                    // map each object's Id to its associated Account Id
                    map<Id, Id> objectId_to_AccountId = new map<Id, Id>();
                    if(!mixes.isEmpty()) { objectId_to_AccountId.putAll(getIDsMap(mixes)); }
                    if(!materials.isEmpty()) { objectId_to_AccountId.putAll(getIDsMap(materials)); }
                    if(!mms.isEmpty()) { objectId_to_AccountId.putAll(getIDsMap(mms)); }
                    if(!testResults.isEmpty()) { objectId_to_AccountId.putAll(getIDsMap(testResults)); }
                    
                    // find all SCC Zone or BU account team members associated with each referenced account
                    set<Id> accountIDsSet = new set<Id>();
                    accountIDsSet.addAll(objectId_to_AccountId.values());
                    
                    // returns a map<Account ID -> set<Sales Rep IDs>>
                    map<Id, set<Id>> accountId_to_repIDs = new map<Id, set<Id>>();
                    accountId_to_repIDs = sika_SCCZoneFindSalesRepHelper.getAccountIDsAndRepIDs(accountIDsSet);
    
                    // for each object, if the current user isn't an SCC rep on the AccountTeam associated with the object,
                    // add an error to the object; don't complete the DML operation
                    Id tempAcctId;
                    set<Id> tempMapRepIDs;
                    for(sObject s : theObjects){
                        tempAcctId = objectId_to_AccountId.get(s.Id);
                        tempMapRepIDs = accountId_to_repIDs.get(tempAcctId);

                        if(tempMapRepIDs == null || tempMapRepIDs.contains(currentUserId) == false){
    
                            s.addError('Only users associated with this ' + objectName + '\'s owner\'s account can ' + actionName + ' this record.');
                        } 
                    }
                }
            }
        }

    }


    private static map<Id, Id> getIDsMap(list<Mix__c> records){
        map<Id, Id> theMap = new map<Id, Id>();
        set<Id> recordIDs = new set<Id>();
        for(Mix__c r : records){
            recordIDs.add(r.Id);
        }

        list<Mix__c> theMixes = [SELECT Id, Account__c FROM Mix__c WHERE Id IN :recordIDs];
        for(Mix__c m : theMixes){
            theMap.put(m.Id, m.Account__c);
        }
        return theMap;
    }

    private static map<Id, Id> getIDsMap(list<Material__c> records){
        map<Id, Id> theMap = new map<Id, Id>();
        set<Id> recordIDs = new set<Id>();
        for(Material__c r : records){
            recordIDs.add(r.Id);
        }

        list<Material__c> theMaterials = [SELECT Id, Account__c FROM Material__c WHERE Id IN :recordIDs];
        for(Material__c m : theMaterials){
            theMap.put(m.Id, m.Account__c);
        }
        return theMap;
    }

    private static map<Id, Id> getIDsMap(list<Mix_Material__c> records){
        map<Id, Id> theMap = new map<Id, Id>();
        set<Id> recordIDs = new set<Id>();
        for(Mix_Material__c r : records){
            recordIDs.add(r.Id);
        }

        list<Mix_Material__c> theMixMaterials = [SELECT Id, Account__c FROM Mix_Material__c WHERE Id IN :recordIDs];
        for(Mix_Material__c m : theMixMaterials){
            theMap.put(m.Id, m.Account__c);
        }
        return theMap;
    }

    private static map<Id, Id> getIDsMap(list<Test_Result__c> records){
        map<Id, Id> theMap = new map<Id, Id>();
        set<Id> recordIDs = new set<Id>();
        for(Test_Result__c r : records){
            recordIDs.add(r.Id);
        }

        list<Test_Result__c> theTestResults = [SELECT Id, Account_ID__c FROM Test_Result__c WHERE Id IN :recordIDs];
        for(Test_Result__c m : theTestResults){
            theMap.put(m.Id, m.Account_ID__c);
        }
        return theMap;
    }

}