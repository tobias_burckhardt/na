@isTest
public class CaseEmailChatterFileTestHelper {

    //Pre-created blobs for initial and modified attachment bodies
    private static final Blob TEST_BODY = Blob.valueOf('Test Body');

    //Creates and returns the specified number of attachments for the given parent id
    public static List<FeedItem> createTestEmailAttachment(Id parentId, Integer num) {
        List<ContentVersion> cvs = new List<ContentVersion>();
        List<FeedItem> fis = new List<FeedItem>();
        
        for (Integer i = 0; i < num; i++) {
            String name = 'Test File #'+i;
            ContentVersion cv = new ContentVersion();
            cv.Title = name;
            cv.VersionData = TEST_BODY;
            cv.PathOnClient = name;
            cv.ContentLocation = 'S';
            cv.Origin = 'H';
            insert cv;
            cvs.add(cv);
        
            FeedItem fi = new FeedItem();
            fi.Body = 'Email attachment for case: '+ parentId;
            fi.ParentId = parentId;
            fi.RelatedRecordId = cv.Id;
            fi.Type = 'ContentPost';
            fis.add(fi);
        }

        insert fis;
        return fis;
    }
}