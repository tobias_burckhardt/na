public with sharing class AwardQuoteController {
	public Opportunity oppRecord { get; set; }
	public Applicator_Quote__c applicatorQuote {get; set;}
	public List<ApttusProposalWrapper> apttusProposalWC { get; set; }

	private static final AwardQuoteControllerWithoutSharing TRAMPOLINE =
		new AwardQuoteControllerWithoutSharing();

	private User currentUser { get; set; }
	private Map<Id, Map<Id, ApttusProposalWrapper>> apttusProposalMap;

	public AwardQuoteController(ApexPages.StandardController stdController) {
		oppRecord = OpportunityServices.GetOpportunity(stdController.getId());

		List<Profile> communityProfiles = UserDao.getUserPartnerProfileIds();
		Set<Id> profileIds = Pluck.ids(communityProfiles);
		Boolean isCommunityUser = profileIds.contains(UserInfo.getProfileId());
		if (ApexPages.currentPage().getParameters().get('qId') == null) {
			if (isCommunityUser) {
				User currentUser = [SELECT Id, AccountId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
				applicatorQuote = [SELECT Id, Account__c, Contact__c FROM Applicator_Quote__c
						WHERE Opportunity__c = :oppRecord.Id AND Account__c = :currentUser.AccountId LIMIT 1];
			}
		} else {
			applicatorQuote = [SELECT Id, Account__c, Contact__c FROM Applicator_Quote__c
				WHERE Id = :ApexPages.currentPage().getParameters().get('qId')];
		}

		currentUser = [SELECT Id, AccountId FROM User WHERE Id = :userInfo.getUserId()];

		getApttusProposal();
		getApttusProposalLineItems();
		constructBuildingGroup();
	}

	public void getApttusProposal() {
		apttusProposalMap = new Map<Id, Map<Id, ApttusProposalWrapper>>();

		List<Apttus_Proposal__Proposal__c> proposals = TRAMPOLINE.getProposal(oppRecord.Id);
		for (Apttus_Proposal__Proposal__c app : proposals) {
			apttusProposalMap.put(app.Id, new Map<Id, ApttusProposalWrapper>());
		}
	}

	public void getApttusProposalLineItems() {
		if (apttusProposalMap != null) {
			List<Apttus_Proposal__Proposal_Line_Item__c> lineItems =
				TRAMPOLINE.getLineItems(apttusProposalMap.keySet());

			for (Apttus_Proposal__Proposal_Line_Item__c apli : lineItems) {
				ApttusProposalLineItemWrapper tempAPLIW = new ApttusProposalLineItemWrapper(apli);
				tempAPLIW.buildingGroupProduct.Proposal_Line_Item__c = apli.Id;
				tempAPLIW.buildingGroupProduct.Category_Hierarchy__c =
					apli.Apttus_QPConfig__ClassificationId__c;
				tempAPLIW.buildingGroupProduct.Product_lookup__c = apli.Apttus_Proposal__Product__c;
				tempAPLIW.buildingGroupProduct.Attachment_Group__c =
					apli.Apttus_Proposal__Product__r.Attachment_Group__c;

				Id proposalId = apli.Apttus_Proposal__Proposal__c;
				Id hierarchyId =
					apli.Apttus_QPConfig__ClassificationId__r.Apttus_Config2__HierarchyId__c;

				//group by proposal and then by category
				if (!apttusProposalMap.get(proposalId).containsKey(hierarchyId)) {
					apttusProposalMap.get(proposalId).put(
						hierarchyId,
						new ApttusProposalWrapper()
					);
				}

				apttusProposalMap.get(proposalId).get(hierarchyId).lineItems.add(tempAPLIW);

				//Costmetic field values
				apttusProposalMap.get(proposalId).get(hierarchyId).buildingGroup.Opportunity__c =
					oppRecord.Id;
				apttusProposalMap.get(proposalId).get(hierarchyId).buildingGroup.Category__c =
					hierarchyId;
				apttusProposalMap.get(proposalId).get(hierarchyId).buildindGroupSystem =
					apli.Apttus_QPConfig__ClassificationId__r.Apttus_Config2__HierarchyId__r.Name;

				Id productId = apli.Apttus_Proposal__Product__c;

				//Construct select options from children
				if (productId != null && apli.Apttus_QPConfig__ClassificationId__c != null) {

					Boolean productIsMembrane =
						apli.Apttus_Proposal__Product__r.Name.containsIgnoreCase('Membrane');
					Boolean productIsCustom =
						apli.Apttus_Proposal__Product__r.Name.containsIgnoreCase('Custom');
					Boolean productIsStocked =
						apli.Apttus_Proposal__Product__r.Name.containsIgnoreCase('Stocked');
					Boolean productIsWarranty =
						apli.Apttus_Proposal__Product__r.Name.contains('Warranty');
					if(productIsWarranty){
						if(apli.Apttus_Proposal__Product__r.Name.contains('Flat')){
							productIsWarranty = false;
						}
					} 
					Boolean productIsExtension =
						apli.Apttus_Proposal__Product__r.Name.contains('Extension');
					Boolean classIsMembrane =
						apli.Apttus_QPConfig__ClassificationId__r.Name.containsIgnoreCase('Membrane');
					Boolean classIsCustom =
						apli.Apttus_QPConfig__ClassificationId__r.Name.containsIgnoreCase('Custom');
					Boolean classIsStocked =
						apli.Apttus_QPConfig__ClassificationId__r.Name.containsIgnoreCase('Stocked');
					
					if (( (productIsMembrane && (productIsCustom || productIsStocked)) ||
							(classIsMembrane && (classIsCustom || classIsStocked)) ) &&
							!productIsWarranty) {
						apttusProposalMap.get(proposalId).get(hierarchyId).primaryMembrane.add(
							new SelectOption(
								apli.Apttus_Proposal__Product__c,
								apli.Apttus_Proposal__Product__r.APTS_Roofing_Description__c
							)

						);
					}

					if (productIsWarranty && !productIsExtension) {
						apttusProposalMap.get(proposalId).get(hierarchyId).primaryWarranty.add(
							new SelectOption(
								apli.Apttus_Proposal__Product__c,
								apli.Apttus_Proposal__Product__r.Name
							)
						);
					}

					if (productIsWarranty && productIsExtension) {
						apttusProposalMap.get(proposalId).get(hierarchyId).primaryExtension.add(
							new SelectOption(
								apli.Apttus_Proposal__Product__c,
								apli.Apttus_Proposal__Product__r.Name
							)
						);
					}
				}
			}
		}
	}

	public void constructBuildingGroup() {
		apttusProposalWC = new List<ApttusProposalWrapper>();
		for (Id i : apttusProposalMap.keySet()) {
			for (Id j : apttusProposalMap.get(i).keySet()) {
				apttusProposalWC.add(apttusProposalMap.get(i).get(j));
			}
		}
	}

	public PageReference awardNOA() {
		Savepoint sp = Database.setSavepoint();

		List<Profile> communityProfiles = UserDao.getUserPartnerProfileIds();
		Set<Id> profileIds = Pluck.ids(communityProfiles);
		Boolean isCommunityUser = profileIds.contains(UserInfo.getProfileId());

		try {
			applicatorQuote.Award_NOA__c = true;
			applicatorQuote.OwnerId = currentUser.id;
			update applicatorQuote;
			List<ApttusProposalWrapper> results =
				AwardQuoteServices.filterProposals(isCommunityUser, apttusProposalWC);
			if (!results.isEmpty()) {
				AwardQuoteServices.createBuildingGroupsAndProducts(isCommunityUser, results);
				AwardQuoteServices.setApplicatorQuoteAwardedValuesOnOpp(currentUser, oppRecord, applicatorQuote);
				//OpportunitySharingServices.shareOppsWithPTS(new List<Opportunity>{oppRecord});
				createCommunityUserShares();
				PageReference pageRef = new PageReference('/' + oppRecord.Id);
				pageRef.setRedirect(true);
				return pageRef;
			} else {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,
						'One of your building groups is missing required information.'));
			}
		} catch(DmlException e) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'DmlException: ' + e));
			Database.rollback(sp);
		} catch(AwardQuoteServices.IncompleteBuildingGroupException e) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,
				'One of your building groups is missing required information.'));
			Database.rollback(sp);
		} catch(AwardQuoteServices.MissingDeckProductException e) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
				'Can not find any deck Product.'));
			Database.rollback(sp);
		}

		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Record was not saved.'));
		return null;
	}

	private void createCommunityUserShares(){
		Map<Id,List<Id>> userToOpportunityListMap = new Map<Id,List<Id>>();
		Map<Id,List<Id>> userToQuoteListMap = new Map<Id,List<Id>>();
		List<User> usersToShareTo = getUsersToShare(applicatorQuote.Account__c);
		for(User usr : usersToShareTo){
			if(usr.Id != oppRecord.OwnerId){
				userToOpportunityListMap.put(usr.Id,new List<Id>{oppRecord.Id});
			}
			if(usr.Id != applicatorQuote.ownerId){
				userToQuoteListMap.put(usr.Id, new List<Id>{applicatorQuote.Id});
			}
		}
		OpportunitySharingServices.addOpportunitySharesToCommunityUsers(userToOpportunityListMap);
		ApplicatorQuoteSharingServices.addApplicatorQuoteSharesToCommunityUsers(userToQuoteListMap);
	}

	private List<User> getUsersToShare(Id accountId){
		List<Id> communityProfileIds = getCommunityProfileIds();
		return [SELECT id,Contact.AccountId FROM User WHERE Contact.AccountId = :accountId 
										AND IsActive = true
										AND IsPortalEnabled = true
										AND ContactId != null
										AND ProfileId IN :communityProfileIds];
	}

	private List<Id> getCommunityProfileIds(){
		List<Id> communityProfileIds = new List<Id>();
		for(Profile prof : UserDAO.getUserPartnerProfileIds()){
			communityProfileIds.add(prof.Id);
		}
		return communityProfileIds;
	}

	public class ApttusProposalWrapper {
		public Boolean isSelected {get; set;}

		public Apttus_Proposal__Proposal__c apttusProposal {get; set;}
		public Building_Group__c buildingGroup {get; set;}
		public String buildindGroupSystem {get; set;}

		public String selectedMembrane {get; set;}
		public List<SelectOption> primaryMembrane {get; set;}
		public String selectedWarranty {get; set;}
		public List<SelectOption> primaryWarranty {get; set;}
		public String selectedExtension {get; set;}
		public List<SelectOption> primaryExtension {get; set;}

		public List<ApttusProposalLineItemWrapper> lineItems {get; set;}

		public ApttusProposalWrapper() {
			this.isSelected = false;

			this.apttusProposal = new Apttus_Proposal__Proposal__c();
			this.buildingGroup = new Building_Group__c();
			this.buildindGroupSystem = '';
			this.primaryMembrane = new List<SelectOption>();
			primaryMembrane.add(new SelectOption('', 'None'));
			this.primaryWarranty = new List<SelectOption>();
			primaryWarranty.add(new SelectOption('', 'None'));
			this.primaryExtension = new List<SelectOption>();
			primaryExtension.add(new SelectOption('', 'None'));

			this.lineItems = new List<ApttusProposalLineItemWrapper>();
		}
	}

	public class ApttusProposalLineItemWrapper {
		public Apttus_Proposal__Proposal_Line_Item__c apttusProposalLineItem {get; set;}
		public Building_Group_Product__c buildingGroupProduct {get; set;}

		public ApttusProposalLineItemWrapper() {
			this(new Apttus_Proposal__Proposal_Line_Item__c());
		}

		public ApttusProposalLineItemWrapper(Apttus_Proposal__Proposal_Line_Item__c apli) {
			this.apttusProposalLineItem = apli;
			this.buildingGroupProduct = new Building_Group_Product__c();
		}
	}
}