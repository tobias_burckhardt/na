@isTest
public class EBWTechReviewContExtTests {

    @isTest
    public static void testController(){

		Account a = new Account(Name='TestAcc'); insert a;
        
        List<Opportunity> opps = TestUtilities.CreateOpportunities(new List<Account>{a},1,true);
        
        ApexPages.StandardController sc = new ApexPages.standardController(opps[0]);
        EBWTechReviewContExt e = new EBWTechReviewContExt(sc);
        
        System.assertEquals(e.opp, opps[0]);
        System.assert(!e.secondPage);
        System.assert(!e.validated);
    }
    
    @isTest
    public static void testYes(){
		Account a = new Account(Name='TestAcc'); insert a;
        
        List<Opportunity> opps = TestUtilities.CreateOpportunities(new List<Account>{a},1,true);
        
        ApexPages.StandardController sc = new ApexPages.standardController(opps[0]);
        EBWTechReviewContExt e = new EBWTechReviewContExt(sc);
	
		e.yes();

		System.assert(e.secondPage);        
    }
    
    /*@isTest
    public static void testValidateTrue(){

		// THIS TEST IS FAILING, because there is some logic/functionality changing the "Elite__c" field on Account from true to false

        Account a = new Account(Name='TestAcc', Elite__c = true); insert a;
        System.debug(Logginglevel.ERROR, 'a ' + a);
        Opportunity opp = TestUtilities.CreateOpportunities(new List<Account>{a},1,true)[0];
        Opportunity_Firm__c oppf = new Opportunity_Firm__c(Opportunity__c=opp.Id, Firm_Role__c='Building owner'); insert oppf;
        Building_Group__c bg = new Building_Group__c(Opportunity__c=opp.Id); insert bg;
        
        a = [SELECT Id, Name, Elite__c FROM Account];
        System.debug(Logginglevel.ERROR, 'a ' + a);
        
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        EBWTechReviewContExt e = new EBWTechReviewContExt(sc);
	         
		e.validate();
        
        System.assert(e.validated);
    }*/
    
    @isTest
    public static void testAgreeMessage(){
        Account a = new Account(Name='TestAcc', Elite__c = true); insert a;
        Opportunity opp = TestUtilities.CreateOpportunities(new List<Account>{a},1,false)[0];
        opp.Amount = 5;
        opp.EBW_Delivery_Company_Name__c = '';
        insert opp;
        
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        EBWTechReviewContExt e = new EBWTechReviewContExt(sc);
        
        PageReference pr = e.agree();
        
        List<Apexpages.Message> msgs = ApexPages.getMessages();
		Boolean b = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('Please fill in Warranty Start Date, Delivery company name and Delivery company account number.')) b = true;
        }
        system.assert(b);
        
    }
    
    @isTest
    public static void testAgree(){
        Account a = new Account(Name='TestAcc', Elite__c = true); insert a;
        Opportunity opp = TestUtilities.CreateOpportunities(new List<Account>{a},1,false)[0];
        opp.Amount = 5;
        opp.EBW_T_C_Accepted__c = true;
        opp.EBW_Start_Date__c = Date.today();
        opp.EBW_Delivery_Company_Name__c = 'TestCompName';
        opp.EBW_Company_Account_Number__c = 'TestCompAccountNumber';
        opp.Roofing_Primary_Contact_Email__c = 'ebwtechreviewtest@sika.com';
        insert opp;
        
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        EBWTechReviewContExt e = new EBWTechReviewContExt(sc);
        
        e.validated = true;
        PageReference pr = e.agree();
        
        System.assertEquals(new PageReference('/'+opp.Id).getUrl(), pr.getUrl());
        Opportunity oppUpdated = [SELECT Id, EBW__c FROM Opportunity WHERE Id=:opp.Id];
        System.assertEquals(true, oppUpdated.EBW__c);
    }

}