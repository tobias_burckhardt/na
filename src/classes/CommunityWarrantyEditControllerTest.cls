@isTest 
public class CommunityWarrantyEditControllerTest{
	private static Project__c p1;
	private static Warranty__c w1;
	private static Warranty_Line__c wl1;
	private static Account acct;
    private static Id infRecordTypeId;
    private static Id customerWarrantyRecordTypeId;
	static void setup() {
		p1 = TestingUtils.createProjects(1, false)[0];
		p1.Name = 'Test';
		p1.Street_Address__c = '123 Test';
		p1.City__c = 'Chicago';
		p1.Country__c = 'United States';
		p1.State__c = 'IL';
		p1.Zip__c = '60652';
		p1.Description__c = 'This is a test';
		insert p1;

        w1 = TestingUtils.createWarranties(1, p1, true)[0];
        
        wl1 = TestingUtils.createWarrantyLines(1, w1, 1, true)[0];

        acct = TestingUtils.createAccounts('Test', 1, false)[0];
        acct.Name = 'Test';
		acct.BillingStreet = '123 Test';
		acct.BillingCity = 'New York';
		acct.BillingCountry = 'United States';
		acct.BillingState = 'IL';
		acct.BillingPostalCode = '60652';
		insert acct;

        Pricebook__c pb1 = new Pricebook__c();
        pb1.Name = 'Commercial RSB';
        pb1.Pricebook_Name__c = 'Warranty Family Pick';
        pb1.Picklist_Value__c = 'Commercial RSB';
        pb1.Warranty_Record__c = 'Customer_Warranty';
        insert pb1;

        Pricebook__c pb2 = new Pricebook__c();
        pb2.Name = 'INF';
        pb2.Pricebook_Name__c = 'INF';
        pb2.Picklist_Value__c = 'INF';
        pb2.Warranty_Record__c = 'INF';
        insert pb2;

        Schema.RecordTypeInfo cwInfo = Schema.SObjectType.Warranty__c.getRecordTypeInfosByName().get('Customer Warranty');
        System.assertNotEquals(null, cwInfo);
        customerWarrantyRecordTypeId = cwInfo.getRecordTypeId();

        Schema.RecordTypeInfo infInfo = Schema.SObjectType.Warranty__c.getRecordTypeInfosByName().get('INF');
        System.assertNotEquals(null, infInfo);
        infRecordTypeId = infInfo.getRecordTypeId();
	}

    static testMethod void testCommunityWarrantyEditControllerRSB() {
    	setup();
        User u = TestingUtils.createTestUser('BlueWolf123test', 'System Administrator');
        u.Warranty_Type__c = 'Commercial RSB';
        insert u;

        Test.startTest();
            ApexPages.currentPage().getParameters().put('wid',w1.Id);
            ApexPages.currentPage().getParameters().put('pid',p1.Id);
        	CommunityWarrantyEditController commCon = new CommunityWarrantyEditController();
        	commCon.disblpart();
        Test.stopTest();
    
        System.assert(commCon.displayPopUp, 'This should be true');
        System.assertEquals(commCon.validateEntry(), '', 'This should be empty');
        System.assertEquals(commCon.getValue().size(), 0, 'This should be empty');
        System.assertEquals(1, commCon.contusers.size(), 'This should match the number in the pricebook');
        
    }

    static testMethod void testCommunityWarrantyEditControllerINF() {
        setup(); 
        User u = TestingUtils.createTestUser('BlueWolf123test', 'System Administrator');
        u.Warranty_Type__c = 'Commercial RSB';
        insert u;

        

        Test.startTest();
            ApexPages.currentPage().getParameters().put('wid',w1.Id);
            ApexPages.currentPage().getParameters().put('pid',p1.Id);
            CommunityWarrantyEditController commCon = new CommunityWarrantyEditController();
            commCon.disblpart();
        Test.stopTest();
    
        System.assert(commCon.displayPopUp, 'This should be true');
        System.assertEquals(commCon.validateEntry(), '', 'This should be empty');
        System.assertEquals(commCon.getValue().size(), 0, 'This should be empty');
        System.assertEquals(1, commCon.contusers.size(), 'This should match the number in the pricebook');
    }
    
    static testMethod void testCreateRecord() {
    	setup();
        User u = TestingUtils.createTestUser('BlueWolf123test', 'System Administrator');
        u.Warranty_Type__c = 'INF';
        insert u;

    	ApexPages.currentPage().getParameters().put('pid',p1.Id);
    	CommunityWarrantyEditController commCon = new CommunityWarrantyEditController();

        Test.startTest();
        	PageReference pg = commCon.createwarrantyrecord();
        Test.stopTest();
        Id wId = pg.getParameters().get('wid');

        Warranty__c newWarranty = [SELECT Id, Warranty_Purpose__c, Target_Market__c, Warranty_Type__c, Project__c FROM Warranty__c WHERE Id =: wId][0];
        System.assertEquals(newWarranty.Project__c, p1.Id, 'This should be the assigned value');
        System.assertEquals(newWarranty.Warranty_Purpose__c, 'Warranty', 'This should be the assigned value');
        //System.assertEquals(newWarranty.Target_Market__c, 'Interior Finishing', 'This should be the assigned value');
        System.assertEquals(newWarranty.Warranty_Type__c, 'Standard Material', 'This should be the assigned value');
        System.assert(pg.getURL().contains('/apex/CommunityWarrantyEdit?wid='));

    }

    static testMethod void testUpload() {
    	setup();
        User u = TestingUtils.createTestUser('BlueWolf123test', 'System Administrator');
        u.Warranty_Type__c = 'Commercial RSB';
        insert u;

    	ApexPages.currentPage().getParameters().put('wid',w1.Id);
    	ApexPages.currentPage().getParameters().put('pid',p1.Id);
    	CommunityWarrantyEditController commCon = new CommunityWarrantyEditController();
    	commCon.attachment.Name = 'new1';
    	commCon.attachment2.Name = 'new2';
    	commCon.attachment.Body = Blob.valueOf('body1');
    	commCon.attachment2.Body = Blob.valueOf('body2');

        Test.startTest();
            commCon.warranty = w1;
        	commCon.upload();
        Test.stopTest();

        List<Attachment> attches = [SELECT Id, OwnerId, ParentId FROM Attachment WHERE OwnerId =: UserInfo.getUserId()];

        System.assertEquals(2, attches.size(), 'There should only be two attachments that have this info');
    }

    static testMethod void testMeProject() {
    	setup();   
        User u = TestingUtils.createTestUser('BlueWolf123test', 'System Administrator');
        u.Warranty_Type__c = 'Commercial RSB';
        insert u;

    	ApexPages.currentPage().getParameters().put('wid',w1.Id);
    	ApexPages.currentPage().getParameters().put('pid',p1.Id);
    	ApexPages.currentPage().getParameters().put('Recordid',p1.Id);
    	ApexPages.currentPage().getParameters().put('tabname','Project');
    	CommunityWarrantyEditController commCon = new CommunityWarrantyEditController();

        Test.startTest();
        	commCon.testMe();
        Test.stopTest();

        System.assertEquals(commCon.warranty.Project__c , p1.Id, 'These should match');
        System.assertEquals(commCon.warranty.Project_Name__c, p1.Name, 'These should match');
        System.assertEquals(commCon.warranty.Project_Address__c, p1.Street_Address__c, 'These should match');
        System.assertEquals(commCon.warranty.Project_City__c, p1.City__c, 'These should match');
        System.assertEquals(commCon.warranty.Project_Country__c, p1.Country__c, 'These should match');  
        System.assertEquals(commCon.warranty.Project_State__c, p1.State__c, 'These should match');
        System.assertEquals(commCon.warranty.Project_Zip_Code__c, p1.Zip__c, 'These should match');
    }

    static testMethod void testMeOwner() {
    	setup();
        User u = TestingUtils.createTestUser('BlueWolf123test', 'System Administrator');
        u.Warranty_Type__c = 'Commercial RSB';
        insert u;

    	ApexPages.currentPage().getParameters().put('wid',w1.Id);
    	ApexPages.currentPage().getParameters().put('pid',p1.Id);
    	ApexPages.currentPage().getParameters().put('Recordid',acct.Id);
    	ApexPages.currentPage().getParameters().put('tabname','Owner');
    	CommunityWarrantyEditController commCon = new CommunityWarrantyEditController();

        Test.startTest();
        	commCon.testMe();
        Test.stopTest();

        System.assertEquals(commCon.warranty.Owner_Name__c, acct.Name, 'These should match');
        System.assertEquals(commCon.warranty.Owner_Address__c, acct.BillingStreet, 'These should match');
        System.assertEquals(commCon.warranty.Owner_City__c, acct.BillingCity, 'These should match');
        System.assertEquals(commCon.warranty.Owner_Country__c, acct.BillingCountry, 'These should match');
        System.assertEquals(commCon.warranty.Owner_State__c, acct.BillingState, 'These should match');
        System.assertEquals(commCon.warranty.Owner_Zip_Code__c, acct.BillingPostalCode, 'These should match');
    }

    static testMethod void testMeApplicator() {
    	setup();
        User u = TestingUtils.createTestUser('BlueWolf123test', 'System Administrator');
        u.Warranty_Type__c = 'Commercial RSB';
        insert u;

    	ApexPages.currentPage().getParameters().put('wid',w1.Id);
    	ApexPages.currentPage().getParameters().put('pid',p1.Id);
    	ApexPages.currentPage().getParameters().put('Recordid',acct.Id);
    	ApexPages.currentPage().getParameters().put('tabname','Applicator');
    	CommunityWarrantyEditController commCon = new CommunityWarrantyEditController();

        Test.startTest();
        	commCon.testMe();
        Test.stopTest();

        System.assertEquals(commCon.warranty.Applicator_Name__c, acct.Name, 'These should match');
        System.assertEquals(commCon.warranty.Applicator_Address__c, acct.BillingStreet, 'These should match');
        System.assertEquals(commCon.warranty.Applicator_City__c, acct.BillingCity, 'These should match');
        System.assertEquals(commCon.warranty.Applicator_Country__c, acct.BillingCountry, 'These should match');
        System.assertEquals(commCon.warranty.Applicator_State__c, acct.BillingState, 'These should match');
        System.assertEquals(commCon.warranty.Applicator_Zip_Code__c, acct.BillingPostalCode, 'These should match');
    }

    static testMethod void testMeGeneral_Contractor() {
    	setup();
        User u = TestingUtils.createTestUser('BlueWolf123test', 'System Administrator');
        u.Warranty_Type__c = 'Commercial RSB';
        insert u;

    	ApexPages.currentPage().getParameters().put('wid',w1.Id);
    	ApexPages.currentPage().getParameters().put('pid',p1.Id);
    	ApexPages.currentPage().getParameters().put('Recordid',acct.Id);
    	ApexPages.currentPage().getParameters().put('tabname','General_Contractor');
    	CommunityWarrantyEditController commCon = new CommunityWarrantyEditController();

        Test.startTest();
        	commCon.testMe();
        Test.stopTest();

        System.assertEquals(commCon.warranty.General_Contractor_Name__c, acct.Name, 'These should match');
        System.assertEquals(commCon.warranty.General_Contractor_Address__c, acct.BillingStreet, 'These should match');
        System.assertEquals(commCon.warranty.General_Contractor_City__c, acct.BillingCity, 'These should match');
        System.assertEquals(commCon.warranty.General_Contractor_Country__c, acct.BillingCountry, 'These should match');
        System.assertEquals(commCon.warranty.General_Contractor_State__c, acct.BillingState, 'These should match');
        System.assertEquals(commCon.warranty.General_Contractor_Zip_Code__c, acct.BillingPostalCode, 'These should match');
    }

    static testMethod void testMeArchitect() {
    	setup();
        User u = TestingUtils.createTestUser('BlueWolf123test', 'System Administrator');
        u.Warranty_Type__c = 'Commercial RSB';
        insert u;

    	ApexPages.currentPage().getParameters().put('wid',w1.Id);
    	ApexPages.currentPage().getParameters().put('pid',p1.Id);
    	ApexPages.currentPage().getParameters().put('Recordid',acct.Id);
    	ApexPages.currentPage().getParameters().put('tabname','Architect');
    	CommunityWarrantyEditController commCon = new CommunityWarrantyEditController();

        Test.startTest();
        	commCon.testMe();
        Test.stopTest();

        System.assertEquals(commCon.warranty.Architect_Name__c, acct.Name, 'These should match');
        System.assertEquals(commCon.warranty.Architect_Address__c, acct.BillingStreet, 'These should match');
        System.assertEquals(commCon.warranty.Architect_City__c, acct.BillingCity, 'These should match');
        System.assertEquals(commCon.warranty.Architect_Country__c, acct.BillingCountry, 'These should match');
        System.assertEquals(commCon.warranty.Architect_State__c, acct.BillingState, 'These should match');
        System.assertEquals(commCon.warranty.Architect_Zip_Code__c, acct.BillingPostalCode, 'These should match');
    }

    static testMethod void testMeDistributor() {
    	setup();
        User u = TestingUtils.createTestUser('BlueWolf123test', 'System Administrator');
        u.Warranty_Type__c = 'Commercial RSB';
        insert u;

    	ApexPages.currentPage().getParameters().put('wid',w1.Id);
    	ApexPages.currentPage().getParameters().put('pid',p1.Id);
    	ApexPages.currentPage().getParameters().put('Recordid',acct.Id);
    	ApexPages.currentPage().getParameters().put('tabname','Distributor');
    	CommunityWarrantyEditController commCon = new CommunityWarrantyEditController();

        Test.startTest();
        	commCon.testMe();
        Test.stopTest();

        System.assertEquals(commCon.warranty.Distributor_Name__c, acct.Name, 'These should match');
        System.assertEquals(commCon.warranty.Distributor_Address__c, acct.BillingStreet, 'These should match');
        System.assertEquals(commCon.warranty.Distributor_City__c, acct.BillingCity, 'These should match');
        System.assertEquals(commCon.warranty.Distributor_Country__c, acct.BillingCountry, 'These should match');
        System.assertEquals(commCon.warranty.Distributor_State__c, acct.BillingState, 'These should match');
        System.assertEquals(commCon.warranty.Distributor_Zip_Code__c, acct.BillingPostalCode, 'These should match');
    }

    static testMethod void testSaveProject() {
    	setup();
        User u = TestingUtils.createTestUser('BlueWolf123test', 'System Administrator');
        u.Warranty_Type__c = 'Commercial RSB';
        insert u;

    	w1.Project_Name__c = 'Test123';
    	update w1;
    	ApexPages.currentPage().getParameters().put('wid',w1.Id);
    	ApexPages.currentPage().getParameters().put('pid',p1.Id);
    	ApexPages.currentPage().getParameters().put('Recordid',acct.Id);
    	CommunityWarrantyEditController commCon = new CommunityWarrantyEditController();
    	commCon.selectedtabname = 'Project';
    	commCon.warranty = w1;
        Test.startTest();
        	commCon.SaveWarranty();
        Test.stopTest();

        List<Project__c> newPrj = [SELECT Id, Name FROM Project__c WHERE Name = :w1.Project_Name__c];
    	System.assertEquals(1, newPrj.size(), 'There should only be one project with the same name');
    }

    static testMethod void testSaveOwner() {
    	setup();
        User u = TestingUtils.createTestUser('BlueWolf123test', 'System Administrator');
        u.Warranty_Type__c = 'Commercial RSB';
        insert u;

    	w1.Owner_Name__c = 'Test123';
    	update w1;
    	ApexPages.currentPage().getParameters().put('wid',w1.Id);
    	ApexPages.currentPage().getParameters().put('pid',p1.Id);
    	ApexPages.currentPage().getParameters().put('Recordid',acct.Id);
    	CommunityWarrantyEditController commCon = new CommunityWarrantyEditController();
    	commCon.selectedtabname = 'Owner';
    	commCon.warranty = w1;
        Test.startTest();
        	commCon.SaveWarranty();
        Test.stopTest();

        List<Account> newAcct = [SELECT Id, Name FROM Account WHERE Name = :w1.Owner_name__c];
    	System.assertEquals(1, newAcct.size(), 'There should only be one project with the same name');
    }

    static testMethod void testSaveApplicator() {
    	setup();
        User u = TestingUtils.createTestUser('BlueWolf123test', 'System Administrator');
        u.Warranty_Type__c = 'Commercial RSB';
        insert u;

    	w1.Applicator_name__c = 'Test123';
    	update w1;
    	ApexPages.currentPage().getParameters().put('wid',w1.Id);
    	ApexPages.currentPage().getParameters().put('pid',p1.Id);
    	ApexPages.currentPage().getParameters().put('Recordid',acct.Id);
    	CommunityWarrantyEditController commCon = new CommunityWarrantyEditController();
    	commCon.selectedtabname = 'Applicator';
    	commCon.warranty = w1;
        Test.startTest();
        	commCon.SaveWarranty();
        Test.stopTest();

        List<Account> newAcct = [SELECT Id, Name FROM Account WHERE Name = :w1.Applicator_name__c];
    	System.assertEquals(1, newAcct.size(), 'There should only be one project with the same name');
    }

    static testMethod void testSaveGeneralContractor() {
    	setup();
        User u = TestingUtils.createTestUser('BlueWolf123test', 'System Administrator');
        u.Warranty_Type__c = 'Commercial RSB';
        insert u;

    	w1.General_Contractor_name__c = 'Test123';
    	update w1;
    	ApexPages.currentPage().getParameters().put('wid',w1.Id);
    	ApexPages.currentPage().getParameters().put('pid',p1.Id);
    	ApexPages.currentPage().getParameters().put('Recordid',acct.Id);
    	CommunityWarrantyEditController commCon = new CommunityWarrantyEditController();
    	commCon.selectedtabname = 'GeneralContractor';
    	commCon.warranty = w1;
        Test.startTest();
        	commCon.SaveWarranty();
        Test.stopTest();

        List<Account> newAcct = [SELECT Id, Name FROM Account WHERE Name = :w1.General_Contractor_name__c];
    	System.assertEquals(1, newAcct.size(), 'There should only be one project with the same name');
    }

    static testMethod void testSaveArchitect() {
    	setup();
        User u = TestingUtils.createTestUser('BlueWolf123test', 'System Administrator');
        u.Warranty_Type__c = 'Commercial RSB';
        insert u;

    	w1.Architect_name__c = 'Test123';
    	update w1;
    	ApexPages.currentPage().getParameters().put('wid',w1.Id);
    	ApexPages.currentPage().getParameters().put('pid',p1.Id);
    	ApexPages.currentPage().getParameters().put('Recordid',acct.Id);
    	CommunityWarrantyEditController commCon = new CommunityWarrantyEditController();
    	commCon.selectedtabname = 'Architect';
    	commCon.warranty = w1;
        Test.startTest();
        	commCon.SaveWarranty();
        Test.stopTest();

        List<Account> newAcct = [SELECT Id, Name FROM Account WHERE Name = :w1.Architect_name__c];
    	System.assertEquals(1, newAcct.size(), 'There should only be one project with the same name');
    }

    static testMethod void testSaveDistributor() {
    	setup();
        User u = TestingUtils.createTestUser('BlueWolf123test', 'System Administrator');
        u.Warranty_Type__c = 'Commercial RSB';
        insert u;

    	w1.Distributor_name__c = 'Test123';
    	update w1;
    	ApexPages.currentPage().getParameters().put('wid',w1.Id);
    	ApexPages.currentPage().getParameters().put('pid',p1.Id);
    	ApexPages.currentPage().getParameters().put('Recordid',acct.Id);
    	CommunityWarrantyEditController commCon = new CommunityWarrantyEditController();
    	commCon.selectedtabname = 'Distributor';
    	commCon.warranty = w1;
        Test.startTest();
        	commCon.SaveWarranty();
        Test.stopTest();

        List<Account> newAcct = [SELECT Id, Name FROM Account WHERE Name = :w1.Distributor_name__c];
    	System.assertEquals(1, newAcct.size(), 'There should only be one project with the same name');
    }
}