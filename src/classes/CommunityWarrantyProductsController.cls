public with sharing class CommunityWarrantyProductsController {
    
    public string warrantyId {get; set;}
    public List<PricebookEntry> prodList {get; set;}
    public List<WarrantyProduct> warrantyProductList {get; set;}
    public List<WarrantyProduct> warrantyProdTempList;
    public Integer counter=0;
    public static Integer queryLimit = 10;
    public Integer currentPage {get; set;}
    public static Integer totalPages {get; set;}
    public Integer pageTotal = 0;
    public Boolean isFirst{get;set;}
    public Boolean isLast{get;set;}
    public String SelectedProdName { get; set; }
     public String SelectedProddesc { get; set; }
     Integer queryCount =0;  
    public CommunityWarrantyProductsController(){
        //determine if url parameters were passed in
        Map<string, string> pageParams = ApexPages.currentPage().getParameters();
        
        isFirst = true;
        isLast  =false;
        currentPage = 1;
        totalPages = 0;
        warrantyProductList = new List<WarrantyProduct>();
        prodList = new List<PricebookEntry>();
            warrantyProductList = new List<WarrantyProduct>();
            warrantyProdTempList = new List<WarrantyProduct>();
        if(pageParams.size() > 0){
            //store off the 
            warrantyId = pageParams.get('wid');
            
            if(warrantyId == null){
                //redirect back to warranty edit page?
                return;
            }
            
            //get max number of records in result set
            queryCount = [select count() from PricebookEntry where IsActive = true and Pricebook2.Name = 'Warranty Family Picklist'];
            totalPages = Integer.valueOf(Math.ceil(queryCount / queryLimit));
            pageTotal = totalPages;
            
            queryProducts();
        }
        else{
            prodList = new List<PricebookEntry>();
            warrantyProdTempList= new List<WarrantyProduct>();
        }
    }
    
    
    private void queryProducts(){
    
        string prodFieldList = 'Product2.Id, Product2.Name, Product2.ProductCode, Name, Product2.Description';
        string queryFormat = 'select {0} from PricebookEntry where IsActive=true and Pricebook2.Name = \'\'Warranty Family Picklist\'\'';
                
        system.debug('queryLimit  '+queryLimit +'  counter'+counter);
        
        system.debug(queryFormat);
        
        string prodQuery = String.format(queryFormat, new List<string>{prodFieldList});
        if( (SelectedProdName != null && SelectedProdName != '' )){
            prodQuery = prodQuery+' and (Product2.Description like \'%'+SelectedProdName+'%\' or product2.name like \'%'+SelectedProdName+'%\') order by Product2.Description desc limit :queryLimit offset :counter';
        List<PricebookEntry> lst=Database.query(prodQuery );
        queryCount=lst.size();
         totalPages = Integer.valueOf(Math.ceil(queryCount / queryLimit));
             pageTotal = totalPages;
        }
        
       
        
        else{
             prodQuery = prodQuery+' order by product2.Name desc limit :queryLimit offset :counter';
             queryCount = [select count() from PricebookEntry where IsActive = true and Pricebook2.Name = 'Warranty Family Picklist'];
             totalPages = Integer.valueOf(Math.ceil(queryCount / queryLimit));
             pageTotal = totalPages;
             
        }
        system.debug('prodQuery '+prodQuery);
        try{
        
            
            for(PricebookEntry p : Database.query(prodQuery )){
                //create warranty product list
                WarrantyProduct wp = new WarrantyProduct();
                wp.CatalogProduct = p;
                wp.IsSelected = false;
                warrantyProductList.add(wp);
               
            }
        }
        catch(Exception e){
            prodList = new List<PricebookEntry>();
            warrantyProductList= new List<WarrantyProduct>();
        }
        
        isFirst = counter == 0 ? true : false;
        isLast = warrantyProductList.size() < queryLimit ? true : false;
        
        checkPriorSelections();
    }
    //function to call for the selected products on base input from user
    public void selectedProducts(){
        if(warrantyProductList != null)
            warrantyProductList.clear();
        queryProducts();
        
    }
    public void setLimitToRecords(){
        warrantyProductList = new List<WarrantyProduct>();
        if( warrantyProdTempList != null && !warrantyProdTempList.isEmpty()){
            counter= 10; // =5 for testing
                
            for(integer i=0;i<warrantyProdTempList.size();i++){
                if(i==10) // =5 for testing
                    break;
                else
                    warrantyProductList.add(warrantyProdTempList[i]);    
                
            }
            if(warrantyProductList.size() == warrantyProdTempList.size() && warrantyProductList.size() <= counter )
                isLast = true;
        }
    }
    
    /*public void next(){
        if(!warrantyProductList.isEmpty()){
            
            warrantyProductList.clear();
            system.debug('counter inside next '+counter);
            for(integer i = counter; i<=warrantyProdTempList.size();i++){
                if(i == counter+10 || i == warrantyProdTempList.size()){ // +5 for testing
                    if(i== warrantyProdTempList.size())
                        isLast = true;
                    break;
                }       
                 warrantyProductList.add(warrantyProdTempList[i]); 
                
            }
            counter  = counter + 10;// +5 for testing
            isFirst = false;
            system.debug('counter end of next '+counter);
        }
    }*/
    
    public void next(){
        if(!warrantyProductList.isEmpty()){
            //gather selected prods
            for(WarrantyProduct wp : warrantyProductList){
                if(wp.IsSelected){
                    warrantyProdTempList.add(wp);
                }
            }
            
            //reset warranty products list
            warrantyProductList.clear();
            
            //set offset for next query
            counter += queryLimit;
            currentPage += 1;
            
            totalPages = pageTotal;
            
            queryProducts();
        }
    }
    
    /*public void previous(){
    
        if(!warrantyProductList.isEmpty()){
            warrantyProductList.clear();
            system.debug('counter inside previous'+counter);
            for(integer i = (counter-(10*2)); i<counter-10;i++){ //5 for testing
                
                
                    if(i== 0)
                        isFirst = true;
                    //break;
               // }
                warrantyProductList.add(warrantyProdTempList[i]); 
                
            }
            counter = counter -10;// =5 for testing
            isLast= false;
            system.debug('counter end of previous'+counter);
        }
    }*/
    
    public void previous(){
        if(!warrantyProductList.isEmpty()){
            //gather selected prods
            for(WarrantyProduct wp : warrantyProductList){
                if(wp.IsSelected){
                    warrantyProdTempList.add(wp);
                }
            }
            
            //reset warranty products list
            warrantyProductList.clear();
            
            //set offset for next query
            if(counter > 0){
                counter -= queryLimit;
            }
            if(currentPage > 0){
                currentPage -= 1;
            }
            
            totalPages = pageTotal;
            
            queryProducts();
        }
    }
    
    private void checkPriorSelections(){
        for(WarrantyProduct newProd : warrantyProductList){
            for(WarrantyProduct selProd : warrantyProdTempList){
                if(selProd.CatalogProduct.Id == newProd.CatalogProduct.Id){
                    newProd.IsSelected = true;
                    break;
                }
            }
        }
    }
    
    public PageReference CreateWarrantyLines(){
        //Check for existing warrantylines.
        List<Warranty_Line__c > oldLineItems = new List<Warranty_Line__c >();
        oldLineItems  = [select id from Warranty_Line__c where Product__c != null and Warranty__c=: warrantyId]; //This warranty line must have a associated product
        
        if(!oldLineItems.isEmpty())
            delete oldLineItems;
            
        //determine what products were selected
        //create warranty lines associated with the warranty Id
        
        List<Warranty_Line__c> newLineItems = new List<Warranty_Line__c>();
        for(WarrantyProduct wp : warrantyProductList){
            if(wp.IsSelected){
                Warranty_Line__c li = new Warranty_Line__c();
                li.Product__c = wp.CatalogProduct.Product2.Id;
                li.Warranty__c = warrantyId;
                li.Quantity__c = 1;
                
                newLineItems.add(li);
            }
        }
        
        if(newLineItems.size() > 0){
            Database.insert(newLineItems);
        }
        return new PageReference('/apex/CommunityWarrantyEdit?wid=' + warrantyId);
    }
    
    public class WarrantyProduct{
        public PricebookEntry CatalogProduct {get; set;}
        public Boolean IsSelected {get; set;}
        
        public WarrantyProduct(){
            CatalogProduct = null;
            IsSelected = false;
        }
    }

}