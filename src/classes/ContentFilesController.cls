public with sharing class ContentFilesController {

    public static Integer PAGE_RECORDS {
        get {
            return 500;
        }
    }

    public static String BASE_URL {
        get {
            return System.Url.getSalesforceBaseUrl().toExternalForm() +
                '/sfc/servlet.shepherd/version/download/';
        }
    }

    public List<ContentVersion> documents { get; private set; }

    public Integer numberOfRecords {
        get {
            return allDocuments.size();
        }
    }

    public Boolean hasPreviousPage {
        get {
            return currentPage > 1;
        }
    }

    public Boolean hasNextPage {
        get {
            return currentPage < Math.ceil((Decimal) numberOfRecords / PAGE_RECORDS);
        }
    }

    public String recordsDisplayed {
        get {
            Integer startIndex = 1 + (currentPage - 1) * PAGE_RECORDS;
            Integer endIndex = hasNextPage ? currentPage * PAGE_RECORDS : allDocuments.size();
            return startIndex + ' - ' + endIndex + ' of ' + allDocuments.size();
        }
    }

    private List<ContentVersion> allDocuments;
    private Integer currentPage;

    public ContentFilesController() {
        allDocuments = [
            SELECT Title, Document_Type__c, Sub_Type_Level_1__c, Sub_Type_Level_2__c,
                   Sub_Type_Level_3__c
            FROM ContentVersion
            ORDER BY Document_Type__c, Sub_Type_Level_1__c, Sub_Type_Level_2__c, Sub_Type_Level_3__c
        ];

        setCurrentPage(1);
    }

    public void previousPage() {
        if (hasPreviousPage) {
            setCurrentPage(currentPage - 1);
        }
    }

    public void nextPage() {
        if (hasNextPage) {
            setCurrentPage(currentPage + 1);
        }
    }

    private void setCurrentPage(Integer page) {
        currentPage = page;
        documents = new List<ContentVersion>();
        Integer startIndex = (page - 1) * PAGE_RECORDS;
        Integer endIndex = PAGE_RECORDS * page;
        for (Integer i = startIndex; i < allDocuments.size() && i < endIndex; i++) {
            documents.add(allDocuments.get(i));
        }
    }
}