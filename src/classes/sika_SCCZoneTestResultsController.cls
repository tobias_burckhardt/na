public with sharing class sika_SCCZoneTestResultsController {
	public Test_Result__c result {get; set;}
    public String theMixId {get; set;}
//  public String theRetURL {get; set;}
    public Boolean isNew {get; set;}

    public Mix__c theMix {get; set;}
    public string chartLabel {get; set;}
    public string mixName {get; set;}
    public Boolean displayPopUp {get; set;}
    public ID resultID {get; set;}
    public ID userID {get; set;}
    
    public sika_SCCZoneTestResultsController() {
        sika_SCCZoneCheckAuth.setAuthStatus(UserInfo.getProfileId());

        theMixId = ApexPages.currentPage().getParameters().get('mixID'); 
        resultID = ApexPages.currentPage().getParameters().get('id'); 
        userID = UserInfo.getUserId();  
        if(theMixId != null){
            theMix = [SELECT ID, 
                             System_of_Measure__c, 
                             Mix_Name__c, 
                             OwnerID 
                        FROM Mix__c 
                       WHERE ID =: theMixId];
        }
        else{
            // if there were no Mix Id passed to the page, the constructor would fail. This lets the constructor finish; then we'll use a test in the pageAction to redirect the user appropriately.
            theMix = new Mix__c();
        }

        if(resultID != null){
            result = [SELECT ID, 
                             Mix__c, 
                             Mix__r.ID, 
                             Mix__r.Mix_Name__c, 
                             Tester__c, 
                             Initial_Test_date__c, 
                             Test_Location__c, 
                             Mixing_Date__c, 
                             Initial_Set_Time__c, 
                             Final_Set_Time__c, 
                             Unit_Weight__c, 
                             Concrete_Temp__c, 
                             Ambient_Temp__c, 
                             Air_Content__c, 
                             Slump_Flow__c, 
                             VSI__c, 
                             T20__c, 
                             Static_segregation__c, 
                             Col_Segregation_Top__c, 
                             JRing_Test__c, 
                             Col_Segregation_Bottom__c, 
                             JRing_Size__c, 
                             Strength_Test_1_label__c, Strength_16hr_1__c, Strength_16hr_2__c, Strength_16hr_3__c, 
                             Strength_Test_2_label__c, Strength_1d_1__c, Strength_1d_2__c, Strength_1d_3__c, 
                             Strength_Test_3_label__c, Strength_3day_1__c, Strength_3day_2__c, Strength_3day_3__c, 
                             Strength_Test_4_label__c, Strength_7day_1__c, Strength_7day_2__c, Strength_7day_3__c, 
                             Strength_Test_5_label__c, Strength_28day_1__c, Strength_28day_2__c, Strength_28day_3__c, 
                             Strength_Test_6_label__c, Strength_56day_1__c, Strength_56day_2__c, Strength_56day_3__c, 
                             Strength_Test_7_label__c, Strength_90day_1__c, Strength_90day_2__c, Strength_90day_3__c, 
                             Strength_Test_8_label__c, Strength_open_1__c, Strength_open_2__c, Strength_open_3__c, 
                             Strength_Test_16h_avg__c, Strength_Test_1d_avg__c, Strength_Test_3d_avg__c, Strength_Test_7d_avg__c, 
                             Strength_Test_28d_avg__c, Strength_Test_56d_avg__c, Strength_Test_90d_avg__c, Strength_Test_open_avg__c 
                        FROM Test_Result__c 
                       WHERE ID =: resultID];
            isNew = false;
        }
        else{
            result = new Test_Result__c(Mix__c = theMixID);
            isNew = true;
        }

        if(theMix != null){
            mixName = String.valueOf(theMix.Mix_Name__c);
            if(theMix.System_of_Measure__c == 'Metric (SI)'){

                chartLabel = 'MPa';
            }
            else{
                chartLabel = 'psi';
            } 

        }

      displayPopUp = false;  

    }


    // check user auth status and confirm that a MixId was passed to the page
    public PageReference loadAction(){
        PageReference authCheck;
        PageReference idCheck;

        // because this controller runs "without sharing" we need to manually confirm that the user should have access. (A user whose session has timed-out, or an otherwise unauthenticated user who accessed this page directly, would be able to view the page otherwise, albeit without any data displayed.)
        authCheck = sika_SCCZoneCheckAuth.doAuthCheck();
        if(authCheck != null){
            return authCheck;
        }
        // be sure the page was passed all appropriate record IDs
        idCheck = checkId();
        if(idCheck != null){
            return idCheck;
        }
        return null;
    }

    private PageReference checkId(){
        if(theMixID == null){
            system.debug('********ERROR: No Mix record Id was passed to the page. Redirecting to the userHomePage.');
            PageReference pageRef = new PageReference('/SCCZone/apex/sika_SCCZoneUserHomePage');
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
    }
    

    public pageReference editPage(){
        pageReference p = Page.sika_SCCZoneTestNewEditPage;
        p.getParameters().put('id', result.Id);
        p.getParameters().put('mixID', theMixId);
        p.setRedirect(true);

        return p;
        
    }

    public PageReference doSave(){

            upsert result;
            
            PageReference goToTestDetail = Page.sika_SCCZoneTestDetailsPage;
            goToTestDetail.getParameters().put('id', result.Id);
            goToTestDetail.getParameters().put('mixID', theMixId);
            goToTestDetail.setRedirect(true);
            return goToTestDetail;

    }

    public pageReference returnMix(){

            pageReference p = new PageReference('/sika_SCCZoneMixDetailsPage');
            p.getParameters().put('mixID', theMixId);
            p.setRedirect(true);

            return p;

    }

    public pageReference doCancel(){
        pageReference p;
        if(isNew){
            p = new PageReference('/sika_SCCZoneMixDetailsPage');
            p.getParameters().put('mixID', theMixId);
        } else {
            p = Page.sika_SCCZoneTestDetailsPage;
            p.getParameters().put('id', result.Id);
            p.getParameters().put('mixID', theMixId);
        }
        p.setRedirect(true);

        return p;

    }
    
    // deleteTest() is called from the link(s) in the Test_Results related list on the Mix Details Page
    public String deleteThisTest {get; set;}
     
    public PageReference deleteTest(){
        pageReference p;
        
        list<Test_Result__c> theTest = [SELECT Id FROM Test_Result__c WHERE Id = :deleteThisTest Limit 1];
        try {
            delete(theTest[0]);

            p = new PageReference('/sika_SCCZoneMixDetailsPage');
            p.getParameters().put('mixID', theMixId); 
                
            p.setRedirect(true);
            return p;
            
        } catch (DmlException e) {
            System.debug('********ERROR: ' + e);
            Apexpages.addMessage(new Apexpages.message(ApexPages.Severity.Error,e.getMessage()));
            return null;
        }
        
    }


    // deleteThisTest() is called from the Delete button on the Test_Results detail page
    public PageReference deleteThisTest(){
        pageReference p;

        try {
        delete(result);
        
                    p = new PageReference('/sika_SCCZoneMixDetailsPage');
                    p.getParameters().put('mixID', theMixId);
                
                p.setRedirect(true);
                return p;
            
        } catch (DmlException e) {
        Apexpages.addMessage(new Apexpages.message(ApexPages.Severity.Error,e.getMessage()));
        return null;
        }
        
    }

    public void closePopup() {        
        displayPopup = false;    
    } 
        
    public void showPopup() {        
        displayPopup = true;    
    }

}