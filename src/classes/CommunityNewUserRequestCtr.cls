public class CommunityNewUserRequestCtr {
// Add/remove "company type" values here:
    list<String> companyTypeList = new list<String>{'Architect',
                                                 'Applicator',
                                               //'Board Producer',
                                               //'Cement Producer',
                                               //'Concrete Producer',
                                               //'Concrete Precast Producer',
                                               //'Consultant',
                                               'Distributor',
                                               //'Engineer',
                                               'General Contractor',
                                               'Owner',
                                               //'Sub Contractor',
                                               'Other'};
    
    public String caseType = 'SCC Zone Access Request';
    
    public String firstName {get; set;}
    public String lastName {get; set;}
    public String companyName {get; set;}
    public String companyType {get; set;}
    public String jobTitle {get; set;}
    public String emailAddress {get; set;} 
    public String phoneNo {get; set;}
    public String streetAddress {get; set;}
    public String city {get; set;}
    public String state {get; set;}
    public String postalCode {get; set;}
    public String country {get; set;}
    public String salesRep {get; set;}
//  public Boolean isStateRequired {get; set;}

    public Lead tempLead {get; set;}

    // constructor
    public CommunityNewUserRequestCtr() {

    }
  

    public list<SelectOption> companyTypes;
    
    public list<SelectOption> getCompanyTypes(){
        list<SelectOption> theSelects = new list<SelectOption>();

        theSelects.add(new SelectOption('', '--Company Type--'));
        for(String s : companyTypeList){
            theSelects.add(new SelectOption(s, s));
        }      
        return theSelects;
        
    }


    @testVisible private Database.SaveResult result;
    @testVisible private String subject;

    // if form validation passes, create a "Request Access" case
    public PageReference createCase() {
        PageReference thankyouPage = new PageReference('/apex/CommunityNewUserRequestThankYou');
        thankyouPage.setRedirect(false);

        if(validateForm()){
        RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType='case' AND Name='Warranty User Request' LIMIT 1];
            Case newCase = new Case();
            
            subject = 'RSB Warranty Community Access Request';
            subject += companyName == null ? '' : ' from ' + companyName;
            newCase.Subject = subject;
            newCase.Status = 'New';
            newCase.Due_Date__c = Datetime.now();
            newCase.Origin = 'RSB Warranty - Request Access';
            newCase.recordtypeid=rt.id;
            //newCase.Type = 'SCC Zone Access Request';
            //newCase.SCC_Zone_Case_Types__c = caseType;
            newCase.Priority = 'High';
            newCase.Customer_Email__c = emailAddress;
            newCase.SuppliedName = firstName + ' ' + lastName;
            newCase.SuppliedCompany = companyName;
            newCase.SuppliedPhone = phoneNo;
            newCase.SuppliedEmail = emailAddress;
            newCase.Web_Street__c = streetAddress;
            newCase.Web_City__c = city;
            newCase.Web_State__c = state;
            newCase.Web_Zip__c = postalCode;
            
            String body = 'A user has requested access to the RSB Warranty Community site. \n\n';
            body += 'Details:\n';
            body += firstName + ' ' + lastName + '\n';
            body += jobTitle + '\n';
            body += companyName == null ? '' : companyName + '\n';
            body += emailAddress + '\n';
            body += phoneNo == null ? '' : phoneNo + '\n';
            body += '\n';
            body += streetAddress + '\n';
            body += city;            body += state == null ? '' : ', ' + state;
            body += postalCode == null ? '' : ' ' + postalCode;
            body += '\n';
            body += country == null ? '' : country;
            body += '\n';
            body += '\n';
            body += salesRep == null ? '' : 'Sales Rep: ' + salesRep + '\n';

            newCase.Description = body;

            AssignmentRule ar = new AssignmentRule();
            ar = [SELECT Id FROM AssignmentRule WHERE SobjectType = 'Case' AND Active = true Limit 1];

            newCase.Do_not_send_auto_response__c = true; // prevents sending submitter the generic Service Cloud email that normally goes out on case submission. (It isn't relevant for SCC Zone requests.)

            Database.DMLOptions options = new Database.DMLOptions();
            options.EmailHeader.TriggerUserEmail = true;  // ensures that queue members receive an alert email when the case is created. Wouldn't otherwise happen since we're creating the case via Apex.
            options.assignmentRuleHeader.assignmentRuleId= AR.id;
            newCase.setOptions(options);
            
            result = Database.insert(newCase);
            System.debug(' ************ result ' + result);

            if(result.isSuccess()){
                thankyouPage.getParameters().put('success','true');
                thankyouPage.setRedirect(true);
            } else {
                thankyouPage.getParameters().put('success','false');
                System.debug('******** SAVE RESULT ERRORS:' + result.getErrors());
                thankyouPage.setRedirect(true);
            }
            return thankyouPage;
        }
        return null;
        
    }
    

    // form validation
    public Boolean hasErrors;
//    public Boolean stateRequired {get; set;}  // set by javascript when the form submit button is clicked

    public Boolean emailIsInvalid {get; private set;}
    public Boolean emailIsBlank {get; private set;}
    public Boolean firstnameError {get; private set;}
    public Boolean lastnameError {get; private set;}
    public Boolean companyNameError {get; private set;}
    public Boolean jobTitleError {get; private set;}
    public Boolean streetAddressError {get; private set;}
    public Boolean cityError {get; private set;}
    public Boolean stateError {get; private set;}
    public Boolean countryError {get; private set;}
    public Boolean postalCodeError {get; private set;}

    @testVisible private Boolean validateForm(){
        // initialize all error Booleans to false. (Ensures we don't reference any null objects, or get tricked into thinking "not false" equals "true"!)
        hasErrors = false;
        firstnameError = false;
        lastnameError = false;
        emailIsBlank = false;
        emailIsInvalid = false;
        companyNameError = false;
        jobTitleError = false;
        streetAddressError = false;
        cityError = false;
        stateError = false;
        postalCodeError = false;
        countryError = false;

        if(String.isBlank(firstName)){
            hasErrors = true;
            firstnameError = true;
        }
        if(String.isBlank(lastName)){
            hasErrors = true;
            lastnameError = true;
        } 
        if(String.isBlank(emailAddress)){
            hasErrors = true;
            emailIsBlank = true;
        } else {
            emailIsBlank = false;
            // we'll display a different message for an invalid email vs an empty email field
            if(WaranttyValidationHelper.validateEmail(emailAddress) == false){
                hasErrors = true;
                emailIsInvalid = true;
            }
        }
        if(String.isBlank(companyName)){
            hasErrors = true;
            companyNameError = true;
        }
        if(String.isBlank(jobTitle)){
            hasErrors = true;
            jobTitleError = true;
        }
        if(String.isBlank(streetAddress)){
            hasErrors = true;
            streetAddressError = true;
        }
        if(String.isBlank(city)){
            hasErrors = true;
            cityError = true;
        }
        if(String.isBlank(country)){
            hasErrors = true;
            countryError = true;
        }

        if(String.isBlank(state)){
            hasErrors = true;
            stateError = true;
        }

        if(String.isBlank(String.valueOf(postalCode))){
            hasErrors = true;
            postalCodeError = true;
        }

        // set (or un-set) the form validation error status and error messages
        if(hasErrors){
            system.debug('******** hasErrors = ' + hasErrors);
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please fill in all of the fields highlighted below.'));
//            if(stateError){
//                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'The state/province selection box is required for your country.'));

//            }
            if(emailIsInvalid == true){
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'A valid email address is required.'));
            } 
            return false;
        }
        return true;
    } 

}