@isTest
private class CaseCommentRestrictUpDelTriggerTest {

    private static final String MODIFIED_BODY = 'Modified Body';

    //Creates the specified number of case comments related to the given case id
    private static List<CaseComment> createCaseComment(Id caseId, Integer num) {
        List<CaseComment> comments = new List<CaseComment>();
        for (Integer i = 0; i < num; i++) {
	        CaseComment cc = new CaseComment(CommentBody='Test Body ' + i, ParentId=caseId);
            comments.add(cc);
        }
        insert comments;
        return comments;
    }

    @testSetup
    static void setUpTests() {
        RestrictedCaseTestHelper.createCustomSettings();
    }

    @isTest
    static void testUpdateDeleteUnrestricted() {
        //Trying to update/delete a comment on a case with an unrestricted record type should succeed
        Case testCase = RestrictedCaseTestHelper.createTestCase(false);
        List<CaseComment> testComments = createCaseComment(testCase.Id, 1);

        Test.startTest();
        testComments[0].CommentBody = MODIFIED_BODY;
        Database.SaveResult updateResult = Database.update(testComments[0], false);
        System.assert(updateResult.isSuccess());
        
        Database.DeleteResult deleteResult = Database.delete(testComments[0], false);
        System.assert(deleteResult.isSuccess());
        Test.stopTest();
    }
    
    @isTest
    static void testUpdateCaseCommentRestricted() {
        //Trying to update a comment on a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<CaseComment> testComments = createCaseComment(testCase.Id, 1);

        Test.startTest();
        testComments[0].CommentBody = MODIFIED_BODY;
        Database.SaveResult updateResult = Database.update(testComments[0], false);
        System.assert(!updateResult.isSuccess());
        System.assertEquals(1, updateResult.getErrors().size());
        System.assertEquals(System.Label.Restricted_Object_Update, updateResult.getErrors()[0].getMessage());
        Test.stopTest();
    }
    
    @isTest
    static void testDeleteCaseCommentRestricted() {
        //Trying to delete a comment on a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<CaseComment> testComments = createCaseComment(testCase.Id, 1);

        Test.startTest();
        Database.DeleteResult deleteResult = Database.delete(testComments[0], false);
        System.assert(!deleteResult.isSuccess());
        System.assertEquals(1, deleteResult.getErrors().size());
        System.assertEquals(System.Label.Restricted_Object_Delete, deleteResult.getErrors()[0].getMessage());
        Test.stopTest();
    }
    
    @isTest
    static void testBulkUpdateCaseCommentsRestricted() {
        //Trying to update comments on a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<CaseComment> testComments = createCaseComment(testCase.Id, 200);

        Test.startTest();
        //Update all comments
        for (CaseComment comm : testComments) {
            comm.CommentBody = MODIFIED_BODY;
        }

        //Try to save all comments
        List<Database.SaveResult> results = Database.update(testComments, false);
        for (Database.SaveResult res : results) {
            System.assert(!res.isSuccess());
            System.assertEquals(1, res.getErrors().size());
            System.assertEquals(System.Label.Restricted_Object_Update, res.getErrors()[0].getMessage());
        }
        Test.stopTest();
    }
    
    @isTest
    static void testBulkDeleteCaseCommentsRestricted() {
        //Trying to delete comments on a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<CaseComment> testComments = createCaseComment(testCase.Id, 200);

        Test.startTest();
        //Try to delete all comments
        List<Database.DeleteResult> results = Database.delete(testComments, false);
        for (Database.DeleteResult res : results) {
            System.assert(!res.isSuccess());
            System.assertEquals(1, res.getErrors().size());
            System.assertEquals(System.Label.Restricted_Object_Delete, res.getErrors()[0].getMessage());
        }
        Test.stopTest();
    }
}