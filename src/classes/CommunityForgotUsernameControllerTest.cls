@IsTest
private class CommunityForgotUsernameControllerTest {

    @TestSetup
    static void setup() {
        Id warrantyProfileId = [SELECT Id FROM Profile WHERE Name = 'Warranty Customer Community Login' LIMIT 1].Id;
        UserRole aRole = (UserRole) SObjectFactory.create(UserRole.SObjectType, new Map<SObjectField, Object> {
                UserRole.Name => 'TestRole'
        });
        User portalOwner = (User) SObjectFactory.create(User.SObjectType, new Map<SObjectField, Object> {
                User.UserRoleId => aRole.Id
        });

        Contact c;
        System.runAs(portalOwner) {
            Account acc = (Account) SObjectFactory.create(Account.SObjectType);
            c = (Contact) SObjectFactory.create(Contact.SObjectType, new Map<SObjectField, Object> {
                    Contact.AccountId => acc.Id
            });
        }

        SObjectFactory.create(User.SObjectType, new Map<SObjectField, Object> {
                User.Email => 'skumar@astreait.com', User.ProfileId => warrantyProfileId, User.ContactId => c.Id
        });
    }

    @IsTest
    static void testCommunityForgotUserName() {
        CommunityForgotUsernameController ctr = new CommunityForgotUsernameController();
        ctr.emailAddress='skumar@astreait.com';
        ctr.success='true';
        ctr.caseEmail='skumar@astreait.com';
        Test.setCurrentPageReference(new PageReference('Page.CommunityForgotUsernameController'));
        System.currentPageReference().getParameters().put('success', 'true');

        ctr.submitRequest();
        ctr.closePopup();
        ctr.showPopup();
        ctr.submitCase();
    }
}