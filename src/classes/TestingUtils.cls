public with sharing class TestingUtils {

    public static List<Case> createCases(Id accId, Integer size, Id rTypeId, Boolean doInsert) {
        List<Case> allCases = new List<Case>();

        for (Integer i = 0; i < size; i++) {
            allCases.add(new Case(
                AccountId = accId,
                RecordTypeId = rTypeId,
                Project_Building_Name__c = 'Project Building Name',
                Category__c = 'Test Category'
            ));
        }

        if (doInsert) {
            insert allCases;
        }

        return allCases;
    }
    
    public static List<Task> createTasks(Id OwnerId, String Subject,String Status,String Priority, Id whatId,Integer size, Boolean doInsert) {
        List<Task> allTasks = new List<Task>();

        for (Integer i = 0; i < size; i++) {
            allTasks.add(new Task(
                OwnerId = OwnerId,
                Subject = Subject,
                Status = Status,
                Priority = Priority,
                whatId = whatId
                ));
        }

        if (doInsert) {
            insert allTasks;
        }

        return allTasks;
    }
     public static List<Opportunity> createOpportunities( Integer numOpportunities, Id accountId, Boolean doInsert ) {
        List<Opportunity> testOpportunities = new List<Opportunity>();

        for ( Integer index = 0; index < numOpportunities; index++ ) {
            Opportunity newOpportunity = new Opportunity();
            newOpportunity.AccountId = accountId;
            newOpportunity.Name = 'Test Opportunity ' + index;
            newOpportunity.CloseDate = Date.today();
            newOpportunity.Target_Market__c = 'Roofing';
            newOpportunity.Region__c = 'East';
            newOpportunity.StageName = 'Quote';
            newOpportunity.State__c = 'FL';
            
            testOpportunities.add( newOpportunity );
        }

        if ( doInsert )
            insert testOpportunities;

        return testOpportunities;
    }
    public static List<Contact> createContacts(String acctID, Integer count, Boolean doInsert) {
        List<Contact> testContacts = new List<Contact>();

        for (Integer i = 0; i < count; i++)
            testContacts.add(new Contact(LastName = 'TestContact' + i, AccountId = acctID ));

        if (doInsert && testContacts.size() > 0) insert testContacts;

        return testContacts;
    }
    public static Contact createContact(String lName, Boolean doInsert) {
        Contact testContact = new Contact(LastName = lName);
        if (doInsert) insert testContact;

        return testContact;
    }

    public static List<Account> createAccounts(String acctName, Integer size) {
        List<Account>allAccounts = new List<Account>();

        for (Integer i = 0; i < size; i++) {
            allAccounts.add(new Account(Name = acctName + i));
        }

        return allAccounts;
    }

    public static List<Account> createAccounts(String acctName, Integer size, Boolean doInsert) {
        List<Account>allAccounts = new List<Account>();

        for (Integer i = 0; i < size; i++) {
            allAccounts.add(new Account(Name = acctName + i));
        }

        if (doInsert) {
            insert allAccounts;
        }

        return allAccounts;
    }
    
    public static List<Apttus_Config2__PriceList__c> createPriceList(String priceListName, Integer size, Boolean doInsert) {
        List<Apttus_Config2__PriceList__c>allPriceList = new List<Apttus_Config2__PriceList__c>();

        for (Integer i = 0; i < size; i++) {
            allPriceList.add(new Apttus_Config2__PriceList__c(Name = priceListName + i));
        }

        if (doInsert) {
            insert allPriceList;
        }

        return allPriceList;
    }
    public static List<Apttus_Proposal__Proposal__c> createProposal(String proposalName, Integer size) {
        List<Apttus_Proposal__Proposal__c> allProposals = new List<Apttus_Proposal__Proposal__c>();

        for (Integer i = 0; i < size; i++) {
            allProposals.add(new Apttus_Proposal__Proposal__c(Apttus_Proposal__Proposal_Name__c = proposalName + i));
        }

        return allProposals;
    }
    public static Applicator_Quote__c createAppQuote(Account acc, Contact con, Opportunity opp,Apttus_Proposal__Proposal__c proposal) {
        Applicator_Quote__c appQuoteInserted = new Applicator_Quote__c();

        appQuoteInserted.Contact__c = con.id;
        appQuoteInserted.Account__c =acc.id;
        appQuoteInserted.Opportunity__c = opp.id;
        appQuoteInserted.Quote_Apttus__c= proposal.id;
        INSERT appQuoteInserted;
        return appQuoteInserted;
    }
    public static Attachment insertAttachment(String attachName) {
        Attachment attach = new Attachment();

        attach.Name=attachName;

        return attach;
    }

    

    
    public static List<Open_Order__c> createOpenOrders(List<String> orderNumbers) {
        List<Open_Order__c> allOrders = new List<Open_Order__c>();

        for (String orderNumber : orderNumbers) {
            allOrders.add(new Open_Order__c(Name = orderNumber, Unique_ID__c = orderNumber,
                                            Order_Number_Key__c = orderNumber));
        }

        return allOrders;
    }

    public static List<Invoice__c> createInvoices(List<String> orderNumbers) {
        List<Invoice__c> invoices = new List<Invoice__c>();

        for (String orderNumber : orderNumbers) {
            invoices.add(new Invoice__c(Name = orderNumber, SAPInvoiceID__c = orderNumber));
        }

        return invoices;
    }

    public static Contact createContact( String firstName, String lastName, Boolean doInsert ) {
        Contact contact = new Contact();
        contact.FirstName = firstName;
        contact.LastName = lastName;

        if ( doInsert )
            insert contact;

        return contact;
    }

    public static List<Project__c> createProjects(Integer num, Boolean doInsert) {
        List<Project__c> projects = new List<Project__c>();
        for (Integer i = 0; i < num; i++) {
            Project__c wl1 = new Project__c();
            projects.add(wl1);
        }
        if (doInsert) {
            insert projects;
        }
        return projects;
    }

    public static List<Warranty__c> createWarranties(Integer num, Project__c p,  Boolean doInsert) {
        List<Warranty__c> warranties = new List<Warranty__c>();
        for (Integer i = 0; i < num; i++) {
            Warranty__c wl1 = new Warranty__c(Project__c = p.Id);
            warranties.add(wl1);
        }
        if (doInsert) {
            insert warranties;
        }
        return warranties;
    }

    public static List<Warranty_Line__c> createWarrantyLines(Integer num, Warranty__c w, Integer quantity, Boolean doInsert) {
        List<Warranty_Line__c> warrantyLines = new List<Warranty_Line__c>();
        for (Integer i = 0; i < num; i++) {
            Warranty_Line__c wl1 = new Warranty_Line__c(Warranty__c = w.Id, Quantity__c = quantity);
            warrantyLines.add(wl1);
        }
        if (doInsert) {
            insert warrantyLines;
        }
        return warrantyLines;
    }

    public static User createTestUser(String userName, String profileName) {
        Id profileId;
        try {
            profileId = [SELECT Id FROM Profile WHERE Name = :profileName LIMIT 1].Id;
        } catch (Exception ex) {
            System.assert( false, profileName + ' profile does not exist in the system');
        }

        User testUser = new User();
        testUser.LastName = 'test ' + userName;
        testUser.Alias = (userName.length() > 8 ) ?  userName.substring(0 , 8) : userName;
        testUser.Email = userName + '@test.com';
        testUser.Username = userName + '@test.com';
        testUser.ProfileId = profileId;
        testUser.CommunityNickname = 'a' + userName;
        testUser.TimeZoneSidKey = 'America/New_York';
        testUser.LocaleSidKey = 'en_US';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.LanguageLocaleKey = 'en_US';
        testUser.IsActive = true;

        return testUser;
    }

    public static List<User> createMultipleTestUsers(Integer size, String userName, String profileName, Boolean doInsert) {
        List<User> testUsers = new List<User>();
        for (Integer i = 0; i < size; i++) {
            testUsers.add(createTestUser(userName + i, profileName));
        }
        if (doInsert) {
            insert testUsers;
        }
        return testUsers;
    }

    public static List<ContentVersion> createContentVersions(Integer quantity, String title,
            Boolean doInsert) {
        List<ContentVersion> documents = new List<ContentVersion>();
        for (Integer i = 0; i < quantity; i++) {
            ContentVersion document = new ContentVersion(
                Title = title + ' + ' + i,
                ContentUrl = './'
            );
            documents.add(document);
        }

        if (doInsert) {
            insert documents;
        }

        return documents;
    }

    public static Request_for_Project_Approval__c createTestRequestForPA(Date testDate, Boolean doInsert) {
        return createTestRequestsForPA(1, testDate, doInsert)[0];
    }

    public static List<Request_for_Project_Approval__c> createTestRequestsForPA(Integer size, Date testDate, Boolean doInsert) {
        List<Request_for_Project_Approval__c> rpas = new List<Request_for_Project_Approval__c>();
        for (Integer i = 0; i < size; i++) {
            Request_for_Project_Approval__c newRpa = new Request_for_Project_Approval__c();
            newRpa.Date__c = testDate;
            newRpa.RPA_Name__c = 'Some RPA Name' + i;
            rpas.add(newRpa);
        }
        if (doInsert) {
            insert rpas;
        }
        return rpas;
    }

    public static Opportunity createOpportunity(String name, String stageName, Date closeDate,
            Boolean doInsert) {
        Opportunity oppty = new Opportunity(
            StageName = stageName,
            Name = name,
            CloseDate = closeDate
        );
        return (Opportunity) insertRecords(new List<Opportunity> {oppty}, doInsert).get(0);
    }

    public static Applicator_Quote__c createApplicatorQuote(Boolean doInsert) {
        Applicator_Quote__c quote = new Applicator_Quote__c();
        return (Applicator_Quote__c) insertRecords(new List<Applicator_Quote__c> {quote}, doInsert)
               .get(0);
    }

    public static Material__c createMaterial(Boolean doInsert) {
        Material__c material = new Material__c();

        if (doInsert) {
            insert material;
        }

        return material;
    }

    public static Apttus_Proposal__Proposal__c createApptusProposal(Boolean doInsert) {
        Apttus_Proposal__Proposal__c proposal = new Apttus_Proposal__Proposal__c();
        return (Apttus_Proposal__Proposal__c) insertRecords(
                   new List<Apttus_Proposal__Proposal__c> {proposal}, doInsert).get(0);
    }

    private static List<SObject> insertRecords(List<SObject> records, Boolean doInsert) {
        if (doInsert) {
            insert records;
        }

        return records;
    }

    public static Valid_Library__c createValidLibrary(String name, Boolean doInsert) {
        Valid_Library__c libName = new Valid_Library__c(Name = name);

        if (doInsert)
            insert libName;

        return libName;
    }

    public static Mix__c createMix(Integer cementCompStrength, String name, Decimal targetAirPercent,
            Decimal targetSCMRatio, Decimal targetWaterCementRatio, Boolean doInsert) {
        Mix__c mix = new Mix__c(
            Cement_Comp_Strength__c = cementCompStrength,
            Mix_Name__c = name,
            Target_Air_Percent__c = targetAirPercent,
            Target_SCM_Ratio__c = targetSCMRatio,
            Target_Water_Cement_Ratio__c = targetWaterCementRatio
        );

        if (doInsert) {
            insert mix;
        }

        return mix;
    }

    public static Mix_Material__c createMixMaterial(Mix__c mix, Material__c material, Boolean doInsert) {
        Mix_Material__c mixMaterial = new Mix_Material__c(
            Mix__c = mix.Id,
            Material__c = material.Id
        );

        if (doInsert) {
            insert mixMaterial;
        }

        return mixMaterial;
    }

    public static Employee__c createEmployee(String legalFirstName, String legalLastName) {
        Employee__c employee = new Employee__c(
            Legal_First_Name__c = legalFirstName,
            Legal_Last_Name__c = legalLastName
        );

        return employee;
    }

    public class SobjectBuilderGenerator implements UnitTest.DataGenerator {
        SobjectType type;
        public SobjectBuilderGenerator(SobjectType type) {
            this.type = type;
        }
        public List<SObject> generateData(Integer howMany) {
            return SobjectFactory.build(howMany, this.type);
        }
    }

    public static UnitTest.DataGenerator generatorOf(SobjectType type) {
        return new SobjectBuilderGenerator(type);
    }
}