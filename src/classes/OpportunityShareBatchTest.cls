@IsTest
private class OpportunityShareBatchTest {

    private static final Integer RECORD_COUNT_CONTACT = 5;
    private static final String PROFILE_NAME = 'PTS Partner Community';
    private static final String BAD_PROFILE_NAME = 'SCC Zone Community User';
    private static final String USERNAMEPREFIX = 'TestGtt';

    @TestSetup
    static void setupData() {
        Account acct = (Account) SObjectFactory.create(Account.SObjectType);
        List<Contact> contactList = (List<Contact>) SObjectFactory.create(RECORD_COUNT_CONTACT, Contact.SObjectType, new Map<SObjectField, Object> {
                Contact.AccountId => acct.Id
        });
        List<User> userList = new List<User>();
        for (Integer i = 0; i < RECORD_COUNT_CONTACT - 1; i++) {
            userList.add((User)SObjectFactory.build(User.SObjectType, new Map<SObjectField, Object> {
                    User.Username => USERNAMEPREFIX + i + '@test.com', User.ContactId => contactList.get(i).Id,
                    User.ProfileId => [SELECT Id FROM Profile WHERE Name = :PROFILE_NAME LIMIT 1].Id
            }));
        }
        // create bad test user
        userList.add((User)SObjectFactory.build(User.SObjectType, new Map<SObjectField, Object> {
                User.Username => USERNAMEPREFIX + (RECORD_COUNT_CONTACT - 1) + '@test.com', User.ContactId => contactList.get(RECORD_COUNT_CONTACT - 1).Id,
                User.ProfileId => [SELECT Id FROM Profile WHERE Name = :BAD_PROFILE_NAME LIMIT 1].Id
        }));
        insert userList;
        Opportunity opp = (Opportunity) SObjectFactory.create(Opportunity.SObjectType, new Map<SObjectField, Object> {
                Opportunity.AccountId => acct.Id
        });
        SObjectFactory.create(Applicator_Quote__c.SObjectType, new Map<SObjectField, Object>{
                Applicator_Quote__c.Contact__c => contactList.get(0).Id, Applicator_Quote__c.Account__c => acct.Id,
                Applicator_Quote__c.Opportunity__c => opp.Id
        });
    }

    @IsTest
    static void executeBatchTest() {
        Test.startTest();
        Database.executeBatch(new OpportunityShareBatch());
        Test.stopTest();

        Integer recordCount = [SELECT COUNT() FROM OpportunityShare WHERE RowCause = 'Manual'];
        System.assertEquals(RECORD_COUNT_CONTACT - 1, recordCount,
                (RECORD_COUNT_CONTACT - 1) + ' No of Opportunity Share Object should be created');
    }
}