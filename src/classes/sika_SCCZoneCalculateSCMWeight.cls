public without sharing class sika_SCCZoneCalculateSCMWeight {

	public static map<String, String> getSCMWeightAndUnit(list<Mix_Material__c> theMMs) {	
		// grab the cements & SCMs, if any

		// if there is a cement and there is an SCM, ...

		// create a map, Mix Id to targetSCMRatio, for each Mix referenced by an MM in theMMs

		// ...

		return null;
		//return doCalc();
	}

	public static map<String, String> getSCMWeightAndUnit(Decimal targetSCMRatio, list<Mix_Material__c> theCementAndSCM) {
		

		return null;
		//return doCalc();
	}

	// called from the MixMaterialsController2 class. No need to update the unit on optimize, since the cement's unit isn't being changed.
	public static map<String, String> getSCMWeightAndUnit(Decimal cementQuantity, String cementUnit, Decimal targetSCMRatio){

		return doCalc(cementQuantity, cementUnit, targetSCMRatio);
		
	}

	// called from the OptimizeMix class. No need to update the unit on optimize, since the cement's unit isn't being changed.
	public static map<String, String> getSCMWeightAndUnit(Decimal cementQuantity, Decimal targetSCMRatio){

		return doCalc(cementQuantity, null, targetSCMRatio);

	}

	// called from the SFDC UI new/edit Mix Material visualforce page controller extension.
	public static map<String, String> getSCMWeightAndUnit(Decimal cementQuantity, String cementUnit, Id theMixId){
		Decimal targetSCMRatio = [SELECT Id, Target_SCM_Ratio__c FROM Mix__c WHERE Id = :theMixId].Target_SCM_Ratio__c;

		return doCalc(cementQuantity, cementUnit, targetSCMRatio);
	}

// -------------------------------

	private static map<String, String> doCalc(Decimal cementQuantity, String cementUnit, Decimal targetSCMRatio){
		Decimal SCMWeight = cementQuantity * (-1 * (targetSCMRatio / (targetSCMRatio -1)));
		String SCMUnit = cementUnit != null ? cementUnit : 'kgs';

		return new map<String, String>{'quantity' => String.valueOf(SCMWeight), 
										 'unit' => SCMUnit}; 
	} 
}