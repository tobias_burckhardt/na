public with sharing class CommunityAccountDetailController {
        
        public Account acct {get; set;}
        
        public CommunityAccountDetailController(){
                //determine if url parameters were passed in
        Map<string, string> pageParams = ApexPages.currentPage().getParameters();
        
        if(pageParams.size() > 0){
                string aid = pageParams.get('aid');
                if(aid != null){
                        //string projectFields = Utilities.GetAllObjectFields('Project__c');
                        string acctFields = 'Name,Type,BillingCity,BillingState,BillingStreet,BillingPostalCode,Phone,Website,BillingCountry';
                string queryFormat = 'select {0} from {1} where {2}';
                string acctQuery = String.format(queryFormat, new List<string>{acctFields, 'Account', 'Id = :aid'});
                acct = Database.query(acctQuery);
                
                if(acct == null){
                        acct = new Account();
                }
                }
        }
        }

}