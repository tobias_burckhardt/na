public class ApplicatorQuoteService {
    
    public static void IndicateSourcePlantInsert(Map<Id, Applicator_Quote__c> appQs) {
            IndicateSourcePlant(appqs);
    }
    
    public static void IndicateSourcePlantUpdate(Map<Id, Applicator_Quote__c> oldAppQ, 
                                                 Map<Id, Applicator_Quote__c> newAppQ) {
         
        Map<Id, Applicator_Quote__c> copyAppQ = new Map<Id, Applicator_Quote__c>(); 
                                                     
        for( Id appqId : newAppQ.keySet() ) {
            if( oldAppQ.get(appqId).Warehouse__c != newAppQ.get(appqId).Warehouse__c ) {
                // remove records with not changed warehouse fields
                copyAppQ.put(appqId, newAppQ.get(appqId));         
            }
        }
                    
        if(copyAppQ != null){
          IndicateSourcePlant(copyAppQ);                                                      
        }
    }
    
    private static void IndicateSourcePlant(Map<Id, Applicator_Quote__c> appQs) {

        Map<String, List<Applicator_Quote__c>> stateAppMap = new Map<String, List<Applicator_Quote__c>>();
        Set<String> states  = new Set<String>();
        Set<String> zips    = new Set<String>();    
        
        for (Applicator_Quote__c aq : [SELECT Id, FOB_Zip__c, Warehouse__c, Opportunity__r.State__c FROM Applicator_Quote__c WHERE Id IN :appQs.keySet()]) {
            if(stateAppMap.containsKey(aq.Opportunity__r.State__c)){
                stateAppMap.get(aq.Opportunity__r.State__c).add(aq);
            }
            else{
                stateAppMap.put(aq.Opportunity__r.State__c, new List<Applicator_Quote__c>{aq});
            }

            states.add(aq.Opportunity__r.State__c);
            zips.add(aq.FOB_Zip__c);   
        }

        //Utility map of freight charts by state then zip, Map<Destination_State__c, Map<FOB_Zip__c, Freight_Charge__c>>
        //this is important to construct this way in order to allow for the scenario where there are not freight charge values
        Map<String, Map<String, Freight_Charge__c>> fcMap = new Map<String, Map<String, Freight_Charge__c>>();
        for (Freight_Charge__c fc : [SELECT Price__c, Transit_Days__c, FOB_Zip__c, Destination_State__c FROM Freight_Charge__c WHERE FOB_Zip__c IN :zips AND Destination_State__c IN :states]) {
            Map<String, Freight_Charge__c> temp = new Map<String, Freight_Charge__c>();
            temp.put(fc.FOB_Zip__c, fc);

            fcMap.put(fc.Destination_State__c, temp);
        }


        //construct data for update, as well as populate values
        //we use the freight charge map to get values instead of loop through values.  this accounts for the scenario
        //where we are switchign from one ware house to another, and there may or may NOT be an associated FC.
        //if we just loop then the old values persist.
        List<Applicator_Quote__c> aqToUpdate = new List<Applicator_Quote__c>();

        for(String str : stateAppMap.keySet()){
            for (Applicator_Quote__c apq : stateAppMap.get(str)) {

                if(fcMap.containsKey(apq.Opportunity__r.State__c)){
                    if(fcMap.get(apq.Opportunity__r.State__c).containsKey(apq.FOB_Zip__c)){
                        apq.Estimated_Freight__c        = fcMap.get(apq.Opportunity__r.State__c).get(apq.FOB_Zip__c).Price__c;
                        apq.No_of_days_for_transit__c   = fcMap.get(apq.Opportunity__r.State__c).get(apq.FOB_Zip__c).Transit_Days__c;
                        aqToUpdate.add(apq);
                    }
                    else{
                        //if the opportunity state does exists but zip code does not exist make values null
                        apq.Estimated_Freight__c        = null;
                        apq.No_of_days_for_transit__c   = null;
                        aqToUpdate.add(apq);
                    }
                }
                else{
                    apq.Estimated_Freight__c        = null;
                    apq.No_of_days_for_transit__c   = null;
                    aqToUpdate.add(apq);
                }
            }
        }

        update aqToUpdate;
    }
}

/*
private static void IndicateSourcePlant(Map<Id, Applicator_Quote__c> appQs) {
    // String state, List of Applicator_Quote__c
    Map<String, List<Applicator_Quote__c>> stateApps = new Map<String, List<Applicator_Quote__c>>();
    
    // get list of opportunities with child quote/proposals
    for (List<Opportunity> opps : [select id, State__c, (select id, FOB_Zip__c, Warehouse__c from Applicator_Quotes__r where id IN :appQs.keySet() and FOB_Zip__c != NULL) from Opportunity where State__c != NULL]) {
        for (Opportunity opp : opps) {
            if (opp.Applicator_Quotes__r.size() > 0) {
                // get existing list by state and append new records
                List<Applicator_Quote__c> quotes = stateApps.get(opp.State__c);
                if (quotes != NULL) {
                    quotes.addAll(opp.Applicator_Quotes__r);
                    stateApps.put(opp.State__c, quotes);
                } else {
                    stateApps.put(opp.State__c, opp.Applicator_Quotes__r);
                }
            }
        }
    }        
    
    // get set for State__c
    Set<String> states = new Set<String>(stateApps.keySet());
    // get set for FOB_Zip__c
    Set<String> zips = new Set<String>();        
    for (List<Applicator_Quote__c> apps : stateApps.values()) {
        for (Applicator_Quote__c app : apps) {
            zips.add(app.FOB_Zip__c);   
        }
    }               
    
    // query for every Freight charge by State__c and FOB_Zip__c
    for (Freight_Charge__c fc :  [ SELECT Price__c, Transit_Days__c, FOB_Zip__c, Destination_State__c FROM Freight_Charge__c  WHERE FOB_Zip__c IN :zips AND Destination_State__c IN :states]) {
        List<Applicator_Quote__c> apqsByState = stateApps.get(fc.Destination_State__c);
        // loop over quotes from freight destination and find the one with same FOB_Zip__c
        for (Applicator_Quote__c apq : apqsByState) {
            if (apq.FOB_Zip__c == fc.FOB_Zip__c) {
                apq.Estimated_Freight__c       = fc.Price__c;
                apq.No_of_days_for_transit__c   = fc.Transit_Days__c;
            }

        }
    }
    

    aqToUpdate.addAll(lists2List(stateApps));

    update aqToUpdate;
}
*/

/*
private static List<Applicator_Quote__c> lists2List(Map<String, List<Applicator_Quote__c>> mapa) {
    
    List<Applicator_Quote__c> returnList = new List<Applicator_Quote__c>();
    
    for (String key : mapa.keySet()) {
        returnList.addAll(mapa.get(key));
    }
    
    return returnList;
}
*/