@isTest
public class PricingTestDataFactory {
    public static List<Product2> createProducts(){
        List<String> productIds = new List<String>();
        List<Product2> listOfNewProductsToBeInserted = new List<Product2>();
        for(Integer i=0;i<2;i++){
            Product2 productObj = new Product2(Name = 'Sika Navixel Product test '+DateTime.now(), family = 'Sika-Navixel Family',Standard_Cost1__c=10,IsActive=true);
            productObj.ProductCode = '1234'+i;
            listOfNewProductsToBeInserted.add(productObj);
        }
        insert listOfNewProductsToBeInserted;
        return listOfNewProductsToBeInserted;
    }
    public static Boolean addProductsToStandardPriceBook(List<String> productIds){
        Boolean areProductsAddedSuccessfully = false;
        Id pricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        for(String productId:productIds){
            PricebookEntry pricebookEntryObj = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = productId,UnitPrice = 10000, IsActive = true);
            pricebookEntries.add(pricebookEntryObj);
        }
        try{
            Database.insert(pricebookEntries,true);
        }catch(Exception e){
            System.debug('Exception caught : '+e);
        }
        
        Integer insertedPricebookEntriesCount = [Select count() from PricebookEntry where Product2Id IN:productIds];
        //System.debug('insertedPricebookEntriesCount--'+insertedPricebookEntriesCount);
        if(insertedPricebookEntriesCount > 0)
            areProductsAddedSuccessfully = true;
        return areProductsAddedSuccessfully;
    }
    public static Id createCustomPricebook(){
         Id pricebookRecordTypeId = Schema.SObjectType.Pricebook2.getRecordTypeInfosByDeveloperName().get('Price_books_for_Pricing_App').getRecordTypeId();
         Pricebook2 customPriceBook = new Pricebook2(Name='Navixel Custom Pricebook '+DateTime.now(), isActive=true,isSAPRelated__c=true,Validity_Start_Date__c=System.today(),Validity_End_Date__c=System.today()+10);
         customPriceBook.RecordTypeId = pricebookRecordTypeId;
         insert customPriceBook;
        
        return customPriceBook.Id;
    }
    public static Id createCPBWithTargetMarket(String targetMarketId){
         Id pricebookRecordTypeId = Schema.SObjectType.Pricebook2.getRecordTypeInfosByDeveloperName().get('Price_books_for_Pricing_App').getRecordTypeId();
         Pricebook2 customPriceBook = new Pricebook2(Name='Navixel Custom Pricebook TM '+DateTime.now(), isActive=true,isSAPRelated__c=true,Validity_Start_Date__c=System.today(),Validity_End_Date__c=System.today()+10);
         customPriceBook.Target_Market__c = targetMarketId;
         customPriceBook.RecordTypeId = pricebookRecordTypeId;
         insert customPriceBook;
        
        return customPriceBook.Id;
    }
    public static List<PricebookEntry> addProductsToCustomPriceBook(List<String> productIds,String customPricebookId){
        Boolean areProductsAddedSuccessfully = false;
        //System.debug('productIds-1-'+productIds);System.debug('customPricebookId-1-'+customPricebookId);
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        for(String productId:productIds){
            PricebookEntry pricebookEntryObj = new PricebookEntry(Pricebook2Id = customPricebookId, Product2Id = productId,UnitPrice = 10000, IsActive = true,Start_Date__c=System.today(),End_Date__c=System.today()+3);
            //System.debug('pricebookEntryObj-1-'+pricebookEntryObj);
            pricebookEntries.add(pricebookEntryObj);
        }
        //System.debug('pricebookEntries-1-'+pricebookEntries);
        insert pricebookEntries;
        
        return pricebookEntries;
    }
    public static Id createAccount(){
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('SoldTo_Account').getRecordTypeId();
        Account objAccount = new Account(Name = 'Test Navixel', BillingCity = 'Test City', BillingState = 'Test State', 
                                                                    BillingStreet = 'Test Street', BillingPostalCode = '12345', 
                                                                    BillingCountry = 'Test Country', Phone = '123456');
       
        objAccount.RecordTypeId = accRecordTypeId;
        insert objAccount; 
       
        return objAccount.Id;
    }
    public static Id createPrimaryContactForAccount(String accountId){
        Id contactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Sika_Pricing').getRecordTypeId();
        Contact contactObj = new Contact();
        contactObj.AccountId = accountId;
        contactObj.Active__c = true;
        contactObj.Email = 'contactNavixel@contact.com';
        contactObj.FirstName = 'contact';
        contactObj.LastName = 'Navixel';
        contactObj.Language__c = 'English';
        contactObj.RecordTypeId = contactRecordTypeId;
        insert contactObj;
        
        return contactObj.Id;
    }
    public static Id createAccountPriceBookEntry(Id accountId,String contactId,String priceBookId){
        Account_Pricebook_Entry__c accountPricebookEntryObj = new Account_Pricebook_Entry__c();
        if(String.isNotBlank(contactId))
        	accountPricebookEntryObj.Customer_Account_Contact__c = contactId;
        accountPricebookEntryObj.Price_Book__c = priceBookId;
        accountPricebookEntryObj.customer_account__c = accountId;
        insert accountPricebookEntryObj;
        
        return accountPricebookEntryObj.Id;
    }
    public static Id createTargetMarket(String accountId){
        Target_Market__c targetMarket = new Target_Market__c(Name='Concrete',Country_Code__c='6101',ISO_Country__c='CA',Active__c=true,Business_Manager__c=PricingTestDataFactory.createEmployee('Test_2'),Key_Contact__c=PricingTestDataFactory.createEmployee('Test_1'));
        insert targetMarket;
        
        return targetMarket.Id;
    }
    public static Id createEmployee(String empName){
        Employee__c employee = new Employee__c();
        employee.Legal_First_Name__c = empName;
        employee.Legal_Last_Name__c = 'Employee';
        employee.Sika_Email__c = empName+'@employee.com';
        employee.Login_Method__c = 'PWD';
        employee.SFDC_User_Account__c = UserInfo.getUserId();
        insert employee;
        return employee.Id;
    }
    public static Id createBusinessUnitRegion(String targetMarketId){
        Id regionRecordTypeId = Schema.SObjectType.Region__c.getRecordTypeInfosByDeveloperName().get('Geographic').getRecordTypeId();
        Region__c businessUnitRegion = new Region__c();
        businessUnitRegion.Name = 'Concrete Region';
        businessUnitRegion.Active__c=true;
        businessUnitRegion.Target_Market__c = 'Concrete';
        businessUnitRegion.Target_Market_Lookup__c = targetMarketId;
        businessUnitRegion.ISO_Country__c = 'CA';
        businessUnitRegion.Local_Name__c = 'Region';
        businessUnitRegion.Country_Code__c = '6101';
        businessUnitRegion.Regional_Manager_EMPL__c = PricingTestDataFactory.createEmployee('Test_3');
        businessUnitRegion.Region_Office_Street__c = '6915 Davand';
        businessUnitRegion.Region_Office_City__c = 'Mississauga';
        businessUnitRegion.Region_Office_State__c = 'On';
        businessUnitRegion.Region_Office_Zip__c = 'L5T 1L5';
        businessUnitRegion.RecordTypeId = regionRecordTypeId;
        insert businessUnitRegion;
        
        return businessUnitRegion.Id;
    }  
    public static Id createSPAOpportunity(String accountId){
        Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SPA_Pricing').getRecordTypeId();
        Id targetmarketId = PricingTestDataFactory.createTargetMarket(accountId);
        Id businessUnitRegionId = PricingTestDataFactory.createBusinessUnitRegion(targetmarketId);
        Account accountObj = [Select Id,Name from Account where Id =:accountId]; 
        Opportunity opportunityObj = new Opportunity(Name='SPA Navixel Opportunity '+System.today(),CloseDate=System.today()+2,AccountId=accountObj.Id,StageName='Draft',Business_Unit_Region__c=businessUnitRegionId);
        opportunityObj.RecordTypeId = opportunityRecordTypeId;
        insert opportunityObj;
        
        return opportunityObj.Id;
    }
    public static Id createSPQOpportunity(String accountId){
        Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SPQ_Pricing').getRecordTypeId();
        Id targetmarketId = PricingTestDataFactory.createTargetMarket(accountId);
        Id businessUnitRegionId = PricingTestDataFactory.createBusinessUnitRegion(targetmarketId);
        Account accountObj = [Select Id,Name from Account where Id =:accountId]; 
        Opportunity opportunityObj = new Opportunity(Name='SPQ Navixel Opportunity '+System.today(),CloseDate=System.today()+2,AccountId=accountObj.Id,StageName='Draft',Business_Unit_Region__c=businessUnitRegionId);
        opportunityObj.RecordTypeId = opportunityRecordTypeId;
        insert opportunityObj;
        
        return opportunityObj.Id;
    }
    public static Id createSPAQuote(String opportunityId){
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('SPA_Pricing').getRecordTypeId();
        Opportunity opportunityObj = [Select Id,Name,AccountId from Opportunity where Id =:opportunityId LIMIT 1]; 
        Id contactId = PricingTestDataFactory.createPrimaryContactForAccount(opportunityObj.AccountId);
        Quote quoteObj = new Quote(Name='SPA Navixel Quote '+System.today(),OpportunityId=opportunityObj.Id,Status='Draft',Language__c='English');
        quoteObj.RecordTypeId = quoteRecordTypeId;
        quoteObj.ContactId = contactId;
        insert quoteObj;
        
        return quoteObj.Id;
    }
    public static Id createSPQQuote(String opportunityId){
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('SPQ_Pricing').getRecordTypeId();
        Opportunity opportunityObj = [Select Id,Name,AccountId from Opportunity where Id =:opportunityId LIMIT 1]; 
        Id contactId = PricingTestDataFactory.createPrimaryContactForAccount(opportunityObj.AccountId);
        Quote quoteObj = new Quote(Name='SPQ Navixel Opportunity '+System.today(),OpportunityId=opportunityObj.Id,Status='Draft',Language__c='English');
        quoteObj.RecordTypeId = quoteRecordTypeId;
        quoteObj.ContactId = contactId;
        insert quoteObj;
        
        return quoteObj.Id;
    }
    public static List<QuoteLineItem> createSPAOppAndQuoteWithLineItems(Id accountId,Id pricebookId,List<PricebookEntry> pricebookEntries){
        Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SPA_Pricing').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('SPA_Pricing').getRecordTypeId();
        Id targetmarketId = PricingTestDataFactory.createTargetMarket(accountId);
        Id businessUnitRegionId = PricingTestDataFactory.createBusinessUnitRegion(targetmarketId);
        /*OPPORTUNITY*/
        Opportunity opportunityObj = new Opportunity(Name='SPQ Navixel Opportunity '+System.today(),CloseDate=System.today()+2,AccountId=accountId,StageName='Draft',Business_Unit_Region__c=businessUnitRegionId,pricebook2Id = pricebookId);
        opportunityObj.RecordTypeId = opportunityRecordTypeId;
        insert opportunityObj;
        /*QUOTE*/
        Id contactId = PricingTestDataFactory.createPrimaryContactForAccount(accountId);
        Quote quoteObj = new Quote(Name='SPA Navixel Quote '+System.today(),OpportunityId=opportunityObj.Id,Status='Draft',pricebook2Id = pricebookId,Language__c='English');
        quoteObj.RecordTypeId = quoteRecordTypeId;
        quoteObj.ContactId = contactId;
        insert quoteObj;
        /*ADD QUOTE LINE ITEMS*/
        List<QuoteLineItem> quoteLineItems = new List<QuoteLineItem>();
        for(PricebookEntry pricebookEntryObj:pricebookEntries){
            System.debug('pricebookEntryObj-1-'+pricebookEntryObj);
            QuoteLineItem quoteLineItemObj = new QuoteLineItem(QuoteId = quoteObj.Id, Quantity = 1,PricebookEntryId=pricebookEntryObj.Id,UnitPrice = 1000);
            quoteLineItems.add(quoteLineItemObj);
        }
        System.debug('quoteLineItems--'+quoteLineItems);
        insert quoteLineItems;
        return quoteLineItems;
    }
    public static void updateSPAQuote(Id quoteId){
        Quote quoteObj = [Select Id,Approved_Date__c from Quote where Id=:quoteId];
        quoteObj.Approved_Date__c = Date.today();
        update quoteObj;
    }
    /*public static List<OpportunityLineItem> addOpportunityLineItems(List<String> pricebookEntryIds,String opportunityId){
        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();
        for(String pricebookEntryId:pricebookEntryIds){
            OpportunityLineItem opportunityLineItemObj = new OpportunityLineItem(OpportunityId = opportunityId, Quantity = 10,PricebookEntryId=pricebookEntryId,UnitPrice = 1000);
            opportunityLineItems.add(opportunityLineItemObj);
        }
        insert opportunityLineItems;
        List<OpportunityLineItem> insertedOpportunityLineItems = [Select Id from OpportunityLineItem where PricebookEntryId IN:pricebookEntryIds and OpportunityId=:opportunityId];
        return insertedOpportunityLineItems;
    }*/
    public static void deleteOpportunity(String opportunityId){
        List<Opportunity> opportunityObjs = [Select Id,Name from Opportunity where Id=:opportunityId];
        delete opportunityObjs;
    }
    public static void addAttachmentToParent(Id parentId) {
    	Blob b = Blob.valueOf('Test Data');
    	
    	Attachment attachment = new Attachment();
    	attachment.ParentId = parentId;
    	attachment.Name = 'Test Attachment for Parent';
    	attachment.Body = b;
    	
    	insert attachment;
    }
    public static void updatePriceBookEntry( String pricebookEntryId){
        List<PricebookEntry> pricebookEntries = [Select Id from PricebookEntry where Id=:pricebookEntryId];
        if(pricebookEntries!=null && pricebookEntries.size()>0){
            List<PricebookEntry> pricebookEntriesToUpdate = new List<PricebookEntry>();
            for(PricebookEntry pricebookEntryObj:pricebookEntries){
                pricebookEntryObj.UnitPrice = 10.00;
                pricebookEntriesToUpdate.add(pricebookEntryObj);
            }
            update pricebookEntriesToUpdate;
        }
    }
}