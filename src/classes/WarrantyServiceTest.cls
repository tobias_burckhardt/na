@IsTest
private class WarrantyServiceTest {

    @TestSetup
    static void setup() {
        Account acc = (Account) SObjectFactory.create(Account.SObjectType, new Map<SObjectField, Object> {
                Account.Name => 'AccName',
                Account.SAP_Phone__c => '1234567890'
        });
        Contact c = (Contact) SObjectFactory.create(Contact.SObjectType, new Map<SObjectField, Object> {
                Contact.FirstName => 'First', Contact.LastName => 'Last', Contact.AccountId => acc.Id, Contact.Phone => '1234567890'
        });
        Opportunity opp = (Opportunity) SObjectFactory.create(Opportunity.SObjectType, new Map<SObjectField, Object> {
                Opportunity.Anticipated_Start_Date__c => Date.today(), Opportunity.Roofing_Primary_Contact__c => c.Id,
                Opportunity.AccountId => acc.Id
        });
        Product2 productIntended = (Product2) SObjectFactory.create(Product2.SObjectType, new Map<SObjectField, Object> {
                Product2.Name => 'Warranty', Product2.Roofing_Zero_Value_Warranty__c => true, Product2.IsActive => true
        });
        Product2 productExtension = (Product2) SObjectFactory.create(Product2.SObjectType, new Map<SObjectField, Object> {
                Product2.Name => 'Warranty Extension'
        });
        SObjectFactory.create(Building_Group__c.SObjectType, new Map<SObjectField, Object> {
                Building_Group__c.Accepted__c => false, Building_Group__c.Intended_Warranty_Product__c => productIntended.Id,
                Building_Group__c.Warranty__c => null, Building_Group__c.Opportunity__c => opp.Id,
                Building_Group__c.Warranty_Extended_Product__c => productExtension.Id, Building_Group__c.Windspeed__c => '46 mph (74 km/h)'
        });
    }

    @IsTest
    static void testIssueWarranty() {
        Building_Group__c buildingGroup = [SELECT Id, Windspeed_MPH__c FROM Building_Group__c LIMIT 1];
        buildingGroup.Accepted__c = true;

        Test.startTest();
        update buildingGroup;
        Test.stopTest();

        Warranty__c warranty = [SELECT Id, Applicator_Name__c, Applicator_Contact_Phone__c, Windspeed__c FROM Warranty__c LIMIT 1];
        System.assertEquals(buildingGroup.Windspeed_MPH__c, warranty.Windspeed__c);
        System.assertEquals('AccName', warranty.Applicator_Name__c);
        System.assertEquals('1234567890', warranty.Applicator_Contact_Phone__c);
        buildingGroup = [SELECT Id, Warranty__c FROM Building_Group__c LIMIT 1];
        System.assertEquals(warranty.Id, buildingGroup.Warranty__c, 'Should have created and assigned a warranty to the building group');
    }
}