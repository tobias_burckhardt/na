@isTest
private class sika_SCCZoneTestFactoryTest {

    @isTest static void testCreateUser(){
        User u = sika_SCCZoneTestFactory.createUser();
        Test.startTest();
        Database.saveResult sr = Database.insert(u);
        system.assertEquals(sr.isSuccess(), true);
        Test.stopTest();
    }

    @isTest static void testCreateAccount() {
        Test.startTest();
        User u = sika_SCCZoneTestFactory.createAdminUser();
        insert(u);

        system.runAs(u){
            Account a = sika_SCCZoneTestFactory.createAccount(new map<String, String>{'ownerId' => u.Id});
            Database.saveResult sr = Database.insert(a);
            system.assertEquals(sr.isSuccess(), true);
        }
        Test.stopTest();
    
    }

    @isTest static void testCreateContact() {
        Account a;
        User u = sika_SCCZoneTestFactory.createAdminUser();
        u.userName = 'adminUser2@test.com.sikasccz';
        u.lastname = 'user2';
        u.email = 'email@mail.com';
        u.alias = 'alias2';

        Test.startTest();
        insert(u);

        system.runAs(u){
            a = sika_SCCZoneTestFactory.createAccount(new map<String, String>{'ownerId' => u.Id});
            Database.saveResult sr = Database.insert(a);
            system.assertEquals(sr.isSuccess(), true);
        }
        Test.stopTest();

        map<String, String> params = new map<String, String>{'acccountId' => a.Id};
        sika_SCCZoneTestFactory.createContact(params);

    }

    @isTest static void testCreateCase(){
        Case c = sika_SCCZoneTestFactory.createCase();
        c.Project_Building_Name__c = 'Project Building Name';
        c.Master_Category__c = 'Account Administration';
        c.Category__c = 'Change Employee Role';
        Test.startTest();
        Database.saveResult sr = Database.insert(c);
        Test.stopTest();
        system.assertEquals(sr.isSuccess(), true);
    }

    @isTest static void testCreateGuestUser(){
        User u = sika_SCCZoneTestFactory.createAdminUser();
        system.assertEquals(u.profileId, sika_SCCZoneTestFactory.getSysAdminProfileId());

    }

    // will insert the created objects and return a map of objectName => object
    @isTest static void testCreateSCCZoneUserContactAndAccount(){
        Test.startTest();
        map<String, sObject> objects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
        Test.stopTest();

        Account a = (Account) objects.get('theAccount');
        User accountOwner = (User) objects.get('theAccountOwner');
        User adminUser = (User) objects.get('adminUser');
        User sccZoneUser = (User) objects.get('SCCZoneUser');
        Contact c = (Contact) objects.get('theContact');

        system.assertEquals(adminUser.ProfileId, sika_SCCZoneTestFactory.getSysAdminProfileId());
        system.assertEquals(sccZoneUser.ProfileId, sika_SCCZoneTestFactory.getSCCZoneUserProfileId());
        system.assertEquals(sccZoneUser.contactId, c.Id);
        system.assertEquals(c.AccountId, a.id);
    }

    @isTest static void testCreateSandStd(){
        Material__c m = sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'type' => 'Sand', 'SOM' => 'US Customary'});
        system.assertEquals(m.Type__c, 'Sand');
        Test.startTest();
        Database.saveResult sr = Database.insert(m);
        Test.stopTest();
        system.assertEquals(sr.isSuccess(), true);
    }
    
    @isTest static void testCreateSandMetric(){
        Material__c m = sika_SCCZoneTestFactory.createMaterialSand();
        system.assertEquals(m.Type__c, 'Sand');
        Test.startTest();
        Database.saveResult sr = Database.insert(m);
        Test.stopTest();
        system.assertEquals(sr.isSuccess(), true);
    }

    @isTest static void testCreateCoarseAgg(){
        Material__c m = sika_SCCZoneTestFactory.createMaterialCoarseAgg();
        system.assertEquals(m.Type__c, 'Coarse Aggregate');
        Test.startTest();
        Database.saveResult sr = Database.insert(m);
        Test.stopTest();
        system.assertEquals(sr.isSuccess(), true);
    }

    @isTest static void testCreateSCM(){
        Material__c m = sika_SCCZoneTestFactory.createMaterialSCM();
        system.assertEquals(m.Type__c, 'SCM');
        Test.startTest();
        Database.saveResult sr = Database.insert(m);
        Test.stopTest();
        system.assertEquals(sr.isSuccess(), true);
    }

    @isTest static void testCreateCement(){
        Material__c m = sika_SCCZoneTestFactory.createMaterialCement();
        system.assertEquals(m.Type__c, 'Cement');
        Test.startTest();
        Database.saveResult sr = Database.insert(m);
        Test.stopTest();
        system.assertEquals(sr.isSuccess(), true);
    }

    @isTest static void testCreateWater(){
        Material__c m = sika_SCCZoneTestFactory.createMaterialWater();
        system.assertEquals(m.Type__c, 'Water');
        Test.startTest();
        Database.saveResult sr = Database.insert(m);
        Test.stopTest();
        system.assertEquals(sr.isSuccess(), true);
    }

    @isTest static void testCreateMaterialUS(){
        Material__c m = sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'SOM' => 'US Customary', 'type' => 'Coarse Aggregate'});
        system.assertEquals(m.Unit_System__c, 'US Customary');
        Test.startTest();
        Database.saveResult sr = Database.insert(m);
        Test.stopTest();
        system.assertEquals(sr.isSuccess(), true);
    }

    @isTest static void testCreateMix(){
        Mix__c m = sika_SCCZoneTestFactory.createMix();
        Test.startTest();
        Database.saveResult sr = Database.insert(m);
        Test.stopTest();
        system.assertEquals(sr.isSuccess(), true);
    }

    @isTest static void testCreateMixMaterial(){
        Mix__c theMix = sika_SCCZoneTestFactory.createMix();
        insert(theMix);
        Material__c theMaterial = sika_SCCZoneTestFactory.createMaterial();
        insert(theMaterial);
        Mix_Material__c mm = sika_SCCZoneTestFactory.createMixMaterial(theMix, theMaterial);
        Test.startTest();
        Database.saveResult sr = Database.insert(mm);
        Test.stopTest();
        system.assertEquals(sr.isSuccess(), true);
    }

    /**
     * Tests the basic generation of random strings along with the entropy of them.
     * We want to ensure that generation of the strings does not result in a repeat
     * within a batch of 100.  We also execute some tests that ensure that our
     * assumptions about how Apex works (based on documentation) actually functions
     * the way we expect it to.
     */
    @isTest
    static void testRandomString() {
        String randomString;
        Set<String> generated = new Set<String>();
        for (Integer index = 0; index < 500; index++) {
            // generate a string and check length
            randomString = sika_SCCZoneTestFactory.randomStr(false);
            System.assert(randomString.length() >= 3);
            
            // add() returns false if the set hasn't changed as a result of adding an object
            // If the randomString was already returned once, s.add(randomString) will return false
            // this tests the entropy of the random data generation
            System.assert(generated.add(randomString), 'Random string repeated in iteration ' + (index + 1));
            System.assert(generated.contains(randomString), 'We added a string, how is it not there?');
            
            // try adding it againt to validate a no-update scenario as stated above
            System.assert(!generated.add(randomString), 'Set should not have changed.');
        }
    }

    @isTest static void testGetters(){
        system.assert((String.valueOf(sika_SCCZoneTestFactory.getUserRoleId()).length() == 15) || (String.valueOf(sika_SCCZoneTestFactory.getUserRoleId()).length() == 18));
        system.assert((String.valueOf(sika_SCCZoneTestFactory.getAdminUserRoleId()).length() == 15) || (String.valueOf(sika_SCCZoneTestFactory.getAdminUserRoleId()).length() == 18));
        system.assert((String.valueOf(sika_SCCZoneTestFactory.getSysAdminProfileId()).length() == 15) || (String.valueOf(sika_SCCZoneTestFactory.getSysAdminProfileId()).length() == 18));
        system.assert((String.valueOf(sika_SCCZoneTestFactory.getStdCommsUserProfileId()).length() == 15) || (String.valueOf(sika_SCCZoneTestFactory.getStdCommsUserProfileId()).length() == 18));
        system.assert((String.valueOf(sika_SCCZoneTestFactory.getSCCZoneUserProfileId()).length() == 15) || (String.valueOf(sika_SCCZoneTestFactory.getSCCZoneUserProfileId()).length() == 18));
        system.assert((String.valueOf(sika_SCCZoneTestFactory.getGuestUserProfileId()).length() == 15) || (String.valueOf(sika_SCCZoneTestFactory.getGuestUserProfileId()).length() == 18));
        system.assert((String.valueOf(sika_SCCZoneTestFactory.getSalesUserProfileId()).length() == 15) || (String.valueOf(sika_SCCZoneTestFactory.getSalesUserProfileId()).length() == 18));
        
    }
    
}