@isTest
public class TestUtilities {
    
    //******************************************************************
    // CONSTANTS
    //******************************************************************

    public static final String ADMIN_PROFILE_NAME 	= 'System Administrator' ;
    public static final Id PRICEBOOK_ID 			= Test.getStandardPricebookId();
 	public static final String PORTAL_PROFILE_NAME 	= 'SCC Zone Community User';


    //******************************************************************
    // STATIC VARIABLES 
    //******************************************************************


    public static User ADMIN_USER { private set; get{           
            if(ADMIN_USER==null){ 
                ADMIN_USER = [SELECT Id, Name, UserName, IsActive, Email 
                                FROM User 
                                WHERE Profile.Name = : ADMIN_PROFILE_NAME
                                AND IsActive=true 
                                LIMIT 1];
        	}
        	return ADMIN_USER;
        }        
    }

    public static Id PORTAL_PROFILE_ID { private set; get{
            if(PORTAL_PROFILE_ID==null)
                PORTAL_PROFILE_ID = [SELECT Id 
                                        FROM Profile 
                                        WHERE Name = : PORTAL_PROFILE_NAME].Id;
            return PORTAL_PROFILE_ID;
        }       
    }

    //******************************************************************
    // TEST METHODS FOR TEST DATA CREATION
    //******************************************************************

    public static List<Account> CreateAccounts (Integer quantity, Boolean doInsert ){
        return createAccounts('Test Account ',quantity, doInsert);
    }

    public static List<Account> CreateAccounts (String namePrefix, Integer quantity, Boolean doInsert ){
        List<Account> accounts = new List<Account>();
        for(Integer i = 1; i <= quantity; i++){ 
            accounts.add( new Account( 
                Name = namePrefix + String.valueOf(i)
                , Industry 			= 'Other'
                , Type 				= 'Prospect'
                , NumberOfEmployees = i
                , BillingCountry 	= 'United States'
                ) 
            );
		}
        if (doInsert){ 
        	insert accounts;
        }

        return accounts;
    }


    public static List<Contact> CreateContacts (List<Account> accounts, Integer quantity, Boolean doInsert){
        return createContacts ('John', 'Doe ', accounts, quantity, doInsert);
    }

    public static List<Contact> CreateContacts (String firstName, String lastName, List<Account> accounts, Integer quantity, Boolean doInsert){
        List<Contact> contacts = new List<Contact>();

        for(Account a : accounts){ 
            for(Integer i = 1; i<= quantity; i++){
                Contact c 			= new Contact();
                c.AccountId 		= a.Id;
                c.FirstName 		= firstName;
                c.LastName 			= lastName + ('0000'+String.valueOf(i)).right(4);
                c.Email 			= c.FirstName.replace(' ','')+'.'+c.LastName.replace(' ','')+'@'+a.Name.replace(' ','')+'.com';
                c.Phone				= '5556667777';
                c.MailingStreet		= 'TestStreet';
                c.MailingCity		= 'TestCity';
                c.MailingCountry 	= 'United States';
                c.MailingPostalcode	= '12345';
                contacts.add(c);
            }
        }

        if (doInsert){
        	insert contacts;
        }

        return contacts;
    }

    public static List<Opportunity> CreateOpportunities (List<Account> accounts, Integer quantity, Boolean doInsert){
        return createOpportunities ('Test Opp', accounts, quantity, doInsert);
    }

    public static List<Opportunity> CreateOpportunities (String oppNamePrefix, List<Account> accounts, Integer quantity, Boolean doInsert){
        List<Opportunity> opportunities = new List<Opportunity>();

        for(Account a : accounts){
            for(Integer i = 1; i<= quantity; i++) {
                Opportunity opp = new Opportunity();
                opp.Name 			= oppNamePrefix + ' ' + String.valueOf(i);
                opp.AccountId 		= a.Id;
                opp.StageName 		= 'Closed Won';
                opp.CloseDate 		= Date.valueOf(Datetime.now().addMonths(i));
                opp.Amount = 5;
                opp.Roofing_Area_SqFt__c = 5;
                opp.Estimated_Ship_Date__c = Date.today();
                opportunities.add(opp);
            }
        }

        if (doInsert){
        	insert opportunities;
        }

        return opportunities;
    }

    public static List<Project__c> createProjects(Integer quantity, Boolean doInsert) {
        List<Project__c> projects = new List<Project__c>();
        for (Integer i = 0; i < quantity; i++) {
            Project__c proj = new Project__c();
            projects.add(proj);
        }

        if (doInsert) {
            insert projects;
        }

        return projects;
    }

    public static Credit_Split__c createCreditSplits(Opportunity oppty, User salesperson, String type, Boolean doInsert) {
        Credit_Split__c split = new Credit_Split__c(
            Opportunity__c = oppty.Id,
            Salesperson__c = salesperson.Id,
            Type__c = type
        );

        if (doInsert) {
            insert split;
        }

        return split;
    }

    public static User createUser (Id profileId, String userName, Boolean doInsert) {
        User testUser = new User();
        testUser.LastName = userName;
        testUser.Alias = userName;
        testUser.Email = userName + '@sikaapextest.com';
        testUser.Username = userName + '@ikaapextest.com';
        testUser.CommunityNickname = userName;
        testUser.TimeZoneSidKey = 'America/New_York';
        testUser.LocaleSidKey = 'en_US';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.ProfileId = profileId;
        testUser.LanguageLocaleKey = 'en_US';
        testUser.IsActive = true;

        if (doInsert) {
            System.runAs(ADMIN_USER) {
                insert testUser;
            }
        }

        return testUser;
    }
}