public without sharing class AwardQuoteControllerWithoutSharing {
    public List<Apttus_Proposal__Proposal__c> getProposal(Id opptyId) {
        return [
            SELECT Id, Name
            FROM Apttus_Proposal__Proposal__c
            WHERE Apttus_Proposal__Opportunity__c = :opptyId
        ];
    }

    public List<Apttus_Proposal__Proposal_Line_Item__c> getLineItems(Set<Id> proposalIds) {
        return [
            SELECT Id, Name, SystemID__c, APTS_Attachment_Group__c,
                Apttus_Proposal__Proposal__c,
                Apttus_QPConfig__ClassificationId__r.Apttus_Config2__HierarchyId__c,
                Apttus_QPConfig__ClassificationId__r.Apttus_Config2__HierarchyId__r.Name,
                Apttus_QPConfig__ClassificationId__c, Apttus_Proposal__Product__r.Name,
                Apttus_QPConfig__ClassificationId__r.Name, Apttus_Proposal__Product__c,
                Apttus_QPConfig__ClassificationId__r.Include_in_Award_NOA_Page__c,
                Apttus_Proposal__Product__r.Attachment_Group__c,
                Apttus_Proposal__Product__r.APTS_Roofing_Description__c,
                Apttus_Proposal__Product__r.APTS_Applicator_Active__c,
                Apttus_Proposal__Product__r.NOAActive__c,
                Apttus_Proposal__Product__r.PTS_IsMembrane__c
            FROM Apttus_Proposal__Proposal_Line_Item__c
            WHERE Apttus_Proposal__Proposal__c IN :proposalIds
        ];
    }

    public void insertBuildingGroupProducts(List<Building_Group_Product__c> products) {
        insert products;
    }
}