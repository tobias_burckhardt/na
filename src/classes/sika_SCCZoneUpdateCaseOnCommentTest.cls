@isTest
private class sika_SCCZoneUpdateCaseOnCommentTest {

	@isTest static void deployTest() {
		Case c = sika_SCCZoneTestFactory.createCase();
		list<SelectOption> selectOptions = sika_SCCZonePicklistValuesHelper.getPicklistValues(new Case(), 'SCC_Zone_Case_Types__c');
		system.assert(selectOptions.size() > 0);

		c.SCC_Zone_Case_Types__c = String.valueOf(selectOptions[0]);

		c.Project_Building_Name__c = 'Project Building Name';
		c.Master_Category__c = 'Account Administration';
		c.Category__c = 'Change Employee Role';
		insert(c);
		
		dateTime originalLastModifiedValue = [SELECT Id, LastModifiedDate FROM Case WHERE Id = : c.Id].LastModifiedDate;

		CaseComment cc = new CaseComment();
		cc.ParentId = c.Id;
		insert(cc);

		dateTime newLastModifiedValue = [SELECT Id, LastModifiedDate FROM Case WHERE Id = : c.Id].LastModifiedDate;
		system.assert(newLastModifiedValue >= originalLastModifiedValue);
	}

}