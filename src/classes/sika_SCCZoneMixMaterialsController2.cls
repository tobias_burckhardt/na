public without sharing class sika_SCCZoneMixMaterialsController2 {
    
    private Id userID {get; set;}
    public Id mixId {get; set;}
    public Mix__c theMix {get; set;} 

    private list<Material__c> materialList;     // the complete list of materials owned by the current user, plus all Admixtures. (Admixtures are owned by Sika, and visible to all SCC Zone users.)
    private list<Mix_Material__c> theMixMaterials {get; set;}   // list of mix materials that have already been associated with the mix -- i.e., saved to the database.
    
    public list<mmWrapper> selectedMixMaterials {get; set;}     // the list of "mix_materials" (mmWrappers) displayed in the "current mix materials" table
    public list<materialWrapper> wrappedMaterials {get; set;}   // the list of "materials" (materialWrappers) displayed in the "add new materials" table

    private set<Id> selectedMaterialIDs = new set<Id>();        // IDs of all mix_materials with wrappers displayed in the "current mix materials" table -- i.e., with wrappers in the selectedMixMaterials list
    @TestVisible private list<Mix_Material__c> mixMaterialsToDelete = new list<Mix_Material__c>();

    // we're dividing the materialList into separate lists for each material type, in order to make rendering faster when the user selects a filter from the piclist in the "add new materials" section of the page
    private list<Material__c> admixtures = new list<Material__c>();
    private list<Material__c> cements = new list<Material__c>();
    private list<Material__c> SCMs = new list<Material__c>();
    private list<Material__c> sands = new list<Material__c>();
    private list<Material__c> coarseAggs = new list<Material__c>();
    private list<Material__c> waters = new list<Material__c>();
    private list<Material__c> others = new list<Material__c>();
    private list<Material__c> allMaterials = new list<Material__c>();

    // maximum allowable # of each material type allowed in a mix.
    // also referenced from the MixMaterialTriggerHelper.cls; possibly elsewhere (TK).
    //CHANGED: [2015-07-29 SS] From @TestVisible private to public static (so we can use these values in the MixMaterialTriggerHelper too.)
    public static Integer admixtureMax = 5;
    public static Integer cementMax = 1;
    public static Integer scmMax = 1;
    public static Integer sandMax = 5;
    public static Integer aggMax = 5;
    public static Integer waterMax = 1;
    public static Integer otherMax = 2;

    // used to flag materials of a type that is already at its maximum
    public Boolean admixtureOverLimit {get; set;}
    public Boolean cementOverLimit {get; set;}
    public Boolean SCMOverLimit {get; set;}
    public Boolean sandOverLimit {get; set;}
    public Boolean aggOverLimit {get; set;}
    public Boolean waterOverLimit {get; set;}
    public Boolean otherOverLimit {get; set;}

    @TestVisible private set<Id> overLimit {get; set;}
    public Boolean hasErrors {get; set;}
    public String errorString {get; set;}

public Boolean displayPopUp {get; set;}

    public String selectOption {get; set;}  // value of the picklist used to filter the "add new materials" list by material type


    // constructor
    public sika_SCCZoneMixMaterialsController2() {
        sika_SCCZoneCheckAuth.setAuthStatus(UserInfo.getProfileId());

        userId = UserInfo.getUserId();
        mixId = Apexpages.currentPage().getParameters().get('mixID');
        
        // if no mixId is passed to the page, the pageAction will catch the error & redirect the user to the appropriate page. We'll create a new mix here so the constructor won't error out before the pageAction fires
        if(mixId != null){
            theMix = [SELECT ID, Mix_Name__c, Target_SCM_Ratio__c, System_of_Measure__c FROM Mix__c WHERE ID =: mixID LIMIT 1];
        } else {
            theMix = new Mix__c();
        }

        selectedMixMaterials = new list<mmWrapper>();
        overLimit = new set<Id>();
        errorString = '';
        hasErrors = false;

        displayPopup = false;

        // get the list of mix materials already associated with this mix
        theMixMaterials = [SELECT Id, 
                                  Mix__c,
                                  Material__c,
                                  Material__r.Id,
                                  Material_Name__c,
                      Material__r.Material_Name__c,
                                  Material_Type__c,
                      Material__r.Type__c,
                                  Material__r.Source__c,
                                  Material__r.Spec_Gravity__c, 
                                  Material__r.Abs__c,
                                  Moisture__c,
                                  Volume__c,
                                  Quantity__c,
                                  Unit__c,
                                  Cost__c
                             FROM Mix_Material__c
                            WHERE Mix__c = :mixId
                         ORDER BY Material__r.Type__c];

        if(theMixMaterials.isEmpty()){
            theMixMaterials = new list<Mix_Material__c>();
        } else {
        
        // populate a set of material IDs for materials that are already in the mix,
        // add the mix materials to the selectedMixMaterials list (as mmWrappers),
        // and increment the material type counters as needed
            for(Mix_Material__c mm : theMixMaterials){
                selectedMaterialIDs.add(mm.Material__r.Id);
                selectedMixMaterials.add(new mmWrapper(mm, mm.Material_Name__c, mm.Material_Type__c, theMix.System_of_Measure__c, isQuantityRequired(mm.Material_Type__c), isUnitRequired(mm.Material_Type__c), isMoistureRequired(mm.Material_Type__c)));
                incrementTypeCounts(mm);
            }
        }

        // get a list of all materials available to the current user. (User's materials and user's account's materials)
        materialList = [SELECT Id, 
                               Material_Name__c, 
                               Type__c, 
                               Source__c, 
                               Cost__c, 
                               Owner.Name, 
                               Owner.alias, 
                               Description__c, 
                               Unit__c, 
                               Unit_System__c, 
                               Abs__c, 
                               CreatedById, 
                               Spec_Gravity__c, 
                               CreatedDate,
                               isActive__c
                          FROM Material__c 
                         WHERE isActive__c = true 
                           AND (OwnerID = :userID OR Type__c = 'Admixture') 
                      ORDER BY Material_Name__c];
        
        if(materialList.isEmpty()){
            materialList = new list<Material__c>();
        }

        set<Id> admixtureIDs = new set<Id>();

        // to make sure the rerender after selecting a material type filter from the picklist happens quickly, even as the application scales, we'll populate lists segregated by material type on page load. Then just loop through to prevent showing selected materials on list refresh.
        for(Material__c m : materialList){
            if(m.Type__c != 'Admixture'){
                allMaterials.add(m);
            }

            if(m.Type__c == 'Admixture'){ 
                admixtureIDs.add(m.Id);
                admixtures.add(m); 
                continue;
            }
            if(m.Type__c == 'Cement'){ 
                cements.add(m); 
                continue;
            }
            if(m.Type__c == 'SCM'){ 
                SCMs.add(m); 
                continue;
            }
            if(m.Type__c == 'Sand'){ 
                sands.add(m); 
                continue;
            }
            if(m.Type__c == 'Coarse Aggregate'){ 
                coarseAggs.add(m); 
                continue;
            }
            if(m.Type__c == 'Water'){ 
                waters.add(m); 
                continue;
            }
            if(m.Type__c == 'Other'){ 
                others.add(m); 
                continue;
            }
            system.debug('********ERROR: Material type not found!! (' + m.Id + ') :: type = \'' + m.Type__c + '\'');
        }   

        // material records for admixtures are viewable by all users, but only the sysAdmin can create/edit/delete them.  In order to allow SCC Zone users to store the price they
        // were quoted for an admixture, we use the Admixture_Price object.  We'll query that object so we can display the current user's price for each admixture here.
        if(admixtureIDs.size() > 0){
            list<Admixture_Price__c> admixturePrices = [SELECT Admixture_Material__c, Price__c FROM Admixture_Price__c WHERE Admixture_Material__c IN :admixtureIDs];
            map<Id, Decimal> materialID_to_ap = new map<Id, Decimal>();

            for(Admixture_Price__c ap : admixturePrices){
                materialID_to_ap.put(ap.Admixture_Material__c, ap.Price__c);
            }
            // set each (admixture) material's Cost__c value to the value in its related Admixture_Price__c.Price__c field.
            for(Material__c m : admixtures){
                m.Cost__c = materialID_to_ap.get(m.Id) == null ? null : materialID_to_ap.get(m.Id);
            }
        }

        populateWrappedMaterials();

    }

    // because this controller runs "without sharing" we need to manually confirm that the user should have access. (A user whose session has timed-out, or an otherwise unauthenticated user who accessed this page directly, would be able to view the page otherwise, albeit without any data displayed.)
    public PageReference loadAction(){
        PageReference p = sika_SCCZoneCheckAuth.doAuthCheck();

        // if the user is not authorized to view this page, redirect to the login page
        if(p != null){
            return p;
        } 
        // otherwise...
        // be sure the page was passed all appropriate record IDs
        PageReference idCheck = checkId();
        if(idCheck != null){
            return idCheck;
        }
        return null;
    }

    private PageReference checkId(){
        if(mixId == null){
            system.debug('********ERROR: No Mix record Id was passed to the page. Redirecting to the mixes HomePage.');
            PageReference pageRef = new PageReference('/sika_SCCZoneMixesHomePage');
            pageRef.getParameters().put('view', 'me');
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
    }


    public void setWrapper(){
        updateSelectedMaterialsCache();
        wrappedMaterials = populateWrappedMaterials();
    }


    // return a list of materialWrappers for display in the wrappedMaterials ("add new materials") list
    public list<materialWrapper> populateWrappedMaterials() {
        wrappedMaterials = new list<materialWrapper>();
            
        if(selectOption == 'all' || selectOption == null){
            for(Material__c m : allMaterials){
                if(!selectedMaterialIDs.contains(m.Id)){
                    wrappedMaterials.add(new materialWrapper(m));
                }
            }
        }
        if(selectOption == 'ad'){
            for(Material__c m : admixtures){
                if(!selectedMaterialIDs.contains(m.Id)){
                    wrappedMaterials.add(new materialWrapper(m));
                }
            }
        }
        if(selectOption == 'cement'){
            for(Material__c m : cements){
                if(!selectedMaterialIDs.contains(m.Id)){
                    wrappedMaterials.add(new materialWrapper(m));
                }
            }
        }
        if(selectOption == 'scm'){
            for(Material__c m : SCMs){
                if(!selectedMaterialIDs.contains(m.Id)){
                    wrappedMaterials.add(new materialWrapper(m));
                }
            }
        }if(selectOption == 'sand'){
            for(Material__c m : sands){
                if(!selectedMaterialIDs.contains(m.Id)){
                    wrappedMaterials.add(new materialWrapper(m));
                }
            }
        }
        if(selectOption == 'coarse'){
            for(Material__c m : coarseAggs){
                if(!selectedMaterialIDs.contains(m.Id)){
                    wrappedMaterials.add(new materialWrapper(m));
                }
            }
        }
        if(selectOption == 'water'){
            for(Material__c m : waters){
                if(!selectedMaterialIDs.contains(m.Id)){
                    wrappedMaterials.add(new materialWrapper(m));
                }
            }
        }
        if(selectOption == 'other'){
            for(Material__c m : others){
                if(!selectedMaterialIDs.contains(m.Id)){
                    wrappedMaterials.add(new materialWrapper(m));
                }
            }
        }

        for(materialWrapper mw : wrappedMaterials){

            // for materialWrappers that were selected for addition, but rejected (because there were already too many of the related material's type in the mix), highlight the material's name and keep its select box checked.
            if(overLimit.contains(mw.theMaterial.Id)){
                
                // if we're re-populating the available materials list after the user removed a material from the "current mix materials" list, and the material is of the same type as the material removed, and if the material was previously prevented from being added because there were too many of that type in the mix, then remove the mw from the overlimit set, and leave the materialWrapper checked but don't highlight it.  (I.E., don't set typeOverLimit = true)
                if(mw.theMaterial.Type__c == typeRemoved){
                    overLimit.remove(mw.theMaterial.Id);
                    mw.typeOverLimit = false;
                } else {
                    mw.typeOverLimit = true;
                }
                mw.pickMe = true;
            } else { // un-highlight any old rejects!
                mw.typeOverLimit = false;
                mw.pickMe = false;
            }

            // materialWrappers that are in the selectedMaterialsCache -- i.e., materials that were checked previous to the user selecting another type filter from the dropdown -- should show up with their select boxes checked.
            if(selectedMaterialsCache.contains(mw.theMaterial.Id)){
                mw.pickMe = true;
            }
        }
        return wrappedMaterials;

    }


    private set<Id> selectedMaterialsCache = new set<Id>(); // IDs of materials that have been selected for addition to the "current mix materials" list.

    // when the material type filter dropdown is changed, remember which materialWrappers' checkboxes have been checked.  (These materials should be seen as selected even if the user navigates away from the current filtered list of materialWrappers)
    public void updateSelectedMaterialsCache(){
        for(materialWrapper mw : wrappedMaterials){
            if(mw.pickMe == true){
                selectedMaterialsCache.add(mw.theMaterial.Id);
            } else {
                selectedMaterialsCache.remove(mw.theMaterial.Id);  // no need to check whether the set contains mw first. The remove() method won't return an error if the set doesn't contain the material we're "removing"
            }
        }

    }


    Boolean admixtureErrorAdded; // ensures that a type's error message text is only appended to the errorString once.
    Boolean cementErrorAdded;
    Boolean SCMErrorAdded;
    Boolean sandErrorAdded;
    Boolean aggErrorAdded;
    Boolean waterErrorAdded;
    Boolean otherErrorAdded;

    // add an mmWrapper to the selectedMixMaterials list
    public PageReference addMixMaterials(){
        list<materialWrapper> selectedMWs = new list<materialWrapper>();
        
        overLimit.clear();
        hasErrors = false;
        
        // reset old ErrorAdded values
        admixtureErrorAdded = false;
        cementErrorAdded = false;
        SCMErrorAdded = false;
        sandErrorAdded = false;
        aggErrorAdded = false;
        waterErrorAdded = false;
        otherErrorAdded = false;

        // clear all save validation errors when the user adds a mix_material from the mix. (Save validation errors (i.e., highlighted fields) should only show up after a failed save. We'll clear them on any transaction (add/remove) after that.)
        clearMMErrors();

        // add any materialWrappers in the currently-displayed wrappedMaterials list to the selectedMaterialsCache
        updateSelectedMaterialsCache();

        // we need to look at ALL materials, not just those in the currently-displayed wrappedMaterials list
        wrappedMaterials = new list<materialWrapper>();

        for(Material__c m : materialList){
            wrappedMaterials.add(new materialWrapper(m));
        }

        // build a list of selected materials.  Also clear old typeOverLimit values. (typeOverLimit ensures that the over-limit materials aren't added to the mix.)
        for(materialWrapper mw : wrappedMaterials){
            if(selectedMaterialsCache.contains(mw.theMaterial.Id)){
                selectedMWs.add(mw);
            }
            mw.typeOverLimit = false;
        }

        // increment the type counters for the selected materials  (We're doing this as a separate step so we can add those materials that aren't over the limit, while holding back ALL OF those that are.)
        for(materialWrapper mw : selectedMWs){
            incrementTypeCounts(mw);
        }

        // send each material (wrapper) through validation to be sure there aren't too many of its type selected
        errorString = 'A mix cannot contain more than:';
        for(materialWrapper mw : selectedMWs){

            if(isOverLimit(mw)){
                // ...add the wrapper to a set so that we can keep it checked and highlight that it's one of a type that has too many selected
                overLimit.add(mw.theMaterial.Id);
                hasErrors = true;


            } else {  // add the material to the selectedMaterialsList
                selectedMixMaterials.add(new mmWrapper(buildMixMaterial(mw), mw.theMaterial.Material_Name__c, mw.theMaterial.Type__c, theMix.System_of_Measure__c, isQuantityRequired(mw.theMaterial.Type__c), isUnitRequired(mw.theMaterial.Type__c), isMoistureRequired(mw.theMaterial.Type__c)));             
                selectedMaterialIDs.add(mw.theMaterial.Id);
            }
        }
        if(overLimit.size() > 0){
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, errorString));
            
        }

        // decrement the type counters for those materials we're holding back
        for(materialWrapper mw : selectedMWs){
            if(overLimit.contains(mw.theMaterial.Id)){
                decrementTypeCounts(mw);
            }
        }

        populateWrappedMaterials();
        selectedMaterialsCache.clear();
        return null;
    }


    public Id removeThisMaterial {get; set;}
    public String typeRemoved;

    // remove a mmWrapper from the selectedMixMaterials list
    public void removeMixMaterial(){
        hasErrors = false;

        for(integer i=0; i < selectedMixMaterials.size(); i++){
            // clear all save validation errors when the user removes a mix_material from the mix. (Save validation errors (i.e., highlighted fields) should only show up after a failed save. We'll clear them on any transaction (add/remove) after that.)
            clearMMErrors();
            
            if(selectedMixMaterials[i].theMM.Material__c == removeThisMaterial){

                selectedMaterialIDs.remove(selectedMixMaterials[i].theMM.Material__c);

                typeRemoved = selectedMixMaterials[i].matType;

                // decrement the material's type count
                decrementTypeCounts(selectedMixMaterials[i].matType);

                // if the mmWrapper is wrapping a mix_material that was previously saved to the mix; not newly-added from the mWrapper list, we'll need to delete its mix_material record on save.
                if(selectedMixMaterials[i].theMM.Id != null){
                    mixMaterialsToDelete.add(selectedMixMaterials[i].theMM);
                }
                selectedMixMaterials.remove(i); 
            }
        }

        populateWrappedMaterials();
        typeRemoved = null;
    }


    // save changes to mix_materials (& delete removed mix_material records).
    public PageReference saveMM(){
        hasErrors = false;
        clearMMErrors();
        errorString = 'Please fill in the fields highlighted below.';

        list<Mix_Material__c> mmsToSave = new list<Mix_Material__c>();
        list<mmWrapper> validatedMMList = new list<mmWrapper>();

        for(mmWrapper mmw : selectedMixMaterials){
            if(validateMM(mmw)){
                validatedMMList.add(mmw);
            }
        }

        if(hasErrors == false){
            // calculate the weight and set the unit of any SCM Mix_Materials in the Mix
            validatedMMList = calculateSCMWeight(validatedMMList);

            for(mmWrapper mmw : validatedMMList){
                mmsToSave.add(mmw.theMM);
            }
system.debug('********z mmsToSave = ' + mmsToSave);
system.debug('********z mixMaterialsToDelete = ' + mixMaterialsToDelete);
            upsert mmsToSave;
            delete mixMaterialsToDelete;

            PageReference pageRef = new PageReference('/SCCZone/apex/sika_SCCZoneMixDetailsPage?mixID=' + mixID);
            pageRef.setRedirect(true);
            return pageRef;
        }
        ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, errorString));
        return null;
    }


    // calculates any SCM's weight (at most 1 per mix) based on the cement's weight, if there is a cement.
    private list<mmWrapper> calculateSCMWeight(list<mmWrapper> MMWs){
        list<mmWrapper> theMMWs = new list<mmWrapper>(MMWs);

        String cementUnit;
        Decimal cementQty;

        String scmUnit;
        Decimal scmQty;

        // get any Cement mmWrapper's relevant values from the list of selected mmWrappers
        for(mmWrapper mmw : theMMWs){

            if(mmw.matType == 'Cement'){
                cementUnit = mmw.theMM.Unit__c;
                cementQty = mmw.theMM.Quantity__c;
                break; // there is at most 1 Cement per Mix.  (Validation has already run to ensure this by this point.)
            }
        }
           

        // calculate the SCM quantity and set the SCM unit, where appropriate
        Decimal theSCMRatio = theMix.Target_SCM_Ratio__c;
        if(cementUnit == null){
            scmUnit = 'kgs'; // needs a value. Will default to kgs.
        } else {
            scmUnit = cementUnit;
        }

        if(cementQty == null){
            scmQty = 0;
        } else {
            scmQty = cementQty * (-1 * (theSCMRatio / (theSCMRatio -1)));
        }

        for(mmWrapper mmw : theMMWs){
            if(mmw.matType == 'SCM'){
                mmw.theMM.Quantity__c = scmQty;
                mmw.theMM.Unit__c = scmUnit;
                break; // there is at most 1 SCM per mix.  (Validation has already run to ensure this by this point.)
            }
        }
        return theMMWs;
    }


    // validates the mixMaterial wrapper fields.
    private Boolean validateMM(mmWrapper mm){
        
        if((mm.quantityRequired && String.isEmpty(String.valueOf(mm.theMM.Quantity__c))) || (mm.quantityRequired && (mm.theMM.Quantity__c < 0))){ // a quantity of 0 is allowed. (Actually, quantities should default to zero.)
            mm.quantityError = true;
            hasErrors = true;
        }
        if(mm.unitRequired && String.isEmpty(String.valueOf(mm.theMM.Unit__c))){
            mm.unitError = true;
            hasErrors = true;
        }
        if(mm.moistureRequired && String.isEmpty(String.valueOf(mm.theMM.Moisture__c))){
            mm.moistureError = true;
            hasErrors = true;
        }

        if(hasErrors == true) {
            return false;
        }
        return true;
    }


    public PageReference doCancel(){
        PageReference pageRef = new PageReference('/SCCZone/apex/sika_SCCZoneMixDetailsPage?mixID=' + mixID);
        pageRef.setRedirect(true);
        return pageRef;
    }



// -----------------------------------------------------------------------------------------------------------


    private Mix_Material__c buildMixMaterial(materialWrapper mw){
        map<String, Schema.RecordTypeInfo> rt = Schema.SObjectType.Mix_Material__c.getRecordTypeInfosByName();

        Mix_Material__c tempMM = new Mix_Material__c();
        tempMM.Mix__c = mixId;
        tempMM.Material__c = mw.theMaterial.Id;
        tempMM.RecordTypeId = rt.get(mw.theMaterial.Type__c).getRecordTypeId();

        tempMM.Unit__c = null;
        return tempMM;
    }


    private Boolean isQuantityRequired(String theMaterialType){
        if(theMaterialType == 'SCM'){
            return false;
        }
        return true;
    }

    private Boolean isUnitRequired(String theMaterialType){
        if(theMaterialType == 'SCM'){
            return false;
        }
        return true;
    }

    private Boolean isMoistureRequired(String theMaterialType){
        if(theMaterialType == 'Water' || theMaterialType == 'Cement' || theMaterialType == 'SCM' || theMaterialType == 'Admixture'){
            return false;
        }
        return true;
    }


    private void clearMMErrors(){
        for(mmWrapper mm : selectedMixMaterials){
            mm.quantityError = false;
            mm.unitError = false; 
            mm.moistureError = false;
        }
    }


    private Integer admixtureCount = 0;
    private Integer cementCount = 0;
    private Integer SCMCount = 0;
    private Integer sandCount = 0;
    private Integer aggCount = 0;
    private Integer waterCount = 0;
    private Integer otherCount = 0;

    
    private Boolean isOverLimit(materialWrapper mw){

        if(mw.theMaterial.Type__c == 'Admixture' && admixtureCount > admixtureMax){
            if(admixtureErrorAdded == false) {
                errorString += '<BR> ' + admixtureMax + ' Admixture'; 
                if(admixtureMax > 1) {
                    errorString += 's';
                }
                admixtureErrorAdded = true;
            }
            return true;
        }
        if(mw.theMaterial.Type__c == 'Cement' && cementCount > cementMax){
            if(cementErrorAdded == false){
                errorString += '<BR> ' + cementMax + ' Cement'; 
                if(cementMax > 1) {
                    errorString += 's';
                } 
                cementErrorAdded = true;
            }
            return true;
        }
        if(mw.theMaterial.Type__c == 'SCM' && SCMCount > scmMax){
            if(SCMErrorAdded == false){
                errorString += '<BR> ' + scmMax + ' SCM'; 
                if(scmMax > 1) {
                    errorString += 's';
                } 
                SCMErrorAdded = true;
            }
            return true;
        }
        if(mw.theMaterial.Type__c == 'Sand' && sandCount > sandMax){
            if(sandErrorAdded == false){
                errorString += '<BR> ' + sandMax + ' Sand'; 
                if(sandMax > 1) {
                    errorString += 's';
                }
                sandErrorAdded = true;
            }
            return true;
        }
        if(mw.theMaterial.Type__c == 'Coarse Aggregate' && aggCount > aggMax){
            if(aggErrorAdded == false){
                errorString += '<BR> ' + aggMax + ' Coarse Aggregate'; 
                if(aggMax > 1) {
                    errorString += 's';
                }
                aggErrorAdded = true;
            }
            return true;
        }
        if(mw.theMaterial.Type__c == 'Water' && waterCount > waterMax){
            if(waterErrorAdded == false){ 
                errorString += '<BR>' + waterMax + ' Water'; 
                if(waterMax > 1) {
                    errorString += 's';
                }
                waterErrorAdded = true;
            }
            return true;
        }
        if(mw.theMaterial.Type__c == 'Other' && otherCount > otherMax){
            if(otherErrorAdded == false){
                errorString += '<BR> ' + otherMax + ' Other'; 
                if(otherMax > 1) {
                    errorString += 's';
                } 
                otherErrorAdded = true;
            }
            
            return true;
        }
        
        return false;
    }


    private String theType; 

    private void incrementTypeCounts(materialWrapper mw){
        theType = mw.theMaterial.Type__c;
        doIncrement(theType);
    }
    private void incrementTypeCounts(Mix_Material__c mm){
        theType = mm.Material__r.Type__c;
        doIncrement(theType);
    }

    private void doIncrement(String theType){
        if(theType == 'Admixture'){
            admixtureCount ++;
        } else if (theType == 'Cement'){
            cementCount ++;
        } else if(theType == 'SCM'){
            SCMCount ++;
        } else if(theType == 'Sand'){
            sandCount ++;
        } else if(theType == 'Coarse Aggregate'){
            aggCount ++;
        } else if(theType== 'Water'){
            waterCount ++;
        } else if(theType == 'Other'){
            otherCount ++;
        } else {
            system.debug('********ERROR: Material type not found: ' + theType);
        }
    }


    private void decrementTypeCounts(materialWrapper mw){
        theType = mw.theMaterial.Type__c;
        doDecrement(theType);
    }

    private void decrementTypeCounts(String type){
        doDecrement(type);
    }

    private void doDecrement(String theType){
        if(theType == 'Admixture'){
            admixtureCount --;
        } else if (theType == 'Cement'){
            cementCount --;
        } else if(theType == 'SCM'){
            SCMCount --;
        } else if(theType == 'Sand'){
            sandCount --;
        } else if(theType == 'Coarse Aggregate'){
            aggCount --;
        } else if(theType == 'Water'){
            waterCount --;
        } else if(theType == 'Other'){
            otherCount --;
        } else {
            system.debug('********ERROR: Material type not found: ' + theType);
        }

    }

    public void closePopup() {        
        displayPopup = false;    
    } 
        
    public void showPopup() {        
        displayPopup = true;    
    }


    // wrapper class for materials available for addition to the mix
    public class materialWrapper {
        public Material__c theMaterial {get; set;}
        public Boolean pickMe {get; set;}
        public Boolean typeOverLimit {get; set;}

        public materialWrapper(Material__c m){
            theMaterial = m;
            pickMe = false;
        }
    }


    // wrapper class for selected materials -- those already in the mix, and those selected for new inclusion
    public class mmWrapper {
        public Mix_Material__c theMM {get; set;}
        public String matName {get; set;}
        public String matType {get; set;}
        public String matTypeDisp {get; set;}
        public Boolean quantityRequired {get; set;}
        public Boolean unitRequired {get; set;}
        public Boolean moistureRequired {get; set;}
        public Boolean quantityError {get; set;}
        public Boolean unitError {get; set;}
        public Boolean moistureError {get; set;}

        public mmWrapper(Mix_Material__c mm, String mName, String mType, String SOM, Boolean isQuantityRequired, Boolean isUnitRequired, Boolean isMoistureRequired){
            theMM = mm;
            matName = mName.abbreviate(40);
            matType = mType; // including this as a separate property (instead of looking to theMM.Material__r.Type__c) because, unless the mix_material was already associated with the mix (and has thus been queried for instead of created here), there won't be a reference to the mix_material's material (i.e., Material__r)
            matTypeDisp = dispMap.get(mType) == null ? mType : dispMap.get(mType);
            mm.Quantity__c = mm.Quantity__c == null ? 0.00 : mm.Quantity__c;
            mm.Unit__c = mm.Unit__c == null ? getDefaultUnit(SOM, mType) : mm.Unit__c;
            quantityRequired = isQuantityRequired;
            unitRequired = isUnitRequired;
            moistureRequired = isMoistureRequired;
            if(moistureRequired){
                theMM.Moisture__c = mm.Moisture__c == null ? 0 : mm.Moisture__c;
            }

        }

        // used for transforming any values that are too long to display in the table 
        private map<String, String> dispMap = new map<String, String>{'Coarse Aggregate' => 'Coarse Agg'};

        // used to default the mm's unit based on the mix's system-of-measure and the material's type (solid or liquid)
        private String getDefaultUnit(String SOM, String mType){
            if(SOM == 'Metric (SI)'){
                if(mType == 'Sand' || mType == 'Coarse Aggregate' || mType == 'Cement' || mType == 'SCM' || mType == 'Other'){
                    return 'kgs';
                } else if (mType != 'Admixture') {
                    return 'liters';
                }
                return null;
            } else {
                if(mType == 'Sand' || mType == 'Coarse Aggregate' || mType == 'Cement' || mType == 'SCM' || mType == 'Other'){
                    return 'lbs';
                } else if (mType != 'Admixture') {
                    return 'gallons';
                }
                return null;
            }
        }

    }


}