@IsTest
private class TestAwardQuoteController {
	public static final Id PRICEBOOK_ID = Test.getStandardPricebookId();
	static final List<String> PROFILE_NAMES = Label.PartnerUserProfileNames.split(',');

	@TestSetup
	static void setup(){
		Id profileId = [SELECT Id FROM Profile WHERE Name = :PROFILE_NAMES[0] LIMIT 1].Id;
		List<Account> accountList = new List<Account>();
		Account acc1 = new Account(Name = 'Account1');
		accountList.add(acc1);
		insert accountList;

		List<Opportunity> oppList = new List<Opportunity>();
		Opportunity opp1 = new Opportunity(Name = 'Opportunity', StageName = 'Quote', CloseDate = Date.today()+5);
		//Opportunity opp2 = new Opportunity(Name = 'Opportunity1',AccountId = acc1.id, StageName = 'Quote', CloseDate = Date.today()+5);
		oppList.add(opp1);
		//oppList.add(opp2);
		insert oppList;

		List<Contact> contactList = new List<Contact>();
		Contact cont1 = new Contact(LastName = 'lastName', AccountId = acc1.Id);
		contactList.add(cont1);
		insert contactList;

		Apttus_Proposal__Proposal__c testProposal = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Opportunity__c = opp1.Id);
		insert testProposal;

		Pricebook2 pb = new Pricebook2(Name = 'Standard Price Book', Description = 'Price Book Products', IsActive = true);
		insert pb;

		List<Product2> pList = new List<Product2>();
		Product2 prod1 = new Product2(Name = 'Membrane Custom', Family = 'Best Practices', IsActive = true,
				APTS_Roofing_Description__c = 'Test 1', NOAActive__c = true);
		Product2 prod2 = new Product2(Name = 'Warranty', Family = 'Best Practices', IsActive = true,
				APTS_Roofing_Description__c = 'Test 2', NOAActive__c = true);
		Product2 prod3 = new Product2(Name = 'Warranty Extension', Family = 'Best Practices', IsActive = true,
				APTS_Roofing_Description__c = 'Test 3', NOAActive__c = true);
		Product2 prod4 = new Product2(Name = 'DECK', Family = 'Best Practices', IsActive = true,
				APTS_Roofing_Description__c = 'Test 4', NOAActive__c = true);

		plist.add(prod1);
		plist.add(prod2);
		plist.add(prod3);
		plist.add(prod4);
		insert pList;

		List<PricebookEntry> peList = new List<PricebookEntry>();
		for(Product2 p :pList){
			PricebookEntry pentry1 = new PricebookEntry(Pricebook2Id = PRICEBOOK_ID,Product2Id = p.id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
			PricebookEntry pentry2 = new PricebookEntry(Pricebook2Id = pb.Id,Product2Id = p.id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
			peList.add(pentry1);
			peList.add(pentry2);
		}
		insert peList;

		Apttus_Config2__ClassificationName__c testCategory = new Apttus_Config2__ClassificationName__c(Name = 'TestCategory',Apttus_Config2__HierarchyLabel__c = 'TestLabel');
		insert testCategory;
		Apttus_Config2__ClassificationHierarchy__c testClassifiation = new Apttus_Config2__ClassificationHierarchy__c(Name = 'TestClassification',Apttus_Config2__HierarchyId__c = testCategory.Id,Apttus_Config2__Label__c = 'TestLabel');
		insert testClassifiation;

		List<Apttus_Config2__ProductClassification__c> pcList = new List<Apttus_Config2__ProductClassification__c>();
		for(Product2 p :pList){
			Apttus_Config2__ProductClassification__c prodClass = new Apttus_Config2__ProductClassification__c(Apttus_Config2__ClassificationId__c = testClassifiation.Id,Apttus_Config2__ProductId__c = p.Id);
			pcList.add(prodClass);
		}
		insert pcList;

		List<Apttus_Proposal__Proposal_Line_Item__c> ppList = new List<Apttus_Proposal__Proposal_Line_Item__c>();
		for(Product2 p :pList){
			Apttus_Proposal__Proposal_Line_Item__c proposalLineItem = new Apttus_Proposal__Proposal_Line_Item__c(
																				Apttus_Proposal__Product__c = p.Id,
																				Apttus_QPConfig__ClassificationId__c = testClassifiation.Id,
																				Apttus_Proposal__Proposal__c = testProposal.Id);
			pplist.add(proposalLineItem);
		}
		insert ppList;

		Applicator_Quote__c quote = new Applicator_Quote__c(Opportunity__c = opp1.Id,
											Account__c = acc1.Id,
											Contact__c = cont1.Id);
		insert quote;
	}

	@IsTest
	static void TestAwardQuoteController_Constructor_ValidParameters() {
		List<Opportunity> testOpps = [SELECT Id FROM Opportunity LIMIT 1];
		List<Applicator_Quote__c> applicatorQuotes = [SELECT Id FROM Applicator_Quote__c LIMIT 1];

		Test.setCurrentPageReference(new PageReference('/apex/NewOpportunityFirm'));
		System.currentPageReference().getParameters().put('id', testOpps[0].Id);
		System.currentPageReference().getParameters().put('qId', applicatorQuotes[0].Id);

		ApexPages.StandardController standardController = new ApexPages.StandardController(testOpps[0]);
		AwardQuoteController controller 				= new AwardQuoteController(standardController);

		system.assertEquals(controller.oppRecord.Id, testOpps[0].Id);
	}    

	@IsTest
	static void TestAwardQuoteController_GetApttusProposal_ValidParameters() {
		List<Opportunity> testOpps = [SELECT Id FROM Opportunity LIMIT 1];
		List<Applicator_Quote__c> applicatorQuotes = [SELECT Id FROM Applicator_Quote__c LIMIT 1];

		Test.setCurrentPageReference(new PageReference('/apex/NewOpportunityFirm'));
		System.currentPageReference().getParameters().put('id', testOpps[0].Id);
		System.currentPageReference().getParameters().put('qId', applicatorQuotes[0].Id);

		ApexPages.StandardController standardController = new ApexPages.StandardController(testOpps[0]);
		AwardQuoteController controller 				= new AwardQuoteController(standardController);
		controller.getApttusProposal();

		Apttus_Proposal__Proposal__c tempAPP = [SELECT Id, Name FROM Apttus_Proposal__Proposal__c WHERE Apttus_Proposal__Opportunity__c = :testOpps[0].Id];
		system.assertNotEquals(null, tempAPP.Id);   
	}    

	@IsTest
	static void TestAwardQuoteController_GetApttusProposalLineItems_ValidParameters() {
		List<Opportunity> testOpps = [SELECT Id FROM Opportunity LIMIT 1];
		List<Applicator_Quote__c> applicatorQuotes = [SELECT Id FROM Applicator_Quote__c LIMIT 1];

		Test.setCurrentPageReference(new PageReference('/apex/NewOpportunityFirm'));
		System.currentPageReference().getParameters().put('id', testOpps[0].Id);
		System.currentPageReference().getParameters().put('qId', applicatorQuotes[0].Id);

		ApexPages.StandardController standardController = new ApexPages.StandardController(testOpps[0]);
		AwardQuoteController controller 				= new AwardQuoteController(standardController);
		controller.getApttusProposal();

		Test.startTest();
		controller.getApttusProposalLineItems();
		Test.stopTest();

		Apttus_Proposal__Proposal__c tempAPP = [SELECT Id, Name FROM Apttus_Proposal__Proposal__c WHERE Apttus_Proposal__Opportunity__c = :testOpps[0].Id];
		List<Apttus_Proposal__Proposal_Line_Item__c> tempAPLI = [SELECT Id, Name, SystemID__c, Apttus_Proposal__Proposal__c,
				Apttus_QPConfig__ClassificationId__c, Apttus_QPConfig__ClassificationId__r.Name, Apttus_QPConfig__ClassificationId__r.Apttus_Config2__HierarchyId__c, Apttus_Proposal__Product__c 
				FROM Apttus_Proposal__Proposal_Line_Item__c 
				WHERE Apttus_Proposal__Proposal__c = :tempAPP.Id];
	  
		system.assertEquals(4, tempAPLI.size());   
		system.assertEquals(tempAPP.Id, tempAPLI[0].Apttus_Proposal__Proposal__c);   
	}      

	@IsTest
	static void TestAwardQuoteController_ConstructBuildingGroup_ValidParameters() {
		List<Opportunity> testOpps = [SELECT Id FROM Opportunity LIMIT 1];
		List<Applicator_Quote__c> applicatorQuotes = [SELECT Id FROM Applicator_Quote__c LIMIT 1];

		Test.setCurrentPageReference(new PageReference('/apex/NewOpportunityFirm'));
		System.currentPageReference().getParameters().put('id', testOpps[0].Id);
		System.currentPageReference().getParameters().put('qId', applicatorQuotes[0].Id);

		ApexPages.StandardController standardController = new ApexPages.StandardController(testOpps[0]);
		AwardQuoteController controller 				= new AwardQuoteController(standardController);
		controller.getApttusProposal();
		controller.getApttusProposalLineItems();
		controller.constructBuildingGroup();

		System.assertEquals(1, controller.apttusProposalWC.size());
	}       

	@IsTest
	static void TestAwardQuoteController_Save_ValidParameters() {
		List<Opportunity> testOpps = [SELECT Id FROM Opportunity LIMIT 1];
		List<Applicator_Quote__c> applicatorQuotes = [SELECT Id FROM Applicator_Quote__c LIMIT 1];

		Test.setCurrentPageReference(new PageReference('/apex/NewOpportunityFirm'));
		System.currentPageReference().getParameters().put('id', testOpps[0].Id);
		System.currentPageReference().getParameters().put('qId', applicatorQuotes[0].Id);
		List<Product2> pList = [SELECT Id FROM Product2 WHERE Name <> 'DECK' LIMIT 3];
		Account testAcc = [SELECT Id FROM Account LIMIT 1];

		//Opportunity oppty = new Opportunity(AccountId=testAcc.id, Name = 'Opportunity', StageName = 'Quote', CloseDate = Date.today()+5);
		//insert oppty;
		//Apttus_Proposal__Proposal__c testProposal = new Apttus_Proposal__Proposal__c(
		//											Apttus_Proposal__Opportunity__c = oppty.Id,
		//											Apttus_QPConfig__ConfigurationFinalizedDate__c = Datetime.now().addDays(-1));
		//insert testProposal;
		//Account testAccount = new Account(Name = 'testAcc', SAP_Roofing_Customer__c = true, SAP_Account_ID__c = '56');
		//insert testAccount;
		//Applicator_Quote__c quote = new Applicator_Quote__c(Account__c = testAccount.Id, 
		//		Opportunity__c = testOpps[0].Id,
		//		Quote_Apttus__c = testProposal.Id,
		//		Award_NOA__c = true);

		//insert quote;

		ApexPages.StandardController standardController = new ApexPages.StandardController(testOpps[0]);
		AwardQuoteController controller = new AwardQuoteController(standardController);
		controller.getApttusProposal();
		controller.getApttusProposalLineItems();
		controller.constructBuildingGroup();

		controller.apttusProposalWC[0].isSelected = true;
		controller.apttusProposalWC[0].selectedMembrane = pList[0].Id;
		controller.apttusProposalWC[0].selectedWarranty = pList[1].Id;
		controller.apttusProposalWC[0].selectedExtension = pList[2].Id;    
		controller.apttusProposalWC[0].buildingGroup.Building_Group_name__c = 'test';
		controller.apttusProposalWC[0].buildingGroup.Bldg_Sq_Footage__c = 12;

		System.assertEquals(1, controller.apttusProposalWC.size());        
		System.assertEquals(true, controller.apttusProposalWC[0].isSelected);
		System.assertEquals('test', controller.apttusProposalWC[0].buildingGroup.Building_Group_name__c);

		Test.startTest();
		controller.awardNOA();
		Test.stopTest();

		Building_Group__c tempBG = new Building_Group__c();
		for (Building_Group__c bg : [SELECT Id FROM Building_Group__c WHERE Opportunity__c = :testOpps[0].Id LIMIT 1]) {
			tempBG = bg;
		}

		Building_Group_Product__c tempBGP = new Building_Group_Product__c();
		for (Building_Group_Product__c bgp : [SELECT Id, Building_Group__c FROM Building_Group_Product__c WHERE Building_Group__c = :tempBG.Id LIMIT 1]) {
			tempBGP = bgp;
		}

		System.assertNotEquals(null, tempBG);
		System.assertEquals(tempBG.Id, tempBGP.Building_Group__c);
		System.assert(controller.applicatorQuote.Award_NOA__c, 'It should have updated the field Award_NOA__c.');
	}
}