public with sharing class EmailDocumentsController {

	public Id opportunityId {get;set;}
	public List<DocumentType> documentTypes {get;set;}

	public string emailToAddress {get;set;}
	public string emailSubject {get;set;}
	public string emailBody {get;set;}

	private Set<Id> uniqueParentIds {get;set;}
	private Set<Id> uniqueAttachmentIds {get;set;}

	public EmailDocumentsController() {
	}

	public void GetParentIds(){
		opportunityId 	= apexpages.currentpage().getparameters().get('id');
		uniqueParentIds = new Set<Id>();
		documentTypes 	= new List<DocumentType>();

		if(opportunityId != null){
			uniqueParentIds.add(opportunityId);

			for(Building_Group__c bg : [SELECT Id FROM Building_Group__c WHERE Opportunity__c = :opportunityId]){
				uniqueParentIds.add(bg.Id);
			}

			for(Inspection__c ins : [SELECT Id FROM Inspection__c WHERE Opportunity_Name__c = :opportunityId]){
				uniqueParentIds.add(ins.Id);
			}

			for(Warranty__c war : [SELECT Id FROM Warranty__c WHERE Opportunity__c = :opportunityId]){
				uniqueParentIds.add(war.Id);
			}		

			for (Attachment att : [SELECT Id, Name FROM Attachment WHERE parentId IN :uniqueParentIds]){
				documentTypes.add(new DocumentType(att.Id, att.Name));
			}
		}
	}

	public PageReference SendEmail(){
		User usr = [SELECT Id, Email, Name FROM User WHERE Id=:UserInfo.getUserId()];
		String attachmentNames;

		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(EmailToAddress.split(';'));
		mail.setSubject(EmailSubject);
		mail.setBccSender(true);
		mail.setUseSignature(true);
		mail.setHtmlBody(EmailBody);
		mail.setSenderDisplayName(usr.Name);		
		mail.setReplyTo(usr.Email);

		if(!documentTypes.isEmpty()){
			uniqueAttachmentIds = new Set<Id>();
			for (DocumentType dt : documentTypes){
				if (dt.isSelected){	
					uniqueAttachmentIds.add(dt.documentId);
				}
			}	
			
			List<Messaging.EmailFileAttachment> fileAttachments = new List<Messaging.EmailFileAttachment>();
			for (Attachment att : [SELECT Body, Name FROM attachment WHERE id IN :uniqueAttachmentIds]){
	   			Messaging.EmailFileAttachment fileAttachment = new Messaging.EmailFileAttachment();
	      		fileAttachment.setBody(att.Body);
	      		fileAttachment.setFileName(att.Name);
	      		fileAttachments.add(fileAttachment);

	      		attachmentNames += att.Name + ',';
			}		
			mail.setFileAttachments(fileAttachments);

			if(String.isNotBlank(attachmentNames)){
				attachmentNames = attachmentNames.removeEnd(',');
			}
		}

		Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

		if (r[0].isSuccess()){
			Task TaskObj 			= new Task(WhatId=opportunityId);
			TaskObj.Subject 		= 'Email: ' + EmailSubject;
			TaskObj.Type			= 'Email';
			TaskObj.ActivityDate 	= Date.today();
			TaskObj.Status 			= 'Completed';
			
			string sDescription = '';
			
			sDescription += 'To: ' + emailToAddress;
			sDescription += '\n\nSubject: ' + emailSubject;
			sDescription += '\n\nBody: ' + emailBody;
			sDescription += '\n\nAttachments: ' + attachmentNames;
			
			TaskObj.Description = sDescription;
			
			insert TaskObj;

			PageReference pageRef = new PageReference('/' + opportunityId);
			pageRef.setRedirect(true);
       		return pageRef;

       		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Email Sent!'));
		}		
		
		return null;
	}

	public PageReference Cancel(){
		if(opportunityId != null){
			PageReference pageRef = new PageReference('/' + opportunityId);
			pageRef.setRedirect(true);
	   		return pageRef;
   		}
   		else{
			PageReference pageRef = new PageReference('/');
			pageRef.setRedirect(true);
	   		return pageRef;
   		}
	}

}