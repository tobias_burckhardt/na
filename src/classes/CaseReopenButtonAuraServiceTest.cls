@isTest
private class CaseReopenButtonAuraServiceTest {
	
	@isTest 
	static void updateCaseTest() {
		Account acc = new Account(Name = 'TestAccount');
		insert acc;
		Case cas = new Case(AccountId = acc.Id, 
							Status = 'Closed',
							Priority = 'High',
							Master_Category__c = 'Account Administration',
							Subject = 'For testing purposes',
							Description = 'Testing is always necessary',
							Due_Date__c = Datetime.newInstance(Date.today(), Time.newInstance(10, 5, 10, 20)),
							Reason = 'To test aura service',
							Solution__c = 'Code coverage has to be above 75%');
		insert cas;
		Test.startTest();
			CaseReopenButtonAuraService.updateCase(cas.Id);
		Test.stopTest();

		List<Case> updatedCases = [SELECT Id, Status 
								   FROM Case 
								   WHERE Id = :cas.Id];
		System.assertEquals('New', updatedCases[0].Status, 'The status of the case after the update method is fired must be new');	

	}
	
}