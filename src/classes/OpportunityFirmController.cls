public with sharing class OpportunityFirmController {

	public ContactSearchWrapper searchContact {get;set;}
	public List<Contact> foundContacts {get;set;}
	public Id selectedContactId {get;set;}
	public Contact selectedContact {get;set;}
	public Contact newContactToInsert {get;set;}
	public Opportunity_Firm__c newOpportunityFirmToInsert {get;set;}
	public Id opportunityId {get;set;}
	public Boolean showList {get;set;}
	public Boolean showSelected {get;set;}
	public Boolean showNew {get;set;}

	public OpportunityFirmController() {
		opportunityId 		= apexpages.currentpage().getparameters().get('id');

		searchContact 		= new ContactSearchWrapper();
		foundContacts 		= new List<Contact>();
		newContactToInsert 	= new Contact();
		showList 			= false;
		showSelected 		= false;
		showNew 			= false;
	}

	public void SearchContacts(){
		showList 		= true;
		showSelected 	= false;
		showNew 		= false;
		foundContacts = new List<Contact>();

		String queryStr = 'SELECT Id, FirstName, LastName, Phone, Email, MailingStreet, MailingCity, MailingState, MailingPostalCode ';
		queryStr += 'FROM Contact ';
		queryStr += 'WHERE FirstName LIKE \'' + '%' + searchContact.firstName + '%' + '\' ';
		queryStr += 'OR LastName LIKE \'' + '%' + searchContact.lastName + '%' + '\' ';
		queryStr += 'OR Phone LIKE \'' + '%' + searchContact.phone + '%' + '\' ';
		queryStr += 'OR Email LIKE \'' + '%' + searchContact.email + '%' + '\' ';
		queryStr += 'OR MailingStreet LIKE \'' + '%' + searchContact.street + '%' + '\' ';

		foundContacts = Database.query(queryStr);		

		if(foundContacts.isEmpty()){
			showList 		= false;
			showSelected 	= false;
			showNew 		= true;
			NewContact();
		}
	}

	public void SelectFoundContact(){
		showList 		= false;
		showSelected 	= true;
		showNew 		= false;
		newOpportunityFirmToInsert	= new Opportunity_Firm__c();
		selectedContact 			= new Contact();
		selectedContact = [SELECT Id, FirstName, LastName, Phone, Email, MailingStreet, MailingCity, MailingState, MailingPostalCode FROM Contact WHERE Id = :selectedContactId];
	}

	public pageReference SaveFoundContact(){
		try{
			newOpportunityFirmToInsert.Contact__c 			= selectedContactId;
			newOpportunityFirmToInsert.Opportunity__c 		= opportunityId;
			insert newOpportunityFirmToInsert;

			PageReference redirect = new PageReference('/' + newOpportunityFirmToInsert.id);
	        redirect.setRedirect(true);
	        return redirect;			
		}
		catch(DmlException dmle){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'DmlException: ' + dmle));
		}
		return null;
	}

	public void NewContact(){
		newOpportunityFirmToInsert 			= new Opportunity_Firm__c();
		newContactToInsert 					= new Contact();
		newContactToInsert.FirstName 		= searchContact.firstName;		
		newContactToInsert.LastName 		= searchContact.lastName;
		newContactToInsert.Phone 			= searchContact.phone;
		newContactToInsert.Email 			= searchContact.email;
		newContactToInsert.MailingStreet 	= searchContact.street;
	}

	public pageReference SaveNewContact(){
		try{
			insert newContactToInsert;

			newOpportunityFirmToInsert.Contact__c 			= newContactToInsert.Id;
			newOpportunityFirmToInsert.Opportunity__c 		= opportunityId;
			insert newOpportunityFirmToInsert;

			PageReference redirect = new PageReference('/' + newOpportunityFirmToInsert.id);
	        redirect.setRedirect(true);
	        return redirect;			
		}
		catch(DmlException dmle){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'DmlException: ' + dmle));
		}
		return null;
	}

	public pageReference Cancel(){
		PageReference redirect = new PageReference('/' + opportunityId);
        redirect.setRedirect(true);
        return redirect;			
	}


	public class ContactSearchWrapper{
		public String firstName {get;set;}
		public String lastName {get;set;}
		public String phone {get;set;}
		public String email {get;set;}
		public String street {get;set;}
		public String city {get;set;}
		public String state {get;set;}
		public String postalCode {get;set;}

		public ContactSearchWrapper(){
			this.firstName 	= '';
			this.lastName 	= '';
			this.phone 		= '';
			this.email 		= '';
			this.street 	= '';
			this.city 		= '';			
			this.state 		= '';			
			this.postalCode	= '';			
		}
	}
}