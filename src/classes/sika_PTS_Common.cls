public without sharing class sika_PTS_Common {

    // Sales Rep Profile Names
    public static set<String> salesRepProfiles = new set<String>{'PTS Sales Rep',
                                                                 'Sika Sales User',
                                                                 'SikaExternalRep'};                                                   
    // PTS Sales Rep AccountTeamMember Role name
    public static String PTSSalesRepATMRole = 'PTS Sales';
    
    // SysAdmin UserRole name
    public static String adminUserRoleName = 'Sika Admin';

    // PTS User Profile name (authenticated users with PTS access via a Communities license)
    public static String PTSUserProfileName = 'PTS Partner Community';

    // PTS User Profile Id (based on the above)
    private static Id sccId;
    public static Id PTSUserProfileId(){
        if(sccId == null){
            Id theId = [SELECT Name, Id FROM Profile WHERE Name = :Sika_PTS_Common.PTSUserProfileName Limit 1].Id;    
            sccId = theId;
        }
        return sccId;
    }

    // Guest (anonymous) user profile ID
    public static Id guestUserProfileID(){
        Id guestProfileId = [SELECT Id, UserType from User WHERE UserType = 'GUEST' Limit 1].Id;
        return guestProfileId;
    }
}