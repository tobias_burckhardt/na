@isTest
private class sika_SCCZoneSupportHomePageCTRLtest
{
	@isTest
	static void itShould() {  // Display all Cases for the current user, sorted by last updated (desc)
	
	// Create an SCC Zone user and his corresponding account
	map<string,sobject> testMap = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
	Account testAccount = (Account) testMap.get('theAccount');
	User theSCCZoneUser = (User) testMap.get('SCCZoneUser');

	map<String, String> paramsMap1 = new map<String, String>{'subject' => 'test case 1',
															'ownerId' => theSCCZoneUser.Id,
															'dueDateYear' => '2015', 
															'dueDateMonth' => '1', 
															'dueDateDay' => '20'};
	map<String, String> paramsMap2 = new map<String, String>{'subject' => 'test case 2',
															'ownerId' => theSCCZoneUser.Id,
															'dueDateYear' => '2015', 
															'dueDateMonth' => '1', 
															'dueDateDay' => '20'};
	map<String, String> paramsMap3 = new map<String, String>{'subject' => 'test case 3',
															'ownerId' => theSCCZoneUser.Id,
															'dueDateYear' => '2015', 
															'dueDateMonth' => '1', 
															'dueDateDay' => '20'};
	// set page we'll be testing.	
	PageReference pageRef = Page.sika_SCCZoneSupportHomePage;
	Test.setCurrentPage(pageRef);

	Test.startTest();

		// As the SCC Zone user, create & insert three cases one at a time 
		system.runAs(theSCCZoneUser){
			Case theCase1 = sika_SCCZoneTestFactory.createCase(paramsMap1);
			Case theCase2 = sika_SCCZoneTestFactory.createCase(paramsMap2);
			Case theCase3 = sika_SCCZoneTestFactory.createCase(paramsMap3);

		theCase1.Project_Building_Name__c = 'Project Building Name';
		theCase1.Master_Category__c = 'Account Administration';
		theCase1.Category__c = 'Change Employee Role';

		theCase2.Project_Building_Name__c = 'Project Building Name';
		theCase2.Master_Category__c = 'Account Administration';
		theCase2.Category__c = 'Change Employee Role';

		theCase3.Project_Building_Name__c = 'Project Building Name';
		theCase3.Master_Category__c = 'Account Administration';
		theCase3.Category__c = 'Change Employee Role';
    		insert(theCase1);
			insert(theCase2);
			insert(theCase3);

			// instantiate the page controller
			sika_SCCZoneSupportHomePageCTRL ctrl = new sika_SCCZoneSupportHomePageCTRL();

			// confirm that the page loaded. (Didn't redirect to an error page)
			system.assertEquals(pageRef.getURL(), ApexPages.CurrentPage().getURL());
		
			// Confirm that the three cases are returned by the controller in the order in which they were created.
			list<Case> theCases = ctrl.getCases();

			system.assert(theCases[0].lastModifiedDate >= theCases[1].lastModifiedDate && theCases[1].lastModifiedDate >= theCases[2].lastModifiedDate);
		
			// update the case with the oldest lastModified date
			theCases[2].Subject = 'test case 3 updated';

			update(theCases[2]);

			// create a new instance of the page controller
			ctrl = new sika_SCCZoneSupportHomePageCTRL();

			// confirm that the case we modified is now displayed (returned) ahead of the other two
			theCases = ctrl.getCases();

			system.assertEquals(theCases[0].subject, 'test case 3 updated');
		}

	Test.stopTest();

	}

	@isTest static void testLoadAction(){
        User guestUser = sika_SCCZoneTestFactory.createGuestUser();
        insert(guestUser);

        sika_SCCZoneSupportHomePageCTRL ctrl;
        PageReference p;

        System.runAs(guestUser){
            ctrl = new sika_SCCZoneSupportHomePageCTRL();
            p = ctrl.loadAction();
            system.assert(String.valueOf(p).contains('/SCCZone/sika_SCCZoneLoginPage'));
        }
    }
}