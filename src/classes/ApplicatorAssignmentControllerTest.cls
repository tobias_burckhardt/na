@isTest
private class ApplicatorAssignmentControllerTest {
    
    @isTest
    public static void testGenerate_noPairContact() {
	
    	// Account without contacts
        Account acc = 
            (Account)TestFactory.createSObject(new Account(name='Test Acc'), true);    	    
        
        // Project
        Project__c proj = 
            (Project__c)TestFactory.createSObject(new Project__c(name='Proposal'), true);

        Apttus_Config2__PriceList__c pric = new Apttus_Config2__PriceList__c ();
        insert pric;

        Opportunity opp = 
            (Opportunity)TestFactory.createSObject(new Opportunity(name='Oppa gangman style', Project__c = proj.Id, Price_List__c = pric.Id, Amount = 5, Roofing_Area_SqFt__c = 5, Estimated_Ship_Date__c = Date.today()), true);

        // Apttus_Proposal__Proposal__c
        Apttus_Proposal__Proposal__c aPP = 
            (Apttus_Proposal__Proposal__c)TestFactory.createSObject(
                new Apttus_Proposal__Proposal__c(Apttus_Proposal__Opportunity__c = opp.Id), true);        
        
        Test.setCurrentPageReference(new PageReference('ApplicatorAssignment')); 
		System.currentPageReference().getParameters().put('id', aPP.id);
        ApplicatorAssignmentController cont = new ApplicatorAssignmentController();
        
        // smoke test
        cont.cancel();
        // end
        
        System.currentPageReference().getParameters().put('jdwIndex', '0');
        cont.deleteRow();
        
        cont.generate();
        ApexPages.Message message = ApexPages.getMessages()[0]; 
        system.assertEquals(ApplicatorAssignmentController.AT_LEAST_ONE_PAIR, message.getSummary());

        
        // so we add account, but without contacts
		cont.addRow();
        cont.junctionDataWrappers[0].acAQ.Account__c = acc.Id;
        system.assertEquals(1, cont.junctionDataWrappers.size());
        
        cont.generate();
        message = ApexPages.getMessages()[1]; 
        system.assertEquals(ApplicatorAssignmentController.ACCOUNT_CONTACT_PAIR, message.getSummary());
         
    }
    
    @isTest
    public static void testGenerate_dynamicContactPickList() {
    	// Account with 2 contacts
        Account acc = 
            (Account)TestFactory.createSObject(new Account(name='Test Acc'), true); 
        
        Contact cont1 = 
            (Contact)TestFactory.createSObject(new Contact(lastname='Cont 1', accountId=acc.Id), true); 
        Contact cont2 = 
            (Contact)TestFactory.createSObject(new Contact(lastname='Cont 2', accountId=acc.Id), true);        
        
        // Project
        Project__c proj = 
            (Project__c)TestFactory.createSObject(new Project__c(name='Proposal'), true);

        Apttus_Config2__PriceList__c pric = new Apttus_Config2__PriceList__c ();
        insert pric;

        Opportunity opp = 
            (Opportunity)TestFactory.createSObject(new Opportunity(name='Oppa gangman style', Project__c = proj.Id, Price_List__c = pric.Id, Amount = 5, Roofing_Area_SqFt__c = 5, Estimated_Ship_Date__c = Date.today()), true);

        // Apttus_Proposal__Proposal__c
        Apttus_Proposal__Proposal__c aPP = 
            (Apttus_Proposal__Proposal__c)TestFactory.createSObject(
                new Apttus_Proposal__Proposal__c(Apttus_Proposal__Opportunity__c = opp.Id), true);        
        
        Test.setCurrentPageReference(new PageReference('ApplicatorAssignment')); 
		System.currentPageReference().getParameters().put('id', aPP.id);
        ApplicatorAssignmentController cont = new ApplicatorAssignmentController();    
        
        cont.junctionDataWrappers[0].acAQ.Account__c = acc.Id;
        System.currentPageReference().getParameters().put('jdwIndex', '0');        
        cont.updateContactSelectList();
        
        //System.assertEquals(2, cont.junctionDataWrappers[0].contactOptions.size());
    } 
    
    @isTest
    public static void testGenerate() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {        
        	// Account with 2 contacts
            Account acc = 
                (Account)TestFactory.createSObject(new Account(name='Test Acc', SAP_Roofing_Customer__c=true), true); 
            
            Contact cont1 = 
                (Contact)TestFactory.createSObject(new Contact(lastname='Cont 1', accountId=acc.Id), true); 
            Contact cont2 = 
                (Contact)TestFactory.createSObject(new Contact(lastname='Cont 2', accountId=acc.Id), true);   
            
            Id p = [select id from profile where name='Partner Community User'].id;
            
           	User usr = 
                (User)TestFactory.createSObject(new User(ProfileID = p, ContactId = cont1.Id), true);        
            
                    
            // Project
            Project__c proj = 
                (Project__c)TestFactory.createSObject(new Project__c(name='Proposal'), true);

            Apttus_Config2__PriceList__c pric = new Apttus_Config2__PriceList__c ();
            insert pric;

            Opportunity opp = 
                (Opportunity)TestFactory.createSObject(new Opportunity(name='Oppa gangman style', Project__c = proj.Id, Price_List__c = pric.Id, Amount = 5, Roofing_Area_SqFt__c = 5, Estimated_Ship_Date__c = Date.today()), true);

            // Apttus_Proposal__Proposal__c
            Apttus_Proposal__Proposal__c aPP = 
                (Apttus_Proposal__Proposal__c)TestFactory.createSObject(
                    new Apttus_Proposal__Proposal__c(Apttus_Proposal__Opportunity__c = opp.Id, Apttus_QPConfig__ConfigurationFinalizedDate__c = date.today()), true);        
            
            Test.setCurrentPageReference(new PageReference('ApplicatorAssignment')); 
    		System.currentPageReference().getParameters().put('id', aPP.id);
            ApplicatorAssignmentController cont = new ApplicatorAssignmentController();    
            
            //Quote_Apttus__c
            //Apttus_Proposal__Proposal__c tempAPP = new Apttus_Proposal__Proposal__c(id=cont.apqId, Apttus_QPConfig__ConfigurationFinalizedDate__c = date.today());
            //update tempAPP;

            cont.junctionDataWrappers[0].acAQ.Account__c = acc.Id;
            cont.junctionDataWrappers[0].contactId = cont1.Id;

      //      cont.generate();
      //      //Quote_Apttus__r.Apttus_QPConfig__ConfigurationFinalizedDate__c

      //      // Applicator_Quote__c generated
     	//	Applicator_Quote__c apq = [select id from Applicator_Quote__c where Quote_Apttus__c = :cont.apqId LIMIT 1];
            
      //      // one Con_AQ__c inserted
      //      System.assertEquals(1, [select id from Con_AQ__c where AQ__c = :apq.Id].size());

      //      // one Acc_AQ__c inserted
      //      System.assertEquals(1, [select id from Acc_AQ__c where AQ__c = :apq.Id].size());

      //      // one Opp_AQ__c inserted
      //      System.assertEquals(1, [select id from Opp_AQ__c where AQ__c = :apq.Id].size());

      //      // one Pro_AQ__c inserted
      //      System.assertEquals(1, [select id from Pro_AQ__c where AQ__c = :apq.Id].size());   
            
      //      // sharing records(team, owner, manual, rule) for only one community user contact
    		//system.debug('aha ' + [select id, OpportunityId, RowCause, OpportunityAccessLevel, UserOrGroupId from OpportunityShare where OpportunityId = :cont.apqOpportunity]);
            //System.assertEquals(3, [select id from OpportunityShare where OpportunityId = :cont.apqOpportunity].size());
        }
    }
    
    /*@isTest
    public static void testCreateSharingRecord() {
        tested in previous test
    } */
    

}