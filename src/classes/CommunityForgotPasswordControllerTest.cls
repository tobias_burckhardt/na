@IsTest
private class CommunityForgotPasswordControllerTest {

    public static testMethod void TestCommunityForgotPasswordController(){
 
        CommunityForgotPasswordController ctr = new CommunityForgotPasswordController();
        ctr.username = 'skumar@astreait.com';
        ctr.success = 'true';
        ctr.caseEmail = 'skumar@astreait.com';
        Test.setCurrentPageReference(new PageReference('Page.CommunityForgotPasswordController'));         
        System.currentPageReference().getParameters().put('success', 'true');
    
        ctr.submitRequest();        
        ctr.closePopup();
        ctr.showPopup();
        ctr.submitCase();
        ctr.validateForm();
    }
}