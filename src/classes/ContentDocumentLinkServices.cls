public class ContentDocumentLinkServices {

    public static final SObjectType REQUEST_FOR_APPROVAL_SOBJECT_TYPE =
        Schema.Request_for_Project_Approval__c.SObjectType;

    /**
     * Filters ContentDocumentLink records related to a Request_for_Project_Approval__c.
     * 
     * @param  documents the list of ContentDocumentLink records to be filtered
     * @return           the list of filtered ContentDocumentLink records
     */
    public static List<ContentDocumentLink> filterRPARelated(List<ContentDocumentLink> links) {

        List<ContentDocumentLink> filteredLinks = new List<ContentDocumentLink>();

        for (ContentDocumentLink link : links) {
            if (link.LinkedEntityId.getSObjectType() == REQUEST_FOR_APPROVAL_SOBJECT_TYPE) {
                filteredLinks.add(link);
            }
        }

        return filteredLinks;
    }

    public static Map<Id, Request_for_Project_Approval__c> queryRPAs(
            List<ContentDocumentLink> links) {
        Set<Id> approvalIds = Pluck.ids('LinkedEntityId', links);

        List<Request_for_Project_Approval__c> approvals = [
            SELECT Project_Manager__r.SFDC_User_Account__c, Local_Controller__r.SFDC_User_Account__c
            FROM Request_for_Project_Approval__c
            WHERE Id IN :approvalIds
        ];

        return new Map<Id, Request_for_Project_Approval__c>(approvals);
    }

    public static void createNewSharingForRPA(List<ContentDocumentLink> links,
            Map<Id, Request_for_Project_Approval__c> approvalsById) {
        List<ContentDocumentLink> linksToInsert = new List<ContentDocumentLink>();

        for (ContentDocumentLink link : links) {
            Request_for_Project_Approval__c approval = approvalsById.get(link.LinkedEntityId);
            Id userPMId = approval.Project_Manager__r.SFDC_User_Account__c;
            Id userLCId = approval.Local_Controller__r.SFDC_User_Account__c;
            Id docId = link.ContentDocumentId;
            if(userPMId != UserInfo.getUserId()){
                linksToInsert.add(createContentDocumentLinkForRPA(docId, userPMId));
            }
            if(userLCId != UserInfo.getUserId() && userPMId != userLCId){
               linksToInsert.add(createContentDocumentLinkForRPA(docId, userLCId));
            }
        }

        Map<Id, List<ContentDocumentLink>> linksByDocumentId =
            GroupBy.ids('ContentDocumentId', links);

        insertNewContentDocumentLinks(linksToInsert, linksByDocumentId);
    }

    private static ContentDocumentLink createContentDocumentLinkForRPA(Id contentDocumentId,
            Id userId) {
        return new ContentDocumentLink(
            ContentDocumentId = contentDocumentId,
            LinkedEntityId = userId,
            ShareType = 'C'
        );
    }

    private static void insertNewContentDocumentLinks(List<ContentDocumentLink> links,
            Map<Id, List<ContentDocumentLink>> originalLinksByDocumentId) {
        try {
            insert links;
        } catch(DmlException e) {
            String errorTemplate = 'Cannot insert new ContentDocumentLink record: {0}';

            for (Integer i = 0; i < e.getNumDml(); i++) {
                String errorMessage =
                    String.format(errorTemplate, new List<String>{e.getDmlMessage(i)});
                ContentDocumentLink errorLink = links.get(e.getDmlIndex(i));
                List<ContentDocumentLink> originalLinks =
                    originalLinksByDocumentId.get(errorLink.ContentDocumentId);
                for (ContentDocumentLink link : originalLinks) {
                    link.addError(errorMessage);
                }
            }
        }
    }
}