/*
@description Content Documents for unauthenticated users on a force.com site
@URL https://developer.salesforce.com/docs/atlas.en-us.sfFieldRef.meta/sfFieldRef/salesforce_field_reference_ContentVersion.htm
@URL http://training.handsonconnect.org/m/site_customization/l/35011-getting-the-urls-for-documents-you-wish-to-make-available-for-download-on-the-public-site
@URL https://developer.salesforce.com/docs/atlas.en-us.salesforce_platform_portal_implementation_guide.meta/salesforce_platform_portal_implementation_guide/sites_limits.htm

@Note Add site guest user to a public group, add public group to document/contentlibrary, adjust relative URL
@Note 40gb limit for production, 1gb sandbox per 24hrs.
@Note This solution is probably not perfect for Enterprise.  The limit is small and this is unsupported functionality from Salesforce
*/
public without sharing class ContentController {

	public List<ContentVersion> contentFiles {get;set;}
	public List<ContentDistribution> contentDistributions {get;set;}
	public List<Document> documents {get;set;}
	Public Id userId {get;set;}
	Public string testBlob {get;set;}

	public ContentController() {
		userId 					= UserInfo.getUserId();
		testBlob				= string.valueOf([SELECT Id, Body FROM Document LIMIT 1]);
		contentFiles 			= ContentController.GetContentFiles();
		documents 				= ContentController.GetDocuments();
		//contentDistributions 	= ContentController.GetContentDistribution();		
	}

	public PageReference getContentURL() { 
		ContentVersion[] cv = [select id, ContentDocumentId from ContentVersion where contentdocumentid='069290000000TCW']; 

		return new PageReference('/' + cv[0].id); 
	}

	public static List<ContentVersion> GetContentFiles() {
		List<ContentVersion> results = new List<ContentVersion>();
		for(ContentVersion cv: [SELECT Id, 
			Checksum, ContentDocumentId, ContentLocation, ContentModifiedById, ContentModifiedDate, ContentSize, ContentUrl, 
			Description,  
			ExternalDataSourceId, ExternalDocumentInfo1, ExternalDocumentInfo2,
			FeaturedContentBoost, FeaturedContentDate, FileExtension, FileType, FirstPublishLocationId,
			IsDeleted, IsLatest, IsMajorVersion,
			Language,
			NegativeRatingCount,NetworkId,
			Origin, OwnerId,
			PathOnClient,PositiveRatingCount, PublishStatus,
			RatingCount, ReasonForChange,  
			SystemModstamp,
			TagCsv, Title,
			VersionData, VersionNumber
			FROM ContentVersion
			WHERE IsLatest = true]){
			results.add(cv);
		}

		if(!results.isEmpty()){
			return results;
		}
		return null;
	}

	public static List<Document> GetDocuments() {
		List<Document> results = new List<Document>();
		for(Document cv: [SELECT Id, 
			AuthorId,
			Body,BodyLength,
			ContentType, Description, DeveloperName, FolderId, 
			IsBodySearchable, IsDeleted, IsInternalUseOnly, IsPublic,
			Keywords,
			LastReferencedDate, LastViewedDate, 
			Name, NamespacePrefix,
			Type, 
			Url
			FROM Document]){
			results.add(cv);
		}

		if(!results.isEmpty()){
			return results;
		}
		return null;
	}		

	/*
	//This sucks, dont use
	public static List<ContentDistribution> GetContentDistribution() {
		List<ContentDistribution> results = new List<ContentDistribution>();
		for(ContentDistribution cv: [SELECT Id, 
			ContentDocumentId, ContentVersionId, OwnerId, RelatedRecordId, DistributionPublicUrl
			FROM ContentDistribution]){
			results.add(cv);
		}

		if(!results.isEmpty()){
			return results;
		}
		return null;
	}	
	*/

}