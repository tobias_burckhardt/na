/**
 * @author      Josep Vall-llobera <valnavjo_at_gmail.com>
 * @version     1.1.0
 * @since       03/02/2013
 */
public class SfdcZipSampleController {
 
    public static String zipFileName {get; set;}
    public static String zipContent {get; set;}
    public static String parentId {get;set;}
    
    static{
        
        parentId = '001290000065qkwAAA';
    }
    
 	@RemoteAction
    public static String uploadZip(String content,String fileName ) {
         
        Document doc = new Document();
        doc.Name = fileName;
        doc.ContentType = 'application/zip';
        doc.FolderId = UserInfo.getUserId();
        doc.Body = EncodingUtil.base64Decode(content);
         
        insert doc;
         
        return doc.Id;
    }
 	@RemoteAction
    public static List<AttachmentWrapper> getAttachments(String parent) {
        
        List<Attachment> attachments =  [select Id, ParentId, Name, ContentType, BodyLength, Body from Attachment WHERE ParentId = :parent limit 100];
        //List<Document> docs = [select Id, Name, Body From Document limit 100];
        
        List<AttachmentWrapper> wrappedAtts = new List<AttachmentWrapper>();
        
        /*for(Document d : docs){
            
            AttachmentWrapper attWrapper = new AttachmentWrapper();
        	attWrapper.attEncodedBody = EncodingUtil.base64Encode(d.body);
        	attWrapper.attName = d.Name;
            
            wrappedAtts.add(attWrapper);
        }*/
        
        
        for(Attachment a : attachments){
            
            AttachmentWrapper attWrapper = new AttachmentWrapper();
        	attWrapper.attEncodedBody = EncodingUtil.base64Encode(a.body);
        	attWrapper.attName = a.Name;
            
            wrappedAtts.add(attWrapper);
        }
        
        return wrappedAtts;
    }
     
    @RemoteAction
    public static AttachmentWrapper getAttachment(String attId) {
         
        Attachment att = [select Id, Name, ContentType, Body
                          from Attachment
                          where Id = :attId];
         
        AttachmentWrapper attWrapper = new AttachmentWrapper();
        attWrapper.attEncodedBody = EncodingUtil.base64Encode(att.body);
        attWrapper.attName = att.Name;
                           
        return attWrapper;
    }
     
    public class AttachmentWrapper {
        public String attEncodedBody {get; set;}
        public String attName {get; set;}
    }
}