@isTest
private class MixMaterialServiceTest {
    private static final String ABS = '5';
    private static final Integer QUANTITY = 1;
    private static final Decimal MOISTURE = 5.8;

    @TestSetup
    static void testSetup() {
        skipUnwantedMixMaterialTriggers();

        Material__c material = TestingUtils.createMaterial(false);
        material.Type__c = MixMaterialService.MATERIAL_TYPE_COARSE_AGGREGATE;
        material.Abs__c = ABS;
        material.Solid_Content__c = 0.25;
        insert material;

        Mix__c mix = TestingUtils.createMix(5, 'Test Mix', 0.5, 0.5, 0.5, false);
        mix.Water_Adjustment_kg__c = 5.15;
        insert mix;

        Mix_Material__c mixMaterial = TestingUtils.createMixMaterial(mix, material, false);
        mixMaterial.Moisture__c = MOISTURE;
        mixMaterial.Quantity__c = QUANTITY;
        mixMaterial.Unit__c = 'kgs';
        insert mixMaterial;
    }

    static testmethod void testUpdateMoistureAdjustmentInsert() {
        Mix_Material__c mixMaterial =
            [SELECT Moisture_Adjustment_2__c FROM Mix_Material__c WHERE Mix__r.Mix_Name__c = 'Test Mix' LIMIT 1];

        Decimal expected = QUANTITY * (Integer.valueOf(ABS) - MOISTURE) / 100;
        System.assertEquals(expected, mixMaterial.Moisture_Adjustment_2__c,
            'Moisture adjustment should be calculated correctly.');
    }

    static testmethod void testUpdateMoistureAdjustmentInsertUpdate() {
        skipUnwantedMixMaterialTriggers();

        Mix_Material__c mixMaterial =
            [SELECT Moisture_Adjustment_2__c FROM Mix_Material__c WHERE Mix__r.Mix_Name__c = 'Test Mix' LIMIT 1];

        mixMaterial.Quantity__c = 2;

        Test.startTest();
        update mixMaterial;
        Test.stopTest();

        mixMaterial = [SELECT Moisture_Adjustment_2__c FROM Mix_Material__c WHERE Mix__r.Mix_Name__c = 'Test Mix' LIMIT 1];

        Decimal expected = 2 * (Integer.valueOf(ABS) - MOISTURE) / 100;
        System.assertEquals(expected, mixMaterial.Moisture_Adjustment_2__c,
            'Moisture adjustment should be calculated correctly.');
    }

    private static void skipUnwantedMixMaterialTriggers() {
        Sika_SCCZoneMixMaterialTriggerHelper.bypassAccountTeamCheck = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassPopulateMaterialTypeCounts = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassSetSCMWeightAndUnit = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculatePasteVolume = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculateTotalSolidsVolumeLiters = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculateTotalMixVolumeLiters = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculateWaterAdjustment = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassPopulateMaterialTypeWeights = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassConcreteCurveCalc = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculateTotalMaterialCost = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassAddOverLimitErrors = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassMixOptimizationCheck = true;
    }
}