public with sharing class RequestForProjectApprovalServices {

	/**
	 * Finds the Current Approver Names for a list of Request for Project Approval based on Approval Processes
	 * If it can't find it from Approval Process, then it uses a field on the object
	 * @param rpaList A Set of Request for Project Approval Ids
	 * @return A Map of Request for Project Approval Ids to their respective Approval Process Approver Names
	 */
	public static Map<Id, String> findCurrentApprovers (Set<Id> rpaIds){
		Map<Id, String> rpaIdtoActorName = new Map<Id, String>();
		for(ProcessInstanceWorkitem process : [SELECT Id, OriginalActor.Name, Actor.Name, ProcessInstance.TargetObjectId 
			FROM ProcessInstanceWorkitem 
			WHERE ProcessInstance.TargetObjectId IN :rpaIds])
		{
			rpaIdtoActorName.put(process.ProcessInstance.TargetObjectId, process.Actor.Name);
		}

		return rpaIdtoActorName;
	}

    public static List<Request_for_Project_Approval__c> filterNewEmployee(
            List<Request_for_Project_Approval__c> newApprovals,
            Map<Id, Request_for_Project_Approval__c> oldApprovalsById) {
        List<Request_for_Project_Approval__c> filteredApprovals = new List<Request_for_Project_Approval__c>();
        for (Request_for_Project_Approval__c newApproval : newApprovals) {
            Request_for_Project_Approval__c oldApproval = oldApprovalsById.get(newApproval.Id);

            Boolean managerChanged =
                newApproval.Project_Manager__c != oldApproval.Project_Manager__c;
            Boolean controllerChanged =
                newApproval.Local_Controller__c != oldApproval.Local_Controller__c;

            if (managerChanged || controllerChanged) {
                filteredApprovals.add(newApproval);
            }
        }

        return filteredApprovals;
    }

    public static Map<Id, List<ContentDocumentLink>> retrieveRPADocumentLinks(
            List<Request_for_Project_Approval__c> approvals) {
        Set<Id> approvalIds = Pluck.ids(approvals);
        List<ContentDocumentLink> links = [
            SELECT ContentDocumentId, LinkedEntityId
            FROM ContentDocumentLink
            WHERE LinkedEntityId IN :approvalIds
        ];

        return GroupBy.ids('LinkedEntityId', links);
    }

    public static ContentDocumentLinkChanges retrieveContentDocumentLinks(
            List<Request_for_Project_Approval__c> filteredApprovals,
            Map<Id, Request_for_Project_Approval__c> oldApprovalsById,
            Map<Id, List<ContentDocumentLink>> rpaDocumentLinks) {

        Map<Id, Employee__c> employeesById =
            retrieveEmployeesById(filteredApprovals, oldApprovalsById);

        Map<Id, List<Id>> userIdsByDocId = new Map<Id, List<Id>>();
        List<ContentDocumentLink> newLinks = new List<ContentDocumentLink>();
        for (Request_for_Project_Approval__c approval : filteredApprovals) {
            Request_for_Project_Approval__c oldApproval = oldApprovalsById.get(approval.Id);
            if(rpaDocumentLinks.containsKey(approval.Id)){
                for (ContentDocumentLink rpaLink : rpaDocumentLinks.get(approval.Id)) {
                    Id documentId = rpaLink.ContentDocumentId;

                    List<Id> userIds = new List<Id>();
                    if (approval.Project_Manager__c != oldApproval.Project_Manager__c) {
                        Id oldUserId =
                            employeesById.get(oldApproval.Project_Manager__c).SFDC_User_Account__c;
                        Id newUserId =
                            employeesById.get(approval.Project_Manager__c).SFDC_User_Account__c;
                        userIds.add(oldUserId);
                        newLinks.add(new ContentDocumentLink(
                            ContentDocumentId = documentId,
                            LinkedEntityId = newUserId,
                            ShareType = 'C'
                        ));
                    }

                    if (approval.Local_Controller__c != oldApproval.Local_Controller__c) {
                        Id oldUserId =
                            employeesById.get(oldApproval.Local_Controller__c).SFDC_User_Account__c;
                        Id newUserId =
                            employeesById.get(approval.Local_Controller__c).SFDC_User_Account__c;
                        userIds.add(oldUserId);
                        newLinks.add(new ContentDocumentLink(
                            ContentDocumentId = documentId,
                            LinkedEntityId = newUserId,
                            ShareType = 'C'
                        ));
                    }

                    userIdsByDocId.put(rpaLink.ContentDocumentId, userIds);
                }
            }
        }

        List<ContentDocumentLink> oldLinks = userIdsByDocId.isEmpty() ?
            new List<ContentDocumentLink>() : retrieveOldUserLinks(userIdsByDocId);
        return new ContentDocumentLinkChanges(newLinks, oldLinks);
    }

    private static List<ContentDocumentLink> retrieveOldUserLinks(
            Map<Id, List<Id>> userIdsByDocId) {
        String userLinksQueryTemplate = 'SELECT Id FROM ContentDocumentLink WHERE {0}';
        String whereClause = buildWhereClauseForOldUserLinks(userIdsByDocId);
        String query = String.format(userLinksQueryTemplate, new List<String>{whereClause});

        return Database.query(query);
    }

    private static String buildWhereClauseForOldUserLinks(Map<Id, List<Id>> userIdsByDocId) {
        List<String> whereClauses = new List<String>();

        String template = '(ContentDocumentId=\'\'{0}\'\' AND LinkedEntityId IN ({1}))';
        for (Id docId : userIdsByDocId.keySet()) {
            String userIds = String.join(userIdsByDocId.get(docId), '\',\'');
            List<String> params = new List<String>{docId, '\'' + userIds + '\''};
            whereClauses.add(String.format(template, params));
        }

        String whereClause = String.join(whereClauses, 'OR');

        return whereClause;
    }

    private static Map<Id, Employee__c> retrieveEmployeesById(
            List<Request_for_Project_Approval__c> filteredApprovals,
            Map<Id, Request_for_Project_Approval__c> oldApprovalsById) {
        Set<Id> employeeIds = new Set<Id>();

        for (Request_for_Project_Approval__c approval : filteredApprovals) {
            Request_for_Project_Approval__c oldApproval = oldApprovalsById.get(approval.Id);

            if (approval.Project_Manager__c != oldApproval.Project_Manager__c) {
                employeeIds.add(oldApproval.Project_Manager__c);
                employeeIds.add(approval.Project_Manager__c);
            }

            if (approval.Local_Controller__c != oldApproval.Local_Controller__c) {
                employeeIds.add(oldApproval.Local_Controller__c);
                employeeIds.add(approval.Local_Controller__c);
            }
        }

        List<Employee__c> employees = employeeIds.isEmpty() ? new List<Employee__c>() :
            [SELECT SFDC_User_Account__c FROM Employee__c WHERE Id IN :employeeIds];

        return new Map<Id, Employee__c>(employees);
    }
}