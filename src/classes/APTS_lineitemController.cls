global  class APTS_lineitemController{

public id configId{get;set;}
public List<Apttus_Config2__LineItem__c> lineItemList {get;set;}

Public list<Apttus_Config2__LineItem__c> getListLine(){
  lineItemList = [Select id,Apttus_Config2__ProductId__c,Apttus_Config2__ProductId__r.Name,Apttus_Config2__ListPrice__c,APTS_Roofing_Description__c,APTS_NetAdjustment__c,Apttus_Config2__NetPrice__c,Apttus_CQApprov__Approval_Status__c from Apttus_Config2__LineItem__c where Apttus_Config2__ConfigurationId__c=:configId and APTS_CheckDiscount__c = true];
  
  return lineItemList ;
}


    global APTS_lineitemController(){
    
     
    
    }



}