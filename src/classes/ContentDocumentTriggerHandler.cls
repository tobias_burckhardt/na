public with sharing class ContentDocumentTriggerHandler
{
	
	public static Set<Id> validLibraryIds
	{
		get
		{
			if(validLibraryIds == NULL)
			{	
				validLibraryIds = new Set<Id>();
				Set<String> libraryNames = Pluck.Strings('name', Valid_Library__c.getAll().values());
				validLibraryIds = Pluck.Ids([Select Id From ContentWorkSpace Where Name In: libraryNames]);
			}
			return validLibraryIds;
		}
		set;
	}
	
	public static List<ContentDocument> filterContentDocuments_withValidLibraryIds(List<ContentDocument> contentDocs) {
		List<ContentDocument> filteredDocs = new List<ContentDocument>();
		for (ContentDocument contentDoc : contentDocs) {
			if (validLibraryIds.contains(contentDoc.ParentId)) {
				filteredDocs.add(contentDoc);
			}
		}
		return filteredDocs;
	}
	
	public static void contentDocumentRestrictDelete(List<ContentDocument> contentDocs)
	{
		for ( ContentDocument cd : contentDocs ) 
		{

        List<ContentDocumentLink> links = [SELECT LinkedEntityId FROM ContentDocumentLink WHERE ContentDocumentId = :cd.Id];
        Set<Id> linkIds = new Set<Id>();
        for(ContentDocumentLink cdl : links ) 
        {
            linkIds.add(cdl.LinkedEntityId );
        }
        
        //should only be one case
        List<Case> cases = [SELECT Id, ParentId, RecordType.DeveloperName from Case where Id In :linkIds];
        if( !cases.isEmpty()) 
	        {
	            if( RestrictedCaseRecordsUtil.isRestrictedCaseRecordType(cases[0].Id, cases[0].RecordType.DeveloperName) ) {
	                cd.addError(System.Label.Restricted_Object_Delete);
	            }
	        }
    	}
	}

	@future
	public static void createContentHeaders(Set<Id> newContentDocIds)
	{
		List<ContentDocument> newContentDocs = [Select Id, Title, ParentId from ContentDocument Where Id IN: newContentDocIds];
		newContentDocs = filterContentDocuments_withValidLibraryIds(newContentDocs); System.debug('filtered content docs' + newContentDocs);
		if(newContentDocs.isEmpty()) return;
		List<Content_Header__c> contentHeaders = new List<Content_Header__c>();
		for(ContentDocument cd: newContentDocs)
		{
			contentHeaders.add(createContentHeader(cd));
		}
		try
		{
			System.debug(' created content headers ' + contentHeaders);
			insert contentHeaders;
		}
		catch(DMLException dmx)
		{
			Map<Id,ContentDocument> newMap = new Map<Id,ContentDocument>(newContentDocs);
			for(Integer i=0; i<dmx.getNumDml(); i++)
			{
				Integer failedIndex = dmx.getDmlIndex(i);
				String	failedMessage = dmx.getDmlMessage(i);
				newMap.get(contentHeaders[failedIndex].Content_Document__c).addError(failedMessage);
			}
		}
	}

	public static Content_Header__c createContentHeader(ContentDocument contentDoc)
	{
		Content_Header__c newContentHeader = new Content_Header__c(Content_Document__c = contentDoc.Id, Name = contentDoc.title);
		return newContentHeader;
	}


	public static void deleteContentHeaders(List<ContentDocument> contentDocs)
	{
		Set<Id> contentDocIds = Pluck.Ids(contentDocs);
		List<Content_Header__c> contentHeaders = [Select Id From Content_Header__c Where Content_Document__c IN: contentDocIds];

		try
		{
			delete contentHeaders;
		}

		catch(DMLException dmx)
		{
			Map<Id,ContentDocument> newMap = new Map<Id,ContentDocument>(contentDocs);
			for(Integer i=0; i<dmx.getNumDml(); i++)
			{
				Integer failedIndex = dmx.getDmlIndex(i);
				String	failedMessage = dmx.getDmlMessage(i);
				newMap.get(contentHeaders[failedIndex].Content_Document__c).addError(failedMessage);
			}
		}
	}
}