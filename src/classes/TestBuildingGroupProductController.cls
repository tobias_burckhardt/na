@isTest
private class TestBuildingGroupProductController {
	public static final Id PRICEBOOK_ID = Test.getStandardPricebookId();

    static List<Account> testAcc;
    static List<Contact> testCon;
    static List<Opportunity> testOpp;
    static Building_Group__c testBuildingGroup;
    static Apttus_Config2__ClassificationName__c testCategory;
    static Apttus_Config2__ClassificationHierarchy__c testClassifiation;
    static Apttus_Config2__ProductClassification__c testProductClassification;

    static void setup(){
        testAcc = TestUtilities.createAccounts(1, true);
        testCon = TestUtilities.createContacts(testAcc, 1, true);
        testOpp = TestUtilities.createOpportunities(testAcc, 1, true);
        Apttus_Proposal__Proposal__c testProposal = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Opportunity__c=testOpp[0].Id);
        insert testProposal;

        testBuildingGroup = new Building_Group__c(Opportunity__c=testOpp[0].Id);
        insert testBuildingGroup;

	        //Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];

	        Pricebook2 pb = new Pricebook2(Name = 'Standard Price Book', Description = 'Price Book Products', IsActive = true);
	    	insert pb;

	    	Product2 prod = new Product2(Name='Anti-infectives', Family='Best Practices', IsActive=true);
	    	insert prod;

	        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id=PRICEBOOK_ID, Product2Id=prod.Id, UnitPrice=10000, IsActive=true, UseStandardPrice=false);
	        insert standardPrice;

	    	PricebookEntry pbe = new PricebookEntry(Pricebook2Id=pb.Id, Product2Id=prod.Id, UnitPrice=10000, IsActive=true, UseStandardPrice=false);
	    	insert pbe;

		        testCategory = new Apttus_Config2__ClassificationName__c(Name='TestCategory', Apttus_Config2__HierarchyLabel__c='TestLabel');
		        insert testCategory;

		        testClassifiation = new Apttus_Config2__ClassificationHierarchy__c(Name='TestClassification', Apttus_Config2__HierarchyId__c=testCategory.Id, Apttus_Config2__Label__c='TestLabel');
		        insert testClassifiation;

				testProductClassification = new Apttus_Config2__ProductClassification__c(Apttus_Config2__ClassificationId__c=testClassifiation.Id, Apttus_Config2__ProductId__c=prod.Id);
				insert testProductClassification;

		Apttus_Proposal__Proposal_Line_Item__c testLineItem = new Apttus_Proposal__Proposal_Line_Item__c();
		testLineItem.Apttus_Proposal__Product__c 			=  prod.Id;
		testLineItem.Apttus_QPConfig__ClassificationId__c 	= testClassifiation.Id;
		testLineItem.Apttus_QPConfig__ClassificationId__c 	= testClassifiation.Id;
		testLineItem.Apttus_Proposal__Proposal__c 			= testProposal.Id;
		insert testLineItem;

		Building_Group_Product__c testBGP 	= new Building_Group_Product__c();
		testBGP.Building_Group__c 			= testBuildingGroup.Id;
		testBGP.Proposal_Line_Item__c 		= testLineItem.Id;
		testBGP.Product_lookup__c 			= prod.Id;
		insert testBGP;

		//tempAPLIW.buildingGroupProduct.System__c 				= apli.Apttus_QPConfig__ClassificationId__r.Apttus_Config2__HierarchyId__c;

        Test.setCurrentPageReference(new PageReference('/apex/NewOpportunityFirm')); 
        System.currentPageReference().getParameters().put('id', testBuildingGroup.Id);           
        System.currentPageReference().getParameters().put('cat', testCategory.Id);      
    }

    @isTest static void TestBuildingGroupProductController_Constructor_ValidParameters() {
        setup();
        BuildingGroupProductController controller 	= new BuildingGroupProductController();

        system.assertEquals(controller.buildingGroupId, testBuildingGroup.Id);
        system.assertEquals(controller.categoryHierarchy, testCategory.Id);
    }    

    @isTest static void TestBuildingGroupProductController_GetProductClassifications_ValidParameters() {
        setup();
        BuildingGroupProductController controller 	= new BuildingGroupProductController();
        controller.selectedClassification = testClassifiation.Id;
        controller.GetProductClassifications();
        system.assertEquals(0, controller.productClassifications.size());
    }      

    @isTest static void TestBuildingGroupProductController_Save_ValidParameters() {
        setup();
        BuildingGroupProductController controller 	= new BuildingGroupProductController();
        controller.selectedClassification 	= testClassifiation.Id;
        controller.selectedProduct 			= testProductClassification.Apttus_Config2__ProductId__c;
        controller.GetProductClassifications();

        PageReference pageRef = controller.Save();

      	System.assertNotEquals(null,pageRef);     
      	system.assertEquals('/apex/BuildingGroupProducts?id='+controller.buildingGroupId, pageRef.getUrl());
    }      

    @isTest static void TestBuildingGroupProductController_Cancel_ValidParameters() {
        setup();
        BuildingGroupProductController controller 	= new BuildingGroupProductController();
        PageReference pageRef = controller.Cancel();

      	System.assertNotEquals(null,pageRef);     
      	system.assertEquals('/apex/BuildingGroupProducts?id='+controller.buildingGroupId, pageRef.getUrl());
     }          
}
/*
PageReference pageRef = controller.SendEmail();
System.assertNotEquals(null,pageRef);
system.assertEquals('/'+testOpp[0].Id, pageRef.getUrl());
*/