@isTest 
public class CommunityWarrantyDetailControllerTest{
    static testMethod void CommunityWarrantyDetailControllerTest() {
       test.startTest();
       
        Account a1 = new Account();
        a1.name='test';
        a1.Community_User_ID__c=userinfo.getuserid();
        insert a1;
        
        Project__c p1 = new Project__c();
        p1.name='test pro';
        p1.Community_User_ID__c=userinfo.getuserid();
        insert p1;
        
        Warranty__c w1 = new Warranty__c();
        //w1.name='test warr';
        w1.Project__c = p1.id;
        w1.Community_User_ID__c=userinfo.getuserid();
        insert w1;
        
        Warranty_Line__c wl=new Warranty_Line__c ();
        wl.Warranty__c=w1.id;
        //wl.Community_User_ID__c=userinfo.getuserid();
        wl.Quantity__c=20;
        insert wl;
        
         
        PageReference pageRef = Page.CommunityHome; //replace with your VF page name 
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('wid',w1.id);
        ApexPages.currentPage().getParameters().put('pid',p1.id);
       CommunityWarrantyDetailController ch=new CommunityWarrantyDetailController();
        ch.SaveWarranty();
        ch.ValidateEntry();
        ch.getValue();
        apexpages.currentpage().getparameters().put('lineItemNo' ,'0' );
        ch.deleteWarrantyLineItems();
        
        ApexPages.currentPage().getParameters().put('tabname','Project');
        ApexPages.currentPage().getParameters().put('Recordid',p1.id);
        ch.testMe();
        
        ApexPages.currentPage().getParameters().put('tabname','Owner');
        ApexPages.currentPage().getParameters().put('Recordid',a1.id);
        ch.testMe();
        
        ApexPages.currentPage().getParameters().put('tabname','Applicator');
        ApexPages.currentPage().getParameters().put('Recordid',a1.id);
        ch.testMe();
        
        ApexPages.currentPage().getParameters().put('tabname','General_Contractor');
        ApexPages.currentPage().getParameters().put('Recordid',a1.id);
        ch.testMe();
        
        ApexPages.currentPage().getParameters().put('tabname','Architect');
        ApexPages.currentPage().getParameters().put('Recordid',a1.id);
        ch.testMe();
        
        ApexPages.currentPage().getParameters().put('tabname','Distributor');
        ApexPages.currentPage().getParameters().put('Recordid',a1.id);
        ch.testMe();
        PageReference pageRef1 = Page.CommunityHome; //replace with your VF page name 
        Test.setCurrentPage(pageRef1);
      
        ApexPages.currentPage().getParameters().put('pid',p1.id);
       CommunityWarrantyDetailController ch1=new CommunityWarrantyDetailController();
       ch1.upload();
       ch1.cloneRecord();
       ch1.submitForApproval();
       ch1.deleteAttachements();
       ch1.refrenceToEditPage(); 
        
        
        test.stopTest();
        
    }
}