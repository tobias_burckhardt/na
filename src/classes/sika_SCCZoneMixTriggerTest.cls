@isTest
private class sika_SCCZoneMixTriggerTest {	
	
	@isTest static void testMixOwnerTransferCheck(){
		Attachment att;
		Mix__c theMix;

		// create an SCC Zone user, and another to transfer ownership to
		map<String, sObject> objects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
        User SCCZoneUser = (User) objects.get('SCCZoneUser');

		map<String, sObject> moreObjects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
        User SCCZoneUser2 = (User) moreObjects.get('SCCZoneUser');

		// create some stuff, owned by SCCZoneUser
		System.runAs(SCCZoneUser){
			// create a mix
			theMix = sika_SCCZoneTestFactory.createMix();
			insert(theMix);

			// create an Attachment and associate it with theMix
			att = new Attachment();
			att.ParentId = theMix.Id;
			att.OwnerId = SCCZoneUser.Id;
			att.Name = 'testAttachment';

			Blob bodyBlob = Blob.valueOf('Test Attachment Body');
			att.Body = bodyBlob;
			insert(att);

			// change the owner of theMix to SCCZoneUser2
			theMix.OwnerId = SCCZoneUser2.Id;
			update(theMix);
		}

		// confirm that the attachment has changed owners
		list<Attachment> testAtt = [SELECT Id, OwnerId FROM Attachment WHERE Id = :att.Id];
		system.assertEquals(testAtt[0].OwnerId, SCCZoneUser2.Id);

	}
	
	@isTest static void testMixChatterAlert() {
		
		map<String, sObject> objects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
        User adminUser1 = (User) objects.get('adminUser');
        User SCCZoneUser = (User) objects.get('SCCZoneUser');
        User salesUser = (User) objects.get('salesUser');
        Account theAcct = (Account) objects.get('theAccount');

        User adminUser2 = sika_SCCZoneTestFactory.createAdminUser(); 
        insert(adminUser2);

        AccountTeamMember teamMember = new AccountTeamMember();
        teamMember.AccountId = theAcct.Id;
        teamMember.UserId = salesUser.Id;
		teamMember.TeamMemberRole = Sika_SCCZone_Common.SCCZoneSalesRepATMRole;

        System.runAs(adminUser2){ // using the context user or adminUser1 gives me a MIXED_DML_OPERATION error. Using salesUser gives me INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY
        	insert(teamMember);
        }

        Test.startTest();

        System.runAs(SCCZoneUser){
	        Mix__c theMix = sika_SCCZoneTestFactory.createMix();
	        theMix.Account__c = theAcct.Id;
	        insert(theMix);

        }

        list<FeedItem> thePosts = [SELECT Id, ParentId FROM FeedItem WHERE ParentId = :salesUser.Id];
        system.assertEquals(thePosts.size(), 1);

        Test.stopTest();


	}



}