/*
* Author: Martin Kona
* Company: Bluewolf
* Date: May 27, 2016
* Description: Clone building group and all child product records
* 
*/
public class CloneBuildingGroupController {
    
    private final Building_Group__c bg;
    
    @TestVisible
    private Building_Group__c clonedBg;
    
    public CloneBuildingGroupController(ApexPages.StandardController stdController) {
        this.bg = (Building_Group__c)stdController.getRecord();        
    }
    
    public PageReference cloneBuildingGroup() {                
        
        try {
        	this.clonedBG = cloneBG();
        } catch ( Exception ex) {
            // apex messages and stay on page
            ApexPages.addMessages(ex);
            return NULL;
        }
        
        // return on detail page of new record
        return new PageReference('/' + this.clonedBG.id);
    }
    
    private Building_Group__c cloneBG() {
        
        String soql = getCreatableFieldsSOQL('Building_Group__c', 'Building_Group_Products__r', 'Building_Group__c', true, 'id=\'' + bg.Id + '\'');
        Building_Group__c bui = (Building_Group__c)Database.query(soql);
        system.debug(bui.Building_Group_Products__r);         

        Building_Group__c bui2 = bui.clone(false, true);        
        bui2.Cloned__c = true;
        insert bui2;
        
        // we dont preserve ID, preserveReadonlyTimestamps and autonumbers
        List<Building_Group_Product__c> bgProducts = bui.Building_Group_Products__r.deepClone(false, false, false);
        
        // rewrite building group lookup to new cloned bui2
        for (Building_Group_Product__c bgp : bgProducts) {
            bgp.Building_Group__c = bui2.Id;
        }
        
        insert bgProducts; 
        
        return bui2;
    }
    
    /* Returns a dynamic SOQL statement for the whole object and also child records. 
     * Includes only creatable and updateable fields by current user since we will be 
     * inserting a cloned result of this query 
     */
    private static string getCreatableFieldsSOQL(String objectName, String childObjectName, String describeNameObj, Boolean hasWhere, String whereClause){
         
        String selects = '';
         
        if ((whereClause == null || whereClause == '') && hasWhere == TRUE){ return null; }
         
        // Get a map of field name and field token
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(describeNameObj.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
         
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                if (fd.isCreateable() && !fd.isUnique()){
                    selectFields.add(fd.getName());
                }
            }
        }
        
        // add child subquery
        if (childObjectName != NULL) {
            String subQuery = '(' + getCreatableFieldsSOQL(childObjectName, NULL, 'Building_Group_Product__c', false, NULL) + ')';
            selectFields.add(subQuery);
        }
         
        if (!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
             
        }
        
        String soqlFinal = 'SELECT ' + selects + ' FROM ' + objectName;
        
        if (hasWhere) {
           soqlFinal += ' WHERE ' + whereClause; 
        }
        return soqlFinal;          
    }    

}