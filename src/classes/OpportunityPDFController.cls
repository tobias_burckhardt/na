public with sharing class OpportunityPDFController {
    public Opportunity oppRecord {get;set;}
    public List<Opportunity_Firm__c> oppFirm {get;set;}
    public List<BuildingGroupWrapper> bgWC {get;set;}

    public OpportunityPDFController(ApexPages.StandardController stdController) {
        oppRecord = OpportunityServices.GetOpportunity(stdController.getId());
        oppFirm = new List<Opportunity_Firm__c>();
        oppFirm = [SELECT Id, Name, 
        Contact__r.Company__c,
        Contact__r.Company__r.Name,
        Contact__r.Company_Phone__c,
        Contact__r.Email,
        Contact__r.Name,
        Contact__r.Phone
        FROM Opportunity_Firm__c 
        WHERE Opportunity__c = :oppRecord.Id];

        Map<Id, Building_Group__c> bgMap = new Map<Id, Building_Group__c>();
        for(Building_Group__c bg :[SELECT Id, Name,
            Accepted__c,
            Applicator_Comments_Construction_Type__c,
            Applicator_Comments_Structural_Deck__c,
            Applicator_Comments_System_Cross_secion__c,
            Average_Fastener_Pullout__c,
            Building_Usage__c,
            Category__c,
            Category__r.Name,
            Confirmed_to_hold_ballast_weight__c,
            Deck_Slope__c,
            Deck_Thickness__c,
            Deck_Type__c,
            Done_By_Fastener_Pullout__c,
            Done_By_Moisture_Survey__c,
            Done_By_Roof_coating_peel_adhesion_test__c,
            Flat_or_open__c,
            Half_Rows__c,
            Height__c,
            In_a_city__c,
            In_A_Valley__c,
            In_or_near_a_mountain_range__c,
            Large_Door_Opening__c,
            Length__c,
            Multiple_Levels__c,
            Near_a_large_body_of_water__c,
            New_Flashing_type__c,
            New_No_Existing_Material__c,
            on_a_hill__c,
            Perimeter_Width__c,
            Pressurized_Building__c,
            Recover_Existing_Covering__c,
            Roof_Coating_Peel_Adhesion_Test_Adhesive__c,
            Roof_Coating_Peel_Adhesion_Test_Cohesive__c,
            Roof_Hatch_or_other_permanent_access__c,
            Sika_Sarnafil_Comments_Construction_Type__c,
            Sika_Sarnafil_Comments_Structural_Deck__c,
            Sika_Sarnafil_Comments_System_Cross_seci__c,
            Size__c,
            Tear_off_Down_To__c,
            Values_lbs_In_Roof_coating_peel_adhesi__c,
            Warranty__c,
            Warranty_Ext_Part_No__c,
            Water_test__c,
            Width__c,
            Windspeed__c
            FROM Building_Group__c 
            WHERE Opportunity__c = :oppRecord.Id]){
            bgMap.put(bg.Id, bg);
        }

        Map<Id, List<Building_Group_Product__c>> bgpMap = new Map<Id, List<Building_Group_Product__c>>();
        for(Building_Group_Product__c bgp :[SELECT Id, Name, Building_Group__c, 
            Category_Hierarchy__c,
            Category_Hierarchy__r.Name,
            Corner__c,
            Field__c,
            New__c,
            Perimeter__c,
            Product_lookup__c,
            Product_lookup__r.Name
            FROM Building_Group_Product__c 
            WHERE Building_Group__c IN :bgMap.keySet()]){
            if(bgpMap.containsKey(bgp.Building_Group__c)){
                bgpMap.get(bgp.Building_Group__c).add(bgp);
            }
            else{
                bgpMap.put(bgp.Building_Group__c, new List<Building_Group_Product__c>{bgp});
            }
        }       

        bgWC = new List<BuildingGroupWrapper>();
        for(Id bg : bgMap.keySet()){
            BuildingGroupWrapper temp   = new BuildingGroupWrapper();
            temp.buildingGroup          = bgMap.get(bg);
            temp.buildingGroupProducts  = bgpMap.get(bg);

            bgWC.add(temp);
        }
    }

    public class BuildingGroupWrapper{
        public Building_Group__c buildingGroup {get;set;}
        public List<Building_Group_Product__c> buildingGroupProducts {get;set;}

        public BuildingGroupWrapper(){
            this.buildingGroup          = new Building_Group__c();
            this.buildingGroupProducts  = new List<Building_Group_Product__c>();
        }
    }
}