/**
* Test for class provided by APPTUS without any unit tests
*/
@isTest
private class AdobeCPQSupportTest {
	@isTest static void testParams(){
		Test.startTest();
			String par= AdobeCPQSupport.PARAM_ID;
			par= AdobeCPQSupport.PARAM_CART_ID;
			par= AdobeCPQSupport.PARAM_CONFIG_REQUEST_ID;
			par= AdobeCPQSupport.PARAM_USEDEALOPTIMIZER;
			par= AdobeCPQSupport.PARAM_MODE;
			par= AdobeCPQSupport.MODE_READONLY;
			par= AdobeCPQSupport.LINETYPE_PRODUCT;
			par= AdobeCPQSupport.LINETYPE_OPTION;
		Test.stopTest();
	}

	@isTest static void createQuoteOrProposal(){
		Opportunity opp = TestingUtils.createOpportunity('test', 'test', Date.today(), true);
		Opportunity opp2 = [SELECT Id, OwnerId, StageName, Name, CloseDate from Opportunity where Id=:opp.Id];
		opp.OwnerId = opp2.OwnerId;
		Test.startTest();
			AdobeCPQSupport.createQuoteOrProposal(opp);
		Test.stopTest();
	}

	@isTest static void createProposalCurrentUser(){
		Opportunity opp = TestingUtils.createOpportunity('test', 'test', Date.today(), true);

		Test.startTest();
			AdobeCPQSupport.createProposalCurrentUser(opp);
		Test.stopTest();
	}

	@isTest static void getInstanceUrl(){
		Test.startTest();
			AdobeCPQSupport.getInstanceUrl();
		Test.stopTest();
	}

	@isTest static void nullValue(){
		Test.startTest();
			AdobeCPQSupport.nullValue('Not Null', 'Null');
		Test.stopTest();
	}

	@isTest static void getOpportunitySO(){
		Test.startTest();
			AdobeCPQSupport.getOpportunitySO('006000000000000000');
		Test.stopTest();
	}

	@isTest static void getCartSO(){
		Test.startTest();
			AdobeCPQSupport.getCartSO('006000000000000000');
		Test.stopTest();
	}
}