public class attachmentcontroller {

    public Inspection__c insp {get; set;}
    public attachmentcontroller(ApexPages.StandardController controller) {
         this.insp = (Inspection__c)controller.getRecord();
        }
    public Attachment myfile;
    public Attachment getmyfile()
    {
        myfile = new Attachment();
        return myfile;
    }

    Public Pagereference Savedoc()
    { 
        String accid = System.currentPagereference().getParameters().get('id');
        Attachment a = new Attachment(parentId = accid, name=myfile.name, body = myfile.body, description=myfile.description);         
        insert a;  
        insp.Has_Attachment__c = true;
        update insp;         
        PageReference inspection = new PageReference('/'+insp.Id);
        inspection.setRedirect(true);        
        return inspection;
        return NULL;
    }   

}