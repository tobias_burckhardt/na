public class SendEmailWithSFAttachments {

   // public String Items { get; set; }

  public Id ApplicatorQuoteId {get; set;}
  
  
    public EmailTemplate theTemplate {get; private set;}
   
    public String searchCategory ;
             public String getsearchCategory()
             {
              return searchCategory;
             }
             
             public void setsearchCategory(String searchCategory)
             {
              this.searchCategory = searchCategory;
             }

    public Applicator_Quote__c parentQuoteProposalId
    {
        get
        {
            return [SELECT Id, Name, Quote_Apttus__c FROM Applicator_Quote__c WHERE Id = :ApplicatorQuoteId];
        }
    }
    public Id accountSelectedId
    {
        get
        {
            return [SELECT Id,name,Account__c FROM Applicator_Quote__c WHERE Id = :ApplicatorQuoteId].Account__c;
        }
    }
    public String contactSelected
    {
        get
        {
            Id conId = [SELECT Id,name,Contact__c FROM Applicator_Quote__c WHERE Id = :ApplicatorQuoteId].Contact__c;
            return [SELECT id, Name from Contact WHERE id =: conId].name;
        }
    }
    public Id contactSelectedId
    {
        get
        {
            return [SELECT Id,name,Contact__c FROM Applicator_Quote__c WHERE Id = :ApplicatorQuoteId].Contact__c;
            
        }
    }
    public String selectedTemplateId
    {
        get;
        set
        {  system.debug('inside debug :'+ value + ' ---- ' +  selectedTemplateId  );
            if (value != selectedTemplateId && value != 'null' )
            {
                selectedTemplateId = value;
                theTemplate = [Select Id, TemplateType, Markup, HtmlValue, Name, Body from EmailTemplate where Id = :selectedTemplateId];
                system.debug('The template value :'+ theTemplate);
            }else if(value != selectedTemplateId && value == 'null' ){
                theTemplate = null;
            }
        }
    }
         
          
    public SendEmailWithSFAttachments()
    {
        if (!templates.isEmpty())
        {
         //   selectedTemplateId = templates[0].Id;
        }
    }

    public List<Attachment> attachments
    {
        get
        {
            return [SELECT Id, Name, LastModifiedDate, Attachment.Owner.Name
                    FROM Attachment
                    WHERE ParentId = :ApplicatorQuoteId OR ParentId = :parentQuoteProposalId.Quote_Apttus__c];
        }
    }

    public List<Contact> bccContacts
    {
        get
        {
            if (bccContacts == null)
            {
                bccContacts = [SELECT Id, Name, Email
                               FROM Contact
                               WHERE Email != null and account.SAP_Roofing_Customer__c=true
                               ORDER BY Name];
            }
            return bccContacts;
        }
        private set;
    }    

    public List<Contact> contacts
    {
        get
        {
            if (contacts == null)
            {
                contacts = [SELECT Id, Name, Email
                            FROM Contact
                            WHERE Email != null and account.SAP_Roofing_Customer__c=true
                            ORDER BY Name];
            }
            return contacts;
        }
        private set;
    }

  /*  public List<User> users
    {
        get
        {
            if (users == null)
            {
                users = [SELECT Id, Name, Email
                         FROM User
                         WHERE Email != null And isActive = true
                         ORDER BY Name limit 1000];
            }
            return users;
        }
        private set;
    } */

    public List<EmailTemplate> templates
    {
        get
        {
            if (templates == null)
            {
                templates = [SELECT Id,Name
                             FROM EmailTemplate
                             WHERE IsActive = true AND Folder.DeveloperName = 'PTS_Applicator_Email_Templates'];
            }
            return templates;
        }
        private set;
    }
    
    public PageReference cancel(){

    // create case, etc.

    PageReference pr = new PageReference('//'+ApplicatorQuoteId);
    pr.setRedirect(true);
    return pr;
    }

    public class WhereClause
    {
        public String whereColumn;
        public Object whereValue;
        public String eval; // EQUALS, LIKE


        public String print()
        {
            String whereVal = String.valueOf(whereValue);
            String clause = ' ' + String.escapeSingleQuotes(whereColumn);

            if (eval == 'EQUALS')
            {
                if (whereValue instanceOf String)
                {
                    whereVal = String.escapeSingleQuotes(whereVal);
                    whereVal = '\'' + whereVal + '\'';
                }
                clause += ' = ' + whereVal;
            }

            else if (eval == 'LIKE')
            {
                whereVal = whereVal.replaceAll('[*]', '%');
                clause += ' LIKE \'%' + whereVal + '%\'';
            }

            return clause;
        }
    }

    public static String evaluatorClauses(List<WhereClause> clauses)
    {
        List<String> stClause = new List<String>();
        for (WhereClause clause : clauses)
        {
            stClause.add(clause.print());
        }

        return String.join(stClause, ' AND ');
    }

    @RemoteAction
    public static List<sObject> query(List<String> fields, String tableName, List<WhereClause> clauses)
    {
        String strFields = String.escapeSingleQuotes(String.join( fields, ','));
        String query = 'SELECT ' + strFields + ' FROM ' + tableName + ' WHERE ' + evaluatorClauses(clauses);
        return Database.query(query);
    }

    public static Messaging.SingleEmailMessage sendEmail(ApexSingleEmailMessage singleEmail)
    {
        return singleEmail.sendMessage();
    }

    @RemoteAction
    public static void sendEmail(String singleEmailJSON)
    {
        ApexSingleEmailMessage singleEMail =
            (ApexSingleEmailMessage) JSON.deserialize(singleEmailJSON, ApexSingleEmailMessage.class);
        sendEmail(singleEMail);
    }

    public class ApexSingleEmailMessage
    {    SendEmailWithSFAttachments  outerObj;
    
        public ApexSingleEmailMessage(SendEmailWithSFAttachments  outerObj){
            this.outerObj = outerObj;
        }
        /**
         * Optional. A list of blind carbon copy (BCC) addresses. The maximum allowed is 25. This argument is allowed
         * only when a template is not used.
         */
        public List<String> bcc;

        /**
         * Optional. A list of carbon copy (CC) addresses. The maximum allowed is 25. This argument is allowed only
         * when a template is not used.
         */
        public List<String> cc;

        /**
         * Optional. The character set for the email. If this value is null, the user's default value is used.
         */
        public String charSet;

        /**
         * Optional. A list containing the ID of each document object you want to attach to the email.
         */
        public List<String> documentIds;

        /**
         * Optional. A list containing the ID of each document object you want to attach to the email.
         */
        public List<String> attachmentIds;

        /**
         * Required if using a template, optional otherwise. The ID of the contact, lead, or user to which the email
         * will be sent. The ID you specify sets the context and ensures that merge fields in the template contain
         * the correct data.
         */
        public String targetObjectId;

        /**
         * The maximum number of email addresses allowed is 100. This argument is allowed only when a template is NOT used.
         */
        public List<String> toAddresses;

        /**
         * Optional. If you specify a contact for the targetObjectId field, you can specify a whatId as well. This
         * helps to further ensure that merge fields in the template contain the correct data.
         */
        public String whatId;

        /**
         * Optional. The email subject line. If you are using an email template, the subject line of the template
         * overrides this value.
         */
        public String subject;

        public String body;
        public String templateId;
        public String freeText;
        public String radioValueSelected;
        public String freeTextSubject;

        public Messaging.SingleEmailMessage message
        {
            get {
                if (message == null)
                    message = new Messaging.SingleEmailMessage();
                return message;
            }
            private set;
        }

        public Messaging.SingleEmailMessage sendMessage()
        {
            message.setBccAddresses(bcc);
            message.setCcAddresses(cc);
            message.setEntityAttachments(documentIds);
            addAttachments();

            if (Id.valueOf(targetObjectId).getSObjectType() == User.sObjectType)
            {
                message.saveAsActivity = false;
            }

            message.setTargetObjectId(targetObjectId);
            message.setToAddresses(toAddresses);
            message.setWhatId(whatId);
            
            if (templateId != null && radioValueSelected ==  'Template')
            {
                
                message.setTemplateId(templateId);
            }

            else
            {  
                message.setSubject(freeTextSubject);
                //message.setCharset( charSet );
                message.setHtmlBody(freeText);
                //message.setInReplyTo(String) 
               // message.setPlainTextBody( body );
            }

            Messaging.sendEmail(new List<Messaging.Email>{message});

            return message;
        }
        
             
        private void addAttachments()
        {
            if (attachmentIds != null && !attachmentIds.isEmpty())
            {
                List<Attachment> actualContent = [SELECT Id, Body, ContentType, Name
                                                  FROM Attachment
                                                  WHERE Id IN :attachmentIds];
                List<Messaging.EmailFileAttachment> emailAttachments = new List<Messaging.EmailFileAttachment>();
                for (Attachment a : actualContent)
                {
                    Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
                    attach.setBody(a.Body);
                    attach.setContentType(a.ContentType);
                    attach.setFileName(a.Name);
                    attach.setInline(false);
                    emailAttachments.add(attach);
                }
                message.setFileAttachments(emailAttachments);
            }
        }
    }
}