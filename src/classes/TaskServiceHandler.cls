public with sharing class TaskServiceHandler {

    public static void addMember(List<Task> newTasks) {
        Set<Id> opportunityIds = new Set<Id>();
        Set<Id> accountIds = new Set<Id>();

        for (Task t : newTasks) {
            if (t.whatId != null) {
                String whatId = t.WhatId;
                if (whatId.startswith('006')) {
                    opportunityIds.add(t.WhatId);
                } else if (whatId.startswith('001')) {
                    accountIds.add(t.WhatId);
                }
            }
        }

        Map<Id, Opportunity> opportunitiesById = new Map<Id, Opportunity>();
        Map<Id, Account> accountById = new Map<Id, Account>();

        if (!opportunityIds.isEmpty()) {
            opportunitiesById = new Map<Id, Opportunity>([
                SELECT OwnerId, (SELECT OpportunityId, UserId, TeamMemberRole FROM OpportunityTeamMembers)
                FROM Opportunity
                WHERE Id IN :opportunityIds
            ]);
        }

        if (!accountIds.isEmpty()) {
            accountById = new Map<Id, Account>([
                SELECT OwnerId, (SELECT AccountId, UserId, TeamMemberRole FROM AccountTeamMembers)
                FROM Account
                WHERE Id IN :accountIds
            ]);
        } 

        List<OpportunityTeamMember> opportunityMembersToInsert = new List<OpportunityTeamMember>();
        List<AccountTeamMember> accountMembersToInsert = new List<AccountTeamMember>();
        Map<Id, Set<Id>> opptyNotOwnerIds = new Map<Id, Set<Id>>();
        Map<Id, Set<Id>> acctNotOnwerIds = new Map<Id, Set<Id>>();
        Set<Id> opportunityOwnerIds = new Set<Id>();
        Set<Id> accountOwnerIds = new Set<Id>();

        for (Task t : newTasks) {
            if (t.WhatId != null && String.valueOf(t.WhatId).startsWith('006')) {
                Opportunity opp = opportunitiesById.get(t.WhatId);
                Boolean isMember = false;

                if (opp != null) {
                    for (OpportunityTeamMember member : opp.OpportunityTeamMembers) {
                        if (member.UserId == t.OwnerId) {
                            isMember = true;
                            break;

                        }
                    }
                }

                if (!isMember) {
                    OpportunityTeamMember member = new OpportunityTeamMember();
                    member.OpportunityId = t.WhatId;
                    member.UserId = t.OwnerId;
                    if (opp.OwnerId == t.OwnerId) {
                        member.TeamMemberRole = 'Opportunity Owner';
                    } else {
                        member.TeamMemberRole = 'Task Assigned';
                    }

                    opportunityMembersToInsert.add(member);
                    if (opp.OwnerId != t.OwnerId) {
                        if (!opptyNotOwnerIds.containsKey(t.OwnerId)) {
                            opptyNotOwnerIds.put(t.OwnerId, new Set<Id>());
                        }
                        opptyNotOwnerIds.get(t.OwnerId).add(opp.Id);
                    }
                }
            } else if (t.WhatId != null && String.valueOf(t.WhatId).startsWith('001')) {
                Account acc = accountById.get(t.WhatId);
                Boolean isMember = false;

                if (acc != null) {
                    for (AccountTeamMember member : acc.AccountTeamMembers) {
                        if (member.UserId == t.OwnerId) {
                            isMember = true;
                            break;
                        }
                    }
                }

                if (!isMember) {
                    AccountTeamMember member = new AccountTeamMember();
                    member.AccountId = t.WhatId;
                    member.UserId = t.OwnerId;
                    if (acc.ownerId == t.OwnerId) {
                        member.TeamMemberRole = 'Account Owner';
                    } else {
                        member.TeamMemberRole = 'Task Assigned';
                    }

                    accountMembersToInsert.add(member);
                    if (acc.OwnerId != t.OwnerId) {
                        if (!acctNotOnwerIds.containsKey(t.OwnerId)) {
                            acctNotOnwerIds.put(t.OwnerId, new Set<Id>());
                        }
                        acctNotOnwerIds.get(t.OwnerId).add(acc.Id);
                    }
                }
            }
        }

        //if (newTasks.size() > 0) {
        //    for (Task t : newTasks) {
        //        if (t.whatId == null)
        //            continue;
        //        String whatid = t.whatId;
        //        if (whatid.startswith('006')) {
        //            opportunityIds.add(t.whatid);
        //        } else if (whatid.startswith('001')) {
        //            accountIds.add(t.whatid);
        //        }
        //    }

        //    Map<Id, Opportunity> opportunitiesById = new Map<Id, Opportunity>([select id, OwnerId, (select id, OpportunityId , UserId, TeamMemberRole from OpportunityTeamMembers) from Opportunity where id in:opportunityIds]);
        //    Map<Id, Account> accountById = new Map<Id, Account>([select id, OwnerId, (select id, AccountId , UserId, TeamMemberRole from AccountTeamMembers) from Account where id in:accountIds]);
        //    OpportunityTeamMember [] opportunityMembersToInsert = new List<OpportunityTeamMember>();
        //    AccountTeamMember [] accountMembersToInsert = new List<AccountTeamMember>();
        //    Set<id> opportunityOwnerIds = new Set<id>();
        //    Set<id> accountOwnerIds = new Set<id>();

        //    for (Task t : newTasks) {
        //        String whatid = t.whatId == null ? '' : t.whatId;
        //        if (!(whatid.startswith('006') || whatid.startswith('001'))) {
        //            system.debug('inside Not valid if');
        //            continue;
        //        }

        //        if (whatid.startswith('006')) {
        //            Opportunity opp = opportunitiesById.get(t.whatid);
        //            Boolean isMember = false;
        //            if (opp != null) {
        //                for (OpportunityTeamMember otm : opp.OpportunityTeamMembers) {
        //                    if (otm.UserId == t.OwnerId) {
        //                        isMember = true;
        //                        break;

        //                    }
        //                }
        //            }
        //            if (!isMember) {
        //                OpportunityTeamMember member = null;
        //                member = new OpportunityTeamMember();
        //                member.OpportunityId = t.whatId;
        //                member.UserId = t.ownerId;
        //                if (opp.ownerId == t.OwnerId)
        //                    member.TeamMemberRole = 'Opportunity Owner';
        //                else
        //                    member.TeamMemberRole = 'Task Assigned';
        //                opportunityMembersToInsert.add(member);
        //                if (opp.ownerId != t.OwnerId)
        //                    opportunityOwnerIds.add(t.ownerId);
        //            }
        //        } else if (whatid.startswith('001')) {
        //            Account acc = accountById.get(t.whatid);
        //            Boolean isMemberAcc = false;
        //            if (acc != null) {
        //                for (AccountTeamMember otm : acc.AccountTeamMembers) {
        //                    if (otm.UserId == t.OwnerId) {
        //                        isMemberAcc = true;
        //                        break;

        //                    }
        //                }
        //            }
        //            if (!isMemberAcc) {
        //                AccountTeamMember member = null;
        //                member = new AccountTeamMember();
        //                member.AccountId = t.whatId;
        //                member.UserId = t.ownerId;
        //                if (acc.ownerId == t.OwnerId)
        //                    member.TeamMemberRole = 'Account Owner';
        //                else
        //                    member.TeamMemberRole = 'Task Assigned';
        //                accountMembersToInsert.add(member);
        //                if (acc.ownerId != t.OwnerId)
        //                    accountOwnerIds.add(t.ownerId);
        //            }
        //        }
        //    }

        Savepoint sp = Database.setSavepoint();
        if (!opportunityMembersToInsert.isEmpty()) {
            try {
                TaskServicesWithoutSharing.insertOpportunityTeamMembers(opportunityMembersToInsert);
            } catch (DmlException e) {
                Database.RollBack(sp);
                throw e;
            }
        }

        if (!accountMembersToInsert.isEmpty()) {
            try {
                TaskServicesWithoutSharing.insertAccountTeamMembers(accountMembersToInsert);
            } catch (DmlException e) {
                Database.RollBack(sp);
                throw e;
            }
        }

        List<OpportunityShare> opportunityShares = new List<OpportunityShare>();
        if (!opportunityOwnerIds.isEmpty()) {
            opportunityShares = [
                SELECT OpportunityId, OpportunityAccessLevel, UserOrGroupId
                FROM OpportunityShare
                WHERE UserOrGroupId IN :opportunityOwnerIds
            ];
        }

        System.debug(LoggingLevel.DEBUG, '\n$ accountOwnerIds: ' + accountOwnerIds + '\n');

        List<AccountShare> accountShares = new List<AccountShare>();
        if (!accountOwnerIds.isEmpty()) {
            accountShares = [
                SELECT AccountId, AccountAccessLevel, UserOrGroupId
                FROM AccountShare
                WHERE UserOrGroupId IN :accountOwnerIds
            ];
        }

        List<OpportunityShare> opportunitySharesToUpdate = new List<OpportunityShare>();
        for (OpportunityShare share : opportunityShares) {
            if (opptyNotOwnerIds.containsKey(share.UserOrGroupId) &&
                    !share.OpportunityAccessLevel.equals('Edit')) {
                share.OpportunityAccessLevel = 'Edit';
                opportunitySharesToUpdate.add(share);
            }
        }
        
        List<AccountShare> accountSharesToUpdate = new List<AccountShare>();
        for (AccountShare share : accountShares) {
            if (acctNotOnwerIds.containsKey(share.UserOrGroupId) &&
                    !share.AccountAccessLevel.equals('Edit')) {
                share.AccountAccessLevel = 'Edit';
                accountSharesToUpdate.add(share);
            }
        }

        if (!opportunitySharesToUpdate.isEmpty()) {
            try {
                TaskServicesWithoutSharing.updateOpportunityShares(opportunityShares);
            } catch (DmlException e) {
                Database.RollBack(sp);
                throw e;
            }
        }

        if (!accountShares.isEmpty()) {
            try {
                TaskServicesWithoutSharing.updateAccountShares(accountShares);
            } catch (DmlException e) {
                Database.RollBack(sp);
                throw e;
            }
        }
        //}
    }
}