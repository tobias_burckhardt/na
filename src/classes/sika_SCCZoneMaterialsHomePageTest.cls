@isTest
private class sika_SCCZoneMaterialsHomePageTest {
	private static sika_SCCZoneMaterialsHomePageController ctrl;
	private static PageReference pageRef;

	private static User SCCZoneUser1;
	private static User SCCZoneUser2;
	private static Account theAccount;

	private static Integer allMaterialListSize;
	private static Integer i;

	// create the objects we'll be using in the tests.
	private static void init(){
		// create two SCC Zone users, both associated with the same account
		map<String, sObject> objects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
        SCCZoneUser1 = (User) objects.get('SCCZoneUser');
        theAccount = (Account) objects.get('theAccount');
        User adminUser = (User) objects.get('adminUser');
        																		
        map<String, sObject> moreObjects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
        SCCZoneUser2 = (User) moreObjects.get('SCCZoneUser');

		// create some materials for SCCZoneUser1
		Material__c tempMaterial;
		list<Material__c> materialsToInsert = new list<Material__c>();
		String makeThisType;

		// three of each
		makeThisType = 'Cement';
		for(i=3; i > 0; i--){
			if(makeThisType == 'Cement'){
				tempMaterial = sika_SCCZoneTestFactory.createMaterialCement();
				if(i == 1) { 
					makeThisType = 'SCM';
					i = 4; 
				}
			} else if(makeThisType == 'SCM'){
				tempMaterial = sika_SCCZoneTestFactory.createMaterialSCM();
				if(i == 1) { 
					makeThisType = 'Sand';
					i = 4; 
				}
			} else if(makeThisType == 'Sand'){
				tempMaterial = sika_SCCZoneTestFactory.createMaterialSand();
				if(i == 1) { 
					makeThisType = 'Coarse Aggregate';
					i = 4; 
				}
			} else if(makeThisType == 'Coarse Aggregate'){
				tempMaterial = sika_SCCZoneTestFactory.createMaterialCoarseAgg();
				if(i == 1) { 
					makeThisType = 'Water';
					i = 4; 
				}
			} else if(makeThisType == 'Water'){
				tempMaterial = sika_SCCZoneTestFactory.createMaterialWater();
				if(i == 1) { 
					makeThisType = 'Other';
					i = 4; 
				}
			} else if(makeThisType == 'Other'){
				tempMaterial = sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'type' => 'other'});
			}
			tempMaterial.Material_Name__c = makeThisType + ' ' + i;
			tempMaterial.OwnerId = SCCZoneUser1.Id;
			materialsToInsert.add(tempMaterial);

		}
		allMaterialListSize = materialsToInsert.size();

		// bypass the triggers that we don't need to fire during these tests
		sika_SCCZoneMaterialTriggerHelper.bypassPreventDelete = true;
		sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = true;
		sika_SCCZoneMaterialTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = true;

		// insert the materials as the SCCZone user. (The SCCZone user will be the materials' record owner.)
		System.runAs(SCCZoneUser1){
			insert(materialsToInsert);
		} 

		// create some materials for SCCZoneUser2. (Same Account.)
		materialsToInsert.clear();
		for(i = 2; i > 0; i--){
			tempMaterial = sika_SCCZoneTestFactory.createMaterialCoarseAgg();
			tempMaterial.Material_Name__c = 'Coarse Agg ' + i;
			tempMaterial.OwnerId = sccZoneUser2.Id;
			materialsToInsert.add(tempMaterial);
		}
		System.runAs(SCCZoneUser2){
			insert(materialsToInsert);
		} 

		// set page we'll be testing
        pageRef = new PageReference('/sika_SCCZoneMaterialsHomePage');
        pageRef.getParameters().put('state', 'me');
        pageRef.getParameters().put('view', 'all');
        Test.setCurrentPage(pageRef);

	}


	@isTest static void testLoad(){
		init();
		test.startTest();

		System.runAs(SCCZoneUser1){
			// instantiate the page controller
		    ctrl = new sika_SCCZoneMaterialsHomePageController();

			// affirm that the page loaded. (Didn't redirect to an error page)
			system.assert(ApexPages.CurrentPage().getURL().contains('sika_SCCZoneMaterialsHomePage'));

			// affirm that the page loaded to the default view ("my materials", "all")
			system.assertEquals(ctrl.view, 'all');
			
			// affirm that the material list is displaying all materials (except admixtures)
			system.assertEquals(ctrl.displayList.size(), allMaterialListSize);
			

		}
		test.stopTest();

	}


	@isTest static void testFilterByType(){
		init();
		test.startTest();		
		
		System.runAs(SCCZoneUser1){
			// instantiate the page controller
			ctrl = new sika_SCCZoneMaterialsHomePageController();

			ctrl.sortField = 'Material_Name__c';

			ctrl.view = 'cement';
			ctrl.tableSort();
			system.assertEquals(ctrl.tempSortList.size(), 3);
			ctrl.view = 'sand';
			ctrl.tableSort();
			system.assertEquals(ctrl.tempSortList.size(), 3);
			ctrl.view = 'scm';
			ctrl.tableSort();
			system.assertEquals(ctrl.tempSortList.size(), 3);
			ctrl.view = 'coarse';
			ctrl.tableSort();
			system.assertEquals(ctrl.tempSortList.size(), 3);
			ctrl.view = 'water';
			ctrl.tableSort();
			system.assertEquals(ctrl.tempSortList.size(), 3);
			ctrl.view = 'other';
			ctrl.tableSort();
			system.assertEquals(ctrl.tempSortList.size(), 3);

		}
		test.stopTest();
	}


	@isTest static void testSortTable(){
		init();
		String tempString;
		Decimal tempDecimal;
		
		test.startTest();
		
		System.runAs(SCCZoneUser1){
			// instantiate the page controller
		    ctrl = new sika_SCCZoneMaterialsHomePageController();

			// sort the list by name. (Should default to sort ascending)
			ctrl.sortField = 'Material_Name__c';
			ctrl.tableSort();

			// sort by name again. Affirm that the list is now sorted in the other direction. (Descending)
			tempString = ctrl.displayList[0].Material_Name__c;
			ctrl.tableSort();

			system.assertEquals(ctrl.displayList[ctrl.displayList.size() - 1].Material_Name__c, tempString);

			// repeat for the other table columns
			ctrl.sortField = 'Type__c';
			ctrl.tableSort();
			tempString = ctrl.displayList[0].Type__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.displayList[ctrl.displayList.size() - 1].Type__c, tempString);

			ctrl.sortField = 'Source__c';
			ctrl.tableSort();
			tempString = ctrl.displayList[0].Source__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.displayList[ctrl.displayList.size() - 1].Source__c, tempString);

			ctrl.sortField = 'Cost__c';
			ctrl.tableSort();
			tempDecimal = ctrl.displayList[0].Cost__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.displayList[ctrl.displayList.size() - 1].Cost__c, tempDecimal);

			ctrl.sortField = 'Unit__c';
			ctrl.tableSort();
			tempString = ctrl.displayList[0].Unit__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.displayList[ctrl.displayList.size() - 1].Unit__c, tempString);

		}
		test.stopTest();

	}

	@isTest static void testNewMaterialButton(){
		init();
		test.startTest();

		System.runAs(SCCZoneUser1){
			// instantiate the page controller
		    ctrl = new sika_SCCZoneMaterialsHomePageController();

		    ctrl.typeString = 'Sand';
		    ctrl.theUser.System_of_measure__c = 'Metric (SI)';
		    system.assert(ctrl.createNewMaterial().getURL().contains('/sika_SCCZoneCreateEditMaterialPage'));
		}
	}
	

	@isTest static void testSieveQueryHelper(){
		init();
		ctrl = new sika_SCCZoneMaterialsHomePageController();
		ctrl.sieveQueryHelper();
//TODO: finish this
	}



	@isTest static void testLoadAction(){
        User guestUser = sika_SCCZoneTestFactory.createGuestUser();
        insert(guestUser);

        PageReference p;

        System.runAs(guestUser){
            ctrl = new sika_SCCZoneMaterialsHomePageController();
            p = ctrl.loadAction();
            system.assert(String.valueOf(p).contains('/SCCZone/sika_SCCZoneLoginPage'));
        }
    }

}