public class ApplicatorPdfGeneratorController {
    
    public String todayFormatted {get; set;}
    public Account acc {get; set;}
    public Boolean elite {get; set;}
    public String agreements {get; set;}
    public Region__c reg {get; set;}
    public User roofing_Rep_Tech_Mgr {get; set;}

    public ApplicatorPdfGeneratorController(ApexPages.StandardController sc){
        if (!test.isRunningTest()) sc.addFields(new List<String>{'Name','BillingCity', 'BillingPostalCode', 
                    'BillingState', 'Billing_House_Nr__c', 'BillingStreet', 'Partners_Club_Status__c', 
                    'Partners_Club_Agreements__c', 'Roofing_Rep_Tech_Mgr__r', 'Roofing_Rep_Tech_Mgr__r.Name', 
                    'Roofing_Rep_Tech_Mgr__r.Title', 'Roofing_Rep_Tech_Mgr__r.Phone', 
                    'Roofing_Rep_Tech_Mgr__r.Street', 'Roofing_Rep_Tech_Mgr__r.City', 'Roofing_Rep_Tech_Mgr__r.State',
                    'Roofing_Rep_Tech_Mgr__r.Country', 'Roofing_Rep_Tech_Mgr__r.PostalCode', 'Roofing_Rep_Tech_Mgr__r.Email'});
        this.acc = (Account) sc.getRecord();
        this.roofing_Rep_Tech_Mgr = (User) acc.Roofing_Rep_Tech_Mgr__r;
        
		this.todayFormatted = Datetime.now().format('MMMM d,  yyyy');
        this.elite = false;
        if (acc.Partners_Club_Status__c == 'Elite' && String.isNotBlank(acc.Partners_Club_Agreements__c)){
            this.elite = true;
        }
        
        this.agreements = getAgreements(acc.Partners_Club_Agreements__c);
        
        this.reg = selectRegionAddress();
    }
    
    public String getAgreements(String allAgreements){
        String agr = '';
        if (String.isNotBlank(allAgreements)){
            String[] tmpString = allAgreements.split(';');
            Integer length = tmpString.size();
            
            if (length == 1){
                agr = tmpString[0];
            } else if (length >= 2){
                for (Integer i = 0; i<(length-1); i++){
                    agr = agr + tmpString[i] + ', ';
                }
                agr = agr.removeEnd(', ');
                agr = agr + ' and ' + tmpString[length-1];
            }
        }
        return agr;
    }
    
    public Region__c selectRegionAddress(){
        
        List<Region__c> r = [SELECT Address__c , City__c , State__c , Zip__c FROM Region__c WHERE Sales_Person__c =:UserInfo.getUserId()];
        
        if (r != null && !r.isEmpty()){
            return r[0];
        } else return new Region__c(); // default address to be specified here
    }
}