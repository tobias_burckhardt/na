public with sharing  class Dispenser_Installationctrl_Edit {
    public Dispenser_Installation__c  di{get;set;}
    public list<Asset_Type__c> justlist{get;set;}
    public list<Concrete_Dispenser_Product__c> justfylist{get;set;}
    public Integer recCount{get;set;}
    public Integer recCount1{get;set;}
    public integer count{get;set;}
    public integer count1{get;set;}
    public Integer selectedRowIndex{get;set;}
    public Integer selectedRowIndex1{get;set;}
    public list<string> asse{get;set;}
    public account acc1{get;set;}
    public Boolean isUserFromFinance{get;set;}
    public Boolean isUserFromSalesOrTechSales{get;set;}
    public Dispenser_Installationctrl_Edit(ApexPages.StandardController controller) {
        acc1 = new account();
        asse = new list<string>();
        list<Asset_Type__c> ass = [select id,name,Asset_Specification__c,Suppliers_Name__c,Type_Of_Asset__c from Asset_Type__c where Type_Of_Asset__c !=''];
        system.debug('-------------->'+ass.size());
        for(Asset_Type__c as1 : ass){
            asse.add(as1.Type_Of_Asset__c+'/'+as1.name);
        }
        id  did =  ApexPages.currentPage().getParameters().get('id');
        di = [select id,name,Account__c,Customer_s_Account_Code__c,Sales_Representative_s_Name__c,Date__c,Dispenser_Tech__c,Region__c,New_Account__c,Customer_code__c,Customer_Name__c,Customer_Address__c,Customer_s__c,Installation_Address__c,Type_of_Installation__c,customer_account__c,Customer_Code_Transfers__c,Site_Address__c,Region_Transfer__c,Total_Estimated_Investment__c,Purchase_Date__c,Installation_Date__c,Estimated_Installation_Date__c,Justification_For_Upgrade__c,Street__c,City__c,State_Province__c,Zip_Postal_Code__c,Country__c,Installation_Street__c,Customer_Name1__c ,Installation_City__c,Installation_State_Province__c,Installation_Zip_Postal_Code__c,Installation_Country__c ,Real_Cost__c ,Budget_Cost__c ,Last_12_months_Material_Margin__c,Last_12_months_Gross_sales_of_Customer__c,SAP__c,SAP_Asset_Code__c,SAP_Capitalization__c,Distance_From_Sika_Facility__c
              from Dispenser_Installation__c where id=: did];
        justlist = [select id,name,S_No__c,Asset_Specification__c,Suppliers_Name__c,Quantity__c,Total_Cost__c,Purchase_Cost__c,Type_Of_Asset1__c,Suppliers_Name1__c,Asset_Specification1__c from Asset_Type__c where Dispenser_Installation__c =: di.id];
        justfylist = [select id,name,Projected_Sales_Volume__c,Excepted_Selling_Price__c,Estimated_Annual_Sales__c,Standard_Cost__c,Gross_Margin__c,
                      Gross_Margin_In_Percentage__c,MaterialName__c,S_No__c ,Dispenser_Installation__c from Concrete_Dispenser_Product__c where Dispenser_Installation__c =: di.id];
        if(justfylist.size()>0){
            recCount1 = justfylist.size();
        }
        else{
            recCount1 =0;
        }
        if(justlist.size()>0){
            recCount = justfylist.size();
        }
        else{
            recCount =0;
        }
        String permissionSetForFinance = 'CA_dispenser_Access_for_Finance_Domain';
        Integer assignedPerSetToFinUserCount = [SELECT Count() FROM PermissionSetAssignment WHERE AssigneeId = :Userinfo.getUserId() and PermissionSet.Name=:permissionSetForFinance];
        isUserFromFinance = assignedPerSetToFinUserCount>0 ? true:false;
        
        String permissionSetForSales = 'CA_Dispenser_Permissions_For_SalesReps';
        String permissionSetForTechnicians = 'CA_Dispenser_Permissions_Technician';
        Integer assignedPerSetToSalesUserCount = [SELECT Count() FROM PermissionSetAssignment WHERE AssigneeId = :Userinfo.getUserId() and (PermissionSet.Name=:permissionSetForSales OR PermissionSet.Name=:permissionSetForTechnicians)];
        isUserFromSalesOrTechSales = assignedPerSetToSalesUserCount>0 ? true:false;
        
    }
    
    
    public void Add()
    {    
        
        
        recCount+=1;
        Asset_Type__c j = new Asset_Type__c();  
        j.S_No__c=recCount;      
        justlist.add(j);         
        
    }
    
    public void Del()
    {
        system.debug('*****************lect'+selectedRowIndex);
        system.debug('*****************justlist'+justlist);
        
        if(justlist.size()>0)
        {
            system.debug('*****************justlist11111'+justlist);
            for(Integer i=0; i<justlist.size();i++)
            {
                system.debug('*****************recCount'+recCount);
                
                if(justlist[i].S_No__c==selectedRowIndex)
                {
                    count=i;
                    system.debug('*****************i'+i);
                    justlist.remove(i);
                    system.debug('*****************justlist---'+justlist);
                }
            }
            
        }
        
    }
    public void productvalue(){
        
        /***   for(Asset_Type__c js : justlist){
if(js.Type_Of_Assert__c !=null){
list<string> tal = new  list<string>();
tal = js.Type_Of_Assert__c.split('/');
Asset_Type__c pr = [select id,name,Asset_Specification__c,Suppliers_Name__c,Type_Of_Asset__c from Asset_Type__c where name =: tal[1] limit 1];
js.Asset_Specification__c = pr.Asset_Specification__c;
js.Suppliers_Name__c = pr.Suppliers_Name__c;
//js.Type_Of_Assert__c = tal[0];
}


}**/
        
    }
    public void totalcost(){
        for(Asset_Type__c js : justlist){
            list<string> tal = new  list<string>();
            if(js.Quantity__c !=null && js.Purchase_Cost__c !=null){
                js.Total_Cost__c = js.Quantity__c*js.Purchase_Cost__c;
            }
        }
        
    }
    public void purchasecost(){
        for(Asset_Type__c js : justlist){
            if(js.Type_Of_Asset1__c !='' && js.Asset_Specification1__c !='' && js.Suppliers_Name1__c !=''){
                list<Dispenser_Line_Item__c> dli = [select id,Purchase_Cost__c from Dispenser_Line_Item__c where Type_Of_Assert__c =: js.Type_Of_Asset1__c and Asset_Specification__c =: js.Asset_Specification1__c and Suppliers_Name__c =:js.Suppliers_Name1__c];
                if(dli.size()>0)
                    js.Purchase_Cost__c = dli[0].Purchase_Cost__c;
            }
        }
        totalcost();
    }
    public void Add1()
    {    
        
        
        recCount1+=1;
        Concrete_Dispenser_Product__c j = new Concrete_Dispenser_Product__c();  
        j.S_No__c=recCount1;      
        justfylist.add(j);         
        
    }
    
    public void Del1()
    {
        system.debug('*****************lect'+selectedRowIndex1);
        system.debug('*****************justlist'+justfylist);
        
        if(justfylist.size()>0)
        {
            system.debug('*****************justlist11111'+justfylist);
            for(Integer i=0; i<justfylist.size();i++)
            {
                system.debug('*****************recCount'+recCount1);
                
                if(justfylist[i].S_No__c==selectedRowIndex1)
                {
                    count1=i;
                    system.debug('*****************i'+i);
                    justfylist.remove(i);
                    system.debug('*****************justlist---'+justfylist);
                }
            }
            
        }
        
    }
    
    public void productvalue1(){
        
        for(Concrete_Dispenser_Product__c js : justfylist){
            if(js.MaterialName__c !=null){
                Product2 pr = [select id,name,Projected_Sales_Volume__c,Excepted_Selling_Price__c,Estimated_Annual_Sales__c,Standard_Cost__c,Gross_Margin__c,Gross_Margin_In_Percentage__c from Product2 where id =: js.MaterialName__c];
                js.Projected_Sales_Volume__c = pr.Projected_Sales_Volume__c;
                js.Excepted_Selling_Price__c = pr.Excepted_Selling_Price__c;
                js.Estimated_Annual_Sales__c = pr.Estimated_Annual_Sales__c;
                js.Standard_Cost__c = pr.Standard_Cost__c;
                js.Gross_Margin__c = pr.Gross_Margin__c;
               js.Gross_Margin_In_Percentage__c = pr.Gross_Margin_In_Percentage__c;
            }
            
            
        }
        
    }
    
    public PageReference savec(){
        try{
            if(di.Region__c == null){
                Apexpages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.FATAL, 'Please enter Business Unit/Region'));
                return NULL;
            }
            if(di.Customer_Name1__c == null){
                Apexpages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.FATAL, 'Please enter SOLD-TO ACCOUNT NAME'));
                return NULL;
            }
            if(di.Account__c == null){
                Apexpages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.FATAL, 'Please enter Ship-TO ACCOUNT NAME'));
                return NULL;
            }
            if(di.Type_of_Installation__c == ''){
                Apexpages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.FATAL, 'Please enter Type of Installation'));
                return NULL;
            }
            if(di.New_Account__c== 'Yes'){
                insert acc1;
                di.Account__c = acc1.id;
                di.Installation_Street__c= acc1.BillingStreet;
                di.Installation_City__c = acc1.BillingCity;
                di.Installation_State_Province__c = acc1.BillingState;
                di.Installation_Zip_Postal_Code__c = acc1.BillingPostalCode;
                di.Installation_Country__c = acc1.BillingCountry;
                di.Customer_s_Account_Code__c = acc1.SAP_Account_Code__c;
            }
            update di;
            for(integer i=0;i<justlist.size();i++){
            if(justlist[i].Type_Of_Asset1__c !='' && justlist[i].Asset_Specification1__c !='' && justlist[i].Suppliers_Name1__c !=''){
            justlist[i].Dispenser_Installation__c = di.id;
            }
            else{
            justlist.remove(i);
            }
            }
            upsert justlist;
            for(integer i=0;i<justfylist.size();i++){
            if(justfylist[i].MaterialName__c !=null){
            justfylist[i].Dispenser_Installation__c = di.id;
            }
            else{
            justfylist.remove(i);
            }
            }
            upsert justfylist;
            PageReference pg = new PageReference('/'+di.id);
            pg.setRedirect(true);
            return pg;
        }
        catch(DMLException de) {
            system.debug('----------->'+de.getMessage());
            Apexpages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.FATAL, de.getDmlMessage(0)));
            return NULL;
        }
        catch(Exception e) {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.FATAL, e.getMessage()));
            return NULL;
        }
    }
    public void address(){
        
        if(di.Customer_Name1__c != null){
            account acc = [select id,Name, BillingStreet, BillingCity, BillingState, BillingPostalCode,
                           BillingCountry,SAP_Account_Code__c, BillingLatitude, BillingLongitude from account where id =:di.Customer_Name1__c ];
            di.Customer_code__c = acc.SAP_Account_Code__c;
            di.Street__c= acc.BillingStreet;
            di.City__c = acc.BillingCity;
            di.State_Province__c = acc.BillingState;
            di.Zip_Postal_Code__c = acc.BillingPostalCode;
            di.Country__c = acc.BillingCountry;
        }
        
    }
    public void address1(){
        
        if(di.Account__c != null){
            account acc = [select id,Name, BillingStreet, BillingCity, BillingState, BillingPostalCode,
                           BillingCountry, BillingLatitude, BillingLongitude,SAP_Account_Code__c from account where id =:di.Account__c ];
            di.Installation_Street__c= acc.BillingStreet;
            di.Installation_City__c = acc.BillingCity;
            di.Installation_State_Province__c = acc.BillingState;
            di.Installation_Zip_Postal_Code__c = acc.BillingPostalCode;
            di.Installation_Country__c = acc.BillingCountry;
            di.Customer_s_Account_Code__c = acc.SAP_Account_Code__c;
        }
    }
    public PageReference cancel(){
        PageReference pg = new PageReference('/'+di.id);
        return pg;
    }
    
    
}