@isTest
private class sika_SCCZoneUserInfoDisplayComponentTest {

	private static User theUser;
	private static Contact theContact; 
	private static Account theAccount;
	private static User adminUser;

	private static AccountTeamMember SCCZoneAccountTeamMember;
	private static AccountTeamMember BUConcreteAccountTeamMember;
	private static AccountTeamMember otherAccountTeamMember;


	private static void doInitialSetUp() {  

		map<String, String> params = new map<String, String>{
			'account_name' => 'Test Account',
			'contact_firstName' => 'firstName',
			'contact_lastName' => 'lastName',
			'contact_email' => 'e@mail.com',
			'contact_phone' => '202-222-2222'};

		// create an SCCZone Communities User, and its required Contact and Account objects
		map<String, sObject> theObjects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount(params);
		theUser = (User) theObjects.get('SCCZoneUser');
		theContact = (Contact) theObjects.get('theContact');
		theAccount = (Account) theObjects.get('theAccount');
		adminUser =  (User) theObjects.get('adminUser');

		// create some Users to make up the account team for the SCC Zone User's Contact's Account
		list<User> salesReps = new list<User>();
		User SCCZoneRep = sika_SCCZoneTestFactory.createUser(new map<String, String>{'userName' => 'sccZoneATM@sika.com',
																					 'lastName' => 'Rep',
																					 'firstName' => 'SCC Zone',
																					 'email' => 'sccRep@sika.com',
																					 'phone' => '202-222-2222'});
		salesReps.add(SCCZoneRep);
		User BUConcreteRep = sika_SCCZoneTestFactory.createUser(new map<String, String>{'userName' => 'BUConcreteATM@sika.com',
																						'lastName' => 'Concrete',
																					 	'firstName' => 'BU',
																					 	'email' => 'bu@sika.com',
																					 	'phone' => '202-222-3333'});
		salesReps.add(BUConcreteRep);
		User OtherRep = sika_SCCZoneTestFactory.createUser(new map<String, String>{'userName' => 'otherATM@sika.com'});
		salesReps.add(OtherRep);

		insert(salesReps);
		salesReps = null;

		// assign reps to the SCC Zone user's Account as AccountTeamMembers
		list<AccountTeamMember> ATMs = new list<AccountTeamMember>();

		SCCZoneAccountTeamMember = new AccountTeamMember();
		SCCZoneAccountTeamMember.AccountId = theAccount.Id;
		SCCZoneAccountTeamMember.UserId = SCCZoneRep.Id;
		SCCZoneAccountTeamMember.TeamMemberRole = Sika_SCCZone_Common.SCCZoneSalesRepATMRole;
		ATMs.add(SCCZoneAccountTeamMember);

		BUConcreteAccountTeamMember = new AccountTeamMember();
		BUConcreteAccountTeamMember.AccountId = theAccount.Id;
		BUConcreteAccountTeamMember.UserId = BUConcreteRep.Id;
		BUConcreteAccountTeamMember.TeamMemberRole = 'Concrete BU';
		ATMs.add(BUConcreteAccountTeamMember);

		otherAccountTeamMember = new AccountTeamMember();
		OtherAccountTeamMember.AccountId = theAccount.Id;
		OtherAccountTeamMember.UserId = OtherRep.Id;
		OtherAccountTeamMember.TeamMemberRole = 'Some Other role';
		ATMs.add(OtherAccountTeamMember);

		// insert ATMs in a different context to avoid MIXED_DML_OPERATION error
		system.runAs(adminUser){
			insert(ATMs);
		}
		ATMs = null;
	}

	@isTest static void test_method_one() {
		
		doInitialSetUp();

		test.startTest();
		
		// set page we'll be testing
		PageReference pageRef = new PageReference('/sika_SCCZoneUserHomePage');
		Test.setCurrentPage(pageRef);

		// run as SCC Zone communities user
		System.runAs(theUser){

			// create an instance of the controller
			sika_SCCZoneUserInfoDisplayComponentCtrl ctrl = new sika_SCCZoneUserInfoDisplayComponentCtrl();

			// confirm that the page loaded. (Didn't redirect to an error page)
			system.assertEquals(pageRef.getURL(), ApexPages.CurrentPage().getURL());

			// confirm that the page is displaying info for the current user
			Id currentUserId = UserInfo.getUserId();
			system.assertEquals(currentUserId, theUser.Id);

			// confirm that the user info we're displaying is as expected
			system.assertEquals(ctrl.name, 'firstName lastName');
			system.assertEquals(ctrl.email, 'e@mail.com');
			system.assertEquals(ctrl.phone, '202-222-2222');

			system.assertEquals(ctrl.accountName, 'Test Account');

			system.assertEquals(ctrl.repName, 'SCC Zone Rep');
			system.assertEquals(ctrl.repFirstName, 'SCC Zone');
			system.assertEquals(ctrl.repPhone, '202-222-2222');
			system.assertEquals(ctrl.repEmail, 'sccrep@sika.com');

			// remove the SCC Zone Rep from the account team and confirm that the BU rep's info is displayed
			system.runAs(adminUser){
				delete(SCCZoneAccountTeamMember);
			}

			ctrl = new sika_SCCZoneUserInfoDisplayComponentCtrl();

			system.assertEquals(ctrl.repName, 'BU Concrete');
			system.assertEquals(ctrl.repFirstName, 'BU');
			system.assertEquals(ctrl.repPhone, '202-222-3333');
			system.assertEquals(ctrl.repEmail, 'bu@sika.com');

			// remove the BU Rep from the account team and confirm that no rep info is displayed
			system.runAs(adminUser){
				delete(BUConcreteAccountTeamMember);
			}

			ctrl = new sika_SCCZoneUserInfoDisplayComponentCtrl();

			system.assertEquals(ctrl.repName, '');
			system.assertEquals(ctrl.repFirstName, '');
			system.assertEquals(ctrl.repPhone, '');
			system.assertEquals(ctrl.repEmail, '');

		}
		test.stopTest();

	}
		
}