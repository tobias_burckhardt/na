@isTest
public class RequestForProjectApprovalServicesTest {
	private static final Integer TEST_AMOUNT_NUMBER = 50;
	private static final String RPA_APPROVAL_PROCESS_NAME = 'RPA_Standard_Approval';

	static Map<Id, User> setupApprovals(List<Request_for_Project_Approval__c> rpaList){
		List<User> actorList = (List<User>) new SObjectBuilder(User.SObjectType)
			.put(User.FirstName, new SObjectFieldProviders.UniqueStringProvider('john'))
			.count(rpaList.size()).create().getRecords();

        actorList = [SELECT Name FROM User WHERE Id IN :actorList];			

		Map<Id, User> rpaIdToApprover = new Map<Id, User>();

		// Submit RPAs for Approval
		List<Approval.ProcessSubmitRequest> reqs = new List<Approval.ProcessSubmitRequest>();
		for(Integer i = 0; i < rpaList.size(); i++){
			Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
			req.setComments('Approve');
			req.setNextApproverIds(new List<Id>{actorList[i].Id});
			req.setObjectId(rpaList[i].Id);
			req.setProcessDefinitionNameOrId(RPA_APPROVAL_PROCESS_NAME);
			req.setSkipEntryCriteria(true);
			reqs.add(req);
			rpaIdToApprover.put(rpaList[i].Id, actorList[i]);
			rpaList[i].Approver_1__c = actorList[i].Id;
		}
		update rpaList;
		Approval.process(reqs);
		return rpaIdToApprover;
	}

	static testmethod void findCurrentApproversTest(){
		List<Request_for_Project_Approval__c> rpaList = (List<Request_for_Project_Approval__c>) new SObjectBuilder(Request_for_Project_Approval__c.SObjectType)
			.count(TEST_AMOUNT_NUMBER).create().getRecords();

		Map<Id, User> rpaIdToApprover = setupApprovals(rpaList);

        Test.startTest();
        Map<Id, String>testResultMap = RequestForProjectApprovalServices.findCurrentApprovers(Pluck.ids(rpaList));
        Test.stopTest();

        System.assertEquals(TEST_AMOUNT_NUMBER, testResultMap.size(), 'Map size should be equal to the amount of test record we created.' );

        for(Id rpaId : testResultMap.keySet()){
        	System.assertEquals(rpaIdToApprover.get(rpaId).Name, testResultMap.get(rpaId), 'Unexpected approver found for RPA of Id ' + rpaId);
        }
	}
}