public with sharing class CommunityMyAccountsController {
        
        public List<Account> acctList {get; set;}
    
    public string uid {get; set;}
     List<user> adminUser;
    public CommunityMyAccountsController(){
        //get current user id
        string userId = UserInfo.getUserId();
        uid = userId;
        adminUser = [select id, name from user where profile.name='System Administrator' limit 1];
        //load all accounts for this user
        string acctFieldList = Utilities.GetAllObjectFields('Account');
        string queryFormat = 'select {0} from Account where  Community_User_ID__c = :userId';
        string acctQuery = String.format(queryFormat, new List<string>{acctFieldList});
        
        try{
            acctList = Database.query(acctQuery);
        }
        catch(Exception e){
            acctList = new List<Account>();
        }
    }
    public void deleteAccount(){
            String index = ApexPages.currentPage().getParameters().get('AccRecordNum');
            Account AcctoDelete = acctList.get(integer.valueof(index));
            
            delete AcctoDelete ;
            acctList.remove(integer.valueof(index));
        }

}