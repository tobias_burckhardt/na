public class sika_SCCZoneMixesHomePageController {
        
    public Id userID {get; set;}
    public string userEmail {get; set;}
    public Id mixID {get; set;}
    public list<Mix__c> theMixes {get; set;}
    public list<Mix__c> myMixes {get; set;}
    public list<Mix__c> compMixes {get; set;}
    public Mix__c newMix {get; set;}
    public list<Mix_Material__c> mixMaterialList {get; set;}
    public list<Test_Result__c> testResultsList {get; set;}
    public String selectOption {get; set;}
    public String metric {get; set;}
    public list<SelectOption> tableOptions {get; set;}
    public list<Attachment> attachmentList {get; set;}
    
    public Boolean displayPopUp {get; set;}
    public Boolean displayPopUp2 {get; set;}
    public Boolean displayPopUp3 {get; set;}
    public Boolean displayPopUp4 {get; set;}
    public Attachment attachment {get; set;}
    
    public Boolean hasErrors {get; set;}
    
    public String nameError {get; set;}
    public String maxError {get; set;}
    public String reinforcementError {get; set;}
    public String aggError {get; set;}
    public String cementError {get; set;}
    public String strengthError {get; set;}
    public String targetScmError {get; set;}
    public String targetAirError {get; set;}
    public String targetWaterError {get; set;}
    public String targetStrengthError {get; set;}
    public String targetPasteError {get; set;}
    
    public Boolean goToMixMaterial {get; set;}
    
    public Decimal rawMaterialsCostDisp {get; set;}
    public User theUser {get; set;}

    public sika_SCCZoneMixesHomePageController(){        
        sika_SCCZoneCheckAuth.setAuthStatus(UserInfo.getProfileId());
        
        userID = UserInfo.getUserId();
        userEmail = UserInfo.getUserEmail();
        mixID = Apexpages.currentPage().getParameters().get('mixID');
        
        metric = Apexpages.currentPage().getParameters().get('metric');
        
        selectOption = Apexpages.currentPage().getParameters().get('view');

        if(selectOption == null){
            selectOption = 'me';
            Apexpages.currentPage().getParameters().put('view', 'me');
        }
        
        displayPopUp = false;
        displayPopUp2 = false;
        displayPopUp3 = false;
        displayPopUp4 = false;
        hasErrors = false;
        nameError = null;
        maxError = null;
        reinforcementError = null;
        aggError = null;
        cementError = null;
        strengthError = null;
        targetScmError = null;
        targetAirError = null;
        targetWaterError = null;
        targetPasteError = null;
        goToMixMaterial = false; 
        
        tableOptions = new List<SelectOption>();
        tableOptions.add(new SelectOption('me','My Mixes'));
        tableOptions.add(new SelectOption('Company Mixes','Company Mixes'));  
        
        theUser = [Select Id, 
                          AccountID, 
                          ContactID, 
                          System_of_Measure__c 
                     FROM User 
                    WHERE Id = :userID Limit 1];
        
        theMixes = [SELECT Id, 
                            OwnerID, 
                            Mix_Sheet_Workflow_Start__c,
                            isLocked__c, 
                            Account__c,
                            Mix_Name__c, 
                            External_Notes__c, 
                            Internal_Notes__c,
                            Contractor__c, 
                            Project__c, 
                            Water_Cement_Ratio_Calculated__c, 
                            Cement_Comp_Strength__c, 
                            Concrete_Compressive_Strength_DSP__c,
                            Calculated_Raw_Material_Cost__c, 
                            LastModifiedDate 
                        FROM Mix__c 
                        WHERE OwnerID = :userID OR Account__c =: theUser.AccountID 
                        ORDER BY Mix_Name__c];
        
        myMixes = new list<Mix__c>();
        compMixes = new list<Mix__c>();
        
        for(Mix__c m : theMixes){
            if(m.OwnerID == userID){
                myMixes.add(m);
            }
            else{
                compMixes.add(m);
            }
        }
        
        if(mixID != null){
            newMix = [Select Id, External_Notes__c, 
                                Internal_Notes__c, 
                                isLocked__c,
                                OwnerID,
                                Mix_Name__c, 
                                Account__c,
                                Contractor__c, 
                                Total_Volume_Label__c, 
                                Target_Paste_Volume_DSP__c, 
                                Paste_Volume_Label__c, 
                                Project__c, 
                                Cement_Comp_Strength__c, 
                                Total_Volume_DSP__c,
                                Max_Agg_Diameter__c, 
                                Target_Paste_Volume_PKLST__c, 
                                Target_Volume_Auto__c,
                                Reinforcement__c, 
                                Agg_Efficiency_Factor__c, 
                                Optimize_for__c, 
                                System_of_Measure__c, 
                                isOptimized__c, 
                                Calculated_Raw_Material_Cost__c, 
                                Mix_Unit_Weight__c, 
                                PasteVolumeNoAir__c, 
                                Target_Paste_Volume__c, 
                                Water_Cement_Ratio_Calculated__c, 
                                Target_Air_Percent__c, 
                                Target_SCM_Ratio__c, 
                                Total_Volume_liters__c, 
                                Target_Volume__c, 
                                Target_Water_Cement_Ratio__c, 
                                Concrete_Target_Compressive_Strength__c, 
                                Concrete_Compressive_Strength_Label__c,
                                Water_Adjustment__c, 
                                LastModifiedDate                            
                           FROM Mix__c  
                          WHERE ID =: mixID];
        
            mixMaterialList = [SELECT ID, Unit__c, 
                                          Material_Name__c, 
                                          Moisture__c, 
                                          Material_Type__c, 
                                          Material__r.Type__c,
                                          Material__r.Source__c, 
                                          Material__r.Spec_Gravity__c, 
                                          Material__r.Abs__c, 
                                          Quantity__c, 
                                          Volume__c, 
                                          Material__c, 
                                          Cost__c, 
                                          Moisture_Adjustment__c, 
                                          Batch_Weight__c
                                     FROM Mix_Material__c 
                                    WHERE Mix__c =: newMix.ID];
                                    
            testResultsList = [SELECT ID, Name, 
                                      Mix__c, 
                                      Tester__c, 
                                      Mixing_Date__c, 
                                      Initial_Test_Date__c, 
                                      Test_Location__c, 
                                      LastModifiedDate 
                                 FROM Test_Result__c 
                                WHERE Mix__c =: mixID Order By LastModifiedDate DESC];
            
            attachmentList = [SELECT ID, Name, 
                                     OwnerId, 
                                     Description 
                                FROM Attachment 
                               WHERE ParentID =: newMix.ID];
            
            attachment = new Attachment();
        
            rawMaterialsCostDisp = newMix.Calculated_Raw_Material_Cost__c != null ? (newMix.Calculated_Raw_Material_Cost__c).setScale(2) : 0.00;
        
        } else {  
            newMix = new Mix__c(OwnerID = userID, 
                                System_Of_Measure__c = metric, 
                                Account__c = theUser.AccountID);
        
            if(metric == 'US Customary'){
                newMix.Target_Volume__c = 27;
            } else {
                newMix.Target_Volume__c = 1000;
            }
            
            mixMaterialList = new list<Mix_Material__c>();
            testResultsList = new list<Test_Result__c>();
            attachmentList = new list<Attachment>();
            attachment = new Attachment();
        }
        
    }

   
    // because this controller runs "without sharing" we need to manually confirm that the user should have access. (A user whose session has timed-out, or an otherwise unauthenticated user who accessed this page directly, would be able to view the page otherwise, albeit without any data displayed.)
    public PageReference loadAction(){
        return sika_SCCZoneCheckAuth.doAuthCheck();
    }

    public Boolean optimizationSucceeded;
    private map<String, String> optimizationResponse;
    @testVisible private map<String, String> testResponseMap = new map<String, String>();

    public PageReference optimizeMix(){

        list<Mix_Material__c> waterTempList = new list<Mix_Material__c>();
        list<Mix_Material__c> cementTempList = new list<Mix_Material__c>();
        list<Mix_Material__c> coarseTempList = new list<Mix_Material__c>();
        list<Mix_Material__c> sandTempList = new list<Mix_Material__c>();
        set<Id> mixMaterialIDs = new set<Id>();
        hasErrors = false;
        if(mixMaterialList.size() >= 3){
          
          for(Mix_Material__c mm: mixMaterialList){
              if(mm.Material__r.Type__c=='Water'){
                waterTempList.add(mm);
              } 
              else if(mm.Material__r.Type__c=='Cement'){
                cementTempList.add(mm);
              } 
              else if(mm.Material__r.Type__c=='Coarse Aggregate'){
                coarseTempList.add(mm);
              }    //Water, Cement, (Coarse or Sand)
              else if(mm.Material__r.Type__c == 'Sand'){
                sandTempList.add(mm);
              }
              else{

              }
              // make a set of Mix_Material IDs to pass to the doOptimize method
              mixMaterialIDs.add(mm.Id);
          }
          
            // mixes must contain at least one water, one cement, and (one sand or one course aggregate).
            if(waterTempList.size() > 0 && cementTempList.size() > 0 && (coarseTempList.size() > 0 || sandTempList.size() > 0)){
            
                if(Test.isRunningTest() == false){
                    // if we are running for realz -- OPTIMIZE!!
                    optimizationResponse = sika_SCCZoneOptimizeMix.doOptimize(newMix.Id, mixMaterialIDs);  // pass in the Mix, and the set of Mix_Material IDs in the Mix.
                } else {
                    // if we're just testing, return a map of responses defined by the test method
                    optimizationResponse = new map<String, String>(testResponseMap);
                }
                
                if(optimizationResponse.get('passFail') == 'pass'){
                    hasErrors = false;
                    PageReference newocp = new PageReference('/SCCZone/apex/sika_SCCZoneMixDetailsPage?mixID=' + newMix.ID);
                    newocp.setRedirect(true);
                    return newocp;
                } else {
                    hasErrors = true;
                    String errorString = 'Error ' + optimizationResponse.get('statusCode') + ': Optimization failed -- ' + optimizationResponse.get('status') + '.';
                    errorString += '<br/>';
                    errorString += 'Please submit a support ticket referencing Error code ' + optimizationResponse.get('statusCode') + ' and<br/>Mix #' + newMix.Id + ' if this problem persists.';
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, errorString));
                    return null;
                }
            } else {
                //set error boolean to true
                hasErrors = true;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'A mix cannot be optimized until it contains at least one water, one cement, and one coarse aggregate or sand.'));
                return null;
            }

        } else {
            hasErrors = true;
            //return null and error - not enough mix materials
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'A mix cannot be optimized until it contains at least one water, one cement, and one coarse aggregate or sand.'));
            return null;
        }

      
    }


    public PageReference cloneMix(){
        Mix__c clonedMix = newMix.clone(false,false);
        clonedMix.isCloned__c = true;
        clonedMix.isLocked__c = false;
        clonedMix.Mix_Name__c = clonedMix.Mix_Name__c + ' (copy)';
        insert clonedMix;  
        
        list<Mix_Material__c> clonedMixMaterialList = mixMaterialList.deepclone(false, false, false);
        for(Mix_Material__c mm : clonedMixMaterialList){
            mm.Mix__c = clonedMix.ID;
        }
        insert clonedMixMaterialList;    
        
        PageReference newocp = new PageReference('/SCCZone/apex/sika_SCCZoneNewMixPage?mixID=' + clonedMix.ID);
        newocp.setRedirect(true);
        return newocp;
    
    }


    public void closePopup() {        
        displayPopup = false;    
    } 
        
    public void showPopup() { 
        newMix.System_of_Measure__c = theUser.System_of_Measure__c;        
        displayPopup = true;
    }
    
    public void closePopup2() {        
        displayPopup2 = false;    
    } 
        
    public void showPopup2() {        
        displayPopup2 = true;    
    }
    public void closePopup3() {        
        displayPopup3 = false;    
    } 
        
    public void showPopup3() {        
        displayPopup3 = true;    
    }
    
    public void closePopup4() {        
        displayPopup4 = false;    
    } 
        
    public void showPopup4() {        
        displayPopup4 = true;    
    }

    public PageReference optionUpdate(){
        PageReference newocp;
        
        if(selectOption=='comp'){
            newocp = new PageReference('/SCCZone/apex/sika_SCCZoneMixesHomePage?view=comp');
        } else {
            newocp = new PageReference('/SCCZone/apex/sika_SCCZoneMixesHomePage?view=me');
        }
        newocp.setRedirect(true);
        return newocp;
        
    }


    public PageReference createNewMix(){
        PageReference newocp;
        if(mixID != null){
            newocp = new PageReference('/SCCZone/apex/sika_SCCZoneNewMixPage?mixID=' + mixID);
        } else {    
            newocp = new PageReference('/SCCZone/apex/sika_SCCZoneNewMixPage?metric=' + newMix.System_Of_Measure__c);
        }
        newocp.setRedirect(true);
        return newocp;
    
    }

    public PageReference cancelNewMix(){
        PageReference newocp;
        if(mixID != null){
            newocp = new PageReference('/SCCZone/apex/sika_SCCZoneMixDetailsPage?mixID=' + mixID);
            return newocp;
        } else {
            newocp = new PageReference('/SCCZone/apex/sika_SCCZoneMixesHomePage?view=me');
            newocp.setRedirect(true);
            return newocp;
        }
    
    }

    public PageReference manageMaterials(){
        PageReference newocp = new PageReference('/SCCZone/apex/sika_SCCZoneManageMixMaterialsPage2?mixID=' + newMix.ID);
        newocp.setRedirect(true);
        return newocp;
    
    }

    public PageReference newTestResult(){
        PageReference newocp = new PageReference('/SCCZone/sika_SCCZoneTestNewEditPage');
        newocp.getParameters().put('mixID', String.valueOf(newMix.ID));

        newocp.setRedirect(true);
        return newocp;
    
    }

    // delete a mix or a material when the correspinding delete link is clicked
    public String deleteThisMix {get; set;}
    
    public PageReference deleteMix(){
        list<Mix__c> theMix = [SELECT Id FROM Mix__c WHERE Id = :deleteThisMix Limit 1];
        delete(theMix[0]);
        
        PageReference newocp = new PageReference('/SCCZone/apex/sika_SCCZoneMixesHomePage?view=' + selectOption);
        newocp.setRedirect(true);
        return newocp;
    }
    
    public String deleteAttachmentID {get; set;}

    public PageReference deleteAttachment(){
        list<Attachment> attach = [SELECT Id FROM Attachment WHERE Id = :deleteAttachmentID Limit 1];
        delete(attach[0]);
        
        PageReference newocp = new PageReference('/SCCZone/apex/sika_SCCZoneMixDetailsPage?mixID=' + newMix.ID);
        newocp.setRedirect(true);
        return newocp;
    }
    
    public PageReference deleteMixDetail(){
        delete newMix;
        
        PageReference newocp = new PageReference('/SCCZone/apex/sika_SCCZoneMixesHomePage?view=me');
        newocp.setRedirect(true);
        return newocp;
    }
    
    public String deleteThisTest {get; set;}
        public PageReference deleteTest(){
        pageReference p;
        
        list<Test_Result__c> theTest = [SELECT Id FROM Test_Result__c WHERE Id = :deleteThisTest Limit 1];
        
        try {
            if(theTest.size() > 0){
                delete(theTest[0]);
            }
            p = Page.sika_SCCZoneMixDetailsPage;
            p.getParameters().put('mixID', mixID);
        
            p.setRedirect(true);
            return p;
        } catch (DmlException e) {
            Apexpages.addMessage(new Apexpages.message(ApexPages.Severity.Error,e.getMessage()));
            return null;
        }
    
    }
 
        
    public PageReference uploadAttachment() {
        displayPopUp3 = false;
        //attachment.Body = Blob.valueOf(attachment.Name);
        attachment.ParentID = mixID;
        attachment.OwnerID = userID;
        attachment.isPrivate = false;
        
        try {
            insert attachment;
        } catch (DMLException e) {
            hasErrors = true;
            return null;
        } finally {
            attachment = new Attachment(); 
            attachment.body = null;
        }
        
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
        
        PageReference newocp = new PageReference('/SCCZone/apex/sika_SCCZoneMixDetailsPage?mixID=' + newMix.ID);
        newocp.setRedirect(true);
        return newocp;
    }
          
          
    @testVisible private Database.UpsertResult mixSaveResult;

    public PageReference saveNewMix2(){
       
        hasErrors = false;
        nameError = '';
        maxError = '';
        reinforcementError = '';
        aggError = '';
        cementError = '';
        strengthError = '';
        targetScmError = '';
        targetAirError = '';
        targetWaterError = '';
        targetStrengthError = '';
        targetPasteError = '';
  
        if(newMix.Mix_Name__c == null || newMix.Mix_Name__c == '') {
            nameError = 'Please enter a name for your mix:';
            hasErrors = true;
        }

        if(newMix.Max_Agg_Diameter__c == null || newMix.Max_Agg_Diameter__c == '--None--' || newMix.Max_Agg_Diameter__c == 'None'){
            maxError = 'Please select a max coarse aggregate diameter:';
            hasErrors = true;
        } 

        if(newMix.Reinforcement__c == null || newMix.Reinforcement__c == ''){
            reinforcementError = 'Please select a reinforcement level:';
            hasErrors = true;
        }  

        if(String.isBlank(String.valueOf(newMix.Agg_Efficiency_Factor__c))){
            aggError = 'Please enter an aggregate efficiency factor:';
            hasErrors = true;
        } 

        // cement compressive strength must be >= 0 and <= 70 MPa or > 50psi
        if(newMix.System_of_Measure__c == 'Metric (SI)'){
            if(newMix.Cement_Comp_Strength__c < 0 || newMix.Cement_Comp_Strength__c > 70 || newMix.Cement_Comp_Strength__c == null){
                cementError = 'Please enter a compressive strength value between 0 and 70 MPa:';
                hasErrors = true;
            } 
        } 
        else {
            if(newMix.Cement_Comp_Strength__c < 0 || newMix.Cement_Comp_Strength__c > 10000 || newMix.Cement_Comp_Strength__c == null){
                cementError = 'Please enter a compressive strength value between 0 and 10,000 psi:';
                hasErrors = true;
            } 
        }
        
        if(newMix.Optimize_for__c == null || newMix.Optimize_for__c == ''){
            strengthError = 'Please select an optimization goal:';
            hasErrors = true;
        }  

        if(newMix.Target_SCM_Ratio__c < 0 || newMix.Target_SCM_Ratio__c  > 1 || newMix.Target_SCM_Ratio__c == null){  
            targetScmError = 'Please enter an SCM Ratio between 0 and 1:';
            hasErrors = true;
        }

        if(newMix.Target_Air_Percent__c < 0 || newMix.Target_Air_Percent__c > 100 || newMix.Target_Air_Percent__c == null){
            targetAirError = 'Please enter a target Air %:';
            hasErrors = true;
        } 

        if(newMix.Target_Water_Cement_Ratio__c < 0 || newMix.Target_Water_Cement_Ratio__c > 1 || newMix.Target_Water_Cement_Ratio__c == null){
            targetWaterError = 'Please enter a W/C Ratio between 0 and 1:';
            hasErrors = true;
        }

        // concrete compressive strength target must be >= 0 and <= 70 MPa or > 70psi
        if(newMix.System_of_Measure__c == 'Metric (SI)'){
            if(newMix.Concrete_Target_Compressive_Strength__c < 0 || newMix.Concrete_Target_Compressive_Strength__c > 70 || newMix.Concrete_Target_Compressive_Strength__c == null){
                targetStrengthError = 'Please enter a compressive strength value between 0 and 70 MPa:';
                hasErrors = true;
            } 
        } 

        else {
            if(newMix.Concrete_Target_Compressive_Strength__c < 0 || newMix.Concrete_Target_Compressive_Strength__c > 10000 || newMix.Concrete_Target_Compressive_Strength__c == null){
                targetStrengthError = 'Please enter a compressive strength value between 0 and 10,000 psi:';
                hasErrors = true;
            } 
        }

        if(newMix.Target_Paste_Volume_PKLST__c == null){
            targetPasteError = 'Please select a Paste Volume:';
            hasErrors = true;
        }

        if(hasErrors==true){
            system.debug('********ERROR: Validation errors found:');
            system.debug('********nameError = ' + nameError);
            system.debug('********maxError = ' + maxError);
            system.debug('********reinforcementError = ' + reinforcementError);
            system.debug('********aggError = ' + aggError);
            system.debug('********cementError = ' + cementError);
            system.debug('********strengthError = ' + strengthError);
            system.debug('********targetScmError = ' + targetScmError);
            system.debug('********targetAirError = ' + targetAirError);
            system.debug('********targetWaterError = ' + targetWaterError);
            system.debug('******** targetStrengthError = ' + targetStrengthError);
            system.debug('********targetPasteError = ' + targetPasteError);
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please fill in all of the fields highlighted below.'));
            return null;
        } else {
            mixSaveResult = Database.upsert(newMix);
            
            if (mixSaveResult.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully upserted mix. Mix ID: ' + mixSaveResult.getId());
            } else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please fill in all of the fields highlighted below.'));
                hasErrors = true;
                return null;
            }
        }

        // redirect the user to the appropriate page
        PageReference newocp;
        if(goToMixMaterial==true){
          newocp = new PageReference('/SCCZone/apex/sika_SCCZoneManageMixMaterialsPage2?mixID=' + newMix.ID);
        } else {
          newocp = new PageReference('/SCCZone/apex/sika_SCCZoneMixDetailsPage?mixID=' + newMix.ID);
        }
        newocp.setRedirect(true);
        return newocp;
    }


    // kicks off the "generate mix sheet" workflow  by setting the Mix_Sheet_Workflow_Start__c value of the Mix to true
    public void startMixSheetWorkflow(){
        Mix__c updateMe = new Mix__c();
        updateMe.Id = mixID;
        updateMe.Mix_Sheet_Workflow_Start__c = true;
        
        update(updateMe);
    }
    

    public String sortField {get; set;}
    private String nameSortDir = 'DESC';  // this will sort the name column alphabetically in ASCending order on the first click.  (This value gets switched to the inverse before the sort is performed each time, so an initial setting of DESC means the first sort will be in ASCending order.)
    private String contractorSortDir = 'DESC';
    private String projectSortDir = 'DESC';
    private String waterCementSortDir = 'ASC';  // seems more intuitive to sort numbers descending initially.
    private String strengthSortDir = 'ASC';
    private String costSortDir = 'ASC';
    private String lockedSortDir = 'ASC';
    
    public void tableSort(){
        list<Mix__c> tempList = new list<Mix__c>();
        
        if(selectOption == 'me'){
            tempList.addAll(myMixes);
            myMixes.clear();
            myMixes.addAll((list<Mix__c>) sika_SCCZoneSortListHelper.sortList(tempList, sortField, sortDirTracker()));
        } else {
            tempList.addAll(theMixes);
            theMixes.clear();
            theMixes.addAll((list<Mix__c>) sika_SCCZoneSortListHelper.sortList(tempList, sortField, sortDirTracker()));
        }
    
    } 
    

    public String sortDirTracker(){
        if(sortField == 'Mix_Name__c'){
            nameSortDir = sortDir(nameSortDir);
            return nameSortDir;
        } 
        if(sortField == 'Project__c'){
            projectSortDir = sortDir(projectSortDir);
            return projectSortDir;
        } 
        if(sortField == 'Water_Cement_Ratio_Calculated__c'){
            waterCementSortDir = sortDir(waterCementSortDir);
            return waterCementSortDir;
        } 
        if(sortField == 'Concrete_Compressive_Strength_DSP__c'){
            strengthSortDir = sortDir(strengthSortDir);
            return strengthSortDir;
        } 
        if(sortField == 'Calculated_Raw_Material_Cost__c'){
            costSortDir = sortDir(costSortDir);
            return costSortDir;
        } 
        if(sortField == 'isLocked__c'){
            lockedSortDir = sortDir(lockedSortDir);
            return lockedSortDir;
        }
        return 'ASC';
    
    }

    private String sortDir(String dir){
        if(dir == 'ASC'){
            return 'DESC';
        }
        return 'ASC';
    }

}