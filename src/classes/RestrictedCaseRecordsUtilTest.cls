@isTest
private class RestrictedCaseRecordsUtilTest {
        
    @testSetup
    static void setUpTests() {
        RestrictedCaseTestHelper.createCustomSettings();
    }
    
	@isTest
    static void testRecordTypeRestricted() {
        Case restrictedCase = RestrictedCaseTestHelper.createTestCase(true);
        boolean result = RestrictedCaseRecordsUtil.isRestrictedCaseRecordType(restrictedCase.Id, 'RFMigrated');
        System.assertEquals(true, result);
    }
    
    @isTest
    static void testRecordTypeUnrestricted() {
        Case unrestrictedCase = RestrictedCaseTestHelper.createTestCase(false);
        boolean result = RestrictedCaseRecordsUtil.isRestrictedCaseRecordType(unrestrictedCase.Id, 'Technical_Services_Case');
        System.assertEquals(false, result);
    }
    
    @isTest
    static void testRecordTypeRestrictedNullId() {
        Case unrestrictedCase = RestrictedCaseTestHelper.createTestCase(false);
        boolean result = RestrictedCaseRecordsUtil.isRestrictedCaseRecordType(null, 'RFMigrated');
        System.assertEquals(false, result);
    }
    
    @isTest
    static void testCheckFieldsNullValues() {
        sObject oldObj = null;
        sObject newObj = null;
        Schema.FieldSet fieldset = null;
        boolean result = RestrictedCaseRecordsUtil.checkUpdatedFields(oldObj, newObj, '');
        System.assertEquals(true, result);
        result = RestrictedCaseRecordsUtil.checkUpdatedFields(oldObj, newObj, fieldset);
        System.assertEquals(true, result);
    }
}