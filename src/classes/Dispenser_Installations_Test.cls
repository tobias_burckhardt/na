@isTest
public class Dispenser_Installations_Test {
    public testmethod static void dnew(){
       // Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
       // User userToCreate = new User(FirstName ='Test',LastName ='Case1',Email ='gsk260@gmail.com.uat',Username  = 'gsk260@gmail.com.uat',Alias = 'Test123',ProfileId = p.id,/***userroleid = r.id,**/TimeZoneSidKey = 'America/Denver',LocaleSidKey = 'en_US',EmailEncodingKey = 'UTF-8',LanguageLocaleKey = 'en_US');
      //  insert userToCreate; 
      //  system.runAs(userToCreate){
            Region__c rNew = new Region__c(name='Concrete Central',Address__c = '12345 TestStreet', City__c='TestCity', State__c='TestState', Zip__c='12345', Sales_Person__c = UserInfo.getUserId());
            insert rNew;
            Product2 pr = new Product2(name='test product',Projected_Sales_Volume__c=5,Excepted_Selling_Price__c=6,Estimated_Annual_Sales__c=7,Standard_Cost__c=8,Gross_Margin__c=9,Gross_Margin_In_Percentage__c=10,MTART__c='HAWA');
            insert pr;
            account acc = new account(name='test3',BillingStreet = 'test', BillingCity='test', BillingState='test', BillingPostalCode='523320',
                                      BillingCountry='test',SAP_Account_Code__c='test');
            insert acc;
            Dispenser_Installation__c di = new Dispenser_Installation__c(New_Account__c='NO',Customer_Name1__c=acc.id,Account__c=acc.id,Post_Date__c=system.today(),Region__c=rNew.ID,Type_of_Installation__c='New',Real_Cost__c=20,Budget_Cost__c = 20);
            di.Customer_Name1__c=acc.id;
            insert di;
            Asset_Type__c ast = new Asset_Type__c(Type_Of_Asset1__c='Air line Fitting',Asset_Specification1__c='1/2" Od Tube X Tube Union',Suppliers_Name1__c='Westvale',Quantity__c=2,Purchase_Cost__c=3,Total_Cost__c=6,Dispenser_Installation__c=di.Id);
            
            insert ast;
            Concrete_Dispenser_Product__c cdp = new Concrete_Dispenser_Product__c(MaterialName__c=pr.Id,Projected_Sales_Volume__c = pr.Projected_Sales_Volume__c,Excepted_Selling_Price__c = pr.Excepted_Selling_Price__c,Estimated_Annual_Sales__c = pr.Estimated_Annual_Sales__c,Standard_Cost__c = pr.Standard_Cost__c,Gross_Margin__c = pr.Gross_Margin__c,Gross_Margin_In_Percentage__c = pr.Gross_Margin_In_Percentage__c,Dispenser_Installation__c=di.Id);
            insert cdp;
            ApexPages.StandardController controller = new ApexPages.StandardController(di);
            Dispenser_Installations din = new Dispenser_Installations(controller);
        din.di.Account__c=acc.id;
            din.di.Customer_Name1__c=acc.id;
           // din.di.Region__c = rNew.ID;
            din.justlist.add(ast);
            din.justfylist.add(cdp);
            din.Add();
            din.Del();
            din.totalcost();
            din.address();
            din.Add1();
            din.Del1();
            din.productvalue();
            din.productvalue1();
            din.savec();
            din.cancel();
            din.purchasecost();
            din.address1();
            PageReference pageRef = Page.Dispenser_Installation_Edit;
            pageRef.getParameters().put('id', String.valueOf(di.Id));
            Test.setCurrentPage(pageRef);
            Dispenser_Installationctrl_Edit dedit = new Dispenser_Installationctrl_Edit(controller);
            
            dedit.justlist.add(ast);
            dedit.justfylist.add(cdp);
            dedit.Add();
            dedit.Del();
            dedit.totalcost();
            dedit.address();
            dedit.Add1();
            dedit.Del1();
            dedit.productvalue();
            dedit.productvalue1();
            dedit.savec();
            dedit.cancel();
            ApexPages.StandardController sc = new ApexPages.StandardController(ast);
            PageReference pageRef1 = Page.dispenserassert_Editpage;
            pageRef1.getParameters().put('id', String.valueOf(ast.Id));
            Test.setCurrentPage(pageRef1);
            Asserttype_Edit assertedit = new Asserttype_Edit(sc);
            
            assertedit.justlist.add(ast);
            assertedit.justfylist.add(cdp);
            assertedit.Add();
            assertedit.Del();
            assertedit.totalcost();
            assertedit.address();
            assertedit.Add1();
            assertedit.Del1();
            assertedit.productvalue();
            assertedit.productvalue1();
            assertedit.savec();
            assertedit.cancel();
            
            ApexPages.StandardController sc1 = new ApexPages.StandardController(cdp);
            PageReference pageRef2 = Page.dispenserconcret_Editpage;
            pageRef2.getParameters().put('id', String.valueOf(cdp.Id));
            Test.setCurrentPage(pageRef2);
            dispenserconcret_Edit despedit = new dispenserconcret_Edit(sc1);
            
            despedit.justlist.add(ast);
            despedit.justfylist.add(cdp);
            despedit.Add();
            despedit.Del();
            despedit.totalcost();
            despedit.address();
            despedit.Add1();
            despedit.Del1();
            despedit.productvalue();
            despedit.productvalue1();
            despedit.savec();
            despedit.cancel();
       // }
    }
    public testmethod static void dnew1(){
      //  Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
       // User userToCreate = new User(FirstName ='Test',LastName ='Case1',Email ='gsk260@gmail.com.uat',Username  = 'gsk260@gmail.com.uat',Alias = 'Test123',ProfileId = p.id,/***userroleid = r.id,**/TimeZoneSidKey = 'America/Denver',LocaleSidKey = 'en_US',EmailEncodingKey = 'UTF-8',LanguageLocaleKey = 'fr');
       // insert userToCreate; 
       // system.runAs(userToCreate){
            Region__c rNew = new Region__c(name='Concrete Central',Address__c = '12345 TestStreet', City__c='TestCity', State__c='TestState', Zip__c='12345', Sales_Person__c = UserInfo.getUserId());
            insert rNew;
            Product2 pr = new Product2(name='test product',Projected_Sales_Volume__c=5,Excepted_Selling_Price__c=6,Estimated_Annual_Sales__c=7,Standard_Cost__c=8,Gross_Margin__c=9,Gross_Margin_In_Percentage__c=10,MTART__c='HAWA');
            insert pr;
            account acc = new account(name='test2',BillingStreet = 'test', BillingCity='test', BillingState='test', BillingPostalCode='523320',
                                      BillingCountry='test',SAP_Account_Code__c='test');
            insert acc;
            account acc2 = new account(name='test4',BillingStreet = 'test', BillingCity='test', BillingState='test', BillingPostalCode='523320',
                                       BillingCountry='test',SAP_Account_Code__c='test');
            insert acc2;
            Dispenser_Installation__c di = new Dispenser_Installation__c(New_Account__c='NO',Customer_Name1__c=acc.Id,Account__c=acc2.Id,Region__c=rNew.Id,Type_of_Installation__c='New');
            insert di;
            
            Asset_Type__c ast = new Asset_Type__c(Type_Of_Asset1__c='Air line Fitting',Asset_Specification1__c='1/2" Od Tube X Tube Union',Suppliers_Name1__c='Westvale',Quantity__c=2,Purchase_Cost__c=3,Total_Cost__c=6,Dispenser_Installation__c=di.Id);
            insert ast;
            Concrete_Dispenser_Product__c cdp = new Concrete_Dispenser_Product__c(MaterialName__c=pr.Id,Projected_Sales_Volume__c = pr.Projected_Sales_Volume__c,Excepted_Selling_Price__c = pr.Excepted_Selling_Price__c,Estimated_Annual_Sales__c = pr.Estimated_Annual_Sales__c,Standard_Cost__c = pr.Standard_Cost__c,Gross_Margin__c = pr.Gross_Margin__c,Gross_Margin_In_Percentage__c = pr.Gross_Margin_In_Percentage__c,Dispenser_Installation__c=di.Id);
            insert cdp;
            ApexPages.StandardController controller = new ApexPages.StandardController(di);
            Dispenser_Installations din = new Dispenser_Installations(controller);
            din.justlist.add(ast);
            din.justfylist.add(cdp);
         din.di.Customer_Name1__c=acc.id;
            din.di.Region__c = rNew.ID;
            din.Add();
            din.Del();
            din.totalcost();
            din.address();
            din.Add1();
            din.Del1();
            din.productvalue();
            din.productvalue1();
            din.savec();
            din.cancel();
            din.purchasecost();
            din.address1();
            din.acc1=acc2;
            PageReference pageRef = Page.Dispenser_Installation_Edit;
            pageRef.getParameters().put('id', String.valueOf(di.Id));
            Test.setCurrentPage(pageRef);
            Dispenser_Installationctrl_Edit dedit = new Dispenser_Installationctrl_Edit(controller);
            dedit.justlist.add(ast);
            dedit.justfylist.add(cdp);
            dedit.Add();
            dedit.Del();
            dedit.totalcost();
            dedit.address();
            dedit.Add1();
            dedit.Del1();
            dedit.productvalue();
            dedit.productvalue1();
            dedit.savec();
            dedit.cancel();
            dedit.address1();
            ApexPages.StandardController sc = new ApexPages.StandardController(ast);
            PageReference pageRef1 = Page.dispenserassert_Editpage;
            pageRef1.getParameters().put('id', String.valueOf(ast.Id));
            Test.setCurrentPage(pageRef1);
            Asserttype_Edit assertedit = new Asserttype_Edit(sc);
            
            assertedit.justlist.add(ast);
            assertedit.justfylist.add(cdp);
            assertedit.Add();
            assertedit.Del();
            assertedit.totalcost();
            assertedit.address();
            assertedit.Add1();
            assertedit.Del1();
            assertedit.productvalue();
            assertedit.productvalue1();
            assertedit.savec();
            assertedit.cancel();
            ApexPages.StandardController sc1 = new ApexPages.StandardController(cdp);
            PageReference pageRef2 = Page.dispenserconcret_Editpage;
            pageRef2.getParameters().put('id', String.valueOf(cdp.Id));
            Test.setCurrentPage(pageRef2);
            dispenserconcret_Edit despedit = new dispenserconcret_Edit(sc1);
            
            despedit.justlist.add(ast);
            despedit.justfylist.add(cdp);
        
            despedit.Add();
            despedit.Del();
            despedit.totalcost();
            despedit.address();
            despedit.Add1();
            despedit.Del1();
            despedit.productvalue();
            despedit.productvalue1();
            despedit.savec();
            despedit.cancel();
            
    //    }
    }
    public testmethod static void dnew3(){
      //  Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
      //  User userToCreate = new User(FirstName ='Test',LastName ='Case1',Email ='gsk260@gmail.com.uat',Username  = 'gsk260@gmail.com.uat',Alias = 'Test123',ProfileId = p.id,/***userroleid = r.id,**/TimeZoneSidKey = 'America/Denver',LocaleSidKey = 'en_US',EmailEncodingKey = 'UTF-8',LanguageLocaleKey = 'en_US');
      //  insert userToCreate; 
      //  system.runAs(userToCreate){
            Region__c rNew = new Region__c(name='Concrete Central',Address__c = '12345 TestStreet', City__c='TestCity', State__c='TestState', Zip__c='12345', Sales_Person__c = UserInfo.getUserId());
            insert rNew;
            Product2 pr = new Product2(name='test product',Projected_Sales_Volume__c=5,Excepted_Selling_Price__c=6,Estimated_Annual_Sales__c=7,Standard_Cost__c=8,Gross_Margin__c=9,Gross_Margin_In_Percentage__c=10,MTART__c='HAWA');
            insert pr;
            account acc = new account(name='test3',BillingStreet = 'test', BillingCity='test', BillingState='test', BillingPostalCode='523320',
                                      BillingCountry='test',SAP_Account_Code__c='test');
            insert acc;
            Dispenser_Installation__c di = new Dispenser_Installation__c(New_Account__c='yes',Customer_Name1__c=acc.id,Account__c=acc.id,Post_Date__c=system.today(),Region__c=rNew.ID);
            di.Customer_Name1__c=acc.id;
           // insert di;
            Asset_Type__c ast = new Asset_Type__c(Type_Of_Asset1__c='Air line Fitting',Asset_Specification1__c='1/2" Od Tube X Tube Union',Suppliers_Name1__c='Westvale',Quantity__c=2,Purchase_Cost__c=3,Total_Cost__c=6,Dispenser_Installation__c=di.Id);
            
          //  insert ast;
            Concrete_Dispenser_Product__c cdp = new Concrete_Dispenser_Product__c(MaterialName__c=pr.Id,Projected_Sales_Volume__c = pr.Projected_Sales_Volume__c,Excepted_Selling_Price__c = pr.Excepted_Selling_Price__c,Estimated_Annual_Sales__c = pr.Estimated_Annual_Sales__c,Standard_Cost__c = pr.Standard_Cost__c,Gross_Margin__c = pr.Gross_Margin__c,Gross_Margin_In_Percentage__c = pr.Gross_Margin_In_Percentage__c,Dispenser_Installation__c=di.Id);
         //   insert cdp;
            ApexPages.StandardController controller = new ApexPages.StandardController(di);
            PageReference pageRef2 = Page.Dispenser_Installation;
           // pageRef2.getParameters().put('id', String.valueOf(cdp.Id));
            Test.setCurrentPage(pageRef2);
            Dispenser_Installations din = new Dispenser_Installations(controller);
        din.ACTUALINVESTMENT='test';
            din.ESTIMATEDINVESTMENT = 'test';
            din.CustomershiptoNewAccount = 'test';
            din.di.Account__c=acc.id;
            din.di.Customer_Name1__c=acc.id;
            din.di.Region__c = rNew.ID;
          // din.di.New_Account__c = 'Yes';
            din.justlist.add(ast);
            din.justfylist.add(cdp);
            din.acc = acc;
            din.Add();
            din.Del();
            din.totalcost();
            din.address();
            din.Add1();
            din.Del1();
            din.productvalue();
            din.productvalue1();
            din.cancel();
            din.purchasecost();
            din.address1();
            din.savec();
            // din.codecover();
        }
   // }
    
}