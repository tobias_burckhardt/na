public with sharing  class dispenserconcret_Edit {
    public Dispenser_Installation__c  di{get;set;}
    public list<Asset_Type__c> justlist{get;set;}
        public list<Concrete_Dispenser_Product__c> justfylist{get;set;}
    public Integer recCount{get;set;}
    public Integer recCount1{get;set;}
    public integer count{get;set;}
    public integer count1{get;set;}
    public Integer selectedRowIndex{get;set;}
    public Integer selectedRowIndex1{get;set;}
    public list<string> asse{get;set;}
    public dispenserconcret_Edit(ApexPages.StandardController sc1) {
        asse = new list<string>();
        list<Asset_Type__c> ass = [select id,name,Asset_Specification__c,Suppliers_Name__c,Type_Of_Asset__c from Asset_Type__c where Type_Of_Asset__c !=''];
        system.debug('-------------->'+ass.size());
        for(Asset_Type__c as1 : ass){
            asse.add(as1.Type_Of_Asset__c+'/'+as1.name);
        }
        id  did =  ApexPages.currentPage().getParameters().get('id');
        Concrete_Dispenser_Product__c asdi = [select id,name,Dispenser_Installation__c from Concrete_Dispenser_Product__c where id=: did];
        di = [select id,name,Sales_Representative_s_Name__c,Date__c,Dispenser_Tech__c,Region__c,New_Account__c,Customer_code__c,Customer_Name__c,Customer_Address__c,Customer_s__c,Installation_Address__c,Type_of_Installation__c,customer_account__c,Customer_Code_Transfers__c,Site_Address__c,Region_Transfer__c,Total_Estimated_Investment__c,Purchase_Date__c,Installation_Date__c,Justification_For_Upgrade__c,Street__c,City__c,State_Province__c,Zip_Postal_Code__c,Country__c,Installation_Street__c,Customer_Name1__c ,Installation_City__c,Installation_State_Province__c,Installation_Zip_Postal_Code__c,Installation_Country__c ,Real_Cost__c ,Budget_Cost__c ,Last_12_months_Material_Margin__c,Last_12_months_Gross_sales_of_Customer__c,Distance_From_Sika_Facility__c
from Dispenser_Installation__c where id=: asdi.Dispenser_Installation__c];
        justlist = [select id,name,S_No__c,Asset_Specification__c,Suppliers_Name__c,Quantity__c,Total_Cost__c,Purchase_Cost__c,Type_Of_Asset1__c,Suppliers_Name1__c,Asset_Specification1__c from Asset_Type__c where Dispenser_Installation__c =: di.id];
    justfylist = [select id,name,Projected_Sales_Volume__c,Excepted_Selling_Price__c,Estimated_Annual_Sales__c,Standard_Cost__c,Gross_Margin__c,
                      Gross_Margin_In_Percentage__c,MaterialName__c,S_No__c ,Dispenser_Installation__c from Concrete_Dispenser_Product__c where Dispenser_Installation__c =: di.id];
                      if(justfylist.size()>0){
                      recCount1 = justfylist.size();
                      }
                      else{
                      recCount1 =0;
                      }
                       if(justlist.size()>0){
                      recCount = justfylist.size();
                      }
                      else{
                      recCount =0;
                      }
                      
    }
    
    
    public void Add()
    {    
        
        
        recCount+=1;
        Asset_Type__c j = new Asset_Type__c();  
        j.S_No__c=recCount;      
        justlist.add(j);         
        
    }
    
    public void Del()
    {
        system.debug('*****************lect'+selectedRowIndex);
        system.debug('*****************justlist'+justlist);
        
        if(justlist.size()>0)
        {
            system.debug('*****************justlist11111'+justlist);
            for(Integer i=0; i<justlist.size();i++)
            {
                system.debug('*****************recCount'+recCount);
                
                if(justlist[i].S_No__c==selectedRowIndex)
                {
                    count=i;
                    system.debug('*****************i'+i);
                    justlist.remove(i);
                    system.debug('*****************justlist---'+justlist);
                }
            }
            
        }
        
    }
    public void productvalue(){
        
     /***   for(Asset_Type__c js : justlist){
            if(js.Type_Of_Assert__c !=null){
                list<string> tal = new  list<string>();
                tal = js.Type_Of_Assert__c.split('/');
                Asset_Type__c pr = [select id,name,Asset_Specification__c,Suppliers_Name__c,Type_Of_Asset__c from Asset_Type__c where name =: tal[1] limit 1];
                js.Asset_Specification__c = pr.Asset_Specification__c;
                js.Suppliers_Name__c = pr.Suppliers_Name__c;
                //js.Type_Of_Assert__c = tal[0];
            }
            
            
        }**/
        
    }
      public void totalcost(){
    for(Asset_Type__c js : justlist){
            list<string> tal = new  list<string>();
            if(js.Quantity__c !=null && js.Purchase_Cost__c !=null){
            js.Total_Cost__c = js.Quantity__c*js.Purchase_Cost__c;
            }
            }
    
    }
    public void purchasecost(){
        for(Asset_Type__c js : justlist){
            if(js.Type_Of_Asset1__c !='' && js.Asset_Specification1__c !='' && js.Suppliers_Name1__c !=''){
                list<Dispenser_Line_Item__c> dli = [select id,Purchase_Cost__c from Dispenser_Line_Item__c where Type_Of_Assert__c =: js.Type_Of_Asset1__c and Asset_Specification__c =: js.Asset_Specification1__c and Suppliers_Name__c =:js.Suppliers_Name1__c];
                if(dli.size()>0)
                    js.Purchase_Cost__c = dli[0].Purchase_Cost__c;
            }
        }
    }
     public void Add1()
    {    
        
        
        recCount1+=1;
        Concrete_Dispenser_Product__c j = new Concrete_Dispenser_Product__c();  
        j.S_No__c=recCount1;      
        justfylist.add(j);         
        
    }
    
    public void Del1()
    {
        system.debug('*****************lect'+selectedRowIndex1);
        system.debug('*****************justlist'+justfylist);
        
        if(justfylist.size()>0)
        {
            system.debug('*****************justlist11111'+justfylist);
            for(Integer i=0; i<justfylist.size();i++)
            {
                system.debug('*****************recCount'+recCount1);
                
                if(justfylist[i].S_No__c==selectedRowIndex1)
                {
                    count1=i;
                    system.debug('*****************i'+i);
                    justfylist.remove(i);
                    system.debug('*****************justlist---'+justfylist);
                }
            }
            
        }
        
    }
    
     public void productvalue1(){
        
        for(Concrete_Dispenser_Product__c js : justfylist){
            if(js.MaterialName__c !=null){
                Product2 pr = [select id,name,Projected_Sales_Volume__c,Excepted_Selling_Price__c,Estimated_Annual_Sales__c,Standard_Cost__c,Gross_Margin__c,Gross_Margin_In_Percentage__c from Product2 where id =: js.MaterialName__c];
                js.Projected_Sales_Volume__c = pr.Projected_Sales_Volume__c;
                js.Excepted_Selling_Price__c = pr.Excepted_Selling_Price__c;
              //  js.Estimated_Annual_Sales__c = pr.Estimated_Annual_Sales__c;
                js.Standard_Cost__c = pr.Standard_Cost__c;
                js.Gross_Margin__c = pr.Gross_Margin__c;
                js.Gross_Margin_In_Percentage__c = pr.Gross_Margin_In_Percentage__c;
            }
            
            
        }
        
    }
   
    public PageReference savec(){
    try{
        update di;
        for(Asset_Type__c js : justlist){
            js.Dispenser_Installation__c = di.id;
        }
        upsert justlist;
        for(Concrete_Dispenser_Product__c js : justfylist){
            js.Dispenser_Installation__c = di.id;
        }
        upsert justfylist;
        PageReference pg = new PageReference('/'+di.id);
        pg.setRedirect(true);
        return pg;
        }
        catch(DMLException de) {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.FATAL, de.getDmlMessage(0)));
            return NULL;
        }
        catch(Exception e) {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.FATAL, e.getMessage()));
            return NULL;
        }
    }
    public void address(){
   
     if(di.Customer_Name1__c != null){
     account acc = [select id,Name, BillingStreet, BillingCity, BillingState, BillingPostalCode,
       BillingCountry,SAP_Account_Code__c, BillingLatitude, BillingLongitude from account where id =:di.Customer_Name1__c ];
      di.Customer_code__c = acc.SAP_Account_Code__c;
     di.Street__c= acc.BillingStreet;
     di.City__c = acc.BillingCity;
     di.State_Province__c = acc.BillingState;
     di.Zip_Postal_Code__c = acc.BillingPostalCode;
     di.Country__c = acc.BillingCountry;
     }
    
    }
   
    public PageReference cancel(){
        PageReference pg = new PageReference('/a6D/o');
        return pg;
    }
    
    
}