public with sharing class CommunityWarrantyDetailController {

       
        public Warranty__c warranty {get; set;}
        public List<Warranty_Line__c> WarrantyLIList{get; set;}
        public string currentUser = UserInfo.getUserId();
        public string index{get; set;}
        public Boolean canEdit {get; set;}
        public List<attachment> AttachmentList {get; set;}
        public integer attnum{get; set;}
        public Boolean Issubmitted{get; set;}
        public string SelectTabVar{get; set;}
        public warranty__c wr{get;set;}
        private String infStr = 'Interior Finishing';
        
        public Attachment attachment
    {
        get 
        {
            if (attachment == null)
                attachment = new Attachment();
            return attachment;
        }
        set;
    }  
    
    public Attachment attachment2
    {
        get 
        {
            if (attachment2 == null)
                attachment2 = new Attachment();
            return attachment2;
        }
        set;
    }

    public Boolean isINF 
    {
        get
        {
            return infStr.equalsIgnoreCase(warranty.Target_Market__c);
        }
        private set;
    }
        
        
        public CommunityWarrantyDetailController(){
                if(ApexPages.currentPage().getParameters().get('wid')!=null){
                    wr=[select id,name from Warranty__c where id= : ApexPages.currentPage().getParameters().get('wid')];
                }
                WarrantyLIList = new List<Warranty_Line__c>();
                Map<string, string> pageParams = ApexPages.currentPage().getParameters();
                AttachmentList = new List<attachment>();
                canEdit = true;
                Issubmitted = false;
                
                if(pageParams.size() > 0){
                        string warrantyFields = Utilities.GetAllObjectFields('Warranty__c');
                        string queryFormat = '';
                        if(pageParams.get('wid') != null){
                                string wid = pageParams.get('wid');
                                //queryFormat = 'select {0},(select name,Article_Cod__c,Product__r.name,Quantity__c from Warranty_Lines__r) from Warranty__c where Id = :wid';
                                queryFormat = 'select {0}, Project__r.Name, Project__r.Street_Address__c, Project__r.City__c, Project__r.Country__c, Project__r.Description__c, Project__r.State__c, Project__r.Zip__c from Warranty__c where Id = :wid and Community_User_ID__c = :currentUser';
                                
                        }
                        
                        else if(pageParams.get('pid') != null){
                                string pid = pageParams.get('pid');
                                /*
                                        TODO: Show user when there is more than 1 warranty to select from
                                        so they can choose which one to view/edit OR create a new one
                                        
                                        WCS 1/13/15
                                */
                                queryFormat = 'select {0} from Warranty__c where Project__c = :pid order by CreatedDate desc limit 1';
                        }
                        string warrantyQuery = String.format(queryFormat, new List<string>{warrantyFields});
                        try{
                                warranty = Database.query(warrantyQuery);
                                AttachmentList =[select OwnerId, name, Description, CreatedDate from attachment where ParentId =: warranty .id];
                                WarrantyLIList =[select name, Article_Cod__c, Product__r.name, Quantity__c,Warranty__c , Warranty_Term_Years__c, Sales_Unit_of_Measure__c, Substrate_Area__c, Substrate_Unit_of_Measure__c,Warranty_Term_Yearsss__c from Warranty_Line__c where Warranty__c =: warranty.id];
                                
                        }
                        catch(Exception e){
                                //must be no warranty was found - proceed to create a new one
                        }
                        
                        
                }
                
                if(warranty == null){
                        warranty = new Warranty__c();
                        //associate with project
                        if(pageParams.get('pid') != null){
                                warranty.Project__c = pageParams.get('pid');
                        }
                }
                
                if(warranty.Warranty_Status__c != 'Open' && warranty.Warranty_Status__c != 'Draft'){
                    canEdit = false;
                }
        }
        
        //upload function
         public pagereference upload(){
             
            attachment.OwnerId = currentUser ;
            attachment.ParentId = warranty.id ; // the record the file is attached to
            //attachment2.OwnerId = currentUser ;
            //attachment2.ParentId = warranty.id ;
            
        
            try {
              insert attachment;
              //insert attachment2;
            } catch (DMLException e) {
              ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment: ' + e.getMessage()));
              return new PageReference('/apex/CommunityWarrantyDetail?tab=WarrantyDetail&wid=' + warranty.Id);
              
            } finally {
              attachment = new Attachment(); 
            }
        
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
            AttachmentList =[select OwnerId, name, Description, CreatedDate from attachment where ParentId =: warranty .id];
                                    
            return new PageReference('/apex/CommunityWarrantyDetail?tab=WarrantyDetail&wid=' + warranty.Id);
           //return null;
    }
    
      public Pagereference cloneRecord(){
          List<Warranty_Line__c > wlineadd =new List<Warranty_Line__c >();
         Warranty__c wnew = new Warranty__c();
         wnew= warranty.clone();
        wnew.Warranty_Status__c = 'Draft';
        insert wnew;
        for(Warranty_Line__c wrline : WarrantyLIList){
          Warranty_Line__c wlnew =wrline.clone() ;
          wlnew.Warranty__c =   wnew.id;
          wlineadd.add(wlnew);
        }
        if(wlineadd!=null && wlineadd.size()>0){
            insert wlineadd;
        }
        PageReference pg = new PageReference('/apex/CommunityWarrantyEdit?wid=' + wnew.Id);
      pg.setRedirect(true);
      return pg;
      
      }  
      
        public PageReference SaveWarranty(){
        system.debug('1111111111111111'+warranty.Owner_State__c);
        system.debug('2222222222'+warranty.Applicator_State__c);
        system.debug('33333333'+warranty.Project__r.State__c);
                try{
                        //ensure warranty has the community user tied in
                        if(warranty.Community_User_ID__c == null){
                                warranty.Community_User_ID__c = UserInfo.getUserId();
                        }
                        
                        Database.upsertResult res = Database.upsert(warranty);
                        
                        if(res.IsSuccess()){
                                PageReference pr = new PageReference('/apex/CommunityWarrantyEdit?wid=' + res.getId());
                                pr.setRedirect(true);
                                return pr;
                        }
                        else{
                                return null;
                        }
                }
                catch(Exception e){
                        return null;
                }
        }
        
        public string ValidateEntry(){
                return '';
        }
        
        public list<SelectOption> getValue(){
            list<SelectOption> abc =new list<SelectOption>();
            return abc;
        }
        
        public void deleteWarrantyLineItems(){
            string index = ApexPages.currentPage().getParameters().get('lineItemNo');
            System.debug('index: ' +  integer.valueof(index));
            Warranty_Line__c WarLItoDelete = WarrantyLIList.get(integer.valueof(index));
            
            delete WarLItoDelete;
            WarrantyLIList.remove(integer.valueof(index));
            System.debug('LI : ' + WarrantyLIList);
        }
        
        public PageReference submitForApproval(){
            try{
                isSubmitted = false;
                if(WarrantyLIList.size()<=0){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Please select some product before submitting')); 
                     return null;
                }
                else{
                    Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                    req.setObjectId(warranty.Id);
                    Approval.ProcessResult result = Approval.process(req);
                    if(result.isSuccess() == true){
                        Issubmitted = true;
                        canEdit = false;
                         return new PageReference('/apex/CommunityWarrantyDetail?tab=WarrantyDetail&wid=' + warranty.Id);
                         //return null;
                    }
                   // canEdit = false;
                    
                    
                }
                
            }
            catch(Exception e){
                system.debug('Error submitting warranty approval: ' + e.getMessage());
            }
            
            return null;
        }
        
        public pageReference refrenceToEditPage(){
        System.debug('SelectedTab:'+SelectTabVar);
            PageReference pr = new PageReference('/apex/CommunityWarrantyEdit?wid=' + warranty.Id+'&tab='+SelectTabVar);
                                pr.setRedirect(true);
                                return pr;
        }
        
        
        
        public void deleteAttachements() {
        string index = ApexPages.currentPage().getParameters().get('attNum');
            if (AttachmentList != null && AttachmentList.size() > 0){
                attachment a = AttachmentList.get(integer.valueof(index));
                delete a;
                AttachmentList .remove(integer.valueof(index));
           }
        }

     
        
        
        public void testMe(){
            Account tempAcc = new Account();
            Project__c tempProj = new Project__c();
            
            String tbname = ApexPages.currentPage().getParameters().get('tabname');
            String Recordid = ApexPages.currentPage().getParameters().get('Recordid');
            System.debug('tbname '+tbname);
            System.debug('Recordid '+Recordid);
            if(tbname.equals('Project')){
                if(Recordid != null && Recordid !=''){
                    tempProj = [select id, Name , Street_Address__c, City__c, Country__c, State__c, Zip__c, Description__c
                                  from Project__c where id =: Recordid];
                    warranty.Project__c = tempProj.id;
                    warranty.Project__r = tempProj;
                }
                
            }
            else{
                if(Recordid != null && Recordid !=''){
                    tempAcc = [select owner.Name , owner.Street, owner.City, owner.Country, owner.State, owner.PostalCode, 
                                 owner.Phone, owner.Email, owner.Fax, owner.Contact.name from Account where id =: Recordid];
                
                }
                if(tbname.equals('Owner')){
                    
                    warranty.Owner_Name__c = tempAcc.owner.Name;
                    warranty.Owner_Address__c = tempAcc.owner.Street;
                    warranty.Owner_City__c = tempAcc.owner.City;
                    warranty.Owner_Country__c = tempAcc.owner.Country;
                    warranty.Owner_Contact_Fax__c = tempAcc.owner.Fax;
                    warranty.Owner_Contact_Phone__c = tempAcc.owner.Phone;
                    warranty.Owner_State__c = tempAcc.owner.State;
                    warranty.Owner_Zip_Code__c = tempAcc.owner.PostalCode;
                    warranty.Owner_Contact_Name__c = tempAcc.owner.Contact.name;
                    warranty.Owner_Email__c = tempAcc.owner.Email;
                }
                if(tbname.equals('Applicator')){
                    
                    warranty.Applicator_Name__c = tempAcc.owner.Name;
                    warranty.Applicator_Address__c = tempAcc.owner.Street;
                    warranty.Applicator_City__c = tempAcc.owner.City;
                    warranty.Applicator_Country__c = tempAcc.owner.Country;
                    warranty.Applicator_Contact_Fax__c = tempAcc.owner.Fax;
                    warranty.Applicator_Contact_Phone__c = tempAcc.owner.Phone;
                    warranty.Applicator_State__c = tempAcc.owner.State;
                    warranty.Applicator_Zip_Code__c = tempAcc.owner.PostalCode;
                    warranty.Applicator_Contact_Name__c = tempAcc.owner.Contact.name;
                    warranty.Applicator_Email__c = tempAcc.owner.Email;
                }
                if(tbname.equals('General_Contractor')){
                    
                    warranty.General_Contractor_Name__c = tempAcc.owner.Name;
                    warranty.General_Contractor_Address__c = tempAcc.owner.Street;
                    warranty.General_Contractor_City__c = tempAcc.owner.City;
                    warranty.General_Contractor_Country__c = tempAcc.owner.Country;
                    warranty.General_Contractor_Contact_Fax__c = tempAcc.owner.Fax;
                    warranty.General_Contractor_Contact_Phone__c = tempAcc.owner.Phone;
                    warranty.General_Contractor_State__c = tempAcc.owner.State;
                    warranty.General_Contractor_Zip_Code__c = tempAcc.owner.PostalCode;
                    warranty.General_Contractor_Contact_Name__c = tempAcc.owner.Contact.name;
                    warranty.General_Contractor_Email__c = tempAcc.owner.Email;
                }
                if(tbname.equals('Architect')){
                    
                    warranty.Architect_Name__c = tempAcc.owner.Name;
                    warranty.Architect_Address__c = tempAcc.owner.Street;
                    warranty.Architect_City__c = tempAcc.owner.City;
                    warranty.Architect_Country__c = tempAcc.owner.Country;
                    warranty.Architect_Contact_Fax__c = tempAcc.owner.Fax;
                    warranty.Architect_Contact_Phone__c = tempAcc.owner.Phone;
                    warranty.Architect_State__c = tempAcc.owner.State;
                    warranty.Architect_Zip_Code__c = tempAcc.owner.PostalCode;
                    warranty.Architect_Contact_Name__c = tempAcc.owner.Contact.name;
                    warranty.Architect_Email__c = tempAcc.owner.Email;
                }
                if(tbname.equals('Distributor')){
                    
                    warranty.Distributor_Name__c = tempAcc.owner.Name;
                    warranty.Distributor_Address__c = tempAcc.owner.Street;
                    warranty.Distributor_City__c = tempAcc.owner.City;
                    warranty.Distributor_Country__c = tempAcc.owner.Country;
                    warranty.Distributor_Contact_Fax__c = tempAcc.owner.Fax;
                    warranty.Distributor_Contact_Phone__c = tempAcc.owner.Phone;
                    warranty.Distributor_State__c = tempAcc.owner.State;
                    warranty.Distributor_Zip_Code__c = tempAcc.owner.PostalCode;
                    warranty.Distributor_Contact_Name__c = tempAcc.owner.Contact.name;
                    warranty.Distributor_Email__c = tempAcc.owner.Email;
                    
                }
            }
        }
}