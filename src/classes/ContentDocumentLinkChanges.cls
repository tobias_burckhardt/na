public class ContentDocumentLinkChanges {
    public final List<ContentDocumentLink> oldLinks;
    public final List<ContentDocumentLink> newLinks;

    public ContentDocumentLinkChanges(List<ContentDocumentLink> newLinks,
            List<ContentDocumentLink> oldLinks) {
        this.oldLinks = oldLinks;
        this.newLinks = newLinks;
    }

    public void commitChangesToDatabase() {
        if (!oldLinks.isEmpty() || !newLinks.isEmpty()) {
            System.Savepoint sp = Database.setSavepoint();

            try {
                delete oldLinks;
                insert newLinks;
            } catch(DmlException e) {
                Database.rollback(sp);
                throw e;
            }
        }
    }
}