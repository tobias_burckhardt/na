public with sharing class CommunityWarrantyEditController {
    public Boolean submittedForApproval {get; set;}
    public List<WrapperWrap1> wp1 { get; set;}
    public List<SelectOption > contusers {get; set;}
    public Boolean bool {get; set;}
    public List<SelectOption> produnit;
    public String selectedtabname {get; set;}
    public Warranty__c wr {get; set;}
    public Boolean displayPopUp {get; set;}
    public Attachment attachment {
        get
        {
            if (attachment == null)
                attachment = new Attachment();
            return attachment;
        }
        set;
    }

    public Attachment attachment2 {
        get
        {
            if (attachment2 == null)
                attachment2 = new Attachment();
            return attachment2;
        }
        set;
    }
    public Boolean boolwlist {get; set;}
    public Boolean valid;
    public class WrapperWrap1 {
        public Decimal qty {get; set;}
        public String selprod {get; set;}
        public Warranty_Line__c pl {get; set;}
        public boolean isOld;
        public String HighlightNotmatched {get; set;}
        public WrapperWrap1 () {
            this.pl = new Warranty_Line__c ();
            this.isOld = false;
            this.HighlightNotmatched = 'white';
        }
    }
    public User adminUser ;
    public Warranty__c warranty {get; set;}
    public List<Warranty_Line__c> WarrantyLIList {get; set;}
    public String currentUser = UserInfo.getUserId();
    public String index {get; set;}
    public Map<String, Id> warrantyprod = new Map<String, Id>();
    public Map<String, Decimal> warrantyprodval = new Map<String, Decimal>();
    public Map<Id, String> pbeToWarrantyTerm = new Map<Id, String>();
    public Map<Id, String> pbeToIfTerm = new Map<Id, String>();
    private List<PricebookEntry> pbes;
    private String warrantyRecord;
    public Boolean isCommUser {get; set;}
    private String warrantyName;
    private String warrantyType;
    private String warrantyRecordTypeId;
    private String infStr = 'Interior Finishing';

    public Boolean isINF {
        get {
            return infStr.equalsIgnoreCase(warranty.Target_Market__c);
        }
        private set;
    }


    public CommunityWarrantyEditController() {

        displayPopUp = false;
        if (ApexPages.currentPage().getParameters().get('wid') != null) {
            wr = [select Id, Name, RecordType.Name from Warranty__c where id = : ApexPages.currentPage().getParameters().get('wid')];
        }
        adminUser = [select id, name from user where profile.name = 'System Administrator' limit 1];

        wp1 = new List<WrapperWrap1>();
        bool = true;
        valid = false;
        boolwlist = false;
        submittedForApproval = false;
        contusers = new List < SelectOption > ();
        contusers.add(new SelectOption('', '--- None ---'));
        warrantyType = [SELECT Id, Warranty_Type__c FROM User WHERE Id = :UserInfo.getUserId()][0].Warranty_Type__c;
        Pricebook__c pickListValue = Pricebook__c.getInstance(warrantyType);
        warrantyName = pickListValue.Pricebook_Name__c;
        if (warrantyName == 'Warranty Family Picklist') {
            isCommUser = true;
        } else {
            isCommUser = false;
        }
        warrantyRecord = pickListValue.Warranty_Record__c;
        pbes = [select Id, Product2.Id, Product2.Unit__c, Product2.Warranty_Terms__c, Product2.Name, Product2.Max_Warranty_Term__c,
                Product2.ProductCode, Name, Product2.Description, Product2.Is_10_Year__c,  Product2.Is_Wood_Floor__c
                from PricebookEntry
                where IsActive = true
                                 and Pricebook2.Name = :warrantyName
                                         order by Product2.Name ASC];
        for (PricebookEntry  pbe : pbes) {
            contusers.add(new SelectOption(pbe.Product2.id, pbe.Product2.Name));
            warrantyprod.put(pbe.Product2.id, pbe.Product2.id);
            warrantyprodval.put(pbe.Product2.id, pbe.Product2.Max_Warranty_Term__c);
            pbeToWarrantyTerm.put(pbe.Product2.id, pbe.Product2.Warranty_Terms__c);

            //if(pbe.Product2.Is_10_Year__c) {
            //    pbeToIfTerm.put(pbe.Product2.id, '10');
            //}
            //else if(pbe.Product2.Is_Wood_Floor__c) {
            //    pbeToIfTerm.put(pbe.Product2.id, null);
            //}
        }
        //determine if url parameters were passed in
        WarrantyLIList = new List<Warranty_Line__c>();
        Map<string, string> pageParams = ApexPages.currentPage().getParameters();
        //place record type here
        String warrantyDevName = pickListValue.Warranty_Record__c;
        warrantyRecordTypeId = [select Id from RecordType where SObjectType = 'Warranty__c' and DeveloperName = : warrantyDevName][0].Id;
        if (pageParams.size() > 0) {
            string warrantyFields = Utilities.GetAllObjectFields('Warranty__c');
            string queryFormat = '';
            if (pageParams.get('wid') != null) {
                string wid = pageParams.get('wid');
                queryFormat = 'select {0}, Project__r.Name, Project__r.Street_Address__c, Project__r.City__c, Project__r.Country__c, Project__r.Description__c, Project__r.State__c, Project__r.Zip__c from Warranty__c where Id = :wid and Community_User_ID__c = :currentUser';

            }

            else if (pageParams.get('pid') != null) {
                string pid = pageParams.get('pid');
                /*
                        TODO: Show user when there is more than 1 warranty to select from
                        so they can choose which one to view/edit OR create a new one

                        WCS 1/13/15
                */
                queryFormat = 'select {0} from Warranty__c where Project__c = :pid order by CreatedDate desc limit 1';
            }
            string warrantyQuery = String.format(queryFormat, new List<string> {warrantyFields});
            wp1.clear();
            try {
                warranty = Database.query(warrantyQuery);
                for (Warranty_Line__c  Warlist : [select Name, Product__c, Sales_Unit_Of_Measure__c, Substrate_Area__c, Warranty_Term_Yearsss__c, Substrate_Unit_of_Measure__c, Article_Cod__c, Product__r.Name, Quantity__c from Warranty_Line__c where Warranty__c = : warranty.id]) {
                    if (WarrantyLIList != null) {
                        boolwlist = true;
                        WrapperWrap1  objInnerClass = new WrapperWrap1();
                        objInnerClass.pl = Warlist;
                        objInnerClass.selProd = WarList.product__c;
                        objInnerClass.isOld = true;
                        objInnerClass.qty = WarList.Quantity__c ;
                        wp1.add(objInnerClass);

                    }
                }
                for (integer i = wp1.size(); i < 10; i++) {
                    WrapperWrap1  objInnerClass = new WrapperWrap1();

                    wp1.add(objInnerClass);
                }
                if (boolwlist == false) {
                    wp1 = new List<WrapperWrap1>();
                    for (Integer i = 0; i < 10; i++) {
                        WrapperWrap1  objInnerClass = new WrapperWrap1();
                        wp1.add(objInnerClass);
                    }
                }
            } catch (Exception e) {
                //must be no warranty was found - proceed to create a new one
            }
        }
        if (warranty == null) {
            warranty = new Warranty__c();
            warranty.RecordTypeId = warrantyRecordTypeId;
            //associate with project
            if (pageParams.get('pid') != null) {
                warranty.Project__c = pageParams.get('pid');
            }
        }
    }

    public PageReference  createwarrantyrecord() {
        PageReference pr;
        Map<string, string> pageParams = ApexPages.currentPage().getParameters();
        string warrantyId = ApexPages.currentPage().getParameters().get('wid');
        string projectId = ApexPages.currentPage().getParameters().get('pid');
        if (warrantyId == null ) {
            warranty = new Warranty__c();
            if (pageParams.get('pid') != null) {
                warranty.Project__c = pageParams.get('pid');
            }
            if (warranty.Community_User_ID__c == null) {
                warranty.Community_User_ID__c = UserInfo.getUserId();
            }
            warranty.Warranty_Purpose__c = 'Warranty';
            warranty.Warranty_Type__c = 'Standard Material';
            warranty.RecordTypeId = warrantyRecordTypeId;
            warranty.Warranty_Start_Date__c = date.today();

            insert warranty;

            pr = new PageReference('/apex/CommunityWarrantyEdit?wid=' + warranty.Id);

            pr.setRedirect(true);
            return pr;

        }
        return null;
    }

    //upload function
    public pagereference upload() {
        attachment.OwnerId = currentUser ;
        attachment.ParentId = warranty.id ; // the record the file is attached to
        attachment2.OwnerId = currentUser ;
        attachment2.ParentId = warranty.id ;

        try {
            insert attachment;
            insert attachment2;
        } catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error uploading attachment'));
            return null;
        } finally {
            attachment = new Attachment();
        }

        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Attachment uploaded successfully'));
        return null;
    }



    public PageReference SaveWarranty() {
        PageReference pr;

        string warrantyId = ApexPages.currentPage().getParameters().get('wid');
        List<Warranty_Line__c> lstdedct = new List<Warranty_Line__c>();
        String prodWarrantyTerms;
        try {
            valid = false;
            if (wp1.size() > 0) {
                prodWarrantyTerms = pbeToWarrantyTerm.get(wp1[0].selprod);
                for (Integer b = 0; b < wp1.size(); b++) {
                    if (!isCommUser) {
                        wp1[b].pl.Warranty_Term_Yearsss__c = pbeToWarrantyTerm.get(wp1[b].selprod) == null ?
                            null : Decimal.valueOf(pbeToWarrantyTerm.get(wp1[b].selprod));
                    }
                    if ((wp1[b].selprod != null && wp1[b].selprod != '') || (wp1[b].qty != null && wp1[b].qty != 0)  || (wp1[b].pl.Sales_Unit_Of_Measure__c != null && wp1[b].pl.Sales_Unit_Of_Measure__c != '') || (wp1[b].pl.Substrate_Unit_of_Measure__c != null && wp1[b].pl.Substrate_Unit_of_Measure__c != '') || wp1[b].pl.Substrate_Area__c != null || wp1[b].pl.Warranty_Term_Yearsss__c != null ) {
                        if (!wp1[b].isOld)
                            wp1[b].pl.Warranty__c = warrantyId;
                        if (wp1[b].selprod != null) {
                            wp1[b].pl.Product__c = wp1[b].selprod;
                        }
                        if (wp1[b].qty != null) {
                            wp1[b].pl.Quantity__c = wp1[b].qty;
                        }
                        if (wp1[b].pl.Warranty_Term_Yearsss__c > warrantyprodval.get(wp1[b].selprod)) {
                            valid = true;
                            wp1[b].HighlightNotmatched = 'rgb(204, 39, 0)';
                        }
                        //if(warrantyName != 'Warranty Family Picklist' && warrantyName != 'INF' && pbeToWarrantyTerm.get(wp1[b].selprod) != prodWarrantyTerms) {
                        //    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.WarrantyTermError));
                        //    return null;
                        //}
                        lstdedct.add(wp1[b].pl);
                    }
                }
            }

            warranty.Warranty_Terms__c = prodWarrantyTerms == null ? null : Decimal.valueOf(prodWarrantyTerms);

            if (valid == false) {
                try {
                    upsert lstdedct;
                } catch(DmlException e) {
                    System.debug(e.getMessage());
                    ApexPages.addmessages(e);
                }
                //upsert lstdedct;
                //ensure warranty has the community user tied in
                if (warranty.Community_User_ID__c == null) {
                    warranty.Community_User_ID__c = UserInfo.getUserId();
                }
                if (selectedtabname == 'Project' ) {
                    List<project__c> p1 = [select id, name from project__c where name = : warranty.Project_Name__c ];
                    if (p1.size() <= 0) {
                        project__c p2 = new project__c();
                        p2.name = warranty.Project_name__c ;
                        p2.Street_Address__c = warranty.Project_Address__c ;
                        p2.Country__c = warranty.Project_Country__c  ;
                        p2.State__c = warranty.Project_State__c;
                        p2.Zip__c = warranty.Project_Zip_Code__c;
                        p2.Community_User_ID__c = UserInfo.getUserId();
                        insert p2;
                    }
                }
                List<account> a1;
                if (selectedtabname == 'Owner' ) {
                    a1 = [select id, name from account where name = : warranty.Owner_Name__c ];
                    if (a1.size() <= 0) {
                        account a2 = new account();
                        a2.name = warranty.Owner_name__c ;
                        a2.BillingStreet  = warranty.Owner_Address__c ;
                        a2.BillingCity = warranty.Owner_City__c  ;
                        a2.BillingCountry = warranty.Owner_Country__c;
                        a2.BillingState = warranty.Owner_State__c;
                        a2.BillingPostalCode = warranty.Owner_Zip_Code__c;
                        a2.Community_User_ID__c = UserInfo.getUserId();
                        a2.ownerid = adminUser.id;
                        insert a2;
                    }
                }
                if (selectedtabname == 'Applicator' ) {
                    a1 = [select id, name from account where name = : warranty.Applicator_Name__c ];
                    if (a1.size() <= 0) {
                        account a2 = new account();
                        a2.name = warranty.Applicator_name__c ;
                        a2.BillingStreet  = warranty.Applicator_Address__c ;
                        a2.BillingCity = warranty.Applicator_City__c  ;
                        a2.BillingCountry = warranty.Applicator_Country__c;
                        a2.BillingState = warranty.Applicator_State__c;
                        a2.BillingPostalCode = warranty.Applicator_Zip_Code__c;
                        a2.Community_User_ID__c = UserInfo.getUserId();
                        a2.ownerid = adminUser.id;
                        insert a2;
                    }
                }
                if (selectedtabname == 'GeneralContractor' ) {
                    a1 = [select id, name from account where name = : warranty.General_Contractor_Name__c ];
                    if (a1.size() <= 0) {
                        account a2 = new account();
                        a2.name = warranty.General_Contractor_name__c ;
                        a2.BillingStreet  = warranty.General_Contractor_Address__c ;
                        a2.BillingCity = warranty.General_Contractor_City__c  ;
                        a2.BillingCountry = warranty.General_Contractor_Country__c;
                        a2.BillingState = warranty.General_Contractor_State__c;
                        a2.BillingPostalCode = warranty.General_Contractor_Zip_Code__c;
                        a2.Community_User_ID__c = UserInfo.getUserId();
                        a2.ownerid = adminUser.id;

                        insert a2;
                    }
                }
                if (selectedtabname == 'Architect' ) {
                    a1 = [select id, name from account where name = : warranty.Architect_Name__c ];
                    if (a1.size() <= 0) {
                        account a2 = new account();
                        a2.name = warranty.Architect_name__c ;
                        a2.BillingStreet  = warranty.Architect_Address__c ;
                        a2.BillingCity = warranty.Architect_City__c  ;
                        a2.BillingCountry = warranty.Architect_Country__c;
                        a2.BillingState = warranty.Architect_State__c;
                        a2.BillingPostalCode = warranty.Architect_Zip_Code__c;
                        a2.Community_User_ID__c = UserInfo.getUserId();
                        a2.ownerid = adminUser.id;
                        insert a2;
                    }
                }
                if (selectedtabname == 'Distributor' ) {
                    a1 = [select id, name from account where name = : warranty.Distributor_Name__c ];
                    if (a1.size() <= 0) {
                        account a2 = new account();
                        a2.name = warranty.Distributor_name__c ;
                        a2.BillingStreet  = warranty.Distributor_Address__c ;
                        a2.BillingCity = warranty.Distributor_City__c  ;
                        a2.BillingCountry = warranty.Distributor_Country__c;
                        a2.BillingState = warranty.Distributor_State__c;
                        a2.BillingPostalCode = warranty.Distributor_Zip_Code__c;
                        a2.Community_User_ID__c = UserInfo.getUserId();
                        a2.ownerid = adminUser.id;
                        insert a2;
                    }
                }

                Database.upsertResult res = Database.upsert(warranty);

                if (res.IsSuccess()) {
                    pr = new PageReference('/apex/CommunityWarrantyDetail?wid=' + warrantyId + '&tab=' + selectedtabname);
                    pr.setRedirect(true);
                    return pr;
                } else {
                    return null;
                }
            } else {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Warranty Term Years cannot exceed Product Max Warranty Term  ');
                ApexPages.addMessage(myMsg);
                return null;
            }
        } catch (Exception e) {
            ApexPages.addMessages(e);
            return null;
        }
    }
    public string ValidateEntry() {
        return '';
    }

    public list<SelectOption> getValue() {
        list<SelectOption> abc = new list<SelectOption>();
        return abc;
    }

    public void deleteWarrantyLineItems() {
        string index = ApexPages.currentPage().getParameters().get('lineItemNo');
        Warranty_Line__c WarLItoDelete = WarrantyLIList.get(integer.valueof(index));

        delete WarLItoDelete;
        WarrantyLIList.remove(integer.valueof(index));
    }

    public void disblpart() {
        displayPopUp = true;
    }

    public void testMe() {
        Account tempAcc = new Account();
        Project__c tempProj = new Project__c();

        String tbname = ApexPages.currentPage().getParameters().get('tabname');
        String Recordid = ApexPages.currentPage().getParameters().get('Recordid');

        if (tbname.equals('Project')) {
            if (Recordid != null && Recordid != '') {
                tempProj = [select id, Name , Street_Address__c, City__c, Country__c, State__c, Zip__c, Description__c
                            from Project__c where id = : Recordid];
                warranty.Project__c = tempProj.id;
                warranty.Project__r = tempProj;
                warranty.Project_Name__c = tempProj.Name;
                warranty.Project_Address__c = tempProj .Street_Address__c;
                warranty.Project_City__c = tempProj .City__c;
                warranty.Project_Country__c = tempProj .Country__c;
                //warranty.Owner_Contact_Fax__c = tempAcc.oFax;
                //warranty.Owner_Contact_Phone__c = tempAcc.owner.Phone;
                warranty.Project_State__c = tempProj .State__c;
                warranty.Project_Zip_Code__c = tempProj .Zip__c;
                //warranty.Owner_Contact_Name__c = tempAcc.owner.Contact.name;
                //warranty.Owner_Email__c = tempAcc.owner.Email;
            }

        } else {
            if (Recordid != null && Recordid != '') {
                tempAcc = [select Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Type, Website, Phone from Account where id = : Recordid];

            }

            if (tbname.equals('Owner')) {

                warranty.Owner_Name__c = tempAcc.Name;
                warranty.Owner_Address__c = tempAcc.BillingStreet;
                warranty.Owner_City__c = tempAcc.BillingCity;
                warranty.Owner_Country__c = tempAcc.BillingCountry;
                //warranty.Owner_Contact_Fax__c = tempAcc.oFax;
                //warranty.Owner_Contact_Phone__c = tempAcc.owner.Phone;
                warranty.Owner_State__c = tempAcc.BillingState;
                warranty.Owner_Zip_Code__c = tempAcc.BillingPostalCode;
                //warranty.Owner_Contact_Name__c = tempAcc.owner.Contact.name;
                //warranty.Owner_Email__c = tempAcc.owner.Email;
            }
            if (tbname.equals('Applicator')) {

                warranty.Applicator_Name__c = tempAcc.Name;
                warranty.Applicator_Address__c = tempAcc.BillingStreet;
                warranty.Applicator_City__c = tempAcc.BillingCity;
                warranty.Applicator_Country__c = tempAcc.BillingCountry;
                //warranty.Applicator_Contact_Fax__c = tempAcc.oFax;
                //warranty.Applicator_Contact_Phone__c = tempAcc.owner.Phone;
                warranty.Applicator_State__c = tempAcc.BillingState;
                warranty.Applicator_Zip_Code__c = tempAcc.BillingPostalCode;
                //warranty.Applicator_Contact_Name__c = tempAcc.owner.Contact.name;
                //warranty.Applicator_Email__c = tempAcc.owner.Email;
            }
            if (tbname.equals('General_Contractor')) {

                warranty.General_Contractor_Name__c = tempAcc.Name;
                warranty.General_Contractor_Address__c = tempAcc.BillingStreet;
                warranty.General_Contractor_City__c = tempAcc.BillingCity;
                warranty.General_Contractor_Country__c = tempAcc.BillingCountry;
                //warranty.General_Contractor_Contact_Fax__c = tempAcc.oFax;
                //warranty.General_Contractor_Contact_Phone__c = tempAcc.owner.Phone;
                warranty.General_Contractor_State__c = tempAcc.BillingState;
                warranty.General_Contractor_Zip_Code__c = tempAcc.BillingPostalCode;
                //warranty.General_Contractor_Contact_Name__c = tempAcc.owner.Contact.name;
                //warranty.General_Contractor_Email__c = tempAcc.owner.Email;
            }
            if (tbname.equals('Architect')) {

                warranty.Architect_Name__c = tempAcc.Name;
                warranty.Architect_Address__c = tempAcc.BillingStreet;
                warranty.Architect_City__c = tempAcc.BillingCity;
                warranty.Architect_Country__c = tempAcc.BillingCountry;
                //warranty.Architect_Contact_Fax__c = tempAcc.oFax;
                //warranty.Architect_Contact_Phone__c = tempAcc.owner.Phone;
                warranty.Architect_State__c = tempAcc.BillingState;
                warranty.Architect_Zip_Code__c = tempAcc.BillingPostalCode;
                //warranty.Architect_Contact_Name__c = tempAcc.owner.Contact.name;
                //warranty.Architect_Email__c = tempAcc.owner.Email;
            }
            if (tbname.equals('Distributor')) {

                warranty.Distributor_Name__c = tempAcc.Name;
                warranty.Distributor_Address__c = tempAcc.BillingStreet;
                warranty.Distributor_City__c = tempAcc.BillingCity;
                warranty.Distributor_Country__c = tempAcc.BillingCountry;
                //warranty.Distributor_Contact_Fax__c = tempAcc.oFax;
                //warranty.Distributor_Contact_Phone__c = tempAcc.owner.Phone;
                warranty.Distributor_State__c = tempAcc.BillingState;
                warranty.Distributor_Zip_Code__c = tempAcc.BillingPostalCode;
                //warranty.Distributor_Contact_Name__c = tempAcc.owner.Contact.name;
                //warranty.Distributor_Email__c = tempAcc.owner.Email;

            }
        }
    }
}