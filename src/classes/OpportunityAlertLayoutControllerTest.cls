@isTest(SeeAllData=true)
public class OpportunityAlertLayoutControllerTest {
    static testMethod void testNonStaticMethods() {
        Test.StartTest();     
            Account account = OpportunityAlertLayoutControllerTest.createAccount();
            String jsonString = '{"accountName":"'+account.Id+'"}';
            Opportunity opportunity = OpportunityAlertLayoutControllerTest.createOpportunity(jsonString);
            OpportunityAlertLayoutController addProductToObj  = new OpportunityAlertLayoutController(new ApexPages.StandardController(opportunity));
        	addProductToObj.displayAlert = false;
            
        Test.StopTest();
    }
    public static Account createAccount(){
        Account account = new Account();
        account.Name = 'Dummy Test Account';
        account.SAPAccountGroup__c = '0001';
        account.Country_Iso_Code__c = 'CA';
        insert account;
        Account accountQueried = [Select Id,Name from Account where Name like '%Dummy Test Account%' and CreatedDate=Today Limit 1];
        return accountQueried;
    }
    public static Opportunity createOpportunity(String jsonString){
        JSONParser  parser = JSON.createParser(jsonString);
        String accountID;
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'accountName')){
                parser.nextToken();
                accountID=parser.getText();
            }
        }
        Account accountQueried = [Select Id,Name from Account where Id =:accountID  Limit 1];
        Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('CA Free Sample Requisition').getRecordTypeId();
        Opportunity opportunity = new Opportunity();
        opportunity.Name = 'Test Opportunity for Free Sample Requisition';
        opportunity.RecordTypeId = recordTypeId;
        opportunity.CloseDate = Date.newInstance(2020, 2, 17);
        opportunity.StageName = 'Draft';
        opportunity.Amount = 1000;
        //opportunity.Purpose_of_Free_Sample__c = 'Goodwill';
        opportunity.Account= accountQueried;
        opportunity.is_there_a_CCR__c = 'No';
        opportunity.Pickup__c = 'Customer Pickup';
        insert opportunity;
        Opportunity opportunityQueried = [Select Id from Opportunity where Name like '%Test Opportunity for Free Sample Requisition%' limit 1];
        return opportunityQueried;
    }
}