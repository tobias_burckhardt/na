public with sharing class CommunityProjectDetailController {
        
        public Project__c proj {get; set;}
        public Boolean addWarranty {get; set;}
        public List<Warranty__c> warrantyList {get; set;}
        public Boolean showWarranties {get; set;}
        
        public CommunityProjectDetailController(){
            //determine if url parameters were passed in
            Map<string, string> pageParams = ApexPages.currentPage().getParameters();
            
            showWarranties = false;
            try{
                if(pageParams.size() > 0){
                    if(pageParams.get('pid') != null){
                            string pid = pageParams.get('pid');
                            string projectFields = Utilities.GetAllObjectFields('Project__c');
                            string queryFormat = 'select {0} from {1} where {2}';
                            string projQuery = String.format(queryFormat, new List<string>{projectFields, 'Project__c', 'Id = :pid'});
                            proj = Database.query(projQuery);
                            
                            //check to see if any warranties exist
                            string warrantyFields = Utilities.GetAllObjectFields('Warranty__c');
                            string warrQuery = String.format(queryFormat, new List<string>{warrantyFields, 'Warranty__c', 'Project__c = :pid'});
                                            
                                            warrantyList = Database.query(warrQuery);
                                            
                                            if(warrantyList.size() > 0){
                                                    showWarranties = true;
                                            }
                    }
                }
                
                if(proj == null){
                        proj = new Project__c();
                }
                
                if(warrantyList == null){
                    warrantyList = new List<Warranty__c>();
                }
                
                addWarranty = false;
            }catch(Exception e){}
            
        }
        
        //to delete warranties
        public void deleteWarranties(){
            string index = ApexPages.currentPage().getParameters().get('WarrNum');
            if (index != null){
                Warranty__c WartoDelete = warrantyList.get(integer.valueof(index));
                delete WartoDelete;
                warrantyList.remove(integer.valueof(index));
            }
        }
}