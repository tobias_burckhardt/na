/**
 * @author Pavel Halas
 * @company Bluewolf, an IBM Company
 * @date 6/2016
 *
 * Controller for BuildingGroupWarranties.page that provides building groups for a given opportunity
 * and allows users to select a couple of them and issue a (single) warranty.
 */
public class BuildingGroupWarrantiesContExt {

    private final ApexPages.StandardController stdCont;
    private final List<BuildingGroupService.BuildingGroupWrapper> buildingGroupWrappers;
    private final Boolean ISSUE_WARRANTY = false;

    // Warranty object that can be null in case all the building groups have a warranty issued (already)
    public Warranty__c warranty {get; set;}

    public BuildingGroupWarrantiesContExt(ApexPages.StandardController stdCont) {
        this.stdCont = stdCont;
        Opportunity opportunity = [SELECT Id, AccountId, Account.Name, Roofing_Primary_Contact__c, Roofing_Primary_Contact__r.Phone,
                Account.SAP_Phone__c, Anticipated_Start_Date__c FROM Opportunity WHERE Id =: stdCont.getId()];

        List<Building_Group__c> buildingGroups = BuildingGroupService.getBuildingGroupsForOpportunity(opportunity.Id);

        buildingGroupWrappers = BuildingGroupService.constructBuildingGroupWrappers(buildingGroups, ISSUE_WARRANTY);

        // If there is at least one building group without a warranty
        for (BuildingGroupService.BuildingGroupWrapper buildingGroupWrapper : buildingGroupWrappers) {
            if (buildingGroupWrapper.buildingGroup.warranty__c == null) {
                Boolean zeroValueWarranty = buildingGroupWrapper.buildingGroup.Intended_Warranty_Product__r.Roofing_Zero_Value_Warranty__c;
                warranty = WarrantyService.initWarranty(opportunity, zeroValueWarranty);
                break;
            }
        }
    }

    public List<BuildingGroupService.BuildingGroupWrapper> getBuildingGroupWrappers() {
        return buildingGroupWrappers;
    }

    public PageReference save() {
        List<Building_Group__c> toBeUpdated = new List<Building_Group__c>();
        Map<Warranty__c, List<BuildingGroupService.BuildingGroupWrapper>> buildingGroupWrappersByWarranties
                = WarrantyService.issueWarranty(buildingGroupWrappers, warranty);

        if(buildingGroupWrappersByWarranties.size() > 0) {
            warranty = new List<Warranty__c>(buildingGroupWrappersByWarranties.keySet()).get(0);
            System.Savepoint savepoint = Database.setSavepoint();
            try {
                insert warranty;
            } catch(DmlException e) {
                rollbackRecords(savepoint, warranty, toBeUpdated);
                ApexPages.addMessages(e);
                return null;
            }

            for (BuildingGroupService.BuildingGroupWrapper bgw : buildingGroupWrappers) {
                if (bgw.issueWarranty && bgw.buildingGroup.Warranty__c == null) {
                    bgw.buildingGroup.Warranty__c = warranty.Id;
                    toBeUpdated.add(bgw.buildingGroup);
                }
            }

            try {
                BuildingGroupService.updateBuildingGroups(toBeUpdated);
                return new PageReference('/' + warranty.Opportunity__c);
            } catch(DmlException e) {
                rollbackRecords(savepoint, warranty, toBeUpdated);
                ApexPages.addMessages(e);
                return null;
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,
                'You cannot consolidate building groups with mismatched warranty products, ' +
                'warranty extension products and/or windspeed.'));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,
                'Issue Warranty?: ' + (buildingGroupWrappersByWarranties.size() > 0)));
        }
        return null;
    }

    /** Rolls back database and nulls populated Ids and lookups for in-memory records. */
    @TestVisible
    private static void rollbackRecords(System.Savepoint sp, Warranty__c warranty,
            List<Building_Group__c> groups) {
        warranty.RecordTypeId = null;
        warranty.Intended_Warranty_Product__c = null;
        warranty.Warranty_Extended_Product__c = null;
        warranty.Id = null;

        for (Building_Group__c buildingGroup : groups) {
            buildingGroup.Warranty__c = null;
        }

        Database.rollback(sp);
    }

}