@isTest
private class sika_SCCZoneValidationHelperTest {
	
	@isTest static void testEmailValidation(){

		system.assertEquals(sika_SCCZoneValidationHelper.validateEmail(''), false);
		system.assertEquals(sika_SCCZoneValidationHelper.validateEmail('s'), false);
		system.assertEquals(sika_SCCZoneValidationHelper.validateEmail('s@'), false);
		system.assertEquals(sika_SCCZoneValidationHelper.validateEmail('s@steinf'), false);
		system.assertEquals(sika_SCCZoneValidationHelper.validateEmail('@mail.com'), false);
		system.assertEquals(sika_SCCZoneValidationHelper.validateEmail('mail@.com'), false);
		system.assertEquals(sika_SCCZoneValidationHelper.validateEmail('@.'), false);

		system.assertEquals(sika_SCCZoneValidationHelper.validateEmail('estesmike@us.sika.com'), true);

	}

	@isTest static void testvalidatePriceField(){

		system.assertEquals(sika_SCCZoneValidationHelper.validatePriceField(''), false);
		system.assertEquals(sika_SCCZoneValidationHelper.validatePriceField('abc'), false);

		system.assertEquals(sika_SCCZoneValidationHelper.validatePriceField('3'), true);
		system.assertEquals(sika_SCCZoneValidationHelper.validatePriceField('3.00'), true);

	}

}