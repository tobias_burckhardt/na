public class ApttusAdhocWebserviceCalling {
    public static void configurationFullProcess(Id quoteId){
        ApttusAdhocWebserviceCalling test = new ApttusAdhocWebserviceCalling();
        Id cartID = test.createCart(quoteId);
        test.addMultiProducts(cartID);
        test.finalizeCart(cartID);
    }

    public Id createCart(Id quoteId)
    {
        //Search a quote by its name and fetch its ID
         List<Apttus_Proposal__Proposal__c> listQuote = [Select Id,Apttus_Proposal__ExpectedStartDate__c,Apttus_Proposal__Proposal_Expiration_Date__c from Apttus_Proposal__Proposal__c
        where Id=:quoteId LIMIT 1];

        Apttus_CPQApi.CPQ.CreateCartRequestDO createCartRequest = new Apttus_CPQApi.CPQ.CreateCartRequestDO();
        createCartRequest.QuoteId = listQuote[0].Id;
        Apttus_CPQApi.CPQ.CreateCartResponseDO createCartResponse = Apttus_CPQApi.CPQWebService.createCart(createCartRequest);
        ID cartID = createCartResponse.cartID;
        System.debug(cartID);
        return cartID;
    }

    public void addMultiProducts(Id cartId){
        Apttus_Config2__ProductConfiguration__c cart = [SELECT Apttus_QPConfig__Proposald__c FROM Apttus_Config2__ProductConfiguration__c WHERE Id=:cartId LIMIT 1];
        List<Apttus_Proposal__Proposal_Line_Item__c> plis = [SELECT Apttus_QPConfig__Quantity2__c, 
                                                                    Apttus_Proposal__Product__c, 
                                                                    Apttus_QPConfig__SellingTerm__c, 
                                                                    Apttus_QPConfig__StartDate__c, 
                                                                    Apttus_QPConfig__EndDate__c,
                                                                    Apttus_Proposal__Description__c, 
                                                                    Apttus_QPConfig__AdjustmentAmount__c,
                                                                    Apttus_QPConfig__ListPrice__c,
                                                                    Apttus_QPConfig__NetAdjustmentPercent__c,
                                                                    Apttus_QPConfig__NetPrice__c,
                                                                    Apttus_QPConfig__NetUnitPrice__c,
                                                                    Apttus_QPConfig__PriceUom__c,
                                                                    Apttus_QPConfig__Uom__c,
                                                                    Apttus_QPConfig__ClassificationId__c,
                                                                    PTS_Legacy_ID__c,
                                                                    Apttus_QPConfig__ClassificationHierarchy__c,
                                                                    Apttus_QPConfig__PriceListItemId__c
                                                                    FROM Apttus_Proposal__Proposal_Line_Item__c 
                                                                    WHERE Apttus_Proposal__Proposal__c = :cart.Apttus_QPConfig__Proposald__c];

        if(!plis.isEmpty()){
            List <Apttus_CPQApi.CPQ.SelectedProductDO> selectedProdDOList = new List <Apttus_CPQApi.CPQ.SelectedProductDO>();
            for(Apttus_Proposal__Proposal_Line_Item__c pli : plis){

                Apttus_CPQApi.CPQ.SelectedProductDO selProdDO = new Apttus_CPQApi.CPQ.SelectedProductDO();
                selProdDO.ProductID = pli.Apttus_Proposal__Product__c;
                selProdDO.Quantity = pli.Apttus_QPConfig__Quantity2__c;
                selProdDO.SellingTerm = pli.Apttus_QPConfig__SellingTerm__c;
                selProdDO.StartDate = Date.Today();
                selProdDO.EndDate =  pli.Apttus_QPConfig__EndDate__c;
                selProdDO.Comments = pli.Apttus_Proposal__Description__c;
                selectedProdDOList.add(selProdDO);
            }

            Apttus_CPQApi.CPQ.AddMultiProductRequestDO request = new Apttus_CPQApi.CPQ.AddMultiProductRequestDO();
            request.CartId = cart.Id;
            request.SelectedProducts = selectedProdDOList;
            Apttus_CPQApi.CPQ.AddMultiProductResponseDO response = Apttus_CPQApi.CPQWebService.addMultiProducts(request);

            List<Apttus_Config2__LineItem__c> insertedLineItems = [SELECT Id FROM Apttus_Config2__LineItem__c WHERE Apttus_Config2__ConfigurationId__c=:cart.Id ORDER BY Apttus_Config2__LineSequence__c];
            for(Integer i=0; i<plis.size(); i++){
                insertedLineItems[i].Apttus_Config2__AdjustmentAmount__c = plis[i].Apttus_QPConfig__AdjustmentAmount__c;
                insertedLineItems[i].Apttus_Config2__ListPrice__c = plis[i].Apttus_QPConfig__ListPrice__c;
                insertedLineItems[i].Apttus_Config2__BaseExtendedPrice__c = plis[i].Apttus_QPConfig__ListPrice__c;
                insertedLineItems[i].Apttus_Config2__NetPrice__c = plis[i].Apttus_QPConfig__NetPrice__c;
                insertedLineItems[i].Apttus_Config2__NetUnitPrice__c = plis[i].Apttus_QPConfig__NetUnitPrice__c;
                insertedLineItems[i].Apttus_Config2__PriceUom__c = plis[i].Apttus_QPConfig__PriceUom__c;
                insertedLineItems[i].Apttus_Config2__Uom__c = plis[i].Apttus_QPConfig__Uom__c;
                insertedLineItems[i].Apttus_Config2__ClassificationId__c = plis[i].Apttus_QPConfig__ClassificationId__c;
                insertedLineItems[i].Apttus_Config2__NetAdjustmentPercent__c = plis[i].Apttus_QPConfig__NetAdjustmentPercent__c;
                insertedLineItems[i].PTS_Legacy_ID__c = plis[i].PTS_Legacy_ID__c;
                insertedLineItems[i].Apttus_Config2__ClassificationHierarchy__c = plis[i].Apttus_QPConfig__ClassificationHierarchy__c;
                insertedLineItems[i].Apttus_Config2__PriceListItemId__c = plis[i].Apttus_QPConfig__PriceListItemId__c;
            }
            update insertedLineItems;
        }
    }

    public void finalizeCart(Id cartId){
        //Invoke the finalize cart request to update end date with the cart
         Apttus_CPQApi.CPQ.FinalizeCartRequestDO finalizeRequest = new Apttus_CPQApi.CPQ.FinalizeCartRequestDO();
         finalizeRequest.CartId = cartId;
        //Invoke the finalize cart API to update the cart details
         Apttus_CPQApi.CPQ.FinalizeCartResponseDO finalizeResponse = Apttus_CPQApi.CPQWebService.finalizeCart(finalizeRequest);
         System.debug(finalizeResponse);
    }
}