/**
* @author Garvit Totuka
* @date 18 Sep 2017
*
* @group user Sharing Management
*
* @description Opportunity Share Helper Test class to functionalities in Opportunity Share Dao class
*/
@isTest
private class OpportunityShareDaoTest {
    
    static final Integer RECORD_COUNT = 1;
    static final String PROFILE_NAME = 'PTS Partner Community';
    static final String OPPORTUNITY_ACCESS_TYPE = 'Edit';
    
    static List<Account> accounttList;
    static List<Apttus_Config2__PriceList__c> priceList;
    static List<Opportunity> opportunityList;
    static List<Contact> contactList;
    static User testUser;
    
    @isTest static void createOpportunityShareTest() {
        setupDataUser();
        Set<ID> accountIds = new Set<ID>();
        accountIds.add(accounttList[0].Id);
        Set<ID> userIds = new Set<ID>();
        userIds.add(testUser.Id);
        Test.startTest();
            OpportunityShareDao.createOpportunityShare(accountIds,userIds,OPPORTUNITY_ACCESS_TYPE);
        Test.stopTest();
        List<OpportunityShare> opptyShare = [SELECT Id FROM OpportunityShare WHERE OpportunityId = :opportunityList[0].Id];
        System.assert(!opptyShare.isEmpty(), 'OpportunityShare should be created');
    }
    
    @isTest static void createOpportunityShare_FutureTest() {
        setupDataUser();
        Set<ID> accountIds = new Set<ID>();
        accountIds.add(accounttList[0].Id);
        Set<ID> userIds = new Set<ID>();
        userIds.add(testUser.Id);
        Test.startTest();
            OpportunityShareDao.createOpportunityShare_Future(accountIds,userIds,OPPORTUNITY_ACCESS_TYPE);
        Test.stopTest();
        List<OpportunityShare> opptyShare = [SELECT Id FROM OpportunityShare WHERE OpportunityId = :opportunityList[0].Id];
        System.assert(!opptyShare.isEmpty(), 'OpportunityShare should be created');
    }
    
    @isTest static void deleteOpportunityShare_FutureTest() {
        setupDataOpportunity();
        Set<ID> oldOpptyIds = new Set<ID>(); 
        oldOpptyIds.add(opportunityList[0].Id);
        Test.startTest();
            OpportunityShareDao.deleteOpportunityShare_Future(oldOpptyIds);
        Test.stopTest();
        List<OpportunityShare> opptyShare = [SELECT Id FROM OpportunityShare WHERE OpportunityId IN :oldOpptyIds AND RowCause = :'Manual'];
        System.assert(opptyShare.isEmpty(), 'OpportunityShare should be deleted');
    }
    
    @isTest static void deleteOpportunityShareFromUser_FutureTest() {
        setupDataUser();
        Set<ID> userIds = new Set<ID>();
        userIds.add(testUser.Id);
        Test.startTest();
            OpportunityShareDao.deleteOpportunityShareFromUser_Future(userIds);
        Test.stopTest();
        List<OpportunityShare> opptyShare = [SELECT Id FROM OpportunityShare WHERE UserOrGroupId IN :userIds AND RowCause = :'Manual'];
        System.assert(opptyShare.isEmpty(), 'OpportunityShare should be deleted');
    }
    
    @isTest static void createOpportunityShareExceptionTest() {
        setupDataUser();
        Set<ID> accountIds = new Set<ID>();
        accountIds.add(accounttList[0].Id);
        Set<ID> userIds = new Set<ID>();
        userIds.add(testUser.Id);
        DMLException expectedException;
        Test.startTest();
            try {
            OpportunityShareDao.createOpportunityShare(accountIds,userIds,'Read/Write');
            }
            catch (DMLException dmx) {
                expectedException = dmx;
            }
        Test.stopTest();
        system.assertNotEquals(null, expectedException);
    }
   
    static void setupDataUser() {
        accounttList = TestingUtils.createAccounts('test Account',RECORD_COUNT, true);
        priceList = TestingUtils.createPriceList('Sika Test Price List',RECORD_COUNT,true);
        opportunityList = TestingUtils.createOpportunities(RECORD_COUNT, accounttList[0].Id, false);
            opportunityList[0].Price_List__c = priceList[0].Id;
            insert opportunityList;
        contactList = TestingUtils.createContacts(accounttList[0].Id, RECORD_COUNT, true);
        testUser = TestingUtils.createTestUser('garvittest',PROFILE_NAME);
            testUser.ContactId = contactList[0].Id;
            insert testUser;
    }

    static void setupDataOpportunity() {
        accounttList = TestingUtils.createAccounts('test Account',RECORD_COUNT, true);
        contactList = TestingUtils.createContacts(accounttList[0].Id, RECORD_COUNT, true);
        testUser = TestingUtils.createTestUser('garvittest',PROFILE_NAME);
            testUser.ContactId = contactList[0].Id;
            insert testUser;
        priceList = TestingUtils.createPriceList('Sika Test Price List',RECORD_COUNT,true);
        opportunityList = TestingUtils.createOpportunities(RECORD_COUNT, accounttList[0].Id, false);
            opportunityList[0].Price_List__c = priceList[0].Id;
            insert opportunityList;
    }
}