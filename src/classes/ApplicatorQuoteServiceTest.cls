@isTest
class ApplicatorQuoteServiceTest {

    @isTest
    public static void testIndicateSourcePlantInsert() {
        // insert queried Freight
        Freight_Charge__c fc = (Freight_Charge__c) TestFactory.createSObject(
            new Freight_Charge__c(
                Destination_State__c = 'IL',
                Price__c = 50,
                Transit_Days__c = 10,
                FOB_Zip__c = '02021'
            ),
            true
        );

        // insert not wanted Freight
        Freight_Charge__c fc2 = (Freight_Charge__c) TestFactory.createSObject(
            new Freight_Charge__c(
                Destination_State__c = 'Bratislava',
                Price__c = 100,
                Transit_Days__c = 100,
                FOB_Zip__c = '02021'
            ),
            true
        );

        // insert freight used for update assert
        Freight_Charge__c fc3 = (Freight_Charge__c) TestFactory.createSObject(
            new Freight_Charge__c(
                Destination_State__c = 'IL',
                Price__c = 313131,
                Transit_Days__c = 19,
                FOB_Zip__c = '64120'
            ),
            true
        );

        Account acc = (Account) TestFactory.createSObject(new Account(name = 'Test Acc'), true);

        Apttus_Config2__PriceList__c pric = (Apttus_Config2__PriceList__c)
            TestFactory.createSObject(new Apttus_Config2__PriceList__c(), true);

        Opportunity opp = (Opportunity)
            TestFactory.createSObject(new Opportunity(Price_list__c = pric.Id, State__c = 'IL', Amount = 5, Roofing_Area_SqFt__c = 5, Estimated_Ship_Date__c = Date.today()), true);

        // Apttus_Proposal__Proposal__c
        Apttus_Proposal__Proposal__c aPP = (Apttus_Proposal__Proposal__c) TestFactory.createSObject(
            new Apttus_Proposal__Proposal__c(
                Apttus_Proposal__Opportunity__c = opp.Id,
                Apttus_QPConfig__ConfigurationFinalizedDate__c = date.today()
            ),
            true
        );

        Applicator_Quote__c appQ = (Applicator_Quote__c) TestFactory.createSObject(
            new Applicator_Quote__c(
                Opportunity__c = opp.Id,
                warehouse__c = 'Canton, MA',
                Quote_Apttus__c = aPP.Id
            ),
            true
        );

        Applicator_Quote__c appTest =
            [Select Estimated_Freight__c, No_of_days_for_transit__c from Applicator_Quote__c where id = :appQ.Id];

        System.assertEquals(fc.Price__c, appTest.Estimated_Freight__c);
        System.assertEquals(fc.Transit_Days__c, appTest.No_of_days_for_transit__c);

        appTest.warehouse__c = 'Kansas City, MO';
        update appTest;

        Applicator_Quote__c appTest2 =
            [Select Estimated_Freight__c, No_of_days_for_transit__c from Applicator_Quote__c where id = :appTest.Id];

        System.assertEquals(fc3.Price__c, appTest2.Estimated_Freight__c);
        System.assertEquals(fc3.Transit_Days__c, appTest2.No_of_days_for_transit__c);
    }
}