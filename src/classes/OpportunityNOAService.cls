/*
* Author: Martin Kona
* Company: Bluewolf
* Date: May 26, 2016
* Description: NOA Accepted Opportunity service
* 
*/

public class OpportunityNOAService {    
    
    public static void acceptProposalsInsert(List<Opportunity> newOpps) {
        List<Opportunity> oppsToInsert= new List<Opportunity>();  
        
        for (Opportunity opp : newOpps) {
            if (opp.NOA_Awarded__c == TRUE && opp.Take_Action__c != 'NOA Created') {
                opp.Take_Action__c = 'NOA Created'; 
            }
        }
        
        // we dont expect child proposals for this new opportunity
    }    
            
    public static Boolean acceptProposalsUpdate(Map<Id, Opportunity> oldOpps, Map<Id, Opportunity> newOpps) {
        
        Map<Id, Opportunity> changedOpps = new Map<Id, Opportunity>();
        // get changed records
        for (Id key : newOpps.keySet()) {
            if (oldOpps.get(key).NOA_Awarded__c != newOpps.get(key).NOA_Awarded__c) {
				changedOpps.put(key, newOpps.get(key));            	    
            }
        }

        if (changedOpps.values().size() == 0) {
            return FALSE;
        }        
        
        List<Opportunity> oppsToUpdate = new List<Opportunity>();
        List<Apttus_Proposal__Proposal__c> appsToUpdate = new List<Apttus_Proposal__Proposal__c>();
        
        for (List<Opportunity> opps : [SELECT Id, NOA_Awarded__c, Take_Action__c, AccountId, 
                                       	(SELECT Apttus_Proposal__Approval_Stage__c, Apttus_Proposal__Account__c FROM Apttus_Proposal__R00N70000001yUfDEAU__r) FROM Opportunity WHERE Id = :changedOpps.keySet()]) {                                                                        			                                            
            for (Opportunity opp : opps) {    
                if (opp.NOA_Awarded__c == TRUE) {
                    opp.Take_Action__c = 'NOA Created';
                    oppsToUpdate.add(opp);
                    // realationship object name should be pulled from custom setting or is same in every instance?
                    // what if opp is inserted as awarded and we later add proposals..we need proposal trigger to update stage and account
                        if (opp.Apttus_Proposal__R00N70000001yUfDEAU__r.size() > 0) {
                            for (Apttus_Proposal__Proposal__c app : opp.Apttus_Proposal__R00N70000001yUfDEAU__r) {
                                if (app.Apttus_Proposal__Approval_Stage__c != 'Accepted' || app.Apttus_Proposal__Account__c != opp.AccountId) {
                                    app.Apttus_Proposal__Approval_Stage__c = 'Accepted';
                                    app.Apttus_Proposal__Account__c = opp.AccountId;
                                    appsToUpdate.add(app);
                                }
                            }    
                        }                    
                    }                                        
                }                        		
        }
                
        update oppsToUpdate;
        update appsToUpdate;
        
        return true;
    }

}