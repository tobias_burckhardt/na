global without sharing class BuildingGroupController {
    public static final String LOCKED_DEV_NAME_RECORD_TYPE = 'Building_Group_Locked';

    private Id recordId;
    public Building_Group__c buildingGroup { get; set; }
    public List<BuildingGroupProductWrapper> bgpList { get; set; }

    public Id selectedProductToDelete { get; set; }
    public Integer selectedProductToCloneIndex { get; set; }

    public Boolean editMode { get; private set; }

    public BuildingGroupController(ApexPages.StandardController stdController) {
        recordId = stdController.getId();
        if (string.isBlank(recordId)) {
            recordId = ApexPages.currentPage().getParameters().get('id');
        }
        buildingGroup   = BuildingGroupController.GetBuildingGroup(recordId);
        String recordTypeDevName = [
            SELECT RecordType.DeveloperName FROM Building_Group__c WHERE Id = :recordId
        ].RecordType.DeveloperName;
        editMode = !LOCKED_DEV_NAME_RECORD_TYPE.equalsIgnoreCase(recordTypeDevName);
    }

    public static Building_Group__c getBuildingGroup(Id bgId) {
        Building_Group__c results = new Building_Group__c();
        results = [SELECT Id, Name, Category__c, Category_Hierarchy__c, Category_Hierarchy__r.Apttus_Config2__HierarchyId__c
                   FROM Building_Group__c
                   WHERE Id = :bgId];
        return results;
    }

    /**
     * Used by BulidingGroupProducts.page
     */
    public void getBuildingGroupProducts() {
        bgpList = new List<BuildingGroupProductWrapper>();

        for (Building_Group_Product__c bgp : [
            SELECT Id, Name, Primary_Membrane__c, Building_Group__c, Building_Group__r.Intended_Warranty_Product__c,
                   Building_Group__r.Category_Hierarchy__c, Category_Hierarchy__r.Name, Product_lookup__c,
                   Product_Lookup__r.Name, New__c, Attachment_Group__c, Field__c, Cornerr__c, Perimeterr__c, 
                   Product_lookup__r.APTS_Roofing_Description__c, Product_Lookup__r.PTS_IsMembrane__c
            FROM Building_Group_Product__c
            WHERE Building_Group__c=:buildingGroup.Id
            AND (NOT Category_Hierarchy__r.Name  LIKE '%Warranty%')
            ORDER BY Sort_Order__c ASC
        ]) {
            
            BuildingGroupProductWrapper temp = new BuildingGroupProductWrapper(bgp);
            bgpList.add(temp);
        }
    }

    /*
    @description used by BulidingGroupProducts.page
    */
    public PageReference newLayer() {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'New Layer!'));
        PageReference pageRef = new PageReference('/apex/AddBuildingGroupProduct');
        pageRef.setRedirect(true);
        pageRef.getParameters().put('id', buildingGroup.Id);
        if (buildingGroup.Category__c != null) {
            pageRef.getParameters().put('cat', buildingGroup.Category__c);
        } else {
            pageRef.getParameters().put('cat', buildingGroup.Category_Hierarchy__r.Apttus_Config2__HierarchyId__c);
        }
        return pageRef;
    }

    /*
    @description used by BulidingGroupProducts.page
    */
    public void save() {
        try {
            List<Building_Group_Product__c> bgpToUpdate = new List<Building_Group_Product__c>();
            List<Building_Group__c> bgToUpdate          = new List<Building_Group__c>();
            for (BuildingGroupProductWrapper bgpw : bgpList) {
                bgpToUpdate.add(bgpw.bgProduct);
                /*
                if(bgpw.bgProduct.Primary_Membrane__c){
                    Building_Group__c tempBG = new Building_Group__c(Id=bgpw.bgProduct.Building_Group__c, Membrane__c=bgpw.bgProduct.Product_lookup__c);
                    bgToUpdate.add(tempBG);
                }
                */
            }
            upsert bgpToUpdate;
            if (!bgToUpdate.isEmpty()) {
                update bgToUpdate;
            }
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Saved!'));
        } catch (DmlException dmle) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'DmlException: ' + dmle.getMessage()));
        }
    }

    public void deleteSelected() {
        if(String.isBlank(selectedProductToDelete)) {
            getBuildingGroupProducts();
            return;
        }
        Building_Group_Product__c bgpToDelete = new Building_Group_Product__c(Id = selectedProductToDelete);
        try {
            delete bgpToDelete;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Deleted! ' + selectedProductToDelete));
            getBuildingGroupProducts();
        } catch (DmlException e) {
            ApexPages.addMessages(e);
        }
    }

    public void cloneSelectedProduct() {
        BuildingGroupProductWrapper clonedProduct = bgpList.get(selectedProductToCloneIndex).cloneRecord();
        bgpList.add(clonedProduct);
    }

    public class BuildingGroupProductWrapper {
        public Boolean isSelected {get; set;}
        public Building_Group_Product__c bgProduct {get; set;}
        public String partTypeProductLabel { get; private set; }

        public BuildingGroupProductWrapper(Building_Group_Product__c bgp) {
            this.isSelected         = false;
            this.bgProduct          = bgp;
            this.partTypeProductLabel = this.buildPartTypeProductLabel();
        }

        public BuildingGroupProductWrapper cloneRecord() {
            BuildingGroupProductWrapper clone = new BuildingGroupProductWrapper(this.bgProduct.clone(false, true));
            clone.isSelected = this.isSelected;
            return clone;
        }

        private String buildPartTypeProductLabel() {
            List<String> partTypeProductLabels = new List<String>{
                bgProduct.Category_Hierarchy__r.Name,
                bgProduct.Product_lookup__r.Name
            };
            return String.join(partTypeProductLabels, ' - ');
        }
    }

    @remoteAction
    global static String setOrder(List<String> myStrings) {
        system.debug('myStrings:' + myStrings);
        List<Building_Group_Product__c> products = new List<Building_Group_Product__c>();
        for (integer i = 0; i < myStrings.Size(); i++) {
            products.add(new Building_Group_Product__c(Id = myStrings[i], Sort_Order__c = integer.ValueOf(i)));
        }
        update products;

        return 'Remote action success';
    }
}