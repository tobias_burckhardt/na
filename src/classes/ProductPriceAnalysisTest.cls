@isTest(seeAllData='true')
public class ProductPriceAnalysisTest {
    static testMethod void testFunctionalities(){
		Id accountId = PricingTestDataFactory.createAccount();
        Id customPricebookId = PricingTestDataFactory.createCustomPricebook();
        List<Product2> products = PricingTestDataFactory.createProducts();
        List<String> productIds = new List<String>();
        for(Product2 product:products){
            productIds.add(product.Id);
        }
        Boolean areProductsAddedToStdPriceBook = PricingTestDataFactory.addProductsToStandardPriceBook(productIds);
        List<PricebookEntry> insertedPricebookEntries = PricingTestDataFactory.addProductsToCustomPriceBook(productIds,customPricebookId);
        List<String> pricebookEntryIds = new List<String>();
        for(PricebookEntry pricebookEntry:insertedPricebookEntries){
            pricebookEntryIds.add(pricebookEntry.Id);
        }
        List<QuoteLineItem> quoteProducts = PricingTestDataFactory.createSPAOppAndQuoteWithLineItems(accountId,customPricebookId,insertedPricebookEntries);
        System.debug('quoteProducts--'+quoteProducts);
        
        Test.startTest();
        	ProductPriceComparisonAnalysisController controllerObj = new ProductPriceComparisonAnalysisController();
        	System.assertNotEquals(null, controllerObj);
        	String ajaxData = '{"quoteLineItemId":"'+quoteProducts.get(0).Id+'"}';
        	System.assertNotEquals(null, ProductPriceComparisonAnalysisController.getQuoteLineItemInfo(ajaxData));
        	ajaxData = '[{"quantity":"40.00","salesPrice":"10.00","quoteLineItemId":"'+quoteProducts.get(0).Id+'"}]';
        	System.assertNotEquals(null, ProductPriceComparisonAnalysisController.upsertQuoteLineItems(ajaxData));
        	ajaxData = '{"productCode":"123450","accId":"'+accountId+'","recordType":"SPA_Pricing"}';
        	System.assertNotEquals(null, ProductPriceComparisonAnalysisController.getAnalyticsData(ajaxData));
        Test.stopTest();
    }
}