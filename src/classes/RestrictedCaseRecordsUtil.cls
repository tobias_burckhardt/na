public class RestrictedCaseRecordsUtil {

    /**
     * Checks to see if the provided objectId is a Case and the record type name is in the list
     *   of restricted case record types defined in the Restricted Case Record Types custom setting
     * Params:
     *   objectId - the id of the object to check
     *   recordTypeName - the developer name of the record type of the same object
     * Returns:
     *   true if the objectId is a Case and the record type name is in the list of restricted record types
     *   false otherwise
     */
    public static boolean isRestrictedCaseRecordType(Id objectId, String recordTypeName) {
        if (objectId == null) {
            return false;
        }

        if (objectId.getSobjectType() == Schema.Case.SObjectType) {
            //Get list of record types from custom setting
            List<RestrictedCaseRecordTypes__c> recordTypes = RestrictedCaseRecordTypes__c.getAll().values();

            for (RestrictedCaseRecordTypes__c rType : recordTypes) {
                if (rType.Record_Type_Name__c == recordTypeName) {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Retrieves the Restricted Fields custom setting for the given name and then compares the two objects
     *   to see if any of the restricted fields have changed
     * Params:
     *   oldObj - the old version of the object to compare
     *   newObj - the new version of the object to compare
     *   customSettingName - the name of the custom setting record that contains the comma separated list of
     *     restricted fields
     * Returns:
     *   false if any of the specified fields have changed
     *   true otherwise
     */
    public static boolean checkUpdatedFields(sObject oldObj, sObject newObj, String customSettingName) {
        RestrictedFields__c customSetting = RestrictedFields__c.getInstance(customSettingName);

        if (oldObj == null || newObj == null || customSetting == null) {
            return true;
        }

        List<String> restrictedFields = customSetting.Field_Names__c.split(',');
        return checkFields(oldObj, newObj, restrictedFields);
    }
    
    /**
     * Compares the two objects to see if any of the restricted fields in the given field set have changed
     * Params:
     *   oldObj - the old version of the object to compare
     *   newObj - the new version of the object to compare
     *   fieldSet - the field set which defines the fields which should not be updated
     * Returns:
     *   false if any of the specified fields have changed
     *   true otherwise
     */
    public static boolean checkUpdatedFields(sObject oldObj, sObject newObj, Schema.FieldSet fieldSet) {
        if (oldObj == null || newObj == null || fieldSet == null) {
            return true;
        }

        List<String> restrictedFields = new List<String>();
        for (Schema.FieldSetMember member : fieldSet.getFields()) {
            restrictedFields.add(member.getFieldPath());
        }
        return checkFields(oldObj, newObj, restrictedFields);
    }

    //Private helper method which checks the specified fields for change; returns false if any have changed
    //  short circuits if it finds a change to one of the restricted fields
    private static boolean checkFields(sObject oldObj, sObject newObj, List<String> fieldNames) {
        for (String field : fieldNames) {
            if (oldObj.get(field) != newObj.get(field)) {
                return false;
            }
        }

        return true;
    }
}