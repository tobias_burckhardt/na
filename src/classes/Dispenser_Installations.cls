public with sharing  class Dispenser_Installations {
    public Dispenser_Installation__c  di{get;set;}
    public list<Concrete_Dispenser_Product__c> justfylist{get;set;}
    public list<Asset_Type__c> justlist{get;set;}
    public Integer recCount{get;set;}
    public Integer recCount1{get;set;}
    public integer count{get;set;}
    public integer count1{get;set;}
    public Integer selectedRowIndex{get;set;}
    public Integer selectedRowIndex1{get;set;}
    // public Asset_Type__c js1{get;set;}
    public id dispid; 
    //  public list<string> asse{get;set;}
    public string INSTALLATIONDETAILS{get; set;}
    public string INFORMATION{get; set;}
    public string CUSTOMERDETAILS{get; set;}
    public string CUSTOMERSOLDTOACCOUNTDETAILS{get; set;}
    public string CUSTOMERSHIPTOACCOUNTDETAILS{get;set;}
    public string CAPITALINVESTMENT{get;set;}
    public string ACTUALINVESTMENT{get;set;}
    public string ESTIMATEDINVESTMENT{get;set;}
    public string PRODUCTDETAILS{get;set;}
    public string TypeOfAsset{get;set;} 
    public string AssetSpecification{get;set;}
    public string SuppliersName{get;set;}
    public string Quantity{get;set;}
    public string PurchaseCost{get;set;}
    // public string ActualTotalCost  {get;set;}
    public string TotalCost{get;set;}  
    public string MaterialName{get;set;}
    public string ProjectedSalesVolume{get;set;}
    public string  ExceptedSellingPrice{get;set;}
    public string EstimatedAnnualSales{get;set;}
    public string  StandardCost{get;set;}
    public string  GrossMargin{get;set;}
    public string  GrossMarginInPercentage{get;set;}
    public string  Add{get;set;}
    public string CustomershiptoNewAccount{get;set;}
    public account acc{get;set;}
    public account acc1{get;set;}
    public Dispenser_Installations(ApexPages.StandardController controller) {
        User u = [SELECT toLabel(LanguageLocaleKey) FROM User WHERE Id = :UserInfo.getUserId()];
        //  acc = new account();
        acc1 = new account();
        if(u.LanguageLocaleKey=='Français'){
            INSTALLATIONDETAILS = 'DÉTAILS DINSTALLATION';
            INFORMATION = 'INFORMATION';
            CUSTOMERDETAILS ='DÉTAILS DU CLIENT';
            CUSTOMERSOLDTOACCOUNTDETAILS = 'DÉTAILS DU COMPTE CLIENT VENDU';
            CUSTOMERDETAILS = 'DÉTAILS DU CLIENT';
            CUSTOMERSHIPTOACCOUNTDETAILS = 'LA CLIENTÈLE AUX COMPTES';
            CAPITALINVESTMENT = 'INVESTISSEMENT EN CAPITAL';
            ACTUALINVESTMENT= 'INVESTISSEMENT RÉEL';
            ESTIMATEDINVESTMENT = 'ESTIMATION DINVESTISSEMENT';
            PRODUCTDETAILS = 'DÉTAILS DU PRODUIT';
            TypeOfAsset = 'Type dactif' ;
            AssetSpecification = 'Spécification de lactif' ;
            SuppliersName = 'Nom du fournisseur';
            Quantity = 'Quantité';
            PurchaseCost = 'Coût dachat';
            TotalCost = 'Coût total';
            CustomershiptoNewAccount = 'Client expédier à nouveau compte';
            
            MaterialName = 'Nom du matériau';
            ProjectedSalesVolume = 'Volume de vente projeté';
            ExceptedSellingPrice = 'Prix de vente excepté';
            EstimatedAnnualSales = 'Ventes annuelles estimées';
            StandardCost = 'Coût standard'; 
            GrossMargin = 'Marge brute';
            GrossMarginInPercentage = 'Marge brute en pourcentage'; 
            
            Add = 'Ajouter';
            
        }
        else{
            INSTALLATIONDETAILS = 'INSTALLATION DETAILS';
            INFORMATION = 'INFORMATION';
            CUSTOMERDETAILS = 'CUSTOMER DETAILS';
            CUSTOMERSOLDTOACCOUNTDETAILS = 'CUSTOMER SOLD-TO ACCOUNT DETAILS';
            CUSTOMERSHIPTOACCOUNTDETAILS ='CUSTOMER SHIP-TO ACCOUNT DETAILS';
            CAPITALINVESTMENT = 'CAPITAL INVESTMENT';
            PRODUCTDETAILS = 'PRODUCT DETAILS';
            TypeOfAsset = 'Type Of Asset' ;
            AssetSpecification = 'Asset Specification'; 
            SuppliersName = 'Suppliers Name';
            Quantity = 'Quantity';
            PurchaseCost = 'Purchase Cost';
            TotalCost = 'Total Cost';    
            
            MaterialName = 'Material Name';
            ProjectedSalesVolume = 'Projected Sales Volume';
            ExceptedSellingPrice = 'Expected Selling Price';
            EstimatedAnnualSales = 'Estimated Annual Sales';
            StandardCost = 'Standard Cost';
            GrossMargin = 'Gross Margin';
            GrossMarginInPercentage = 'Gross Margin In Percentage';
            
            Add = 'Add';
        }
        di= new Dispenser_Installation__c();
        recCount=1;
        recCount1 = 1;
        justlist = new list<Asset_Type__c>();
        
        justfylist = new list<Concrete_Dispenser_Product__c>();
        Asset_Type__c j = new Asset_Type__c();
        j.Type_Of_Asset1__c = '--None--';//Air line Fitting
        j.Asset_Specification1__c = 'Freight';
        j.Quantity__c = 1;
        j.S_No__c=recCount;
        justlist.add(j);
        
        Concrete_Dispenser_Product__c justfy = new Concrete_Dispenser_Product__c();
        justfy.S_No__c=recCount1;
        justfylist.add(justfy);
    }
    public void Add(){     
        recCount+=1;
        Asset_Type__c j = new Asset_Type__c();  
        j.S_No__c=recCount;      
        justlist.add(j);         
    }
    public void Del(){
        if(justlist.size()>0){
            for(Integer i=0; i<justlist.size();i++){
                if(justlist[i].S_No__c==selectedRowIndex && i!=0){
                    count=i;
                    justlist.remove(i);
                }
            }
        }
    }
    public void totalcost(){
        for(Asset_Type__c js : justlist){
            if(js.Quantity__c !=null && js.Purchase_Cost__c !=null){
                js.Total_Cost__c = js.Quantity__c*js.Purchase_Cost__c;
            }
        }
    }
    public void purchasecost(){
        for(Asset_Type__c js : justlist){
            if(js.Type_Of_Asset1__c !='' && js.Asset_Specification1__c !='' && js.Suppliers_Name1__c !=''){
                list<Dispenser_Line_Item__c> dli = [select id,Purchase_Cost__c from Dispenser_Line_Item__c where Type_Of_Assert__c =: js.Type_Of_Asset1__c and Asset_Specification__c =: js.Asset_Specification1__c and Suppliers_Name__c =:js.Suppliers_Name1__c];
                if(dli.size()>0)
                    js.Purchase_Cost__c = dli[0].Purchase_Cost__c;
            }
        }
        totalcost();
    }
    public void address(){
        if(di.Customer_Name1__c != null){
            account acc = [select id,Name, BillingStreet, BillingCity, BillingState, BillingPostalCode,
                           BillingCountry, BillingLatitude, BillingLongitude,SAP_Account_Code__c from account where id =:di.Customer_Name1__c ];
            di.Street__c= acc.BillingStreet;
            di.City__c = acc.BillingCity;
            di.State_Province__c = acc.BillingState;
            di.Zip_Postal_Code__c = acc.BillingPostalCode;
            di.Country__c = acc.BillingCountry;
            di.Customer_code__c = acc.SAP_Account_Code__c;
        } 
    }
    public void address1(){        
        if(di.Account__c != null){
            account acc = [select id,Name, BillingStreet, BillingCity, BillingState, BillingPostalCode,
                           BillingCountry, BillingLatitude, BillingLongitude,SAP_Account_Code__c from account where id =:di.Account__c ];
            di.Installation_Street__c= acc.BillingStreet;
            di.Installation_City__c = acc.BillingCity;
            di.Installation_State_Province__c = acc.BillingState;
            di.Installation_Zip_Postal_Code__c = acc.BillingPostalCode;
            di.Installation_Country__c = acc.BillingCountry;
            di.Customer_s_Account_Code__c = acc.SAP_Account_Code__c;
        }
    }
    public void Add1(){    
        recCount1+=1;
        Concrete_Dispenser_Product__c j = new Concrete_Dispenser_Product__c();  
        j.S_No__c=recCount1;      
        justfylist.add(j);           
    }
    public void Del1(){
        if(justfylist.size()>0){
            for(Integer i=0; i<justfylist.size();i++){
                if(justfylist[i].S_No__c==selectedRowIndex1){
                    count1=i;
                    justfylist.remove(i);
                }
            }
        }
    }
    public void productvalue(){}
    public void productvalue1(){
        for(Concrete_Dispenser_Product__c js : justfylist){
            if(js.MaterialName__c !=null){
                Product2 pr = [select id,name,Projected_Sales_Volume__c,Excepted_Selling_Price__c,Estimated_Annual_Sales__c,Standard_Cost__c,Gross_Margin__c,Gross_Margin_In_Percentage__c from Product2 where id =: js.MaterialName__c];
                js.Projected_Sales_Volume__c = pr.Projected_Sales_Volume__c;
                js.Excepted_Selling_Price__c = pr.Excepted_Selling_Price__c;
                js.Estimated_Annual_Sales__c = pr.Estimated_Annual_Sales__c;
                js.Standard_Cost__c = pr.Standard_Cost__c;
                js.Gross_Margin__c = pr.Gross_Margin__c;
                js.Gross_Margin_In_Percentage__c = pr.Gross_Margin_In_Percentage__c;
            }
        }   
    }
    public PageReference savec(){
        try {
             if(di.Region__c == null){
                Apexpages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.FATAL, 'Please Enter Business Unit/Region'));
                return NULL;
            }
            if(di.Customer_Name1__c == null){
                Apexpages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.FATAL, 'Please enter SOLD-TO ACCOUNT NAME'));
                return NULL;
            }
            
            if(di.Type_of_Installation__c == ''){
                Apexpages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.FATAL, 'Please enter Type of Installation'));
                return NULL;
            }
            if(di.File_del__c == Null){
                Apexpages.addMessage(new Apexpages.Message(ApexPages.SEVERITY.FATAL, 'Please upload your File'));
                return NULL;
            }
            if(di.New_Account__c== 'Yes'){
                insert acc1;
                di.Account__c = acc1.id;
                di.Installation_Street__c= acc1.BillingStreet;
                di.Installation_City__c = acc1.BillingCity;
                di.Installation_State_Province__c = acc1.BillingState;
                di.Installation_Zip_Postal_Code__c = acc1.BillingPostalCode;
                di.Installation_Country__c = acc1.BillingCountry;
                di.Customer_s_Account_Code__c = acc1.SAP_Account_Code__c;
            }
            insert di;
            for(integer i=0;i<justlist.size();i++){
            if(justlist[i].Type_Of_Asset1__c !='' && justlist[i].Asset_Specification1__c !='' && justlist[i].Suppliers_Name1__c !=''){
            justlist[i].Dispenser_Installation__c = di.id;
            }
            else{
            justlist.remove(i);
            }
            }
            insert justlist;
            
            for(integer i=0;i<justfylist.size();i++){
            if(justfylist[i].MaterialName__c !=null){
            justfylist[i].Dispenser_Installation__c = di.id;
            }
            else{
            justfylist.remove(i);
            }
            }
            insert justfylist;
            PageReference pg = new PageReference('/'+di.id);
            pg.setRedirect(true);
            return pg;
        }
       catch(DMLException de) {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.FATAL, de.getDmlMessage(0)));
            return NULL;
        }
        catch(Exception e) {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.FATAL, e.getMessage()));
            return NULL;
        }
    }
    public PageReference cancel(){
      // PageReference pg = new PageReference('/'+di.id);
     PageReference pg = ApexPages.currentPage();
        return pg;
    }
}