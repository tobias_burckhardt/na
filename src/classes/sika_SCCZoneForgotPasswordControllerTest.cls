@IsTest
private class sika_SCCZoneForgotPasswordControllerTest {
	private static PageReference pageRef;
    private static User guestUser;
    private static User SCCZoneUser;
    private static sika_SCCZoneForgotPasswordController ctrl;

    // static initializer
    static {
        // create a guest (anonymous) user to run the test as
        guestUser = sika_SCCZoneTestFactory.createUser(new Map<String, String>{'profileId' =>  sika_SCCZoneTestFactory.getGuestUserProfileId()});
        insert(guestUser);
        
        // create an SCCZone user to validate against.  (Query only searches for usernames associated with SCC Zone users; not for all Salesforce users.  Not opening up that kind of security hole here!)
        Map<String, sObject> usefulStuff = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount(new Map<String, String>{'contact_email' => 'test@us.sika.com',
                                                                                                                              'userName' => 'test@us.sika.com.username'});
        SCCZoneUser = (User) usefulStuff.get('SCCZoneUser');
    }

    private static void init(){
        // set page we'll be testing
        pageRef = Page.sika_SCCZoneForgotPasswordPage;
        Test.setCurrentPage(pageRef);
    }


    @IsTest static void test_happy_path(){
        init();
        System.runAs(guestUser){
            // create an instance of the page's controller
            ctrl = new sika_SCCZoneForgotPasswordController();

            // confirm that the page loaded. (Didn't redirect to an error page)
            System.assertEquals(pageRef.getUrl(), ApexPages.currentPage().getUrl());

            ctrl.userName = SCCZoneUser.userName;
            PageReference submitResponse = ctrl.submitRequest();
            System.assert(!ctrl.hasError, 'The page has an error when it should not');
            System.assert(submitResponse.getUrl().contains(pageRef.getUrl()), 'We should be on SCC Zone Page but we are on :' + submitResponse.getUrl() );
            System.assertEquals(submitResponse.getParameters().get('success'), 'true', 'Success parameter was not set to true');
        }

    }

    @IsTest static void test_sad_path(){
        init();
        System.runAs(guestUser){
            ctrl = new sika_SCCZoneForgotPasswordController();

            ctrl.userName = 'nobodyHere@incorrect.edu';
            PageReference submitResponse = ctrl.submitRequest();
            System.assert(ctrl.hasError == true);
            System.assertEquals(ctrl.errorText, sika_SCCZoneForgotPasswordController.usernameNotRecognizedErrorMsg);

            ctrl.userName = SCCZoneUser.userName;
            submitResponse = ctrl.submitRequest();
            System.assert(submitResponse.getUrl().contains(pageRef.getUrl()));
            System.assertEquals(submitResponse.getParameters().get('success'), 'true');
    
        }

    }

    @IsTest static void test_bad_path(){
        init();
        System.runAs(guestUser){
            ctrl = new sika_SCCZoneForgotPasswordController();
            PageReference submitResponse;

            ctrl.userName = '';
            submitResponse = ctrl.submitRequest();
            System.assert(ctrl.hasError == true);
            System.assertEquals(ctrl.errorText, sika_SCCZoneForgotPasswordController.invalidEmailErrorMsg);

            ctrl.userName = 'notanemail@com';
            submitResponse = ctrl.submitRequest();
            System.assert(ctrl.hasError == true);
            System.assertEquals(ctrl.errorText, sika_SCCZoneForgotPasswordController.invalidEmailErrorMsg);

            ctrl.userName = 'notanemail.com';
            submitResponse = ctrl.submitRequest();
            System.assert(ctrl.hasError == true);
            System.assertEquals(ctrl.errorText, sika_SCCZoneForgotPasswordController.invalidEmailErrorMsg);

            ctrl.userName = '@.com';
            submitResponse = ctrl.submitRequest();
            System.assert(ctrl.hasError == true);
            System.assertEquals(ctrl.errorText, sika_SCCZoneForgotPasswordController.invalidEmailErrorMsg);

            ctrl.userName = SCCZoneUser.email;
            submitResponse = ctrl.submitRequest();
            System.assert(submitResponse.getUrl().contains(pageRef.getUrl()));
            System.assertEquals(submitResponse.getParameters().get('success'), 'true');
    
        }
    }
}