@isTest
private class CaseTriggerHandlerTest {
    
    private static String validTestEmail = 'test@unittest.net';
    
    private static Integer orderNumber
    {
        get
        {
            if(orderNumber == NULL)
                orderNumber = 100200;
            return orderNumber++;
        }
        
        set;
    }
    private static Integer invoiceNumber
    {
        get
        {
            if(invoiceNumber == NULL)
                invoiceNumber = 00100;
            return invoiceNumber++;
        }
        
        set;
    }
    private static Integer customerNumber
    {
        get
        {
            if(customerNumber == NULL)
                customerNumber = 0;
            return customerNumber++;
        }
        
        set;
    }
    

    final static Integer NUM_OF_CASES = 40;
    
    static void setup_Cases(Boolean insertObjects)
    {
        RecordType w2cRT = [SELECT Id FROM RecordType WHERE DeveloperName='Web_to_Case' AND isActive=true AND SObjectType='Case'][0];
		RecordType wrongRT = [SELECT Id FROM RecordType WHERE DeveloperName!='Web_to_Case' AND isActive=true AND SObjectType='Case'][0];
		
		List<Contact> allContacts = new List<Contact>();
		
		List<Case> allCases = TestingUtils.createCases(NULL, NUM_OF_CASES, w2cRT.Id, false);
		UnitTest.addData( allCases ).branch('WrongRT|NoOrderNumber|NoEmail|NoInvoiceNumber|NoCustomerNumber|NoOICNumber|NoOICNumberNoEmail|Correct').tag('All Cases');
		
		// Setup all the cases properly, including the Correct cases.
		for(Case c : (List<Case>)UnitTest.get('All Cases').getList())
		{
			Contact tempContact = TestingUtils.createContact('Test First', 'Test Last', false);
			
		    c.Order_Number__c = String.valueOf(orderNumber);
		    c.Invoice_Number__c = String.valueOf(invoiceNumber);
		    c.Customer_Number__c = String.valueOf(customerNumber);
		    c.Customer_Email__c = c.Customer_Number__c + validTestEmail;
		    
		    // Create Contacts
		    tempContact.Email = c.Customer_Email__c;
		    allContacts.add( tempContact );
		}
		
		// Setup objects
		if(insertObjects){
			List<Account> accList = TestingUtils.createAccounts('Test Acc', 3 * NUM_OF_CASES);
			UnitTest.addData(accList).branch('OrderNumAcc|InvoiceNumAcc|CustomerNumAcc').tag('All Accounts');
			UnitTest.addData(allContacts).tag('All Contacts').insertAll();

			UnitTest.get('CustomerNumAcc').property('SAP_Account_Code__c').assignFrom(
				new List<String>(Pluck.strings('Customer_Number__c', UnitTest.get('All Cases').getList())));

			UnitTest.get('All Accounts').insertAll();

	    	UnitTest.addData(TestingUtils.createInvoices(new List<String>(
	    		Pluck.strings('Invoice_Number__c', UnitTest.get('All Cases').getList())
	    	))).property('Account__c').assignFrom(new List<Id>(UnitTest.getIds('InvoiceNumAcc'))).insertAll();

	    	UnitTest.addData(TestingUtils.createOpenOrders(new List<String>(
	    		Pluck.strings('Order_Number__c', UnitTest.get('All Cases').getList())
	    	))).property('Account__c').assignFrom(new List<Id>(UnitTest.getIds('OrderNumAcc'))).insertAll();
		}
		
		// Setup Bad Record Type Cases -> Will not pass filter.
		UnitTest.get('WrongRT').property('RecordTypeId').assign(wrongRT.Id);
		
		// Setup Bad Order Number Cases -> Will pass filter.
		UnitTest.get('NoOrderNumber').property('Order_Number__c').assign(NULL);
		
		// Setup Bad Email Cases -> Will pass filter.
		UnitTest.get('NoEmail').property('Customer_Email__c').assign(NULL);
		
		// Setup Bad Invoice Number Cases -> Will pass filter.
		UnitTest.get('NoInvoiceNumber').property('Invoice_Number__c').assign(NULL);
		
		// Setup Bad Customer Number Cases -> Will pass filter.
		UnitTest.get('NoCustomerNumber').property('Customer_Number__c').assign(NULL);
		
		// Setup Bad Order, Invoice, and Customer Number  -> Will pass filter.
		UnitTest.get('NoOICNumber').property('Invoice_Number__c').assign(NULL);
		UnitTest.get('NoOICNumber').property('Order_Number__c').assign(NULL);
		UnitTest.get('NoOICNumber').property('Customer_Number__c').assign(NULL);
		
		// Setup Bad Order, Invoice, and Customer Number  -> Will not pass filter.
		UnitTest.get('NoOICNumberNoEmail').property('Invoice_Number__c').assign(NULL);
		UnitTest.get('NoOICNumberNoEmail').property('Order_Number__c').assign(NULL);
		UnitTest.get('NoOICNumberNoEmail').property('Customer_Number__c').assign(NULL);
		UnitTest.get('NoOICNumberNoEmail').property('Customer_Email__c').assign(NULL);
    }
	
	static testMethod void  testFilterWebToCase() 
	{
		setup_Cases(false);
				
		// Re-tag expected Cases. (For asssert)
		UnitTest.get('Correct').tag('Expected');
		UnitTest.get('NoEmail').tag('Expected');
		UnitTest.get('NoOrderNumber').tag('Expected');
		UnitTest.get('NoCustomerNumber').tag('Expected');
		UnitTest.get('NoInvoiceNumber').tag('Expected');
		UnitTest.get('NoOICNumber').tag('Expected');
		
		Test.startTest();
			CaseTriggerHandler.filterWebToCase( UnitTest.get('All Cases').getList() );
		Test.stopTest();
		
		Set<Case> actualCasesSet = new Set<Case>(CaseTriggerHandler.allCases);
		Set<Case> expectedCasesSet = new Set<Case>((List<Case>)UnitTest.get('Expected').getList());
		System.assertEquals(expectedCasesSet, actualCasesSet, 'We expect the sets to be the same');
		
		System.assert(!CaseTriggerHandler.allCases.isEmpty(), 'We expect none of these to be in the set');
		
		System.assert(CaseTriggerHandler.allCases.containsAll( (List<Case>) UnitTest.get('NoEmail').getList() ), 'We expect the No Email cases to be in the Account set.');
		System.assert(CaseTriggerHandler.allCases.containsAll( (List<Case>) UnitTest.get('NoOrderNumber').getList() ), 'We expect the No Order cases to be in the Account set.');
		System.assert(CaseTriggerHandler.allCases.containsAll( (List<Case>) UnitTest.get('NoCustomerNumber').getList() ), 'We expect the No Customer cases to be in the Account set.');
		System.assert(CaseTriggerHandler.allCases.containsAll( (List<Case>) UnitTest.get('NoInvoiceNumber').getList() ), 'We expect the No Invoice cases to be in the Account set.');
		
		System.assert(CaseTriggerHandler.allCases.containsAll( (List<Case>) UnitTest.get('NoOrderNumber').getList() ), 'We expect the No Order cases to be in the Contact set.');
		System.assert(CaseTriggerHandler.allCases.containsAll( (List<Case>) UnitTest.get('NoCustomerNumber').getList() ), 'We expect the No Customer cases to be in the Contact set.');
		System.assert(CaseTriggerHandler.allCases.containsAll( (List<Case>) UnitTest.get('NoInvoiceNumber').getList() ), 'We expect the No Invoice cases to be in the Contact set.');
		System.assert(CaseTriggerHandler.allCases.containsAll( (List<Case>) UnitTest.get('NoOICNumber').getList() ), 'We expect the No Order, Invoice, Customer Number cases to be in the Contact set.');
	}
	
	static testMethod void testPopulateAccountAndContactMaps()
	{
	    setup_Cases(true);
	    
	    // Re-tag expected Cases. (For asssert)
		UnitTest.get('Correct').tag('Expected');
		UnitTest.get('NoEmail').tag('Expected');
		UnitTest.get('NoOrderNumber').tag('Expected');
		UnitTest.get('NoCustomerNumber').tag('Expected');
		UnitTest.get('NoInvoiceNumber').tag('Expected');
		UnitTest.get('NoOICNumber').tag('Expected');

	    CaseTriggerHandler.filterWebToCase( UnitTest.get('Expected').getList() );
	    
	    Test.startTest();
	        CaseTriggerHandler.populateAccountAndContactMaps();
	    Test.stopTest();
	    
	    System.assert(!CaseTriggerHandler.orderMap.isEmpty(), 'We expect the orderMap map to be populated.');
	    System.assert(!CaseTriggerHandler.customerToAccountMap.isEmpty(), 'We expect the CustomerToAccount map to be populated.');
	    System.assert(!CaseTriggerHandler.invoiceMap.isEmpty(), 'We expect the invoiceMap map to be populated.');
	    System.assert(!CaseTriggerHandler.emailToContactMap.isEmpty(), 'We expect the EmailToAccount map to be populated.');
	}
	
	static testMethod void testPopulateAccountAndContactMaps_EmptySets()
	{
		setup_Cases(false);
	    // Re-tag expected Cases. (For asssert)
		UnitTest.get('Correct').tag('Expected');
		UnitTest.get('NoEmail').tag('Expected');
		UnitTest.get('NoOrderNumber').tag('Expected');
		UnitTest.get('NoCustomerNumber').tag('Expected');
		UnitTest.get('NoInvoiceNumber').tag('Expected');
		UnitTest.get('NoOICNumber').tag('Expected');
		
	    CaseTriggerHandler.allCases = new Set<Case>();
	    Test.startTest();
	        CaseTriggerHandler.populateAccountAndContactMaps();
	    Test.stopTest();
	    
	    System.assert(CaseTriggerHandler.orderMap.isEmpty(), 'We expect the orderMap map to be empty.');
	    System.assert(CaseTriggerHandler.customerToAccountMap.isEmpty(), 'We expect the CustomerToAccount map to be empty.');
	    System.assert(CaseTriggerHandler.invoiceMap.isEmpty(), 'We expect the invoiceMap map to be empty.');
	    System.assert(CaseTriggerHandler.emailToContactMap.isEmpty(), 'We expect the EmailToAccount map to be empty.');
	}

	static testMethod void testPopulateCaseLookups()
	{
		setup_Cases(true);

	    // Re-tag expected Cases. (For asssert)
		UnitTest.get('Correct').tag('Expected');
		UnitTest.get('NoEmail').tag('Expected');
		UnitTest.get('NoOrderNumber').tag('Expected');
		UnitTest.get('NoCustomerNumber').tag('Expected');
		UnitTest.get('NoInvoiceNumber').tag('Expected');
		UnitTest.get('NoOICNumber').tag('Expected');
		
		List<Case> caseList = UnitTest.get('Expected').getList();

	   	CaseTriggerHandler.filterWebToCase( caseList );
	    CaseTriggerHandler.populateAccountAndContactMaps();

	    Test.startTest();
	        CaseTriggerHandler.caseFieldMapping();
	    Test.stopTest();

	    Set<Id> orderNumbers = Pluck.ids((List<Account>)UnitTest.get('OrderNumAcc').getList());
	    Set<Id> invoiceNumbers = Pluck.ids((List<Account>)UnitTest.get('InvoiceNumAcc').getList());
	    Set<Id> customerNumbers = Pluck.ids((List<Account>)UnitTest.get('CustomerNumAcc').getList());

	    Set<Id> contacts = Pluck.ids((List<Contact>)UnitTest.get('All Contacts').getList());

	    System.assert(orderNumbers.size() > 0);
	    System.assert(invoiceNumbers.size() > 0);
	    System.assert(customerNumbers.size() > 0);
	    System.assert(contacts.size() > 0);

	    for(Case c : (List<Case>)UnitTest.get('NoOrderNumber').getList()) {
	    	System.assert(invoiceNumbers.contains(c.AccountId), 'This should be mapped to the invoice number Account');
	    	System.assert(contacts.contains(c.ContactId), 'This should be mapped to a contact');
	    }

	    for(Case c : (List<Case>)UnitTest.get('NoCustomerNumber').getList()) {
	    	System.assert(invoiceNumbers.contains(c.AccountId), 'This should be mapped to the invoice number Account');
	    	System.assert(contacts.contains(c.ContactId), 'This should be mapped to a contact');
	    }

	    for(Case c : (List<Case>)UnitTest.get('NoInvoiceNumber').getList()) {
	    	System.assert(orderNumbers.contains(c.AccountId), 'This should be mapped to the invoice number Account');
	    	System.assert(contacts.contains(c.ContactId), 'This should be mapped to a contact');
	    }

	    for(Case c : (List<Case>)UnitTest.get('NoEmail').getList()) {
	    	System.assert(invoiceNumbers.contains(c.AccountId), 'This should be mapped to the invoice number Account');
	    	System.assert(c.Contact == null);
	    }
	    
	    for(Case c : (List<Case>)UnitTest.get('NoOICNumber').getList()) {
	    	System.assert(c.Account == null);
	    	System.assert(contacts.contains(c.ContactId), 'This should be mapped to a contact');
	    }

	}

	static testMethod void caseTriggerTest()
	{
		setup_Cases(true);

	    // Re-tag expected Cases. (For asssert)
		UnitTest.get('Correct').tag('Expected');
		UnitTest.get('NoEmail').tag('Expected');
		UnitTest.get('NoOrderNumber').tag('Expected');
		UnitTest.get('NoCustomerNumber').tag('Expected');
		UnitTest.get('NoInvoiceNumber').tag('Expected');
		UnitTest.get('NoOICNumber').tag('Expected');
		
	    Test.startTest();
	        UnitTest.get('All Cases').insertAll();
	    Test.stopTest();
	    
	    List<Case> resultCases = [SELECT Id, AccountId, ContactId 
	    							FROM Case
	    							WHERE Account.Id != null
	    							OR Contact.Id != null];
	    Set<Id> accountIds = Pluck.Ids('AccountId', resultCases);
	    Set<Id> contactIds = Pluck.Ids('ContactId', resultCases);
		
	    Set<Id> orderNumbers = Pluck.ids((List<Account>)UnitTest.get('OrderNumAcc').getList());
	    Set<Id> invoiceNumbers = Pluck.ids((List<Account>)UnitTest.get('InvoiceNumAcc').getList());
	    Set<Id> customerNumbers = Pluck.ids((List<Account>)UnitTest.get('CustomerNumAcc').getList());


	    Set<Id> contacts = Pluck.ids((List<Contact>)UnitTest.get('All Contacts').getList());

	    
	    
	   for(Case c : resultCases) {
	   		if(c.AccountId != null) {
	   			System.assert(accountIds.contains(c.AccountId));
	   		}
	   		if(c.ContactId != null) {
	   			System.assert(contactIds.contains(c.ContactId));
	   		}
	   }
	}
}