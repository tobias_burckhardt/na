global class addProductTo {
    public Opportunity theOpp {get;set;} 
    public String selectedLang{get;set;}
    public Boolean multipleCurrencies {get; set;}
    public Boolean isMobile { get; set; }
    public addProductTo(ApexPages.StandardController controller) {

        // Need to know if org has multiple currencies enabled
        multipleCurrencies = UserInfo.isMultiCurrencyOrganization();
            
        // Get information about the Opportunity being worked on
        if(multipleCurrencies)
            theOpp = database.query('select Id, Pricebook2Id, Pricebook2.Name, CurrencyIsoCode from Opportunity where Id = \'' + controller.getRecord().Id + '\' limit 1');
        else
            theOpp = [select Id, Pricebook2Id, PriceBook2.Name from Opportunity where Id = :controller.getRecord().Id limit 1];
        selectedLang = UserInfo.getLanguage();
       
        isMobile = UserInfo.getUiTheme() == 'Theme4t';
    }  
    public String getChosenCurrency(){
        if(multipleCurrencies)
            return (String)theOpp.get('CurrencyIsoCode');
        else
            return '';
    }
    @RemoteAction
	global static String getLineItems(String dataList){
		Map<String,Object> request = (Map<String,Object>)JSON.deserializeUntyped(dataList);
		String oppId = (String)request.get('oppId');
		Map<String,Object> dataToBeSentBack = new Map<String,Object>(); 
        try{
            List<OpportunityLineItem> selectedProducts = [select Id, Quantity, PriceBookEntryId, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name,PriceBookEntry.Product2.ProductCode,PriceBookEntry.Product2.Sales_Unit__c,PriceBookEntry.Product2.Article_code__c from opportunityLineItem where OpportunityId=:oppId LIMIT 200];
        	dataToBeSentBack.put('selectedProducts',selectedProducts);
            dataToBeSentBack.put('status',true);
        }catch(Exception e){
            dataToBeSentBack.put('status',false);
        }
	    return JSON.serializePretty(dataToBeSentBack); 
	}
    @RemoteAction
	global static String getAvailableProducts(String dataList){
		Map<String,Object> dataToBeSentBack = new Map<String,Object>();
        Opportunity theOpp = null;
        List<PriceBookEntry> availableProducts = new List<PriceBookEntry>();
        List<string> preConditions = new List<String>();
        List<string> dynamicConditions = new List<String>();
        Integer LimitSize= 10;
        List<String> materialTypesAllowed = new List<String>{'FERT','HAWA','ROH'};
        String emptyString = '';
        Set<Id> selectedEntries = new Set<Id>();
        Pricebook2 theBook = null;
        Boolean multipleCurrencies = UserInfo.isMultiCurrencyOrganization();
        
        Map<String,Object> request = (Map<String,Object>)JSON.deserializeUntyped(dataList);
        
        String productName = (String)request.get('productName');
        String articleCode = (String)request.get('articleCode');
        String opportunityId = (String)request.get('oppId');
        Integer OffsetSize = (Integer)request.get('OffsetSize');
        if(multipleCurrencies)
            theOpp = database.query('select Id, Pricebook2Id, Pricebook2.Name, CurrencyIsoCode from Opportunity where Id = \'' + opportunityId + '\' limit 1');
        else
            theOpp = [select Id, Pricebook2Id, PriceBook2.Name from Opportunity where Id = :opportunityId limit 1];
        List<opportunityLineItem> selectedProducts = [select Id,PricebookEntryId from opportunityLineItem where OpportunityId=:opportunityId];
        if(selectedProducts!=null && selectedProducts.size()>0){
            for(opportunityLineItem d:selectedProducts){
                selectedEntries.add(d.PricebookEntryId);
            }
        }
        Pricebook2[] activepbs = [select Id, Name from Pricebook2 where Name like '%Canada All Products%' limit 1];
        if(activepbs.size() >0){
            theBook = activepbs[0];
        }
        
        String countSoqlQuery = 'select COUNT() from PricebookEntry';
        String soqlQuery = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.IsActive, Product2.Description,Product2.ProductCode, UnitPrice,Product2.Base_Unit__c,Product2.Sales_Unit__c,Product2.Article_code__c,Product2.MVGR2_Text__c,Product2.Target_Market__r.Name,Product2.Application_Field_Text__c from PricebookEntry';
        
        //preConditions.add(' Product2.MTART__c IN :materialTypesAllowed');
        //preConditions.add(' Product2.CA_Product__c = true');
        //preConditions.add(' Product2.Sales_Unit__c !=\''+emptyString+'\'');
        
        preConditions.add(' Pricebook2Id = \'' + theBook.Id + '\'');
        preConditions.add(' IsActive=true');
        preConditions.add(' UnitPrice > 0');
        preConditions.add(' Product2.Article_code__c !=\''+emptyString+'\'');
        
        if(multipleCurrencies){
            dynamicConditions.add(' CurrencyIsoCode = \'' + theOpp.get('currencyIsoCode') + '\'');
        }
        
        if(String.isNotBlank(productName)){
            
            String productNameSearchString = '%'+String.escapeSingleQuotes(productName.trim())+'%';
            dynamicConditions.add(' Product2.Name Like :productNameSearchString');// or Product2.Description Like :productNameSearchString
        }
        
        if(String.isNotBlank(articleCode)){
            
            String productArtCodeSearchString = '%'+String.escapeSingleQuotes(articleCode.trim())+'%';
            System.debug('productArtCodeSearchString--'+productArtCodeSearchString);
            dynamicConditions.add(' Product2.Article_code__c Like :productArtCodeSearchString');
        }
        
        if(selectedEntries.size()>0){
            String tempFilter = ' Id not in (';
            for(Id i : selectedEntries){
                tempFilter+= '\'' + (String)i + '\',';
            }
            String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
            extraFilter+= ')';
            dynamicConditions.add(extraFilter);
        }
        
        if(preConditions !=null && preConditions.size() > 0) {
           soqlQuery += '  WHERE ' + preConditions[0];
           countSoqlQuery += '  WHERE ' + preConditions[0];
           for(Integer i = 1; i < preConditions.size(); i++){
              soqlQuery += '  AND ' + preConditions[i];
              countSoqlQuery += '  AND ' + preConditions[i];
           }
        }
        
        if(dynamicConditions !=null && dynamicConditions.size() > 0) {
           for(Integer i = 0; i < dynamicConditions.size(); i++){
              soqlQuery += '  AND ' + dynamicConditions[i];
              countSoqlQuery += '  AND ' + dynamicConditions[i];
           }
        }
		System.debug('LimitSize--'+LimitSize);
        System.debug('OffsetSize--'+OffsetSize);
        soqlQuery+= ' order by Product2.Name LIMIT :LimitSize OFFSET :OffsetSize';
        System.debug('soqlQuery--'+soqlQuery);
        System.debug('countSoqlQuery--'+countSoqlQuery);
        Integer totalRecs = Database.countquery(countSoqlQuery);
        try{
            availableProducts = Database.query(soqlQuery);
            System.debug('availableProducts--'+availableProducts);
            dataToBeSentBack.put('availableProducts',availableProducts);
        	dataToBeSentBack.put('status',true);
        	dataToBeSentBack.put('totalRecs',totalRecs);
        }catch(Exception e){
            System.debug('exception--'+e.getStackTraceString()+'---'+e.getCause()+'--'+e.getTypeName()+'--'+e.getMessage());
            dataToBeSentBack.put('status',false);
        }        
        
        
        return JSON.serializePretty(dataToBeSentBack); 
	}
    @RemoteAction
	global static String saveProductToLineItem(String dataList){
		Map<String,Object> request = (Map<String,Object>)JSON.deserializeUntyped(dataList);
		String product2Id = (String)request.get('product2Id');
        String oppId = (String)request.get('oppId');
        String pricebookEntryId = (String)request.get('pricebookEntryId');
        PricebookEntry priceBookEntryObj = [Select Id,UnitPrice from priceBookEntry where Id=:pricebookEntryId LIMIT 1];
		OpportunityLineItem newOpportunityLineItem = new opportunityLineItem(OpportunityId=oppId, PriceBookEntry=priceBookEntryObj, PriceBookEntryId=priceBookEntryObj.Id, UnitPrice=priceBookEntryObj.UnitPrice,Quantity=1);
		Map<String,Object> dataToBeSentBack = new Map<String,Object>(); 
        try{
            upsert(newOpportunityLineItem);
            dataToBeSentBack.put('status',true);
        }
        catch(Exception e){
             dataToBeSentBack.put('status',false);
        } 
        
	    return JSON.serializePretty(dataToBeSentBack); 
	}
    @RemoteAction
	global static String removeProductFromLineItem(String dataList){
		Map<String,Object> request = (Map<String,Object>)JSON.deserializeUntyped(dataList);
        
		String product2Id = (String)request.get('product2Id');
        String oppId = (String)request.get('oppId');
        String pricebookEntryId = (String)request.get('pricebookEntryId');
        
        Map<String,Object> dataToBeSentBack = new Map<String,Object>(); 
        
        PricebookEntry priceBookEntryObj = [Select Id from priceBookEntry where Id=:pricebookEntryId LIMIT 1];
		List<OpportunityLineItem> productsToBeRemoved = [select Id, Quantity, PriceBookEntryId, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name,PriceBookEntry.Product2.ProductCode,PriceBookEntry.Product2.Sales_Unit__c,PriceBookEntry.Product2.Article_code__c from opportunityLineItem where OpportunityId=:oppId and PriceBookEntryId =:pricebookEntryId and product2Id=:product2Id];
        try{
            if(productsToBeRemoved.size()>0)
            	delete productsToBeRemoved;
            dataToBeSentBack.put('status',true);
        }catch(Exception e){
            dataToBeSentBack.put('status',false);
            System.debug('exception caught : '+e.getCause());
        }
	    return JSON.serializePretty(dataToBeSentBack); 
	}
    @RemoteAction
	global static String upsertOpportunityLineItems(String jsonInput){
         Map<String,Object> dataToBeSentBack = new Map<String,Object>(); 
		Map<string,Object> responseData= new Map<string,Object>();
        List<Object> dataList =  (List<Object>)JSON.deserializeUntyped(jsonInput);
        List<OpportunityLineItem> opportunityLineItemsToUpdate = new List<OpportunityLineItem>();
        Map<String,String> oppLineItemsMap = new Map<String,String>();
        for(Object ob:dataList){
            Map<String,Object> fieldObject = (Map<String,Object>)ob;
			oppLineItemsMap.put((Id)fieldObject.get('oppLineItemId'),(String)fieldObject.get('quantity'));
               
		}
		List<OpportunityLineItem> listOopportunityLineItems = [Select Id,PriceBookEntryId,quantity,UnitPrice,TotalPrice from OpportunityLineItem where Id IN :oppLineItemsMap.keySet()];
        for(OpportunityLineItem opplineItem:listOopportunityLineItems){
            //for(String oppLineItemIdKey:oppLineItemsMap.keySet()){
            //    if(opplineItem.Id==oppLineItemIdKey && opplineItem.Quantity!=Decimal.valueOf(oppLineItemsMap.get(oppLineItemIdKey))){
                    opplineItem.Quantity = Integer.valueOf(oppLineItemsMap.get(opplineItem.Id));
            		opplineItem.UnitPrice = opplineItem.UnitPrice;
            		opplineItem.TotalPrice = Integer.valueOf(oppLineItemsMap.get(opplineItem.Id))*opplineItem.UnitPrice;
                    opportunityLineItemsToUpdate.add(opplineItem);
            //    }
            //}  
        }
        try{
            if(opportunityLineItemsToUpdate.size()>0)
            	update opportunityLineItemsToUpdate;
            dataToBeSentBack.put('status',true);
        }catch(Exception e){
            dataToBeSentBack.put('status',false);
            System.debug('exception caught : '+e.getCause());
        }
	    return JSON.serializePretty(dataToBeSentBack); 
	}
    @RemoteAction
	global static String getAccounts(String dataList){
		Map<String,Object> dataToBeSentBack = new Map<String,Object>();
        List<Account> availableAccounts = new List<Account>();
        List<string> preConditions = new List<String>();
        List<string> dynamicConditions = new List<String>();
        Integer LimitSize= 10;

        Map<String,Object> request = (Map<String,Object>)JSON.deserializeUntyped(dataList);
        
        String accountName = (String)request.get('accountName');
        String oppId = (String)request.get('oppId');
        Integer OffsetSize = (Integer)request.get('OffsetSize');
        String excludeAccountId = '';
		Opportunity opportunityObj = [Select Id,AccountId from Opportunity where Id=:oppId]; 
        if(opportunityObj!=null)
            excludeAccountId = opportunityObj.AccountId;
        String countSoqlQuery = 'select COUNT() from Account';
        String soqlQuery = 'select Id,Name,SAP_Account_ID__c,BillingStreet, BillingCity, BillingState, BillingPostalCode,BillingCountry from Account';
        List<String> recordTypes = new List<String>();
        recordTypes.add('SoldTo_Account');recordTypes.add('One_Time_Customer');recordTypes.add('Affiliate_SoldTo');
        String countryCACode = 'CA';
        String poundSign = '#%';
		preConditions.add(' (RecordType.DeveloperName IN:recordTypes)');
        preConditions.add(' Country_Iso_Code__c=:countryCACode');
		preConditions.add(' (NOT Name like :poundSign)');//(NOT Account__r.Name  like 'Test%')
        if(String.isNotBlank(accountName)){
            String accountNameSearchString = '%'+String.escapeSingleQuotes(accountName)+'%';
            dynamicConditions.add(' Name Like :accountNameSearchString');
        }        
        if(String.isNotBlank(excludeAccountId)){
            dynamicConditions.add(' Id !=\''+excludeAccountId+'\'');
        }
        if(preConditions.size() > 0) {
           soqlQuery += ' WHERE ' + preConditions[0];
           countSoqlQuery += ' WHERE ' + preConditions[0];
           for(Integer i = 1; i < preConditions.size(); i++){
              soqlQuery += ' AND ' + preConditions[i];
              countSoqlQuery += ' AND ' + preConditions[i];
           }
        }
        if(dynamicConditions.size() > 0) {
           for(Integer i = 0; i < dynamicConditions.size(); i++){
              soqlQuery += ' AND ' + dynamicConditions[i];
              countSoqlQuery += ' AND ' + dynamicConditions[i];
           }
        }
        
        soqlQuery+= ' order by Name LIMIT :LimitSize OFFSET :OffsetSize';
        Integer totalRecs = Database.countquery(countSoqlQuery);
        try{
            availableAccounts = Database.query(soqlQuery);
            dataToBeSentBack.put('status',true);
            dataToBeSentBack.put('availableAccounts',availableAccounts);
        	dataToBeSentBack.put('totalRecs',totalRecs);
        }catch(Exception e){
            System.debug('exception--'+e.getStackTraceString()+'---'+e.getCause()+'--'+e.getTypeName()+'--'+e.getMessage());
            dataToBeSentBack.put('status',false);
        }  
        
        return JSON.serializePretty(dataToBeSentBack); 
	}
    @RemoteAction
	global static String updateAccountToOpportunity(String dataList){
		Map<String,Object> request = (Map<String,Object>)JSON.deserializeUntyped(dataList);
		String accountId = (String)request.get('accountId');
        String oppId = (String)request.get('oppId');
        Account accountObj = [Select Id,Name from Account where Id=:accountId LIMIT 1];
		Opportunity opportunityObj = [Select Id,AccountId from Opportunity where Id=:oppId LIMIT 1];
        opportunityObj.AccountId = accountObj.Id;
		Map<String,Object> dataToBeSentBack = new Map<String,Object>(); 
        try{
            update opportunityObj;
            dataToBeSentBack.put('status',true);
        }
        catch(Exception e){
            System.debug('exception--'+e.getStackTraceString()+'---'+e.getCause()+'--'+e.getTypeName()+'--'+e.getMessage());
             dataToBeSentBack.put('status',false);
        }        
	    return JSON.serializePretty(dataToBeSentBack); 
	}
    @RemoteAction
	global static String getSelectedAccountDetail(String dataList){
		Map<String,Object> request = (Map<String,Object>)JSON.deserializeUntyped(dataList);
		String oppId = (String)request.get('oppId');
        Opportunity opp = [Select Id,Name,AccountId,Pickup__c,Use_Customer_Account_Address__c,Street_Address__c,City__c,State_Province__c,Country__c,Postal_Code__c from Opportunity where Id=:oppId LIMIT 1];
        Account accountObj = null;
        if(String.isNotBlank(opp.AccountId)){
			accountObj = [Select Id,Name,SAP_Account_ID__c,BillingStreet, BillingCity, BillingState, BillingPostalCode,BillingCountry from Account where Id=:opp.AccountId LIMIT 1];
            
        }
		Map<String,Object> dataToBeSentBack = new Map<String,Object>(); 
		
        try{
            if(accountObj!=null){
            	dataToBeSentBack.put('selectedAccounts',accountObj);
                dataToBeSentBack.put('deliveryAddressDetail',opp);
        		dataToBeSentBack.put('status',true);
            }else{
        		dataToBeSentBack.put('status',false);
            }
        }
        catch(Exception e){
            System.debug('exception--'+e.getStackTraceString()+'---'+e.getCause()+'--'+e.getTypeName()+'--'+e.getMessage());
             dataToBeSentBack.put('status',false);
        } 
	    return JSON.serializePretty(dataToBeSentBack); 
    }
    @RemoteAction
	global static String updateAccountAddressDetail(String dataList){
        Map<String,Object> dataToBeSentBack = new Map<String,Object>(); 
		Map<String,Object> request = (Map<String,Object>)JSON.deserializeUntyped(dataList);
		
        String oppId = (String)request.get('oppId');
        String pickup = (String)request.get('pickup'); 
        System.debug('pickup is--'+pickup);
        String customerDeliveryAddress = (String)request.get('customerDeliveryAddress');
        String accountId = (String)request.get('accountId');
        String accBillingStreet = (String)request.get('accBillingStreet');
        String accBillingCity = (String)request.get('accBillingCity');
        String accBillingState = (String)request.get('accBillingState');
        String accBillingCountry = (String)request.get('accBillingCountry');
        String accBillingPostalCode = (String)request.get('accBillingPostalCode');
 
        Opportunity opp = [Select Id,AccountId,Pickup__c,Use_Customer_Account_Address__c,Street_Address__c,City__c,State_Province__c,Postal_Code__c,Country__c from Opportunity where Id=:oppId LIMIT 1];
        //if(pickup.equalsIgnoreCase('Freight Collect') || pickup.equalsIgnoreCase('Prepaid')){
                if('No'.equalsIgnoreCase(customerDeliveryAddress)){
                    try{
                        dataToBeSentBack.put('status',true);
                        opp.Pickup__c = pickup;
                        System.debug('opp.Pickup__c is--'+opp.Pickup__c);
                        opp.Use_Customer_Account_Address__c = customerDeliveryAddress;
                        opp.Street_Address__c = accBillingStreet !='' ? accBillingStreet:'';
                        opp.City__c = accBillingCity!=''?accBillingCity:'';
                        opp.State_Province__c = accBillingState!=''?accBillingState:'';
                        opp.Country__c = accBillingCountry!=''?accBillingCountry:'';
                        opp.Postal_Code__c = accBillingPostalCode!=''?accBillingPostalCode:'';
                        update opp;
                        System.debug('opp is--'+opp);
                        
                    }catch(Exception e){
                        System.debug('exception--'+e.getStackTraceString()+'---'+e.getCause()+'--'+e.getTypeName()+'--'+e.getMessage());
                         dataToBeSentBack.put('status',false);
                    }    
                }else{
                    try{
                        opp.Pickup__c = pickup;
                        opp.Use_Customer_Account_Address__c = customerDeliveryAddress;
                        Account accountObj = null;
                        System.debug('opp.AccountId is::'+opp.AccountId);
                        if(String.isNotBlank(opp.AccountId)){
                            accountObj = [Select Id,Name,SAP_Account_ID__c,BillingStreet, BillingCity, BillingState, BillingPostalCode,BillingCountry from Account where Id=:opp.AccountId LIMIT 1];
                            System.debug('accountObj is::'+accountObj);
                            opp.Street_Address__c = accountObj.BillingStreet !='' ? accountObj.BillingStreet:'';
                            opp.City__c = accountObj.BillingCity !=''?accountObj.BillingCity:'';
                            opp.State_Province__c = accountObj.BillingState !=''?accountObj.BillingState:'';
                            opp.Country__c = accountObj.BillingCountry !=''?accountObj.BillingCountry:'';
                            opp.Postal_Code__c = accountObj.BillingPostalCode !=''?accountObj.BillingPostalCode:'';
                        }
                        System.debug('opp is::'+opp);
                        update opp;
                        System.debug('opp after is::'+opp);
                        dataToBeSentBack.put('status',true);
                    }catch(Exception e){
                        System.debug('exception--'+e.getStackTraceString()+'---'+e.getCause()+'--'+e.getTypeName()+'--'+e.getMessage());
                        dataToBeSentBack.put('status',false);
                    } 
                }
        //}
	    return JSON.serializePretty(dataToBeSentBack); 
    }
}