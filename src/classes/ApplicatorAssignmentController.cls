/*
* Author: Martin Kona
* Company: Bluewolf
* Date: May 4, 2016
* Description:
*
*/

public with sharing class ApplicatorAssignmentController {
    // aB9290000004EHvCAM
    // Apttus Proposal Quote
    public Id apqId { get; set;}
    public Id oppAplicatorId {get;set;}

    public static final String AT_LEAST_ONE_PAIR = 'Please specify at least one pair of Account and Contact.';
    public static final String ACCOUNT_CONTACT_PAIR = 'Please assign to every Account one Contact.';
    public static final String REQUIRED_STRING = 'You must enter a value';

    //public String updateContactsByAccountId { get; set;}

    // increment counter when user asks for another row
    private Integer counter = 0;

    public Id apqOpportunity {
        get {
            if (apqOpportunity == null) {
                apqOpportunity = [SELECT Apttus_Proposal__Opportunity__c
                                  FROM Apttus_Proposal__Proposal__c
                                  WHERE Id = :apqId
                                  LIMIT 1].Apttus_Proposal__Opportunity__c;
            }
            return apqOpportunity;
        }
        set;
    }

    public Id apqProject {
        get {
            if (apqProject == null) {
                apqProject = [SELECT Project__c
                              FROM Opportunity
                              WHERE Id = :apqOpportunity
                              LIMIT 1].Project__c;
            }
            return apqProject;
        }
        set;
    }

    public List<JunctionDataWrapper> junctionDataWrappers {
        get {
            if (junctionDataWrappers == null) {
                junctionDataWrappers =
                    new List<JunctionDataWrapper>{new JunctionDataWrapper(this.apqId)};
            }
            return junctionDataWrappers;
        }
        set;
    }

    public ApplicatorAssignmentController() {
        //get Apttus Proposal Proposal Id from URL
        apqId = ApexPages.currentPage().getParameters().get('id');//apqId = 'aB9290000004EHvCAM';

        for(Apttus_Proposal__Proposal__c app : [SELECT Apttus_Proposal__Opportunity__r.AccountId FROM Apttus_Proposal__Proposal__c WHERE Id = :apqId LIMIT 1]){
          oppAplicatorId  = app.Apttus_Proposal__Opportunity__r.AccountId;
        }
    }

    public ApplicatorAssignmentController(ApexPages.StandardController stdController) {
        apqId = stdController.getId();//apqId = 'aB9290000004EHvCAM';

        for(Apttus_Proposal__Proposal__c app : [SELECT Apttus_Proposal__Opportunity__r.AccountId FROM Apttus_Proposal__Proposal__c WHERE Id = :apqId LIMIT 1]){
          oppAplicatorId  = app.Apttus_Proposal__Opportunity__r.AccountId;
        }
    }

    // create junctions with Apttus_Proposal_Proposal__c (Apttus Quote)
    public PageReference generate() {

        if (junctionDataWrappers.size() == 0) {
            ApexPages.addMessage(
                    new ApexPages.Message(ApexPages.severity.WARNING, AT_LEAST_ONE_PAIR));
            return null;
        }

        //List<sObject> objs = new List<sObject>();
        List<OpportunityShare> oppsShr                  = new List<OpportunityShare>();
        List<Apttus_Proposal__Proposal__Share> quoteShr = new List<Apttus_Proposal__Proposal__Share>();
        List<Applicator_Quote__c> aqs                   = new List<Applicator_Quote__c>();

        // create for every Account-Contact pair one Applicator_Quote__c
        for (JunctionDataWrapper jdw : junctionDataWrappers) {
            if (jdw.contactId == null) {
                ApexPages.addMessage(
                        new ApexPages.Message(ApexPages.severity.WARNING, ACCOUNT_CONTACT_PAIR));
                return null;
            }

            Applicator_Quote__c aq  = new Applicator_Quote__c();
            aq.Contact__c           = jdw.contactId;
            aq.Quote_Apttus__c      = apqId;
            aq.Opportunity__c       = apqOpportunity;
            aq.Account__c           = jdw.acAQ.Sold_To_Account__c;

            //prevents adding sharing to opportunity if applicator is already awarded
            if(oppAplicatorId == null){
              //add sharing to opportunity
              oppsShr.addAll(createOpportunitySharingRecord(jdw.acAQ.Sold_To_Account__c));
            }

            //add sharing to quote
            quoteShr.addAll(createQuoteSharingRecord(jdw.acAQ.Sold_To_Account__c, apqId));

            aqs.add(aq);
        }

        // insert Apttus_Proposal_Proposal__c junctions
        insert aqs;
        // insert sharing records
        insertOpportunitySharingRecords(oppsShr);
        insertQuoteSharingRecords(quoteShr);

        List<Con_AQ__c> conAqList = new List<Con_AQ__c>();
        List<Acc_AQ__c> accAqList = new List<Acc_AQ__c>();
        List<Opp_AQ__c> oppAqList = new List<Opp_AQ__c>();
        List<Pro_AQ__c> proAqList = new List<Pro_AQ__c>();

        // create for every Applicator Quote 4 junction objects: Con_AQ__c, Acc_AQ__c, Opp_AQ__c, Pro_AQ__c
        for (Applicator_Quote__c aq : aqs) {

            // create junction with Contact Con_AQ__c
              Con_AQ__c conaq = new Con_AQ__c(AQ__c = aq.Id, Contact__c = aq.contact__c);
            conAqList.add(conaq);
            //objs.add(conaq);

            // create junction with Account Acc_AQ__c
            Acc_AQ__c accaq = new Acc_AQ__c(AQ__c = aq.Id, Sold_To_Account__c = aq.Account__c);
            accAqList.add(accaq);
            //objs.add(accaq);

            // create junction with Opportunity object Opp_AQ__c
            Opp_AQ__c oppaq = new Opp_AQ__c (AQ__c = aq.Id, Opportunity__c = apqOpportunity);
            oppAqList.add(oppaq);
            //objs.add(oppaq);

            // create junction with Project object Pro_AQ__c
            Pro_AQ__c proaq = new Pro_AQ__c (AQ__c = aq.Id, Project__c = apqProject);
            proAqList.add(proaq);
            //objs.add(proaq);
        }

        insert conAqList;
        insert accAqList;
        insert oppAqList;
        insert proAqList;

        // redirect to?
        return new PageReference('/' + apqId);
    }

    private List<Apttus_Proposal__Proposal__Share> createQuoteSharingRecord(Id accountId, Id quoteId) {

        List<Apttus_Proposal__Proposal__Share> appShareList = new List<Apttus_Proposal__Proposal__Share>();

        for (User usr : [SELECT Id FROM User WHERE accountId = :accountId]) {
            Apttus_Proposal__Proposal__Share appShare = new Apttus_Proposal__Proposal__Share();
            appShare.ParentId       = quoteId;
            appShare.UserOrGroupId  = usr.Id;
            appShare.AccessLevel    = 'Edit';
            appShare.RowCause       = Schema.Apttus_Proposal__Proposal__Share.RowCause.Manual;

            appShareList.add(appShare);
        }

        return appShareList;
    }

    private List<OpportunityShare> createOpportunitySharingRecord(Id accountId) {

        List<OpportunityShare> oppsShr = new List<OpportunityShare>();
        for (User usr : [SELECT Id FROM User WHERE accountId = :accountId]) {
          OpportunityShare oppShr  = new OpportunityShare();
          // Set the ID of record being shared.
          oppShr.OpportunityId = this.apqOpportunity;

          // Set the ID of contact community user
          oppShr.UserOrGroupId = usr.Id;

          // Set the access level.
          oppShr.OpportunityAccessLevel = 'Edit';

          // Set rowCause to 'manual' for manual sharing.
          oppShr.RowCause = Schema.OpportunityShare.RowCause.Manual;

          oppsShr.add(oppShr);
        }

        return oppsShr;
    }

    private void insertOpportunitySharingRecords(List<OpportunityShare> rec) {

      List<Database.SaveResult> srs = Database.insert(rec,false);

        for (Database.SaveResult sr : srs) {
              // Process the save results.
              if(sr.isSuccess()){
                 // Indicates success
              }
              else {
                 // Get first save result error.
                 Database.Error err = sr.getErrors()[0];

                 // Check if the error is related to trival access level.
                 // Access levels equal or more permissive than the object's default
                 // access level are not allowed.
                 // These sharing records are not required and thus an insert exception is acceptable.
                 if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&
                          err.getMessage().contains('AccessLevel')){
                    // meh.
                 }
                 else{
                ApexPages.addMessage(
                    new ApexPages.Message(ApexPages.severity.ERROR, err.getMessage()));
                 }
               }
        }
    }

    private void insertQuoteSharingRecords(List<Apttus_Proposal__Proposal__Share> rec) {

      List<Database.SaveResult> srs = Database.insert(rec,false);

        for (Database.SaveResult sr : srs) {
              // Process the save results.
              if(sr.isSuccess()){
                 // Indicates success
              }
              else {
                 // Get first save result error.
                 Database.Error err = sr.getErrors()[0];

                 // Check if the error is related to trival access level.
                 // Access levels equal or more permissive than the object's default
                 // access level are not allowed.
                 // These sharing records are not required and thus an insert exception is acceptable.
                 if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&
                          err.getMessage().contains('AccessLevel')){
                    // meh.
                 }
                 else{
                ApexPages.addMessage(
                    new ApexPages.Message(ApexPages.severity.ERROR, err.getMessage()));
                 }
               }
        }
    }

    public PageReference cancel() {
        return new PageReference('/' + apqId);
    }

    public void addRow() {
        if (junctionDataWrappers.size() > 0 && junctionDataWrappers.get(junctionDataWrappers.size() - 1).acAQ.Sold_To_Account__c == null) {
            junctionDataWrappers.get(junctionDataWrappers.size() - 1).acAQ.Sold_To_Account__c.addError(REQUIRED_STRING);
            return;
        }
        junctionDataWrappers.add(new JunctionDataWrapper(this.apqId));
    }

    public void deleteRow() {
        Integer jdwIndex =
                Integer.valueOf(ApexPages.currentPage().getParameters().get('jdwIndex').trim());

        junctionDataWrappers.remove(jdwIndex);
    }

    public void updateContactSelectList() {

        Integer jdwIndex =
                Integer.valueOf(ApexPages.currentPage().getParameters().get('jdwIndex').trim());
        if (junctionDataWrappers.get(jdwIndex).acAQ.Sold_To_Account__c == null) {
            junctionDataWrappers.get(jdwIndex).acAQ.Sold_To_Account__c.addError(REQUIRED_STRING);
            return;
        }
        String accountId =
                junctionDataWrappers[jdwIndex].acAq.Sold_To_Account__c;

        List<SelectOption> options = new List<SelectOption>();
        List<Contact> accContacts = [SELECT Id, Name FROM Contact WHERE accountID = :accountId AND NLWC__c = false AND Email != null ORDER BY Name];

        for (Contact cont : accContacts) {
            options.add(new SelectOption(cont.Id, cont.Name));
        }

        this.junctionDataWrappers[jdwIndex].contactOptions = options;
    }

    public class JunctionDataWrapper {
        // use object insted of Account so we can use input field (lookup with popup)
        public Acc_AQ__c  acAQ { get; set;}

        // selected contact from select list
        public String contactId { get; set;}

        // contacts select list
        public List<SelectOption> contactOptions{
            get {
                if (contactOptions == null) {
                    contactOptions = new List<SelectOption>();
                }
                return contactOptions;
            }
            set;
        }

        public JunctionDataWrapper(Id apttProposalQuoteId) {
            this.acAQ = new Acc_AQ__c(AQ__c = apttProposalQuoteId);
        }
    }

}