@isTest
private class sika_SCCZoneNewMixPageTest {
	private static sika_SCCZoneMixesHomePageController ctrl;
	private static PageReference pageRef;
	private static User SCCZoneUser;
	private static Mix__c theMix;
	private static String SOM;  // system-of-measure

	private static Integer i;
	
	private static void init(){
		// bypass the triggers we won't need to have firing during setup
		sika_SCCZoneMixTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMixTriggerHelper.bypassMixOwnerTransferCheck = true;
		sika_SCCZoneMixTriggerHelper.bypassMixChatterAlert = true;
		sika_SCCZoneMixTriggerHelper.bypassUpdateSCMQuantity = true;
		
		// create an SCCZone user to set as the owner of this mix
		map<String, sObject> objects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
        SCCZoneUser = (User) objects.get('SCCZoneUser');
   
	}
	private static void initNewMix(){
		init();
		// set page we'll be testing
        pageRef = new PageReference('/sika_SCCZoneMixDetailsPage');
        pageRef.getParameters().put('metric', SOM == null ? 'Metric (SI)' : SOM);
        Test.setCurrentPage(pageRef);
	
		// create an instance of the controller class
		ctrl = new sika_SCCZoneMixesHomePageController();
	}
	private static void initEditMix(){
		init();
		system.runAs(SCCZoneUser){
			theMix = sika_SCCZoneTestFactory.createMix();
        	insert(theMix);
		}
		// set page we'll be testing
        pageRef = new PageReference('/sika_SCCZoneMixDetailsPage');
        pageRef.getParameters().put('mixID', theMix.Id);
        Test.setCurrentPage(pageRef);
		
		// create an instance of the controller class
		ctrl = new sika_SCCZoneMixesHomePageController();
	}
	
	
	@isTest static void testNewMixHappy() {
		initEditMix();
		system.runAs(SCCZoneUser){
			system.assert(ctrl.saveNewMix2().getURL().contains('sika_SCCZoneMixDetailsPage'));
			system.assertEquals(ctrl.saveNewMix2().getParameters().get('mixID'), theMix.Id);
		}
		
	}
	
	@isTest static void testNewMixSad(){
		initEditMix();
		system.runAs(SCCZoneUser){
			ctrl.newMix.Mix_Name__c = null;
			ctrl.newMix.Max_Agg_Diameter__c = null;
			ctrl.newMix.Reinforcement__c = null;
			ctrl.newMix.Agg_Efficiency_Factor__c = null;
			ctrl.newMix.Cement_Comp_Strength__c = null;
			ctrl.newMix.Optimize_for__c = null;
			ctrl.newMix.Target_SCM_Ratio__c = null;
			ctrl.newMix.Target_Air_Percent__c = null;
			ctrl.newMix.Target_Water_Cement_Ratio__c = null;
			ctrl.newMix.Concrete_Target_Compressive_Strength__c = null;
			ctrl.newMix.Target_Paste_Volume_PKLST__c = null;
			
			system.assertEquals(ctrl.saveNewMix2(), null);
			
			ctrl = new sika_SCCZoneMixesHomePageController();
			
			ctrl.newMix.System_of_Measure__c = 'Customary (US)';
			ctrl.newMix.Cement_Comp_Strength__c = null;
			ctrl.newMix.Concrete_Target_Compressive_Strength__c = 10001;
			
			system.assertEquals(ctrl.saveNewMix2(), null);
		}
	}

}