global with sharing class sika_PTSLoginPageController {
	
	

	global String username {get; set;}
    global String password {get; set;}

    global sika_PTSLoginPageController() { }
    
    // if the login page is accessed by an already-authenticated user, redirect the user to the userHomePage.
    /*public Pagereference loadAction(){
    	sika_PTSCheckAuth.setAuthStatus(UserInfo.getProfileId());
		PageReference isAuthenticated = sika_PTSCheckAuth.doAuthCheck();
		PageReference pageRef;
		
		if(isAuthenticated == null){
			pageRef = new PageReference('/PTS/sika_PTSUserHomePage');
	        pageRef.setRedirect(true);
	        return pageRef; 
		}
		return null;
	}*/

    global PageReference login() {

        return Site.login(username, password, '/home/home.jsp');

    }  
}