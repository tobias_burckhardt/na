public with sharing class PickApproversCtlr {
    private final Integer NUM_OF_APPROVERS = 8;

    @TestVisible private final String REMOVE_MSG = 'Removal will not take effect until page is saved';
    @TestVisible private final String REORDER_MSG = 'Re-order will not take effect until page is saved';
    @TestVisible private final String DATA_ERR_MSG = 'There were blank approvers or apporver numbers found. Please remove blank rows';
    @TestVisible private final String DUPL_NAME_ERR_MSG = 'There were duplicate approvers found. Please remove duplicates';
    @TestVisible private final String DUPL_NUM_ERR_MSG = 'There were duplicate or invalid approver numbers found. Please remove errors';
    private final Map<PickApproversModel.ErrorCode, String> codeToMessage = 
        new Map<PickApproversModel.ErrorCode, String>{
            PickApproversModel.ErrorCode.DUPLICATE_NAME_ERROR => DUPL_NAME_ERR_MSG,
            PickApproversModel.ErrorCode.DUPLICATE_NUM_ERROR => DUPL_NUM_ERR_MSG,
            PickApproversModel.ErrorCode.DATA_ERROR => DATA_ERR_MSG
        };


	private final Request_for_Project_Approval__c mysObject;
    @TestVisible private List<String> errorMessages;

    public PickApproversModel paModel {get; set;}
    public Integer currentNum { 
        get {
            return paModel.approvers.size();
        } set;
    }

    public PickApproversCtlr(ApexPages.StandardController stdController) {
        System.debug('CONSTRUCTOR!!!');
        this.mysObject = (Request_for_Project_Approval__c) stdController.getRecord();
        Map<Integer, User> existingApprovers = new Map<Integer, User>();
        for (Integer i=0; i<NUM_OF_APPROVERS; i++) {
            Integer approverNumber = i+1;
            Id approverId = (Id) this.mysObject.get('Approver_'+approverNumber+'__c');
            if (approverId != null) {
                existingApprovers.put(i, new User(Id=approverId));
            }
        }
        paModel = new PickApproversModel(existingApprovers);
    }

    public void moveUp() {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, REORDER_MSG));
        Integer moveNumber =  
            Integer.valueof(
                ApexPages.currentPage().getParameters().get('moveNumber').trim()
            );
        moveUp(moveNumber);
    }

    private void moveUp(Integer pos) {
        paModel.tradePositions(pos, pos-1);
    }

    public void moveDown() {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, REORDER_MSG));
        Integer moveNumber =  
            Integer.valueof(
                ApexPages.currentPage().getParameters().get('moveNumber').trim()
            );
        moveDown(moveNumber);        
    }

    private void moveDown(Integer pos) {
        paModel.tradePositions(pos, pos+1);
    }

    public PageReference updateState() {
        paModel.refresh();
        return null;
    }

    public void add() {
        paModel.addAndGetNextUp(null);
    }

    public void removeRow() {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, REMOVE_MSG));
        Integer removalNumber =  
            Integer.valueof(
                ApexPages.currentPage().getParameters().get('removalNumber').trim()
            );
        printApprovals();
        remove(removalNumber);
        printApprovals();
    }

    private void printApprovals() {
        for (PickApproversModel.ApproverWrapper approver : paModel.approvers) {
            System.debug('('+approver.placeholderRequest.Approver_1__c+', '+approver.pos+')');
        }
    }

    @TestVisible private void remove(Integer posToRemove) {
        paModel.remove(posToRemove);
    }

    public PageReference savePage() {
        printApprovals();
        saveAndHandleDataError(paModel.getErrors());
        return displayMessages();
    }

    private void addMessage(String msg) {
        if (errorMessages == null) {
            errorMessages = new List<String>();
        } 
        errorMessages.add(msg);
    }

    @TestVisible private PageReference displayMessages() {
        if (errorMessages != null) {
            for (String msg : errorMessages) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
            }
            errorMessages = null;
            return null;
        } else {
            return new ApexPages.StandardController(mysObject).view();
        }
    }

    @TestVisible private void saveAndHandleDataError(Set<PickApproversModel.ErrorCode> errorCodes) { //@TODO change name
        if (errorCodes.isEmpty()) {
            saveAndHandleDMLError(paModel.outputApproversAsMap());
        } else {
            for (PickApproversModel.ErrorCode errCode : errorCodes) {
                addMessage(codeToMessage.get(errCode));
            }
        }
    }

    @TestVisible private void saveAndHandleDMLError(Map<Integer, Id> currentState) {
        try {
            save(currentState);
        }
        catch (DmlException ex) {
            addMessage(ex.getMessage());
        }
    }

    @TestVisible private void save(Map<Integer, Id> currentState) {
        for (Integer i=0; i<NUM_OF_APPROVERS; i++) {
            Integer approverNumber = i+1;
            if (currentState.containsKey(i)) {
                mysObject.put('Approver_'+approverNumber+'__c', currentState.get(i));
            } else {
                mysObject.put('Approver_'+approverNumber+'__c', null);                    
            }
        }
        update mysObject; 
    }
}