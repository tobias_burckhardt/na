@isTest
private class AttachmentRestrictUpDelTriggerTest {

    //Pre-created blobs for initial and modified attachment bodies
    private static final Blob TEST_BODY = Blob.valueOf('Test Body');
    private static final Blob MODIFIED_BODY = Blob.valueOf('Modified body');
    
    @testSetup static void setupCustomSetting() {
        AWS_Settings__c awsSetting = new AWS_Settings__c(AWS_Key__c = 'Test', S3_Bucket_Name__c = 'Test', SF_Library_Name__c = 'Test');
        insert awsSetting;
    }
    //Creates and returns the specified number of attachments for the given parent id
    private static List<Attachment> createTestAttachment(Id parentId, Integer num) {
        List<Attachment> attachments = new List<Attachment>();
        for (Integer i = 0; i < num; i++) {
            Attachment attach = new Attachment(Name='Test Attachment: ' + parentId + ' ' + i, Body=TEST_BODY, ParentId=parentId);
            attachments.add(attach);
        }
        insert attachments;
        return attachments;
    }

    //Creates and returns a task related to the given parent id
    private static Task createTestTask(Id parentId) {
        Task tsk = new Task(Subject='Test Subject', WhatId=parentId, Status='Not Started');
        insert tsk;
        return tsk;
    }

    //Creates and returns an event related to the given parent id
    private static Event createTestEvent(Id parentId) {
        Event ev = new Event(Subject='Test Subject', WhatId=parentId, DurationInMinutes=60, ActivityDateTime=System.now());
        insert ev;
        return ev;
    }
    
    //Creates and returns an email message related to the given case id
    private static EmailMessage createTestEmail(Id caseId) {
        EmailMessage em = new EmailMessage(ParentId=caseId);
        insert em;
        return em;
    }

    @testSetup
    static void setUpTests() {
        RestrictedCaseTestHelper.createCustomSettings();
    }

    @isTest
    static void testUpdateDeleteUnrestrictedOnCase() {
        //Trying to update/delete an attachment attached to a case with an unrestricted record type should succeed
        Case testCase = RestrictedCaseTestHelper.createTestCase(false);
        List<Attachment> testAttach = createTestAttachment(testCase.Id, 1);

        Test.startTest();
        testAttach[0].Body = MODIFIED_BODY;
        Database.SaveResult updateResult = Database.update(testAttach[0], false);
        System.assert(updateResult.isSuccess());

        Database.DeleteResult deleteResult = Database.delete(testAttach[0], false);
        System.assert(deleteResult.isSuccess());
        Test.stopTest();
    }

    @isTest
    static void testUpdateAttachmentOnRestrictedCase() {
        //Trying to update an attachment attached to a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<Attachment> testAttach = createTestAttachment(testCase.Id, 1);

        Test.startTest();
        testAttach[0].Body = MODIFIED_BODY;
        Database.SaveResult updateResult = Database.update(testAttach[0], false);
        System.assert(!updateResult.isSuccess());
        System.assertEquals(1, updateResult.getErrors().size());
        System.assertEquals(System.Label.Restricted_Object_Update, updateResult.getErrors()[0].getMessage());
        Test.stopTest();
    }

    @isTest
    static void testDeleteAttachmentOnRestrictedCase() {
        //Trying to delete an attachment attached to a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<Attachment> testAttach = createTestAttachment(testCase.Id, 1);

        Test.startTest();
        Database.DeleteResult deleteResult = Database.delete(testAttach[0], false);
        System.assert(!deleteResult.isSuccess());
        System.assertEquals(1, deleteResult.getErrors().size());
        System.assertEquals(System.Label.Restricted_Object_Delete, deleteResult.getErrors()[0].getMessage());
        Test.stopTest();
    }

    @isTest
    static void testUpdateAttachmentOnTaskOnRestrictedCase() {
        //Trying to update an attachment attached to a task related to case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        Task testTask = createTestTask(testCase.Id);
        List<Attachment> testAttach = createTestAttachment(testTask.Id, 1);

        Test.startTest();
        testAttach[0].Body = MODIFIED_BODY;
        Database.SaveResult updateResult = Database.update(testAttach[0], false);
        System.assert(!updateResult.isSuccess());
        System.assertEquals(1, updateResult.getErrors().size());
        System.assertEquals(System.Label.Restricted_Object_Update, updateResult.getErrors()[0].getMessage());
        Test.stopTest();
    }

    @isTest
    static void testDeleteAttachmentOnTaskOnRestrictedCase() {
        //Trying to delete an attachment attached to a task related to a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        Task testTask = createTestTask(testCase.Id);
        List<Attachment> testAttach = createTestAttachment(testTask.Id, 1);

        Test.startTest();
        Database.DeleteResult deleteResult = Database.delete(testAttach[0], false);
        System.assert(!deleteResult.isSuccess());
        System.assertEquals(1, deleteResult.getErrors().size());
        System.assertEquals(System.Label.Restricted_Object_Delete, deleteResult.getErrors()[0].getMessage());
        Test.stopTest();
    }

    @isTest
    static void testUpdateAttachmentOnEventOnRestrictedCase() {
        //Trying to update an attachment attached to a event related to case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        Event testEvent = createTestEvent(testCase.Id);
        List<Attachment> testAttach = createTestAttachment(testEvent.Id, 1);

        Test.startTest();
        testAttach[0].Body = MODIFIED_BODY;
        Database.SaveResult updateResult = Database.update(testAttach[0], false);
        System.assert(!updateResult.isSuccess());
        System.assertEquals(1, updateResult.getErrors().size());
        System.assertEquals(System.Label.Restricted_Object_Update, updateResult.getErrors()[0].getMessage());
        Test.stopTest();
    }

    @isTest
    static void testDeleteAttachmentOnEventOnRestrictedCase() {
        //Trying to delete an attachment attached to a event related to a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        Event testEvent = createTestEvent(testCase.Id);
        List<Attachment> testAttach = createTestAttachment(testEvent.Id, 1);

        Test.startTest();
        Database.DeleteResult deleteResult = Database.delete(testAttach[0], false);
        System.assert(!deleteResult.isSuccess());
        System.assertEquals(1, deleteResult.getErrors().size());
        System.assertEquals(System.Label.Restricted_Object_Delete, deleteResult.getErrors()[0].getMessage());
        Test.stopTest();
    }

    @isTest
    static void testDeleteAttachmentOnEmailMessageOnRestrictedCase() {
        //Trying to delete an attachment attached to a event related to a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        EmailMessage testEmail = createTestEmail(testCase.Id);
        List<Attachment> testAttach = createTestAttachment(testEmail.Id, 1);

        Test.startTest();
        Database.DeleteResult deleteResult = Database.delete(testAttach[0], false);
        System.assert(!deleteResult.isSuccess());
        System.assertEquals(1, deleteResult.getErrors().size());
        System.assertEquals(System.Label.Restricted_Object_Delete, deleteResult.getErrors()[0].getMessage());
        Test.stopTest();
    }

    @isTest
    static void testUpdateAttachmentOnUnrelatedEvent() {
        //Trying to update an attachment attached to an unrelated event (no WhatId) should succeed
        Event testEvent = createTestEvent(null);
        List<Attachment> testAttach = createTestAttachment(testEvent.Id, 1);

        Test.startTest();
        testAttach[0].Body = MODIFIED_BODY;
        Database.SaveResult updateResult = Database.update(testAttach[0], false);
        System.assert(updateResult.isSuccess());
        System.assertEquals(0, updateResult.getErrors().size());
        Test.stopTest();
    }

    @isTest
    static void testDeleteAttachmentOnUnrelatedEvent() {
        //Trying to delete an attachment attached to an unrelated event (no WhatId) should succeed
        Event testEvent = createTestEvent(null);
        List<Attachment> testAttach = createTestAttachment(testEvent.Id, 1);

        Test.startTest();
        Database.DeleteResult deleteResult = Database.delete(testAttach[0], false);
        System.assert(deleteResult.isSuccess());
        System.assertEquals(0, deleteResult.getErrors().size());
        Test.stopTest();
    }

    @isTest
    static void testBulkUpdateAttachmentOnRestrictedCase() {
        //Trying to update an attachment attached to a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<Attachment> testAttach = createTestAttachment(testCase.Id, 200);

        Test.startTest();
        for (Attachment attach : testAttach) {
            attach.Body = MODIFIED_BODY;
        }

        List<Database.SaveResult> updateResult = Database.update(testAttach, false);
        for (Database.SaveResult res : updateResult) {
            System.assert(!res.isSuccess());
            System.assertEquals(1, res.getErrors().size());
            System.assertEquals(System.Label.Restricted_Object_Update, res.getErrors()[0].getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void testBulkDeleteAttachmentOnRestrictedCase() {
        //Trying to delete an attachment attached to a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<Attachment> testAttach = createTestAttachment(testCase.Id, 200);

        Test.startTest();
        List<Database.DeleteResult> deleteResult = Database.delete(testAttach, false);
        for (Database.DeleteResult res : deleteResult) {
            System.assert(!res.isSuccess());
            System.assertEquals(1, res.getErrors().size());
            System.assertEquals(System.Label.Restricted_Object_Delete, res.getErrors()[0].getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void testBulkDeleteIndirectAttachmentOnRestrictedCase() {
        //Trying to delete an attachment indirectly attached to a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        Task testTask = createTestTask(testCase.Id);
        Event testEvent = createTestEvent(testCase.Id);
        EmailMessage testEmail = createTestEmail(testCase.Id);

        List<Attachment> testAttach = createTestAttachment(testTask.Id, 100);
        testAttach.addAll(createTestAttachment(testEvent.Id, 100));

        Test.startTest();
        List<Database.DeleteResult> deleteResult = Database.delete(testAttach, false);
        for (Database.DeleteResult res : deleteResult) {
            System.assert(!res.isSuccess());
            System.assertEquals(1, res.getErrors().size());
            System.assertEquals(System.Label.Restricted_Object_Delete, res.getErrors()[0].getMessage());
        }
        Test.stopTest();
    }
}