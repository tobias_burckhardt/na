@isTest
private class InstallationCloneControllerTest {
    static final Integer RECORD_COUNT = 20;
    static Sika_Asset_Installation__c chosenInstallation;

    static void setup() {
        UnitTest.createTestData(TestingUtils.generatorOf(Sika_Asset_Installation__c.SobjectType)).many(2)
            .tag('existingInstallations')
            .branch('chosen|control')
            .insertAll();
        chosenInstallation = (Sika_Asset_Installation__c) UnitTest.get('chosen').getList().get(0);
        UnitTest.createTestData(TestingUtils.generatorOf(Sika_Asset__c.SobjectType)).many(RECORD_COUNT)
                .tag('assets');
        UnitTest.get('assets').insertAll();

        // set up assignments for each asset for the control and chosen installation
        UnitTest.forEach('assets').create(1, TestingUtils.generatorOf(Sika_Assignment__c.SobjectType))
                .copyProperty('Id->Sika_Asset__c')
                .tag('assignments');
        UnitTest.get('assignments')
                .property('Asset_Installation__c').assignFrom('control');
        UnitTest.forEach('assets').create(1, TestingUtils.generatorOf(Sika_Assignment__c.SobjectType))
                .copyProperty('Id->Sika_Asset__c')
                .tag('assignments')
                .tag('assignments to clone');
        UnitTest.get('assignments to clone')
                .property('Asset_Installation__c').assignFrom('chosen');
        UnitTest.get('assignments').insertAll();
    }

    static List<Sika_Asset__c> getAssets(List<Sika_Assignment__c> assignments) {
        List<Sika_Asset__c> assets = new List<Sika_Asset__c>();
        for (Sika_Assignment__c assignment : assignments) {
            assets.add(assignment.Sika_Asset__r);
        }
        return assets;
    }

    static TestMethod void testGetInstallationCloneInformation_records() {

        setup();

        Test.startTest();
            LightningSobjectsAndDescribe cloneInfo =
                InstallationCloneController.getInstallationCloneInformation(chosenInstallation.Id);
        Test.stopTest();

        System.assertEquals(
            chosenInstallation.Id,
            cloneInfo.sobjects.get('install')[0].Id,
            'we expect that the passed in installation is queried and returned');
        System.assertEquals(
            UnitTest.getIds('assignments to clone'),
            pluck.ids(cloneInfo.sobjects.get('assigns')),
            'we expect each assignment that looks up to the installation to be provided');
    }

    static TestMethod void testGetInstallationCloneInformation_describe() {
        setup();

        Test.startTest();
            LightningSobjectsAndDescribe cloneInfo =
                InstallationCloneController.getInstallationCloneInformation(chosenInstallation.Id);
        Test.stopTest();

        Map<String, Set<String>> expectedWritableDescribes = new Map<String, Set<String>>{
            'Sika_Asset__c' => new Set<String>{
                'Asset_Name__c', 'Asset_Type__c', 'Asset_Size__c',
                'Manufacturer__c', 'Purchased_from_Competitor__c',
                'Sika_Owned_Asset__c', 'Status__c',
                'Asset_Description__c', 'ID_Serial__c',
                'Purchase_Date__c'
            },
            'Sika_Asset_Installation__c' =>
                new Set<String>{'Direct_Feed__c', 'Estimated_Total_Equipment_Value__c', 'Location_of_Equipment_on_Site__c', 'Account_Name__c', 'Account__c'},
            'Sika_Assignment__c' => 
                new Set<String>{'Name', 'Sika_Asset__c', 'Product__c'}
        };
        for (String objName : expectedWritableDescribes.keySet()) {
            LightningDescribe.ObjectInfo objDescribe = cloneInfo.describe.objects.get(objName);
            System.assertNotEquals(null, objDescribe,
                'we expect a describe to be created for fields of the object ' + objName);
            for (String fieldname : expectedWritableDescribes.get(objName)) {
                LightningDescribe.FieldInfo fieldDescribe = objDescribe.fields.get(fieldName);
                System.assertNotEquals(null, fieldDescribe,
                    'we expect the field ' + fieldName + ' to have schema information passed back to the server, instead, info for the following fields was passed back:' + objDescribe.fields.keySet());
                System.assertEquals(LightningDescribe.BASELINE_FIELD_WRITES, fieldDescribe.hasProps,
                    'we expect the the field ' + fieldName + ' to have schema information necessary for writes passed back to the server');
            }
        }
    }

    static TestMethod void testCloneInstallation_installation() {

        setup();

        LightningSobjectsAndDescribe cloneInfo =
            InstallationCloneController.getInstallationCloneInformation(chosenInstallation.Id);

        String SOME_NEW_LOCATION = 'Alderaan';
        cloneInfo.sobjects.get('install')[0].put('Location_of_Equipment_on_Site__c', SOME_NEW_LOCATION);

        Test.startTest();
            Id installationId = InstallationCloneController.cloneInstallation(
                (Sika_Asset_Installation__c) cloneInfo.sobjects.get('install')[0],
                cloneInfo.sobjects.get('assigns'),
                getAssets(cloneInfo.sobjects.get('assigns')));
        Test.stopTest();

        List<Sika_Asset_Installation__c> newInstallations = [
            SELECT Location_of_Equipment_on_Site__c
            FROM Sika_Asset_Installation__c
            WHERE Id NOT IN: UnitTest.getIds('existingInstallations')
        ];
        System.assertEquals(1, newInstallations.size(),
            'we expect the installation to be cloned');
        System.assertEquals(newInstallations[0].Id, installationId,
            'we expect the id of the created clone to be returned');
        System.assertEquals(SOME_NEW_LOCATION, newInstallations[0].Location_of_Equipment_on_Site__c,
            'we expect that if a value is provided for specified fields in the installation provided, that value replaces the cloned record\'s value');
    }

    static TestMethod void testCloneInstallation_assets() {

        setup();

        LightningSobjectsAndDescribe cloneInfo =
            InstallationCloneController.getInstallationCloneInformation(chosenInstallation.Id);

        String SOME_NEW_DESCRIPTION = 'asset hound';
        for (Sika_Assignment__c assignment : (List<Sika_Assignment__c>) cloneInfo.sobjects.get('assigns')) {
            assignment.Sika_Asset__r.Asset_Description__c = SOME_NEW_DESCRIPTION;
        }
        UnitTest.addData(cloneInfo.sobjects.get('assigns')).tag('receivedAssignments')
            .branch('provided | not provided');
        List<Sika_Assignment__c> providedAssignments = (List<Sika_Assignment__c>) UnitTest.get('provided').getList();

        Test.startTest();
            Id installationId = InstallationCloneController.cloneInstallation(
                (Sika_Asset_Installation__c) cloneInfo.sobjects.get('install')[0],
                providedAssignments,
                getAssets(providedAssignments));
        Test.stopTest();

        List<Sika_Asset__c> newAssets =
            [ SELECT Asset_Description__c FROM Sika_Asset__c WHERE Id NOT IN : UnitTest.getIds('assets') ];
        System.assertEquals(UnitTest.getIds('provided').size(), newAssets.size(),
            'we expect the assets of the provided assignments to be cloned');
        System.assertEquals(new Set<String>{SOME_NEW_DESCRIPTION}, pluck.strings('Asset_Description__c', newAssets),
            'we expect that if a value is provided for specified fields in the assets of the assignments provided, that value replaces the cloned record\'s value');
    }

    static TestMethod void testCloneInstallation_assignments() {

        setup();

        LightningSobjectsAndDescribe cloneInfo =
            InstallationCloneController.getInstallationCloneInformation(chosenInstallation.Id);

        Id SOME_PRODUCT_ID = SobjectFactory.create(1, Product2.SobjectType)[0].Id;
        for (Sika_Assignment__c assignment : (List<Sika_Assignment__c>) cloneInfo.sobjects.get('assigns')) {
            assignment.Product__c = SOME_PRODUCT_ID;
        }
        UnitTest.addData(cloneInfo.sobjects.get('assigns')).tag('receivedAssignments')
            .branch('provided | not provided');
        List<Sika_Assignment__c> providedAssignments = (List<Sika_Assignment__c>) UnitTest.get('provided').getList();

        Test.startTest();
            Id installationId = InstallationCloneController.cloneInstallation(
                (Sika_Asset_Installation__c) cloneInfo.sobjects.get('install')[0],
                providedAssignments, 
                getAssets(providedAssignments));
        Test.stopTest();

        List<Sika_Assignment__c> newAssignments =
           [ SELECT Asset_Installation__c, Product__c FROM Sika_Assignment__c WHERE Id NOT IN: UnitTest.getIds('assignments') ];
        System.assertEquals(UnitTest.getIds('provided').size(), newAssignments.size(),
            'we expect the assignments provided to be cloned');
        System.assertEquals(new Set<Id>{installationId}, pluck.ids('Asset_Installation__c', newAssignments),
            'we expect that the cloned assignments look up to the installation clone');
        System.assertEquals(new Set<Id>{SOME_PRODUCT_ID}, pluck.ids('Product__c', newAssignments),
            'we expect that if a value is provided for specific fields in the assignments provided, that value replaces the cloned record\'s value');
    }
}