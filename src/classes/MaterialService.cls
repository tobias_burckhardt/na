public class MaterialService {
    public static void updateMixMaterialMoistureAdjustment(List<Material__c> newMaterials,
            Map<Id, Material__c> oldMaterialsById) {
        List<Material__c> filteredMaterials = filterUpdateMoistureAdjustment(newMaterials, oldMaterialsById);

        List<Mix_Material__c> mixMaterials = getRelatedMixMaterials(filteredMaterials);
        Map<Id, Mix__c> mixesById = new Map<Id, Mix__c>(MixMaterialService.getRelatedMixes(mixMaterials));
        Map<Id, Material__c> materialsById = new Map<Id, Material__c>(filteredMaterials);

        List<Mix_Material__c> mixMaterialsToUpdate = new List<Mix_Material__c>();
        for (Mix_Material__c mixMaterial : mixMaterials) {
            Material__c material = materialsById.get(mixMaterial.Material__c);
            Mix__c mix = mixesById.get(mixMaterial.Mix__c);
            if (material != null && mix != null) {
                MixMaterialService.updateMoistureAdjustment(mixMaterial, material, mix);
                mixMaterialsToUpdate.add(mixMaterial);
            }
        }

        updateRelatedMixMaterials(mixMaterialsToUpdate, materialsById);
    }

    private static void updateRelatedMixMaterials(List<Mix_Material__c> mixMaterials, Map<Id, Material__c> materialsById) {
        if (!mixMaterials.isEmpty()) {
            try {
                update mixMaterials;
            } catch(DmlException e) {
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    String errorMessage = 'Unable to update Moisture Adjustment of related Mix Material records: ' +
                        e.getDmlMessage(i);
                    materialsById.get(mixMaterials.get(e.getDmlIndex(i)).Material__c).addError(errorMessage);
                }
            }
        }
    }

    private static List<Material__c> filterUpdateMoistureAdjustment(List<Material__c> newMaterials,
            Map<Id, Material__c> oldMaterialsById) {
        List<Material__c> filteredMaterials = new List<Material__c>();
        for (Material__c newMaterial : newMaterials) {
            Material__c oldMaterial = oldMaterialsById.get(newMaterial.Id);
            if (newMaterial.Type__c != oldMaterial.Type__c || newMaterial.Abs__c != oldMaterial.Abs__c ||
                    newMaterial.Solid_Content__c != oldMaterial.Solid_Content__c) {
                filteredMaterials.add(newMaterial);
            }
        }
        return filteredMaterials;
    }

    private static List<Mix_Material__c> getRelatedMixMaterials(List<Material__c> materials) {
        return [
            SELECT Quanity_kg__c, Moisture__c, Material__c, Mix__c
            FROM Mix_Material__c
            WHERE Material__c IN :materials
        ];
    }
}