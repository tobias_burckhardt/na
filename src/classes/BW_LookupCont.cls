public class BW_LookupCont {

    @TestVisible class QueryFilter {
        public String fieldPath { get; set; }
        public String operator { get; set; }
        public String value { get; set; }
        public List<QueryFilter> subfilters { get; set; }

        public String safeSerialize(DescribeSobjectResult objectDescribe) {
            Set<String> compositional = new Set<String>{'AND', 'OR'};
            Set<String> negation = new Set<String>{'NOT'};
            Set<String> comparative = new Set<String>{'=', '!=', '>', '<', '<>', 'IN', 'LIKE'};
            Set<Schema.DisplayType> serializedFieldTypes = new Set<Schema.DisplayType>{Schema.DisplayType.STRING, Schema.DisplayType.PICKLIST, Schema.DisplayType.EMAIL, Schema.DisplayType.URL};
            
            if (compositional.contains(operator)) {
                List<String> results = new List<String>();
                for (QueryFilter subfilter : subfilters) {
                    results.add(subfilter.safeSerialize(objectDescribe));
                }
                return '(' + String.join(results, ') ' + operator + ' (') + ')';
            } else if (negation.contains(operator)) { 
                return '(NOT (' + subfilters[0].safeSerialize(objectDescribe) +'))';
            } else if (comparative.contains(operator)) {
                if (fieldPath == null) {
                    throw new AuraHandledException('a field path must be provided');
                }
                DescribeFieldResult fieldDescribe = fieldDescribeFromFieldPath(objectDescribe, fieldPath);
           		if (!fieldDescribe.isAccessible()) {
                    throw new AuraHandledException('you attempted to filter using the fieldPath ' + fieldPath + ', which you do not access to');
                }
                if (serializedFieldTypes.contains(fieldDescribe.getType()) && value != null) {
                    value = '\'' + value + '\'';
                }
                return fieldPath + ' ' + operator + ' ' + value;
            } else {
                throw new AuraHandledException(' could not recognize a provided query operator');  
            }
        }
    }

    @AuraEnabled public Static LightningDescribe dfieldsAll(String objectName) {
        return dfieldsSome(objectName,
            Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap().values());
    }
    
    static LightningDescribe dfieldsSome(String objectName, List<SobjectField> sobjFields) {
        return new LightningDescribe().addObject(
                new LightningDescribe.ObjectInfo(objectName)
                    .addFields(new Set<SobjectField>(sobjFields), LightningDescribe.BASELINE_FIELD_READS)
            );
    }

    @AuraEnabled public static List<Sobject> dynamicQuery(List<String> fieldPaths, String objectName, String queryFilterJson) {
        String q;
        try {
            List<String> queryArr = new List<String>{'SELECT'};
            DescribeSobjectResult objectDescribe = Schema.getGlobalDescribe().get(objectName).getDescribe();
            List<String> finalFieldPaths = filterCanReadFieldpath(objectDescribe, fieldPaths);
            queryArr.add(String.join(finalFieldPaths, ','));
            queryArr.add('FROM');
            queryArr.add(objectName);
            if (queryFilterJson != null) {
                QueryFilter qFilt = (QueryFilter) JSON.deserialize(queryFilterJson, QueryFilter.class);
                queryArr.add('WHERE ' + qFilt.safeSerialize(objectDescribe));
            }
            queryArr.add('LIMIT 1000');
            q = String.join(queryArr, ' ');
            return Database.query(String.join(queryArr, ' '));
        } catch (AuraHandledException e) {
            throw e;
        } catch(Exception e) {
            system.debug(q + e.getMessage() + e.getStackTraceString());
        }
        return new List<Sobject>();
    }
    
    static List<String> filterCanReadFieldpath(DescribeSobjectResult objectDescribe, List<String> fieldPaths) {
        List<String> results = new list<String>();
        for (String fieldPath : fieldPaths) {
            if (fieldDescribeFromFieldPath(objectDescribe, fieldPath).isAccessible()) {
            	results.add(fieldPath);
            }
        }
        return results;
    }

    static DescribeFieldResult fieldDescribeFromFieldPath(DescribeSobjectResult objectDescribe, String fieldPath) {
		Map<String, SobjectField> fieldMap = objectDescribe.fields.getMap();
        if (fieldMap.containsKey(fieldPath)) {
         	return fieldMap.get(fieldPath).getDescribe();   
        } else {
            //TODO implement
            return null;
        }
    }
}