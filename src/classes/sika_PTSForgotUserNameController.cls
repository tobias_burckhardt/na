public without sharing class sika_PTSForgotUserNameController {

    public String emailAddress {get; set;}
    public String success {get; set;}
    public Boolean submitted {get; set;}
    public String boxText {get; set;}

    @TestVisible private static String emailNotRecognizedErrorMsg  = 'The email address you entered was not recognized. Please try again.';
    @TestVisible private static String invalidEmailErrorMsg = 'Please enter a valid email address.';
    
    public sika_PTSForgotUserNameController() {
        success = ApexPages.currentPage().getParameters().get('success');

        if(success != null && Boolean.valueOf(success) == true){
            submitted = true;
            boxText = 'Thank you! You should receive an email containing your username shortly.';
        } else {
            submitted = false;
            boxText = 'Please enter the email address associated with your account:';
        }

    }

    public PageReference submitRequest(){

        List<User> theUsers = validateForm();
        if(theUsers != null){
            if(theUsers.size() > 0){

                // email username to user
                String theSubject = 'Your PTS username -- Sika.com';
                String theBody;
                if(theUsers.size() > 1) {
                    theBody = 'Hello PTS user!\n\n';
                    theBody += 'Below you will find the PTS usernames associated with your email address. Please let us know if you still have trouble logging in.\n\n\n';
                    for(User u : theUsers){
                        theBody += u.UserName + '\n';
                    }
                } else {
                    theBody = theUsers[0].FirstName + ',\n\n';
                    theBody += 'Below you will find your username for the PTS site. Please let us know if you still have trouble logging in.\n\n\n';
                    theBody += 'Username: ' + theUsers[0].UserName; 
                }
                Messaging.SingleEmailMessage theEmail = new Messaging.SingleEmailMessage(); 
                theEmail.setSubject(theSubject);
                theEmail.setToAddresses(new List<String>{theUsers[0].Email});
                theEmail.setPlainTextBody(theBody);

                Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{theEmail});

                // return to page and display success message
                PageReference thePage = new PageReference(ApexPages.currentPage().getUrl());
                thePage.getParameters().put('success', 'true');
                thePage.setRedirect(true);
                return thePage;
            }
        }
        return null;

    }


    public Boolean hasError {get; private set;}
    public String errorText {get; private set;}

    private List<User> validateForm (){
        hasError = false;
        errorText = '';

        if(!sika_PTSValidationHelper.validateEmail(emailAddress) || emailAddress == null){
            hasError = true;
            errorText = invalidEmailErrorMsg;

            return null;
        }

        List<User> users = [SELECT FirstName, Email, UserName, ProfileId FROM User WHERE email = :emailAddress AND ProfileId IN :Pluck.ids(UserDao.getUserPartnerProfileIds())];
        if(users.size() > 0){
            return users;
        }
        hasError = true;
        errorText = emailNotRecognizedErrorMsg;
        return null;
    }


}