/**
 * Service class to get data for Lighting Reports using External Objects as data source.
 *
 * @author eru, dse
 * @copyright PARX
 */
public with sharing class FinancialReportsService
{
	private static final List<String> generalFields = new List<String>{'Bukrs__c',
																		'Calmonth__c',
																		'GrossSalesDeviation__c',
																		'GrossSalesDeviationPercent__c'};
	private static final List<String> salesFields = new List<String>{'Cust__c',
																	'GrossSalesCurrYear__c',
																	'MatMarginCurrYear__c',
																	'MatMarginPercentCurrYear__c',
																	'NetWeightCurrYear__c',
																	'GrossSalesPrevYear__c',
																	'MatMarginPrevYear__c',
																	'MatMarginPercentPrevYear__c',
																	'NetWeightPrevYear__c',
																	'Month__c',
																	'Waers__c',
																	'WeigthUom__c'};
	private static final List<String> salesByRepFields = new List<String>{'cust__c',
																	'ActualGrossSales__c',
																	'ActualMatMargin__c',
																	'ActualMatMarginPercent__c',
																	'ActualNetWeight__c',
																	'Currency__c',
																	'PrevYearGrossSales__c',
																	'PrevYearMatMargin__c',
																	'PrevYearMatMarginPercent__c',
																	'PrevYearNetWeight__c',
																	'Month__c',
																	'Uom__c'};
	private static final List<String> articleMixFields = new List<String>{'ActualQtyBuom__c',
																		'ActualGrossSales__c',
																		'ActualMatMargin__c',
																		'ActualMatMarginPercent__c',
																		'ActualNetWeight__c',
																		'Currency__c',
																		'PrevYearGrossSales__c',
																		'PrevYearMatMargin__c',
																		'PrevYearMatMarginPercent__c',
																		'PrevYearNetWeight__c',
																		'Buom__c',
																		'Kunnr__c',
																		'Material__c',
																		'Materialname__c',
																		'PrevYearQtyBuom__c',
																		'Prodhierarchy__c',
																		'Shipto__c',
																		'Uom__c'};
	private static final List<String> productMixFields = new List<String>{'Kunnr__c',
																		'ActualGrossSales__c',
																		'ActualMatMargin__c',
																		'ActualMatMarginPercent__c',
																		'ActualNetWeight__c',
																		'Currency__c',
																		'PrevYearGrossSales__c',
																		'PrevYearMatMargin__c',
																		'PrevYearMatMarginPercent__c',
																		'PrevYearNetWeight__c',
																		'Prodhierarchy__c','Shipto__c',
																		'Uom__c'};
	private static final List<String> salesRepsFields = new List<String>{'SalesRep__c', 'SalesRepName__c'};
	private static final String RECORD_TYPE_SOLD_TO = 'SoldTo_Account';
	private static final String RECORD_TYPE_SHIP_TO = 'ShipTo_Account';
	private static final Map<String, String> reportTypeToExternalObjectMap = new Map<String, String>
	{
		'salesFigures' => 'custPeriodeODMEsSet__x', //wave 1
		'salesFiguresBySalesReps' => 'custSalesRepODMEsSet__x', //wave 1
		'shipToSalesFigures' => 'customerPeriode_ST_ODMEsSet__x', //wave 1
		'shipToSalesFiguresBySalesReps' => 'customerSalesRep_ST_ODMEsSet__x', //wave 1
		'articleMix' => 'articleMixCustomerODMEsSet__x', //wave 1
		'articleMixBySalesReps' => 'articleMixSalesRepODMEsSet__x', //wave 1
		'productMix' => 'productMixODMEsSet__x', //wave 1
		'productMixBySalesReps' => 'productMixSalesRepODMEsSet__x' //wave 1
	};

	@TestVisible
	private List<articleMixSalesRepODMEsSet__x> mockArticleMixSalesRepSet;

	/**
	 * @description Returns the data for Sales Figures report
	 */
	@AuraEnabled
	public static List<custPeriodeODMEsSet__x> getSalesFiguresData(String accountId, Integer selectedYear)
	{
		if ( String.isBlank(accountId) )
		{
			return null;
		}
		Date today = Date.today();
		Account selectedAccount = [SELECT ERP_ID__c, Company_Code__c FROM Account WHERE Id =:accountId];
		al.ConditionGroup anAndConditions = new al.AndCondition();
		List<String> allRequiredFields = new List<String>(generalFields);
		allRequiredFields.addAll(salesFields);
		List<String> filteredFields = getSelectableFields(allRequiredFields, getAccessibleColumns('salesFigures'));
		al.SoqlBuilder aBuilder = new al.SoqlBuilder().selectx(filteredFields).fromx('custPeriodeODMEsSet__x').wherex(anAndConditions);
		anAndConditions.add(new al.FieldCondition('Bukrs__c', al.Operator.EQUALS, sapCompanyCodesMapping.get(selectedAccount.Company_Code__c)));
		anAndConditions.add(new al.FieldCondition('Calmonth__c', al.Operator.EQUALS, '12.' + selectedYear));
		// DSE: not needed, year is always 12 in this report! anAndConditions.add(getPeriodsClausewithYear('12', selectedYear));
		anAndConditions.add(new al.FieldCondition('Cust__c', al.Operator.EQUALS, selectedAccount.ERP_ID__c));
		DateTime startTime = DateTime.now();
		String query = aBuilder.toSoql();
		String queryParameters = query.substring(query.indexOf('WHERE') + 5, query.length()).removeStart('(').removeEnd(')');
		List<custPeriodeODMEsSet__x> salesFiguresData = null;
		try
		{
			System.debug(query);
			salesFiguresData = Database.query(query);
			DateTime endTime = DateTime.now();
			createLogEntry('getSalesFiguresData', startTime, endTime, 'SalesFigures', salesFiguresData.size(), queryParameters);
		}
		catch ( Exception ex )
		{
			System.debug(LoggingLevel.ERROR, 'OData Exception: ' + ex.getMessage());
			DateTime endTime = DateTime.now();
			createLogEntry('getSalesFiguresData', startTime, endTime, 'SalesFigures', 0, queryParameters, ex.getMessage());
			throw ex;
		}
		return salesFiguresData;
	}

		/**
	 * @description Returns the data for Sales Figures By Sales Reps and My Sales Figures reports
	 */
	@AuraEnabled
	public static List<custSalesRepODMEsSet__x> getSalesFiguresBySalesRepsData(String accountId, String salesRepId, Integer selectedYear, String selectedMonths)
	{
		Date today = Date.today();
		Account selectedAccount;
		User currentSalesRep;
		if ( String.isNotBlank(accountId) )
		{
			selectedAccount = [SELECT ERP_ID__c, Company_Code__c FROM Account WHERE Id = :accountId];
		}
		else
		{
			currentSalesRep = [SELECT Company_Code__c FROM User WHERE Id = :UserInfo.getUserId()];
		}
		al.ConditionGroup anAndConditions = new al.AndCondition();
		List<String> allRequiredFields = new List<String>(generalFields);
		allRequiredFields.addAll(salesRepsFields);
		allRequiredFields.addAll(salesByRepFields);
		al.SoqlBuilder aBuilder = new al.SoqlBuilder().selectx(allRequiredFields).fromx('custSalesRepODMEsSet__x').wherex(anAndConditions);
		anAndConditions.add(new al.FieldCondition('Calmonth__c', al.Operator.EQUALS, '12.' + selectedYear));
		anAndConditions.add(getPeriodsClausewithYear(selectedMonths, selectedYear));
		// DSE: not needed, year is always 12 in this report! anAndConditions.add(getPeriodsClausewithYear('12', selectedYear));
		if ( String.isNotBlank(accountId) )
		{
			anAndConditions.add(new al.FieldCondition('Bukrs__c', al.Operator.EQUALS, sapCompanyCodesMapping.get(selectedAccount.Company_Code__c)));
			anAndConditions.add(new al.FieldCondition('Cust__c', al.Operator.EQUALS, selectedAccount.ERP_ID__c));
		}
		else
		{
			anAndConditions.add(new al.FieldCondition('Bukrs__c', al.Operator.EQUALS, sapCompanyCodesMapping.get(currentSalesRep.Company_Code__c)));
		}
		if ( String.isNotBlank(salesRepId) )
		{
			anAndConditions.add(new al.FieldCondition('SalesRep__c', al.Operator.EQUALS, salesRepId));
		}
		DateTime startTime = DateTime.now();
		String query = aBuilder.toSoql();
		System.debug(query);
		String queryParameters = query.substring(query.indexOf('WHERE') + 5, query.length()).removeStart('(').removeEnd(')');
		List<custSalesRepODMEsSet__x> salesFiguresData = null;
		try
		{
			salesFiguresData = Database.query(query);
			DateTime endTime = DateTime.now();
			createLogEntry('getSalesFiguresBySalesRepsData', startTime, endTime, 'SalesFiguresBySalesReps', salesFiguresData.size(), queryParameters);
		}
		catch ( Exception ex )
		{
			System.debug(LoggingLevel.ERROR, 'OData Exception: ' + ex.getMessage());
			DateTime endTime = DateTime.now();
			createLogEntry('getSalesFiguresBySalesRepsData', startTime, endTime, 'SalesFiguresBySalesReps', 0, queryParameters, ex.getMessage());
			throw ex;
		}
		return salesFiguresData;
	}

	/**
	 * TODO merge
	 * @description Returns the data for Sales Figures By Sales Reps and My Sales Figures reports
	 */
	@AuraEnabled
	public static List<custSalesRepODMEsSet__x> getSalesFiguresBySalesRepsData(String accountId, String salesRepId, Integer selectedYear)
	{
		Date today = Date.today();
		Account selectedAccount;
		User currentSalesRep;
		if ( String.isNotBlank(accountId) )
		{
			selectedAccount = [SELECT ERP_ID__c, Company_Code__c FROM Account WHERE Id = :accountId];
		}
		else
		{
			currentSalesRep = [SELECT Company_Code__c FROM User WHERE Id = :UserInfo.getUserId()];

		}
		al.ConditionGroup anAndConditions = new al.AndCondition();
		List<String> allRequiredFields = new List<String>(generalFields);
		allRequiredFields.addAll(salesRepsFields);
		allRequiredFields.addAll(salesByRepFields);
		al.SoqlBuilder aBuilder = new al.SoqlBuilder().selectx(allRequiredFields).fromx('custSalesRepODMEsSet__x').wherex(anAndConditions);
		anAndConditions.add(new al.FieldCondition('Calmonth__c', al.Operator.EQUALS, '12.' + selectedYear));
		// DSE: not needed, year is always 12 in this report! anAndConditions.add(getPeriodsClausewithYear('12', selectedYear));
		if ( String.isNotBlank(accountId) )
		{
			anAndConditions.add(new al.FieldCondition('Bukrs__c', al.Operator.EQUALS, sapCompanyCodesMapping.get(selectedAccount.Company_Code__c)));
			anAndConditions.add(new al.FieldCondition('Cust__c', al.Operator.EQUALS, selectedAccount.ERP_ID__c));
		}
		else
		{
			anAndConditions.add(new al.FieldCondition('Bukrs__c', al.Operator.EQUALS, sapCompanyCodesMapping.get(currentSalesRep.Company_Code__c)));
		}
		if ( String.isNotBlank(salesRepId) )
		{
			anAndConditions.add(new al.FieldCondition('SalesRep__c', al.Operator.EQUALS, salesRepId));
		}
		DateTime startTime = DateTime.now();
		String query = aBuilder.toSoql();
		System.debug(query);
		String queryParameters = query.substring(query.indexOf('WHERE') + 5, query.length()).removeStart('(').removeEnd(')');
		List<custSalesRepODMEsSet__x> salesFiguresData = null;
		try
		{
			salesFiguresData = Database.query(query);
			DateTime endTime = DateTime.now();
			createLogEntry('getSalesFiguresBySalesRepsData', startTime, endTime, 'SalesFiguresBySalesReps', salesFiguresData.size(), queryParameters);
		}
		catch ( Exception ex )
		{
			System.debug(LoggingLevel.ERROR, 'OData Exception: ' + ex.getMessage());
			DateTime endTime = DateTime.now();
			createLogEntry('getSalesFiguresBySalesRepsData', startTime, endTime, 'SalesFiguresBySalesReps', 0, queryParameters, ex.getMessage());
			throw ex;
		}
		return salesFiguresData;
	}

	/**
	 * @description Returns the data for Ship To Sales Figures report
	 */
	@AuraEnabled
	public static List<customerPeriode_ST_ODMEsSet__x> getShipToSalesFiguresData(String accountId, Integer selectedYear)
	{
		if ( String.isBlank(accountId) )
		{
			return null;
		}
		Date today = Date.today();
		Account selectedAccount = [SELECT ERP_ID__c, Company_Code__c FROM Account WHERE Id =:accountId];
		al.ConditionGroup anAndConditions = new al.AndCondition();
		List<String> allRequiredFields = new List<String>(generalFields);
		allRequiredFields.add('Month__c');
		List<String> filteredFields = getSelectableFields(allRequiredFields, getAccessibleColumns('salesFigures'));
		al.SoqlBuilder aBuilder = new al.SoqlBuilder().selectx(filteredFields).fromx('customerPeriode_ST_ODMEsSet__x').wherex(anAndConditions);
		anAndConditions.add(new al.FieldCondition('Bukrs__c', al.Operator.EQUALS, sapCompanyCodesMapping.get(selectedAccount.Company_Code__c)));
		anAndConditions.add(new al.FieldCondition('Calmonth__c', al.Operator.EQUALS, '12.' + selectedYear));
		anAndConditions.add(new al.FieldCondition('ShipTo__c', al.Operator.EQUALS, selectedAccount.ERP_ID__c));
		DateTime startTime = DateTime.now();
		String query = aBuilder.toSoql();
		String queryParameters = query.substring(query.indexOf('WHERE') + 5, query.length()).removeStart('(').removeEnd(')');
		List<customerPeriode_ST_ODMEsSet__x> salesFiguresData = null;
		try
		{
			salesFiguresData = Database.query(query);
			DateTime endTime = DateTime.now();
			createLogEntry('getShipToSalesFiguresData', startTime, endTime, 'ShipToSalesFigures', salesFiguresData.size(), queryParameters);
		}
		catch ( Exception ex )
		{
			System.debug(LoggingLevel.ERROR, 'OData Exception: ' + ex.getMessage());
			DateTime endTime = DateTime.now();
			createLogEntry('getShipToSalesFiguresData', startTime, endTime, 'ShipToSalesFigures', 0, queryParameters, ex.getMessage());
			throw ex;
		}
		return salesFiguresData;
	}

	@AuraEnabled
	public static List<custSalesRepODMEsSet__x> getSalesPerMonthPerSalesRepData(String companyCode, String salesRepId, String userType, String selectedMonths, String Vkbur, String Spart, String Vtweg, Integer selectedYear)
	{
		List<String> additionalFields = new List<String>{
				'Month__c', 'SalesRep__c', 'SalesRepName__c'
		};

		al.SoqlBuilder queryBuiler = createQueryBuilder('custSalesRepODMEsSet__x', generalFields, additionalFields, 'Month__c', companyCode, salesRepId, userType, selectedMonths, Vkbur, null, Spart, null, selectedYear, false, true);

		List<custSalesRepODMEsSet__x> companyTurnOversData = (List<custSalesRepODMEsSet__x>)executeAndLogReportQuery(queryBuiler, 'getSalesPerMonthPerSalesRepData', 'custSalesRepODMEsSet__x');

		return companyTurnOversData;
	}

	/**
	 * @description Returns the data for Ship To Sales Figures By Sales Reps  reports
	 */
	@AuraEnabled
	public static List<customerSalesRep_ST_ODMEsSet__x> getShipToSalesFiguresBySalesRepsData(String accountId, Integer selectedYear)
	{
		if ( String.isBlank(accountId) )
		{
			return null;
		}
		Date today = Date.today();
		Account selectedAccount = [SELECT ERP_ID__c, Company_Code__c FROM Account WHERE Id = :accountId];
		al.ConditionGroup anAndConditions = new al.AndCondition();
		List<String> allRequiredFields = new List<String>(generalFields);
		allRequiredFields.addAll(salesRepsFields);
		allRequiredFields.add('Month__c');
		al.SoqlBuilder aBuilder = new al.SoqlBuilder().selectx(allRequiredFields).fromx('customerSalesRep_ST_ODMEsSet__x').wherex(anAndConditions);
		anAndConditions.add(new al.FieldCondition('Calmonth__c', al.Operator.EQUALS, '12.' + selectedYear));
		anAndConditions.add(new al.FieldCondition('Bukrs__c', al.Operator.EQUALS, sapCompanyCodesMapping.get(selectedAccount.Company_Code__c)));
		anAndConditions.add(new al.FieldCondition('ShipTo__c', al.Operator.EQUALS, selectedAccount.ERP_ID__c));
		DateTime startTime = DateTime.now();
		String query = aBuilder.toSoql();
		String queryParameters = query.substring(query.indexOf('WHERE') + 5, query.length()).removeStart('(').removeEnd(')');
		List<customerSalesRep_ST_ODMEsSet__x> salesFiguresData = null;
		try
		{
			salesFiguresData = Database.query(query);
			DateTime endTime = DateTime.now();
			createLogEntry('getShipToSalesFiguresBySalesRepsData', startTime, endTime, 'ShipToSalesFiguresBySalesReps', salesFiguresData.size(), queryParameters);
		}
		catch ( Exception ex )
		{
			System.debug(LoggingLevel.ERROR, 'OData Exception: ' + ex.getMessage());
			DateTime endTime = DateTime.now();
			createLogEntry('getShipToSalesFiguresBySalesRepsData', startTime, endTime, 'ShipToSalesFiguresBySalesReps', 0, queryParameters, ex.getMessage());
			throw ex;
		}
		return salesFiguresData;
	}

	/**
	 * @description Returns the data for Article Mix report
	 */
	@AuraEnabled
	public static List<articleMixCustomerODMEsSet__x> getArticleMixData(String erpNo, String companyCode, String selectedMonths, String accountType, Integer selectedYear)
	{
		if ( String.isBlank(erpNo) )
		{
			return null;
		}
		Date today = Date.today();
		//Account selectedAccount = [SELECT ERP_ID__c, Company_Code__c, RecordType.DeveloperName FROM Account WHERE Id =:accountId];
		al.ConditionGroup anAndConditions = new al.AndCondition();
		List<String> allRequiredFields = new List<String>(generalFields);
		allRequiredFields.addAll(articleMixFields);
		al.SoqlBuilder aBuilder = new al.SoqlBuilder().selectx(allRequiredFields).fromx('articleMixCustomerODMEsSet__x').wherex(anAndConditions);
		anAndConditions.add(new al.FieldCondition('Bukrs__c', al.Operator.EQUALS, sapCompanyCodesMapping.get(companyCode)));
		anAndConditions.add(new al.FieldCondition('Kunnr__c', al.Operator.EQUALS, erpNo));
		anAndConditions.add(getPeriodsClausewithYear(selectedMonths, selectedYear));
		if ( String.isNotBlank(accountType) && accountType == 'soldto' )
		{
			//anAndConditions.add(new al.FieldCondition('SoldTo__c', al.Operator.EQUALS, erpNo));
		}
		else if ( String.isNotBlank(accountType) && accountType == 'shipto' )
		{
			anAndConditions.add(new al.FieldCondition('ShipTo__c', al.Operator.EQUALS, erpNo));
		}
		else
		{
			return null;
		}
		DateTime startTime = DateTime.now();
		String query = aBuilder.toSoql();
		System.debug('query: ' + query);
		String queryParameters = query.substring(query.indexOf('WHERE') + 5, query.length()).removeStart('(').removeEnd(')');
		List<articleMixCustomerODMEsSet__x> articleMixData = null;
		try
		{
			articleMixData = Database.query(query);
			DateTime endTime = DateTime.now();
			createLogEntry('getArticleMixData', startTime, endTime, 'ArticleMix', articleMixData.size(), queryParameters);
		}
		catch ( Exception ex )
		{
			System.debug(LoggingLevel.ERROR, 'OData Exception: ' + ex.getMessage());
			DateTime endTime = DateTime.now();
			createLogEntry('getArticleMixData', startTime, endTime, 'ArticleMix', 0, queryParameters, ex.getMessage());
			throw ex;
		}

		return articleMixData;
	}
	/**
	 * @description Returns the data for Article Mix By Sales Rep and My Article Mix reports
	 */
	@AuraEnabled
	public static List<articleMixSalesRepODMEsSet__x> getArticleMixBySalesRepsData(String erpNo, String companyCode, String selectedMonths, String accountType, String salesRepId, Integer selectedYear)
	{
		if ( String.isBlank(erpNo) )
		{
			return null;
		}
		// Account selectedAccount = [SELECT ERP_ID__c, Company_Code__c, RecordType.DeveloperName FROM Account WHERE Id =:accountId];
		al.ConditionGroup anAndConditions = new al.AndCondition();
		List<String> allRequiredFields = new List<String>(generalFields);
		allRequiredFields.addAll(articleMixFields);
		allRequiredFields.addAll(salesRepsFields);
		al.SoqlBuilder aBuilder = new al.SoqlBuilder().selectx(allRequiredFields).fromx('articleMixSalesRepODMEsSet__x').wherex(anAndConditions);
		anAndConditions.add(new al.FieldCondition('Bukrs__c', al.Operator.EQUALS, sapCompanyCodesMapping.get(companyCode)));
		anAndConditions.add(new al.FieldCondition('Kunnr__c', al.Operator.EQUALS, erpNo));
		anAndConditions.add(getPeriodsClausewithYear(selectedMonths, selectedYear));
		if ( String.isNotBlank(salesRepId) )
		{
			anAndConditions.add(new al.FieldCondition('SalesRep__c', al.Operator.EQUALS, salesRepId));
		}
		if ( String.isNotBlank(accountType) && accountType == 'soldto' )
		{
			//anAndConditions.add(new al.FieldCondition('SoldTo__c', al.Operator.EQUALS, erpNo));
		}
		else if ( String.isNotBlank(accountType) && accountType == 'shipto' )
		{
			anAndConditions.add(new al.FieldCondition('ShipTo__c', al.Operator.EQUALS, erpNo));
		}
		else
		{
			return null;
		}
		DateTime startTime = DateTime.now();
		String query = aBuilder.toSoql();
		String queryParameters = query.substring(query.indexOf('WHERE') + 5, query.length()).removeStart('(').removeEnd(')');
		List<articleMixSalesRepODMEsSet__x> articleMixData = null;
		try
		{
			articleMixData = Database.query(query);
			DateTime endTime = DateTime.now();
			createLogEntry('getArticleMixBySalesRepsData',startTime, endTime, 'ArticleMixBySalesReps', articleMixData.size(), queryParameters);
		}
		catch ( Exception ex )
		{
			System.debug(LoggingLevel.ERROR, 'OData Exception: ' + ex.getMessage());
			DateTime endTime = DateTime.now();
			createLogEntry('getArticleMixBySalesRepsData', startTime, endTime, 'ArticleMixBySalesReps', 0, queryParameters, ex.getMessage());
			throw ex;
		}
		return articleMixData;
	}



	@AuraEnabled
	public static List<productMixODMEsSet__x> getProductMixData(String erpNo, String companyCode, String selectedMonths, String accountType,Integer selectedYear)
	{
		System.debug(LoggingLevel.WARN, 'getProductMixData ' + erpNo + ' ' + companyCode + ' ' + accountType);
		if ( String.isBlank(erpNo) )
		{
			return null;
		}
		// Account selectedAccount = [SELECT ERP_ID__c, Company_Code__c, RecordType.DeveloperName FROM Account WHERE Id =:accountId];
		al.ConditionGroup anAndConditions = new al.AndCondition();
		List<String> allRequiredFields = new List<String>(generalFields);
		allRequiredFields.addAll(productMixFields);
		al.SoqlBuilder aBuilder = new al.SoqlBuilder().selectx(allRequiredFields).fromx('productMixODMEsSet__x').wherex(anAndConditions);
		anAndConditions.add(new al.FieldCondition('Bukrs__c', al.Operator.EQUALS, sapCompanyCodesMapping.get(companyCode)));
		anAndConditions.add(new al.FieldCondition('Kunnr__c', al.Operator.EQUALS, erpNo));
		anAndConditions.add(getPeriodsClausewithYear(selectedMonths, selectedYear));
		if ( String.isNotBlank(accountType) && accountType == 'soldto' )
		{
			//anAndConditions.add(new al.FieldCondition('SoldTo__c', al.Operator.EQUALS, erpNo));
		}
		else if ( String.isNotBlank(accountType) && accountType == 'shipto' )
		{
			anAndConditions.add(new al.FieldCondition('ShipTo__c', al.Operator.EQUALS, erpNo));
		}
		else
		{
			return null;
		}
		DateTime startTime = DateTime.now();
		String query = aBuilder.toSoql();
		String queryParameters = query.substring(query.indexOf('WHERE') + 5, query.length()).removeStart('(').removeEnd(')');
		List<productMixODMEsSet__x> productMixData = null;
		try
		{
			productMixData = Database.query(query);
			DateTime endTime = DateTime.now();
			createLogEntry('getProductMixData', startTime, endTime, 'ProductMix', productMixData.size(), queryParameters);
		}
		catch ( Exception ex )
		{
			System.debug(LoggingLevel.ERROR, 'OData Exception: ' + ex.getMessage());
			DateTime endTime = DateTime.now();
			createLogEntry('getProductMixData', startTime, endTime, 'ProductMix', 0, queryParameters, ex.getMessage());
			throw ex;
		}
		return productMixData;
	}
	/**
	 * @description Returns the data for Product Mix By Sales Reps and My Product Mix reports
	 */
	@AuraEnabled
	public static List<productMixSalesRepODMEsSet__x> getProductMixBySalesRepsData(String erpNo, String companyCode, String selectedMonths, String accountType, String salesRepId, Integer selectedYear)
	{
		if ( String.isBlank(erpNo) )
		{
			return null;
		}
		// Account selectedAccount = [SELECT ERP_ID__c, Company_Code__c, RecordType.DeveloperName FROM Account WHERE Id =:accountId];
		al.ConditionGroup anAndConditions = new al.AndCondition();
		List<String> allRequiredFields = new List<String>(generalFields);
		allRequiredFields.addAll(productMixFields);
		allRequiredFields.addAll(salesRepsFields);
		al.SoqlBuilder aBuilder = new al.SoqlBuilder().selectx(allRequiredFields).fromx('productMixSalesRepODMEsSet__x').wherex(anAndConditions);
		anAndConditions.add(new al.FieldCondition('Bukrs__c', al.Operator.EQUALS, sapCompanyCodesMapping.get(companyCode)));
		anAndConditions.add(new al.FieldCondition('Kunnr__c', al.Operator.EQUALS, erpNo));
		//anAndConditions.add(getPeriodsClause(selectedMonths)); - commented by RG
		anAndConditions.add(getPeriodsClausewithYear(selectedMonths, selectedYear));
		if ( String.isNotBlank(salesRepId) )
		{
			anAndConditions.add(new al.FieldCondition('SalesRep__c', al.Operator.EQUALS, salesRepId));
		}
		if ( String.isNotBlank(accountType) && accountType == 'soldto' )
		{
			//anAndConditions.add(new al.FieldCondition('SoldTo__c', al.Operator.EQUALS, erpNo));
		}
		else if ( String.isNotBlank(accountType) && accountType == 'shipto' )
		{
			anAndConditions.add(new al.FieldCondition('ShipTo__c', al.Operator.EQUALS, erpNo));
		}
		else
		{
			return null;
		}
		DateTime startTime = DateTime.now();
		String query = aBuilder.toSoql();
		String queryParameters = query.substring(query.indexOf('WHERE') + 5, query.length()).removeStart('(').removeEnd(')');
		List<productMixSalesRepODMEsSet__x> productMixData = null;
		try
		{
			productMixData = Database.query(query);
			DateTime endTime = DateTime.now();
			createLogEntry('getProductMixBySalesRepsData',startTime, endTime, 'ProductMixBySalesReps', productMixData.size(), queryParameters);
		}
		catch ( Exception ex )
		{
			System.debug(LoggingLevel.ERROR, 'OData Exception: ' + ex.getMessage());
			DateTime endTime = DateTime.now();
			createLogEntry('getProductMixBySalesRepsData', startTime, endTime, 'ProductMixBySalesReps', 0, queryParameters, ex.getMessage());
			throw ex;
		}
		return productMixData;
	}

	/**
	 * Create the Wave 2 query
	 */
	private static al.SoqlBuilder createQueryBuilder(String externalReportObject, List<String> generalFields, List<String> additionalFields, String orderByField, String companyCode, String salesRepId, String userType, String selectedMonths, String Vkbur, String SBU, String Spart, String Vtweg, Integer selectedYear, Boolean monthSelectable, Boolean withoutVkorg)
	{
		System.debug('==== executing createQueryBuilder(companyCode=' + companyCode + ', salesRepId=' + salesRepId + ', userType=' + userType + ', selectedMonths=' + selectedMonths + ', selectedYear='+selectedYear);

		// Get the field to query
		List<String> allRequiredFields = new List<String>(generalFields);
		allRequiredFields.addAll(additionalFields);

		// Create the query
		al.ConditionGroup andConditions = new al.AndCondition();
		al.OrderBy orderby = new al.OrderBy(orderByField).ascending();
		al.SoqlBuilder queryBuilder = new al.SoqlBuilder().selectx(allRequiredFields).fromx(externalReportObject).wherex(andConditions).orderByx(orderby);

		andConditions.add(new al.FieldCondition('Bukrs__c', companyCode));
		if(!withoutVkorg)
		{
			andConditions.add(new al.FieldCondition('Vkorg__c', companyCode));
		}

		if (String.isNotBlank(salesRepId)) {
			andConditions.add(new al.SetCondition('SalesRep__c', al.Operator.INX, FinancialReportsUtil.stringToList(salesRepId)));
		}

		if (String.isNotBlank(Vkbur)) {
			andConditions.add(new al.FieldCondition('Vkbur__c', al.Operator.EQUALS, Vkbur));
		}

		if ( String.isNotBlank(SBU) ){
			andConditions.add(new al.FieldCondition('SBU__c', al.Operator.EQUALS, SBU));
		}

		if (String.isNotBlank(Vtweg)) {
			List<String> selectedDistChannels = Vtweg.split(';');
			al.ConditionGroup orConditions = new al.OrCondition();
			for ( String distChannel : selectedDistChannels )
			{
				orConditions.add(new al.FieldCondition('Vtweg__c', al.Operator.EQUALS, distChannel));
			}
			andConditions.add(orConditions);
		}

		if (String.isNotBlank(Spart)) {
			List<String> selectedDivisions = Spart.split(';');
			al.ConditionGroup orConditions = new al.OrCondition();
			for ( String division : selectedDivisions )
			{
				orConditions.add(new al.FieldCondition('Spart__c', al.Operator.EQUALS, division));
			}
			andConditions.add(orConditions);
		}

		// add the month conditions depending on whether this report type allows for month to be selected
		if(monthSelectable)
		{
			andConditions.add(getPeriodsClausewithYear(selectedMonths,selectedYear));
		}
		else
		{
			andConditions.add(new al.FieldCondition('Calmonth__c', al.Operator.EQUALS, '12.' + selectedYear));
		}

		return queryBuilder;
	}

	private static List<SObject> executeAndLogReportQuery(al.SoqlBuilder queryBuiler, String methodName, String objectName)
	{
		DateTime startTime = DateTime.now();
		String query = queryBuiler.toSoql();
		system.debug('==== ' + methodName + ' Query: ' + query);
		String queryParameters = query.substring(query.indexOf('WHERE') + 5, query.length()).removeStart('(').removeEnd(')');

		List<SObject> queryData = null;

		String exceptionMessage = '';
		try
		{
			queryData = Database.query(query + ' Limit 100'); // DSE we have to limit the number of records in order to prevent severe lightning exceptions!
		}
		catch ( Exception ex )
		{
			exceptionMessage = ex.getMessage();
			throw ex;
		}
		finally
		{
			DateTime endTime = DateTime.now();
			createLogEntry(methodName, startTime, endTime, objectName, queryData != null ? queryData.size() : 0, queryParameters, exceptionMessage);
		}

		return queryData;
	}


	/**
	 * @description Returns companyCode to SAPCompanyCode map
	 */
	private static Map<String, String> sapCompanyCodesMapping
	{
		get
		{
			if ( sapCompanyCodesMapping == null )
			{
				sapCompanyCodesMapping = new Map<String, String>();
				Map<String, CompanyCodeMappings__c> mapping = CompanyCodeMappings__c.getAll();
				for ( String companyCode : mapping.keySet() ) {
					sapCompanyCodesMapping.put(companyCode, mapping.get(companyCode).SAP_Company_Code__c);
				}
			}
			return sapCompanyCodesMapping;
		}
		set;
	}

	/**
	 * @description Creates logs entry
	 */
	public static void createLogEntry(String methodName, DateTime startTime, DateTime endTime, String reportType, Integer recordCount, String queryParameters)
	{
		createLogEntry(methodName, startTime, endTime, reportType, recordCount, queryParameters, '');
	}

	public static void createLogEntry(String methodName, DateTime startTime, DateTime endTime, String reportType, Integer recordCount, String queryParameters, String errorMessage)
	{
		ODataLog__c logEntry = new ODataLog__c();
		logEntry.APEXClass__c = 'FinancialReportsService.' + methodName;
		logEntry.LoggedInUser__c = UserInfo.getUserId();
		logEntry.Start__c = startTime;
		logEntry.End__c = endTime;
		logEntry.Duration__c = (endTime.getTime() - startTime.getTime()) / 1000.0;
		logEntry.ExternalObject__c = reportType;
		logEntry.ResultRecordCount__c = recordCount;
		logEntry.QueryParameters__c = queryParameters;
		logEntry.ErrorMessage__c = errorMessage;
		insert logEntry;
	}

	/**
	 * @description Returns the data for Report Header
	 */
	@AuraEnabled
	public static String getHeaderInfo(String accountId)
	{
		Account currentAccount;
		User currentSalesRep;
		if ( String.isNotBlank(accountId) )
		{
			currentAccount = [SELECT Name,Company_Code__c FROM Account WHERE Id = :accountId];
		}
		else
		{
			currentSalesRep = [SELECT Company_Code__c FROM User WHERE Id = :UserInfo.getUserId()];
		}
		String accountName = currentAccount != null ? currentAccount.Name : '';
		String sapCompanyCode = currentAccount != null ? sapCompanyCodesMapping.get(currentAccount.Company_Code__c) : sapCompanyCodesMapping.get(currentSalesRep.Company_Code__c);
		String salesRepName = UserInfo.getName();
		return '{"accountName":"' + accountName +'", "sapCompanyCode":"' + sapCompanyCode + '", "salesRepName":"' + salesRepName + '"}';
	}

	/**
	 * @description Returns a list of accessible columns
	 */
	@AuraEnabled
	public static List<String> getAccessibleColumns(String reportType)
	{
		List<String> accessibleFields = new List<String>();
		String externalObjectName = reportTypeToExternalObjectMap.get(reportType);
		Map<String, Schema.SObjectField> fieldsMap = Schema.getGlobalDescribe().get(externalObjectName).getDescribe().fields.getMap();
		for ( String fieldName : fieldsMap.keySet() )
		{
			Schema.DescribeFieldResult fieldDescribe = fieldsMap.get(fieldName).getDescribe();
			if ( fieldDescribe.isAccessible() )
			{
				accessibleFields.add(fieldName);
			}

		}
		return accessibleFields;
	}

	/**
	 * @description Returns a list of accessible fields
	 */
	private static List<String> getSelectableFields(List<String> objectFields, List<String> accessibleColumns)
	{
		Set<String> accessibleColumnsSet = new Set<String>(accessibleColumns);
		List<String> filteredFields = new List<String>();
		for ( String fieldName : objectFields )
		{
			if ( accessibleColumnsSet.contains(fieldName.toLowerCase()) )
			{
				filteredFields.add(fieldName);
			}
		}
		return filteredFields;
	}

	// DSE: new method considering selected year
	private static al.OrCondition getPeriodsClausewithYear(String selectedMonths, Integer selectedYear)
	{
		Integer currentYear = selectedYear;
		al.OrCondition conditions = new al.OrCondition();
		List<String> selectedPeriodMonths = selectedMonths.split(',');
		for ( String selectedMonth : selectedPeriodMonths )
		{
			String month = '';
			Integer monthNumber = Integer.valueOf(selectedMonth);
			if ( monthNumber < 10 )
			{
				month += '0' + monthNumber;
			}
			else
			{
				month += monthNumber;
			}
			month += '.' + currentYear;
			System.debug('===== adding month condition: ' + month);
			conditions.add(new al.FieldCondition('Calmonth__c', al.Operator.EQUALS, month));
		}
		return conditions;
	}

	/**
	 * @description Returns a List of Sales Rep Erp Ids of the direct rerports for a given manager Erp id
	 */
	public static List<String> getSalesRepIdsOfDirectReports(String managerUserId)
	{
		List<User> directReports = [select id , ERPId__c, name from User where  ManagerId =:managerUserId];

		List<String> salesRepIds = new List<String>();
		for (User directReport : directReports)
		{
			if(directReport.ERPId__c != null)
			{
				salesRepIds.add(directReport.ERPId__c);
			}
		}

		return salesRepIds;
	}







 


}