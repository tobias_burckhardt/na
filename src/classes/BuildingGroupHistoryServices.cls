/**
* @description service for Building_Group_History__c object 
*/
public with sharing class BuildingGroupHistoryServices {

    /**
    * @description Creates record Building_Group_History__c using new and old product name
    * @param new and old list Building_Group_Product__c
    */
    public static void saveBuildingGroupHistory(List<Building_Group_Product__c> newList, Map<Id, Building_Group_Product__c> oldMap) {
        List<Building_Group_History__c> buildingGhForCreate = new List<Building_Group_History__c>();
        for(Building_Group_Product__c bgp : newList) {
            if( isChangeProduct(bgp, oldMap.get(bgp.Id)) ) {
                buildingGhForCreate.add( createBuildingGroupHistory(bgp, oldMap.get(bgp.Id)) );
            }
        }
        
        if(!buildingGhForCreate.isEmpty()) {
            insert buildingGhForCreate;
        }
    }
    
    /**
    * @description creates object Building_Group_History__c using new and old Building_Group_Product__c
    * @param new and old Building_Group_Product__c
    * @return Building_Group_History__c
    */
    private static Building_Group_History__c createBuildingGroupHistory(Building_Group_Product__c newObject, Building_Group_Product__c oldObject) {
        return new Building_Group_History__c(Modified_From__c = oldObject.Product_Lookup_Name__c, Modified_To__c = newObject.Product_Lookup_Name__c);
    }
    
    /**
    * @description check if change product
    * @param new and old Building_Group_Product__c
    * @return boolean
    */
    private static Boolean isChangeProduct(Building_Group_Product__c newObject, Building_Group_Product__c oldObject) {
        return newObject.Product_lookup__c != oldObject.Product_lookup__c;
    }
}