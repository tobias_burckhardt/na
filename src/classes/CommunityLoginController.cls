/* **********************************************************
Name        :CommunityLoginController
Description :this is a controller for CommunityLogin page which exposes the 
             Community Login functionality
Author      :Shradha Bansal
Created On  :8/January/2015
Modified On :
********************************************************** 
*/
global with sharing class CommunityLoginController {
    global String username{get;set;}
    global String password{get;set;}
    global CommunityLoginController () {}
    global PageReference forwardToCustomAuthPage() {
        return new PageReference( '/CommunityLogin');
    }
    global PageReference login() {
        return Site.login(username, password, null);
    }

}