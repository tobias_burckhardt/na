public class sika_SCZoneSideNavigationController {
	public String pageName {get; set;}
	public Boolean showFilters {get; set;}
	public String homeStyle {get; set;}
	public String mixStyle {get; set;}
	public String matStyle {get; set;}
	public String supStyle {get; set;}
	
	public String allStyle {get; set;}
	public String cementStyle {get; set;}
	public String scmStyle {get; set;}
	public String sandStyle {get; set;}
	public String adStyle {get; set;}
	public String coarseStyle {get; set;}
	public String waterStyle {get; set;}
	public String otherStyle {get; set;}
	public list<String> pageList {get; set;}
	public String selectOption {get; set;}
	
	public sika_SCZoneSideNavigationController(){
	    showFilters = false;
	    selectOption = Apexpages.currentPage().getParameters().get('state');
	    if(selectOption=='' || selectOption==null){
	        selectOption='me';
	    }
	    homeStyle = '';
        mixStyle = '';
        matStyle = '';
        supStyle = '';
        allStyle = '';
        cementStyle = '';
        scmStyle = '';
        sandStyle = '';
        adStyle = '';
        coarseStyle = '';
        waterStyle = '';
        otherStyle = '';
	    
	    String tempPage;  
	    tempPage = ApexPages.currentPage().getUrl();
	    
	    String view;
	    view = ApexPages.currentPage().getParameters().get('view') != null ? ApexPages.currentPage().getParameters().get('view') : ''; 

		if(tempPage.contains('apex/')){
	    	tempPage = tempPage.split('apex/')[1];
		} else {
			tempPage = tempPage.split('SCCZone/')[1];
		}
	    
	    pageList = tempPage.split('\\?');
	    pageName = pageList[0];
	    
	    if(pageName == 'sika_SCCZoneUserHomePage'){
	        homeStyle = 'active';
	        mixStyle = '';
	        matStyle = '';
	        supStyle = '';
	    }
	    else if(pageName == 'sika_SCCZoneMixesHomePage' || pageName=='sika_SCCZoneNewMixPage' || pageName=='sika_SCCZoneMixDetailsPage' || pageName =='sika_SCCZoneManageMixMaterialsPage2'){
	        homeStyle = '';
	        mixStyle = 'active';
	        matStyle = '';
	        supStyle = '';
	    
	    }
	    else if(pageName =='sika_SCCZoneCreateEditMaterialPage' || pageName=='sika_SCCZoneMaterialDetailPage'){
	        homeStyle = '';
	        mixStyle = '';
	        matStyle = 'active';
	        supStyle = '';
	        showFilters = true;
	    }
	    
	    else if(pageName =='sika_SCCZoneMaterialsHomePage'){
	        homeStyle = '';
	        mixStyle = '';
	        matStyle = '';
	        supStyle = '';
	        showFilters = true;
	        if(pageName =='sika_SCCZoneMaterialsHomePage' && view == 'all'){
	            allStyle = 'active';
	            cementStyle = '';
	            scmStyle = '';
	            sandStyle = '';
	            adStyle = '';
	            coarseStyle = '';
	            waterStyle = '';
	            otherStyle = '';
	        }
	        else if(pageName =='sika_SCCZoneMaterialsHomePage' && view == 'cement'){
	            allStyle = '';
	            cementStyle = 'active';
	            scmStyle = '';
	            sandStyle = '';
	            adStyle = '';
	            coarseStyle = '';
	            waterStyle = '';
	            otherStyle = '';
	        }
	        else if(pageName =='sika_SCCZoneMaterialsHomePage' && view == 'scm'){
	            allStyle = '';
	            cementStyle = '';
	            scmStyle = 'active';
	            sandStyle = '';
	            adStyle = '';
	            coarseStyle = '';
	            waterStyle = '';
	            otherStyle = '';
	        }
	        else if(pageName =='sika_SCCZoneMaterialsHomePage' && view == 'sand'){
	            allStyle = '';
	            cementStyle = '';
	            scmStyle = '';
	            sandStyle = 'active';
	            adStyle = '';
	            coarseStyle = '';
	            waterStyle = '';
	            otherStyle = '';
	        }
	        else if(pageName =='sika_SCCZoneMaterialsHomePage' && view == 'ad'){
	            allStyle = '';
	            cementStyle = '';
	            scmStyle = '';
	            sandStyle = '';
	            adStyle = 'active';
	            coarseStyle = '';
	            waterStyle = '';
	            otherStyle = '';
	        }
	        else if(pageName =='sika_SCCZoneMaterialsHomePage' && view == 'coarse'){
	            allStyle = '';
	            cementStyle = '';
	            scmStyle = '';
	            sandStyle = '';
	            adStyle = '';
	            coarseStyle = 'active';
	            waterStyle = '';
	            otherStyle = '';
	        }
	        else if(pageName =='sika_SCCZoneMaterialsHomePage' && view == 'water'){
	            allStyle = '';
	            cementStyle = '';
	            scmStyle = '';
	            sandStyle = '';
	            adStyle = '';
	            coarseStyle = '';
	            waterStyle = 'active';
	            otherStyle = '';
	        }
	        else if(pageName =='sika_SCCZoneMaterialsHomePage' && view == 'other'){
	            allStyle = '';
	            cementStyle = '';
	            scmStyle = '';
	            sandStyle = '';
	            adStyle = '';
	            coarseStyle = '';
	            waterStyle = '';
	            otherStyle = 'active';
	        }
	    }
	    else if(pageName == 'sika_SCCZoneSupportHomePage' || pageName=='sika_SCCZoneContactUpdatePage' || pageName=='sika_SCCZoneSupportTicket'){
	        homeStyle = '';
	        mixStyle = '';
	        matStyle = '';
	        supStyle = 'active';
	    }
	    else {
	    	system.debug('********ERROR: sika_SCZoneSideNavigationController didn\'t find a matching rule for page "' + ApexPages.currentPage().getUrl() + '" -- setting the nav highlight to Home');
	        homeStyle = 'active';
	        mixStyle = '';
	        matStyle = '';
	        supStyle = '';
	    }
	   
	}

}