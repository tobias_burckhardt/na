@IsTest 
private class CommunitiesLandingControllerTest {
    @IsTest static void testCommunitiesLandingController() {
        // Instantiate a new controller
        CommunitiesLandingController controller = new CommunitiesLandingController();

        system.assert(
        	(String.valueOf(controller.forwardToCustomStartPage()) == String.valueOf(new PageReference('/sika_SCCZoneDisclaimer'))) 
        		|| 
        	(String.valueOf(controller.forwardToCustomStartPage()) == String.valueOf(new PageReference('/sika_SCCZoneUserHomePage')))
        );
    }
}