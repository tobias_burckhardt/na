public class OpportunityAlertLayoutController {
    public String selectedLang{get;set;}
    public Boolean displayAlert{get;set;}
    public Boolean displayAlertForAccount{get;set;}
    public Opportunity opp{get;set;}
    public OpportunityAlertLayoutController( ApexPages.StandardController std){
        
        selectedLang = UserInfo.getLanguage();
        if( std.getRecord().Id != null ){
            opp = [Select Id,AccountId,Amount,Purpose_of_Free_Sample__c,Customer_Claim_Number__c,OwnerId from Opportunity where Id=:std.getRecord().Id]; 
            if(String.isBlank(opp.AccountId))
                displayAlertForAccount = true;
            else
                displayAlertForAccount = false;   
            IF(opp.Amount > 500 && (String.isNotBlank(opp.Purpose_of_Free_Sample__c) && 'Goodwill'.equalsIgnoreCase(opp.Purpose_of_Free_Sample__c)) && opp.Customer_Claim_Number__c==null){
                displayAlert = true;
            }
        }
    }
}