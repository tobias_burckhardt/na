/**
* @author Garvit Totuka
* @date 18 Sep 2017
*
* @group user Sharing Management
*
* @description Opportunity Share At User Level Batch class updates Opportunity Share Objects that runs for all active and portal enabled users
*/
global class OpportunityShareAtUserLevelBatch implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id, ContactId, IsActive, AccountId, IsPortalEnabled, ProfileId
            FROM User
            WHERE IsActive = true AND IsPortalEnabled = true
        ]);
    }

    global void execute(Database.BatchableContext BC, List<user> scope) {
        OpportunityShareAtUserLevelBatchServices.processBatch(scope);

    }

    global void finish(Database.BatchableContext BC) {
    }
}