public without sharing class sika_SCCZoneNewEditMMOverride {
	private String retURL;
	private Id theMixId;
	private RecordType recordType;

	public String materialType {get; private set;}
	public String theMixName {get; private set;}
	public Mix_Material__c theMM {get; set;}
	public Boolean materialSelectError {get; private set;}

	public String action {get; private set;}
	public Boolean isQuantityRequired {get; private set;}
	public Boolean isUnitRequired {get; private set;}
	public Boolean isMoistureRequired {get; private set;}
	public Boolean isAdxPriceRequired {get; private set;}


	public sika_SCCZoneNewEditMMOverride(ApexPages.StandardController stdCtrl) { 
		Mix_Material__c theRecord = (Mix_Material__c) stdCtrl.getRecord();		
		retURL = ApexPages.currentPage().getParameters().get('retURL');
		materialSelectError = false;

		// when creating a new MM, getRecord() returns the related Mix Id and the recordType Id.
		// when editing an existing MM, getRecord() returns just the MM's id.
system.debug('******** theRecord = ' + theRecord);
		Boolean isNew;

		// if we're creating a new Mix Material...
		if(theRecord.Id == null){
			recordType = getRecordType(theRecord.RecordTypeId);

			theMM = new Mix_Material__c();
			theMixId = theRecord.Mix__c == null ? null : theRecord.Mix__c;
			if(theMixId == null){
				theMixName = null;
			} else {
				theMixName = [SELECT Id, Mix_Name__c FROM Mix__c WHERE Id = :theMixId].Mix_Name__c;
			}
			
			materialType = recordType.Name;

			theMM.RecordTypeId = theRecord.RecordTypeId;
			theMM.Mix__c = theMixId == null ? null : theMixId;
			
			isNew = true;

		// if we're editing an existing Mix Material...
		} else {
			theMM = [SELECT Id, 
							Material__c, 
							Material_Type__c, 
							RecordTypeId, 
							Quantity__c, 
							Unit__c, 
							Moisture__c, 
							Admixture_Cost__c, 
							Mix__c, 
							Mix__r.Mix_Name__c 
					   FROM Mix_Material__c 
					  WHERE Id = :theRecord.Id];

			recordType = getRecordType(theMM.RecordTypeId);
			theMixName = theMM.Mix__r.Mix_Name__c;
			theMixId = theMM.Mix__c;
			materialType = theMM.Material_Type__c;

			isNew = false;
		}
		action = isNew == true ? 'New' : 'Edit';

		// populate the is[X]Required Boolean values associated with each field 
		setFieldRequirements();

	}


	public PageReference saveAction(){
		list<Mix_Material__c> mmsToUpsert = new list<Mix_Material__c>();
		Mix_Material__c theSCM;
		Mix_Material__c theCement;
		map<String, String> SCMInfo;

		list<Mix_Material__c> theMMs = [SELECT Id, 
											   Mix__c, 
											   Material__c,
											   Material_Type__c,
											   RecordTypeId, 
											   Quantity__c, 
											   Unit__c 
									 	  FROM Mix_Material__c 
										 WHERE Mix__c = :theMixId];


		if(materialType == 'Cement'){
			// see if there is an SCM MM associated with the same mix
			for(Mix_Material__c mm : theMMs){
				if(mm.Material_Type__c == 'SCM'){
					theSCM = mm;
					break;
				}
			}

			// if there is no SCM in the Mix, we'll just insert/update the MM
			if(theSCM == null){
				mmsToUpsert.add(theMM);

			// if there is an SCM in the mix, calculate the SCM's quantity and set its unit
			} else {
				SCMinfo = sika_SCCZoneCalculateSCMWeight.getSCMWeightAndUnit(theMM.Quantity__c, theMM.Unit__c, theMixId);
				theSCM.Quantity__c = Decimal.valueOf(SCMinfo.get('quantity'));
				theSCM.Unit__c = SCMinfo.get('unit');

				// add the Cement and the SCM to the list of MMs to be updated.  ("the list" = the MM record being created/edited, plus this SCM MM.)
				mmsToUpsert.add(theMM);
				mmsToUpsert.add(theSCM);

			}
			
		} else if(materialType == 'SCM'){
			// see if there is a Cement MM associated with the same mix
			for(Mix_Material__c mm : theMMs){
				if(mm.Material_Type__c == 'Cement'){
					theCement = mm;
					break;
				}
			}

			// if there is no Cement associated with the mix, set the SCM's quantity to 0 and its unit to ...it doesn't matter.  We'll default to kgs.
			if(theCement == null){
				theMM.Quantity__c = 0;
				theMM.Unit__c = 'kgs';

				mmsToUpsert.add(theMM);

			// if there is, calculate the SCM's quantity and set its unit
			} else {
				SCMinfo = sika_SCCZoneCalculateSCMWeight.getSCMWeightAndUnit(theCement.Quantity__c, theCement.Unit__c, theMixId);
				theMM.Quantity__c = Decimal.valueOf(SCMinfo.get('quantity'));
				theMM.Unit__c = SCMinfo.get('unit');	

				mmsToUpsert.add(theMM);
			}
			
		// if the Mix Material record isn't a Cement or an SCM, ...	
		} else { 
			mmsToUpsert.add(theMM);
		}

		// if the Mix already contains the selected material, return the user to the page and display an error.
		// otherwise, upsert the material(s);
		if(checkSelectedMaterialError(theMM, theMMs) == false){
			// upsert the MM(s)
			upsert(mmsToUpsert);

			// return the user to the page from which the new/edit action was started
			PageReference p = new PageReference(retURL);
			p.setRedirect(true);
			return p;

		} else {
			// return the user to the new/edit MMs page, with the relevant error message displayed
			return null;
		}

	}


	public Boolean checkSelectedMaterialError(Mix_Material__c selectedMM, list<Mix_Material__c> theMMs){
		Boolean hasErrors = false;
		for(Mix_Material__c mm : theMMs){
			// if the selected material is already in the mix, and the selected material is NOT the material we're editing...
			if(mm.Material__c == selectedMM.Material__c && theMM.Material__c != selectedMM.Material__c){
				theMM.Material__c.addError('The Mix already contains this Material. Please select another.');
				hasErrors = true;
			}
		}
// selectedMM.Material_Type__c value is null at this point.  (We don't have access to selectedMM's fields, beyond those given to us by the lookup select window.)
	// might be able to query for selected mm by Id??
/*
		system.debug('******** String.valueOf(selectedMM.Material_Type__c) = ' + String.valueOf(selectedMM.Material_Type__c));
		system.debug('******** materialType = ' + materialType);
		// if the selected material does not match the material type referenced in the recordType selector, ...
		if(String.valueOf(selectedMM.Material_Type__c) != materialType){
			theMM.Material__c.addError('The material selected is not of type ' + materialType + '. Please select another.');
			hasErrors = true;
		}
*/
		if(hasErrors == true){
			return true;
		}
		return false; // all is well
	}


	private RecordType getRecordType(Id recordTypeId){
		RecordType rt = new RecordType();
		rt = [SELECT Id, 
					 Name 
				FROM RecordType 
			   WHERE SobjectType = 'Mix_Material__c' 
				 AND Id = :recordTypeId]; 

		return rt;
	}


	private void setFieldRequirements(){
		isQuantityRequired = true;
		isUnitRequired = true;
		isMoistureRequired = true;
		isAdxPriceRequired = false;

		if(materialType == 'Water'){
			isMoistureRequired = false;
		} else if(materialType == 'Sand'){
			//
		} else if(materialType == 'Coarse Aggregate'){
			//
		} else if(materialType == 'Cement'){
			isMoistureRequired = false;
		} else if(materialType == 'SCM'){
			isQuantityRequired = false;
			isUnitRequired = false;
			isMoistureRequired = false;
		} else if(materialType == 'Admixture'){
			isAdxPriceRequired = true;
			isMoistureRequired = false;
		} else if(materialType == 'Other'){
			//
		} else {
			system.debug(loggingLevel.ERROR, '********ERROR: material type is not recognized: ' + materialType);
		}
	}

}