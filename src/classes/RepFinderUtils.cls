/**
 * Utilities for the Rep Finder
 *
 * @author ben.burwell@trifecta.com
 */
public without sharing class RepFinderUtils {
	public static final String RECORDTYPE_REGION_GEOGRAPHIC = 'Geographic';
	public static final String RECORDTYPE_REGION_VERTICAL = 'Vertical';

	public static Map<String,RecordType> regionRecordTypes;

	/**
	 * Get the picklist entries as a list of strings for a SObject field
	 *
	 * @param field the describe info for the field to return the picklist values of
	 * @return the picklist entries for the specified field as strings
	 */
	public static List<String> getPicklistEntries(Schema.DescribeFieldResult field) {
		List<Schema.PicklistEntry> picklistEntries = field.getPicklistValues();
		List<String> entryStrings = new List<String>();
		for (Schema.PicklistEntry entry : picklistEntries) {
			entryStrings.add(entry.getValue());
		}
		return entryStrings;
	}

	/**
	 * Get a phone number in its canonical form, with no punctuation or spaces
	 *
	 * @param phone a phone number
	 * @return the phone number with all formatting stripped out
	 */
	public static String getCanonicalPhone(String phone) {
		if (String.isBlank(phone)) {
			return '';
		} else {
			return phone.replaceAll('[^0-9]', '');
		}
	}

	/**
	 * Perform a rep search based on supplied criteria
	 *
	 * @param targetMarket the target market to look for reps in
	 * @param state the state to look for reps in
	 * @param vertical the vertical to look for reps in
	 * @return a list of RepFinderSearchResult
	 */
	public static List<RepFinderSearchResult> doRepSearch(String targetMarket, String state, String vertical) {
		List<FindARep__c> reps = Database.query(SoqlUtils.getSelect(
			RepFinderSearchResult.REP_FIELDS, 'FindARep__c'
		).withCondition(getCondition(targetMarket, state, vertical)).toString());
		List<RepFinderSearchResult> results = new List<RepFinderSearchResult>();
		for (FindARep__c rep : reps) {
			RepFinderSearchResult result = new RepFinderSearchResult(rep);
			result.state = state;
			results.add(result);
		}
		return results;
	}

	/**
	 * Get a list of SOQL search conditions based on the criteria provided
	 *
	 * @param targetMarket the target market to apply conditions for
	 * @param state the state to apply conditions for
	 * @param vertical the vertical to apply conditions for
	 * @return a SoqlUtils.SoqlCondition
	 */
	private static SoqlUtils.SoqlCondition getCondition(String targetMarket, String state, String vertical) {
		List<SoqlUtils.SoqlCondition> conditions = new List<SoqlUtils.SoqlCondition>();
		conditions.add(SoqlUtils.getEq('Active__c', true));
		conditions.add(SoqlUtils.getEq('Region__r.Active__c', true));

		// target market conditions
		if (!String.isBlank(targetMarket)) {
			conditions.add(SoqlUtils.getEq('Region__r.Target_Market__c', targetMarket));
		}

		// state/region conditions
		if (!String.isBlank(state)) {
			conditions.add(SoqlUtils.getIncludesAll('Rep_State__c', new List<String>{ state }));
		}

		// vertical conditions
		if (!String.isBlank(vertical)) {
			conditions.add(SoqlUtils.getEq('Region__r.Vertical__c', vertical));
			conditions.add(SoqlUtils.getEq('Region__r.RecordType.DeveloperName', RECORDTYPE_REGION_VERTICAL));
		}

		return SoqlUtils.getAnd(conditions);
	}

	/**
	 * Get region record types
	 *
	 * @param developerName the developer name to look for
	 * @return the RecordType matching the developer name specified
	 */
	public static RecordType getRegionRecordType(String developerName) {
		if (regionRecordTypes == null) {
			List<RecordType> recordTypes = [
				SELECT
					Id,
					DeveloperName
				FROM
					RecordType
				WHERE
					SObjectType = 'Region__c'
			];
			regionRecordTypes = new Map<String,RecordType>();
			for (RecordType t : recordTypes) {
				regionRecordTypes.put(t.DeveloperName, t);
			}
		}
		if (regionRecordTypes.containsKey(developerName)) {
			return regionRecordTypes.get(developerName);
		}
		System.debug('Region has no record type with developer name ' + developerName);
		throw new NoDataFoundException();
	}
}