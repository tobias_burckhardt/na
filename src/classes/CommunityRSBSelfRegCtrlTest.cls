@IsTest
private class CommunityRSBSelfRegCtrlTest {

    static final String USERNAME = 'estes.mike@us.sika.com';

    @TestSetup
    static void setup() {
        if ([SELECT COUNT() FROM User WHERE Username =: USERNAME] < 1) {
            SObjectFactory.create(User.SObjectType, new Map<SObjectField, Object> {
                    User.UserName => USERNAME
            });
        }
    }

    @IsTest
    static void testCompanyTypes() {
        Test.setCurrentPage(Page.CommunityRSBSelfRegistration);
        Test.startTest();
        CommunityRSBSelfRegController controller = new CommunityRSBSelfRegController();
        List<SelectOption> companyTypes = controller.getCompanyTypes();
        Test.stopTest();

        System.assertEquals(7, companyTypes.size(), 'Company type list should have been retrieved successfully');
    }

    @IsTest
    static void validateEmptyForm() {
        Test.setCurrentPage(Page.CommunityRSBSelfRegistration);
        Test.startTest();
        CommunityRSBSelfRegController controller = new CommunityRSBSelfRegController();
        controller.emailAddress = 'email';
        Boolean isValid = controller.validateForm();
        Test.stopTest();

        System.assertEquals(false, isValid, 'Form should not be valid');
        System.assertNotEquals(0, ApexPages.getMessages().size(), 'There should have been Apex Messages generated!');
    }

    @IsTest
    static void validateForm() {
        Test.setCurrentPage(Page.CommunityRSBSelfRegistration);

        Test.startTest();
        CommunityRSBSelfRegController controller = new CommunityRSBSelfRegController();
        controller.firstName = 'FirstName';
        controller.lastName = 'LastName';
        controller.companyName = 'CompanyName';
        controller.companyType = controller.getCompanyTypes().get(1).getValue();
        controller.jobTitle = 'Title';
        controller.emailAddress = 'email@email.com';
        controller.country = 'United States';
        controller.phoneNo = '1234567890';
        controller.streetAddress = '321 N Clark';
        controller.city = 'Chicago';
        controller.state = 'Illinois';
        controller.postalCode = '60654';
        Boolean isValid = controller.validateForm();
        PageReference newPage = controller.signUp();
        Test.stopTest();

        System.assertEquals(true, isValid, 'Form should now be valid');
    }
}