public with sharing class RequestForProjectApprovalTriHandler {

	public static void handleBeforeUpdate(List<Request_for_Project_Approval__c> newRPA, Map<Id, Request_for_Project_Approval__c> oldMapbyID){
		
	}

    public static void handleAfterUpdate(List<Request_for_Project_Approval__c> newApprovals,
            Map<Id, Request_for_Project_Approval__c> oldApprovalsById) {
        updateSharingForRPADocuments(newApprovals, oldApprovalsById);
        updateCurrentApprovers(newApprovals, oldApprovalsById);
    }

    /**
     * Updates the sharing rules for the Request for Project Approval's files so that the
     * Project Manager and the Local Approver can view and edit them.
     *
     * Note that if running batch updates for the Project Manager and Local Controller lookups this
     * step can be problematic since we have to query for old ContentDocumentLink records, for which
     * the where clause is relatively complex. Also the number of records to query can grow rapidly.
     * 
     * @param newApprovals     the new Request for Project Approval records
     * @param oldApprovalsById the map with the old Request for Project Approval records
     */
    private static void updateSharingForRPADocuments(
            List<Request_for_Project_Approval__c> newApprovals,
            Map<Id, Request_for_Project_Approval__c> oldApprovalsById) {

        List<Request_for_Project_Approval__c> filteredApprovals =
            RequestForProjectApprovalServices.filterNewEmployee(newApprovals, oldApprovalsById);
            
        if (!filteredApprovals.isEmpty()) {
            Map<Id, List<ContentDocumentLink>> rpaLinks =
                RequestForProjectApprovalServices.retrieveRPADocumentLinks(filteredApprovals);
            ContentDocumentLinkChanges linkChanges =
                RequestForProjectApprovalServices.retrieveContentDocumentLinks(filteredApprovals,
                oldApprovalsById, rpaLinks);
            linkChanges.commitChangesToDatabase();
        }

    }

	private static void updateCurrentApprovers(List<Request_for_Project_Approval__c> newRPA, Map<Id, Request_for_Project_Approval__c> oldMapbyID){
		List<Request_for_Project_Approval__c> filteredList =  filterForMissingCurrentApprover(newRPA, oldMapbyID);
        if(!filteredList.isEmpty()){
            updateApprovers(Pluck.ids(filteredList));
        }
	}

    // Uses a future method to handle this because approver data is not set until after trigger runs
    @future
    public static void updateApprovers(Set<Id> rpaIds){
        Map<Id, String> rpaIdtoActorName = RequestForProjectApprovalServices.findCurrentApprovers(rpaIds);
        List<Request_for_Project_Approval__c> rpasForUpdate = new List<Request_for_Project_Approval__c>();
        for(Id rpaId : rpaIdtoActorName.keySet()){
            rpasForUpdate.add(
                new Request_for_Project_Approval__c(
                    Id = rpaId, 
                    Current_Approver__c = rpaIdtoActorName.get(rpaId)
                )
            );
        }
        RPAServicesWithoutSharing.updateRequests(rpasForUpdate);
    }    

	private static List<Request_for_Project_Approval__c> filterForMissingCurrentApprover (List<Request_for_Project_Approval__c> newRPAs, Map<Id, Request_for_Project_Approval__c> oldMap){
		List<Request_for_Project_Approval__c> filteredList = new List<Request_for_Project_Approval__c> ();
		for(Request_for_Project_Approval__c rpa : newRPAs ){
			if(oldMap.get(rpa.Id).Current_Approver__c != rpa.Current_Approver__c && rpa.Current_Approver__c == null){
				filteredList.add(rpa);
			}
		}
		return filteredList;
	}
}