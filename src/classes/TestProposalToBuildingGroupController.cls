@isTest
private class TestProposalToBuildingGroupController {
	public static final Id PRICEBOOK_ID = Test.getStandardPricebookId();

    static List<Account> testAcc;
    static List<Contact> testCon;
    static List<Opportunity> testOpp;
    static Building_Group__c testBuildingGroup;

    static void setup(){
        testAcc = TestUtilities.createAccounts(1, true);
        testCon = TestUtilities.createContacts(testAcc, 1, true);
        testOpp = TestUtilities.createOpportunities(testAcc, 1, true);
        Apttus_Proposal__Proposal__c testProposal = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Opportunity__c=testOpp[0].Id);
        insert testProposal;

        testBuildingGroup = new Building_Group__c(Opportunity__c=testOpp[0].Id);
        insert testBuildingGroup;

	        //Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];

	        Pricebook2 pb = new Pricebook2(Name = 'Standard Price Book', Description = 'Price Book Products', IsActive = true);
	    	insert pb;

	    	Product2 prod = new Product2(Name='Anti-infectives', Family='Best Practices', IsActive=true);
	    	insert prod;

	        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id=PRICEBOOK_ID, Product2Id=prod.Id, UnitPrice=10000, IsActive=true, UseStandardPrice=false);
	        insert standardPrice;

	    	PricebookEntry pbe = new PricebookEntry(Pricebook2Id=pb.Id, Product2Id=prod.Id, UnitPrice=10000, IsActive=true, UseStandardPrice=false);
	    	insert pbe;

		        Apttus_Config2__ClassificationName__c testCategory = new Apttus_Config2__ClassificationName__c(Name='TestCategory', Apttus_Config2__HierarchyLabel__c='TestLabel');
		        insert testCategory;

		        Apttus_Config2__ClassificationHierarchy__c testClassifiation = new Apttus_Config2__ClassificationHierarchy__c(Name='TestClassification', Apttus_Config2__HierarchyId__c=testCategory.Id, Apttus_Config2__Label__c='TestLabel');
		        insert testClassifiation;

				Apttus_Config2__ProductClassification__c testProductClassification = new Apttus_Config2__ProductClassification__c(Apttus_Config2__ClassificationId__c=testClassifiation.Id, Apttus_Config2__ProductId__c=prod.Id);
				insert testProductClassification;

		Apttus_Proposal__Proposal_Line_Item__c testLineItem = new Apttus_Proposal__Proposal_Line_Item__c();
		testLineItem.Apttus_Proposal__Product__c 			=  prod.Id;
		testLineItem.Apttus_QPConfig__ClassificationId__c 	= testClassifiation.Id;
		testLineItem.Apttus_QPConfig__ClassificationId__c 	= testClassifiation.Id;
		testLineItem.Apttus_Proposal__Proposal__c 			= testProposal.Id;
		insert testLineItem;

		Building_Group_Product__c testBGP 	= new Building_Group_Product__c();
		testBGP.Building_Group__c 			= testBuildingGroup.Id;
		testBGP.Proposal_Line_Item__c 		= testLineItem.Id;
		testBGP.Product_lookup__c 			= prod.Id;
		insert testBGP;

		//tempAPLIW.buildingGroupProduct.System__c 				= apli.Apttus_QPConfig__ClassificationId__r.Apttus_Config2__HierarchyId__c;

        Test.setCurrentPageReference(new PageReference('/apex/NewOpportunityFirm')); 
        System.currentPageReference().getParameters().put('id', testOpp[0].Id);           
    }

    @isTest static void TestProposalToBuildingGroupController_Constructor_ValidParameters() {
        setup();
        ApexPages.StandardController standardController = new ApexPages.StandardController(testOpp[0]);
        ProposalToBuildingGroupController controller 	= new ProposalToBuildingGroupController(standardController);

        system.assertEquals(controller.oppRecord.Id, testOpp[0].Id);
    }    

    @isTest static void TestProposalToBuildingGroupController_GetApttusProposalLineItems_ValidParameters() {
        setup();
        ApexPages.StandardController standardController = new ApexPages.StandardController(testOpp[0]);
        ProposalToBuildingGroupController controller 	= new ProposalToBuildingGroupController(standardController);
        controller.GetApttusProposalLineItems();

        system.assertEquals(1, controller.lineItems.size());
    }    

    @isTest static void TestProposalToBuildingGroupController_GetBuildingGroups_ValidParameters() {
        setup();
        ApexPages.StandardController standardController = new ApexPages.StandardController(testOpp[0]);
        ProposalToBuildingGroupController controller 	= new ProposalToBuildingGroupController(standardController);
        controller.GetApttusProposalLineItems();
        List<SelectOption> tempSO = controller.GetBuldingGroups();

        system.assertEquals(2, tempSO.size());
    }    

    @isTest static void TestProposalToBuildingGroupController_GetSelectedBuildingGroupProduct_ValidParameters() {
        setup();
        ApexPages.StandardController standardController = new ApexPages.StandardController(testOpp[0]);
        ProposalToBuildingGroupController controller 	= new ProposalToBuildingGroupController(standardController);
        controller.GetApttusProposalLineItems();
        List<SelectOption> tempSO = controller.GetBuldingGroups();
        controller.selectedBuildingGroupId = testBuildingGroup.Id;
        controller.GetSelectedBuildingGroupProduct();
        
        system.assertEquals(1, controller.bgpList.size());
    }        

    @isTest static void TestProposalToBuildingGroupController_Save_ValidParameters() {
        setup();
        ApexPages.StandardController standardController = new ApexPages.StandardController(testOpp[0]);
        ProposalToBuildingGroupController controller 	= new ProposalToBuildingGroupController(standardController);
        controller.GetApttusProposalLineItems();
        List<SelectOption> tempSO = controller.GetBuldingGroups();
        controller.selectedBuildingGroupId = testBuildingGroup.Id;
        controller.GetSelectedBuildingGroupProduct();
        controller.lineItems[0].isSelected = true;
        controller.Save();

        Building_Group__c tempBG 				= [SELECT Id FROM Building_Group__c WHERE Opportunity__c =:testOpp[0].Id];
        List<Building_Group_Product__c> tempBGP = [SELECT Id, Building_Group__c FROM Building_Group_Product__c WHERE Building_Group__c = :tempBG.Id];

      	system.assertNotEquals(null, tempBG);
      	system.assertEquals(2, tempBGP.size());        
      	system.assertEquals(tempBG.Id, tempBGP[0].Building_Group__c);        
    }         
}