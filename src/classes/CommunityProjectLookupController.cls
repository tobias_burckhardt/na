public with sharing class CommunityProjectLookupController {
     
    public List<Project__C> projList {get; set;}
    public Project__c proj{get;set;}
    public boolean isCreate{get;set;}
    public string selectedName{get;set;}
    public string selectedEmail{get;set;}
    public string selectedPhone{get;set;}
    
    public Project__c createdProject{get; set;}
    public string uid{get; set;}
    public CommunityProjectLookupController(ApexPages.StandardController controller) {
       this.proj = (Project__c)controller.getRecord();
        isCreate = false;
        createdProject = new Project__c();
        //load any project for this user
        loadProjects();
    }

   
    public CommunityProjectLookupController(){
    
       
        isCreate = false;
       
        //load any project for this user
        loadProjects();
    }
    
    private void loadProjects(){
        uid = UserInfo.getUserId();
        try{
            projList = [select Id, name, City__c, State__c from Project__c where Community_User_ID__c = :uid];
        }
        catch(Exception e){
            //projList = new List<Project__c>();
        }
    }
    
    public void selectedProjectList(){
        projList.clear();
        system.debug('uid: '+ uid);
        system.debug('selectedname: '+ selectedName);
        string projQuery;
        if(selectedName != null && selectedName != ''){
            projQuery = 'select Id, name, City__c, State__c from Project__c where Community_User_ID__c =:uid AND name like \'%'+selectedName+'%\'' ;
            projList = database.query(projQuery);
        }
        else{
            projList = [select Id, name, City__c, State__c from Project__c where Community_User_ID__c = :uid];
        }
        system.debug('projList: '+projList);
        }
        
    public void saveProject(){
        
                this.proj.Community_User_ID__c = uid;
                
                upsert this.proj;
                isCreate = true;
                loadProjects();
                  
    }

}