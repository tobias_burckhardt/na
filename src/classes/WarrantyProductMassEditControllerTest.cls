@isTest 
public class WarrantyProductMassEditControllerTest {
    static testMethod void TestWarrantyProductMassEditController() {

        Warranty__c w1 = new Warranty__c();
        insert w1;
        
        Product2 p1 = new Product2();
        p1.name = 'test';
        p1.Max_Warranty_Term__c = 3;
        insert p1;
        
        Warranty_Line__c wl1 = new Warranty_Line__c();
        wl1.Warranty__c = w1.id;
        wl1.Quantity__c = 2;
        wl1.Product__c = p1.id;
        wl1.Warranty_Term_Years__c = '4';
        insert wl1;

        test.startTest();        
        WarrantyProductMassEditController wpmec1 = new WarrantyProductMassEditController();
        wpmec1.saveWarrantyLines();       
        ApexPages.currentPage().getParameters().put('wid', w1.id);

        WarrantyProductMassEditController wpmec2 = new WarrantyProductMassEditController();
        wpmec2.saveWarrantyLines();
        wpmec2.clearvalues();
        test.stopTest();
    }
}