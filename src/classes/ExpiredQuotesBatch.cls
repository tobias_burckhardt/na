public with sharing virtual class ExpiredQuotesBatch implements Database.Batchable<sObject>, Schedulable {
    private Update_Price_List_Assignment__c priceList = Update_Price_List_Assignment__c.getInstance();
    private String priceListName = priceList == null ? '' : priceList.Price_List_Name__c;

    private String query = 'SELECT Id ' +
                           '  FROM Apttus_Proposal__Proposal__c ' +
                           ' WHERE Apttus_Proposal__Proposal_Expiration_Date__c < TODAY ' +
                           ' AND Apttus_QPConfig__PriceListId__r.Name != :priceListName ' +
                           ' AND Apttus_Proposal__Opportunity__r.NOA_Created__c = FALSE ' +
                           ' AND ( ' +
                           '       Apttus_Proposal__Opportunity__r.AccountId = NULL ' +
                           '       OR (Apttus_Proposal__Opportunity__r.AccountId != NULL AND ' +
                           '           Apttus_Proposal__Opportunity__r.Account.PTS_Legacy_ID__c != NULL) ' +
                           ' )';
    private Date expirationDate = Date.today();
    private String errors = '';
    private Apttus_Config2__PriceList__c updatedPriceList;
    private static Integer BATCH_SIZE = 20;// Apttus too many queries exception

    public ExpiredQuotesBatch() {
        updatedPriceList = [SELECT Id FROM Apttus_Config2__PriceList__c WHERE Name = :priceListName];
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Apttus_Proposal__Proposal__c> quotesToUpdate = updateExpiredQuotes(scope);
        Map<Id, Apttus_Proposal__Proposal__c> proposals = new Map<Id, Apttus_Proposal__Proposal__c>(quotesToUpdate);
        List<Apttus_Proposal__Proposal__c> expiredQuotesUpdated = new List<Apttus_Proposal__Proposal__c>();

        if( !quotesToUpdate.isEmpty() ) {
            List<Database.SaveResult> dmlResults = Database.update( quotesToUpdate, false );

            addErrors(dmlResults, 'Apttus_Proposal__Proposal__c failures');

            for( Database.SaveResult dmlResult : dmlResults ) {
                if(dmlResult.isSuccess()) {
                    expiredQuotesUpdated.add(proposals.get(dmlResult.getId()));
                }

            }
            List<Apttus_Config2__ProductConfiguration__c> productConfigsToUpdate = updateProductConfiguration(expiredQuotesUpdated);
            if(!productConfigsToUpdate.isEmpty()){
                addErrors( Database.update( productConfigsToUpdate, false ), 'Apttus_Config2__ProductConfiguration__c failures');
            }

        }
    }

    private void addErrors(List<Database.SaveResult> dmlResults, String errorPrefix){
        for( Database.SaveResult dmlResult : dmlResults ) {
            if( !dmlResult.isSuccess() ) {
                errors += errorPrefix + ' \n';
                for( Database.Error err: dmlResult.getErrors() )  {
                    errors += err.getMessage() + ' \n';
                }
            }
        }
    }

    public void execute( SchedulableContext sc ) {
        ExpiredQuotesBatch quotesBatch = new ExpiredQuotesBatch();
        Database.executeBatch( quotesBatch, BATCH_SIZE );
    }
    
    public void finish(Database.BatchableContext context) {
        if( String.isEmpty( errors ) ) return;

        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                             FROM AsyncApexJob
                           WHERE Id =:context.getJobId()
                           ];

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
     
        mail.setToAddresses( new List<String>{ job.CreatedBy.Email } );
        mail.setSubject('ATTENTION REQUIRED: Failed to Update Expired Quotes' );
     
        String emailBody = 'The following errors occured:\n'+ errors;
        mail.setPlainTextBody(emailBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

    public List<Apttus_Proposal__Proposal__c> updateExpiredQuotes(List<Apttus_Proposal__Proposal__c> expiredQuotes){
        if(updatedPricelist == null){
            return new List<Apttus_Proposal__Proposal__c>();
        }

        List<Apttus_Proposal__Proposal__c> toUpdateQuotes = new List<Apttus_Proposal__Proposal__c>();

        for(Apttus_Proposal__Proposal__c quote : expiredQuotes){
            quote.Apttus_QPConfig__PriceListId__c = updatedPricelist.Id;
            quote.Configurations_from_Reprice_Quote__c = 0;
            toUpdateQuotes.add(quote);
        }
         return toUpdateQuotes;
    }

    protected List<Apttus_Config2__ProductConfiguration__c> updateProductConfiguration(List<Apttus_Proposal__Proposal__c> updatedQuotes){
        Map<Id, Apttus_Proposal__Proposal__c> quoteById = new Map<Id, Apttus_Proposal__Proposal__c>(updatedQuotes);
        List<Apttus_Config2__ProductConfiguration__c> configs = [SELECT Id, Apttus_QPConfig__Proposald__c
                                                                    FROM Apttus_Config2__ProductConfiguration__c
                                                                 WHERE Apttus_QPConfig__Proposald__c in :quoteById.keySet()];

        for(Apttus_Config2__ProductConfiguration__c config : configs){
            Apttus_Proposal__Proposal__c quote = quoteById.get(config.Apttus_QPConfig__Proposald__c);
            config.Apttus_Config2__EffectivePriceListId__c = quote.Apttus_QPConfig__PriceListId__c;
            config.Apttus_Config2__PriceListId__c = quote.Apttus_QPConfig__PriceListId__c;
        }

        return configs;
    }
}