/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof.
 * Edited by Trifecta Technologies (SS) 2014-12-23, to allow for the use of a custom start page.
 * Current use is for the creation of the SCC Zone Communities site, so we're pointing to the SCC Zone User Homepage.
 * As other Comunities sites are added, this code will have to be modified to determine which start page the user should
 * be redirected to.
 * An upcoming Salesforce release will enable this functionality through the Communities Admin UI, but, for now, the established
 * way of handling this is by editing this controller.
 */ 
 
public with sharing class CommunitiesLandingController {
    
    // ** To point to a different Visualforce start page, change the value below to the name of the new page ** /
	private String SCCZoneStartPage = 'sika_SCCZoneDisclaimer';
//private String SCCZoneStartPage = 'sika_SCCZoneUserHomePage';

	//public CommunitiesLandingController() {}

    public PageReference forwardToStartPage() {
        return Network.communitiesLanding();
    }

    // New code added by Trifecta (SS) 2014-12-23, to allow for the use of a custom start page for SCC Zone
    // Instead of calling forwardToStartPage() on page load, the CommunitiesLandingPage will call the forwardToCustomStartPage() method below. 
    public PageReference forwardToCustomStartPage() {
    	return new PageReference('/' + SCCZoneStartPage);
//return null;
	}
}