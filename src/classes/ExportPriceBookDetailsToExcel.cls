public class ExportPriceBookDetailsToExcel {
    public List<PriceBookEntryWrapper> pricebookEntriesWrapperList {get;set;}
    public String pricebookId{get;set;}
    public ExportPriceBookDetailsToExcel(){
    	    Map<String, String> paramMap=ApexPages.currentPage().getParameters();
        	pricebookId = paramMap.get('pricebookId');
        	List<PricebookEntry> pricebookEntries = [Select Id, Pricebook2Id, IsActive,Pricebook2.Name,Pricebook2.Application__c,Pricebook2.Condition_Table__c,Pricebook2.Condition_Type__c,Pricebook2.Distribution__c,Pricebook2.Division__c,Pricebook2.Price_List_Type__c,Pricebook2.Sales_Org__c,Pricebook2.isSAPRelated__c,Base_Price__c,Base_Scale_Quantity__c,Currency__c,End_Date__c,isToIncludeInPriceBookUpload__c,Pricing_Unit__c,Scale_Quantity_1__c,Scale_Quantity_10__c,Scale_Quantity_2__c,Scale_Quantity_3__c,Scale_Quantity_4__c,Scale_Quantity_5__c,Scale_Quantity_6__c,Scale_Quantity_7__c,Scale_Quantity_8__c,Scale_Quantity_9__c,Scale_Rate_1__c,Scale_Rate_10__c,Scale_Rate_2__c,Scale_Rate_3__c,Scale_Rate_4__c,Scale_Rate_5__c,Scale_Rate_6__c,Scale_Rate_7__c,Scale_Rate_8__c,Scale_Rate_9__c,Scale_UOM__c,Start_Date__c,UnitPrice,Product2.Id, Product2.Name,Product2.ProductCode,Product2.IsActive,Price_UOM__c, Product2.Sales_Unit__c,Product2.Article_code__c from PricebookEntry where Pricebook2Id=:pricebookId and isToIncludeInPriceBookUpload__c=true];
        	// and IsActive=true and Product2.IsActive=true	
        	pricebookEntriesWrapperList = new List<PriceBookEntryWrapper>();    
        	for(PricebookEntry pricebookEntryOb:pricebookEntries){
                PriceBookEntryWrapper pricebookEntryWrapper = new PriceBookEntryWrapper();
            
            	pricebookEntryWrapper.conditionTable = pricebookEntryOb.Pricebook2.Condition_Table__c;
                pricebookEntryWrapper.application = pricebookEntryOb.Pricebook2.Application__c;
                pricebookEntryWrapper.conditionType = pricebookEntryOb.Pricebook2.Condition_Type__c;
                pricebookEntryWrapper.salesOrg = pricebookEntryOb.Pricebook2.Sales_Org__c;
                pricebookEntryWrapper.distribution = pricebookEntryOb.Pricebook2.Distribution__c;
                pricebookEntryWrapper.division = pricebookEntryOb.Pricebook2.Division__c;
                pricebookEntryWrapper.pricelistType = pricebookEntryOb.Pricebook2.Price_List_Type__c;
                pricebookEntryWrapper.productCode = String.isNotBlank(pricebookEntryOb.Product2.Article_code__c) ? pricebookEntryOb.Product2.Article_code__c.substringAfter('_') : '';
				Date startDate = pricebookEntryOb.Start_Date__c;
                if(startDate!=null){
                  	String startMonth = startDate.month()>9 ? ''+startDate.month():'0'+startDate.month();
                	pricebookEntryWrapper.startDate = startDate.day()+'.'+ startMonth +'.'+startDate.year();  
                }
                Date endDate = pricebookEntryOb.End_Date__c;
                if(endDate!=null){
                    String endMonth = endDate.month()>9 ? ''+endDate.month():'0'+endDate.month();
                	pricebookEntryWrapper.endDate = endDate.day()+'.'+endMonth+'.'+endDate.year();  
                }       
                pricebookEntryWrapper.scaleUOM = pricebookEntryOb.Scale_UOM__c;
                pricebookEntryWrapper.baseScaleQty = pricebookEntryOb.Base_Scale_Quantity__c;
                pricebookEntryWrapper.basePrice = pricebookEntryOb.Base_Price__c;
                pricebookEntryWrapper.currencyRate = pricebookEntryOb.Currency__c;
                pricebookEntryWrapper.pricingUnit = pricebookEntryOb.Pricing_Unit__c;
                pricebookEntryWrapper.priceUOM = 0.0;//pricebookEntryOb.Price_UOM__c;
                pricebookEntryWrapper.priceUOMScale = pricebookEntryOb.Price_UOM__c;
                pricebookEntryWrapper.scaleQty1 = pricebookEntryOb.Scale_Quantity_1__c;
                pricebookEntryWrapper.scaleRate1 = pricebookEntryOb.Scale_Rate_1__c;
                pricebookEntryWrapper.scaleQty2 = pricebookEntryOb.Scale_Quantity_2__c;
                pricebookEntryWrapper.scaleRate2 = pricebookEntryOb.Scale_Rate_2__c;
                pricebookEntryWrapper.scaleQty3 = pricebookEntryOb.Scale_Quantity_3__c;
                pricebookEntryWrapper.scaleRate3 = pricebookEntryOb.Scale_Rate_3__c;
                pricebookEntryWrapper.scaleQty4 = pricebookEntryOb.Scale_Quantity_4__c;
                pricebookEntryWrapper.scaleRate4 = pricebookEntryOb.Scale_Rate_4__c;
                pricebookEntryWrapper.scaleQty5 = pricebookEntryOb.Scale_Quantity_5__c;
                pricebookEntryWrapper.scaleRate5 = pricebookEntryOb.Scale_Rate_5__c;
                pricebookEntryWrapper.scaleQty6 = pricebookEntryOb.Scale_Quantity_6__c;
                pricebookEntryWrapper.scaleRate6 = pricebookEntryOb.Scale_Rate_6__c;
                pricebookEntryWrapper.scaleQty7 = pricebookEntryOb.Scale_Quantity_7__c;
                pricebookEntryWrapper.scaleRate7 = pricebookEntryOb.Scale_Rate_7__c;
                pricebookEntryWrapper.scaleQty8 = pricebookEntryOb.Scale_Quantity_8__c;
                pricebookEntryWrapper.scaleRate8 = pricebookEntryOb.Scale_Rate_8__c;
                pricebookEntryWrapper.scaleQty9 = pricebookEntryOb.Scale_Quantity_9__c;
                pricebookEntryWrapper.scaleRate9 = pricebookEntryOb.Scale_Rate_9__c;
                pricebookEntryWrapper.scaleQty10 = pricebookEntryOb.Scale_Quantity_10__c;
                pricebookEntryWrapper.scaleRate10 = pricebookEntryOb.Scale_Rate_10__c;
                pricebookEntryWrapper.priceBookId = pricebookEntryOb.Pricebook2Id;
                pricebookEntryWrapper.priceBookEntryId = pricebookEntryOb.Id;
                pricebookEntryWrapper.productId = pricebookEntryOb.Product2.Id;
                pricebookEntriesWrapperList.add(pricebookEntryWrapper);
            }
    }
}