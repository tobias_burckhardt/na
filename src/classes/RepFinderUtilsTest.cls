@isTest
private class RepFinderUtilsTest {
	@isTest
	private static void testGetPicklistEntries() {
		Schema.DescribeFieldResult dfr = Region__c.Region_State__c.getDescribe();
		List<Schema.PicklistEntry> entries = dfr.getPicklistValues();
		List<String> options = RepFinderUtils.getPicklistEntries(dfr);

		System.assertNotEquals(null, options, 'Should have a list of entries');
		System.assertNotEquals(0, options.size(), 'Should have some options');
		System.assertEquals(entries.size(), options.size(), 'Number of entries should match');
	}

	@isTest
	private static void testCanonicalPhone() {
		System.assertEquals('', RepFinderUtils.getCanonicalPhone(null));
		System.assertEquals('', RepFinderUtils.getCanonicalPhone(''));
		System.assertEquals('', RepFinderUtils.getCanonicalPhone('abc'));
		System.assertEquals('16105307200', RepFinderUtils.getCanonicalPhone('+1 (610) 530-7200'));
		System.assertEquals('6105307200', RepFinderUtils.getCanonicalPhone('(610) 530-7200'));
		System.assertEquals('5307200', RepFinderUtils.getCanonicalPhone('530-7200'));
		System.assertEquals('5307200', RepFinderUtils.getCanonicalPhone('5307200'));
	}
}