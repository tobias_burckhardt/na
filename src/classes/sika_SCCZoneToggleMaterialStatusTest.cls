@isTest
private class sika_SCCZoneToggleMaterialStatusTest {

	@isTest private static void theTest() {

		// ...create & insert a material.  (Make sure it's owned by the runAs user -- a salesperson.)
		map<String, sObject> objects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
		User salesUser = (User) objects.get('salesUser');
		Material__c theMaterial = sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'type' => 'sand', 'ownerId' => salesUser.Id});
		
		system.runAs(salesUser){
			insert(theMaterial);
		}
	
		// confirm that theMaterial is active
		system.assertEquals(theMaterial.isActive__c, true);
		
		system.runAs(salesUser){

			// deactivate the material
			sika_SCCZoneToggleMaterialStatus.activateDeactivateMaterial(theMaterial.Id, 'deactivate');
			
			// query for the material
			Material__c testMaterial = [SELECT OwnerId, isActive__c FROM Material__c WHERE OwnerId = :salesUser.Id];
			
			// confirm that the material is now inactive
			system.assertEquals(testMaterial.isActive__c, false);
			
			// re-activate the material
			sika_SCCZoneToggleMaterialStatus.activateDeactivateMaterial(theMaterial.Id, 'activate');
			
			// query for the material
			testMaterial = [SELECT OwnerId, isActive__c FROM Material__c WHERE OwnerId = :salesUser.Id];
			
			// confirm that the material is active again
			system.assertEquals(testMaterial.isActive__c, true);
			
		}
	}

}