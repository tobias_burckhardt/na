@isTest
public class PricingApexTriggerTest {
    static testMethod void testFunctionalitiesPartOne(){
        Id accountId = PricingTestDataFactory.createAccount();
        Id contactId = PricingTestDataFactory.createPrimaryContactForAccount(accountId);
        Id targetMarketId = PricingTestDataFactory.createTargetMarket(accountId);
        Id customPricebookId = PricingTestDataFactory.createCPBWithTargetMarket(targetMarketId);
        List<Product2> products = PricingTestDataFactory.createProducts();
        List<String> productIds = new List<String>();
        for(Product2 product:products){
            productIds.add(product.Id);
        }
        Boolean areProductsAddedToStdPriceBook = PricingTestDataFactory.addProductsToStandardPriceBook(productIds);
        //System.debug('areProductsAddedToStdPriceBook--'+areProductsAddedToStdPriceBook);
        List<PricebookEntry> insertedPricebookEntries = PricingTestDataFactory.addProductsToCustomPriceBook(productIds,customPricebookId);
        Id accountPriceBookEntryId = PricingTestDataFactory.createAccountPriceBookEntry(accountId,contactId,customPricebookId);
        Test.startTest();
        PricingTestDataFactory.createSPAOpportunity(accountId);
        Test.stopTest();
    }
    static testMethod void testFunctionalitiesPartTwo(){
        Id accountId = PricingTestDataFactory.createAccount();
        Id contactId = PricingTestDataFactory.createPrimaryContactForAccount(accountId);
        Id targetMarketId = PricingTestDataFactory.createTargetMarket(accountId);
        Id customPricebookId = PricingTestDataFactory.createCPBWithTargetMarket(targetMarketId);
        List<Product2> products = PricingTestDataFactory.createProducts();
        List<String> productIds = new List<String>();
        for(Product2 product:products){
            productIds.add(product.Id);
        }
        Boolean areProductsAddedToStdPriceBook = PricingTestDataFactory.addProductsToStandardPriceBook(productIds);
        //System.debug('areProductsAddedToStdPriceBook--'+areProductsAddedToStdPriceBook);
        List<PricebookEntry> insertedPricebookEntries = PricingTestDataFactory.addProductsToCustomPriceBook(productIds,customPricebookId);
        Id accountPriceBookEntryId = PricingTestDataFactory.createAccountPriceBookEntry(accountId,contactId,customPricebookId);
        Test.startTest();
        PricingTestDataFactory.createSPQOpportunity(accountId);
        Test.stopTest();
    }
    static testMethod void testFunctionalitiesPartThree(){
        Id accountId = PricingTestDataFactory.createAccount();
        Id contactId;// = PricingTestDataFactory.createPrimaryContactForAccount(accountId);
        Id targetMarketId = PricingTestDataFactory.createTargetMarket(accountId);
        Id customPricebookId = PricingTestDataFactory.createCPBWithTargetMarket(targetMarketId);
        List<Product2> products = PricingTestDataFactory.createProducts();
        List<String> productIds = new List<String>();
        for(Product2 product:products){
            productIds.add(product.Id);
        }
        Boolean areProductsAddedToStdPriceBook = PricingTestDataFactory.addProductsToStandardPriceBook(productIds);
        List<PricebookEntry> insertedPricebookEntries = PricingTestDataFactory.addProductsToCustomPriceBook(productIds,customPricebookId);
        List<String> pricebookEntryIds = new List<String>();
        for(PricebookEntry pricebookEntryObj:insertedPricebookEntries){
            pricebookEntryIds.add(pricebookEntryObj.Id);
        }
        Id accountPriceBookEntryId = PricingTestDataFactory.createAccountPriceBookEntry(accountId,contactId,customPricebookId);
        Id opportunityId = PricingTestDataFactory.createSPAOpportunity(accountId);
        Id spaQuoteId = PricingTestDataFactory.createSPAQuote(opportunityId);
        Test.startTest();
        PricingTestDataFactory.deleteOpportunity(opportunityId); 
        List<QuoteLineItem> quotelineItems = PricingTestDataFactory.createSPAOppAndQuoteWithLineItems(accountId,customPricebookId,insertedPricebookEntries);
        delete quotelineItems;
        Test.stopTest();
    }
    static testMethod void testFunctionalitiesPartFour(){
        Id accountId = PricingTestDataFactory.createAccount();
        Id contactId;// = PricingTestDataFactory.createPrimaryContactForAccount(accountId);
        Id targetMarketId = PricingTestDataFactory.createTargetMarket(accountId);
        Id customPricebookId = PricingTestDataFactory.createCPBWithTargetMarket(targetMarketId);
        List<Product2> products = PricingTestDataFactory.createProducts();
        List<String> productIds = new List<String>();
        for(Product2 product:products){
            productIds.add(product.Id);
        }
        Boolean areProductsAddedToStdPriceBook = PricingTestDataFactory.addProductsToStandardPriceBook(productIds);
        List<PricebookEntry> insertedPricebookEntries = PricingTestDataFactory.addProductsToCustomPriceBook(productIds,customPricebookId);
        List<String> pricebookEntryIds = new List<String>();
        for(PricebookEntry pricebookEntryObj:insertedPricebookEntries){
            pricebookEntryIds.add(pricebookEntryObj.Id);
        }
        Id accountPriceBookEntryId = PricingTestDataFactory.createAccountPriceBookEntry(accountId,contactId,customPricebookId);
        Id opportunityId = PricingTestDataFactory.createSPAOpportunity(accountId);
        Id spaQuoteId = PricingTestDataFactory.createSPAQuote(opportunityId);
        Test.startTest();
        PricingTestDataFactory.addAttachmentToParent(spaQuoteId);
        PricingTestDataFactory.deleteOpportunity(opportunityId); 
        Test.stopTest();
    }
    static testMethod void testFunctionalitiesPartFive(){
        Id accountId = PricingTestDataFactory.createAccount();
        Id targetMarketId = PricingTestDataFactory.createTargetMarket(accountId);
        Id customPricebookId = PricingTestDataFactory.createCPBWithTargetMarket(targetMarketId);
        List<Product2> products = PricingTestDataFactory.createProducts();
        List<String> productIds = new List<String>();
        for(Product2 product:products){
            productIds.add(product.Id);
        }
        Boolean areProductsAddedToStdPriceBook = PricingTestDataFactory.addProductsToStandardPriceBook(productIds);
        List<PricebookEntry> insertedPricebookEntries = PricingTestDataFactory.addProductsToCustomPriceBook(productIds,customPricebookId);
        List<String> pricebookEntryIds = new List<String>();
        for(PricebookEntry pricebookEntryObj:insertedPricebookEntries){
            pricebookEntryIds.add(pricebookEntryObj.Id);
        }
        Id accountPriceBookEntryId = PricingTestDataFactory.createAccountPriceBookEntry(accountId,null,customPricebookId);
        Test.startTest();
        List<QuoteLineItem> quotelineItems = PricingTestDataFactory.createSPAOppAndQuoteWithLineItems(accountId,customPricebookId,insertedPricebookEntries);
        String quoteId;
        for(QuoteLineItem quoteLineItemObj:quotelineItems){
            quoteId = quoteLineItemObj.quoteId;
            break;
        }
        PricingTestDataFactory.updateSPAQuote(quoteId);
        Test.stopTest();
    }
}