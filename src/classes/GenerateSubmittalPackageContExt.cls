/**
* @author Pavel Halas
* @company Bluewolf, an IBM Company
* @date 6/2016
* 
* Controller extension that handles GenerateSubmittalPackage.page remote calls
* from submittal_js (static resource) providing documents metadata and content. 
*/
public class GenerateSubmittalPackageContExt {
    private Id recordId {get;set;}

    public GenerateSubmittalPackageContExt(ApexPages.StandardController stdCont) {
        recordId = stdCont.getId();
        // nothing
    }
 
    public PageReference Cancel(){
        PageReference pageRef = new PageReference('/'+ recordId);
        pageRef.setRedirect(true);
        return pageRef;
    }

    public void NothingMethod(){
    }

    @RemoteAction
	public static List<ContentVersion> getMetadata(String buildingGroupId) {
        return ContentService.getDocumentMetadataForBuildingGroup(buildingGroupId);
	}
    
    @RemoteAction
	public static List<ContentService.ContentVersionWrapper> getData(List<String> documentIds) {
        return ContentService.getDocumentsContent(documentIds);
    }

}