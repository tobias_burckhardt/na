public without sharing class CommunityForgotUsernameController{

    public String emailAddress {get; set;}
    public String success {get; set;}
    public Boolean submitted {get; set;}
    public String boxText {get; set;}
    public Boolean displayPopUp {get; set;}
    public String caseEmail{get; set;}
    public String caseDesc {get; set;}
    public Boolean hasError {get; private set;}
    public String errorText {get; private set;}

    @TestVisible private static String emailNotRecognizedErrorMsg  = 'The email address you entered was not recognized. Please try again.';
    @TestVisible private static String invalidEmailErrorMsg = 'Please enter a valid email address.';
    private static final List<String> PROFILES = new List<String>{'Warranty Community User', 'Warranty Customer Community Login'};
    
    public CommunityForgotUsernameController() {
        displayPopUp = false;
        success = ApexPages.currentPage().getParameters().get('success');

        if(success != null && Boolean.valueOf(success) == true){
            submitted = true;
            boxText = 'Thank you! You should receive an email containing your username shortly.';
        } else {
            submitted = false;
            boxText = 'Please enter the email address associated with your account:';
        }
    }
    
    public void closePopup() {        
        displayPopup = false;    
    } 
        
    public void showPopup() {        
        displayPopup = true;    
    }

    public PageReference submitRequest(){
        List<User> theUsers = validateForm();
        if(theUsers != null){
            if(theUsers.size() > 0){

                // email username to user
                String theSubject = 'Your RSB Warranty username -- Sika.com';
                String theBody;
                if(theUsers.size() > 1) {
                    theBody = 'Hello RSB Warranty user!\n\n';
                    theBody += 'Below you will find the RSB Warranty username associated with your email address. Please let us know if you still have trouble logging in.\n\n\n';
                    for(User u : theUsers){
                        theBody += u.UserName + '\n';
                    }
                } else {
                    theBody = theUsers[0].FirstName + ',\n\n';
                    theBody += 'Below you will find your username for the RSB Warranty site. Please let us know if you still have trouble logging in.\n\n\n';
                    theBody += 'Username: ' + theUsers[0].UserName; 
                }
                Messaging.SingleEmailMessage theEmail = new Messaging.SingleEmailMessage(); 
                theEmail.setSubject(theSubject);
                theEmail.setToAddresses(new List<String>{theUsers[0].Email});
                theEmail.setPlainTextBody(theBody);

                Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{theEmail});

                // return to page and display success message
                PageReference thePage = new PageReference(ApexPages.currentPage().getUrl());
                thePage.getParameters().put('success', 'true');
                thePage.setRedirect(true);
                return thePage;
            }
        }
        return null;

    }
    
    public void submitCase(){
        Case theCase = new Case();
        theCase.Description = caseDesc;
        theCase.Status = 'New';
        theCase.Due_Date__c = Datetime.now();
        //theCase.Origin = 'Web';
        theCase.Origin = 'RSB Warranty Community';
        theCase.Type = 'Website Help';
        theCase.Priority = 'Medium';
        theCase.Subject = 'Website Help request from ' + caseEmail + ' :: (forgot username page)';
        theCase.Customer_Email__c = caseEmail;
        theCase.Community_Warranty__c= 'Website Help';
        theCase.Do_not_send_auto_response__c = true;
        
        List<Contact> u = [SELECT ID, Name, Email FROM Contact WHERE Email =: caseEmail LIMIT 1];
        if(u.size() == 1){
          theCase.ContactID = u[0].ID;
        }

        Database.DMLOptions options = new Database.DMLOptions();
        options.assignmentRuleHeader.useDefaultRule = true;
        theCase.setOptions(options);
    
        insert(theCase);
        displayPopup = false;   
    }

    private List<User> validateForm (){
        hasError = false;
        errorText = '';

        if(!WaranttyValidationHelper.validateEmail(emailAddress) || emailAddress == null){
            hasError = true;
            errorText = invalidEmailErrorMsg;

            return null;
        }

        List<Profile> warrantyCommProfiles = [SELECT Id FROM Profile WHERE Name IN :PROFILES];
        
        List<User> users = [SELECT FirstName, Email, UserName, ProfileId FROM User WHERE email = :emailAddress AND ProfileId IN :warrantyCommProfiles];
        if(users.size() > 0){
            return users;
        }
        hasError = true;
        errorText = emailNotRecognizedErrorMsg;
        return null;
    }
}