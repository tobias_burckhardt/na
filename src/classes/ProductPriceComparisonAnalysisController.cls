global class ProductPriceComparisonAnalysisController {
    public String selectedLang{get;set;}
    public Boolean isMobile { get; set; }
    public String accId{get;set;}
    public String productCode{get;set;}
    public String quoteLineItemId{get;set;}
    public String quoteId{get;set;}
    public ProductPriceComparisonAnalysisController(){
        selectedLang = UserInfo.getLanguage();
        isMobile = UserInfo.getUiTheme() == 'Theme4t';
        Map<String, String> paramMap=ApexPages.currentPage().getParameters();
        accId = paramMap.get('Id');
        productCode = paramMap.get('pc');
        quoteLineItemId = paramMap.get('qli');
        quoteId = paramMap.get('quoteId');
    }
    @RemoteAction
    global static String getQuoteLineItemInfo(String dataFields){
        Map<String,Object> responseMap = new Map<String,Object>();
        Map<String,Object> request = (Map<String,Object>)JSON.deserializeUntyped(dataFields);
        String quoteLineItemId = (String)request.get('quoteLineItemId');
        try{
			QuoteLineItem quoteLineItemObj = [Select Id,Product2.Name,ListPrice,UnitPrice,Article__c,Quantity,Quote.Name,Quote.RecordType.DeveloperName from QuoteLineItem where Id=:quoteLineItemId];
            PriceComparisonWrapper productPriceComparisonWrapper = new PriceComparisonWrapper();
            productPriceComparisonWrapper.productName = quoteLineItemObj.Product2.Name;
            productPriceComparisonWrapper.articleCode = quoteLineItemObj.Article__c;
            productPriceComparisonWrapper.listPrice = String.valueOf(quoteLineItemObj.ListPrice);
            productPriceComparisonWrapper.salesPrice = String.valueOf(quoteLineItemObj.UnitPrice);
            productPriceComparisonWrapper.quantity = String.valueOf(quoteLineItemObj.Quantity);
            productPriceComparisonWrapper.quoteLineItemId = quoteLineItemObj.Id;
            productPriceComparisonWrapper.quoteName = quoteLineItemObj.Quote.Name;
            productPriceComparisonWrapper.isSPAQuote = quoteLineItemObj.Quote.RecordType.DeveloperName =='SPA_Pricing'?true:false;
			responseMap.put('productPriceComparisonWrapper',productPriceComparisonWrapper);
			responseMap.put('status',true);            
        }catch(Exception e){
            responseMap.put('status',false);
        }
        return JSON.serializePretty(responseMap);
    }
    @RemoteAction
    global static String upsertQuoteLineItems(String dataFields){
        Map<String,Object> dataToBeSentBack = new Map<String,Object>();
        List<Object> dataList =  (List<Object>)JSON.deserializeUntyped(dataFields);
        List<OpportunityLineItem> opportunityLineItemsToUpdate = new List<OpportunityLineItem>();
        Map<String,String> quoteLineItemsMap = new Map<String,String>();
        String quantity,salesPrice,quoteLineItemId;
        for(Object ob:dataList){
            Map<String,Object> fieldObject = (Map<String,Object>)ob;
            quantity = (String)fieldObject.get('quantity');
            salesPrice = (String)fieldObject.get('salesPrice');
            quoteLineItemId = (Id)fieldObject.get('quoteLineItemId');
            break;
		}
		QuoteLineItem quoteLineItemObj = [Select Id,quantity,UnitPrice,TotalPrice from QuoteLineItem where Id =:quoteLineItemId];
        quoteLineItemObj.Quantity = Integer.valueOf(quantity);
        quoteLineItemObj.UnitPrice = Decimal.valueOf(salesPrice);
        try{
            update quoteLineItemObj;
            dataToBeSentBack.put('status',true);
        }catch(Exception e){
            dataToBeSentBack.put('status',false);
            System.debug('exception caught : '+e.getCause());
        }
        return JSON.serializePretty(dataToBeSentBack);
    }
    @ReadOnly
	@RemoteAction
    global static String getAnalyticsData(String dataFields){
        Map<String,Object> responseMap = new Map<String,Object>();
        Map<String,Object> request = (Map<String,Object>)JSON.deserializeUntyped(dataFields);
        String productCode = (String)request.get('productCode');
        String accId = (String)request.get('accId');
        String recordType = (String)request.get('recordType');
        String quoteId = (String)request.get('quoteId');
        String productPriceComparisonReport = 'Sika_Pricing_Product_Price_Comparison';
        try{
            // Get the report ID
            List <Report> reportList = [SELECT Id,DeveloperName FROM Report where 
                DeveloperName =:productPriceComparisonReport];
            System.debug('reportList--'+reportList);
            String reportId = (String)reportList.get(0).get('Id');
            System.debug('reportId--'+reportId);
            Account account = [Select Id,Name from Account where Id=:accId];
            System.debug('account--'+account);
            Reports.ReportMetadata reportMetaDataObj = new Reports.ReportMetadata();
            
            // SET UP REPORT FILTERS
            List<Reports.ReportFilter> reportFilters = new List<Reports.ReportFilter>();
            
            if(String.isNotBlank(productCode)){
                Reports.ReportFilter reportFilterObj = new Reports.ReportFilter();
                reportFilterObj.setColumn('QLI.PRODUCT.PRODUCT_CODE'); 
                reportFilterObj.setOperator('equals');
                reportFilterObj.setValue(productCode);
                reportFilters.add(reportFilterObj);
            }
            Reports.ReportFilter reportFilterObjForRecordType = new Reports.ReportFilter();
            reportFilterObjForRecordType.setColumn('RECORDTYPE'); 
            reportFilterObjForRecordType.setOperator('equals');
            reportFilterObjForRecordType.setValue(recordType);
            reportFilters.add(reportFilterObjForRecordType);
            
            /*Reports.ReportFilter reportFilterObjForStatus = new Reports.ReportFilter();
            List<String> quoteStatus = new List<String>{'Approved'};
            reportFilterObjForStatus.setColumn('Q.STATUS'); 
            reportFilterObjForStatus.setOperator('equals');
            reportFilterObjForStatus.setValue('Approved');
            reportFilters.add(reportFilterObjForStatus);*/
            
            Reports.ReportFilter reportFilterObjForId = new Reports.ReportFilter();
            reportFilterObjForId.setColumn('QUOTE_ID'); 
            reportFilterObjForId.setOperator('notEqual');
            reportFilterObjForId.setValue(quoteId);
            reportFilters.add(reportFilterObjForId);
            
            Reports.ReportFilter reportFilterObjForAccount = new Reports.ReportFilter();
            
            reportFilterObjForAccount.setColumn('ACCOUNT.NAME'); 
            reportFilterObjForAccount.setOperator('equals');
            reportFilterObjForAccount.setValue(account.Name);
            reportFilters.add(reportFilterObjForAccount);
            
            reportMetaDataObj.setReportFilters(reportFilters);
            // SET UP DETAIL COLUMNS
            List<String> detailColumns = new List<String>{'QUOTE_NAME', 'QLI.QUANTITY', 'QLI.LIST_PRICE', 'QLI.UNIT_PRICE', 'QLI.TOTAL_PRICE','OPPORTUNITY.NAME','Q.EXPIRATION_DATE','Q.CREATED_DATE','RECORDTYPE','Q.STATUS'};
            reportMetaDataObj.setDetailColumns(detailColumns);
            // INCLUDE ONLY THOSE HAVE DETAIL ROWS
            reportMetaDataObj.setHasDetailRows(true);
            // INCLUDE THE RECORD COUNT
            reportMetaDataObj.setHasRecordCount(true);
            
            Reports.reportResults results = Reports.ReportManager.runReport(reportId,reportMetaDataObj,true);
            System.debug('results--'+results);
            Reports.ReportFactWithDetails factDetailsObjs = (Reports.ReportFactWithDetails)results.getFactMap().get('T!T');
            List<PriceComparisonWrapper> priceComparisonDataList = new List<PriceComparisonWrapper>();
            System.debug('factDetailsObjs--'+factDetailsObjs);
            for(Reports.ReportDetailRow rdr :factDetailsObjs.getRows()){
                System.debug('rdr--'+rdr);
                List<Reports.ReportDataCell> rdc = rdr.getDataCells();
                System.debug('rdc--'+rdc);
                PriceComparisonWrapper priceComparisonWrapperObj = new PriceComparisonWrapper();
                priceComparisonWrapperObj.quoteName = (String)rdc[0].getLabel();
                priceComparisonWrapperObj.quantity = (String)rdc[1].getLabel();
                priceComparisonWrapperObj.listPrice = (String)rdc[2].getLabel();
                priceComparisonWrapperObj.salesPrice = (String)rdc[3].getLabel();
                priceComparisonWrapperObj.totalPrice = (String)rdc[4].getLabel();
                priceComparisonWrapperObj.opportunityName = (String)rdc[5].getLabel();
                priceComparisonWrapperObj.createdDate = (String)rdc[6].getLabel();
                priceComparisonWrapperObj.expirationDate = (String)rdc[7].getLabel();
                priceComparisonWrapperObj.quoteStatus = (String)rdc[9].getLabel();
                priceComparisonDataList.add(priceComparisonWrapperObj);
                //priceComparisonWrapperObj.createdDate
                //priceComparisonWrapperObj.quoteOwner
                //priceComparisonWrapperObj.priceTierAvailable
                //priceComparisonWrapperObj.quoteStatus
            }
            System.debug('priceComparisonDataList--'+priceComparisonDataList);
            responseMap.put('reportData',priceComparisonDataList);
            Integer recordCount = priceComparisonDataList!=null && priceComparisonDataList.size()>0 ? priceComparisonDataList.size() : 0;
            responseMap.put('recordCount',recordCount);
            responseMap.put('status',true);
            responseMap.put('recordType',recordType);
        }catch(Exception e){
            responseMap.put('status',false);
            responseMap.put('recordType',recordType);
        }
        return JSON.serializePretty(responseMap);
    }
}