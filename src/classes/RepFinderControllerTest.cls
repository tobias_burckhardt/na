@isTest
private class RepFinderControllerTest {
	private static final String NOTES = 'Here are some test notes';
	private static final String REGION_NAME = 'Northeast';
	private static final String REGION_LOCAL_NAME = 'Northeast Office';
	private static final String REGION_OFFICE_STREET = '612 Hamilton St';
	private static final String REGION_OFFICE_CITY = 'Allentown';
	private static final String REGION_OFFICE_STATE = 'PA';
	private static final String REGION_OFFICE_ZIP = '18101';
	private static final String REGION_ISO_COUNTRY = 'US';
	private static final String REGION_COUNTRY_CODE = '6101';
	private static final String REGION_IMAGE_URL = 'http://www.example.com/image.png';
	private static final String EMPL_JOB_TITLE = 'Sales Rep/Manager';
	private static final String EMPL_BUSINESS_CELL_COUNTRY_CODE = '1';
	private static final String EMPL_BUSINESS_CELL_AREA_CODE = '610';
	private static final String EMPL_BUSINESS_CELL_PHONE = '530-7200';
	private static final String EMPL_EMAIL = 'trifecta.service@us.sika.com';
	private static final String EMPL_LEGAL_FIRST_NAME = 'John';
	private static final String EMPL_LEGAL_LAST_NAME = 'Doe';
	private static final String REFERRAL_SUBJECT = 'A New Referral';
	private static final String REFERRAL_COMMENTS = 'Some comments';
	private static final String REFERRAL_CROSS_SELL_TEAM = null;

	private static Integer nextRegionNumber = 1;
	private static Integer nextEmailSuffix = 1;

	@isTest
	private static void testGetTargetMarketOptions() {
		Test.startTest();
		List<String> options = RepFinderController.getTargetMarketOptions();
		Test.stopTest();

		System.assertNotEquals(null, options, 'Should have a list of options');
		System.assertNotEquals(0, options.size(), 'List should not be empty');
	}

	@isTest
	private static void testGetStateOptions() {
		Test.startTest();
		List<String> options = RepFinderController.getStateOptions();
		Test.stopTest();

		System.assertNotEquals(null, options, 'Should have a list of options');
		System.assertNotEquals(0, options.size(), 'List should not be empty');
	}

	@isTest
	private static void testGetCrossSellTeamOptions() {
		Test.startTest();
		List<String> options = RepFinderController.getCrossSellTeamOptions();
		Test.stopTest();

		System.assertNotEquals(null, options, 'Should have a list of options');
		System.assertNotEquals(0, options.size(), 'List should not be empty');
	}

	@isTest
	private static void testGetVerticalOptions() {
		Test.startTest();
		List<String> options = RepFinderController.getVerticalOptions();
		Test.stopTest();

		System.assertNotEquals(null, options, 'Should have a list of options');
		System.assertNotEquals(0, options.size(), 'List should not be empty');
	}

	@isTest
	private static void testSearchNoResults() {
		Test.startTest();
		List<RepFinderSearchResult> results = RepFinderController.search('asdfasdf', 'hji23jkj', null);
		Test.stopTest();

		System.assertNotEquals(null, results, 'Should have a list of results');
		System.assertEquals(0, results.size(), 'List should be empty');
	}

	@isTest
	private static void testSearchWithResults() {
		List<String> targetMarkets = RepFinderController.getTargetMarketOptions();
		List<String> states = RepFinderController.getStateOptions();
		String targetMarket = targetMarkets.get(0);
		String state = REGION_OFFICE_STATE;

		// create the result to be returned
		createSearchResult(targetMarket, state);

		// create another result that should not be returned
		createSearchResult(targetMarkets.get(1), states.get(1));

		Test.startTest();
		List<RepFinderSearchResult> results = RepFinderController.search(targetMarket, state, null);
		Test.stopTest();

		System.assertNotEquals(null, results, 'Should have a list of results');
		System.assertEquals(1, results.size(), 'Should have one result');
		RepFinderSearchResult r = results.get(0);
		System.assertNotEquals(null, r.repName);
		System.assertEquals(REGION_LOCAL_NAME, r.region);
		System.assertEquals(EMPL_JOB_TITLE, r.repTitle);
		System.assertEquals(targetMarket, r.targetMarket);
		System.assertEquals(UserInfo.getUserId(), r.repUserId);
		System.assertEquals(NOTES, r.repComments);
		System.assertEquals('1 610 530-7200', r.phone);
		System.assert(r.email.startsWith(EMPL_EMAIL));
		System.assertEquals(REGION_IMAGE_URL, r.regionImage);
		System.assertNotEquals(null, r.regionalManagerName);
		System.assertEquals(EMPL_JOB_TITLE, r.regionalManagerTitle);
		System.assertEquals(UserInfo.getUserId(), r.regionalManagerUserId);
		System.assertEquals(REGION_OFFICE_STREET, r.regionalOfficeStreet);
		System.assertEquals(REGION_OFFICE_CITY, r.regionalOfficeCity);
		System.assertEquals(REGION_OFFICE_STATE, r.regionalOfficeState);
		System.assertEquals(REGION_OFFICE_ZIP, r.regionalOfficeZip);
		System.assertEquals(REGION_ISO_COUNTRY, r.regionalOfficeIsoCountry);
		System.assertEquals(true, r.isGeographic);
		System.assertEquals(false, r.isVertical);
	}

	@isTest
	private static void testSearchWithResultsVertical() {
		List<String> targetMarkets = RepFinderController.getTargetMarketOptions();
		String targetMarket = targetMarkets.get(0);

		// create the result to be returned
		createSearchResultVertical(targetMarket, 'Education');

		// create another result that should not be returned
		createSearchResultVertical(targetMarkets.get(1), 'Education');

		Test.startTest();
		List<RepFinderSearchResult> results = RepFinderController.search(targetMarket, null, 'Education');
		Test.stopTest();

		System.assertNotEquals(null, results, 'Should have a list of results');
		System.assertEquals(1, results.size(), 'Should have one result');
		RepFinderSearchResult r = results.get(0);
		System.assertNotEquals(null, r.repName);
		System.assertEquals('Education', r.vertical);
		System.assertEquals(true, r.isVertical);
		System.assertEquals(false, r.isGeographic);
	}

	@isTest
	private static void testCreateReferral() {
		String targetMarket = RepFinderController.getTargetMarketOptions().get(0);
		Id assignedTo = UserInfo.getUserId();
		String subject = REFERRAL_SUBJECT;
		String comments = REFERRAL_COMMENTS;
		String crossSellTeam = REFERRAL_CROSS_SELL_TEAM;

		Test.startTest();
		RepFinderController.createReferral(targetMarket, assignedTo, subject, comments, crossSellTeam);
		Test.stopTest();

		System.assertEquals(1, getReferralCount());
		Referral__c referral = getReferral();
		System.assertEquals(targetMarket, referral.Target_Market__c);
		System.assertEquals(UserInfo.getUserId(), referral.Assigned_To__c);
		System.assertEquals(REFERRAL_SUBJECT, referral.Subject__c);
		System.assertEquals(REFERRAL_COMMENTS, referral.Referral_Comments__c);
		System.assertEquals(REFERRAL_CROSS_SELL_TEAM, referral.Cross_Sell_Team__c);
	}

	private static Integer getReferralCount() {
		return [ SELECT COUNT() FROM Referral__c ];
	}

	private static Referral__c getReferral() {
		return [
			SELECT
				Id,
				Target_Market__c,
				Assigned_To__c,
				Subject__c,
				Referral_Comments__c,
				Cross_Sell_Team__c
			FROM
				Referral__c
			LIMIT
				1
		];
	}

	private static void createSearchResult(String targetMarket, String state) {
		Region__c region = createRegion(targetMarket, state);
		Employee__c employee = createEmployee();
		FindARep__c rep = new FindARep__c();
		rep.Region__c = region.Id;
		rep.Employee__c = employee.Id;
		rep.Rep_State__c = state;
		rep.Notes__c = NOTES;
		insert rep;
	}

	private static void createSearchResultVertical(String targetMarket, String vertical) {
		Region__c region = createRegionVertical(targetMarket, vertical);
		Employee__c employee = createEmployee();
		FindARep__c rep = new FindARep__c();
		rep.Region__c = region.Id;
		rep.Employee__c = employee.Id;
		rep.Notes__c = NOTES;
		insert rep;
	}

	private static Region__c createRegion(String targetMarket, String state) {
		Region__c region = new Region__c();
		region.Active__c = true;
		region.Name = REGION_NAME;
		region.Region_Office_City__c = REGION_OFFICE_CITY;
		region.Country_Code__c = REGION_COUNTRY_CODE;
		region.ISO_Country__c = REGION_ISO_COUNTRY;
		region.Local_Name__c = REGION_LOCAL_NAME;
		region.Region_Office_State__c = REGION_OFFICE_STATE;
		region.Region_Office_Street__c = REGION_OFFICE_STREET;
		region.Region_Office_Zip__c = REGION_OFFICE_ZIP;
		region.Regional_Manager_EMPL__c = createEmployee().Id;
		region.Region_Image_URL__c = REGION_IMAGE_URL;
		region.Region_Number__c = getNextRegionNumber();
		region.Region_State__c = state;
		region.Target_Market__c = targetMarket;
		region.RecordTypeId = RepFinderUtils.getRegionRecordType(
			RepFinderUtils.RECORDTYPE_REGION_GEOGRAPHIC
		).Id;
		insert region;
		return region;
	}

	private static Region__c createRegionVertical(String targetMarket, String vertical) {
		Region__c region = new Region__c();
		region.Active__c = true;
		region.Name = REGION_NAME;
		region.Country_Code__c = REGION_COUNTRY_CODE;
		region.ISO_Country__c = REGION_ISO_COUNTRY;
		region.Local_Name__c = REGION_LOCAL_NAME;
		region.Regional_Manager_EMPL__c = createEmployee().Id;
		region.Region_Image_URL__c = REGION_IMAGE_URL;
		region.Region_Number__c = getNextRegionNumber();
		region.Vertical__c = vertical;
		region.Target_Market__c = targetMarket;
		region.RecordTypeId = RepFinderUtils.getRegionRecordType(
			RepFinderUtils.RECORDTYPE_REGION_VERTICAL
		).Id;
		insert region;
		return region;
	}

	private static String getNextRegionNumber() {
		return String.valueOf(nextRegionNumber++);
	}

	private static Employee__c createEmployee() {
		Employee__c empl = new Employee__c();
		empl.Legal_First_Name__c = EMPL_LEGAL_FIRST_NAME;
		empl.Legal_Last_Name__c = EMPL_LEGAL_LAST_NAME;
		empl.SFDC_User_Account__c = UserInfo.getUserId();
		empl.Job_Title__c = EMPL_JOB_TITLE;
		empl.Business_Cell_Country_Code__c = EMPL_BUSINESS_CELL_COUNTRY_CODE;
		empl.Business_Cell_Area_Code__c = EMPL_BUSINESS_CELL_AREA_CODE;
		empl.Business_Cell_Phone__c = EMPL_BUSINESS_CELL_PHONE;
		empl.Sika_Email__c = getNextEmail();
		insert empl;
		return empl;
	}

	private static String getNextEmail() {
		return EMPL_EMAIL + '.' + (nextEmailSuffix++);
	}
}