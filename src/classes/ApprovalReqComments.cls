public class ApprovalReqComments {
    public ID targetObjectId { get; set; }
    public String comments {
        get {
            System.debug('comments--'+comments);
            if ( String.isBlank(comments)) {
                ProcessInstanceStep lastStep = getLastApprovalStep();
                System.debug('lastStep--'+lastStep);
                comments = ( lastStep != null ) ? lastStep.comments : '';
            }
            return comments;
        }
        private set;
    }
    public ApprovalReqComments() {}
    private ProcessInstanceStep getLastApprovalStep() {
        List<ProcessInstanceStep> approvalSteps = new List<ProcessInstanceStep>([select Comments from ProcessInstanceStep where ProcessInstance.TargetObjectId = :targetObjectId order by SystemModStamp desc limit 1]);
        System.debug('approvalSteps--'+approvalSteps);
        return ( approvalSteps.size() > 0 ) ? approvalSteps[0] : null;
    }
}