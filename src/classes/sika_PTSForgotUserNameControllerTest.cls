@IsTest
private class sika_PTSForgotUserNameControllerTest {
	private static PageReference pageRef;
	private static User guestUser;
	private static User SCCZoneUser;
	private static sika_SCCZoneForgotUserNameController ctrl;

	// static initializer
	static {
		// create a guest (anonymous) user to run the test as
		guestUser = sika_SCCZoneTestFactory.createUser(new Map<String, String>{'profileId' =>  sika_SCCZoneTestFactory.getGuestUserProfileId()});
		insert(guestUser);
		
		// create an SCCZone user to validate against.  (Query only searches for usernames associated with SCC Zone users; not for all Salesforce users.  Not opening up that kind of security hole here!)
		Map<String, sObject> usefulStuff = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount(new Map<String, String>{'contact_email' => 'test@us.sika.com'});
		SCCZoneUser = (User) usefulStuff.get('SCCZoneUser');
	}

	private static void init(){
		// set page we'll be testing
		pageRef = new PageReference('/sika_SCCZoneForgotUserNamePage');
		Test.setCurrentPage(pageRef);
	}


	@IsTest static void test_happy_path(){
		init();
		System.runAs(guestUser){
			// create an instance of the page's controller
			ctrl = new sika_SCCZoneForgotUserNameController();

			// confirm that the page loaded. (Didn't redirect to an error page)
			System.assertEquals(pageRef.getUrl(), ApexPages.currentPage().getUrl());

			ctrl.emailAddress = SCCZoneUser.email;
			PageReference submitResponse = ctrl.submitRequest();
			System.assert(submitResponse.getUrl().contains('/sika_SCCZoneForgotUserNamePage'));
			System.assertEquals(submitResponse.getParameters().get('success'), 'true');
		}

	}

	@IsTest static void test_sad_path(){
		init();
		System.runAs(guestUser){
			ctrl = new sika_SCCZoneForgotUserNameController();

			ctrl.emailAddress = 'wrong@email.com';
			PageReference submitResponse = ctrl.submitRequest();
			System.assert(ctrl.hasError == true);
			System.assertEquals(ctrl.errorText, sika_SCCZoneForgotUserNameController.emailNotRecognizedErrorMsg);

			ctrl.emailAddress = SCCZoneUser.email;
			submitResponse = ctrl.submitRequest();
			System.assert(submitResponse.getUrl().contains('/sika_SCCZoneForgotUserNamePage'));
			System.assertEquals(submitResponse.getParameters().get('success'), 'true');
	
		}

	}

	@IsTest static void test_bad_path(){
		init();
		System.runAs(guestUser){
			ctrl = new sika_SCCZoneForgotUserNameController();
			PageReference submitResponse;

			ctrl.emailAddress = '';
			submitResponse = ctrl.submitRequest();
			System.assert(ctrl.hasError == true);
			System.assertEquals(ctrl.errorText, sika_SCCZoneForgotUserNameController.invalidEmailErrorMsg);

			ctrl.emailAddress = 'notanemail@com';
			submitResponse = ctrl.submitRequest();
			System.assert(ctrl.hasError == true);
			System.assertEquals(ctrl.errorText, sika_SCCZoneForgotUserNameController.invalidEmailErrorMsg);

			ctrl.emailAddress = 'notanemail.com';
			submitResponse = ctrl.submitRequest();
			System.assert(ctrl.hasError == true);
			System.assertEquals(ctrl.errorText, sika_SCCZoneForgotUserNameController.invalidEmailErrorMsg);

			ctrl.emailAddress = '@.com';
			submitResponse = ctrl.submitRequest();
			System.assert(ctrl.hasError == true);
			System.assertEquals(ctrl.errorText, sika_SCCZoneForgotUserNameController.invalidEmailErrorMsg);

			ctrl.emailAddress = SCCZoneUser.email;
			submitResponse = ctrl.submitRequest();
			System.assert(submitResponse.getUrl().contains('/sika_SCCZoneForgotUserNamePage'));
			System.assertEquals(submitResponse.getParameters().get('success'), 'true');
	
		}

	}
}