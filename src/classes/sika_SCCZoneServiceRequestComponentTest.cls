@isTest
private class sika_SCCZoneServiceRequestComponentTest {
	private static User theUser;
	private static Account theAccount;
	private static sika_SCCZoneServiceRequestComponentCtrl ctrl;
	
	private static void init() {
		map<String, sObject> objects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
		theUser = (User) objects.get('SCCZoneUser');
		theAccount = (Account) objects.get('theAccount');
		
	}
	
	//@isTest static void testHappy(){
	//	init();
		
	//	PageReference successPage = new pageReference('/sika_SCCZoneSupportTicket');
	//	PageReference actualPage;
	//	String caseDesc = 'I could use a hand over here!';
	//	system.runAs(theUser){
	//		ctrl = new sika_SCCZoneServiceRequestComponentCtrl();

	//		ctrl.selectedType = 'Mix Support';
	//		ctrl.caseDesc = caseDesc;
			
	//		actualPage = ctrl.submitCase();
			
	//	}
	//	// strip off the querystring before comparing
	//	String actualPageStr = actualPage.getURL();
	//	list<String> testPageStr = actualPageStr.split('\\?');
		
	//	system.assertEquals(testPageStr[0], successPage.getURL());
		
	//	list<Case> c = [SELECT Description, Customer_Email__c FROM Case];
	//	system.assert(c.size() == 1);
	//	system.assertEquals(c[0].Description, caseDesc);
	//}
	
	//@isTest static void testSad(){
	//	init();
		
	//	PageReference actualPage;
		
	//	system.runAs(theUser){
	//		ctrl = new sika_SCCZoneServiceRequestComponentCtrl();
			
	//		ctrl.selectedType = null;
	//		ctrl.caseDesc = null;
			
	//		actualPage = ctrl.submitCase();
	//		system.assertEquals(ctrl.caseDescError, true);
	//	}
	//}
	
	@isTest static void testGetSelectTypes(){
		init();
		system.runAs(theUser){
			ctrl = new sika_SCCZoneServiceRequestComponentCtrl();
			list<SelectOption> opt = ctrl.getRequestTypes();
			system.assert(opt.size() > 0);
		}
		
	}
	
	@isTest static void testSilly(){
		init();
		system.runAs(theUser){
			ctrl = new sika_SCCZoneServiceRequestComponentCtrl();
			ctrl.showPopup();
			system.assertEquals(ctrl.displayPopup, true);
			ctrl.closePopup();
			system.assertEquals(ctrl.displayPopup, false);
		}
	}
	
}