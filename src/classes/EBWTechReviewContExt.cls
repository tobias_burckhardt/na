public class EBWTechReviewContExt {

    public Opportunity opp {get; set;}
    public Boolean secondPage {get; set;} // renders the two page blocks
    public Boolean validated {get; set;} // validation if the customer meets requirements
    public final String FIRM_ROLE = 'Building owner'; // needs to be modified

    public EBWTechReviewContExt(ApexPages.StandardController stdController){
        if (!test.isRunningTest()) {
            stdController.addFields(new List<String>{
                'Account.Elite__c',
                'Account.Id',
                'Account.Name',
                'Account.SAP_Account_ID__c',
                'EBW__c',
                'EBW_T_C_Accepted__c',
                'EBW_Start_Date__c',
                'EBW_Delivery_Company_Name__c',
                'EBW_Company_Account_Number__c',
                'SAP_Project_Number__c'
            });
        }

        this.opp = (Opportunity) stdController.getRecord();
        this.secondPage = false;
        this.validated = false;
    }

    public PageReference agree(){

        if (opp.EBW_T_C_Accepted__c == false ||
            opp.EBW_Start_Date__c == null ||
            (!opp.Account.Elite__c && 
            (String.isBlank(opp.EBW_Delivery_Company_Name__c) ||
            String.isBlank(opp.EBW_Company_Account_Number__c)))){

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Please fill in Warranty Start Date, Delivery company name and Delivery company account number.'));

        } else if (opp.EBW_T_C_Accepted__c && this.validated){
            opp.EBW_T_C_Declined__c = false;
            opp.EBW__c = true;
            opp.Terms_Warranty_Request_Date__c = DateTime.newInstance(opp.EBW_Start_Date__c, Time.newInstance(0, 0, 0, 0));
            update opp;
 			return new PageReference('/'+opp.Id);
        }
        return ApexPages.currentPage();
    }

    public PageReference Disagree(){
        opp.EBW_T_C_Accepted__c = false;
        opp.EBW_T_C_Declined__c = true;
        update opp;

        PageReference pageRef = new PageReference('/'+ opp.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }

    public void yes(){
        this.secondPage = true;
    }

    public void validate(){

        Opportunity oppFromQuery = [SELECT Company_Name__c ,Contact_Name__c ,Building_Owner_Street_Address__c , 
                                        Building_Owner_City__c ,Building_Owner_State__c ,Building_Owner_Zipcode__c , 
                                        Building_Owner_Phone__c  
                                        FROM Opportunity WHERE Id =: opp.Id];
        List< Building_Group__c > oppBuildingGroups = [SELECT Id FROM Building_Group__c WHERE Opportunity__c =: opp.Id];

        System.debug(oppBuildingGroups);


        if (opp.Account.Elite__c 
            && oppFromQuery != null
            && oppFromQuery.Company_Name__c != null 
            && oppFromQuery.Contact_Name__c != null 
            && oppFromQuery.Building_Owner_Street_Address__c != null 
            && oppFromQuery.Building_Owner_City__c != null 
            && oppFromQuery.Building_Owner_State__c != null 
            && oppFromQuery.Building_Owner_Zipcode__c != null 
            && oppFromQuery.Building_Owner_Phone__c != null 
            && oppBuildingGroups != null 
            && !oppBuildingGroups.isEmpty()){
            this.validated = true;
        } else {
 			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Selected customer cannot be converted to Early Bird, because he doesn\'t meet the following criteria: Opportunity must have at least one related Building Group and all fields in the Warranty Building Owner section must be populated.'));
        }
    }

    public PageReference cancel() {
        opp.Take_Action__c = 'NOA Created';
        opp.Tech_Review_Requested__c = false;
        opp.Roofing_Tech_Review_Status__c = null;

        try {
            update opp;
        } catch(DmlException e) {
            ApexPages.addMessages(e);
        }

        return new PageReference('/' + opp.Id);
    }

}