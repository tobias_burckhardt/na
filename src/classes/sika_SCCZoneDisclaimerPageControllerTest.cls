@isTest
private class sika_SCCZoneDisclaimerPageControllerTest{
	private static sika_SCCZoneDisclaimerPageController ctrl;

	@isTest static void theTest(){
		PageReference pageRef = new PageReference('/sika_SCCZoneDisclaimer');
        Test.setCurrentPage(pageRef);

		ctrl = new sika_SCCZoneDisclaimerPageController();
		system.assert(ctrl.acceptDisclaimer().getURL().contains('/sika_SCCZoneUserHomePage'));
		system.assert(ctrl.rejectDisclaimer().getURL().contains('/secur/logout.jsp'));

	}

	@isTest static void testLoadAction(){
        User guestUser = sika_SCCZoneTestFactory.createGuestUser();
        insert(guestUser);

        PageReference p;

        System.runAs(guestUser){
            ctrl = new sika_SCCZoneDisclaimerPageController();
            p = ctrl.loadAction();
            system.assert(String.valueOf(p).contains('/SCCZone/sika_SCCZoneLoginPage'));
        }
    }

}