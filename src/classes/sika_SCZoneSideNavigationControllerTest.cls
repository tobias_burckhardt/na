@isTest 
private class sika_SCZoneSideNavigationControllerTest {
	private static sika_SCZoneSideNavigationController ctrl;
	private static PageReference pageRef;
	
	private static void init(){
		Test.setCurrentPage(pageRef);
		ctrl = new sika_SCZoneSideNavigationController();
	}

	private static void doAssert(String activeStyle){
		system.debug('********SideNavControllerTest: pageRef = ' + pageRef + ' :: asserting activeStyle = ' + activeStyle);
		system.assertEquals(ctrl.homeStyle, activeStyle == 'home' ? 'active' : '');
		system.assertEquals(ctrl.mixStyle, activeStyle == 'mix' ? 'active' : '');
		system.assertEquals(ctrl.matStyle, activeStyle == 'material' ? 'active' : '');
        system.assertEquals(ctrl.supStyle, activeStyle == 'sup' ? 'active' : '');
        system.assertEquals(ctrl.allStyle, activeStyle == 'all' ? 'active' : '');
        system.assertEquals(ctrl.cementStyle, activeStyle == 'cement' ? 'active' : '');
        system.assertEquals(ctrl.scmStyle, activeStyle == 'scm' ? 'active' : '');
        system.assertEquals(ctrl.sandStyle, activeStyle == 'sand' ? 'active' : '');
        system.assertEquals(ctrl.adStyle, activeStyle == 'ad' ? 'active' : '');
        system.assertEquals(ctrl.coarseStyle, activeStyle == 'coarse' ? 'active' : '');
        system.assertEquals(ctrl.waterStyle, activeStyle == 'water' ? 'active' : '');
        system.assertEquals(ctrl.otherStyle, activeStyle == 'other' ? 'active' : '');
	}

	@isTest static void sideNavigationPagePrefixSuffixTest(){
		pageRef = new PageReference('SCCZone/apex/sika_SCCZoneUserHomePage');
		init();
		doAssert('home');
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneUserHomePage');
		init();
		doAssert('home');
		
		pageRef = new PageReference('apex/sika_SCCZoneUserHomePage');
		init();
		doAssert('home');
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneMaterialsHomePage');
		pageRef.getParameters().put('view', 'cement');
		pageRef.getParameters().put('state', 'me');
		init();
		doAssert('cement');
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneMaterialsHomePage');
		pageRef.getParameters().put('bla', 'bla');
		pageRef.getParameters().put('view', 'cement');
		pageRef.getParameters().put('bah', 'bah');
		init();
		doAssert('cement');
	
	}

	@isTest static void sideNavigationControllerTest() {
		pageRef = new PageReference('SCCZone/sika_SCCZoneUserHomePage');
		init();
		doAssert('home');
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneContactUpdatePage');
		init();
 		doAssert('sup');
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneSupportHomePage');
		init();
 		doAssert('sup');

		pageRef = new PageReference('SCCZone/sika_SCCZoneSupportTicket');
		init();
		doAssert('sup');
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneMixesHomePage');
		init();
		doAssert('mix');
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneNewMixPage');
		init();
		doAssert('mix');
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneMixDetailsPage');
		init();
		doAssert('mix');
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneManageMixMaterialsPage2');
		init();
		doAssert('mix');
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneCreateEditMaterialPage');
		init();
		doAssert('material');
		system.assertEquals(ctrl.showFilters, true);
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneMaterialDetailPage');
		init();
		doAssert('material');
		system.assertEquals(ctrl.showFilters, true);
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneMaterialsHomePage');
		pageRef.getParameters().put('view', 'all');
		init();
		doAssert('all');
		system.assertEquals(ctrl.showFilters, true);
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneMaterialsHomePage');
		pageRef.getParameters().put('view', 'cement');
		init();
		doAssert('cement');
		system.assertEquals(ctrl.showFilters, true);
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneMaterialsHomePage');
		pageRef.getParameters().put('view', 'scm');
		init();
		doAssert('scm');
		system.assertEquals(ctrl.showFilters, true);
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneMaterialsHomePage');
		pageRef.getParameters().put('view', 'sand');
		init();
		doAssert('sand');
		system.assertEquals(ctrl.showFilters, true);
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneMaterialsHomePage');
		pageRef.getParameters().put('view', 'ad');
		init();
		doAssert('ad');
		system.assertEquals(ctrl.showFilters, true);
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneMaterialsHomePage');
		pageRef.getParameters().put('view', 'coarse');
		init();
		doAssert('coarse');
		system.assertEquals(ctrl.showFilters, true);
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneMaterialsHomePage');
		pageRef.getParameters().put('view', 'water');
		init();
		doAssert('water');
		system.assertEquals(ctrl.showFilters, true);
		
		pageRef = new PageReference('SCCZone/sika_SCCZoneMaterialsHomePage');
		pageRef.getParameters().put('view', 'other');
		init();
		doAssert('other');
		system.assertEquals(ctrl.showFilters, true);
		
	}
	
}