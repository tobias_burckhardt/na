public without sharing class sika_SCCZoneSupportTicketController {
	
	public String theCaseId {get; private set;}
	public String commentForm {get; set;}
	public String createdDateET {get; private set;}
	public String modifiedDateET {get; private set;}
	private String retURL {get; set;}

	// constructor
	public sika_SCCZoneSupportTicketController() {
		sika_SCCZoneCheckAuth.setAuthStatus(UserInfo.getProfileId());
		
		theCaseId = ApexPages.CurrentPage().getParameters().get('id');

	}

	// check user auth status and confirm that a support ticket Id was passed to the page
	public PageReference loadAction(){
        PageReference authCheck;
        PageReference idCheck;

        // because this controller runs "without sharing" we need to manually confirm that the user should have access. (A user whose session has timed-out, or an otherwise unauthenticated user who accessed this page directly, would be able to view the page otherwise, albeit without any data displayed.)
        authCheck = sika_SCCZoneCheckAuth.doAuthCheck();
        if(authCheck != null){
            return authCheck;
        }
        // be sure the page was passed all appropriate record IDs
        idCheck = checkId();
        if(idCheck != null){
            return idCheck;
        }
        return null;
    }

    private PageReference checkId(){
        if(theCaseID == null){
        	system.debug('********ERROR: No Case Id was passed to the page. Redirecting to the support homepage.');
            PageReference pageRef = new PageReference('/SCCZone/apex/sika_SCCZoneSupportHomePage');
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
    }

	public Case getCaseDetails(){
		Case theCase;
		
		if(theCaseId != null){
			theCase = [SELECT Id, 
							  CaseNumber, 
							  // Type,
							  SCC_Zone_Case_Types__c, 
							  CreatedDate, 
							  LastModifiedDate, 
							  Subject,
							  Description 
					   	 FROM Case 
					   	WHERE Id = :theCaseId Limit 1];
		} else {
			
			// if no Case Id were passed to the page, the page would fail to load. We'll pass a blank case record instead, and let the pageAction redirect to the userHomePage.
			theCase = new Case();
			theCase.createdDate = dateTime.now();
			theCase.LastModifiedDate = dateTime.now();
		}

		createdDateET = theCase.CreatedDate.format('MM/dd/yyyy hh:mm a', 'America/New_York');
		modifiedDateET = theCase.LastModifiedDate.format('MM/dd/yyyy hh:mm a', 'America/New_York');

		return theCase;
	}

	public String firstComment {get; set;}

	public list<String> getComments(){
		list<CaseComment> theComments = [SELECT Id, 
												 CreatedById, 
												 CreatedBy.Name,
												 CreatedDate, 
												 ParentId, 
												 CommentBody 
											FROM CaseComment 
										   WHERE ParentId = :theCaseId 
										Order By CreatedDate ASC];
		
		if(theComments.size() > 0){
			firstComment = formatMyComment(theComments[0]);
			theComments.remove(0);

		} else {
			firstComment = '';
		}

		list<String> theFormattedComments = new list<String>();
		String formattedComment;

		for(CaseComment c : theComments){
			formattedComment = formatMyComment(c);
			theFormattedComments.add(formattedComment);
		}

		return theFormattedComments;
	}

	private String formatMyComment(CaseComment c){
		//CHANGED: 3/1/15 SS
		// String timestamp = formatMyDate(c.CreatedDate);
		// String formattedComment = '[' + timestamp + '] ' + c.CommentBody;
		String formattedComment = '[' + c.CreatedBy.Name + ']:  ' + c.CommentBody;
		
		return formattedComment;
	}
	/*
	private String formatMyDate(dateTime d){
		String s = d.format('MM/dd/yyyy hh:mm a');
		
		return s;
	}
	*/

	public PageReference cancel(){
		PageReference returnPage = new PageReference('/sika_SCCZoneSupportHomePage');
		returnPage.setRedirect(true);

		return returnPage;
	}

	public PageReference submitComment(){
		if(validateForm()){

			// create the CaseComment
			CaseComment theCaseComment = new CaseComment();
			theCaseComment.ParentId = theCaseId;
			theCaseComment.commentBody = commentForm;
			theCaseComment.isPublished = true;

			insert(theCaseComment);

			commentForm = '';
			return null;
		}
		return null;
	}

	public Boolean hasErrors {get; private set;}
	public Boolean commentError {get; private set;}
	public String commentErrorText {get; private set;}

	@testVisible private Boolean validateForm (){
		commentError = false;
		commentErrorText = '';

		if(String.isBlank(commentForm)){
            commentErrorText = 'Please fill in the comment form above.';
            commentError = true;

            return false;
        }
        return true;
	}
}