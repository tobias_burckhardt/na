public with sharing class CommunityAccountLookupController {

    

    public String accstret { get; set; }

    public String testVar { get; set; }
    public String accname {get;set;}
    public String accstre{get;set;}
    public String acctype {get;set;}
    public String acccity {get;set;}
    public String accstate {get;set;}
    public String accpone {get;set;}
    public String acczip {get;set;}
    public String accweb {get;set;}
    public List<Account> acctList {get; set;}
    public Account acc{get;set;}
    public boolean isCreate{get;set;}
    public string selectedName{get;set;}
    public string selectedEmail{get;set;}
    public string selectedPhone{get;set;}
    public string selectedCompany{get;set;}
    
    public Account createdAccount{get; set;}
     List<user> adminUser;
    public string uid{get; set;}
    public CommunityAccountLookupController(){
    
        acc = new Account();
        isCreate = false;
        createdAccount = new Account();
         adminUser = [select id, name from user where profile.name='System Administrator' limit 1];
        loadAccounts();
    }
    
    private void loadAccounts(){
        uid = UserInfo.getUserId();
        
            acctList = [select Id, name, type,BillingCity,BillingState from Account where Community_User_ID__c = :uid ];
        }
        
            
        
    
    
    public void selectedAccountList(){
        acctList.clear();
        system.debug('uid: '+ uid);
        system.debug('selectedname: '+ selectedName);
        string accQuery;
        if((selectedName != null && selectedName != '') || (selectedEmail != null && selectedEmail != '') || (selectedPhone != null && selectedPhone != '') && (selectedCompany != null && selectedCompany != '')){
            accQuery = 'select Id, name, phone,BillingCity,BillingState from Account where Community_User_ID__c =:uid AND (name like \'%'+selectedName+'%\' or Company_Email__c like \'%'+selectedEmail+'%\' or phone like \'%'+selectedPhone+'%\' or SAP_Company_Code__c like \'%'+selectedCompany+'%\')' ;
            acctList = database.query(accQuery);
        }
        else{
            acctList = [select Id, name, phone ,BillingCity,BillingState from Account where Community_User_ID__c = :uid];
        }
        system.debug('acctlist: '+acctList);
        }
        
    public void saveAccount(){
    
               createdAccount.Community_User_ID__c = uid;
               createdAccount.name=accname;
               createdAccount.Type=acctype;
               createdAccount.BillingCity=acccity;
               createdAccount.BillingState=accstate;
               createdAccount.website=accweb;
               createdAccount.phone=accpone;
               createdAccount.BillingStreet=accstre;
               createdAccount.BillingPostalCode=acczip ;
               createdAccount.ownerid=adminUser[0].id;
                upsert createdAccount;
                isCreate = true;                
               loadAccounts();  
        
    }

}