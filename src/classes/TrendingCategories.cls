public with sharing class TrendingCategories{ 
    public List<AggregateResult> TrendingToday {get; set;}
    public List<AggregateResult> TrendingThisWeek {get; set;}
    public List<AggregateResult> TrendingThisMonth {get; set;} 
    public List<AggregateResult> TrendingThisYear {get; set;} 
    public List<AggregateResult> OpenIncidentTiming {get; set;} 
    
    public TrendingCategories(){ 
        TrendingToday = 
            [
            SELECT BMCServiceDesk__FKCategory__r.Name Category, SUM(Count__c) Total
            FROM BMCServiceDesk__Incident__c 
            WHERE BMCServiceDesk__openDateTime__c = TODAY
            GROUP BY BMCServiceDesk__FKCategory__r.Name
            ORDER BY SUM(Count__c) DESC
            LIMIT 5
            ];

        TrendingThisWeek = 
            [
            SELECT BMCServiceDesk__FKCategory__r.Name Category, SUM(Count__c) Total
            FROM BMCServiceDesk__Incident__c 
            WHERE BMCServiceDesk__openDateTime__c = THIS_WEEK
            GROUP BY BMCServiceDesk__FKCategory__r.Name
            ORDER BY SUM(Count__c) DESC
            LIMIT 5
            ];

        TrendingThisMonth = 
            [
            SELECT BMCServiceDesk__FKCategory__r.Name Category, SUM(Count__c) Total
            FROM BMCServiceDesk__Incident__c 
            WHERE BMCServiceDesk__openDateTime__c = THIS_MONTH
            GROUP BY BMCServiceDesk__FKCategory__r.Name
            ORDER BY SUM(Count__c) DESC
            LIMIT 5
            ];

        TrendingThisYear = 
            [
            SELECT BMCServiceDesk__FKCategory__r.Name Category, SUM(Count__c) Total
            FROM BMCServiceDesk__Incident__c 
            WHERE BMCServiceDesk__openDateTime__c = THIS_YEAR
            GROUP BY BMCServiceDesk__FKCategory__r.Name
            ORDER BY SUM(Count__c) DESC
            LIMIT 5
            ];
        
        OpenIncidentTiming = 
            [
            SELECT Owner.Name Owner, SUM(Count__c) Total
            FROM BMCServiceDesk__Incident__c 
            WHERE BMCServiceDesk__state__c = TRUE AND Duration_Time__c >= 7
            GROUP BY Owner.Name
            ORDER BY Owner.Name ASC
            ];

    }    
}