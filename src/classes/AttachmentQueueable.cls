global class AttachmentQueueable implements Queueable {
	private Integer batchSize;
	private List<Attachment> attachmentsToInsert;

	public static Integer BATCH_SIZE {get {
		if(BATCH_SIZE == null) {
			List<Attachment_Setting__mdt> attachmentSettings = [SELECT Batch_Size__c FROM Attachment_Setting__mdt WHERE DeveloperName = 'Default' LIMIT 1];
			if(attachmentSettings.size() > 0) {
				BATCH_SIZE = attachmentSettings[0].Batch_Size__c.intValue();
			}
		} 
		return BATCH_SIZE;
	} set;}
	
	public AttachmentQueueable(List<Attachment> attachments, Integer batchSize) {
		this.attachmentsToInsert = attachments; 
		this.batchSize = batchSize;
	}
	
	public AttachmentQueueable(List<Attachment> attachments) {
		this.attachmentsToInsert = attachments;
		this.batchSize = BATCH_SIZE;
	}

	global void execute(QueueableContext context) {
		List<Attachment> overflowAttachments = new List<Attachment>();
		List<Attachment> emailToCaseAttachments = new List<Attachment>();

		for(Integer i = 0; i < attachmentsToInsert.size(); i++) {
			if(i < batchSize) {
				emailToCaseAttachments.add(attachmentsToInsert.get(i));

			} else {
				overflowAttachments.add(attachmentsToInsert.get(i));
			}
		}

		if(emailToCaseAttachments.size() > 0) {
			EmailToCaseService.createChatterFile(emailToCaseAttachments);
		}

		if(overflowAttachments.size() > 0) {
			if(!Test.isRunningTest()) {
				System.enqueueJob(new AttachmentQueueable(overflowAttachments, batchSize));
			}
		}
	}
}