@isTest
private class sika_SCCZoneOptimizeMixTest {
    private static Integer i;
    private static Mix__c testMix;
    private static Mix__c testMixValues;
    private static list<Mix_Material__c> testMixMaterials;
    
    private static void init(){
        // no need to fire these triggers during (most of) these tests
        sika_SCCZoneMixTriggerHelper.bypassAccountTeamCheck = true;
        sika_SCCZoneMixTriggerHelper.bypassMixOwnerTransferCheck = true;
        sika_SCCZoneMixTriggerHelper.bypassMixChatterAlert = true;
        
        sika_SCCZoneMaterialTriggerHelper.bypassAccountTeamCheck = true;
        sika_SCCZoneMaterialTriggerHelper.bypassPreventDelete = true;
        sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = true;
        sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = true;
        
        sika_SCCZoneMixMaterialTriggerHelper.mixMaterialTriggerBypassSwitch(true);
/*      
        sika_SCCZoneMixMaterialTriggerHelper.bypassAccountTeamCheck = true;
        sika_SCCZoneMixMaterialTriggerHelper.bypassMixOptimizationCheck = true;
        sika_SCCZoneMixMaterialTriggerHelper.bypassSetSCMWeightAndUnit = true;
        sika_SCCZoneMixMaterialTriggerHelper.bypassConcreteCurveCalc = true;
*/  
        
        // create a mix
        testMix = sika_SCCZoneTestFactory.createMix();
        insert testMix;
        
        // query for the Mix we just created, so we can get all of the fields required to test the optimizer
        testMixValues = [SELECT Id,
                            System_of_Measure__c,
                            Optimize_for__c,
                            Target_Water_Cement_Ratio__c,
                            Target_SCM_Ratio__c
                       FROM Mix__c
                      WHERE Id = :testMix.Id];
        
        // create a list of materials
        list<Material__c> testMaterials = new list<Material__c>();
        String matName;

        // five sands
        for(i=5; i > 0; i--){
            matName = 'Test_Sand_' + i;
            testMaterials.add(sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'type' => 'Sand', 'name' => matName}));
        }

        // five coarse aggregates
        for(i=5; i > 0; i--){
            matName = 'Test_Coarse_Agg_' + i;
            testMaterials.add(sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'type' => 'Coarse Aggregate', 'name' => matName}));
        }

        // two "other"
        for(i=2; i > 0; i--){
            matName = 'Other_' + i;
            testMaterials.add(sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'type' => 'Other', 'name' => matName}));
        }

        // one water 
        testMaterials.add(sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'type' => 'Water', 'name' => 'Test_Water_1'}));

        // one SCM
        testMaterials.add(sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'type' => 'SCM', 'name' => 'Test_SCM_1'}));        

        // one cement
        testMaterials.add(sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'type' => 'Cement', 'name' => 'Test_Cement_1'}));

        // insert the materials 
        insert(testMaterials);

        // Associate the materials we just created with the mix
        list<Mix_Material__c> theMixMaterials = new list<Mix_Material__c>();
        for(Material__c m : testMaterials){
            theMixMaterials.add(sika_SCCZoneTestFactory.createMixMaterial(testMix, m));
        }
        
        insert(theMixMaterials);
        
        // query for the mix_materials we just created, so we can get all of the fields required by the optimizer
        testMixMaterials = [SELECT Id,
                                 Material_Type__c,
                                 Moisture__c,
                                 Material__r.Spec_Gravity__c,
                                 Material__r.Abs__c,
                                 Volume__c,
/*
                                 Sieve_ord_1__c,
                                 Sieve_ord_2__c,
                                 Sieve_ord_3__c,
                                 Sieve_ord_4__c,
                                 Sieve_ord_5__c,
                                 Sieve_ord_6__c,
                                 Sieve_ord_7__c,
                                 Sieve_ord_8__c,
                                 Sieve_ord_9__c,
                                 Sieve_ord_10__c,
                                 Sieve_ord_11__c,
                                 Sieve_ord_12__c,
                                 Sieve_ord_13__c,
                                 Sieve_ord_14__c,
                                 Sieve_ord_15__c,
                                 Sieve_ord_16__c,
*/
                                 Material__r.Sieve_80mm_calculated__c,
                                 Material__r.Sieve_63mm_calculated__c,
                                 Material__r.Sieve_31_5mm_calculated__c,
                                 Material__r.Sieve_25mm_calculated__c,
                                 Material__r.Sieve_20mm_calculated__c,
                                 Material__r.Sieve_16mm_calculated__c,
                                 Material__r.Sieve_14mm_calculated__c,
                                 Material__r.Sieve_12_5mm_calculated__c,
                                 Material__r.Sieve_10mm_calculated__c,
                                 Material__r.Sieve_5mm_calculated__c,
                                 Material__r.Sieve_2_5mm_calculated__c,
                                 Material__r.Sieve_1_25mm_calculated__c,
                                 Material__r.Sieve_0_63mm_calculated__c,
                                 Material__r.Sieve_0_315mm_calculated__c,
                                 Material__r.Sieve_0_16mm_calculated__c,
                                 Material__r.Sieve_0_08mm_calculated__c,
                                 Quanity_kg__c      // sic. (The field name is misspeeled. Going with it.)
                            FROM Mix_Material__c];
    }
    
/*
    @isTest static void testCreateOptimizationInputObj() {
        init();
        
        Test.startTest();

        sika_SCCZoneOptimizeMix.optimizationInputObj inputObj = new sika_SCCZoneOptimizeMix.optimizationInputObj(testMix, testMixMaterials);

        system.debug('******** inputObj = ' + inputObj);
        
        String SOM = testMixValues.System_of_Measure__c == 'Metric (SI)' ? 'metric' : 'standard';
        system.assertEquals(inputObj.Measure, SOM);
        
        system.assertEquals(inputObj.WC_Target, testMixValues.Target_Water_Cement_Ratio__c);
        system.assertEquals(inputObj.Optimization_Type, testMixValues.Optimize_for__c);
        system.assertEquals(inputObj.Optimization_Type, testMixValues.Optimize_for__c);
        
        system.assert(inputObj.Admixture1 == null);     
        system.assert(inputObj.Admixture2 == null);
        system.assert(inputObj.Admixture3 == null);
        system.assert(inputObj.Admixture4 == null);
        system.assert(inputObj.Admixture5 == null);
        
        set<Id> cementIDs = new set<Id>();
        set<Id> coarseAggIDs = new set<Id>();
        set<Id> sandIDs = new set<Id>();
        set<Id> otherIDs = new set<Id>();
        
        for(Mix_Material__c mm : testMixMaterials){
            if(mm.Material_Type__c == 'Cement'){
                cementIDs.add(mm.Id);
            } else if(mm.Material_Type__c == 'Sand'){
                sandIDs.add(mm.Id);
            } else if(mm.Material_Type__c == 'Coarse Aggregate'){
                coarseAggIDs.add(mm.Id);
            } else if(mm.Material_Type__c == 'Other'){
                otherIDs.add(mm.Id);
            }
        }
        
        system.assert(cementIDs.contains(inputObj.Cement));
        
        system.assert(coarseAggIDs.contains(inputObj.Coarse1));
        system.assert(coarseAggIDs.contains(inputObj.Coarse2));
        system.assert(coarseAggIDs.contains(inputObj.Coarse3));
        system.assert(coarseAggIDs.contains(inputObj.Coarse4));
        system.assert(coarseAggIDs.contains(inputObj.Coarse5));
        
        system.assert(otherIDs.contains(inputObj.Other1));
        system.assert(otherIDs.contains(inputObj.Other2));
        
        system.assert(sandIDs.contains(inputObj.Sand1));
        system.assert(sandIDs.contains(inputObj.Sand2));
        system.assert(sandIDs.contains(inputObj.Sand3));
        system.assert(sandIDs.contains(inputObj.Sand4));
        system.assert(sandIDs.contains(inputObj.Sand5));
        
        Test.stopTest();
    }
*/
    
    @isTest static void testDoOptimize(){
        // we'll confirm that it fails without mix-materials first...
        Mix__c tempMix = sika_SCCZoneTestFactory.createMix();
        insert tempMix;
        map<String, String> responseMap = sika_SCCZoneOptimizeMix.doOptimize(tempMix.Id, new set<Id>());
        system.assertEquals(responseMap.get('passFail'), 'fail');
        
        // and now let's get going...
        init();
        Test.startTest();
         
        set<Id> mmIDs = new set<Id>();
        for(Mix_Material__c mm : testMixMaterials){
            mmIDs.add(mm.Id);
        }
        
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        responseMap = sika_SCCZoneOptimizeMix.doOptimize(testMix.Id, mmIDs);
        
        Test.stopTest();
    }


    @isTest static void testspRangeEncode(){
        String reinforcement = 'Highly Reinforced';
        String diameter = '8mm';
        system.assertEquals(sika_SCCZoneOptimizeMix.spRangeEncode(diameter, reinforcement), '8 mm HR');
        diameter = '14mm';
        system.assertEquals(sika_SCCZoneOptimizeMix.spRangeEncode(diameter, reinforcement), '14 mm HR');
        diameter = '20mm';
        system.assertEquals(sika_SCCZoneOptimizeMix.spRangeEncode(diameter, reinforcement), '20 mm HR');
        diameter = '3/8"';
        system.assertEquals(sika_SCCZoneOptimizeMix.spRangeEncode(diameter, reinforcement), '8 mm HR');
        diameter = '1/2"';
        system.assertEquals(sika_SCCZoneOptimizeMix.spRangeEncode(diameter, reinforcement), '14 mm HR');
        diameter = '3/4"';
        system.assertEquals(sika_SCCZoneOptimizeMix.spRangeEncode(diameter, reinforcement), '20 mm HR');
        diameter = 'none!';
        system.assertEquals(sika_SCCZoneOptimizeMix.spRangeEncode(diameter, reinforcement), null);
    }

    

    @isTest static void testMeasureEncode(){
        system.assertEquals(sika_SCCZoneOptimizeMix.measureEncode('Metric (SI)'), 'metric');
        system.assertEquals(sika_SCCZoneOptimizeMix.measureEncode('US Customary'), 'standard');
        system.assertEquals(sika_SCCZoneOptimizeMix.measureEncode('NOTA'), null);
    }
    
    @isTest static void testOptimizeForEncode(){
        system.assertEquals(sika_SCCZoneOptimizeMix.optimizeForEncode('W/C Ratio'), 'WC');
        system.assertEquals(sika_SCCZoneOptimizeMix.optimizeForEncode('[anything else]'), 'Strength');
    }
    
    
}