public with sharing class EmailToCaseService {

    public static void createChatterFile(List<Attachment> attachments) {
        Set<Id> attachmentParents = Pluck.ids('ParentId', attachments);
        Map<Id, EmailMessage> emailMessagesById = new Map<Id, EmailMessage>([SELECT Id, ParentId FROM EmailMessage WHERE Id IN :attachmentParents]);

        List<Attachment> attachmentsWithEmailMessageParent = new List<Attachment>();
        List<ContentVersion> contentVersionsToInsert = new List<ContentVersion>();
        List<FeedItem> feedItemsToInsert = new List<FeedItem>();

        // Filter out attachments that do not have an email message parent
        for (Attachment att : attachments) {
            if (emailMessagesById.containsKey(att.ParentId)) {
                attachmentsWithEmailMessageParent.add(att);
            }
        }

        for (Attachment att : attachmentsWithEmailMessageParent) {
            ContentVersion cv = new ContentVersion();
            cv.Title = att.Name;
            cv.VersionData = att.Body;
            cv.PathOnClient = att.Name;
            cv.ContentLocation = 'S';
            cv.Origin = 'H';
            contentVersionsToInsert.add(cv);
        }
        insert contentVersionsToInsert;

        for (Integer i = 0; i < attachmentsWithEmailMessageParent.size(); i++) {
            EmailMessage em = emailMessagesById.get(attachmentsWithEmailMessageParent.get(i).ParentId);

            FeedItem fi = new FeedItem();
            fi.Body = 'Email attachment for case: '+ em.ParentId;
            fi.ParentId = em.ParentId;
            fi.RelatedRecordId = contentVersionsToInsert.get(i).Id;
            fi.Type = 'ContentPost';
            feedItemsToInsert.add(fi);
        }
        insert feedItemsToInsert;
    }
}