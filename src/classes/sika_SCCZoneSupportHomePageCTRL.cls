public without sharing class sika_SCCZoneSupportHomePageCTRL {
	
	private Id userId {get; set;}

	// constructor
	public sika_SCCZoneSupportHomePageCTRL() {
		sika_SCCZoneCheckAuth.setAuthStatus(UserInfo.getProfileId());

		userId = UserInfo.getUserId();
	}

	// check user auth status
    public PageReference loadAction(){
        PageReference authCheck;

        // because this controller runs "without sharing" we need to manually confirm that the user should have access. (A user whose session has timed-out, or an otherwise unauthenticated user who accessed this page directly, would be able to view the page otherwise, albeit without any data displayed.)
        authCheck = sika_SCCZoneCheckAuth.doAuthCheck();
        if(authCheck != null){
            return authCheck;
        }
        return null;
    }

	public list<Case> getCases(){
		list<Case> theCases = [SELECT Id, 
									  Subject, 
									  SCC_Zone_Case_Types__c, 
									  Status, 
									  CreatedDate, 
									  LastModifiedDate, 
									  CreatedById 
								 FROM Case 
								WHERE CreatedById = :userId 
								ORDER BY lastModifiedDate DESC];

		return theCases;
	}

}