/**
* @author Pavel Halas
* @company Bluewolf, an IBM Company
* @date 6/2016
*
* Shared logic for Warranty__c object.
*/
public with sharing class WarrantyService {

    private static final RecordType ROOFING_RECORD_TYPE = [SELECT Id FROM RecordType WHERE SObjectType = 'Warranty__c' AND DeveloperName = 'Roofing'];
    private static final User RUNNING_USER = [SELECT Name, Title FROM User WHERE Id = :System.UserInfo.getUserId()];
    
    /**
     * Creates a new instance of a Warranty__c object with dates defaulted to today and current user information.
     */
    public static Warranty__c initWarranty(Opportunity opp, Boolean zeroValueWarranty) {
        Date nowDate = (System.now()).date();
        Warranty__c warranty = new Warranty__c();

        if (opp.AccountId != null) {
            warranty.Applicator__c = opp.AccountId;
            warranty.Applicator_Name__c = opp.Account.Name;
        }
        if (opp.Roofing_Primary_Contact__c != null) {
            warranty.Applicator_Contact__c = opp.Roofing_Primary_Contact__c;
        }

        warranty.Applicator_Contact_Phone__c = opp.Account.SAP_Phone__c;
        warranty.Entered_By__c  = RUNNING_USER.Name;
        warranty.Title_of_Entered_By__c = RUNNING_USER.Title;
        warranty.Date_of_Entered_By__c = nowDate;
        warranty.Revision_Date__c = nowDate;
        warranty.Opportunity__c = opp.Id;
        warranty.Substantial_Completion_Date__c = nowDate;
        warranty.Inspection_Date__c = nowDate;
        if (zeroValueWarranty) {
            warranty.Warranty_Start_Date__c = opp.Anticipated_Start_Date__c.date();
            warranty.Wty_St_Date__c = opp.Anticipated_Start_Date__c.date();
        } else {
            warranty.Warranty_Start_Date__c = nowDate;
            warranty.Wty_St_Date__c = nowDate;
        }
        warranty.Date_Signed_by_President__c = nowDate;
        warranty.RecordTypeId = ROOFING_RECORD_TYPE.Id;
        return warranty;
    }

    public static void issueWarranties(Map<Id, List<Building_Group__c>> buildingGroupsByOppId) {
        Map<Id, Opportunity> opportunitiesByIds = new Map<Id, Opportunity>([SELECT Id, AccountId, Account.Name, Account.SAP_Phone__c, Roofing_Primary_Contact__c,
                Roofing_Primary_Contact__r.Phone, Anticipated_Start_Date__c FROM Opportunity WHERE Id IN :buildingGroupsByOppId.keySet()]);
        Map<Id, List<BuildingGroupService.BuildingGroupWrapper>> buildingGroupWrappersByOppId = new Map<Id, List<BuildingGroupService.BuildingGroupWrapper>>();
        Map<Id, Warranty__c> warrantyByOppId = new Map<Id, Warranty__c>();

        for (Id oppId : buildingGroupsByOppId.keySet()) {
            Warranty__c warranty = initWarranty(opportunitiesByIds.get(oppId), true); // all building groups have zero value warranties here
            warranty.Roofing_Zero_Warranty_Product__c = true;
            warranty.Substantial_Completion_Date__c = opportunitiesByIds.get(oppId).Anticipated_Start_Date__c.date();
            warranty.Expected_Start_Date__c = opportunitiesByIds.get(oppId).Anticipated_Start_Date__c.date();
            warranty.Warranty_Status__c = 'Open';

            List<BuildingGroupService.BuildingGroupWrapper> buildingGroupWrappers = BuildingGroupService.constructBuildingGroupWrappers(buildingGroupsByOppId.get(oppId), true);
            Map<Warranty__c, List<BuildingGroupService.BuildingGroupWrapper>> returnedWrappersByWarranties = issueWarranty(buildingGroupWrappers, warranty);
            if (returnedWrappersByWarranties.size() > 0) {
                Warranty__c w = new List<Warranty__c>(returnedWrappersByWarranties.keySet()).get(0);
                warrantyByOppId.put(w.Opportunity__c, w);
                buildingGroupWrappersByOppId.put(w.Opportunity__c, returnedWrappersByWarranties.get(w));
            }
        }
        if(buildingGroupWrappersByOppId.size() > 0) {
            List<Building_Group__c> toBeUpdated = new List<Building_Group__c>();
            System.Savepoint savepoint = Database.setSavepoint();

            try {
                insert new List<Warranty__c>(warrantyByOppId.values());
            } catch(DmlException e) {
                EmailUtils.sendSimpleEmail(UserInfo.getUserEmail(), 'An error occurred inserting warranties: ', e.getMessage());
            }

            for (Id oppId : buildingGroupsByOppId.keySet()) {
                for (BuildingGroupService.BuildingGroupWrapper bgw : buildingGroupWrappersByOppId.get(oppId)) {
                    if (bgw.issueWarranty && bgw.buildingGroup.Warranty__c == null) {
                        bgw.buildingGroup.Warranty__c = warrantyByOppId.get(oppId).Id;
                        toBeUpdated.add(bgw.buildingGroup);
                    }
                }
            }

            try {
                BuildingGroupService.updateBuildingGroups(toBeUpdated);
            } catch(DmlException e) {
                Database.rollback(savepoint);
                EmailUtils.sendSimpleEmail(UserInfo.getUserEmail(), 'An error occurred updating building groups: ', e.getMessage());
            }
        }
    }

    public static Map<Warranty__c, List<BuildingGroupService.BuildingGroupWrapper>> issueWarranty(List<BuildingGroupService.BuildingGroupWrapper> buildingGroupWrappers, Warranty__c warranty) {
        Id intendedWarrantyProductId;
        Id warrantyExtendedProductId;
        Decimal windspeed;

        Boolean issueWarranty = false;

        // All the building groups that are being issued a warranty need to have the same Intended_Warranty_Product__c and
        // Warranty_Extended_Product__c. Here a map is created with all the combinations, if we have more than one
        // combination it means we don't meet the requirement, and therefore the warranty is not issued.
        Set<String> uniquePairs = new Set<String>();
        for (BuildingGroupService.BuildingGroupWrapper bgw : buildingGroupWrappers) {
            if (bgw.issueWarranty && bgw.buildingGroup.Warranty__c == null) {
                intendedWarrantyProductId = bgw.buildingGroup.Intended_Warranty_Product__c;
                warrantyExtendedProductId = bgw.buildingGroup.Warranty_Extended_Product__c;
                windspeed = bgw.buildingGroup.Windspeed_MPH__c;
                String pairString = String.valueOf(intendedWarrantyProductId) +
                        String.valueOf(warrantyExtendedProductId) + String.valueOf(windspeed);
                uniquePairs.add(pairString);
            }
        }

        // Check that we only have one combination of Intended_Warranty_Product__c and Warranty_Extended_Product__c.
        issueWarranty = uniquePairs.size() == 1;

        // Link back warranty and building groups (if the warranty has been issued)
        if (issueWarranty) {
            warranty.Intended_Warranty_Product__c = intendedWarrantyProductId;
            warranty.Warranty_Extended_Product__c = warrantyExtendedProductId;
            warranty.Windspeed__c = windspeed;
        } else {
            return new Map<Warranty__c, List<BuildingGroupService.BuildingGroupWrapper>>();
        }
        return new Map<Warranty__c, List<BuildingGroupService.BuildingGroupWrapper>> {warranty => buildingGroupWrappers};
    }
}