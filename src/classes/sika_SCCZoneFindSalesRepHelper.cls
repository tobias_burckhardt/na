public without sharing class sika_SCCZoneFindSalesRepHelper {
	
	// Method for determining who an SCC Zone user's salesperson (or salespeople) are. Given an Account ID, this returns a map of AccountTeamMember role (either the SCC Zone sales reps or Concrete BU reps), to AccountTeamMember User IDs.
    public static map<String, set<Id>> findTheSCCSalesReps(Id userAccountId) {
        set<Id> sccRepsIds = new set<Id>();
        set<Id> buRepsIds = new set<Id>();
        map<String, set<Id>> role_to_userIDs = new map<String, set<Id>>();

        String SCCZoneRepAcctTeamRole = Sika_SCCZone_Common.SCCZoneSalesRepATMRole;

        list<AccountTeamMember> teamMembers = [SELECT AccountID,
                                                      TeamMemberRole,
                                                      UserId
                                                 FROM AccountTeamMember
                                                WHERE (TeamMemberRole = :Sika_SCCZone_Common.SCCZoneSalesRepATMRole OR TeamMemberRole = 'Concrete BU') 
                                                  AND AccountID = :userAccountId];

        for(accountTeamMember t : teamMembers){
            if(t.TeamMemberRole == SCCZoneRepAcctTeamRole){
                sccRepsIds.add(t.UserId);
                continue;
            } else {
                buRepsIds.add(t.UserId);
                continue;
            }
        }
        role_to_userIDs.put('SCC', sccRepsIds);
        role_to_userIDs.put('BU', buRepsIds);

        if(!role_to_userIDs.isEmpty()){
            return role_to_userIDs;
        } else {
            return new map<String, set<Id>>();
        }

    }


    public static Id findOneSCCSalesRep(Id userAccountId){ 
        map<string, set<Id>> theReps = findTheSCCSalesReps(userAccountId);
        Id firstRepsId;
        if((theReps.get('SCC').size() > 0)){
            for(Id i : theReps.get('SCC')){
                firstRepsId = i;
                break;
            }
        } else {
            if((theReps.get('BU').size() > 0)){
                for(Id i : theReps.get('BU')){
                    firstRepsId = i;
                    break;
                }
            }
        }
        Id theRepId = firstRepsId != null ? firstRepsId : null;
        return theRepId;

    }


    // Given a set of Account IDs, return a map of AccountIDs => a set of (preferred) rep IDs.  (We give preference to AccountTeamMembers with the SCC Zone sales rep type, but, if none are found, we'll settle for Concrete BU sales reps instead.)
    public static map<Id, set<Id>> getAccountIDsAndRepIDs(set<Id> accountIDs){
        map<String, set<Id>> repType_to_repIDs;
        map<Id, set<Id>> accountId_to_repIDs = new map<Id, set<Id>>();
        set<Id> repIDs = new set<Id>();
        
        for(Id accountId : accountIDs){
            // get the sales reps for each account
            repType_to_repIDs = sika_SCCZoneFindSalesRepHelper.findTheSCCSalesReps(accountId);
            
            // grab the preferred sales reps
            if(!repType_to_repIDs.isEmpty()){
                // if any SCC Zone sales reps are on the account, prefer those
                if(repType_to_repIDs.get('SCC') != null){
                    repIDs = repType_to_repIDs.get('SCC');
                } else {  // otherwise, use the Concrete BU sales reps, if any
                    repIDs = repType_to_repIDs.get('BU');
                }
            }

            // map the accountID to the set of preferred sales rep(s) for that account
            if(!repIDs.isEmpty()){
                if(accountId_to_repIDs.get(accountId) == null){
                    accountId_to_repIDs.put(accountId, repIDs);
                } else { // this will happen when multiple objects are associated with the same account.
                    accountId_to_repIDs.put(accountId, repIDs);
                }
            }
        }
        if(!accountId_to_repIDs.isEmpty()){
            return accountId_to_repIDs;
        } else {
            return new map<Id, set<Id>>();
        }
    }

}