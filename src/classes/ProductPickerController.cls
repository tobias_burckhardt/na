public with sharing class ProductPickerController {
	
	public List<Apttus_Config2__ClassificationName__c> categories {get;set;}
	public List<Apttus_Config2__ClassificationHierarchy__c> classifications {get;set;}
	public List<Apttus_Config2__ProductClassification__c> productClassifications {get;set;}

	public List<SelectOption> categorySO {get;set;}
	public List<SelectOption> classificationSO {get;set;}
	public List<SelectOption> productClassificationSO {get;set;}

	public Id selectedCategory {get;set;}
	public Id selectedClassification {get;set;}
	public Id selectedProduct {get;set;}

	public ProductPickerController() {
		GetCategory();
	}

	/*
	https://sikana--pts.cs19.my.salesforce.com/01I290000008gui?setupid=CustomObjects
	Apttus_Config2__ClassificationName__c
	//Apttus_Config2__Active__c, Apttus_Config2__HierarchyLabel__c, Apttus_Config2__GuidePage__c, Apttus_Config2__Type__c
	*/
	public void GetCategory(){
		categories = new List<Apttus_Config2__ClassificationName__c>();
		categorySO = new List<SelectOption>();
		categorySO.add(new SelectOption('', 'None'));

		for(Apttus_Config2__ClassificationName__c cat :[SELECT Id, Name 
			FROM Apttus_Config2__ClassificationName__c]){

			categories.add(cat);
			categorySO.add(new SelectOption(cat.Id, cat.Name));
		}
	}

	/*
	https://sikana--pts.cs19.my.salesforce.com/01I290000008guh?setupid=CustomObjects
	Apttus_Config2__ClassificationHierarchy__c
	*/
	public void GetClassification(){
		classifications 	= new List<Apttus_Config2__ClassificationHierarchy__c>();
		classificationSO 	= new List<SelectOption>();
		classificationSO.add(new SelectOption('', 'None'));

		for(Apttus_Config2__ClassificationHierarchy__c cl :[SELECT Id, Name 
			FROM Apttus_Config2__ClassificationHierarchy__c
			WHERE Apttus_Config2__HierarchyId__c = :selectedCategory]){

			classifications.add(cl);
			classificationSO.add(new SelectOption(cl.Id, cl.Name));
		}		
	}

	/*
	https://sikana--pts.cs19.my.salesforce.com/01I290000008gw7?setupid=CustomObjects
	https://sikana--pts.cs19.my.salesforce.com/p/setup/layout/LayoutFieldList?type=Product2&setupid=CustomObjects
	Apttus_Config2__ProductClassification__c
	Apttus_Config2__ProductClassification__r.Apttus_Config2__ProductId__c	
	*/
	public void GetProductClassifications(){
		productClassifications 	= new List<Apttus_Config2__ProductClassification__c>();
		productClassificationSO = new List<SelectOption>();
		productClassificationSO.add(new SelectOption('', 'None'));

		for(Apttus_Config2__ProductClassification__c pcl :[SELECT Id, Name, Product_Name__c, Apttus_Config2__ProductId__c
			FROM Apttus_Config2__ProductClassification__c
			WHERE Apttus_Config2__ClassificationId__c = :selectedClassification]){

			productClassifications.add(pcl);
			productClassificationSO.add(new SelectOption(pcl.Id, pcl.Name));
		}			
	}	

	public void AllSelected(){
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'selectedCategory: ' + selectedCategory));
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'selectedClassification: ' + selectedClassification));
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'selectedProduct: ' + selectedProduct));
	}
}