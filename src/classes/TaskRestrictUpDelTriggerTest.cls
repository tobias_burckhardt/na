@isTest
private class TaskRestrictUpDelTriggerTest {

    private static final String MODIFIED_SUBJECT = 'Modified Subject';

    //Creates and returns the specified number of open/closed tasks related to the given case
    private static List<Task> createTestTask(Id caseId, boolean closed, Integer num) {
        List<Task> tasks = new List<Task>();
        String status = closed ? 'Completed' : 'Not Started';
        for (Integer i = 0; i < num; i++) {
	        Task t = new Task(Subject='Test Subject ' + i, WhatId=caseId, Status=status);
            tasks.add(t);
        }
        insert tasks;
        return tasks;
    }
        
    @testSetup
    static void setUpTests() {
        RestrictedCaseTestHelper.createCustomSettings();
    }
    
    @isTest
    static void testUpdateDeleteUnrestrictedOpenTask() {
        //Trying to update/delete an open task related to a case with an unrestricted record type should succeed
        Case testCase = RestrictedCaseTestHelper.createTestCase(false);
        List<Task> testTasks = createTestTask(testCase.Id, false, 1);

        Test.startTest();
        testTasks[0].Subject = MODIFIED_SUBJECT;
        Database.SaveResult updateResult = Database.update(testTasks[0], false);
        System.assert(updateResult.isSuccess());
        
        Database.DeleteResult deleteResult = Database.delete(testTasks[0], false);
        System.assert(deleteResult.isSuccess());
        Test.stopTest();
    }
    
    @isTest
    static void testUpdateDeleteUnrestrictedClosedTask() {
        //Trying to update/delete a closed task related to a case with an unrestricted record type should succeed
        Case testCase = RestrictedCaseTestHelper.createTestCase(false);
        List<Task> testTasks = createTestTask(testCase.Id, true, 1);

        Test.startTest();
        testTasks[0].Subject = MODIFIED_SUBJECT;
        Database.SaveResult updateResult = Database.update(testTasks[0], false);
        System.assert(updateResult.isSuccess());
        
        Database.DeleteResult deleteResult = Database.delete(testTasks[0], false);
        System.assert(deleteResult.isSuccess());
        Test.stopTest();
    }
    
    @isTest
    static void testUpdateOpenTaskRestricted() {
        //Trying to update an open task related to a case with a restricted record type should succeed
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<Task> testTasks = createTestTask(testCase.Id, false, 1);

        Test.startTest();
        testTasks[0].Subject = MODIFIED_SUBJECT;
        Database.SaveResult updateResult = Database.update(testTasks[0], false);
        System.assert(updateResult.isSuccess());
        System.assertEquals(0, updateResult.getErrors().size());
        Test.stopTest();
    }
    
    @isTest
    static void testReparentOpenTaskRestricted() {
        //Trying to reparent an open task related to a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        Case newParent = RestrictedCaseTestHelper.createTestCase(true);
        List<Task> testTasks = createTestTask(testCase.Id, false, 1);

        Test.startTest();
        testTasks[0].WhatId = newParent.Id;
        Database.SaveResult updateResult = Database.update(testTasks[0], false);
        System.assert(!updateResult.isSuccess());
        System.assertEquals(1, updateResult.getErrors().size());
        System.assertEquals(System.Label.Restricted_Object_Update, updateResult.getErrors()[0].getMessage());
        Test.stopTest();
    }
    
    @isTest
    static void testUpdateClosedTaskRestricted() {
        //Trying to update a closed task related to a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<Task> testTasks = createTestTask(testCase.Id, true, 1);

        Test.startTest();
        testTasks[0].Subject = MODIFIED_SUBJECT;
        Database.SaveResult updateResult = Database.update(testTasks[0], false);
        System.assert(!updateResult.isSuccess());
        System.assertEquals(1, updateResult.getErrors().size());
        System.assertEquals(System.Label.Restricted_Object_Update, updateResult.getErrors()[0].getMessage());
        Test.stopTest();
    }
    
    @isTest
    static void testDeleteOpenTaskRestricted() {
        //Trying to delete an open task related to a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<Task> testTasks = createTestTask(testCase.Id, false, 1);

        Test.startTest();
        Database.DeleteResult deleteResult = Database.delete(testTasks[0], false);
        System.assert(!deleteResult.isSuccess());
        System.assertEquals(1, deleteResult.getErrors().size());
        System.assertEquals(System.Label.Restricted_Object_Delete, deleteResult.getErrors()[0].getMessage());
        Test.stopTest();
    }
    
    @isTest
    static void testDeleteClosedTaskRestricted() {
        //Trying to delete a closed task related to a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<Task> testTasks = createTestTask(testCase.Id, true, 1);

        Test.startTest();
        Database.DeleteResult deleteResult = Database.delete(testTasks[0], false);
        System.assert(!deleteResult.isSuccess());
        System.assertEquals(1, deleteResult.getErrors().size());
        System.assertEquals(System.Label.Restricted_Object_Delete, deleteResult.getErrors()[0].getMessage());
        Test.stopTest();
    }
    
    @isTest
    static void testUpdateUnrelatedTask() {
        //Trying to update an unrelated task (no WhatId) should succeed
        List<Task> testTasks = createTestTask(null, true, 1);

        Test.startTest();
        testTasks[0].Subject = MODIFIED_SUBJECT;
        Database.SaveResult updateResult = Database.update(testTasks[0], false);
        System.assert(updateResult.isSuccess());
        System.assertEquals(0, updateResult.getErrors().size());
        Test.stopTest();
    }

    @isTest
    static void testDeleteUnrelatedTask() {
        //Trying to delete an unrelated task (no WhatId) should succeed
        List<Task> testTasks = createTestTask(null, true, 1);

        Test.startTest();
        Database.DeleteResult deleteResult = Database.delete(testTasks[0], false);
        System.assert(deleteResult.isSuccess());
        System.assertEquals(0, deleteResult.getErrors().size());
        Test.stopTest();
    }

    @isTest
    static void testBulkUpdateClosedTasksRestricted() {
        //Trying to update closed tasks related to a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<Task> testTasks = createTestTask(testCase.Id, true, 10);

        Test.startTest();
        //Update all tasks
        for (Task tsk : testTasks) {
            tsk.Subject = MODIFIED_SUBJECT;
        }

        //Try to save all tasks
        List<Database.SaveResult> results = Database.update(testTasks, false);
        for (Database.SaveResult res : results) {
            System.assert(!res.isSuccess());
            System.assertEquals(1, res.getErrors().size());
            System.assertEquals(System.Label.Restricted_Object_Update, res.getErrors()[0].getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void testBulkDeleteClosedTasksRestricted() {
        //Trying to delete closed tasks related to a case with a restricted record type should fail
        Case testCase = RestrictedCaseTestHelper.createTestCase(true);
        List<Task> testTasks = createTestTask(testCase.Id, true, 10);

        Test.startTest();
        //Try to delete all tasks
        List<Database.DeleteResult> results = Database.delete(testTasks, false);
        for (Database.DeleteResult res : results) {
            System.assert(!res.isSuccess());
            System.assertEquals(1, res.getErrors().size());
            System.assertEquals(System.Label.Restricted_Object_Delete, res.getErrors()[0].getMessage());
        }
        Test.stopTest();
    }
}