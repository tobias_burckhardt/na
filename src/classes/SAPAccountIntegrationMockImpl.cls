@isTest
public class SAPAccountIntegrationMockImpl implements WebServiceMock {
    public static final String CUSTOMER_NUMBER = '1234';
    public static final String LANGUAGE_KEY_MISSING = 'Language key is missing.';
    
    public void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
            String requestName, String responseNS, String responseName, String responseType) {

        SAPAccountIntegration.CustomerResponse_element responseElement =
            new SAPAccountIntegration.CustomerResponse_element();

        responseElement.ResponseMessages = new List<SAPAccountIntegration.ResponseMessages_element>();
        SAPAccountIntegration.DT_CustomerCreateRequest request_x = (SAPAccountIntegration.DT_CustomerCreateRequest) request;
        responseElement.ResponseMessages.addAll(addErrorMissingFields(request_x));

        if (responseElement.ResponseMessages.isEmpty()) {
            responseElement.CustomerNumber = CUSTOMER_NUMBER;
        }

        SAPAccountIntegration.DT_CustomerCreateResponse responseObject =
            new SAPAccountIntegration.DT_CustomerCreateResponse();
        responseObject.CustomerResponse = responseElement;

        response.put('response_x', responseObject); 
    }

    private List<SAPAccountIntegration.ResponseMessages_element> addErrorMissingFields(
            SAPAccountIntegration.DT_CustomerCreateRequest request) {

        List<SAPAccountIntegration.ResponseMessages_element> errors =
            new List<SAPAccountIntegration.ResponseMessages_element>();

        if (String.isBlank(request.GeneralData.Language)) {
            SAPAccountIntegration.ResponseMessages_element error = new SAPAccountIntegration.ResponseMessages_element();
            error.MessageType = SAPIntegrationService.ERROR_TYPE;
            error.MessageDescription = LANGUAGE_KEY_MISSING;
            errors.add(error);
        }

        return errors;
    }
}