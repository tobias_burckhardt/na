@isTest
private class ExpiredQuoteUpdateTest {
    static Apttus_Proposal__Proposal__c quote;
    static Apttus_Config2__PriceList__c expectedPriceList;
    static final String PRICELIST_NAME = 'Apttus_Config2__PriceList__c';
    static void setup(){
        quote = (Apttus_Proposal__Proposal__c)new SObjectBuilder(Apttus_Proposal__Proposal__c.SObjectType)
                                                            .put(Apttus_Proposal__Proposal__c.Apttus_Proposal__Proposal_Expiration_Date__c, Date.today().addDays(-1)) 
                                                            .create().getRecord();

        expectedPriceList = (Apttus_Config2__PriceList__c)new SObjectBuilder(Apttus_Config2__PriceList__c.SObjectType)
                                                            .put(Apttus_Config2__PriceList__c.Name, PRICELIST_NAME)
                                                            .create().getRecord();
        Update_Price_List_Assignment__c assignment = (Update_Price_List_Assignment__c)new SObjectBuilder(Update_Price_List_Assignment__c.SObjectType)
                                                     .put(Update_Price_List_Assignment__c.Price_List_Name__c, PRICELIST_NAME)
                                                     .create().getRecord();

        List<Apttus_Config2__ProductConfiguration__c> configs = (List<Apttus_Config2__ProductConfiguration__c>)new SObjectBuilder(Apttus_Config2__ProductConfiguration__c.SObjectType)
                                                      .put(Apttus_Config2__ProductConfiguration__c.Apttus_QPConfig__Proposald__c, quote.Id)
                                                      .count(3)
                                                      .create()
                                                      .getRecords();
    }

    @isTest
    static void testUpdateQuote_expiredQuote(){
        setup();
        ExpiredQuoteUpdate updater = new ExpiredQuoteUpdate(new ApexPages.StandardController(quote));

        Test.startTest();
            updater.updateQuote();
        Test.stopTest();

        Apttus_Proposal__Proposal__c updatedQuote = [SELECT Apttus_QPConfig__PriceListId__c,Configurations_from_Reprice_Quote__c FROM Apttus_Proposal__Proposal__c WHERE Id = :quote.Id];
        System.assertEquals(expectedPriceList.Id, updatedQuote.Apttus_QPConfig__PriceListId__c, 'All Quotes with that are "expired"(Apttus_Proposal__Proposal_Expiration_Date__c where the due date is in the past) and do not have Apttus_QPConfig__PriceListId__c set shall be updated');
        System.assertEquals(0.0, (Decimal)updatedQuote.Configurations_from_Reprice_Quote__c, 'When the quote has been given a new pricelist; we expect the pricelist to also be updated.');

        for(Apttus_Config2__ProductConfiguration__c config : [SELECT Apttus_Config2__EffectivePriceListId__c, Apttus_Config2__PriceListId__c
                                                                FROM Apttus_Config2__ProductConfiguration__c
                                                             WHERE Apttus_QPConfig__Proposald__c = :quote.Id]){

            System.assertEquals(expectedPriceList.Id, config.Apttus_Config2__EffectivePriceListId__c, 'All configs belonging to quotes updated shall update the effective price');
            System.assertEquals(expectedPriceList.Id, config.Apttus_Config2__PriceListId__c, 'All configs belonging to quotes updated shall update the price list');
        }
        System.assertEquals(true, ApexPages.hasMessages(ApexPages.Severity.CONFIRM), 'When the update has successfully happened; we expect to see the confirmation message');
    }
}