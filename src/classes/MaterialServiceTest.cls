@isTest
private class MaterialServiceTest {
    private static final String ABS = '5';
    private static final Integer QUANTITY = 1;
    private static final Decimal MOISTURE = 5.8;
    private static final Decimal SOLID_CONTENT = 0.25;

    @TestSetup
    static void testSetup() {
        skipUnwantedMixMaterialTriggers();

        Material__c material = TestingUtils.createMaterial(false);
        material.Type__c = MixMaterialService.MATERIAL_TYPE_COARSE_AGGREGATE;
        material.Abs__c = ABS;
        material.Solid_Content__c = SOLID_CONTENT;
        insert material;

        Mix__c mix = TestingUtils.createMix(5, 'Test Mix', 0.5, 0.5, 0.5, false);
        mix.Water_Adjustment_kg__c = 5.15;
        insert mix;

        Mix_Material__c mixMaterial = TestingUtils.createMixMaterial(mix, material, false);
        mixMaterial.Moisture__c = MOISTURE;
        mixMaterial.Quantity__c = QUANTITY;
        mixMaterial.Unit__c = 'kgs';
        insert mixMaterial;
    }

    static testmethod void testUpdateMixMaterialMoistureAdjustment() {
        skipUnwantedMixMaterialTriggers();

        Material__c material =
            [SELECT Id FROM Material__c WHERE Type__c = :MixMaterialService.MATERIAL_TYPE_COARSE_AGGREGATE LIMIT 1];

        material.Type__c = MixMaterialService.MATERIAL_TYPE_ADMIXTURE;

        Test.startTest();
        update material;
        Test.stopTest();

        Mix_Material__c mixMaterial =
            [SELECT Moisture_Adjustment_2__c FROM Mix_Material__c WHERE Mix__r.Mix_Name__c = 'Test Mix' LIMIT 1];

        Decimal expected =  QUANTITY * (1 - SOLID_CONTENT);
        System.assertEquals(expected, mixMaterial.Moisture_Adjustment_2__c,
            'Moisture adjustment should be calculated correctly.');
    }

    private static void skipUnwantedMixMaterialTriggers() {
        Sika_SCCZoneMixMaterialTriggerHelper.bypassAccountTeamCheck = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassPopulateMaterialTypeCounts = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassSetSCMWeightAndUnit = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculatePasteVolume = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculateTotalSolidsVolumeLiters = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculateTotalMixVolumeLiters = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculateWaterAdjustment = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassPopulateMaterialTypeWeights = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassConcreteCurveCalc = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculateTotalMaterialCost = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassAddOverLimitErrors = true;
        Sika_SCCZoneMixMaterialTriggerHelper.bypassMixOptimizationCheck = true;
        Sika_SCCZoneMaterialTriggerHelper.bypassAccountTeamCheck = true;
        Sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = true;
        Sika_SCCZoneMaterialTriggerHelper.bypassMixOwnerTransferCheck = true;
        Sika_SCCZoneMaterialTriggerHelper.bypassPreventDelete = true;
        Sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = true;
    }
}