@isTest
private class LightningSobjectsAndDescribeTest {
    
    static TestMethod void testConstructor_default() {
    
        Test.startTest();
            LightningSobjectsAndDescribe sobjectsAndDescribe = new LightningSobjectsAndDescribe();
        Test.stopTest();
        
        System.assertNotEquals(null, sobjectsAndDescribe.sobjects,
            'we expect a map of string -> list<sobject> to be instantiated');
        System.assert(sobjectsAndDescribe.sobjects.keySet().isEmpty(),
            'we expect that by default, the map has no key value pairs');
        System.assertNotEquals(null, sobjectsAndDescribe.describe,
            'we expect a LightningDescribe instance to be instantiated');
        System.assert(sobjectsAndDescribe.describe.objects.isEmpty(),
            'we expect that by default, the LightningDescribe instance has no LightningDescribe.ObjectInfos');        
    }

    static TestMethod void testConstructor_assignment() {
    
        Map<String, List<Sobject>> passedInSobjects = new Map<String, List<Sobject>>();
        LightningDescribe passedInDescribe = new LightningDescribe();
    
        Test.startTest();
            LightningSobjectsAndDescribe sobjectsAndDescribe = new LightningSobjectsAndDescribe(
                passedInSobjects, passedInDescribe);
        Test.stopTest();
        
        System.assert(sobjectsAndDescribe.sobjects === passedInSobjects,
            'we expect that the sobjects map passed in is directly assigned to the "sobjects" instance property');
        System.assert(sobjectsAndDescribe.describe === passedInDescribe,
            'we expect that the LightningDescribe passed in is directly assigned to the "describe" instance property');
    }

}