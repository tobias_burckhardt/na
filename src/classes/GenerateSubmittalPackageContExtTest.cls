/**
* @author Pavel Halas
* @company Bluewolf, an IBM Company
* @date 6/2016
* 
* Test for GenerateSubmittalPackageContExt.
*/
@IsTest
public class GenerateSubmittalPackageContExtTest {
    static Building_Group__c buildingGroup;
    static ContentVersion contentVersion;

    @testSetup static void setupCustomSetting() {
      AWS_Settings__c awsSetting = new AWS_Settings__c(AWS_Key__c = 'Test', S3_Bucket_Name__c = 'Test', SF_Library_Name__c = 'Test');
      insert awsSetting;
    }

    static void setUp() {
        Product2 product = (Product2) TestFactory.createSObject(new Product2(Name = 'Test'), true);
        TestFactory.createSObject(new Product2(Name = 'NotMatching'), true);
        buildingGroup = (Building_Group__c) TestFactory.createSObject(new Building_Group__c(Building_Group_Name__C = 'Test'), true);
        Building_Group_Product__c buildingGroupProduct = (Building_Group_Product__c) TestFactory.createSObject(new Building_Group_Product__c(Product_lookup__c = product.Id, Building_Group__c = buildingGroup.Id), true);

        contentVersion = (ContentVersion) TestFactory.createSObject(new ContentVersion(Title = 'Test', PathOnClient= 'Test.pdf', IsMajorVersion = true, Related_Product__c = product.Id, VersionData = Blob.valueOf('Test Content')), true);
        TestFactory.createSObject(new ContentVersion(Title = 'NotMatching', PathOnClient = 'nowhere.txt', VersionData = Blob.valueOf('Test Content')), true);
    }

    //@IsTest
    //static void testGetMetadata() {
    //    setup();
        
    //    Test.startTest();
    //    List<ContentVersion> versions = GenerateSubmittalPackageContExt.getMetadata(buildingGroup.Id);
    //    Test.stopTest();
        
    //    System.assertEquals(1, versions.size());
    //    System.assertEquals(contentVersion.Id, versions[0].Id);
    //}
    
    @IsTest
    static void testGetData() {
        setup();
        
        // To fetch the ContentDocumentId
        contentVersion = [select Id, ContentDocumentId from ContentVersion where Id = :contentVersion.Id];
        
        Test.startTest();
        List<ContentService.ContentVersionWrapper> wrappers = GenerateSubmittalPackageContExt.getData(new List<String> { contentVersion.ContentDocumentId });
        Test.stopTest();
        
        System.assertEquals(1, wrappers.size());
        System.assertEquals(contentVersion.Id, wrappers[0].Id);
        System.assertEquals(contentVersion.ContentDocumentId, wrappers[0].ContentDocumentId);
        System.assert(wrappers[0].VersionData != null);
    }
}