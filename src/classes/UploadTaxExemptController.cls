/*
* Author: Martin Kona
* Company: Bluewolf
* Date: April 29, 2016
* Description: Controller for multi file upload component
* 
*/
 
public with sharing class UploadTaxExemptController 
{
    // the parent object id
    public Id sobjId {get; set;}
    
    // actual Opportunity    
    public Opportunity opp {
        get {
            if (opp == NULL) {
                opp = [SELECT UserRecordAccess.HasReadAccess, UserRecordAccess.HasDeleteAccess 
                       FROM Opportunity 
                       WHERE Id = :sobjId 
                       LIMIT 1];    
            }
            return opp;
        }
        set;
    }
    
    // list of existing Contents populated on demand
    public List<Attachment> contents;
    
    // list of new Contents to add
    public List<Attachment> newContents {get; set;}
    
    // the number of new Content sto add to the list when the user clicks 'Add More'
    public static final Integer NUM_CONTENTS_TO_ADD=5;

    // constructor
    public UploadTaxExemptController()
    {
        // instantiate the list with a single Content
        newContents = new List<Attachment>{new Attachment()};                         
    }   
 
    public UploadTaxExemptController(ApexPages.StandardController stdController)
    {
        // instantiate the list with a single Content
        newContents = new List<Attachment>{new Attachment()};                         
    }  
    
    // retrieve the existing Contents
    public List<Attachment> getContents()
    {
        if (null == contents) {
            /*List <FeedItem> fI = [SELECT RelatedRecordId FROM FeedItem where ParentId = :sobjId];
            // get RelatedRecordId's into List
            List<String> relatedIds = new List<String>();
            for (FeedItem feed : fI) {
                relatedIds.add(feed.RelatedRecordId);
            }
            // parent traverse relationship doesn't seem to work, so im doing separate query
            //contents = [SELECT Id, Title, Description FROM ContentVersion WHERE Id IN :relatedIds];
            //*/
            
            contents = [SELECT Id, Name, Description, CreatedDate 
                        FROM Attachment 
                        WHERE parentId = :sobjId
                        ORDER BY CreatedDate DESC];
        }
        
        return contents;
    }

    // Add more Attachment action method
    public void addMore()
    {
        // append NUM_CONTENTS_TO_ADD to the new Attachment list
        for (Integer idx = 0; idx < NUM_CONTENTS_TO_ADD; idx++)
        {
            newContents.add(new Attachment());
        }
    }    
    
    // Save action method
    public PageReference save() {
        List<Attachment> contentsToInsert = new List<Attachment>();
        //List<FeedItem> feedItemToInsert = new List<FeedItem>();
        
        Boolean hasBody = FALSE;
        
        for (Attachment newCont : newContents)
        {
            if (newCont.Body != NULL) {
                newCont.parentId = sobjId;                
                hasBody = TRUE;
                contentsToInsert.add(newCont);                
            } 
        }
        
        // if at least one file is inserted, then opportunity “Tax doc uploaded” checkbox is checked
        if(hasBody) {
            Opportunity opp = new Opportunity(Id = sobjId);
            opp.Tax_doc_uploaded__c = TRUE;
            update opp;     
        }
        
        try {
            insert contentsToInsert;            
        } catch (DMLException  e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file'));
            return null;            
        } finally {
            contentsToInsert.clear();
        } 

        
        /* assign content versions to feed item to relate them with Opportunity record
        for (ContentVersion cv : contentVersionToInsert) {
            FeedItem fi = 
                new FeedItem(Body = cv.Title + ' file uploaded from Upload Tax exempt process.', 
                             ParentId = sobjId, 
                             RelatedRecordId = cv.Id, 
                             Type = 'ContentPost'); 

            feedItemToInsert.add(fi);
            
        }
        
        insert feedItemToInsert;        
        */
       
        newContents.clear();
        newContents.add(new Attachment());
        
        // redirect to same page so we loose form data
        PageReference myPage = Page.UploadTaxExempt;
        myPage.setRedirect(true);
        myPage.getParameters().put('id', sobjId); 
        
        // null the list of existing contents - this will be rebuilt when the page is refreshed
        contents = null;
        return myPage;
    }
    
    // Action method when the user is done
    public PageReference done() {
        // save ContentVersions
        save();
        // send the user to the detail page for the sobject
        return new PageReference('/' + sobjId);
    }   
    
    // delete Content
    public void deleteContent() {
        // get Content id from URL
        Id contentId = ApexPages.currentPage().getParameters().get('contentId');
        System.debug('>>> Delete Content with Id: ' + contentId);
        if (contentId != NULL) {
            /*Id contentDocumentId = [ SELECT ContentDocumentId 
                                  FROM ContentVersion 
                                  WHERE Id = :contentId 
                                  LIMIT 1].ContentDocumentId ; 
            
            delete new ContentDocument(id=contentDocumentId); */
            Attachment atch = new Attachment(id = contentId);
            delete atch;
            
            // remove this content from list in loop instead of re-initialize list with query
            for (Integer i = 0; i < contents.size(); ++i) {
                if (contents.get(i).Id == contentId)
                    contents.remove(i);
            }
        }
    }
    
    /*public PageReference viewContentVersion() {
        Id contentId = ApexPages.currentPage().getParameters().get('contentId');        
        String sfdcURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + contentId; 
                
        return new PageReference(sfdcURL);
    } */
}