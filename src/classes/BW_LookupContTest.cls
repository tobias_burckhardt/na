@isTest
private class BW_LookupContTest {

    static Integer RECORD_COUNT = 200;
    static List<String> fieldPaths;
    static String objectName;
    static String allAccountsTag;
    static String allContactsTag;
    static BW_LookupCont.QueryFilter qFilt;

    static void setupQuery() {
        fieldPaths = new List<String>{'LastName'};
        objectName = 'Contact';
        allAccountsTag = 'accounts';
        allContactsTag = 'contacts';
        UnitTest.createTestData(TestingUtils.generatorOf(Account.SobjectType)).many(2)
                .tag(allAccountsTag)
                .insertAll();
        UnitTest.forEach(allAccountsTag).create(RECORD_COUNT/2, TestingUtils.generatorOf(Contact.SobjectType))
                .tag(allContactsTag);
        UnitTest.get(allContactsTag).insertAll();        
        qFilt = null;
    }
    
    static BW_LookupCont.QueryFilter comparativeFilterFactory(String fieldPath, String operator, Object value) {
        BW_LookupCont.QueryFilter filter = new BW_LookupCont.QueryFilter();
        filter.fieldPath = fieldPath;
        filter.operator = operator;
        filter.value = (String) value;
        return filter;
    }
    
    static BW_LookupCont.QueryFilter compositionalFilterFactory(String operator, List<BW_LookupCont.QueryFilter> subfilters) {
        BW_LookupCont.QueryFilter filter = new BW_LookupCont.QueryFilter();
        filter.operator = operator;
        filter.subfilters = subfilters;
        return filter;
    }
    
    static TestMethod void testDfieldsAll() {
        SobjectType someObject = Account.SobjectType;
        objectName = someObject.getDescribe().getName();
        
        Test.startTest();
            LightningDescribe describe = BW_LookupCont.dfieldsAll(objectName);
        Test.stopTest();
        
        System.assertEquals(
            new Set<String>{objectName},
            describe.objects.keySet(),
            'we expect a description to be returned for the provided object');
        LightningDescribe.ObjectInfo objInfo = describe.objects.get(objectName);
        for (SobjectField sobjField : 
            someObject.getDescribe().fields.getMap().values()) {
            String fieldName = sobjField.getDescribe().getName();
            System.assert(
                objInfo.fields.containsKey(fieldName),
                'we expect a description to be returned for each field of the object, including object field ' + fieldName
            );
            System.assertEquals(
                LightningDescribe.BASELINE_FIELD_READS,
                objInfo.fields.get(fieldName).hasProps,
                'we expect the description to describe each field of the object with sufficient information for interpreting its data'
            );
        }
    }
    
    static TestMethod void testDynamicQuery_fieldPaths() {
    
        setupQuery();
        //fieldPaths.add('Account.Name');
        
        Test.startTest();
            List<Contact> returned = BW_LookupCont.dynamicQuery(
                fieldPaths, objectName, null);
        Test.stopTest();
        
        System.assertEquals(
            UnitTest.getIds(allContactsTag),
            pluck.ids(returned),
            'we expect any records of the specified object name to be returned'
        );
        System.assert(
            ! pluck.strings('LastName', returned).contains(null),
            'we expect any fieldpaths specified to be returned to be returned on the records'
        );
        /*for (Contact record : returned) {
            System.assertNotEquals(null, record.account.Name,
                'we expect fieldpaths to be returned even if they are on related records');
        }*/
    }
    
    static TestMethod void testDynamicQuery_comparativeFilter() {
    
        setupQuery();
        String MATCHING_LAST_NAME = 'Jones Jr.';
        UnitTest.get(allContactsTag)
            .branch('matching | control')
            .filter('matching')
            .assign('LastName', MATCHING_LAST_NAME)
            .updateAll();
    
        Test.startTest();
            List<Contact> returned = BW_LookupCont.dynamicQuery(
                fieldPaths, objectName,
                    JSON.serialize(comparativeFilterFactory(
                        'LastName', '=', MATCHING_LAST_NAME
                    )));
        Test.stopTest();

        System.assertEquals(
            UnitTest.getIds('matching'),
            pluck.ids(returned),
            'we expect that when serialized comparative filters are provided, they are applied in filtering the returned records'
        );
    }
    
    static TestMethod void testDynamicQuery_negationFilter() {
    
        setupQuery();
        String MATCHING_LAST_NAME = 'Jones Jr.';
        UnitTest.get(allContactsTag)
            .branch('matching | control')
            .filter('control')
            .assign('LastName', MATCHING_LAST_NAME)
            .updateAll();
    
        Test.startTest();
            List<Contact> returned = BW_LookupCont.dynamicQuery(
                fieldPaths, objectName,
                    JSON.serialize(compositionalFilterFactory('NOT',
                        new List<BW_LookupCont.QueryFilter>{
                            comparativeFilterFactory(
                                'LastName', '=', MATCHING_LAST_NAME
                            )
                       }
                    )));
        Test.stopTest();

        System.assertEquals(
            UnitTest.getIds('matching'),
            pluck.ids(returned),
            'we expect that when serialized negation filters are provided, they are applied in filtering the returned records'
        );
    }
    
    static TestMethod void testDynamicQuery_composedFilters() {
    
        setupQuery();
        String MATCHING_LAST_NAME = 'Jones Jr.';
        String MATCHING_FIRST_NAME = 'Henry';
        UnitTest.get(allContactsTag)
                .branch('matchingLast | controlLast')
                .branch('matchingFirst | controlFirst');
        UnitTest.get('matchingLast')
                .assign('LastName', MATCHING_LAST_NAME);
        UnitTest.get('matchingFirst')
                .assign('FirstName', MATCHING_FIRST_NAME);
        UnitTest.get('matchingFirst').filter('matchingLast').tag('matching');
        UnitTest.get(allContactsTag).updateAll();
    
        Test.startTest();
            List<Contact> returned = BW_LookupCont.dynamicQuery(
                fieldPaths, objectName,
                    JSON.serialize(compositionalFilterFactory('AND',
                        new List<BW_LookupCont.QueryFilter>{
                            comparativeFilterFactory(
                                'LastName', '=', MATCHING_LAST_NAME
                            ),
                            comparativeFilterFactory(
                                'FirstName', '=', MATCHING_FIRST_NAME
                            )
                       }
                    )));
        Test.stopTest();

        System.assertEquals(
            UnitTest.getIds('matching'),
            pluck.ids(returned),
            'we expect that when serialized compositional filters are provided, they are applied in filtering the returned records'
        );
    }
    
    static TestMethod void testDynamicQuery_invalidOperator() {
    
        setupQuery();
        AuraHandledException caught;

        Test.startTest();
            try {
                List<Contact> returned = BW_LookupCont.dynamicQuery(
                    fieldPaths, objectName,
                        JSON.serialize(comparativeFilterFactory(
                                'LastName', 'quack', 'quack'
                       )));
            } catch (AuraHandledException e) {
                caught = e;
            }
        Test.stopTest();

        System.assertNotEquals(null, caught,
            'we expect a message to be provided to the client if an invalid operator is provided for a filter');
    }
    static TestMethod void testDynamicQuery_invalidFieldPath() {
    
        setupQuery();
        AuraHandledException caught;

        Test.startTest();
            try {
                List<Contact> returned = BW_LookupCont.dynamicQuery(
                    fieldPaths, objectName,
                        JSON.serialize(comparativeFilterFactory(
                                null, '=', 'whatever'
                       )));
            } catch (AuraHandledException e) {
                caught = e;
            }
        Test.stopTest();

        System.assertNotEquals(null, caught,
            'we expect a message to be provided to the client if an invalid field is provided for a filter');
    }    
}