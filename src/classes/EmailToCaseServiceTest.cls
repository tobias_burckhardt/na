@IsTest
public with sharing class EmailToCaseServiceTest {

    @TestSetup static void setupCustomSetting() {
        AWS_Settings__c awsSetting = new AWS_Settings__c(AWS_Key__c = 'Test', S3_Bucket_Name__c = 'Test', SF_Library_Name__c = 'Test');
        insert awsSetting;
    }

    private static void createEmailWithAttachments(Id parentId, Integer num) {
        EmailMessage em = new EmailMessage();
        em.ParentId = parentId;
        em.Subject = 'Test Email with Attachment';
        insert em;

        List<Attachment> atts = new List<Attachment>();

        for( Integer i = 0; i < num; i++ ) {
            Attachment att = new Attachment();
            att.Body = Blob.valueOf('Test Body');
            att.Name = 'Test Attachment';
            att.ParentId = em.Id;

            atts.add(att);
        }

        insert atts;
    }

    @IsTest
    static void testCreateChatterFileFromEmailAttachment_None() {
        //create unrestricted case
        Case testCase = RestrictedCaseTestHelper.createTestCase(false);

        Test.startTest();
        List<FeedItem> caseFeedItems = [SELECT Id, RelatedRecordId FROM FeedItem WHERE ParentId = :testCase.Id];
        Set<Id> cvIds = new Set<Id>();

        for( FeedItem fi : caseFeedItems ) {
            cvIds.add(fi.RelatedRecordId);
        }

        List<ContentVersion> cvs = [SELECT Id, Title FROM ContentVersion WHERE Id IN :cvIds];
        System.assert(cvs.size() == 0);

        Test.stopTest();
    }

    @IsTest
    static void testCreateChatterFileFromEmailAttachment_One() {
        //create unrestricted case
        Case testCase = RestrictedCaseTestHelper.createTestCase(false);

        Test.startTest();
            createEmailWithAttachments(testCase.Id, 1);
        Test.stopTest();

        List<FeedItem> caseFeedItems = [SELECT Id, RelatedRecordId FROM FeedItem WHERE ParentId = :testCase.Id];
        Set<Id> cvIds = new Set<Id>();

        for( FeedItem fi : caseFeedItems ) {
            cvIds.add(fi.RelatedRecordId);
        }

        List<ContentVersion> cvs = [SELECT Id, Title FROM ContentVersion WHERE Id IN :cvIds];
        System.assert(cvs.size() == 1);
    }

    @IsTest
    static void testCreateChatterFileFromEmailAttachment_Two() {
        //create unrestricted case
        Case testCase = RestrictedCaseTestHelper.createTestCase(false);

        Test.startTest();
            createEmailWithAttachments(testCase.Id, 2);
        Test.stopTest();

        List<FeedItem> caseFeedItems = [SELECT Id, RelatedRecordId FROM FeedItem WHERE ParentId = :testCase.Id];
        Set<Id> cvIds = new Set<Id>();

        for( FeedItem fi : caseFeedItems ) {
            cvIds.add(fi.RelatedRecordId);
        }

        List<ContentVersion> cvs = [SELECT Id, Title FROM ContentVersion WHERE Id IN :cvIds];
        System.assert(cvs.size() == 2);
    }
}