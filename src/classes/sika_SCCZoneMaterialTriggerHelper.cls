public without sharing class sika_SCCZoneMaterialTriggerHelper {
	public static Boolean bypassAccountTeamCheck = false;
	public static Boolean bypassMixMaterialUpdate = false;
	public static Boolean bypassPreventDelete = false;
	public static Boolean bypassPreventEdit = false;
	
	public static Boolean bypassMixOwnerTransferCheck = false;

	private static Boolean updateMixMaterialsHasRun = false;
	private static Boolean materialOwnerTransferCheckHasRun = false;


	// prevents salespeople from editing or deleting Material records associated with accounts on which they are not accountTeamMembers or owners.
	// the SCC Zone permission set should be added to SCC Zone salespeople, and should provide view all & modify all access to the Material object. 
	// Will only fire once per execution context.
	public static void accountTeamCheck(list<sObject> theMaterials, String actionName){
		sika_SCCZoneAccountTeamCheckHelper.accountTeamCheck(theMaterials, 'Material', actionName);
	}


	// if a material is updated, its related Mix_Material records are also (or should also be) affected. (This is here to fire any triggers associated with a Mix_Material update.)
	public static void updateMixMaterials(set<Id> theMaterialIDs){
		if(updateMixMaterialsHasRun == false){
			updateMixMaterialsHasRun = true;

			list<Mix_Material__c> theMixMaterials = [SELECT Id, Material__r.Id FROM Mix_Material__c WHERE Material__r.Id IN: theMaterialIDs];

			if(theMixMaterials.size() > 0){
				system.debug('******** Running a no-change update on all Mix_Materials associated with the following Materials, which have been passed into the sika_SCCZone_MaterialTrigger (isUpdate == true): ' + Trigger.newMap.keySet());
				update(theMixMaterials);
			}
		}
	}
	
	// if a material's owner has changed, its Account__c field should be updated
	public static void materialOwnerTransferCheck(list<Material__c> newMaterials, map<Id, Material__c> oldMaterialMap){
	    if(materialOwnerTransferCheckHasRun == false){
    		materialOwnerTransferCheckHasRun = true;

	    	set<Id> materialsChangingOwners = new set<Id>();
	    	set<Id> ownerIDs = new set<Id>();
	    	map<Id, Id> materialId_to_ownerId = new map<Id, Id>();
	    	map<Id, Id> ownerId_to_accountId = new map<Id, Id>();
	    	
	    	// create a set of IDs of the materials with different OwnerIDs in Trigger.new vs Trigger.old
	    	// also create a map Material.Id => OwnerId
	    	for(Material__c m : newMaterials){
	    		if(m.OwnerId != oldMaterialMap.get(m.Id).OwnerId){
	    			materialsChangingOwners.add(m.Id);
	    			ownerIDs.add(m.OwnerId);
	    			materialId_to_ownerId.put(m.Id, m.OwnerId);
	    		}
	    	}
	    	
	    	list<User> theUsers = [SELECT Id, AccountId FROM User WHERE Id IN :ownerIDs];
	    	for(User u : theUsers){
	    	    ownerId_to_accountId.put(u.Id, u.AccountId);
	    	}

	    	if(materialsChangingOwners.size() > 0){
	    		system.debug('******** The following material records have changed owners: ' + materialsChangingOwners);

	    		for(Material__c m : newMaterials){
	    		    m.Account__c = ownerId_to_accountId.get(materialId_to_ownerId.get(m.Id));
	    		}
	    		
	    	}
	    }
	     
	}


	// prevents Materials that are used in Mixes from being deleted
	public static void preventDelete(list<Material__c> theMaterials){
		set<Id> doNotDelete = new set<Id>();

		// get the IDs of all materials passed to the trigger that ARE included in mixes, and add their IDs to the doNotDelete set
		list<Mix_Material__c> mixMaterials = [SELECT Id, Material__c FROM Mix_Material__c WHERE Material__c IN :theMaterials];
		for(Mix_Material__c mm : mixMaterials){
			doNotDelete.add(mm.Material__c);
		}

		for(Material__c m : theMaterials){
			// if the material's ID is in the doNotDelete set, add an error to the record, thus preventing the DML operation from succeeding
			if(doNotDelete.contains(m.Id)){
				m.addError('This Material is currently being used in a Mix, and therefore cannot be deleted. Consider setting the Material\'s \'isActive\' property to false instead');
			}
		}
	}


	// prevents Materials that are used in LOCKED Mixes from being edited by anyone but a sysAdmin
	public static void preventEdit(list<Material__c> theMaterials){
		String profileName = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId() Limit 1].Name;
			
		if(profileName != 'System Administrator' && profileName != Sika_SCCZone_Common.adminUserRoleName){

			set<Id> doNotEdit = new set<Id>();

			// get the IDs of all materials passed to the trigger that are included in LOCKED mixes, and add their IDs to the doNotEdit set
			list<Mix_Material__c> mixMaterials = [SELECT Id, Material__c, Mix__r.isLocked__c FROM Mix_Material__c WHERE Material__c IN :theMaterials AND Mix__r.isLocked__c = true];
			for(Mix_Material__c mm : mixMaterials){
				doNotEdit.add(mm.Material__c);
			}

			for(Material__c m : theMaterials){
				if(doNotEdit.contains(m.Id)){
					m.addError('This Material cannot be edited. It is either associated with an account on which you are not a team member, or is currently used in a Mix that has test results attached.');
				}
			}
		}
	}

}