/**
 * Test class for FinancialReportsService class
 *
 * @author eru
 * @copyright PARX
 */
@isTest
public class Test_FinancialReportsService
{
    @testSetup
    public static void setupData()
    {
        Account testAccount = new Account(Name = 'Test account', External_ERP_ID__c = '2104_123456', Company_Code__c = '2104');
        insert testAccount;
    }

    @isTest
    public static void testSalesFiguresReport()
    {
        List<Account> testAccounts = [SELECT Id, ERP_ID__c, Company_Code__c FROM Account];
        FinancialReportsService.getSalesFiguresData(testAccounts[0].Id, 2016);
    }

    @isTest
    public static void testSalesFiguresBySalesRepReport()
    {
        List<Account> testAccounts = [SELECT Id, ERP_ID__c, Company_Code__c FROM Account];
        FinancialReportsService.getSalesFiguresBySalesRepsData(testAccounts[0].Id, null, 2016);
    }

    @isTest
    public static void testShipToSalesFiguresReport()
    {
        List<Account> testAccounts = [SELECT Id, ERP_ID__c, Company_Code__c FROM Account];
        FinancialReportsService.getShipToSalesFiguresData(testAccounts[0].Id, 2017);
    }

    @isTest
    public static void testShipToSalesFiguresBySalesRepsReport()
    {
        List<Account> testAccounts = [SELECT Id, ERP_ID__c, Company_Code__c FROM Account];
        FinancialReportsService.getShipToSalesFiguresBySalesRepsData(testAccounts[0].Id, 2017);
    }

//    @isTest
//    public static void testArticleMixReport()
//    {
//        List<Account> testAccounts = [SELECT Id, ERP_ID__c, Company_Code__c FROM Account];
//        FinancialReportsService.getArticleMixData(testAccounts[0].ERP_ID__c,testAccounts[0].Company_Code__c, '1,2,3,4,5,6,7,8,9,10,11,12', 'soldto', 2016);
//    }

//    @isTest
//    public static void testArticleMixBySalesRepsReport()
//    {
//        List<Account> testAccounts = [SELECT Id, ERP_ID__c, Company_Code__c FROM Account];
//
//        insert new ArticleMixSalesRep2Set__x(Bukrs__c = '123');
//
//        FinancialReportsService.getArticleMixBySalesRepsData(testAccounts[0].ERP_ID__c,testAccounts[0].Company_Code__c, '1,2,3,4,5,6,7,8,9,10,11,12', 'soldto', '123', 2016);
//    }

    @isTest
    public static void testProductMixReport()
    {
        List<Account> testAccounts = [SELECT Id, ERP_ID__c, Company_Code__c FROM Account];
        FinancialReportsService.getProductMixData(testAccounts[0].ERP_ID__c,testAccounts[0].Company_Code__c, '1,2,3,4,5,6,7,8,9,10,11,12', 'soldto', 2016);
    }

    @isTest
    public static void testProductMixBySalesRepsReport()
    {
        List<Account> testAccounts = [SELECT Id, ERP_ID__c, Company_Code__c FROM Account];
        FinancialReportsService.getProductMixBySalesRepsData(testAccounts[0].ERP_ID__c,testAccounts[0].Company_Code__c, '1,2,3,4,5,6,7,8,9,10,11,12', 'soldto', '123', 2016);
    }

    @isTest
    public static void testHeaderInfo()
    {
        List<Account> testAccounts = [SELECT Id FROM Account];
        FinancialReportsService.getHeaderInfo(testAccounts[0].Id);
    }

}