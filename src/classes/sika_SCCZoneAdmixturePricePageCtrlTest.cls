@isTest
private class sika_SCCZoneAdmixturePricePageCtrlTest {
	private static PageReference pageRef;
	private static sika_SCCZoneAdmixturePricePageController ctrl;
	private static User SCCZoneUser;
	private static Id theMaterialId;

	// static initializer
    static {
        
        // create an SCCZone user
        map<String, sObject> usefulStuff = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
        SCCZoneUser = (User) usefulStuff.get('SCCZoneUser');

        User adminUser = (User) usefulStuff.get('adminUser');

        // create a material (type = admixture)
        Material__c admixtureMaterial = sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'type' => 'Admixture', 'name' => 'testMaterial'});
        admixtureMaterial.Cost__c = null;  // materials of type "Admixture" don't have a price or unit associated with the material.
        admixtureMaterial.Unit__c = null;  // Rather, SCC Zone users can save their own price/unit for admixture Materials in the form of Admixture_Price records.
        admixtureMaterial.Unit_System__c = null;
        admixtureMaterial.Spec_Gravity__c = null;
        
        System.runAs(adminUser){
	        insert(admixtureMaterial);
	    }

        theMaterialId = admixtureMaterial.Id;

        sika_SCCZoneAdmixturePriceTriggerHelper.bypassMixMaterialUpdate = true;
    }

    private static void init(){
        // set page we'll be testing
        pageRef = new PageReference('/sika_SCCZoneAdmixturePricePage');
        pageRef.getParameters().put('id', theMaterialId);
        Test.setCurrentPage(pageRef);

    }


    @isTest static void testHappy(){
    	init();
        System.runAs(SCCZoneUser){
	    	ctrl = new sika_SCCZoneAdmixturePricePageController();
	    	system.assertEquals(ctrl.theMaterial.Type__c, 'Admixture');
	    	system.assertEquals(ctrl.theMaterial.Material_Name__c, 'testMaterial');
	    	system.assertEquals(ctrl.userID, SCCZoneUser.Id);

	    	ctrl.theAdmixturePrice.Price__c = 5.0;
	    	ctrl.theAdmixturePrice.Unit__c = 'kg';

	    	PageReference saveResponse = ctrl.doSave();
	    	system.assert(saveResponse.getURL().contains('/sika_SCCZoneMaterialDetailPage'));
	    	system.assert(saveResponse.getURL().contains(theMaterialId));

    	}
    }


    @isTest static void testValidation(){
    	init();
        System.runAs(SCCZoneUser){
	    	ctrl = new sika_SCCZoneAdmixturePricePageController();
	    	Boolean p;

	    	ctrl.theAdmixturePrice.Price__c = null;
	    	ctrl.theAdmixturePrice.Unit__c = 'kg';
            p = ctrl.validate();
     
            system.assert(ctrl.hasErrors == true);
            system.assert(ctrl.priceIsInvalid == true);
           
            ctrl.theAdmixturePrice.Unit__c = '';
            p = ctrl.validate();
            system.assert(ctrl.unitIsBlank == true);

            ctrl.theAdmixturePrice.Price__c = Decimal.valueOf('3.00');
            ctrl.theAdmixturePrice.Unit__c = 'kg';
            p = ctrl.validate();
            system.assert(ctrl.hasErrors == false);
            system.assert(ctrl.priceIsInvalid == false);
            system.assert(ctrl.unitIsBlank == false);
            
	    }
	}


    @isTest static void testCancel(){
    	init();
    	ctrl = new sika_SCCZoneAdmixturePricePageController();
    	System.runAs(SCCZoneUser){
	    	system.assert(ctrl.doCancel().getURL().contains('/sika_SCCZoneMaterialDetailPage'));
	    }

    }

    @isTest static void testLoadAction(){
        User guestUser = sika_SCCZoneTestFactory.createGuestUser();
        insert(guestUser);

        init();
        PageReference p;

        System.runAs(SCCZoneUser){
            ctrl = new sika_SCCZoneAdmixturePricePageController();
            p = ctrl.loadAction();
            system.assertEquals(p, null);
        }

        System.runAs(guestUser){
            ctrl = new sika_SCCZoneAdmixturePricePageController();
            p = ctrl.loadAction();
            system.assert(String.valueOf(p).contains('/SCCZone/sika_SCCZoneLoginPage'));
        }
    }

}