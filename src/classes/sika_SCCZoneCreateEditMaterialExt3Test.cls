@isTest
private class sika_SCCZoneCreateEditMaterialExt3Test {
	static Integer i;

	private static sika_SCCZoneCreateEditMaterialCtrlExt3 ext;
	private static sika_SCCZoneMaterialsHomePageController ctrl;


    private static list<Apexpages.Message> pgMessages;

    private static void init(){
    	// set the page we'll be testing (so we can read pageMessages, etc.)
		PageReference ref = new PageReference('/sika_SCCZoneCreateEditMaterialPage');
		Test.setCurrentPage(ref);

		// create an instance of the controller class and its extension class
		ctrl = new sika_SCCZoneMaterialsHomePageController();
		
		ext = new sika_SCCZoneCreateEditMaterialCtrlExt3(ctrl);

		// bypass any triggers we don't need to fire while testing
		sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = true;
		sika_SCCZoneMaterialTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = true;

    }


	@isTest static void sieveValidationHappy(){
		init();

		// create a list of sieveWrappers, each with values.  (ord 1 = 90%, ord 2 = 80%, ...ord 10 = 0%)
		list<sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper> validateWrappers = new list<sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper>();

		for(i=1; i < 10; i++){  													  // ordinal, fieldName, calculated fieldName, label, value (% passing as string), aperture size as decimal, system-of-measure
			validateWrappers.add(new sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper(i, String.valueOf(i), String.valueOf(i), String.valueOf(i), String.valueOf(100-(i*10)), Decimal.valueOf(10-i), 'Metric (SI)'));
		}

		// send the list through validation.  Should pass.
		system.assert(ext.sieveValidation(validateWrappers));

		// add one more sieveWrapper, with a larger aperture than the rest, and with no value (% passing) filled in.
		validateWrappers.add(new sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper(0, '0', '0', '0', '', Decimal.valueOf(11), 'Metric (SI)'));

		// send the list through validation. Should still pass.
		system.assert(ext.sieveValidation(validateWrappers));

	}

	@isTest static void sieveValidationSad(){
		init();

		// create a list of sieveWrappers
		list<sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper> validateWrappers = new list<sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper>();

		// add some sieveWrappers to the list, but don't include any with values
		validateWrappers.add(new sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper(1, 'sieve1', 'sieve1Calc', 'Sieve 1', '', 100, 'Metric (SI)'));
		validateWrappers.add(new sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper(2, 'sieve2', 'sieve2Calc', 'Sieve 2', '', 50, 'Metric (SI)'));
		validateWrappers.add(new sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper(3, 'sieve3', 'sieve3Calc', 'Sieve 3', '', 25, 'Metric (SI)'));

		// send the list through validation.  Should switch hasErrors == true, and put an error message on list index 0
		system.assert(ext.sieveValidation(validateWrappers) == false);
		system.assert(validateWrappers[0].hasError);
		system.assert(validateWrappers[0].errorMsg == 'Please fill in at least two sieve test values');
		system.assert(ext.hasErrors);

		validateWrappers.clear();
		validateWrappers.add(new sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper(1, 'sieve1', 'sieve1Calc', 'Sieve 1', '', 100, 'Metric (SI)'));
		validateWrappers.add(new sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper(2, 'sieve2', 'sieve2Calc', 'Sieve 2', '50', 50, 'Metric (SI)'));
		validateWrappers.add(new sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper(3, 'sieve3', 'sieve3Calc', 'Sieve 3', '60', 25, 'Metric (SI)'));
		validateWrappers.add(new sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper(4, 'sieve4', 'sieve4Calc', 'Sieve 4', '', 12.5, 'Metric (SI)'));
		validateWrappers.add(new sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper(5, 'sieve5', 'sieve5Calc', 'Sieve 5', '55', 6.25, 'Metric (SI)'));
		validateWrappers.add(new sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper(6, 'sieve6', 'sieve6Calc', 'Sieve 6', '40', 6, 'Metric (SI)'));

		ext.sieveValidation(validateWrappers);

		system.assertEquals(validateWrappers[0].theOrd, 1);
		system.assert(validateWrappers[0].hasError == false);

		system.assertEquals(validateWrappers[1].theOrd, 2);
		system.assert(validateWrappers[1].hasError == false);

		system.assertEquals(validateWrappers[2].theOrd, 3);
		system.assert(validateWrappers[2].hasError == true);
		system.assertEquals(validateWrappers[2].errorMsg, '% passing cannot be greater than ' + validateWrappers[1].theValue + '.');

		system.assertEquals(validateWrappers[3].theOrd, 4);
		system.assert(validateWrappers[3].hasError == false);

		system.assertEquals(validateWrappers[4].theOrd, 5);
		system.assert(validateWrappers[4].hasError == false);

		system.assertEquals(validateWrappers[5].theOrd, 6);
		system.assert(validateWrappers[5].hasError == false);

	}

	@isTest static void sieveValidationBad(){
		init();

		// create a list of sieveWrappers
		list<sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper> validateWrappers = new list<sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper>();

		// add some sieveWrappers to the list, including one with an invalid value
		validateWrappers.add(new sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper(1, 'sieve1', 'sieve1Calc', 'Sieve 1', '150', 100, 'Metric (SI)'));
		validateWrappers.add(new sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper(2, 'sieve2', 'sieve2Calc', 'Sieve 2', 'twenty two', 50, 'Metric (SI)'));
		validateWrappers.add(new sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper(3, 'sieve1', 'sieve3Calc', 'Sieve 3', '10', 25, 'Metric (SI)'));

		system.assert(ext.sieveValidation(validateWrappers) == false);

		system.assertEquals(validateWrappers[1].theValue, 'twenty two');
		system.assert(validateWrappers[1].hasError == true);
		system.assertEquals(validateWrappers[1].errorMsg, 'Please enter a numeric value');  // validation should return false here. Running further tests on non-numeric values would cause issues.

		validateWrappers[1].theValue = '22';
		system.assert(ext.sieveValidation(validateWrappers) == false);

		system.assertEquals(validateWrappers[0].theValue, '150');
		system.assert(validateWrappers[0].hasError == true);
		system.assertEquals(validateWrappers[0].errorMsg, '% passing must be between 0 and 100');

		validateWrappers[0].theValue = '100';

		system.assert(ext.sieveValidation(validateWrappers) == true);
		system.assert(validateWrappers[1].hasError == false);
		system.assertEquals(validateWrappers[1].errorMsg, '');
		system.assert(ext.hasErrors == false);

	}

	@isTest static void detailsValidationHappy(){
		init();

		// create a material, type = cement
		ext.theMaterial = sika_SCCZoneTestFactory.createMaterialCement();

		// run the validation on this known-to-be valid material & confirm that it passes
		system.assert(ext.detailsValidation());

		// run the validation on a known-to-be valid sand, SCM, coarse aggregate, and water. Confirm that all pass.
		ext.theMaterial = sika_SCCZoneTestFactory.createMaterialSand();
		system.assert(ext.detailsValidation());
		ext.theMaterial = sika_SCCZoneTestFactory.createMaterialSCM();
		system.assert(ext.detailsValidation());
		ext.theMaterial = sika_SCCZoneTestFactory.createMaterialCoarseAgg();
		system.assert(ext.detailsValidation());
		ext.theMaterial = sika_SCCZoneTestFactory.createMaterialWater();
		system.assert(ext.detailsValidation());

	}
	
	@isTest static void detailsValidationSadSCM1(){
		init();

		// create an SCM
		ext.theMaterial = sika_SCCZoneTestFactory.createMaterialSCM();

		// clear out its required fields
		ext.theMaterial.Material_Name__c = null;
		ext.theMaterial.Spec_Gravity__c = null;
		ext.theMaterial.Cost__c = null;
		ext.theMaterial.Unit__c = null;
		ext.theMaterial.SCM_Effective_Factor__c = null;

		// run the validation, and be sure that it fails and that the expected error messages are returned.
		system.assert(ext.detailsValidation() == false);

		pgMessages = ApexPages.getMessages();

		for(Apexpages.Message m : pgMessages){
        	system.debug('********CEM3 (SCM1) = ' + m);
        	system.assert(m.getDetail().contains('Material Name cannot be blank'));	
        	system.assert(m.getDetail().contains('Spec Gravity cannot be blank'));
        	system.assert(m.getDetail().contains('Unit cannot be blank'));	
        	system.assert(m.getDetail().contains('Cost cannot be blank'));	
        	system.assert(m.getDetail().contains('SCM Effective Factor cannot be blank'));	
			
			system.assert(m.getDetail().contains('Max absorption cannot be blank') == false);	
    	}

	}
	
	@isTest static void detailsValidationSadSCM2(){
		init();

		// create an SCM
		ext.theMaterial = sika_SCCZoneTestFactory.createMaterialSCM();

		// give its SCM Eff Factor field an invalid value.  (Must be between 0 & 2 (inclusive).)
		ext.theMaterial.SCM_Effective_Factor__c = Decimal.valueOf(3);
    	// run the validation, and be sure that it fails and that the expected error messages are returned.
		system.assert(ext.detailsValidation() == false);

		pgMessages = ApexPages.getMessages();

    	system.assert(ext.detailsValidation() == false);
    	for(Apexpages.Message m : pgMessages){
        	system.assert(m.getDetail().contains('SCM Effective Factor must be between 0 and 2'));	
    	}
    }

    // repeat for cement
    @isTest static void detailsValidationSadCement(){
    	init();

    	ext.theMaterial = sika_SCCZoneTestFactory.createMaterialCement();

		ext.theMaterial.Material_Name__c = null;
		ext.theMaterial.Spec_Gravity__c = null;
		ext.theMaterial.Cost__c = null;
		ext.theMaterial.Unit__c = null;

		system.assert(ext.detailsValidation() == false);

		pgMessages = ApexPages.getMessages();

		// look for cement-specific errors
		for(Apexpages.Message m : pgMessages){
        	system.debug('********CEM3 (cement) = ' + m);
        	system.assert(m.getDetail().contains('SCM Effective Factor cannot be blank') == false);
			system.assert(m.getDetail().contains('Max absorption cannot be blank') == false);	
    	}
    }

    // repeat for water
    @isTest static void detailsValidationSadWater(){
    	init();

    	ext.theMaterial = sika_SCCZoneTestFactory.createMaterialWater();

		ext.theMaterial.Material_Name__c = null;
		ext.theMaterial.Spec_Gravity__c = null;
		ext.theMaterial.Cost__c = null;
		ext.theMaterial.Unit__c = null;

		system.assert(ext.detailsValidation() == false);
		pgMessages = ApexPages.getMessages();

		for(Apexpages.Message m : pgMessages){
        	system.debug('********CEM3 (water) = ' + m);
        	system.assert(m.getDetail().contains('SCM Effective Factor cannot be blank') == false);
			system.assert(m.getDetail().contains('Max absorption cannot be blank') == false);	
    	}
    }

    // repeat for sand
    @isTest static void detailsValidationSadSand(){
    	init();

    	ext.theMaterial = sika_SCCZoneTestFactory.createMaterialSand();

		ext.theMaterial.Material_Name__c = null;
		ext.theMaterial.Abs__c = null;
		ext.theMaterial.Spec_Gravity__c = null;
		ext.theMaterial.Cost__c = null;
		ext.theMaterial.Unit__c = null;

		system.assert(ext.detailsValidation() == false);
		pgMessages = ApexPages.getMessages();

		for(Apexpages.Message m : pgMessages){
        	system.debug('********CEM3 (sand) = ' + m);
        	system.assert(m.getDetail().contains('Material Name cannot be blank'));	
        	system.assert(m.getDetail().contains('Spec Gravity cannot be blank'));
        	system.assert(m.getDetail().contains('Unit cannot be blank'));	
        	system.assert(m.getDetail().contains('Cost cannot be blank'));
        	system.assert(m.getDetail().contains('Max absorption cannot be blank'));	
        	system.assert(m.getDetail().contains('SCM Effective Factor cannot be blank') == false);
    	}
    }

    // repeat for coarse aggregate
    @isTest static void detailsValidationSadCoarseAgg(){
    	init();

    	ext.theMaterial = sika_SCCZoneTestFactory.createMaterialCoarseAgg();

		ext.theMaterial.Material_Name__c = null;
		ext.theMaterial.Abs__c = null;
		ext.theMaterial.Spec_Gravity__c = null;
		ext.theMaterial.Cost__c = null;
		ext.theMaterial.Unit__c = null;

		system.assert(ext.detailsValidation() == false);
		pgMessages = ApexPages.getMessages();

		for(Apexpages.Message m : pgMessages){
        	system.debug('********CEM3 (coarse agg) = ' + m);
        	system.assert(m.getDetail().contains('Material Name cannot be blank'));	
        	system.assert(m.getDetail().contains('Spec Gravity cannot be blank'));
        	system.assert(m.getDetail().contains('Unit cannot be blank'));	
        	system.assert(m.getDetail().contains('Cost cannot be blank'));
        	system.assert(m.getDetail().contains('Max absorption cannot be blank'));	
        	system.assert(m.getDetail().contains('SCM Effective Factor cannot be blank') == false);
    	}
    }

    // repeat for "other"
    @isTest static void detailsValidationSadOther(){
    	init();

    	map<String, String> paramsMap = new map<String, String>{'type' => 'other'};
    	ext.theMaterial = sika_SCCZoneTestFactory.createMaterial(paramsMap);

		ext.theMaterial.Material_Name__c = null;
		ext.theMaterial.Abs__c = null;
		ext.theMaterial.Spec_Gravity__c = null;
		ext.theMaterial.Cost__c = null;
		ext.theMaterial.Unit__c = null;

		system.assert(ext.detailsValidation() == false);
		pgMessages = ApexPages.getMessages();

		for(Apexpages.Message m : pgMessages){
        	system.debug('********CEM3 (other) = ' + m);
        	system.assert(m.getDetail().contains('Material Name cannot be blank'));	
        	system.assert(m.getDetail().contains('Spec Gravity cannot be blank'));
        	system.assert(m.getDetail().contains('Unit cannot be blank'));	
        	system.assert(m.getDetail().contains('Cost cannot be blank'));
        	system.assert(m.getDetail().contains('Max absorption cannot be blank'));	
        	system.assert(m.getDetail().contains('SCM Effective Factor cannot be blank') == false);
    	}

	}

	@isTest static void testDoSaveNewHappy(){
		init();

		// create a material
		ext.theMaterial = sika_SCCZoneTestFactory.createMaterialSCM();
		ext.theMaterial.Material_Name__c = 'Material Save Test 1';
		ext.controller.unitString = 'kgs'; // the value of the unit picklist is pulled in from the page on save...so, unless we set it manually here, it will be "pulled in" as null and fail validation.
		
		// save this known-to-be valid material & confirm that it succeeds and that the doSave method returns a reference to the material detail page.  (All required fields are returned by the sika_SCCZoneTestFactory.createMaterialCoarseAgg() method...in theory.)
		PageReference testSave = ext.doSave();
		system.assert(testSave.getURL().contains('sika_SCCZoneMaterialDetailPage'));

		// confirm that the record is now in the database.
		String newMaterialID = testSave.getParameters().get('id');
		Material__c m = [SELECT Id, Material_Name__c FROM Material__c WHERE Id = :newMaterialID];
		system.assertEquals(m.Material_Name__c, 'Material Save Test 1');

	}

	@isTest static void testDoSaveEditHappy(){
		// create a material
		 
//FIXME: Change the material type back to sand or coarse agg & see why that results in the sieve validation method not receiving a list of sieveWrappers!! 		 
		
		Material__c m = sika_SCCZoneTestFactory.createMaterialCement();

		// insert the material
		insert(m);

		PageReference ref = new PageReference('/sika_SCCZoneCreateEditMaterialPage');
		ref.getParameters().put('id', m.Id);

		Test.setCurrentPage(ref);

		// create an instance of the controller class and its extension class
		ctrl = new sika_SCCZoneMaterialsHomePageController();
		sika_SCCZoneCreateEditMaterialCtrlExt3 extension = new sika_SCCZoneCreateEditMaterialCtrlExt3(ctrl);

		extension.theMaterial.Material_Name__c = 'Edited Name';

		// save and confirm that we're taken to the edited record's SCC Zone material detail page
		PageReference testSave = extension.doSave();
		system.assert(testSave.getURL().contains('sika_SCCZoneMaterialDetailPage'));
		system.assert(testSave.getURL().contains(m.Id));

		// query material and confirm that the name has changed
		Material__c m2 = [SELECT Id, Material_Name__c FROM Material__c WHERE Id = :m.Id];
		system.assertEquals(m2.Material_Name__c, 'Edited Name');

	}

	@isTest static void testDoSaveSad(){
		init();

		// create a material
		ext.theMaterial = sika_SCCZoneTestFactory.createMaterialCement();
		ext.theMaterial.Material_Name__c = null;
		ext.controller.unitString = 'kgs';

		// save the material and confirm that it fails with the expected error
		PageReference testSave = ext.doSave();
		system.assert(ext.doSave() == null);
		pgMessages = ApexPages.getMessages();

		for(Apexpages.Message m : pgMessages){
        	system.assert(m.getDetail().contains('Material Name cannot be blank'));	
        	system.assert(m.getDetail().contains('Spec Gravity cannot be blank') == false);
        	system.assert(m.getDetail().contains('Unit cannot be blank') == false);	
        	system.assert(m.getDetail().contains('Cost cannot be blank') == false);
        	system.assert(m.getDetail().contains('Max absorption cannot be blank') == false);	
        	system.assert(m.getDetail().contains('SCM Effective Factor cannot be blank') == false);
    	}

		system.assert(ApexPages.currentPage().getURL().contains('sika_SCCZoneCreateEditMaterialPage'));
	}

}