public with sharing class CommunityMyProjectsController {
    
    public List<Project__c> projList {get; set;}
    
    public string uid {get; set;}
    
    public CommunityMyProjectsController(){
        //get current user id
        string userId = UserInfo.getUserId();
        uid = userId;
        
        //load all projects for this user
        string projFieldList = Utilities.GetAllObjectFields('Project__c');
        string queryFormat = 'select {0} from Project__c where Community_User_ID__c = :userId';  
        string projQuery = String.format(queryFormat, new List<string>{projFieldList});
        
        try{
            projList = Database.query(projQuery);
        }
        catch(Exception e){
            projList = new List<Project__c>();
        }
    }
    
    public void deleteProjects(){
            string index = ApexPages.currentPage().getParameters().get('ProjNum');
            Project__c ProjtoDelete = projList.get(integer.valueof(index));
            delete ProjtoDelete;
           projList.remove(integer.valueof(index));
        }

}