@isTest
private class sika_SCCZoneMixesDetailsPageTest {
	private static sika_SCCZoneMixesHomePageController ctrl;
	private static PageReference pageRef;
	private static User SCCZoneUser;
	private static Mix__c theMix;
	private static list<Mix_Material__c> theMMs;

	private static Integer i;

	private static void init(){
		// bypass the triggers we won't need to have firing during setup
		sika_SCCZoneMixTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMixTriggerHelper.bypassMixOwnerTransferCheck = true;
		sika_SCCZoneMixTriggerHelper.bypassMixChatterAlert = true;
		sika_SCCZoneMixTriggerHelper.bypassUpdateSCMQuantity = true;
	
		sika_SCCZoneMaterialTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = true;
		sika_SCCZoneMaterialTriggerHelper.bypassPreventDelete = true;
		sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = true;
		
		sika_SCCZoneMixMaterialTriggerHelper.mixMaterialTriggerBypassSwitch(true);
		
		sika_SCCZoneTestResultTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneTestResultTriggerHelper.bypassLockOrUnlockMixAndMaterials = true;
		
		// create an SCC Zone user, a mix, and some materials to associate with the mix
		map<String, sObject> objects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
        SCCZoneUser = (User) objects.get('SCCZoneUser');
        
        system.runAs(SCCZoneUser){
	        list<Material__c> theMaterials = new list<Material__c>();
	        theMaterials.add(sika_SCCZoneTestFactory.createMaterialWater());
	        theMaterials.add(sika_SCCZoneTestFactory.createMaterialSand());
	        theMaterials.add(sika_SCCZoneTestFactory.createMaterialCement());
	        theMaterials.add(sika_SCCZoneTestFactory.createMaterialCoarseAgg());
	        
	        insert(theMaterials);
        
        	theMix = sika_SCCZoneTestFactory.createMix();
        	insert(theMix);
        	
        	theMMs = new list<Mix_Material__c>();
        	
        	for(Material__c m : theMaterials){
        		theMMs.add(sika_SCCZoneTestFactory.createMixMaterial(theMix, m));
        	}
        	insert(theMMs);
        	
        }
	
		// set page we'll be testing
        pageRef = new PageReference('/sika_SCCZoneMixDetailsPage');  // ** actually, some of this covers the mixes homepage. The controller is the same for both, but watch out for testing pagemessages against the wrong page!
        pageRef.getParameters().put('mixID', theMix.Id);
        Test.setCurrentPage(pageRef);
        
	}


	@isTest static void testBasicStuff() {
		init();
		
		Test.startTest();
		system.runAs(SCCZoneUser){
			// create an instance of the controller
        	ctrl = new sika_SCCZoneMixesHomePageController();
        	
        	// confirm that the mix we created is the one being displayed
        	system.assertEquals(ctrl.newMix.Id, theMix.Id);
        	
        	// confirm that the associated materials are being displayed
        	system.assertEquals(ctrl.mixMaterialList.size(), theMMs.size());

        	// confirm that there are no test results
        	system.assertEquals(ctrl.testResultsList.size(), 0);
        	
        	// confirm that there are no attachments
        	system.assert(ctrl.attachmentList.size() == 0);

		}
		Test.stopTest();
	}


	@isTest static void testEditButton(){
		init();
		
		// create an instance of the controller
    	ctrl = new sika_SCCZoneMixesHomePageController();
    	system.assert(ctrl.createNewMix().getURL().contains('/sika_SCCZoneNewMixPage'));

	}


	@isTest static void testManageMaterialsButton(){
		init();
		
		// create an instance of the controller
    	ctrl = new sika_SCCZoneMixesHomePageController();
    	
		PageReference returnedRef = ctrl.manageMaterials();
    	system.assert(returnedRef.getURL().contains('/sika_SCCZoneManageMixMaterialsPage2'));
  	
	}

	
	@isTest static void testOptimizationCall(){  // does not actually send data to the optimization class. Receives a response we define here
		init();
		system.runAs(SCCZoneUser){
			// remove the cement and water materials from the mix
			list<Mix_Material__c> deleteMMs = [SELECT Mix__c, Material_Type__c FROM Mix_Material__c WHERE (Material_Type__c = 'Water' OR Material_Type__c = 'Cement') AND Mix__c = :theMix.Id];
			system.assertEquals(deleteMMs.size(), 2);
			delete(deleteMMs);
			
			ctrl = new sika_SCCZoneMixesHomePageController();
			
			Test.startTest();
	    	
	    	// confim that the optimizeMix validation fails. (Must have at least 3 materials: 1 water, 1 cement, and 1 sand or coarse aggregate)
	    	system.assertEquals(ctrl.optimizeMix(), null);
	        
	        list<sObject> obj;
	        Material__c material;
	        Mix_Material__c mm;
	        
	        // add a water material back.  (Should still fail b/c it doesn't have a cement)
	        obj = new list<sObject>();
	        material = sika_SCCZoneTestFactory.createMaterialWater();
	        insert(material);
	        mm = sika_SCCZoneTestFactory.createMixMaterial(theMix, material);
	        obj.add(mm);
	        
	        insert(obj);
	        
	        // re-load and confirm that it still fails
	        //ctrl = new sika_SCCZoneMixesHomePageController();
	        //system.assertEquals(ctrl.optimizeMix(), null);
	        
	        // add a cement material.
	        obj = new list<sObject>();
	        material = sika_SCCZoneTestFactory.createMaterialCement();
	        insert(material);
	        mm = sika_SCCZoneTestFactory.createMixMaterial(theMix, material);
	        obj.add(mm);
	        
	        insert(obj);
	        
	        Test.stopTest();
	        
	        ctrl = new sika_SCCZoneMixesHomePageController();
	        
	        // simulate a failure response from the optimize class
	        ctrl.testResponseMap.put('passFail', 'fail');
	        ctrl.testResponseMap.put('status', 'like life, the universe, and everything, it just will not optimize.');
	        ctrl.testResponseMap.put('statusCode', '42 error');
	        
	        system.assertEquals(ctrl.optimizeMix(), null);
	        
	        List<Apexpages.Message> msgs = ApexPages.getMessages();
			Boolean hasTheAnswer = false;
			for(Apexpages.Message msg : msgs){
//TODO: ???
				if (msg.getDetail().contains('like life, the universe, and everything, it just will not optimize')) hasTheAnswer = true;
			}	
			system.assert(hasTheAnswer);
			
			// simulate a passing response from the optimize class
			ctrl.testResponseMap.clear();
			ctrl.testResponseMap.put('passFail', 'pass');
	        
	        system.assert(ctrl.optimizeMix().getURL().contains('/sika_SCCZoneMixDetailsPage'));
	        
		}
	}


	@isTest static void testDeleteMix(){
		init();
		system.runAs(SCCZoneUser){
			// create an instance of the controller
        	ctrl = new sika_SCCZoneMixesHomePageController();
        	ctrl.deleteThisMix = theMix.Id;
			PageReference p = ctrl.deleteMix();
			PageReference q = new PageReference('/sika_SCCZoneMixesHomePage');

			system.assert(p.getURL().contains('/sika_SCCZoneMixesHomePage'));
		}
		
	}

	@isTest static void testDeleteMixDetail(){
		init();
		
		system.runAs(SCCZoneUser){
			// create an instance of the controller
        	ctrl = new sika_SCCZoneMixesHomePageController();
        	
			PageReference deleteResponse = ctrl.deleteMixDetail();
        	system.assert(deleteResponse.getURL().contains('/sika_SCCZoneMixesHomePage'));
        	
        	list<Mix__c> noMoreMix = [SELECT Id FROM Mix__C WHERE Id = :theMix.Id];
        	system.assert(noMoreMix.size() == 0);
		}
		
	}


	@isTest static void testNewTestResult(){
	 	init();
	 	ctrl = new sika_SCCZoneMixesHomePageController();
        PageReference p = ctrl.newTestResult();
		system.assert(p.getURL().contains('/sika_SCCZoneTestNewEditPage'));
    }
   
   
    @isTest static void deleteTestResultTest(){
    	init();
    	Test_Result__c tr = new Test_Result__c();
		tr.Mix__c = theMix.Id;
		system.runAs(SCCZoneUser){
			insert(tr);	
		
	    	Test.startTest();
	    	ctrl = new sika_SCCZoneMixesHomePageController();
	
	    	system.assertEquals(ctrl.testResultsList.size(), 1);
	   		ctrl.deleteThisTest = tr.Id;
		
	   		system.assert(ctrl.deleteTest().getURL().contains('/sika_scczonemixdetailspage'));
	   		system.assertEquals(ctrl.deleteTest().getParameters().get('mixID'), theMix.Id);
			
			PageReference p = ctrl.deleteTest();
			system.assert(p.getURL().contains('/sika_scczonemixdetailspage'));

   		}
    	Test.stopTest();
    	
    	list<Test_Result__c> TRs = [SELECT Mix__c FROM Test_Result__c WHERE Mix__c = :theMix.Id];
    	system.assertEquals(TRs.size(), 0);
  
    }


	@isTest static void AttachmentTest(){
    	init();
    	system.runAs(SCCZoneUser){  // ...to avoid MIXED_DML_OPERATION error.  (The context user has already inserted setup objects. Running uploadAttachment() in the same context would mean inserting a non-setup object...and that just don't work none.)
	    	ctrl = new sika_SCCZoneMixesHomePageController();
	    	ctrl.attachment.body = Blob.valueOf('this is the attachmet body');
	    	ctrl.attachment.name = 'fileName.txt';
	    	ctrl.attachment.description = 'description';
	    	
	    	list<Attachment> theAttachments = new list<Attachment>();
	
    		system.assert(ctrl.uploadAttachment().getURL().contains('sika_SCCZoneMixDetailsPage'));
		
	    	theAttachments = [SELECT Id, ParentID FROM Attachment WHERE ParentID = :theMix.Id];
	    	system.assert(theAttachments.size() == 1);

            ctrl.deleteAttachmentID = theAttachments[0].Id;
            PageReference p = ctrl.deleteAttachment();
            
    		theAttachments = [SELECT ParentID FROM Attachment WHERE ParentID = :theMix.Id];
    		system.assert(theAttachments.size() == 0);
		}
    }  


	@isTest static void testDeleteAttachment(){
		init();
	    Attachment attachment = new Attachment();
	    
	    Blob b = Blob.valueOf('Test Data');  
	    attachment.ParentId = theMix.Id;  
	    attachment.Name = 'Test Attachment for Parent';  
	    attachment.Body = b;  
		system.runAs(SCCZoneUser){
		    insert(attachment); 
		    ctrl = new sika_SCCZoneMixesHomePageController();
		    
		    ctrl.deleteAttachmentID = attachment.Id;
            PageReference p = ctrl.deleteAttachment();
		    system.assert(p.getURL().contains('/sika_SCCZoneMixDetailsPage'));
		}

	}
    

    @isTest static void testSortTable(){
		init();
		String tempString;
		Decimal tempDecimal;
		Boolean tempBoolean;
		
		test.startTest();
		
		System.runAs(SCCZoneUser){
			// instantiate the page controller
		    ctrl = new sika_SCCZoneMixesHomePageController();

			// sort the list by name. (Should default to sort ascending)
			ctrl.sortField = 'Mix_Name__c';
			ctrl.tableSort();

			// sort by name again. Affirm that the list is now sorted in the other direction. (Descending)
			tempString = ctrl.myMixes[0].Mix_Name__c;
			ctrl.tableSort();

			system.assertEquals(ctrl.myMixes[ctrl.myMixes.size() - 1].Mix_Name__c, tempString);

			// repeat for the other table columns
			ctrl.sortField = 'Project__c';
			ctrl.tableSort();
			tempString = ctrl.myMixes[0].Project__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.myMixes[ctrl.myMixes.size() - 1].Project__c, tempString);

			ctrl.sortField = 'Water_Cement_Ratio_Calculated__c';
			ctrl.tableSort();
			tempDecimal = ctrl.myMixes[0].Water_Cement_Ratio_Calculated__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.myMixes[ctrl.myMixes.size() - 1].Water_Cement_Ratio_Calculated__c, tempDecimal);

			ctrl.sortField = 'Concrete_Compressive_Strength_DSP__c';
			ctrl.tableSort();
			tempDecimal = ctrl.myMixes[0].Concrete_Compressive_Strength_DSP__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.myMixes[ctrl.myMixes.size() - 1].Concrete_Compressive_Strength_DSP__c, tempDecimal);

			ctrl.sortField = 'Calculated_Raw_Material_Cost__c';
			ctrl.tableSort();
			tempDecimal = ctrl.myMixes[0].Calculated_Raw_Material_Cost__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.myMixes[ctrl.myMixes.size() - 1].Calculated_Raw_Material_Cost__c, tempDecimal);

			ctrl.sortField = 'isLocked__c';
			ctrl.tableSort();
			tempBoolean = ctrl.myMixes[0].isLocked__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.myMixes[ctrl.myMixes.size() - 1].isLocked__c, tempBoolean);
			
			// switch from sorting "my mixes" to sorting "my company's mixes"
			
			// sort the list by name. (Should default to sort ascending)
			ctrl.sortField = 'Mix_Name__c';
			ctrl.tableSort();

			// sort by name again. Affirm that the list is now sorted in the other direction. (Descending)
			tempString = ctrl.theMixes[0].Mix_Name__c;
			ctrl.tableSort();

			system.assertEquals(ctrl.theMixes[ctrl.theMixes.size() - 1].Mix_Name__c, tempString);

			// repeat for the other table columns
			ctrl.sortField = 'Project__c';
			ctrl.tableSort();
			tempString = ctrl.theMixes[0].Project__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.theMixes[ctrl.theMixes.size() - 1].Project__c, tempString);

			ctrl.sortField = 'Water_Cement_Ratio_Calculated__c';
			ctrl.tableSort();
			tempDecimal = ctrl.theMixes[0].Water_Cement_Ratio_Calculated__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.theMixes[ctrl.theMixes.size() - 1].Water_Cement_Ratio_Calculated__c, tempDecimal);

			ctrl.sortField = 'Concrete_Compressive_Strength_DSP__c';
			ctrl.tableSort();
			tempDecimal = ctrl.theMixes[0].Concrete_Compressive_Strength_DSP__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.theMixes[ctrl.theMixes.size() - 1].Concrete_Compressive_Strength_DSP__c, tempDecimal);

			ctrl.sortField = 'Calculated_Raw_Material_Cost__c';
			ctrl.tableSort();
			tempDecimal = ctrl.theMixes[0].Calculated_Raw_Material_Cost__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.theMixes[ctrl.theMixes.size() - 1].Calculated_Raw_Material_Cost__c, tempDecimal);

			ctrl.sortField = 'isLocked__c';
			ctrl.tableSort();
			tempBoolean = ctrl.theMixes[0].isLocked__c;
			ctrl.tableSort();
			system.assertEquals(ctrl.theMixes[ctrl.theMixes.size() - 1].isLocked__c, tempBoolean);
		}
		test.stopTest();

	}
    
    
    @isTest static void testPopupMethods_aka_need_just_a_tiny_bit_more_code_coverage(){
    	ctrl = new sika_SCCZoneMixesHomePageController();
    	ctrl.closePopup();
    	system.assertEquals(ctrl.displayPopup, false);
    	ctrl.closePopup2();
    	system.assertEquals(ctrl.displayPopup2, false);
    	ctrl.closePopup3();
    	system.assertEquals(ctrl.displayPopup3, false);
    	ctrl.closePopup4();
    	system.assertEquals(ctrl.displayPopup4, false);
    	ctrl.showPopup();
    	system.assertEquals(ctrl.displayPopup, true);
    	ctrl.showPopup2();
    	system.assertEquals(ctrl.displayPopup, true);
    	ctrl.showPopup3();
    	system.assertEquals(ctrl.displayPopup, true);
    	ctrl.showPopup4();
    	system.assertEquals(ctrl.displayPopup, true);
    }

}