/*
* Author: Martin Kona
* Company: Bluewolf
* Date: May 23, 2016
* Description: Service class for Opportunity_Firm__c trigger for vice-versa OpportunityContactRole object creation
* 
*/

public class OpportunityFirmService {
    
    public static void insertOpportunityContactRole(Map<Id, Opportunity_Firm__c> oppFirms) {
                                        
      	
		// Opportunity Id, List of Opportunity_Firm__c
        Map<Id, List<Opportunity_Firm__c>> IdOppFirms = new Map<Id, List<Opportunity_Firm__c>>();
        
      	// Opportunity Id, List of OpportunityContactRoles
        Map<Id, List<OpportunityContactRole>> IdOppContRoles = new Map<Id, List<OpportunityContactRole>>();        
        
        Set<Id> oppIds = new Set<Id>();
        // get Opp ids
        for (Opportunity_Firm__c firm : oppFirms.values()) {
            oppIds.add(firm.Opportunity__c);
        }
        
        // get Opportunities with child Opportunity Firms and OpportunityContactRoles, create copy record for OpportunityContactRole if doesnt exist    
        for (List<Opportunity> opps : [SELECT Id, (SELECT Account__c, Contact__c, Opportunity__c FROM Opportunity_Firms__r), (SELECT ContactId , Opportunity.AccountId, Role FROM OpportunityContactRoles) FROM Opportunity WHERE Id = :oppIds]) {                             
            for (Opportunity opp : opps) {
                // create Firm map
                if (opp.Opportunity_Firms__r.size() > 0) {
                    // get existing list by Opportunity Id and append new firm records
                    List<Opportunity_Firm__c> firms = IdOppFirms.get(opp.Id);
                    if (firms != NULL) {
                        firms.addAll(opp.Opportunity_Firms__r);
                        IdOppFirms.put(opp.Id, firms);
                    } else {
                    	IdOppFirms.put(opp.Id, opp.Opportunity_Firms__r);
                    }
            	}
                // create Opp Contact Role map
                if (opp.OpportunityContactRoles.size() > 0) {
                    // get existing list by Opportunity Id and append new opp contact roles records
                    List<OpportunityContactRole> firms = IdOppContRoles.get(opp.Id);
                    if (firms != NULL) {
                        firms.addAll(opp.OpportunityContactRoles);
                        IdOppContRoles.put(opp.Id, firms);
                    } else {
                    	IdOppContRoles.put(opp.Id, opp.OpportunityContactRoles);
                    }
            	}
            }
        }
        
        System.debug('Firms: ' + IdOppFirms);
        System.debug('OppContRoles: ' + IdOppContRoles);        
    }
    
    private String key() {
        return null;
    }
    
    /*
    public static void updateOpportunityContactRole() {
        
    }

    public static void deleteOpportunityContactRole() {
        
    }

    public static void undeleteOpportunityContactRole() {
        
    }    */

}