@isTest
private class sika_SCCZoneSupportTicketControllerTest {
	private static Case theCase;
	private static User theUser;
	private static PageReference pageRef;
	private static sika_SCCZoneSupportTicketController ctrl;
	
	private static void init(){
		// create an SCCZone user to run these tests as
		map<String, sObject> objects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
		
		theUser = (User) objects.get('SCCZoneUser');
		
		// create a Case
		theCase = sika_SCCZoneTestFactory.createCase();
		theCase.Project_Building_Name__c = 'Project Building Name';
		theCase.Master_Category__c = 'Account Administration';
		theCase.Category__c = 'Change Employee Role';

		system.runAs(theUser){
			insert(theCase);
		}
		
		pageRef = new PageReference('/sika_SCCZoneSupportTicket');
		
	}
	
	// make sure the user is redirected to the support homepage if no case id is passed to the page
	@isTest static void testNoId() {
		init();
		system.runAs(theUser){
			Test.setCurrentPage(pageRef);
			system.assert(ApexPages.CurrentPage().getParameters().get('id') == null);
			
			ctrl = new sika_SCCZoneSupportTicketController();
			system.assert(ctrl.loadAction().getURL().contains('/sika_SCCZoneSupportHomePage'));
		}
		
	}
	
	@isTest static void testHappy(){
		init();
		system.runAs(theUser){
			pageRef.getParameters().put('id', theCase.Id);
			Test.setCurrentPage(pageRef);
			
			ctrl = new sika_SCCZoneSupportTicketController();
			
			system.assertEquals(ctrl.loadAction(), null);
			
			Case testCase = ctrl.getCaseDetails();
			system.assertEquals(testCase.Id, theCase.Id);
			
			list<String> comments = ctrl.getComments();
			system.assert(comments.size() == 0);
			system.assertEquals(ctrl.firstComment, '');
			
			// test submitComment()
			String comment1Body = 'This is a test comment.';
			ctrl.commentForm = comment1Body;
			
			system.assertEquals(ctrl.validateForm(), true);
			ctrl.submitComment();
			
			CaseComment testComment1 = [SELECT ParentId, commentBody FROM CaseComment WHERE ParentId = :theCase.Id];
			system.assertEquals(testComment1.commentBody, comment1Body);
			
			String comment2Body = 'This is a follow-up.';
			ctrl.commentForm = comment2Body;
			
			system.assertEquals(ctrl.validateForm(), true);
			ctrl.submitComment();
			
			CaseComment testComment2 = [SELECT Id, ParentId, commentBody FROM CaseComment WHERE ParentId = :theCase.Id AND Id != :testComment1.Id];
			system.assertEquals(testComment2.commentBody, comment2Body);
			
		}
		
	}
	
	@isTest static void testSad(){
		init();
		system.runAs(theUser){
			pageRef.getParameters().put('id', theCase.Id);
			Test.setCurrentPage(pageRef);
			
			ctrl = new sika_SCCZoneSupportTicketController();
			
			system.assertEquals(ctrl.validateForm(), false);
			
		}
	}
	
	@isTest static void testCancel(){
		init();
		pageRef = new PageReference('/sika_SCCZoneSupportTicket');
		pageRef.getParameters().put('id', theCase.Id);
		Test.setCurrentPage(pageRef);
			
		ctrl = new sika_SCCZoneSupportTicketController();
		PageReference resultPage = ctrl.cancel();
		system.assert(resultPage.getURL().contains('/sika_SCCZoneSupportHomePage'));

	}

	
}