/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 @ isTest
private class GenericObjectCountTest {

    private static void insertTriggerConfig() {
    
            List < ConstructionPts__CPConfig__c > triggerValidity = [Select ConstructionPts__Config_Name__c, ConstructionPts__Config_Value__c from ConstructionPts__CPConfig__c where ConstructionPts__Config_Name__c = 'ActivateGenericObjCountTrigger'];
            if (triggerValidity.isEmpty()) {
                ConstructionPts__CPConfig__c trigValidity = new ConstructionPts__CPConfig__c();
                trigValidity.ConstructionPts__Config_Name__c = 'ActivateGenericObjCountTrigger';
                trigValidity.ConstructionPts__Config_Value__c = '1';

                insert trigValidity;
            } else {
                if (triggerValidity[0] != null && triggerValidity[0].ConstructionPts__Config_Value__c != null) {
                    triggerValidity[0].ConstructionPts__Config_Value__c = '1';
                    update TriggerValidity[0];
                } else {
                    ConstructionPts__CPConfig__c trigValidity = new ConstructionPts__CPConfig__c();
                    trigValidity.ConstructionPts__Config_Name__c = 'ActivateGenericObjCountTrigger';
                    trigValidity.ConstructionPts__Config_Value__c = '1';
                }
            }
        

    }

    static testMethod void Test_GenericObejct_On_Update_Project_Null() {
        insertTriggerConfig();
        ConstructionPts__CP_Project__c pr = new ConstructionPts__CP_Project__c();
        pr.Name = 'Test';
        pr.ConstructionPts__ProjectID__c = '777777';
        insert pr;

        Project__c testobj = new Project__c();
        testobj.CP_Project__c = pr.Id;
        testobj.Name = 'Test obj';
        insert testobj;

        ConstructionPts__CP_Project__c pr2 = new ConstructionPts__CP_Project__c();
        pr2.Name = 'Test2';
        pr2.ConstructionPts__ProjectID__c = '7777772';
        insert pr2;

        List < Project__c > currObjs = [SELECT CP_Project__c from Project__c where id = : testobj.id];
        List < ConstructionPts__CP_Project__c > Proj = [SELECT related_Projects__c from ConstructionPts__CP_Project__c where id = : pr.id];

        System.assert(!currObjs.isEmpty(), 'GenericObject is empty');
        System.assert(proj[0].related_Projects__c == 1, 'proj Related Generic Object count  = ' + proj[0].related_Projects__c);

        if (currObjs[0] != null) {
            currObjs[0].Name = 'Test Obj2';
            currObjs[0].CP_Project__c = null;
            update currObjs[0];
        }
        List < ConstructionPts__CP_Project__c > currProj = [SELECT related_Projects__c from ConstructionPts__CP_Project__c where id = : pr.id];
        System.assert(!currProj.isEmpty(), 'CPProject is empty');
        if (currProj[0] != null) {
            system.debug(logginglevel.error, 'updated Related Generic Object count = ' + currProj[0].related_Projects__c);
            system.assertEquals(0, currProj[0].related_Projects__c);
        }

    }

    static testMethod void Test_GenericObj_On_Update_Project2() {
        insertTriggerConfig();
        ConstructionPts__CP_Project__c pr = new ConstructionPts__CP_Project__c();
        pr.Name = 'Test';
        pr.ConstructionPts__ProjectID__c = '777777';
        insert pr;

        Project__c testObj = new Project__c();
        testObj.CP_Project__c = pr.Id;
        testObj.Name = 'Test Obj';
        insert testObj;

        ConstructionPts__CP_Project__c pr2 = new ConstructionPts__CP_Project__c();
        pr2.Name = 'Test2';
        pr2.ConstructionPts__ProjectID__c = '7777772';
        insert pr2;

        List < Project__c > currObj = [SELECT CP_Project__c from Project__c where id = : testObj.id];
        List < ConstructionPts__CP_Project__c > Proj = [SELECT related_Projects__c from ConstructionPts__CP_Project__c where id = : pr.id];
        System.assert(!currObj.isEmpty(), 'GenericObj is empty');
        System.assert(proj[0].related_Projects__c == 1, 'proj related Generic Object count = ' + proj[0].related_Projects__c);
        List < ConstructionPts__CP_Project__c > test = [SELECT related_Projects__c from ConstructionPts__CP_Project__c where id = : pr2.id];
        System.debug('test  = ' + test[0].related_Projects__c);
        if (currObj[0] != null) {
            currObj[0].Name = 'Test Obj2';
            currObj[0].CP_Project__c = pr2.id;
            update currObj[0];
        }
        List < ConstructionPts__CP_Project__c > currProj = [SELECT related_Projects__c from ConstructionPts__CP_Project__c where id = : pr.id];
        System.assert(!currProj.isEmpty(), 'CPProject is empty');
        if (currProj[0] != null) {
            system.debug(logginglevel.error, 'updated Related Generic Object count = ' + currProj[0].related_Projects__c);
            system.assertEquals(0, currProj[0].related_Projects__c);
        }

        List < ConstructionPts__CP_Project__c > currProj2 = [SELECT related_Projects__c from ConstructionPts__CP_Project__c where id = : pr2.id];
        if (currProj2[0] != null) {
            system.debug(logginglevel.error, 'updated related generic object count = ' + currProj2[0].related_Projects__c);
            system.assertEquals(1, currProj2[0].related_Projects__c);
        }
    }

    static testMethod void Test_GenericObjCount_On_Delete() {
        insertTriggerConfig();
        ConstructionPts__CP_Project__c pr = new ConstructionPts__CP_Project__c();
        pr.Name = 'Test';
        pr.ConstructionPts__ProjectID__c = '777777';
        insert pr;

        Project__c testObj = new Project__c();
        testObj.CP_Project__c = pr.Id;
        testObj.Name = 'Test GenericObj';
        insert testObj;

        List < ConstructionPts__CP_Project__c > Proj = [SELECT related_Projects__c from ConstructionPts__CP_Project__c where id = : pr.id];

        System.assert(proj[0].related_Projects__c == 1, 'proj Related Generic Object count  = ' + proj[0].related_Projects__c);

        delete testObj;
        List < ConstructionPts__CP_Project__c > currProj = [SELECT related_Projects__c from ConstructionPts__CP_Project__c where id = : pr.id];
        if (currProj[0] != null) {
            system.debug(logginglevel.error, 'updated related generic object count = ' + currProj[0].related_Projects__c);
            system.assertEquals(0, currProj[0].related_Projects__c);
        }

    }

}