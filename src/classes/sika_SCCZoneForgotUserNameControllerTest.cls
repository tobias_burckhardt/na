@isTest
private class sika_SCCZoneForgotUserNameControllerTest {
	private static PageReference pageRef;
	private static User guestUser;
	private static User SCCZoneUser;
	private static sika_SCCZoneForgotUserNameController ctrl;

	// static initializer
	static {
		// create a guest (anonymous) user to run the test as
		guestUser = sika_SCCZoneTestFactory.createUser(new map<String, String>{'profileId' =>  sika_SCCZoneTestFactory.getGuestUserProfileId()});
		insert(guestUser);
		
		// create an SCCZone user to validate against.  (Query only searches for usernames associated with SCC Zone users; not for all Salesforce users.  Not opening up that kind of security hole here!)
		map<String, sObject> usefulStuff = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount(new map<String, String>{'contact_email' => 'test@us.sika.com'});
		SCCZoneUser = (User) usefulStuff.get('SCCZoneUser');
	}

	private static void init(){
		// set page we'll be testing
		pageRef = new PageReference('/sika_SCCZoneForgotUserNamePage');
		Test.setCurrentPage(pageRef);
	}


	@isTest static void test_happy_path(){
		init();
		system.runAs(guestUser){
			// create an instance of the page's controller
			ctrl = new sika_SCCZoneForgotUserNameController();

			// confirm that the page loaded. (Didn't redirect to an error page)
			system.assertEquals(pageRef.getURL(), ApexPages.CurrentPage().getURL());

			ctrl.emailAddress = SCCZoneUser.email;

			PageReference submitResponse = ctrl.submitRequest();
			system.assert(submitResponse.getURL().contains('/sika_SCCZoneForgotUserNamePage'));
			system.assertEquals(submitResponse.getParameters().get('success'), 'true');
		}

	}

	@isTest static void test_sad_path(){
		init();
		system.runAs(guestUser){
			ctrl = new sika_SCCZoneForgotUserNameController();

			ctrl.emailAddress = 'wrong@email.com';

			PageReference submitResponse = ctrl.submitRequest();

			system.assert(ctrl.hasError == true);
			system.assertEquals(ctrl.errorText, sika_SCCZoneForgotUserNameController.emailNotRecognizedErrorMsg);

			ctrl.emailAddress = SCCZoneUser.email;

			submitResponse = ctrl.submitRequest();
			system.assert(submitResponse.getURL().contains('/sika_SCCZoneForgotUserNamePage'));
			system.assertEquals(submitResponse.getParameters().get('success'), 'true');
	
		}

	}

	@isTest static void test_bad_path(){
		init();
		system.runAs(guestUser){
			ctrl = new sika_SCCZoneForgotUserNameController();
			PageReference submitResponse;

			ctrl.emailAddress = '';

			submitResponse = ctrl.submitRequest();

			system.assert(ctrl.hasError == true);
			system.assertEquals(ctrl.errorText, sika_SCCZoneForgotUserNameController.invalidEmailErrorMsg);

			ctrl.emailAddress = 'notanemail@com';

			submitResponse = ctrl.submitRequest();

			system.assert(ctrl.hasError == true);
			system.assertEquals(ctrl.errorText, sika_SCCZoneForgotUserNameController.invalidEmailErrorMsg);

			ctrl.emailAddress = 'notanemail.com';

			submitResponse = ctrl.submitRequest();

			system.assert(ctrl.hasError == true);
			system.assertEquals(ctrl.errorText, sika_SCCZoneForgotUserNameController.invalidEmailErrorMsg);

			ctrl.emailAddress = '@.com';

			submitResponse = ctrl.submitRequest();

			system.assert(ctrl.hasError == true);
			system.assertEquals(ctrl.errorText, sika_SCCZoneForgotUserNameController.invalidEmailErrorMsg);

			ctrl.emailAddress = SCCZoneUser.email;

			submitResponse = ctrl.submitRequest();
			system.assert(submitResponse.getURL().contains('/sika_SCCZoneForgotUserNamePage'));
			system.assertEquals(submitResponse.getParameters().get('success'), 'true');
	
		}

	}

	// test the "contact us here" case submission form
	//@isTest static void testContactForm(){
	//	init();
	//	system.runAs(guestUser){
	//		ctrl = new sika_SCCZoneForgotUserNameController();

	//		ctrl.caseEmail = SCCZoneUser.email;
	//		ctrl.caseDesc = 'I forgot my username';

	//		ctrl.submitCase();
	//	}

	//	Case submittedCase = [SELECT Id, Description, Customer_Email__c FROM Case WHERE Customer_Email__c = :SCCZoneUser.email];
	//	system.assertEquals(submittedCase.Description, 'I forgot my username');

	//}

}