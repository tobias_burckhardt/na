@isTest
private class cloneIncidentAttachmentsTest {
	
    @isTest
    private static void forceBatchExecuteCoverage() {
        Database.executeBatch(new cloneIncidentAttachments(Datetime.now(), false, true));
    }
	
    @isTest
    private static void forceExecuteCoverage() {
        new cloneIncidentAttachments(Datetime.now(), false, true).execute(null, new List<SObject>());
    }
}