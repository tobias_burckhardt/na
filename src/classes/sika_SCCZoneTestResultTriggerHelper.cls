public without sharing class sika_SCCZoneTestResultTriggerHelper {
	public static Boolean bypassAccountTeamCheck = false;
	public static Boolean bypassLockOrUnlockMixAndMaterials = false; // used for lockMix and Unlock Mix methods

	// used by the TestResult trigger to prevent salespeople from editing or deleting Mix records associated with accounts on which they are not accountTeamMembers (or owners)
	// The SCC Zone permission set should be added to SCC Zone salespeople, and should provide view all & modify all access to the Mix object.
    public static void accountTeamCheck(list<sObject> theResults, String actionName){
        // adds an error to the record if it's being edited/deleted by an unauthorized User
    	sika_SCCZoneAccountTeamCheckHelper.accountTeamCheck(theResults, 'Test Result', actionName);
    }

	
    // locks mix records that have test results attached.  Also locks material records associated w/ the mix.
    public static void lockMixAndMaterials(list<Test_Result__c> tList){
        list<Mix__c> mixList = new list<Mix__c>();
        set<ID> mixSet = new set<ID>();

        list<Material__c> materialsList = new list<Material__c>();
        set<ID> materialsSet = new set<ID>();

        list<Mix_Material__c> mixMaterialsList = new list<Mix_Material__c>();

        // create a set of mix IDs associated with the test results passed from the trigger
        for(Test_Result__c tr : tList){
            mixSet.add(tr.Mix__c);
        }

        // query the mixes in the set
        mixList = [SELECT ID, isLocked__c FROM Mix__c WHERE ID IN: mixSet AND isLocked__c = false];
        list<Mix__c> updateMixList = new list<Mix__c>();
        RecordType rt = [select Id from RecordType where Name = 'Locked' and SobjectType = 'Mix__c' limit 1];
        
        // prepare to update the mixes, locking each (i.e., setting isLocked = true and the record type to "locked")
        for(Mix__c m : mixList){
            updateMixList.add(new Mix__c(id = m.ID, isLocked__c = true, RecordTypeID = rt.Id));
        }

        // get the mix_materials for the mix associated with each test result.  (Exclude Admixture materials. Those can't be updated by SCC Zone users.)
        mixMaterialsList = [SELECT Id, Material__c, Material__r.Type__c FROM Mix_Material__c WHERE Mix__c IN :mixSet AND Material__r.Type__c != 'Admixture'];
        for(Mix_Material__c mm : mixMaterialsList){
            materialsSet.add(mm.Material__c);
        }

        // get the materials associated with each mix_material, and...
        materialsList = [SELECT Id FROM Material__c WHERE Id IN : materialsSet];

        // ...lock the materials 
        list<Material__c> materialsToLock = new list<Material__c>();
        for(Material__c m : materialsList){
            materialsToLock.add(new Material__c(Id = m.Id, isLocked__c = true));
        }

        sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = true;
        sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = true;
        sika_SCCZoneMixTriggerHelper.bypassMixOwnerTransferCheck = true;
        sika_SCCZoneMixTriggerHelper.bypassMixChatterAlert = true;
        sika_SCCZoneMixTriggerHelper.bypassUpdateSCMQuantity = true;

        update materialsToLock;
        update updateMixList;

        sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = false;
        sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = false;
        sika_SCCZoneMixTriggerHelper.bypassMixOwnerTransferCheck = false;
        sika_SCCZoneMixTriggerHelper.bypassMixChatterAlert = false;
        sika_SCCZoneMixTriggerHelper.bypassUpdateSCMQuantity = false;
    }


    // unlocks mixes (and the materials in each mix) when there are no longer any test results attached
    public static void unlockMix(list<Test_Result__c> tList){
        list<Mix__c> lockedMixList = new list<Mix__c>();
        set<ID> mixSet = new set<ID>();
        set<ID> testSet = new set<ID>();

        RecordType rt = [select Id from RecordType where Name = 'Open' and SobjectType = 'Mix__c' limit 1];
        
        for(Test_Result__c tr : tList){
            mixSet.add(tr.Mix__c);  // IDs of ALL mixes associated with test results passed to the trigger
            testSet.add(tr.ID);
        }

        // find all LOCKED mixes associated with the test results passed to the trigger
        lockedMixList = [SELECT ID, isLocked__c FROM Mix__c WHERE ID IN: mixSet AND isLocked__c = true];

        list<Test_Result__c> remainingTestResults = new list<Test_Result__c>();
        
        // find all test results that are associated with a mix referenced in mixSet, but that weren't passed to the trigger.
        remainingTestResults = [SELECT ID, Mix__c FROM Test_Result__c WHERE Mix__c IN: mixSet AND ID NOT IN: testSet];

        // create a set of the mix ids associated with the remaining test results
        set<Id> mixesWithRemainingTestResults = new set<Id>();
        for(Test_Result__c t : remainingTestResults){
            mixesWithRemainingTestResults.add(t.Id);
        }

        // get all mixes that won't have test results remaining after the test results passed to the trigger are deleted
        list<Mix__c> mixesToUnlock = new list<Mix__c>();
            
        for(Mix__c m : lockedMixList){
            if(mixesWithRemainingTestResults.contains(m.Id)){
                continue;
            } else {
                mixesToUnlock.add(new Mix__c(id = m.ID, isLocked__c = false, RecordTypeID = rt.Id));
            }
        }
        
        sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = true;
        sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = true;
        sika_SCCZoneMixTriggerHelper.bypassMixOwnerTransferCheck = true;
        sika_SCCZoneMixTriggerHelper.bypassMixChatterAlert = true;
        sika_SCCZoneMixTriggerHelper.bypassUpdateSCMQuantity = true;

        // unlock the mixes
        update mixesToUnlock;

        // unlock all material records associated with the mixes we've just unlocked, if the material isn't associated with another locked mix
        sika_SCCZoneUnlockMaterialsHelper.unlockMaterials(mixesToUnlock);

        sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = false;
        sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = false;
        sika_SCCZoneMixTriggerHelper.bypassMixOwnerTransferCheck = false;
        sika_SCCZoneMixTriggerHelper.bypassMixChatterAlert = false;
        sika_SCCZoneMixTriggerHelper.bypassUpdateSCMQuantity = false;

    }

}