public with sharing class CommunityAccountEditController {
        public Boolean bool { get; set; }
         public Boolean bool1 { get; set; }   
        public Account acct {get; set;}
        public String testVar { get; set; }
        public String accname {get;set;}
         public String selectedCountrypro{get;set;}
        public String acczip {get;set;}
        public String accstret{get;set;}
        public String acctype {get;set;}
        public String acccity {get;set;}
        public String accstate {get;set;}
        public String accpone {get;set;}
        public String accweb {get;set;}
        List<user> adminUser;
 public PageReference SaveAccounts() {
          acct.Community_User_ID__c = UserInfo.getUserId();
          acct.ownerid=adminUser[0].id;            
            Database.upsertResult res = Database.upsert(acct);
            
            if(res.IsSuccess()){
                PageReference pr = null;
                pr = new PageReference('/apex/CommunityAccountDetail?aid=' + res.getId());

                pr.setRedirect(true);
                return pr;
            }
            else{
                return null;
            }
             return null;
        }
        

    
        public CommunityAccountEditController(){
         bool=true;
                  bool1=false;
                //determine if url parameters were passed in
        Map<string, string> pageParams = ApexPages.currentPage().getParameters();
        adminUser = [select id, name from user where profile.name='System Administrator' AND IsActive = true limit 1];
        if(pageParams.size() > 0){
                string aid = pageParams.get('aid');
                if(aid != null){
                        //string projectFields = Utilities.GetAllObjectFields('Project__c');
                        string acctFields = 'Name,Type,BillingCity,BillingState,BillingStreet,BillingPostalCode,Website,Phone,Community_User_ID__c,BillingCountry';
                string queryFormat = 'select {0} from {1} where {2}';
                string acctQuery = String.format(queryFormat, new List<string>{acctFields, 'Account', 'Id = :aid'});
                acct = Database.query(acctQuery);
                
                if(acct != null){
                  bool=false;
                  bool1=true;
                }
                }
        }
        
        if(acct == null){
            acct = new Account();
        }

        }
        
        public PageReference SaveAccount(){
               acct.Community_User_ID__c = UserInfo.getUserId();
               acct.name=accname;
               acct.Type=acctype;
               acct.BillingCity=acccity;
               acct.BillingState=selectedCountrypro;
               acct.website=accweb;
               acct.phone=accpone;
               acct.BillingStreet=accstret;
               acct.ownerid=adminUser[0].id;
               acct.BillingPostalCode=acczip;
               acct.Admin_ID__c=adminUser[0].id;
        
            Database.upsertResult res = Database.upsert(acct);
            
            if(res.IsSuccess()){
               ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'Record Saves') ;
                ApexPages.addMessage(myMsg);
                
                PageReference pr = null;
                pr = new PageReference('/apex/CommunityAccountDetail?aid=' + res.getId());

                pr.setRedirect(true);
                return pr;
            }
            else{
                return null;
            }
        }
        
    }