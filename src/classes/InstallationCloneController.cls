public class InstallationCloneController {

    static final Set<SobjectField> installationFields = new Set<SobjectField>{
        Sika_Asset_Installation__c.Direct_Feed__c,
        Sika_Asset_Installation__c.Estimated_Total_Equipment_Value__c,
        Sika_Asset_Installation__c.Location_of_Equipment_on_Site__c,
        Sika_Asset_Installation__c.Account_Name__c,
        Sika_Asset_Installation__c.Account__c
    };

    static final Set<SobjectField> assignmentFields = new Set<SobjectField>{
        Sika_Assignment__c.Name,
        Sika_Assignment__c.Sika_Asset__c,
        Sika_Assignment__c.Product__c
    };

    static final Set<SobjectField> assetFields = new Set<SobjectField>{
        Sika_Asset__c.Id, //must be included because we are fetching the object as related
        Sika_Asset__c.Asset_Name__c,
        Sika_Asset__c.Asset_Type__c,
        Sika_Asset__c.Asset_Size__c,
        Sika_Asset__c.Manufacturer__c,
        Sika_Asset__c.Purchased_from_Competitor__c,
        Sika_Asset__c.Sika_Owned_Asset__c,
        Sika_Asset__c.Status__c,
        Sika_Asset__c.Asset_Description__c,
        Sika_Asset__c.ID_Serial__c,
        Sika_Asset__c.Purchase_Date__c
    };

    @AuraEnabled
    public static LightningSobjectsAndDescribe getInstallationCloneInformation(Id installationId)
    {
        LightningSobjectsAndDescribe cloneInfo = new LightningSobjectsAndDescribe();
        cloneInfo.sobjects.put('install', Database.query('SELECT ' +
            String.join(
                mapSobjectFieldToApiNames(installationFields), ',') +
           ' FROM Sika_Asset_Installation__c' +
           ' WHERE Id =:installationId'));
        cloneInfo.sobjects.put('assigns', Database.query('SELECT ' + 
            'product__r.Name, ' +
            String.join(mapSobjectFieldToApiNames(assignmentFields), ',') +
            ',sika_asset__r.' +
            String.join(
                mapSobjectFieldToApiNames(assetFields), ', sika_asset__r.') +
            ' FROM Sika_Assignment__c' +
            ' WHERE Asset_Installation__c =: installationId'));
        cloneInfo.describe.addObjects(new List<LightningDescribe.ObjectInfo>{
            new LightningDescribe.ObjectInfo(Sika_Asset_Installation__c.SobjectType)
                .addFields(installationFields, LightningDescribe.BASELINE_FIELD_WRITES),
            new LightningDescribe.ObjectInfo(Sika_Asset__c.SobjectType)
                .addFields(assetFields, LightningDescribe.BASELINE_FIELD_WRITES),
            new LightningDescribe.ObjectInfo(Sika_Assignment__c.SobjectType)
                .addFields(assignmentFields, LightningDescribe.BASELINE_FIELD_WRITES)
        });
        return cloneInfo;
    }

    @AuraEnabled
    public static Id cloneInstallation(Sika_Asset_Installation__c installation, List<Sika_Assignment__c> assignments, List<Sika_Asset__c> assets)  {
        try { 
            Sika_Asset_Installation__c installationFullFields = buildInstallationClone(installation);

            Map<Id, Sika_Asset__c> assetFullFieldsIdMap = buildAssetClonesMap(assets);

            List<Sika_Assignment__c> assignmentsFullFields = buildAssignmentClones(assignments);
            Savepoint sp = Database.setSavepoint();
            try {
                insert installationFullFields;
                insert assetFullFieldsIdMap.values();
                for (Sika_Assignment__c assignment : assignmentsFullFields) {
                    assignment.Asset_Installation__c = installationFullFields.Id;
                    assignment.Sika_Asset__c = assetFullFieldsIdMap.get(assignment.Sika_Asset__c).Id;
                }
                insert assignmentsFullFields;
                return installationFullFields.Id;
            } catch (Exception e) {
                Database.rollback(sp);
                throw new AuraHandledException(e.getMessage() + e.getStackTraceString());
            }
        } catch (AuraHandledException e) {
            throw e;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage() + e.getStackTraceString());
        }
        return null;
    }
    
    static List<String> mapSobjectFieldToApiNames(Set<SobjectField> fields) {
        List<String> results = new List<String>();
        for (SobjectField field : fields) {
            results.add(field.getDescribe().getName());
        }
        return results;
    }


    static Sika_Asset_Installation__c buildInstallationClone(Sika_Asset_Installation__c updates) {
        Sika_Asset_Installation__c installationFullFields = (Sika_Asset_Installation__c)
            getSavableValues(Sika_Asset_Installation__c.SobjectType, new Set<Id>{updates.Id}).get(0);
        installationFullFields.Id = null;
        Set<SobjectField> uxEditableFields = new Set<SobjectField>{
            Sika_Asset_Installation__c.Direct_Feed__c,
            Sika_Asset_Installation__c.Estimated_Total_Equipment_Value__c,
            Sika_Asset_Installation__c.Location_of_Equipment_on_Site__c,
            Sika_Asset_Installation__c.Account__c
        };
        for (SobjectField uxEditableField : uxEditableFields) {
            installationFullFields.put(uxEditableField, updates.get(uxEditableField));
        }
        return installationFullFields;
    }

    static List<Sika_Assignment__c> buildAssignmentClones(List<Sika_Assignment__c> updates) {
        List<Sika_Assignment__c> assignmentsFullFields = getSavableValues(
            Sika_Assignment__c.SobjectType, pluck.ids(updates));
        Map<Id, Sika_Assignment__c> assignmentsFullFieldsIdMap = new Map<Id, Sika_Assignment__c>(
            assignmentsFullFields);
        Set<SobjectField> uxEditableFields = new Set<SobjectField>{
            Sika_Assignment__c.Product__c
        };
        for (Sika_Assignment__c assignmentUpdate : updates) {
            Sika_Assignment__c newAssignment = assignmentsFullFieldsIdMap.get(assignmentUpdate.Id);
            newAssignment.Id = null;
            for (SobjectField uxEditableField : uxEditableFields) {
                newAssignment.put(uxEditableField, assignmentUpdate.get(uxEditableField));
            }
        }
        return assignmentsFullFields;
    }

    static Map<Id, Sika_Asset__c> buildAssetClonesMap(List<Sika_Asset__c> assets) {
        Map<Id, Sika_Asset__c> assetFullFieldsIdMap = new Map<Id, Sika_Asset__c>(
            (List<Sika_Asset__c>) getSavableValues(
                Sika_Asset__c.SobjectType,
                //pluck.ids(list<sobject>) does not apply for multiple sobjects of same Id
                pluck.ids('Id', assets)
            )
        );
        Set<SobjectField> uxEditableFields = new Set<SobjectField>{
            Sika_Asset__c.Asset_Description__c,
            Sika_Asset__c.Status__c,
            Sika_Asset__c.ID_Serial__c
        };
        for (Sika_Asset__c assetSobj : assets) {
            Sika_Asset__c newAsset = assetFullFieldsIdMap.get(assetSobj.Id);
            newAsset.Id = null;
            for (SobjectField uxEditableField : uxEditableFields) {
                newAsset.put(uxEditableField, assetSobj.get(uxEditableField));
            }
            // as of winter 17 salesforce coerces date fields into datetime 
            // when passed from lightning to server
            Object purchaseDatetime = assetSobj.get(Sika_Asset__c.Purchase_Date__c);
            if (purchaseDatetime == null) {
                newAsset.Purchase_Date__c = null;
            } else {
                newAsset.Purchase_Date__c = ((DateTime) purchaseDatetime).date();
            }
        }
        return assetFullFieldsIdMap;
    }

    static List<Sobject> getSavableValues(SObjectType type, set<Id> recordIds) {
        DescribeSObjectResult description = type.getDescribe();
        Map<String, SobjectField> fieldByApiName = description.fields.getMap();
        List<String> savableFieldNames = new List<String>();
        for (String fieldApiName : fieldByApiName.keySet()) {
            DescribeFieldResult token = fieldByApiName.get(fieldApiName).getDescribe();
            if (!token.isCalculated() && token.isUpdateable()) {
                savableFieldNames.add(fieldApiName);
            }
        }
        return Database.query('SELECT ' +
            System.String.join(savableFieldNames, ',') +
            ' FROM ' + description.getName() + ' WHERE Id IN (\'' +
            System.String.join(new List<Id>(recordIds), '\', \'') +
            '\')' );
    }
}