public with sharing class OpportunityShareAtUserLevelBatchServices {
    public static void processBatch(List<User> scope) {
        Set<Id> accountIds = new Set<Id>();
        Set<Id> userIdsToInsertShare = new Set<Id>();
        Map<Id, List<User>> usersByProfile = UserDao.createUsersByProfileId(scope);//To get users for Profile Partner users (ex. Name: PTS Partner Community)

        for (User usr : scope) {
            if (usr.IsPortalEnabled && usr.ContactId != null && usr.AccountId != null) {
                if (usersByProfile.get(usr.ProfileId) != null && usersByProfile.containsKey(usr.ProfileId)) {
                    if (usr.IsActive == true) {
                        accountIds.add(usr.AccountId);
                        userIdsToInsertShare.add(usr.Id);
                    }
                }
            }
        }

        if(!accountIds.isEmpty() && !userIdsToInsertShare.isEmpty()) {
            List<Applicator_Quote__c> applicatorQuotes = [SELECT Id FROM Applicator_Quote__c WHERE Account__c IN :accountIds];
            update applicatorQuotes;
        }
    }
}