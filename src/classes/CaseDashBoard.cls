public with sharing class CaseDashBoard{ 
    public List<AggregateResult> TrendingToday {get; set;}
    public List<AggregateResult> TrendingThisWeek {get; set;}
    public List<AggregateResult> TrendingThisMonth {get; set;} 
    public List<AggregateResult> TrendingThisYear {get; set;} 
    public List<AggregateResult> OpenCaseTiming {get; set;} 
    
    public CaseDashBoard(){ 
        TrendingToday = 
            [
            SELECT Category__c, SUM(Count__c) Total
            FROM Case
            WHERE CreatedDate = TODAY AND RecordType.Name in ('Internal IT Support (Closed)','Internal IT Support')
            GROUP BY Category__c
            ORDER BY SUM(Count__c) DESC
            LIMIT 5
            ];

        TrendingThisWeek = 
            [
            SELECT Category__c, SUM(Count__c) Total
            FROM Case
            WHERE CreatedDate = THIS_WEEK AND RecordType.Name in ('Internal IT Support (Closed)','Internal IT Support')
            GROUP BY Category__c
            ORDER BY SUM(Count__c) DESC
            LIMIT 5
            ];

        TrendingThisMonth = 
            [
            SELECT Category__c, SUM(Count__c) Total
            FROM Case
            WHERE CreatedDate = THIS_MONTH AND RecordType.Name in ('Internal IT Support (Closed)','Internal IT Support')
            GROUP BY Category__c
            ORDER BY SUM(Count__c) DESC
            LIMIT 5
            ];

        TrendingThisYear = 
            [
            SELECT Category__c, SUM(Count__c) Total
            FROM Case
            WHERE CreatedDate = THIS_YEAR AND RecordType.Name in ('Internal IT Support (Closed)','Internal IT Support')
            GROUP BY Category__c
            ORDER BY SUM(Count__c) DESC
            LIMIT 5
            ];
        
        OpenCaseTiming = 
            [
            SELECT Owner.Name Owner, SUM(Count__c) Total
            FROM Case
            WHERE RecordType.Name in ('Internal IT Support (Closed)','Internal IT Support') AND Case_Duration_Time_Current__c >= 7 And Status <> 'Closed'
            GROUP BY Owner.Name
            ORDER BY Owner.Name ASC
            ];

    }    
}