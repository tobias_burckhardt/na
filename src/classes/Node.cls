public class Node {

	public Node(){

		children = new List<Node>();
	}

    public String title { get; set; }
    public List<Node> children { get; set; }

    public String parent { get; set; }
    public String path {get; set; }
    
    public Boolean hasChildren{

    	get{

    		return children.size() > 0;
    	}
    }
}