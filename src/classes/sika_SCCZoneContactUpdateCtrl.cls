public without sharing class sika_SCCZoneContactUpdateCtrl {
	
	public User theUser {get; set;} // set to public so we can use the SFDC default related State/Country selects from the User object. (The user doesn't have write access to the Contact object.)
	private Contact theContact;

	public String firstName {get; set;}
	public String lastName {get; private set;}
	public String jobTitle {get; set;}
	public String companyName {get; private set;}
	public String companyType {get; private set;}
	public String emailAddress {get; set;}
	public String phone {get; set;}
	public String streetAddress {get; set;}
	public String city {get; set;}
	public String state {get; set;}
	public String postalCode {get; set;}
	public String country {get; set;}
	public String som {get; set;}

	public sika_SCCZoneContactUpdateCtrl() {
		sika_SCCZoneCheckAuth.setAuthStatus(UserInfo.getProfileId());

		// get the current user's ID
		Id theUserId = UserInfo.getUserId();

		// query the User & get relevant info -- i.e., the info we'll be updating, plus the ID of the User's associated Contact record
		theUser = [SELECT Id, 
						  ContactId, 
						  FirstName, 
						  LastName, 
						  Title, 
						  Email, 
						  Phone, 
						  Street, System_of_Measure__c,
						  City, 
					//StateCode,
						  State, 
						  PostalCode, 
						  Country
					//CountryCode 
					 FROM User 
					WHERE Id = :theUserId];

		// query for the User's associated Contact record
		theContact = [Select Id, 
							 FirstName, 
							 LastName, 
							 Company_Type__c, 
							 Account.Name, 
							 Title, 
							 Email, 
							 Phone, 
							 MailingStreet, 
							 MailingCity, 
						//MailingStateCode,
							 MailingState,
							 MailingPostalCode,
						//MailingCountryCode
							 MailingCountry
						FROM Contact 
					   WHERE Id = :theUser.ContactId];

		// set field values...just to make things less confusing! (user field vs contact field)
		firstName = theUser.FirstName;
		lastName = theUser.LastName;
		jobTitle = theContact.Title;
		emailAddress = theContact.Email;
		phone = theContact.Phone;
		streetAddress = theContact.MailingStreet;
		city = theContact.MailingCity;
		state = theContact.MailingState;
		postalCode = theContact.MailingPostalCode;
		country = theContact.MailingCountry;

		companyName = theContact.Account.Name;
		companyType = theContact.Company_Type__c;
		som = theUser.System_of_Measure__c;

	}

	// check user auth status
    public PageReference loadAction(){
        PageReference authCheck;

        // because this controller runs "without sharing" we need to manually confirm that the user should have access. (A user whose session has timed-out, or an otherwise unauthenticated user who accessed this page directly, would be able to view the page otherwise, albeit without any data displayed.)
        authCheck = sika_SCCZoneCheckAuth.doAuthCheck();
        if(authCheck != null){
            return authCheck;
        }
        return null;
    }


	@testVisible private Database.SaveResult userSaveResult;
	@testVisible private Database.SaveResult contactSaveResult;

	public PageReference save(){

		if(validateForm()){

			// set updated User info
			theUser.FirstName = firstName;
			theUser.LastName = lastName;
			theUser.Title = jobTitle;
			theUser.Email = emailAddress;
			theUser.Phone = phone;
			theUser.Street= streetAddress;
			theUser.City = city;
			theUser.State = state;
			theUser.PostalCode = postalCode;
			theUser.Country = country;

			// set updated Contact info
			theContact.FirstName = firstName;
			theContact.LastName = lastName;
			theContact.Title = jobTitle;
			theContact.Email = emailAddress;
			theContact.Phone = phone;
			theContact.MailingStreet = streetAddress;
			theContact.MailingCity = city;
			theContact.MailingState = state;
			theContact.MailingPostalCode = postalCode;
			theContact.MailingCountry = country;

			// update the User info
			userSaveResult = Database.update(theUser);
			if(userSaveResult.isSuccess()){
				system.debug('******** userSaveResult == success!');
			} else {
				system.debug('******** userSaveReult == fail. Msg: ' + userSaveResult.getErrors());
				ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.FATAL, 'User info update has failed. Please contact our system administrator for assistance.'));
				return null;
			}

			// update the Contact info
			contactSaveResult = Database.update(theContact);
			if(contactSaveResult.isSuccess()){
				system.debug('******** contactSaveResult == success!');
			} else {
				system.debug('******** contactSaveResult == fail. Msg: ' + contactSaveResult.getErrors());	
				ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.FATAL, 'Contact info update has failed. Please contact our system administrator for assistance.'));
				return null;
			}

			// if validation and DML updates are successful...
			return new PageReference('/sika_SCCZoneUserHomePage');
		}
		return null;

	}

	public PageReference cancel(){
		return new PageReference('/sika_SCCZoneUserHomePage');
	}


	// form validation
	public Boolean hasErrors;

    public Boolean emailIsBlank {get; private set;}
    public Boolean emailIsInvalid {get; private set;}
    public Boolean phoneError {get; private set;}
    public Boolean jobTitleError {get; private set;}
    public Boolean streetAddressError {get; private set;}
    public Boolean cityError {get; private set;}
    public Boolean stateError {get; private set;}
    public Boolean postalCodeError {get; private set;}
    public Boolean countryError {get; private set;}

	@testVisible private Boolean validateForm(){
        // initialize all error Booleans to false. (Ensures we don't reference any null objects, or get tricked into thinking "not false" equals "true"!)
        hasErrors = false;
        emailIsBlank = false;
        emailIsInvalid = false;
        jobTitleError = false;
        streetAddressError = false;
        cityError = false;
        stateError = false;
        postalCodeError = false;
        countryError = false;

        if(String.isBlank(emailAddress)){
            hasErrors = true;
            emailIsBlank = true;
        } else {
            emailIsBlank = false;
            // we'll display a different message for an invalid email vs an empty email field
            if(sika_SCCZoneValidationHelper.validateEmail(emailAddress) == false){
                hasErrors = true;
                emailIsInvalid = true;
            }
        }

        if(String.isBlank(phone)){
            hasErrors = true;
            phoneError = true;
        }
        if(String.isBlank(jobTitle)){
            hasErrors = true;
            jobTitleError = true;
        }
        if(String.isBlank(streetAddress)){
            hasErrors = true;
            streetAddressError = true;
        }
        if(String.isBlank(city)){
            hasErrors = true;
            cityError = true;
        }
        if(String.isBlank(country)){
            hasErrors = true;
            countryError = true;
        }

        if(String.isBlank(state)){
            hasErrors = true;
            stateError = true;
        }
        
        if(String.isBlank(String.valueOf(postalCode))){
            hasErrors = true;
            postalCodeError = true;
        }

        

        // set (or un-set) the form validation error status and error messages
        if(hasErrors){
            system.debug('******** hasErrors = ' + hasErrors);
            system.debug('******** emailIsBlank = ' + emailIsBlank);
	        system.debug('******** emailIsInvalid = ' + emailIsInvalid);
	        system.debug('******** jobTitleError = ' + jobTitleError);
	        system.debug('******** streetAddressError = ' + streetAddressError);
	        system.debug('******** cityError = ' + cityError);
	        system.debug('******** stateError = ' + stateError);
	        system.debug('******** postalCodeError = ' + postalCodeError);
	        system.debug('******** countryError = ' + countryError);
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please fill in all of the fields highlighted below correctly.'));
            if(stateError){
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'The state/province selection box is required for your country.'));

            }
         
            if(emailIsInvalid == true){
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'A valid email address is required.'));
            } 
            return false;
        }
        return true;
    } 
}