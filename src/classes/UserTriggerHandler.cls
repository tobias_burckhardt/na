/**
* @author Garvit Totuka
* @date 18 Sep 2017
*
* @group user Sharing Management
*
* @description user Trigger handler handles apex trigger for User object
*/
public with sharing class UserTriggerHandler {

	/*
	Control variable that indicates after update methods already executed to prevent unintentional recursion
	*/
	public static Boolean onAfterUpdate_FirstRun = true;
	public static Boolean onAfterInsert_FirstRun = true;
	public static Boolean skipTrigger = false;

	/*
	Indicates the number of records to be processed on the thread
	*/
	private Integer batchSize = 0;

	/*
	Indicates whether the handler is being executed within a trigger context
	*/
	public  Boolean isTriggerContext { get; private set; }

	/*
	Constructor
	@param Boolean isTriggerContextParam - indicates whether the handler is being executed within a trigger context
	@param Integer batchSizeParam - indicates the number of records to be processed on the thread
	*/
	public UserTriggerHandler(Boolean isTriggerContextParam, Integer batchSizeParam) {
		isTriggerContext = (isTriggerContextParam == null) ? false : isTriggerContextParam;
		batchSize = batchSizeParam;
	}

	/*
	Method used to handle recursion for After Insert
	*/
	public void onAfterInsert(List<User> newList, Map<Id, User> newMap) {
		if (onAfterInsert_FirstRun) {
			onAfterUpdate_FirstRun = false;
            //onAfterInsert_FirstRun???
		}
	}

	/*
	Method used to handle recursion for After Update
	*/
	public void onAfterUpdate(List<User> newList, Map<Id, User> oldMap) {
		if (onAfterUpdate_FirstRun) {
			 onAfterUpdate_FirstRun = false;
		}
	}

	/*
	Method used to check User IsActive Status i.e. User is activated or deactivated, to perform action on Opportunity Share object
	*/
	public void checkUserActivationOnUpdate (List<User> triggerNew, Map<Id,User> oldMap){
		if(skipTrigger) {
			return;
		}
		List<Id> communityProfileIds = getCommunityProfileIds();
		List<User> usersToShareTo = new List<User>();
		Set<Id> usersToDeleteShareFrom = new Set<Id>();
		List<Profile> communityProfiles = UserDAO.getUserPartnerProfileIds();
		for(User usr : triggerNew){
			if(isEligablePortalUser(usr,communityProfileIds)){
				if(usr.IsActive != oldMap.get(usr.Id).IsActive){
					if(usr.IsActive) {
                        System.debug('User Added: ' + usr);
						usersToShareTo.add(usr);
					}else{
						usersToDeleteShareFrom.add(usr.Id);
					}
				}
			}
		}
		if(!usersToShareTo.isEmpty()){
			List<Id> userIdsToShareTo = new List<Id>();
			for(User usr : usersToShareTo){
				userIdsToShareTo.add(usr.Id);
			}
			gatherOpportunityAndQuoteReordsToShare(userIdsToShareTo);
		}
		if(!usersToDeleteShareFrom.isEmpty()){
			unshareOpps(usersToDeleteShareFrom);
		}
	}

	public void checkUserActivationOnInsert (List<User> triggerNew){
		if(skipTrigger) {
			return;
		}
		System.debug('in checkUserActivationOnInsert');
		List<Id> communityProfileIds = getCommunityProfileIds();
		List<User> usersToShareTo = new List<User>();
		Set<Id> usersToDeleteShareFrom = new Set<Id>();
		for(User usr : triggerNew){
			System.debug(usr);
			if(isEligablePortalUser(usr,communityProfileIds)){
				System.debug('isEligablePortalUser = true ');
				if(usr.IsActive) {
					System.debug('isActive = true');
					usersToShareTo.add(usr);
				}else{
					usersToDeleteShareFrom.add(usr.Id);
				}
			}
		}
		if(!usersToShareTo.isEmpty()){
			List<Id> userIdsToShareTo = new List<Id>();
			for(User usr : usersToShareTo){
				userIdsToShareTo.add(usr.Id);
			}
			gatherOpportunityAndQuoteReordsToShare(userIdsToShareTo);
		}
		if(!usersToDeleteShareFrom.isEmpty()){
			unshareOpps(usersToDeleteShareFrom);
		}
	}

	private List<Id> getCommunityProfileIds(){
		List<Id> communityProfileIds = new List<Id>();
		for(Profile prof : UserDAO.getUserPartnerProfileIds()){
			communityProfileIds.add(prof.Id);
		}
		return communityProfileIds;
	}

	private boolean isEligablePortalUser(User usr, List<Id> communityProfileIds){
		System.debug('usr.IsPortalEnabled: ' + usr.IsPortalEnabled);
		System.debug('usr.ContactId: ' + usr.ContactId);
		System.debug('usr.AccountId: ' + usr.AccountId);
		System.debug('communityProfileIds.contains(usr.ProfileId): ' + communityProfileIds.contains(usr.ProfileId));
		System.debug('profile id: ' + usr.ProfileId);
		System.debug('profile list: ' + communityProfileIds);
		return (usr.IsPortalEnabled && usr.ContactId != null && usr.AccountId != null && communityProfileIds.contains(usr.ProfileId));
	}

	@future
	public static void gatherOpportunityAndQuoteReordsToShare(List<Id> usrIdList){
        System.debug('Gather Opportunity and Quote Records to Share');
		List<Id> accountIds = new List<Id>();
		List<Id> oppIds = new List<Id>();
		List<User> usrList = [SELECT Id, AccountId FROM User WHERE Id IN :usrIdList];
		System.debug('usrList: ' + usrList);
		for(User usr : usrList){
			accountIds.add(usr.AccountId);
		}
		List<Applicator_Quote__c> applicatorQuotes = [SELECT Id,
                                                      Opportunity__c,
                                                      Account__c
                                                      FROM Applicator_Quote__c
                                                      WHERE Account__c IN :accountIds
                                                      AND Opportunity__r.RecordType.Name = 'roofing'
                                                      AND (Opportunity__r.StageName = 'Closed Won' 
                                                           OR Opportunity__r.StageName = 'Quote'
                                                           OR Opportunity__r.StageName = 'Open')];
        
        System.debug(applicatorQuotes.size());
        
		Map<Id,List<Id>> userToApplicatorQuoteMap = createUserToApplicatorQuoteMap(applicatorQuotes,usrList);
        
		for (Applicator_Quote__c quote : applicatorQuotes){
			oppIds.add(quote.Opportunity__c);
		}
														
		List<Opportunity> opportunities = [SELECT Id,
                                           AccountId,
                                           StageName
											FROM Opportunity
											WHERE Id IN :oppIds
											OR AccountId IN :accountIds];
		System.debug(opportunities);
		Map<Id,List<Id>> userToOpportunityMap = new Map<Id,List<Id>>();

		for(Opportunity opp : opportunities){
			addOpportunityToUserOpportunityMap(userToOpportunityMap, opp, usrList, applicatorQuotes);
		}
        
		
		ApplicatorQuoteSharingServices.addApplicatorQuoteSharesToCommunityUsers(userToApplicatorQuoteMap);
		OpportunitySharingServices.addOpportunitySharesToCommunityUsers(userToOpportunityMap);
	}

	public static void addOpportunityToUserOpportunityMap(Map<Id,List<Id>> userToOpportunityMap,Opportunity opp, List<User> usrList, List<Applicator_Quote__c> applicatorQuotes){
		for(User usr : usrList){
            if(usr.AccountId == opp.AccountId){
                putOrAddOpportunityToUser(userToOpportunityMap, opp, usr);
            }
            for(Applicator_Quote__c quote : applicatorQuotes){
                if(usr.AccountId == quote.Account__c &&
                  opp.Id == quote.Opportunity__c){
                      if(opp.StageName == 'Closed Won'){
                          if (opp.AccountId == usr.AccountId){
                              putOrAddOpportunityToUser(userToOpportunityMap, opp, usr);
                          }
                      }else{
                          putOrAddOpportunityToUser(userToOpportunityMap, opp, usr);
                      }
                }
            }
        }
	}
    
    public static void putOrAddOpportunityToUser(Map<Id,List<Id>> userToOpportunityMap, Opportunity opp, User usr){
        if(!userToOpportunityMap.containsKey(usr.Id)){
            List<Id> oppIdsPerUser = new List<Id>{opp.Id};
            userToOpportunityMap.put(usr.id,oppIdsPerUser);
        }else{
            List<Id> oppList = userToOpportunityMap.get(usr.id);
            oppList.add(opp.id);
            userToOpportunityMap.put(usr.id,oppList);
        }
    }

	public static Map<Id,List<Id>> createUserToApplicatorQuoteMap(List<Applicator_Quote__c> quotes,List<User> usrList){
		Map<Id,List<Id>> userToApplicatorQuoteMap = new Map<Id,List<Id>>();
		for(Applicator_Quote__c quote : quotes){
			for(User usr : usrList){
				if(usr.AccountId == quote.Account__c){
					if(!userToApplicatorQuoteMap.containsKey(usr.Id)){
						List<Id> quoteIdsPerUser = new List<Id>{quote.id};
						userToApplicatorQuoteMap.put(usr.id,quoteIdsPerUser);
					}else{
						List<Id> quoteList = userToApplicatorQuoteMap.get(usr.id);
						quoteList.add(quote.id);
						userToApplicatorQuoteMap.put(usr.id,quoteList);
					}
				}
			}
		}
		return userToApplicatorQuoteMap;
	}

	//remove
	@Future
	static void shareApplicatorQuotes(Set<Id> accountIds) {
		List<Applicator_Quote__c> applicatorQuotes = [SELECT Id FROM Applicator_Quote__c WHERE Account__c IN :accountIds];
		ApplicatorQuoteSharingServices.shareApplicatorQuoteWithPTS(applicatorQuotes);
		List<Opportunity> opportunities = [SELECT Id FROM Opportunity WHERE AccountId IN :accountIds];
		OpportunitySharingServices.shareOppsWithPTS(opportunities);
	}

	//remove
	@Future
	static void unshareOpps(Set<Id> userIds) {
		List<OpportunityShare> oppSharesToDelete = [SELECT Id FROM OpportunityShare WHERE RowCause = :'Manual' AND UserOrGroupId IN :userIds];
		List<Applicator_Quote__Share> aqSharesToDelete = [SELECT Id FROM Applicator_Quote__Share WHERE RowCause = :'Manual' AND UserOrGroupId IN :userIds];
		delete oppSharesToDelete;
		delete aqSharesToDelete;
	}

}