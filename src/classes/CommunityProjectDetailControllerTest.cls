@isTest 
public class CommunityProjectDetailControllerTest {
    static testMethod void TestCommunityProjectDetailController() {

        Project__c p1 = new Project__c();
        insert p1;
        
        Warranty__c w1 = new Warranty__c();
        w1.Project__c = p1.id;
        insert w1;

        CommunityProjectDetailController cpdc1 = new CommunityProjectDetailController();
        test.startTest();
        cpdc1.deleteWarranties();

        ApexPages.currentPage().getParameters().put('pid', p1.id);
        ApexPages.currentPage().getParameters().put('ProjNum', '0');
        ApexPages.currentPage().getParameters().put('WarrNum', '0');
        
        CommunityProjectDetailController cpdc2 = new CommunityProjectDetailController();
        cpdc2.deleteWarranties();
        test.stopTest();

    }
}