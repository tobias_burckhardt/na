@isTest
private class SendOpportunityToSAPTest {
    private static final String CUSTOMER_NUMBER = '4321';

    @testSetup
    static void testSetup() {
        Opportunity oppty = TestUtilities.createOpportunities(TestUtilities.createAccounts(1, true), 1, false).get(0);
        oppty.Language__c = 'EN';
        oppty.SAP_Name_40__c = 'SAP Name 40';
        oppty.Project__c = TestUtilities.createProjects(1, true).get(0).Id;
        insert oppty;
    }

    static testmethod void testCreateProjectInSAP() {
        Opportunity oppty = [SELECT Id FROM Opportunity LIMIT 1];
        ApexPages.StandardController stdController = new ApexPages.StandardController(oppty);
        SendOpportunityToSAP ctrl = new SendOpportunityToSAP(stdController);

        Test.setMock(WebServiceMock.class, new SAPAccountIntegrationMockImpl());

        Test.startTest();
        PageReference pageRef = ctrl.createProjectInSAP();
        Test.stopTest();

        oppty = [SELECT Project_AccountId__c FROM Opportunity WHERE Id = :oppty.Id];

        System.assert(!ApexPages.hasMessages(), 'The page should not have any message.');
        System.assertEquals(SAPAccountIntegrationMockImpl.CUSTOMER_NUMBER, oppty.Project_AccountId__c,
            'It should have updated the opportunity\'s Project_AccountId__c');
        System.assertEquals('/' + oppty.Id, pageRef.getUrl(), 'It should redirect back to the Opportunity.');
    }

    static testmethod void testCreateProjectInSAPMissingField() {
        Opportunity oppty = [SELECT Id FROM Opportunity LIMIT 1];
        oppty.Language__c = null;
        update oppty;

        ApexPages.StandardController stdController = new ApexPages.StandardController(oppty);
        SendOpportunityToSAP ctrl = new SendOpportunityToSAP(stdController);

        Test.setMock(WebServiceMock.class, new SAPAccountIntegrationMockImpl());

        Test.startTest();
        PageReference pageRef = ctrl.createProjectInSAP();
        Test.stopTest();

        oppty = [SELECT Project_AccountId__c FROM Opportunity WHERE Id = :oppty.Id];
        Integer logCount = [SELECT COUNT() FROM ExLog__c];

        System.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR), 'The page should have error messages.');
        System.assertEquals(2, ApexPages.getMessages().size(), 'It should have two messages.');
        System.assert(ApexPages.getMessages().get(1).getSummary().contains(SAPAccountIntegrationMockImpl.LANGUAGE_KEY_MISSING),
            'It should have the correct error message.');
        System.assertEquals(null, oppty.Project_AccountId__c,
            'It should mpt have updated the opportunity\'s Project_AccountId__c');
        System.assertEquals(null, pageRef, 'It should refresh the page.');
        System.assertEquals(1, logCount, 'It should have created one exception log.');
    }

    static testmethod void testCreateShipToInSAP() {
        Opportunity oppty = [SELECT Id FROM Opportunity LIMIT 1];
        oppty.Project_AccountId__c = CUSTOMER_NUMBER;
        update oppty;

        ApexPages.StandardController stdController = new ApexPages.StandardController(oppty);
        SendOpportunityToSAP ctrl = new SendOpportunityToSAP(stdController);

        Test.setMock(WebServiceMock.class, new SAPAccountIntegrationMockImpl());

        Test.startTest();
        PageReference pageRef = ctrl.createShipToInSAP();
        Test.stopTest();

        oppty = [SELECT ShipTo_AccountId__c FROM Opportunity WHERE Id = :oppty.Id];

        System.assert(!ApexPages.hasMessages(), 'The page should not have any message.');
        System.assertEquals(SAPAccountIntegrationMockImpl.CUSTOMER_NUMBER, oppty.ShipTo_AccountId__c,
            'It should have updated the opportunity\'s ShipTo_AccountId__c');
        System.assertEquals('/' + oppty.Id, pageRef.getUrl(), 'It should redirect back to the Opportunity.');
    }

    static testmethod void testCreateShipToInSAPMissingField() {
        Opportunity oppty = [SELECT Id FROM Opportunity LIMIT 1];
        oppty.Project_AccountId__c = CUSTOMER_NUMBER;
        oppty.Language__c = null;
        update oppty;

        ApexPages.StandardController stdController = new ApexPages.StandardController(oppty);
        SendOpportunityToSAP ctrl = new SendOpportunityToSAP(stdController);

        Test.setMock(WebServiceMock.class, new SAPAccountIntegrationMockImpl());

        Test.startTest();
        PageReference pageRef = ctrl.createShipToInSAP();
        Test.stopTest();

        oppty = [SELECT ShipTo_AccountId__c FROM Opportunity WHERE Id = :oppty.Id];
        Integer logCount = [SELECT COUNT() FROM ExLog__c];

        System.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR), 'The page should have error messages.');
        System.assertEquals(2, ApexPages.getMessages().size(), 'It should have two messages.');
        System.assert(ApexPages.getMessages().get(1).getSummary().contains(SAPAccountIntegrationMockImpl.LANGUAGE_KEY_MISSING),
            'It should have the correct error message.');
        System.assertEquals(null, oppty.ShipTo_AccountId__c,
            'It should mpt have updated the opportunity\'s ShipTo_AccountId__c');
        System.assertEquals(null, pageRef, 'It should refresh the page.');
        System.assertEquals(1, logCount, 'It should have created one exception log.');
    }

    static testmethod void testCreateShipToInSAPMissingAccount() {
        Opportunity oppty = [SELECT Id FROM Opportunity LIMIT 1];

        ApexPages.StandardController stdController = new ApexPages.StandardController(oppty);
        SendOpportunityToSAP ctrl = new SendOpportunityToSAP(stdController);

        Test.setMock(WebServiceMock.class, new SAPAccountIntegrationMockImpl());

        Test.startTest();
        PageReference pageRef = ctrl.createShipToInSAP();
        Test.stopTest();

        oppty = [SELECT ShipTo_AccountId__c FROM Opportunity WHERE Id = :oppty.Id];

        System.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR), 'The page should have error messages.');
        System.assertEquals(1, ApexPages.getMessages().size(), 'It should have two messages.');
        System.assert(ApexPages.getMessages().get(0).getSummary().contains(SendOpportunityToSAP.EMPTY_PROJECT_ACCOUNT_ERROR),
            'It should have the correct error message.');
        System.assertEquals(null, oppty.ShipTo_AccountId__c,
            'It should mpt have updated the opportunity\'s ShipTo_AccountId__c');
        System.assertEquals(null, pageRef, 'It should refresh the page.');
    }

    static testmethod void testCreateContractInSAP() {
        Opportunity oppty = [SELECT Id FROM Opportunity LIMIT 1];
        oppty.Project_AccountId__c = CUSTOMER_NUMBER;
        oppty.ShipTo_AccountId__c = CUSTOMER_NUMBER;
        update oppty;

        ApexPages.StandardController stdController = new ApexPages.StandardController(oppty);
        SendOpportunityToSAP ctrl = new SendOpportunityToSAP(stdController);

        Test.setMock(WebServiceMock.class, new SAPContractIntegrationMockImpl());

        Test.startTest();
        PageReference pageRef = ctrl.createContractInSAP();
        Test.stopTest();

        oppty = [SELECT SAP_ContractId__c FROM Opportunity WHERE Id = :oppty.Id];

        System.assert(!ApexPages.hasMessages(), 'The page should not have any message.');
        System.assertEquals(SAPContractIntegrationMockImpl.CONTRACT_NUMBER, oppty.SAP_ContractId__c,
            'It should have updated the opportunity\'s SAP_ContractId__c');
        System.assertEquals('/' + oppty.Id, pageRef.getUrl(), 'It should redirect back to the Opportunity.');
    }

    static testmethod void testCreateContractInSAPMissingField() {
        Opportunity oppty = [SELECT Id FROM Opportunity LIMIT 1];
        oppty.Project_AccountId__c = CUSTOMER_NUMBER;
        oppty.ShipTo_AccountId__c = CUSTOMER_NUMBER;
        oppty.SAP_Name_40__c = null;
        update oppty;

        ApexPages.StandardController stdController = new ApexPages.StandardController(oppty);
        SendOpportunityToSAP ctrl = new SendOpportunityToSAP(stdController);

        Test.setMock(WebServiceMock.class, new SAPContractIntegrationMockImpl());

        Test.startTest();
        PageReference pageRef = ctrl.createContractInSAP();
        Test.stopTest();

        oppty = [SELECT SAP_ContractId__c FROM Opportunity WHERE Id = :oppty.Id];
        Integer logCount = [SELECT COUNT() FROM ExLog__c];

        System.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR), 'The page should have error messages.');
        System.assertEquals(2, ApexPages.getMessages().size(), 'It should have two messages.');
        System.assert(ApexPages.getMessages().get(1).getSummary().contains(SAPContractIntegrationMockImpl.JOB_NAME_MISSING),
            'It should have the correct error message.');
        System.assertEquals(null, oppty.SAP_ContractId__c,
            'It should mpt have updated the opportunity\'s SAP_ContractId__c');
        System.assertEquals(null, pageRef, 'It should refresh the page.');
        System.assertEquals(1, logCount, 'It should have created one exception log.');
    }

    static testmethod void testCreateContractInSAPMissingShipTo() {
        Opportunity oppty = [SELECT Id FROM Opportunity LIMIT 1];
        oppty.Project_AccountId__c = CUSTOMER_NUMBER;
        update oppty;

        ApexPages.StandardController stdController = new ApexPages.StandardController(oppty);
        SendOpportunityToSAP ctrl = new SendOpportunityToSAP(stdController);

        Test.setMock(WebServiceMock.class, new SAPContractIntegrationMockImpl());

        Test.startTest();
        PageReference pageRef = ctrl.createContractInSAP();
        Test.stopTest();

        oppty = [SELECT SAP_ContractId__c FROM Opportunity WHERE Id = :oppty.Id];

        System.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR), 'The page should have error messages.');
        System.assertEquals(1, ApexPages.getMessages().size(), 'It should have two messages.');
        System.assert(ApexPages.getMessages().get(0).getSummary().contains(SendOpportunityToSAP.EMPTY_SHIP_TO_ERROR),
            'It should have the correct error message.');
        System.assertEquals(null, oppty.SAP_ContractId__c,
            'It should mpt have updated the opportunity\'s SAP_ContractId__c');
        System.assertEquals(null, pageRef, 'It should refresh the page.');
    }
}