@isTest
private class sika_SCCZoneSieveCalculationTest {
	private static Integer i;

	@isTest static void testSieveValueCalculation(){

		// create an instance of the controller class and its extension class
		sika_SCCZoneMaterialsHomePageController ctrl = new sika_SCCZoneMaterialsHomePageController();
		sika_SCCZoneCreateEditMaterialCtrlExt3 ext = new sika_SCCZoneCreateEditMaterialCtrlExt3(ctrl);

		// create a test list of sieveWrappers here, so I don't have to keep typing in the long path to the class!
		list<sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper> testWrappers = new list<sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper>();

		// start test for metric calculation
		ext.system_of_measure = 'Metric (SI)';

		// get the sievewrappers for metric
 		list<sika_SCCZoneCreateEditMaterialCtrlExt3.sieveWrapper> sWrappers = ext.getSieveWrappers();

		// confirm that the sieve wrappers are in the list, ordered from largest aperture to smallest aperture
		for(i = 1; i <= 15; i++){
			system.assert(sWrappers[i].theApSize < sWrappers[i-1].theApSize);
		}

		// run calculateValues with no values entered. List of sieveWrappers should be returned without any value in theCalculatedValue field
		testWrappers = ext.calculateValues(sWrappers);
		for(i=0; i < testWrappers.size(); i++){
			system.assert(testWrappers[i].theCalculatedValue == null);
		}

		// (Calling getSieveWrappers when we've already built a list of sievewrappers will return the original list, not create a new one. Set sieveWrappers == null to start fresh.)
		ext.sieveWrappers = null;
		sWrappers = ext.getSieveWrappers();
		
		// set the % passing value for the largest sieve to 100, and the smallest sieve to 0.
		// Note that theValue and calculatedValue are stirng fields, since Salesforce treats blank decimal fields as zeroes. Zero and blank are very different for our purposes. (We interpolate/extrapolate the values of blank fields.)
		sWrappers[0].theValue = '100';
		sWrappers[15].theValue = '0';

		// interpolate the values for the remaining 14 sieves
		testWrappers = null;
		testWrappers = ext.calculateValues(sWrappers);

		// sort the list by sieve aperture, and confirm that the calculated values are as expected, within a reasonable tolerance range (+/- .001mm, then rounded to 2 decimal places))
		sika_SCCZoneCreateEditMaterialCtrlExt3.sortBy = 'ap';
		testWrappers.sort();

		map<Integer, Decimal> listIndex_to_expectedValues = new map<Integer, Decimal>{0 => 100,
																		 			  1 => 78.728728728729,
																		 			  2 => 39.314314314314,
																		 			  3 => 31.181181181181,
																		 			  4 => 24.924924924925,
																		 			  5 => 19.91991991992,
																		 			  6 => 17.417417417417,
																		 			  7 => 15.540540540541,
																		 			  8 => 12.412412412412,
																		 			  9 => 6.1561561561562,
																		 			 10 => 3.028028028028,
																		 			 11 => 1.463963963964,
																		 			 12 => 0.68818818818819,
																		 			 13 => 0.29404404404404,
																		 			 14 => 0.10010010010011,
																		 			 15 => 0};
		system.assert(Decimal.valueOf(testWrappers[0].theCalculatedValue) == 100);
		system.assert(Decimal.valueOf(testWrappers[15].theCalculatedValue) == 0);
		
		decimal high;
		decimal low;
		
		for(i=1; i < 15; i++){
			high = (listIndex_to_expectedValues.get(i) + .001).setScale(2);
			low = (listIndex_to_expectedValues.get(i) - .001).setScale(2);
			system.assert(Decimal.valueOf(testWrappers[i].theCalculatedValue) >= low && Decimal.valueOf(testWrappers[i].theCalculatedValue) <= high);
		}

		// set the % passing value for the third-largets sieve to 100, and the value of the third smallest sieve to 0.
		// Values for sieves with larger apertures than the third largest should all be == 100.  (Can't have more than 100% passing!)
		// Values for sieves with smaller apertures than the third smallest should all be == 0.
		ext.sieveWrappers = null;
		sWrappers = ext.getSieveWrappers();
		sWrappers[2].theValue = '100';
		sWrappers[13].theValue = '0';

		testWrappers = null;
		testWrappers = ext.calculateValues(sWrappers);

		testWrappers.sort();
		listIndex_to_expectedValues.clear();
		listIndex_to_expectedValues = new map<Integer, Decimal>{0 => 100,
												 			    1 => 100,
												 			    2 => 100,
												 			    3 => 79.156645823312,
												 			    4 => 63.12329645663,
												 			    5 => 50.296616963284,
												 			    6 => 43.883277216611,
												 			    7 => 39.073272406606,
												 			    8 => 31.056597723264,
												 			    9 => 15.023248356582,
												 			   10 => 7.0065736732403,
												 			   11 => 2.9982363315697,
												 			   12 => 1.010101010101,
												 			   13 => 0,
												 			   14 => 0,
												 			   15 => 0};

		system.assert(Decimal.valueOf(testWrappers[0].theCalculatedValue) == 100);
		system.assert(Decimal.valueOf(testWrappers[1].theCalculatedValue) == 100);
		system.assert(Decimal.valueOf(testWrappers[2].theCalculatedValue) == 100);

		for(i=3; i < 13; i++){
			high = (listIndex_to_expectedValues.get(i) + .001).setScale(2);
			low = (listIndex_to_expectedValues.get(i) - .001).setScale(2);
			system.debug('******** ' + high + ' :: ' + testWrappers[i].theCalculatedValue + ' :: ' + low + ' (' + testWrappers[i].theLabel + ')');
			system.assert(Decimal.valueOf(testWrappers[i].theCalculatedValue) >= low && Decimal.valueOf(testWrappers[i].theCalculatedValue) <= high);
		}

		system.assert(Decimal.valueOf(testWrappers[13].theCalculatedValue) == 0);
		system.assert(Decimal.valueOf(testWrappers[14].theCalculatedValue) == 0);
		system.assert(Decimal.valueOf(testWrappers[15].theCalculatedValue) == 0);

	
		// create a new instance of the controller and its extension
		ctrl = new sika_SCCZoneMaterialsHomePageController();
		ext = new sika_SCCZoneCreateEditMaterialCtrlExt3(ctrl);

		// start test for Standard (i.e., not Metric) calculation
		ext.system_of_measure = 'US Customary';

		// get the US/Customary sievewrappers
	 	sWrappers = ext.getSieveWrappers();

		// When sieve values are entered in US Customary, the calculateValues() method adds all Metric sieves to the list as well.  All calculations in Salesforce and in the optimizer are done in Metric, so...
		// we take the US sized sieves, order them by their metric equivalent (mm) aperature size, then add the metric sieves to the list, sort the list 
		// so that all sieves are now in size order, and use the user-entered values to interpolate/extrapolate the values for ALL of the sieves.  Both sets of values are stored on the Material object.

		// Set the % passing value for the 5th US sized seive (3/8") to 60 and the 12th US sized sieve (#40) to 10.
		// This time, the method will extrapolate the values above the 5th instead of setting them to 100% automatically.
		// Likewise, it will extrapolate the values below the 10th instead of setting them to 0%/
		// When the extrapolated values go beyond 100% or 0%, the values will be capped at 100% or 0% respectively. All remaining larger aperture sieves should be set to 100%; all remaining smaller aperture sieves should be set to 0%. 
		sWrappers[4].theValue = '60';
		sWrappers[11].theValue = '10';

		// interpolate or extrapolate the values for the remaining sieves
		testWrappers = null;
		testWrappers = ext.calculateValues(sWrappers);

		// sort the list by sieve aperture, and confirm that the calculated values are as expected (with .001mm tolerance level)
		sika_SCCZoneCreateEditMaterialCtrlExt3.sortBy = 'ap';
		testWrappers.sort();

		listIndex_to_expectedValues.clear();
		listIndex_to_expectedValues = new map<Integer, Decimal>{0 => 100,				// 80mm
												 			    1 => 100,				// 63mm
												 			    2 => 100,				// 2" (50.8mm)
												 			    3 => 100,				// 1.5" (38.1mm)
												 			    4 => 100,				// 31.5mm
												 			    5 => 100,				// 1" (25.4mm)
												 			    6 => 100,				// 25mm
												 			    7 => 100,				// 20mm
												 			    8 => 100,				// 3/4" (19mm)  // EXTRAPOLATION REACHES 112.2 HERE. WILL CAP THE % AT 100. ALL LARGER SIEVES WILL BE SET TO 100.
 												 			    9 => 95.698569856986,	// 16mm
												 			   10 => 84.697469746975,	// 14mm
												 			   11 => 76.446644664466, 	// 12.5mm
												 			   12 => 62.695269526953,	// 10mm
												 			   13 => 60,	// ******	3/8" (9.51mm)
												 			   14 => 35.192519251925,	// 5mm
												 			   15 => 33.872387238724,	// #4 (4.76mm)
												 			   16 => 21.441144114411,	// 2.5mm
												 			   17 => 20.781078107811,	// #8 (2.38mm)
												 			   18 => 18.690869086909,	// #10 (2mm)
												 			   19 => 14.565456545655,	// 1.25mm
												 			   20 => 14.235423542354,	// #16 (1.19mm)
												 			   21 => 12.315731573157,	// #20 (0.841mm)
												 			   22 => 11.155115511551,	// 0.63mm
												 			   23 => 10.962596259626,	// #30 (0.595mm)
												 			   24 => 10,	// ******	// #40 (0.42mm)
												 			   25 => 9.4224422442244,	// 0.315mm
												 			   26 => 8.6633663366337,	// #80 (0.177mm)
												 			   27 => 8.5698569856986,	// 16mm
												 			   28 => 8.5093509350935,	// #100 (0.149mm)
												 			   29 => 8.3058305830583,	// #150 (0.112mm)
												 			   30 => 8.1298129812981,	// 0.08mm
												 			   31 => 8.1023102310231};	// #200 (0.075mm)

		for(i=0; i < testWrappers.size(); i++){
			high = (listIndex_to_expectedValues.get(i) + .001).setScale(2);
			low = (listIndex_to_expectedValues.get(i) - .001).setScale(2);
			system.debug('******** ' + high + ' :: ' + testWrappers[i].theCalculatedValue + ' :: ' + low + ' (' + testWrappers[i].theLabel + ')');
			//system.assert(Decimal.valueOf(testWrappers[i].theCalculatedValue) >= low && Decimal.valueOf(testWrappers[i].theCalculatedValue) <= high); Commented out 12/14/2016
		}

	}

}