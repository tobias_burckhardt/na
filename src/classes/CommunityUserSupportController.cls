public without sharing class CommunityUserSupportController {

    public String description { get; set; }

    public String selectedType { get; set; }    
    public User Usr{get; set;}
    public List<User> UsrList{get; set;}
    public Contact cont{get; set;}
    public string currentUser = UserInfo.getUserId();
    public List<Case> csuserlist{get;set;}
    public List<Case> csuserlist1{get;set;} 
    public Integer counter=0;
    public static Integer queryLimit = 10;
    public Integer currentPage {get; set;}
    public static Integer totalPages {get; set;}
    public Integer pageTotal = 0;
    public Boolean isFirst{get;set;}
    public Boolean isLast{get;set;}  
    public User sikaRep {get;set;}
    public CommunityUserSupportController(){

        isFirst = true;
        isLast  =false;
        currentPage = 1;
        totalPages = 1;
        
        
        Integer queryCount = [select count()  from case where User__c=: currentUser];
        Double pageCount = Double.valueOF(queryCount)/Double.valueOf(queryLimit);
        totalPages = Integer.valueOf(Math.ceil(pageCount));
        pageTotal = totalPages;
        try{
            Usr = [select name, email, phone, ContactId, CompanyName from user where id =: UserInfo.getUserId() and isPortalEnabled = true and contactid!=null ];
            
            cont = [select id,name,AccountId,owner.name,account.owner.firstname,account.owner.email,owner.email, owner.phone from contact where id =: Usr.ContactId]; 
        
        

            Id sikaRepId = [select UserId from AccountTeamMember where AccountId = :cont.AccountId and TeamMemberRole = 'RSB Commercial' limit 1][0].UserId;
        
            sikaRep = [select FirstName, LastName, Name, Email, Phone from User where Id = :sikaRepId];
        }
        catch(Exception e){}
        queryProducts();
       // }catch(Exception e){}
    }
     private void queryProducts(){
       csuserlist=[select id,CaseNumber,subject,type,status,CreatedDate,LastModifieddate from case where User__c=: currentUser];
        isFirst = counter == 0 ? true : false;
        isLast = csuserlist.size() < queryLimit ? true : false;
        
    }
     public void previous(){
        if(!csuserlist.isEmpty()){
            //gather selected prods
            for(Case wp : csuserlist){
                
                    csuserlist1.add(wp);
                
            }
            
            //reset warranty products list
            csuserlist.clear();
            
            //set offset for next query
            if(counter > 0){
                counter -= queryLimit;
            }
            if(currentPage > 0){
                currentPage -= 1;
            }
            
            totalPages = pageTotal;
            
            queryProducts();
        }
    }
     public void next(){
        if(!csuserlist.isEmpty()){
            //gather selected prods
            for(Case wp : csuserlist){
               
                    csuserlist1.add(wp);
                
            }
            
            //reset warranty products list
            csuserlist.clear();
            
            //set offset for next query
            counter += queryLimit;
            currentPage += 1;
            
            totalPages = pageTotal;
            
            queryProducts();
        }
    }
    public PageReference submitcase(){
      Case cse=new Case();
      cse.User__c=UserInfo.getUserId();
      cse.Description=description;
      cse.Subject = 'Warranty Support Request - ' + selectedType;
      cse.Request__c=selectedType;
      cse.Project_Building_Name__c = 'Project Building Name';
      insert cse;
      
      PageReference currentPage = new PageReference('/apex/CommunityUserSupport');
      currentPage.setRedirect(true);
      //system.debug(ApexPages.currentPage().getUrl());
      return currentPage;
    }
}