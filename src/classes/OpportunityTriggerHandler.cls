public with sharing class OpportunityTriggerHandler {

    //public static String INTERNAL_TEAM_ROLE = 'Account Manager';
    //public static String PARTNER_TEAM_ROLE = 'Dealer Account Manager';
    
    /* 
    Control variable that indicates after update methods already executed to prevent unintentional recursion 
    */
    public static Boolean onAfterUpdate_FirstRun = true;
    public static Boolean onAfterInsert_FirstRun = true;

    /*
    Indicates the number of records to be processed on the thread 
    */
    private Integer batchSize = 0;

    /* 
    Indicates whether the handler is being executed within a trigger context
    */
    public  Boolean isTriggerContext { get; private set; }    

    /*
    Constructor
    @param Boolean isTriggerContextParam - indicates whether the handler is being executed within a trigger context
    @param Integer batchSizeParam - indicates the number of records to be processed on the thread
    */
    public OpportunityTriggerHandler(Boolean isTriggerContextParam, Integer batchSizeParam) {
        isTriggerContext = (isTriggerContextParam == null) ? false : isTriggerContextParam;
        batchSize = batchSizeParam;
    }

    public void onAfterInsert(List<Opportunity> newList) {
        if (onAfterInsert_FirstRun) {
            onAfterUpdate_FirstRun = false;
        }
    }

    public void onAfterUpdate(List<Opportunity> newList, Map<Id, Opportunity> oldMap) {
        if (onAfterUpdate_FirstRun) {
            setOpportunityAccount(newList, oldMap);
            onAfterUpdate_FirstRun = false;
        }
    }

    public void setOpportunityAccount(List<Opportunity> newList, Map<Id, Opportunity> oldMap){
        User currentUser = [SELECT Id, AccountId FROM User WHERE Id = :userInfo.getUserId()];
        List<Opportunity> oppList = new List<Opportunity>();
        for(Opportunity opp : newList){
            if((opp.Roofing_Community_Quote_Request__c != null && oldMap == null) || (opp.Roofing_Community_Quote_Request__c != oldMap.get(opp.Id).Roofing_Community_Quote_Request__c)){
                if(currentUser.AccountId != null){
                    opp.AccountId = currentUser.AccountId;
                    oppList.add(opp);
                }
            }
        }
        
        if(!oppList.isEmpty()){
            update oppList;
        }
    }
}