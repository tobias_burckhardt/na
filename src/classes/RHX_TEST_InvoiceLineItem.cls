@isTest(SeeAllData=true)
public class RHX_TEST_InvoiceLineItem {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM InvoiceLineItem__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new InvoiceLineItem__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}