public with sharing class BuildingGroupTriggerHandler {

    public static void onAfterUpdate(List<Building_Group__c> buildingGroups, Map<Id, Building_Group__c> oldMap) {
        buildingGroups = [SELECT Id, Accepted__c, Intended_Warranty_Product__c, Warranty_Extended_Product__c, Opportunity__c,
                Intended_Warranty_Product__r.Roofing_Zero_Value_Warranty__c, Warranty__c, Windspeed_MPH__c
        FROM Building_Group__c WHERE Id IN :oldMap.keySet()];

        Map<Id, List<Building_Group__c>> buildingGroupsByOppId = new Map<Id, List<Building_Group__c>>();
        for (Building_Group__c buildingGroup : buildingGroups) {
            if (buildingGroup.Accepted__c == true && oldMap.get(buildingGroup.Id).Accepted__c == false && buildingGroup.Warranty__c == null
                    && buildingGroup.Intended_Warranty_Product__r.Roofing_Zero_Value_Warranty__c == true) {
                if (!buildingGroupsByOppId.containsKey(buildingGroup.Opportunity__c)) {
                    buildingGroupsByOppId.put(buildingGroup.Opportunity__c, new List<Building_Group__c>());
                }
                buildingGroupsByOppId.get(buildingGroup.Opportunity__c).add(buildingGroup);
            }
        }
        WarrantyService.issueWarranties(buildingGroupsByOppId);
    }
}