/**
 * Wrapper class for a SearchResult
 *
 * @author ben.burwell@trifecta.com
 */
public class RepFinderSearchResult {
	@AuraEnabled public String repName;
	@AuraEnabled public String region;
	@AuraEnabled public String repTitle;
	@AuraEnabled public String targetMarket;
	@AuraEnabled public String state;
	@AuraEnabled public Id repUserId;
	@AuraEnabled public String repComments;
	@AuraEnabled public String phone;
	@AuraEnabled public String phoneLink;
	@AuraEnabled public String email;
	@AuraEnabled public String regionImage;
	@AuraEnabled public String regionalManagerName;
	@AuraEnabled public String regionalManagerTitle;
	@AuraEnabled public Id regionalManagerUserId;
	@AuraEnabled public String regionalOfficeStreet;
	@AuraEnabled public String regionalOfficeCity;
	@AuraEnabled public String regionalOfficeState;
	@AuraEnabled public String regionalOfficeZip;
	@AuraEnabled public String regionalOfficeIsoCountry;
	@AuraEnabled public String states;
	@AuraEnabled public Boolean isVertical;
	@AuraEnabled public Boolean isGeographic;
	@AuraEnabled public String vertical;

	public static final List<String> REP_FIELDS = new List<String>{
			'Employee__r.Mobile_Phone_Full__c',
			'Employee__r.Job_Title__c',
			'Employee__r.SFDC_User_Account__c',
			'Employee__r.Name',
			'Employee__r.Sika_Email__c',
			'Notes__c',
			'Rep_State__c',
			'Region__r.ISO_Country__c',
			'Region__r.Local_Name__c',
			'Region__r.RecordType.DeveloperName',
			'Region__r.Region_Image_URL__c',
			'Region__r.Region_Office_City__c',
			'Region__r.Region_Office_State__c',
			'Region__r.Region_Office_Street__c',
			'Region__r.Region_Office_Zip__c',
			'Region__r.Region_State__c',
			'Region__r.Regional_Manager_EMPL__r.Job_Title__c',
			'Region__r.Regional_Manager_EMPL__r.SFDC_User_Account__c',
			'Region__r.Regional_Manager_EMPL__r.Name',
			'Region__r.Target_Market__c',
			'Region__r.Vertical__c'
	};

	public RepFinderSearchResult(FindARep__c rep) {
		this.repName = rep.Employee__r.Name;
		this.region = rep.Region__r.Local_Name__c;
		this.repTitle = rep.Employee__r.Job_Title__c;
		this.targetMarket = rep.Region__r.Target_Market__c;
		this.repUserId = rep.Employee__r.SFDC_User_Account__c;
		this.repComments = rep.Notes__c;
		this.phone = String.isNotBlank(rep.Employee__r.Mobile_Phone_Full__c) ? rep.Employee__r.Mobile_Phone_Full__c : 'Unavailable';
		this.phoneLink = RepFinderUtils.getCanonicalPhone(this.phone);
		this.email = rep.Employee__r.Sika_Email__c;
		this.regionImage = rep.Region__r.Region_Image_URL__c;
		this.regionalManagerName = rep.Region__r.Regional_Manager_EMPL__r.Name;
		this.regionalManagerTitle = rep.Region__r.Regional_Manager_EMPL__r.Job_Title__c;
		this.regionalManagerUserId = rep.Region__r.Regional_Manager_EMPL__r.SFDC_User_Account__c;
		this.regionalOfficeStreet = rep.Region__r.Region_Office_Street__c;
		this.regionalOfficeCity = rep.Region__r.Region_Office_City__c;
		this.regionalOfficeState = rep.Region__r.Region_Office_State__c;
		this.regionalOfficeZip = rep.Region__r.Region_Office_Zip__c;
		this.regionalOfficeIsoCountry = rep.Region__r.ISO_Country__c;
		this.states = rep.Rep_State__c != null ? rep.Rep_State__c.replaceAll(';', ', ') : '';
		this.vertical = rep.Region__r.Vertical__c;
		this.isGeographic = rep.Region__r.RecordType.DeveloperName == RepFinderUtils.RECORDTYPE_REGION_GEOGRAPHIC;
		this.isVertical = rep.Region__r.RecordType.DeveloperName == RepFinderUtils.RECORDTYPE_REGION_VERTICAL;
	}
}