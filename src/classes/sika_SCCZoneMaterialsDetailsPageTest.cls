@isTest
private class sika_SCCZoneMaterialsDetailsPageTest {  // actually testing detail page and new/edit page. Same class covers both.
	private static User SCCZoneUser;
	private static Account theAccount;
	private static Material__c theMaterial;
	private static PageReference pageRef;
	private static sika_SCCZoneMaterialsHomePageController ctrl;
	
	private static testMethod void init() {
		// bypass the triggers we won't need to have firing during setup
		sika_SCCZoneMixTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMixTriggerHelper.bypassMixOwnerTransferCheck = true;
		sika_SCCZoneMixTriggerHelper.bypassMixChatterAlert = true;
		sika_SCCZoneMixTriggerHelper.bypassUpdateSCMQuantity = true;
	
		sika_SCCZoneMaterialTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate = true;
		sika_SCCZoneMaterialTriggerHelper.bypassPreventDelete = true;
		sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit = true;
		
		sika_SCCZoneMixMaterialTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneMixMaterialTriggerHelper.bypassMixOptimizationCheck = true;
		sika_SCCZoneMixMaterialTriggerHelper.bypassSetSCMWeightAndUnit = true;
		sika_SCCZoneMixMaterialTriggerHelper.bypassConcreteCurveCalc = true;
		
		sika_SCCZoneTestResultTriggerHelper.bypassAccountTeamCheck = true;
		sika_SCCZoneTestResultTriggerHelper.bypassLockOrUnlockMixAndMaterials = true;
		
		// create an SCCZone user (with contact, account, etc...)
		map<String, sObject> objects = sika_SCCZoneTestFactory.createSCCZoneUserContactAndAccount();
        SCCZoneUser = (User) objects.get('SCCZoneUser');
        theAccount = (Account) objects.get('theAccount');
		
	
		// create a material for referencing later. (May want to create specifc materials in some tests, but here's one we can use generally.)
		system.runAs(SCCZoneUser){
			theMaterial = sika_SCCZoneTestFactory.createMaterial();
			insert(theMaterial);
		}
		
	}


	@isTest static void testLoadNewPage(){
		init();
		pageRef = new PageReference('/sika_SCCZoneCreateEditMaterialPage');
        pageRef.getParameters().put('unit', 'Metric (SI)');
        pageRef.getParameters().put('type', 'Sand');
        Test.setCurrentPage(pageRef);
        
        test.startTest();

		System.runAs(SCCZoneUser){
			// instantiate the page controller
		    ctrl = new sika_SCCZoneMaterialsHomePageController();

			// affirm that the page loaded. (Didn't redirect to an error page)
			system.assert(ApexPages.CurrentPage().getURL().contains('/sika_SCCZoneCreateEditMaterialPage'));
			
		}
       
	}
	
	
	@isTest static void testLoadEditPage(){
		init();
		pageRef = new PageReference('/sika_SCCZoneCreateEditMaterialPage');
		pageRef.getParameters().put('Id', theMaterial.Id);
        Test.setCurrentPage(pageRef);
        
        test.startTest();

		System.runAs(SCCZoneUser){
			// instantiate the page controller
		    ctrl = new sika_SCCZoneMaterialsHomePageController();

			// affirm that the page loaded. (Didn't redirect to an error page)
			system.assert(ApexPages.CurrentPage().getURL().contains('/sika_SCCZoneCreateEditMaterialPage'));
			
			// affirm that the material the page is showing is what we expected
			system.assertEquals(ctrl.theMaterial.Id, theMaterial.Id);
			
			theMaterial.Unit_System__c = 'US Customary';
			update(theMaterial);
			
			pageRef = new PageReference('/sika_SCCZoneCreateEditMaterialPage');
			pageRef.getParameters().put('Id', theMaterial.Id);
        	Test.setCurrentPage(pageRef);
			ctrl = new sika_SCCZoneMaterialsHomePageController();
			
			system.assert(ApexPages.CurrentPage().getURL().contains('/sika_SCCZoneCreateEditMaterialPage'));

		}
		test.stopTest();
	}
	
	
	@isTest static void testLoadDetailPage(){
		init();
		pageRef = new PageReference('/sika_SCCZoneMaterialDetailPage');
		pageRef.getParameters().put('Id', theMaterial.Id);
        Test.setCurrentPage(pageRef);

		test.startTest();

		System.runAs(SCCZoneUser){
			// instantiate the page controller
		    ctrl = new sika_SCCZoneMaterialsHomePageController();

			// affirm that the page loaded. (Didn't redirect to an error page)
			system.assert(ApexPages.CurrentPage().getURL().contains('/sika_SCCZoneMaterialDetailPage'));
			
			// affirm that the material the page is showing is what we expected
			system.assertEquals(ctrl.theMaterial.Id, theMaterial.Id);
			
		}
		test.stopTest();
	}
	
	
	@isTest static void testLoadDetailPageWhereTheMaterialHasBeenUsedInMixes(){
		init();
		System.runAs(SCCZoneUser){
			
		// create some mixes
		list<Mix__c> theMixes = new list<Mix__c>();
		Mix__c Mix1 = sika_SCCZoneTestFactory.createMix();
		Mix__c Mix2 = sika_SCCZoneTestFactory.createMix();
		theMixes.add(Mix1);
		theMixes.add(Mix2);
		insert(theMixes);
		
		// add this material to the mixes
		list<Mix_Material__c> theMMs = new list<Mix_Material__c>();
		theMMs.add(sika_SCCZoneTestFactory.createMixMaterial(Mix1, theMaterial));
		theMMs.add(sika_SCCZoneTestFactory.createMixMaterial(Mix1, theMaterial));
		insert(theMMs);
		
		pageRef = new PageReference('/sika_SCCZoneMaterialDetailPage');
		pageRef.getParameters().put('Id', theMaterial.Id);
        Test.setCurrentPage(pageRef);

		test.startTest();
		
			// instantiate the page controller
		    ctrl = new sika_SCCZoneMaterialsHomePageController();

			// affirm that the page loaded. (Didn't redirect to an error page)
			system.assert(ApexPages.CurrentPage().getURL().contains('/sika_SCCZoneMaterialDetailPage'));
			
			// affirm that the material the page is showing is what we expected
			system.assertEquals(ctrl.theMaterial.Id, theMaterial.Id);
			
			// affirm that the two Mixes are listed in the "mixes where this material is used" list
			list<Mix_Material__c> testMMList = ctrl.getMixMaterials();
			system.assertEquals(testMMList.size(), 2);
			
		}
		test.stopTest();
	}
	
	
	@isTest static void testLoadDetailPageWithAdmixtureMaterial(){
		init();
		
		System.runAs(SCCZoneUser){
			Material__c admixture = sika_SCCZoneTestFactory.createMaterial(new map<String, String>{'type' => 'Admixture'});
			admixture.Cost__c = null;
			admixture.Unit__c = null;
			insert(admixture);
			
			pageRef = new PageReference('/sika_SCCZoneMaterialDetailPage');
			pageRef.getParameters().put('Id', admixture.Id);
	        Test.setCurrentPage(pageRef);
			
			test.startTest();
		
			// instantiate the page controller
		    ctrl = new sika_SCCZoneMaterialsHomePageController();

			// affirm that the page loaded. (Didn't redirect to an error page)
			system.assert(ApexPages.CurrentPage().getURL().contains('/sika_SCCZoneMaterialDetailPage'));
			
			// affirm that the material the page is showing is what we expected
			system.assertEquals(ctrl.theMaterial.Id, admixture.Id);
			
		}
		test.stopTest();
		
	}
	
	
	@isTest static void testEditMaterial(){
		init();
		pageRef = new PageReference('/sika_SCCZoneMaterialDetailPage');
		pageRef.getParameters().put('Id', theMaterial.Id);
        Test.setCurrentPage(pageRef);
        
        System.runAs(SCCZoneUser){
        	// instantiate the page controller
		    ctrl = new sika_SCCZoneMaterialsHomePageController();
		    
		    system.assert(ctrl.editMaterial().getURL().contains('/sika_SCCZoneCreateEditMaterialPage'));
		    system.assertEquals(ctrl.editMaterial().getParameters().get('id'), theMaterial.Id);
		    
		    // make sure the link points to the AdmixturePricePage if the material is an...admixture.
		    pageRef = new PageReference('/sika_SCCZoneMaterialDetailPage');
			pageRef.getParameters().put('Id', theMaterial.Id);
	        Test.setCurrentPage(pageRef);
		    
		    ctrl = new sika_SCCZoneMaterialsHomePageController();
		    ctrl.theMaterial.Type__c = 'Admixture';
		    
		    system.assert(ctrl.editMaterial().getURL().contains('/sika_SCCZoneAdmixturePricePage'));
		    system.assertEquals(ctrl.editMaterial().getParameters().get('id'), theMaterial.Id);
        
        }
		
	}
	
	
	@isTest static void testEditAdmixture(){
		init();
		pageRef = new PageReference('/sika_SCCZoneMaterialDetailPage');
		pageRef.getParameters().put('Id', theMaterial.Id);
        Test.setCurrentPage(pageRef);
        
        System.runAs(SCCZoneUser){
        	// instantiate the page controller
		    ctrl = new sika_SCCZoneMaterialsHomePageController();
		    
		    system.assert(ctrl.editAdmixture().getURL().contains('/sika_SCCZoneAdmixturePricePage'));
        }
		
	}
	
	
	@isTest static void testCancelNewMaterial(){
		init();
		pageRef = new PageReference('/sika_SCCZoneMaterialDetailPage');
		pageRef.getParameters().put('Id', theMaterial.Id);
        Test.setCurrentPage(pageRef);
        
        System.runAs(SCCZoneUser){
        	// instantiate the page controller
		    ctrl = new sika_SCCZoneMaterialsHomePageController();
		    
		    system.assert(ctrl.cancelNewMaterial().getURL().contains('/sika_SCCZoneMaterialDetailPage'));
		    system.assertEquals(ctrl.cancelNewMaterial().getParameters().get('id'), theMaterial.Id);

        }
        
	}
	
	
	@isTest static void testDeleteMaterial(){
		init();
		test.startTest();
		
		System.runAs(SCCZoneUser){
			// instantiate the page controller
		    ctrl = new sika_SCCZoneMaterialsHomePageController();

			ctrl.deleteThisMaterial = theMaterial.Id;
			system.assert(ctrl.deleteMaterial().getURL().contains('sika_SCCZoneMaterialsHomePage'));
			
		}
		test.stopTest();
		
		Material__c tempMaterial = [SELECT Id, isActive__c FROM Material__c WHERE Id = :theMaterial.Id];
		system.assertEquals(tempMaterial.isActive__c, false);
		
	}


	@isTest static void testDeleteMaterialDetail(){
		init();
		pageRef = new PageReference('/sika_SCCZoneMaterialDetailPage');
		pageRef.getParameters().put('Id', theMaterial.Id);
        Test.setCurrentPage(pageRef);

		test.startTest();

		System.runAs(SCCZoneUser){
			// instantiate the page controller
		    ctrl = new sika_SCCZoneMaterialsHomePageController();
		    
		    system.assert(ctrl.deleteMaterialDetail().getURL().contains('/sika_SCCZoneMaterialsHomePage'));
		    
		}
		test.stopTest();
		
		Material__c tempMaterial = [SELECT Id, isActive__c FROM Material__c WHERE Id = :theMaterial.Id];
		system.assertEquals(tempMaterial.isActive__c, false);
		
	}
	

	@isTest static void testUnDeleteMaterial(){
		init();
		theMaterial.isActive__c = false;
		System.runAs(SCCZoneUser){
			update(theMaterial);
			test.startTest();
			
			pageRef = new PageReference('/sika_SCCZoneMaterialDetailPage');
			pageRef.getParameters().put('Id', theMaterial.Id);
	        Test.setCurrentPage(pageRef);
	        
	        // instantiate the page controller
		    ctrl = new sika_SCCZoneMaterialsHomePageController();
		    
		    system.assert(ctrl.unDeleteMaterial().getURL().contains('/sika_SCCZoneMaterialDetailPage'));
		    
			test.stopTest();
			
			Material__c tempMaterial = [SELECT Id, isActive__c FROM Material__c WHERE Id = :theMaterial.Id];
			system.assertEquals(tempMaterial.isActive__c, true);
			
		}
	}

}