public with sharing class DocumentType {
	public boolean isSelected {get;set;}
	public Id documentId {get;set;}
	public String documentName {get;set;}

	public DocumentType(Id dId, String dName){
		this.IsSelected 	= false;
		this.documentId 	= dId;	
		this.documentName 	= dName;
	}
}