public with sharing class RoofingOpportunityTechRequestToEBWExt {

    public Opportunity opp;

    public PageReference flowFinishLocation {
        get {
            Set<Id> communityProfileIds = new Set<Id>(Pluck.ids(UserDao.getUserPartnerProfileIds()));
            String url = '/apex/EBW_InitialRequest?scontrolCaching=1&id=' + opp.Id;
            if (communityProfileIds.contains(UserInfo.getProfileId())) {
                url = '/PTS' + url;
            }
            return new PageReference(url);
        }
    }

    public RoofingOpportunityTechRequestToEBWExt(ApexPages.StandardController std) {
        this.opp = (Opportunity) std.getRecord();
    }
}