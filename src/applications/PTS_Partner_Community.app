<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>PTS Partner Community</label>
    <tabs>standard-Opportunity</tabs>
    <tabs>standard-Chatter</tabs>
    <tabs>Applicator_Opportunities</tabs>
    <tabs>Roofing_Community_Quote_Request__c</tabs>
    <tabs>ConstructionPts__CP_Action__c</tabs>
</CustomApplication>
