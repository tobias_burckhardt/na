<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>BMC Remedyforce from BMC</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Sika Helpdesk</label>
    <logo>Logos/Sika_Helpdesk_Logo.png</logo>
    <tabs>standard-Chatter</tabs>
    <tabs>BMCServiceDesk__Remedyforce_Self_Service</tabs>
    <tabs>BMCServiceDesk__RemedyforceConsole</tabs>
    <tabs>BMCServiceDesk__KM_KnowledgeArticle__c</tabs>
    <tabs>BMCServiceDesk__Remedyforce_Administration</tabs>
    <tabs>Customer_Survey__c</tabs>
    <tabs>ConstructionPts__CP_Action__c</tabs>
</CustomApplication>
