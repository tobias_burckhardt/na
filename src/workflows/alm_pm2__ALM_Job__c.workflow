<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>alm_pm2__SCAN_Login_Error_Notification</fullName>
        <ccEmails>productdev@bluewolfgroup.com</ccEmails>
        <description>SCAN Login Error Notification</description>
        <protected>false</protected>
        <recipients>
            <field>alm_pm2__Mail_To__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>alm_pm2__Bluewolf_ALM/alm_pm2__Login_Error_Email_Notification</template>
    </alerts>
    <rules>
        <fullName>alm_pm2__Login Error Email Notification</fullName>
        <actions>
            <name>alm_pm2__SCAN_Login_Error_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>alm_pm2__ALM_Job__c.alm_pm2__Login_Error__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notify a user when a login error occurs during a SCAN run</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
