<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>alm_qa__Copy_Apex_Tests</fullName>
        <field>alm_qa__Apex_Tests__c</field>
        <formula>IF(alm_qa__Apex_Tests_Rollup__c = 0, PRIORVALUE(alm_qa__Apex_Tests_Rollup__c) , alm_qa__Apex_Tests_Rollup__c)</formula>
        <name>Copy Apex Tests</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>alm_qa__Copy_Automated_Tests</fullName>
        <field>alm_qa__Automated_Tests__c</field>
        <formula>IF(alm_qa__Automated_Tests_Rollup__c = 0, PRIORVALUE(alm_qa__Automated_Tests_Rollup__c) , alm_qa__Automated_Tests_Rollup__c)</formula>
        <name>Copy Automated Tests</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>alm_qa__Copy_Failed_Apext_Tests</fullName>
        <field>alm_qa__Failed_Apex_Tests__c</field>
        <formula>IF(alm_qa__Failed_Apex_Tests_Rollup__c = 0, PRIORVALUE(alm_qa__Failed_Apex_Tests_Rollup__c) , alm_qa__Failed_Apex_Tests_Rollup__c)</formula>
        <name>Copy Failed Apext Tests</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>alm_qa__Copy_Failed_Automated_Tests</fullName>
        <field>alm_qa__Failed_Automated_Tests__c</field>
        <formula>IF(alm_qa__Failed_Automated_Tests_Rollup__c = 0, PRIORVALUE(alm_qa__Failed_Automated_Tests_Rollup__c) , alm_qa__Failed_Automated_Tests_Rollup__c)</formula>
        <name>Copy Failed Automated Tests</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>alm_qa__Copy_Failed_Manual_Tests</fullName>
        <field>alm_qa__Failed_Manual_Tests__c</field>
        <formula>IF(alm_qa__Failed_Manual_Tests_Rollup__c = 0, PRIORVALUE(alm_qa__Failed_Manual_Tests_Rollup__c) , alm_qa__Failed_Manual_Tests_Rollup__c)</formula>
        <name>Copy Failed Manual Tests</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>alm_qa__Copy_Manual_Tests</fullName>
        <field>alm_qa__Manual_Tests__c</field>
        <formula>IF(alm_qa__Manual_Tests_Rollup__c = 0, PRIORVALUE(alm_qa__Manual_Tests_Rollup__c) , alm_qa__Manual_Tests_Rollup__c)</formula>
        <name>Copy Manual Tests</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>alm_qa__Copy_Total_Apex_Unit_Tests</fullName>
        <field>alm_qa__Total_Apex_Unit_Tests__c</field>
        <formula>IF(alm_qa__Total_Apex_Unit_Tests_Rollup__c = 0, PRIORVALUE(alm_qa__Total_Apex_Unit_Tests_Rollup__c) , alm_qa__Total_Apex_Unit_Tests_Rollup__c)</formula>
        <name>Copy Total Apex Unit Tests</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>alm_qa__Copy_Total_Failed_Apex_Unit_Tests</fullName>
        <field>alm_qa__Total_Failed_Apex_Unit_Tests__c</field>
        <formula>IF(alm_qa__Total_Failed_Apex_Unit_Tests_Rollup__c = 0, PRIORVALUE(alm_qa__Total_Failed_Apex_Unit_Tests_Rollup__c) , alm_qa__Total_Failed_Apex_Unit_Tests_Rollup__c)</formula>
        <name>Copy Total Failed Apex Unit Tests</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>alm_qa__Copy_Total_Failed_Tests</fullName>
        <field>alm_qa__Total_Failed_Tests__c</field>
        <formula>IF(alm_qa__Total_Failed_Tests_Formula__c = 0, PRIORVALUE(alm_qa__Total_Failed_Tests_Formula__c) , alm_qa__Total_Failed_Tests_Formula__c)</formula>
        <name>Copy Total Failed Tests</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>alm_qa__Copy_Total_Tests</fullName>
        <field>alm_qa__Total_Tests__c</field>
        <formula>IF(alm_qa__Total_Tests_Formula__c = 0, PRIORVALUE(alm_qa__Total_Tests_Formula__c) , alm_qa__Total_Tests_Formula__c)</formula>
        <name>Copy Total Tests</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>alm_qa__BUTR Failure</fullName>
        <active>false</active>
        <description>Send an email alert one hour after a BUTR test fails.</description>
        <formula>AND(alm_qa__Completed__c = true, alm_qa__Result__c = &quot;Fail&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>alm_qa__Copy Roll Up Values</fullName>
        <actions>
            <name>alm_qa__Copy_Apex_Tests</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>alm_qa__Copy_Automated_Tests</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>alm_qa__Copy_Failed_Apext_Tests</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>alm_qa__Copy_Failed_Automated_Tests</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>alm_qa__Copy_Failed_Manual_Tests</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>alm_qa__Copy_Manual_Tests</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>alm_qa__Copy_Total_Apex_Unit_Tests</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>alm_qa__Copy_Total_Failed_Apex_Unit_Tests</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>alm_qa__Copy_Total_Failed_Tests</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>alm_qa__Copy_Total_Tests</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies roll up field values to mirror fields to keep historical values when test results get deleted</description>
        <formula>alm_qa__Lock_Summary_Values__c = false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
