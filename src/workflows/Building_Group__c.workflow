<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Average_Fastener_Pullout_Values</fullName>
        <field>Average_Fastener_Pullout__c</field>
        <formula>TEXT(Sum_Fastener_Pullout_Values__c/Fastener_Pullout_Values_Count__c)</formula>
        <name>Average - Fastener Pullout Values</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_EcoSmart_Box_on_Opportunity</fullName>
        <description>Checks the box that indicates if Building Group contains EcoSmart</description>
        <field>EcoSmart_System__c</field>
        <literalValue>1</literalValue>
        <name>Check EcoSmart Box on Opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Opportunity__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Dow_Reuse_Warranty_1</fullName>
        <field>Dow_Reuse_Warranty_1__c</field>
        <formula>1</formula>
        <name>Dow Reuse Warranty 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Dow_Reuse_Warranty_Needed_True</fullName>
        <field>Dow_Reuse_Warranty_Needed__c</field>
        <literalValue>1</literalValue>
        <name>Dow Reuse Warranty Needed True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_EcoSmart_Field</fullName>
        <field>EcoSmart_System__c</field>
        <literalValue>1</literalValue>
        <name>Update Opportunity EcoSmart Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Opportunity__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Non_EcoSmart_Field</fullName>
        <field>EcoSmart_System__c</field>
        <literalValue>0</literalValue>
        <name>Update Opportunity Non-EcoSmart Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Opportunity__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Average - Fastener Pullout Values</fullName>
        <actions>
            <name>Average_Fastener_Pullout_Values</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( 
ISCHANGED( Values_lbs_In_Fastener_Pullout__c ), 
ISCHANGED( Values_2_lbs_In_Fastener_Pullout__c ), 
ISCHANGED( Values_3_lbs_In_Fastener_Pullout__c ), 
ISCHANGED( Values_4_lbs_In_Fastener_Pullout__c ), 
ISCHANGED( Values_5_lbs_In_Fastener_Pullout__c ) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Dow Reuse Warranty Needed</fullName>
        <actions>
            <name>Dow_Reuse_Warranty_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Dow_Reuse_Warranty_Needed_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Building_Group__c.System_formula__c</field>
            <operation>equals</operation>
            <value>Ecosmart</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Is system Ecosmart</fullName>
        <actions>
            <name>Update_Opportunity_EcoSmart_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Building_Group__c.System_formula__c</field>
            <operation>contains</operation>
            <value>Ecosmart</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Non-EcoSmart Field</fullName>
        <actions>
            <name>Update_Opportunity_Non_EcoSmart_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Building_Group__c.System_formula__c</field>
            <operation>notContain</operation>
            <value>Ecosmart</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
