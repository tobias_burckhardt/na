<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approved_Notifcation</fullName>
        <description>Approved Notifcation</description>
        <protected>false</protected>
        <recipients>
            <field>Employee_Name__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PTO_Email_Templates/PTO_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>PTO_Request_15_Days</fullName>
        <description>PTO Request 15 Days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTO_Email_Templates/PTO_Request_15_Days</template>
    </alerts>
    <alerts>
        <fullName>Rejected_Notification</fullName>
        <description>Rejected Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Employee_Name__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PTO_Email_Templates/PTO_Request_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Field_Update_Submitted</fullName>
        <description>Update Request to Submitted</description>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Field Update Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Draft</fullName>
        <field>Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Status to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
