<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Order_Number_Code_Update</fullName>
        <description>Pull the Order Header# from the Order Number Key (remove line item)</description>
        <field>SAP_Order_Code__c</field>
        <formula>left(Order_Number_Key__c, 8)</formula>
        <name>Order Number Code Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SAP Open Order Code Update</fullName>
        <actions>
            <name>Order_Number_Code_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(ISNEW() &amp;&amp; NOT(ISBLANK(Order_Number_Key__c))) || ISCHANGED(Order_Number_Key__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
