<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approved_Notification</fullName>
        <description>Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Vaykay__Vaykay_Templates/Vaykay__Vacation_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>Approved_Notification_VayKay</fullName>
        <description>Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Vaykay__Vaykay_Templates/Vaykay__Vacation_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>Time_off_Request_15_Days</fullName>
        <description>Time off Request - 15 Days +</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/X15_Day_Event_Notification</template>
    </alerts>
    <alerts>
        <fullName>Vaykay__Approved_Notification</fullName>
        <description>Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Vaykay__Vaykay_Templates/Vaykay__Vacation_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>Vaykay__Rejected_Notification</fullName>
        <description>Rejected Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Vaykay__Vaykay_Templates/Vaykay__Vacation_Request_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Field_Update_Submitted</fullName>
        <field>Vaykay__Status__c</field>
        <literalValue>Not Approved</literalValue>
        <name>Field Update Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_Approved</fullName>
        <description>Change Request Status to Approved</description>
        <field>Vaykay__Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Status Update: Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Vaykay__Status_Update_Approved</fullName>
        <field>Vaykay__Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Status Update: Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Vaykay__Status_Update_Not_Approved</fullName>
        <field>Vaykay__Status__c</field>
        <literalValue>Not Approved</literalValue>
        <name>Status Update: Not Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
