<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Social_Media_Auto_Invite</fullName>
        <description>Social Media Auto Invite</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Social_Media_Auto_Invite</template>
    </alerts>
    <outboundMessages>
        <fullName>Social_Media_Auto_Invite</fullName>
        <apiVersion>27.0</apiVersion>
        <endpointUrl>http://usa.sarnafil.sika.com/</endpointUrl>
        <fields>Email__c</fields>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>sarnafil.admin@us.sika.com</integrationUser>
        <name>Social Media Auto Invite</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Roofing Owner Survey Social Media Invite</fullName>
        <actions>
            <name>Social_Media_Auto_Invite</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Roofing_Owner_Survey__c.Q3_Answer__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Roofing_Owner_Survey__c.Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
