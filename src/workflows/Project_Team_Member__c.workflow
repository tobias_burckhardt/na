<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Project_Team_Member_Notification</fullName>
        <description>New Project Team Member Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Name__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Template/Project_Team_Notice</template>
    </alerts>
    <rules>
        <fullName>New Team Notification</fullName>
        <actions>
            <name>New_Project_Team_Member_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>CreatedBy.Id &lt;&gt; Name__r.Id</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
