<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Received_Incident_History</fullName>
        <description>Email Received - Incident History</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/Email_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Owner_on_Client_Note_Added</fullName>
        <description>Notify Owner on Client Note Added</description>
        <protected>false</protected>
        <recipients>
            <field>BMCServiceDesk__FKUser__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BMCServiceDesk__SDE_Emails/Notify_Owner_on_Note_Added</template>
    </alerts>
    <rules>
        <fullName>Email Conversations</fullName>
        <actions>
            <name>Email_Received_Incident_History</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BMCServiceDesk__IncidentHistory__c.BMCServiceDesk__actionId__c</field>
            <operation>equals</operation>
            <value>Email Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.OwnerId</field>
            <operation>contains</operation>
            <value>NA SAP,US Systems</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__IncidentHistory__c.BMCServiceDesk__EmailConversationData__c</field>
            <operation>contains</operation>
            <value>helpdesk@</value>
        </criteriaItems>
        <description>This rule is to allow agents to receive notifications when a user responds back to a ticket, but only after a ticket has been created.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Created History</fullName>
        <actions>
            <name>Email_Received_Incident_History</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BMCServiceDesk__IncidentHistory__c.BMCServiceDesk__actionId__c</field>
            <operation>equals</operation>
            <value>Email Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.OwnerId</field>
            <operation>notContain</operation>
            <value>US Service desk,CA Service Desk,Incident Queue,NA SAP,US Systems</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify Owner on Note Added</fullName>
        <actions>
            <name>Notify_Owner_on_Client_Note_Added</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BMCServiceDesk__IncidentHistory__c.BMCServiceDesk__description__c</field>
            <operation>equals</operation>
            <value>Client Note</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
