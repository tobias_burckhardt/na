<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Last_Status_Update</fullName>
        <description>Update Date of Last Status Update</description>
        <field>Last_Status_Update__c</field>
        <formula>today()</formula>
        <name>Last Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>User_Status_History_Update</fullName>
        <description>Update Enhancement Status History</description>
        <field>Status_History__c</field>
        <formula>TEXT(TODAY())&amp;&quot; - &quot;&amp;$User.FirstName&amp;&quot; &quot;&amp;$User.LastName&amp;&quot;:&quot;&amp; BR()&amp;
Current_Status__c&amp;BR()&amp;
BR()&amp;
 Status_History__c</formula>
        <name>User Status History Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Enhancement Status Update</fullName>
        <actions>
            <name>Last_Status_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>User_Status_History_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Actions taken when Enhancement Status is updated</description>
        <formula>isnew() ||
ISCHANGED ( Current_Status__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
