<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Idea_Alert</fullName>
        <description>Idea Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>estes.mike@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Template/Idea_Alert</template>
    </alerts>
    <rules>
        <fullName>New Idea Notice</fullName>
        <actions>
            <name>Idea_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email Admin with New Idea is Created</description>
        <formula>ischanged(NumComments)||
ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
