<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Last_Email_Received</fullName>
        <field>Last_Email_Received__c</field>
        <formula>NOW()</formula>
        <name>Update Last Email Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Email_Text</fullName>
        <description>update the case field with the text of the email</description>
        <field>Last_Email_Text__c</field>
        <formula>TextBody</formula>
        <name>Update Last Email Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Case Email Received Field Update</fullName>
        <actions>
            <name>Update_Last_Email_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Last_Email_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Update the last email received field when incoming email arrives</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
