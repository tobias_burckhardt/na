<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BMCServiceDesk__Notify_Incident_Owner_when_final_Task_Linked_to_Incident_is_Closed</fullName>
        <description>Notify incident owner when final task linked to incident is closed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/BMCServiceDesk__All_Tasks_closed_for_Incident</template>
    </alerts>
    <alerts>
        <fullName>BMCServiceDesk__Notify_client_when_service_request_status_changes</fullName>
        <ccEmails>rfdialog@us.sika.com</ccEmails>
        <description>Notify_client_when_service_request_status_changes</description>
        <protected>false</protected>
        <recipients>
            <field>BMCServiceDesk__FKContact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>BMCServiceDesk__FKClient__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/BMCServiceDesk__SRM_Status_Change</template>
    </alerts>
    <alerts>
        <fullName>BMCServiceDesk__Notify_owner_when_linked_task_is_closed</fullName>
        <description>Notify incident owner when linked task is closed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BMCServiceDesk__SDE_Emails/BMCServiceDesk__Linked_Task_for_an_incident_is_closed</template>
    </alerts>
    <alerts>
        <fullName>BMCServiceDesk__call_status_from_email</fullName>
        <description>call_status_from_email</description>
        <protected>false</protected>
        <recipients>
            <field>BMCServiceDesk__FKContact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>BMCServiceDesk__FKClient__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/BMCServiceDesk__Incident_Email_Template_5_Version_2</template>
    </alerts>
    <alerts>
        <fullName>BMCServiceDesk__notify_assign_to_on_ticket_followup</fullName>
        <description>notify_assign_to_on_ticket_followup</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/BMCServiceDesk__Incident_Email_Template_2_Version_2</template>
    </alerts>
    <alerts>
        <fullName>BMCServiceDesk__notify_assign_to_on_ticket_reopen</fullName>
        <description>notify_assign_to_on_ticket_reopen</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/BMCServiceDesk__Incident_Email_Template_8_Version_2</template>
    </alerts>
    <alerts>
        <fullName>BMCServiceDesk__notify_client_on_service_request_reopen</fullName>
        <ccEmails>rfdialog@us.sika.com</ccEmails>
        <description>notify_client_on_service_request_reopen</description>
        <protected>false</protected>
        <recipients>
            <field>BMCServiceDesk__FKContact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>BMCServiceDesk__FKClient__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/BMCServiceDesk__SRM_Reopened</template>
    </alerts>
    <alerts>
        <fullName>BMCServiceDesk__notify_client_on_ticket_reopen</fullName>
        <description>notify_client_on_ticket_reopen</description>
        <protected>false</protected>
        <recipients>
            <field>BMCServiceDesk__FKContact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>BMCServiceDesk__FKClient__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/BMCServiceDesk__Incident_Email_Template_8_Version_2</template>
    </alerts>
    <alerts>
        <fullName>BMCServiceDesk__notify_client_when_incident_is_closed</fullName>
        <ccEmails>rfdialog@us.sika.com</ccEmails>
        <description>notify_client_when_incident_is_closed</description>
        <protected>false</protected>
        <recipients>
            <field>BMCServiceDesk__FKContact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>BMCServiceDesk__FKClient__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/SikaCloseNotice</template>
    </alerts>
    <alerts>
        <fullName>BMCServiceDesk__notify_client_when_incident_is_created</fullName>
        <ccEmails>rfdialog@us.sika.com</ccEmails>
        <description>notify_client_when_incident_is_created</description>
        <protected>false</protected>
        <recipients>
            <field>BMCServiceDesk__FKContact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>BMCServiceDesk__FKClient__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/Sika_Incident_Create_Notice</template>
    </alerts>
    <alerts>
        <fullName>BMCServiceDesk__notify_client_when_service_request_is_closed</fullName>
        <ccEmails>rfdialog@us.sika.com</ccEmails>
        <description>notify_client_when_service_request_is_closed</description>
        <protected>false</protected>
        <recipients>
            <field>BMCServiceDesk__FKContact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>BMCServiceDesk__FKClient__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/BMCServiceDesk__SRM_Closed</template>
    </alerts>
    <alerts>
        <fullName>BMCServiceDesk__notify_client_when_service_request_is_created</fullName>
        <ccEmails>rfdialog@us.sika.com</ccEmails>
        <description>notify_client_when_service_request_is_created</description>
        <protected>false</protected>
        <recipients>
            <field>BMCServiceDesk__FKContact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>BMCServiceDesk__FKClient__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/BMCServiceDesk__SRM_Created</template>
    </alerts>
    <alerts>
        <fullName>BMCServiceDesk__notify_staff_of_incident_due_in_1_hour</fullName>
        <description>notify_staff_of_incident_due_in_1_hour</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/BMCServiceDesk__Incident_Email_Template_9_Version_2</template>
    </alerts>
    <alerts>
        <fullName>BMCServiceDesk__notify_staff_of_incident_nearing_due_date</fullName>
        <description>notify_staff_of_incident_nearing_due_date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/BMCServiceDesk__Incident_Email_Template_4_Version_2</template>
    </alerts>
    <alerts>
        <fullName>BMCServiceDesk__notify_staff_when_incident_is_assigned_to_them</fullName>
        <description>notify_staff_when_incident_is_assigned_to_them</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/BMCServiceDesk__Incident_Email_Template_7_Version_2</template>
    </alerts>
    <alerts>
        <fullName>BMCServiceDesk__notify_staff_when_incident_is_created</fullName>
        <description>notify_staff_when_incident_is_created</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/BMCServiceDesk__Incident_Email_Template_1_Version_2</template>
    </alerts>
    <alerts>
        <fullName>BMCServiceDesk__notify_staff_when_no_action_has_occurred_for_24_hours</fullName>
        <description>notify_staff_when_no_action_has_occurred_for_24_hours</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/BMCServiceDesk__Incident_Email_Template_3_Version_2</template>
    </alerts>
    <alerts>
        <fullName>Notify_Finance_of_Corporate_Credit_Card_Request</fullName>
        <ccEmails>rfdialog@us.sika.com</ccEmails>
        <description>Notify Finance of Corporate Credit Card Request</description>
        <protected>false</protected>
        <recipients>
            <recipient>spohn.darlene@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/Service_Request_Contains_Corporate_Credit_Card</template>
    </alerts>
    <alerts>
        <fullName>Notify_Fleet_of_Company_Car_Request</fullName>
        <ccEmails>rfdialog@us.sika.com</ccEmails>
        <description>Notify Fleet of Company Car Request</description>
        <protected>false</protected>
        <recipients>
            <recipient>spohn.darlene@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/Service_Request_Contains_Company_Car</template>
    </alerts>
    <alerts>
        <fullName>notify_staff_when_incident_is_assigned_to_FTS_P1</fullName>
        <ccEmails>9172829770@vtext.com</ccEmails>
        <ccEmails>9732023868@vtext.com</ccEmails>
        <ccEmails>2017040317@vtext.com</ccEmails>
        <ccEmails>2017076417@vtext.com</ccEmails>
        <ccEmails>2016932809@vtext.com</ccEmails>
        <description>notify_staff_when_incident_is_assigned_to_FTS_P1</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/BMCServiceDesk__Incident_Email_Template_7_Version_2</template>
    </alerts>
    <alerts>
        <fullName>notify_staff_when_incident_is_escalated</fullName>
        <description>notify_staff_when_incident_is_escalated</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/Sika_Incident_Escalation_Notice</template>
    </alerts>
    <alerts>
        <fullName>notify_staff_when_service_incident_is_assigned_to_them</fullName>
        <description>notify_staff_when_service_request_is_assigned_to_them</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>BMCServiceDesk__SDE_Emails/SRM_Created</template>
    </alerts>
    <fieldUpdates>
        <fullName>Add_NA_SAP_AUTOROUTE_Template</fullName>
        <description>Updates the Template field to NA_SAP_AUTOROUTE.</description>
        <field>BMCServiceDesk__TemplateName__c</field>
        <formula>&quot;NA_SAP_AUTOROUTE&quot;</formula>
        <name>Add NA_SAP_AUTOROUTE Template</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_US_SYSTEMS_AUTOROUTE_Template</fullName>
        <description>Updates the Template field to US_SYSTEMS_AUTOROUTE.</description>
        <field>BMCServiceDesk__TemplateName__c</field>
        <formula>&quot;US_SYSTEMS_AUTOROUTE&quot;</formula>
        <name>Add US_SYSTEMS_AUTOROUTE Template</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_CA_Service_Desk</fullName>
        <field>OwnerId</field>
        <lookupValue>CA_Service_Desk</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to CA Service Desk</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_EDI_Queue</fullName>
        <description>Chnage Incident Owner to EDI Queue</description>
        <field>OwnerId</field>
        <lookupValue>NA_EDI</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to EDI Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_US_Service_Desk</fullName>
        <field>OwnerId</field>
        <lookupValue>US_Service_desk</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to US Service Desk</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BMCServiceDesk__Apply_BBSA_Template</fullName>
        <field>BMCServiceDesk__TemplateName__c</field>
        <name>Apply BBSA Template</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BMCServiceDesk__Apply_BSA_Template</fullName>
        <field>BMCServiceDesk__TemplateName__c</field>
        <name>Apply BSA Template</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BMCServiceDesk__Apply_EUEM_Template</fullName>
        <field>BMCServiceDesk__TemplateName__c</field>
        <name>Apply EUEM Template</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BMCServiceDesk__Asset_Core_Approval_Status_Approved</fullName>
        <description>Update Asset Core Approval Status with Approved after successful operational rule deployment</description>
        <field>BMCServiceDesk__ACApprovalStatus__c</field>
        <literalValue>Approved</literalValue>
        <name>Asset Core Approval Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BMCServiceDesk__Asset_Core_Approval_Status_Rejected</fullName>
        <description>Update Asset Core Approval Status with Rejected after failed operational rule deployment</description>
        <field>BMCServiceDesk__ACApprovalStatus__c</field>
        <literalValue>Rejected</literalValue>
        <name>Asset Core Approval Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BMCServiceDesk__Update_Service_Request_as_Approved</fullName>
        <description>Updates the flag approved to TRUE on final approval</description>
        <field>BMCServiceDesk__Approved__c</field>
        <literalValue>1</literalValue>
        <name>Update Service Request as Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BMCServiceDesk__Update_the_ShowDueDateDialog_Field</fullName>
        <field>BMCServiceDesk__ShowDueDateDialog__c</field>
        <literalValue>1</literalValue>
        <name>Update the ShowDueDateDialog Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_to_Mail</fullName>
        <description>Incident Contact Source to Mail</description>
        <field>BMCServiceDesk__contactType__c</field>
        <literalValue>Mail</literalValue>
        <name>Change to Mail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Logicalis</fullName>
        <field>Logicalis__c</field>
        <literalValue>1</literalValue>
        <name>Check Logicalis</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Default_Template_Name</fullName>
        <description>Change Template Name to Default</description>
        <field>BMCServiceDesk__TemplateName__c</field>
        <formula>&quot;Default Template&quot;</formula>
        <name>Default Template Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incident_Call_Back_Number_Update</fullName>
        <description>Update New Incident with Client Call back Number</description>
        <field>Callback_Phone_Number__c</field>
        <formula>IF(BMCServiceDesk__FKClient__r.Phone&lt;&gt;&quot;&quot;, BMCServiceDesk__FKClient__r.Phone,
IF(BMCServiceDesk__FKClient__r.MobilePhone &lt;&gt;&quot;&quot;, BMCServiceDesk__FKClient__r.MobilePhone,
&quot;Not Given&quot;))</formula>
        <name>Incident Call Back Number Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NA_SAP_Queue_Assign</fullName>
        <description>This field update changes the Owner of a RemedyForce ticket to NA SAP.</description>
        <field>OwnerId</field>
        <lookupValue>NA_SAP</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>NA SAP Queue Assign</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NSM_Due_Date</fullName>
        <description>Update Due Date to 11/15/13</description>
        <field>BMCServiceDesk__dueDateTime__c</field>
        <name>NSM Due Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>US_Systems_Queue_Assign</fullName>
        <description>Changes Incident Owner to US Systems Queue</description>
        <field>OwnerId</field>
        <lookupValue>US_Systems</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>US Systems Queue Assign</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approved_to_True</fullName>
        <field>BMCServiceDesk__Approved__c</field>
        <literalValue>1</literalValue>
        <name>Update Approved to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <flowActions>
        <fullName>New_Hire_Flow_Trigger</fullName>
        <flow>New_Hire</flow>
        <flowInputs>
            <name>varIncidentId</name>
            <value>{!Id}</value>
        </flowInputs>
        <label>New Hire Flow Trigger</label>
        <language>en_US</language>
        <protected>false</protected>
    </flowActions>
    <rules>
        <fullName>Assign HR Termination to US System Queue</fullName>
        <actions>
            <name>Add_US_SYSTEMS_AUTOROUTE_Template</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>US_Systems_Queue_Assign</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>contains</operation>
            <value>IT Queue Assignment: US Systems</value>
        </criteriaItems>
        <description>Assign HR Termination to US System Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign SAP Emails to NA SAP Queue</fullName>
        <actions>
            <name>Add_NA_SAP_AUTOROUTE_Template</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NA_SAP_Queue_Assign</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>contains</operation>
            <value>IT Queue Assignment: NA SAP</value>
        </criteriaItems>
        <description>Assign SAP Emails to NA SAP Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Apply template to BMC Server Automation created incident</fullName>
        <actions>
            <name>BMCServiceDesk__Apply_BSA_Template</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>contains</operation>
            <value>Job id:</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>contains</operation>
            <value>Name:</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>contains</operation>
            <value>Job Group id:</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>contains</operation>
            <value>Job Run id:</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>contains</operation>
            <value>Start Time:</value>
        </criteriaItems>
        <description>This workflow will apply a template to the incident created by BMC Server Automation</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Apply template to End User Experience Management created incident</fullName>
        <actions>
            <name>BMCServiceDesk__Apply_EUEM_Template</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8</booleanFilter>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>contains</operation>
            <value>Date:</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>contains</operation>
            <value>Source:</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>contains</operation>
            <value>Incident type:</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>contains</operation>
            <value>Incident detection rule:</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>contains</operation>
            <value>Watchpoint:</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>contains</operation>
            <value>Urgency rating:</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>contains</operation>
            <value>Sessions:</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>contains</operation>
            <value>Description:</value>
        </criteriaItems>
        <description>This workflow will apply a template to the incident created by End User Experience Management</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Notify incident owner when each linked task is closed</fullName>
        <actions>
            <name>BMCServiceDesk__Notify_owner_when_linked_task_is_closed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notifies incident owner when each linked task of the incident is closed.</description>
        <formula>AND( ISCHANGED( BMCServiceDesk__Task_Closed_Controller__c), NOT(ISBLANK( BMCServiceDesk__Task_Closed_Controller__c) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Notify incident owner when final task linked to incident is closed</fullName>
        <actions>
            <name>BMCServiceDesk__Notify_Incident_Owner_when_final_Task_Linked_to_Incident_is_Closed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notify incident owner when final task linked to incident is closed</description>
        <formula>AND( BMCServiceDesk__state__c, NOT( BMCServiceDesk__inactive__c ), IF(BMCServiceDesk__AllTaskCloseController__c, ISCHANGED(BMCServiceDesk__AllTaskCloseController__c), false) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Notify staff of incident due in 1 hour</fullName>
        <active>false</active>
        <description>Notify staff of incident due in 1 hour</description>
        <formula>BMCServiceDesk__state__c  = True  &amp;&amp;  NOT(ISBLANK(BMCServiceDesk__dueDateTime__c) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Notify staff of incident nearing due date</fullName>
        <active>false</active>
        <description>Notify staff of incident nearing due date</description>
        <formula>BMCServiceDesk__state__c  = True  &amp;&amp;  NOT(ISBLANK(BMCServiceDesk__dueDateTime__c) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Notify staff when no action has occurred for 24 hours</fullName>
        <active>false</active>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__state__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notifies the assigned staff member if the staff member has not taken any action on the incident for 24 Hours</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Notify the assigned staff member when an incident has been marked for follow up</fullName>
        <actions>
            <name>BMCServiceDesk__notify_assign_to_on_ticket_followup</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notifies the staff member when an incident assigned to the staff member has been marked for follow up</description>
        <formula>BMCServiceDesk__followUp__c  = True &amp;&amp;  BMCServiceDesk__state__c  = False &amp;&amp;  PRIORVALUE( BMCServiceDesk__state__c )  = True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Notify the assigned staff member when an incident is created and assigned to the staff member</fullName>
        <actions>
            <name>BMCServiceDesk__notify_staff_when_incident_is_created</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notifies the staff member when an incident is created and assigned to the staff member</description>
        <formula>$User.Id  &lt;&gt;  OwnerId  &amp;&amp;  BMCServiceDesk__state__c  = True</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Notify the assigned staff member when an incident is reassigned to the staff member</fullName>
        <actions>
            <name>BMCServiceDesk__notify_staff_when_incident_is_assigned_to_them</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notifies the staff member when an incident is reassigned to the staff member</description>
        <formula>OwnerId &lt;&gt; $User.Id  &amp;&amp;  ISCHANGED( OwnerId )  &amp;&amp;   NOT(ISNEW() ) &amp;&amp; BMCServiceDesk__state__c  = True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Notify the assigned staff member when an incident is reopened</fullName>
        <actions>
            <name>BMCServiceDesk__notify_assign_to_on_ticket_reopen</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notifies the staff member when a closed incident is reopened and assigned to the staff member</description>
        <formula>$User.Id  &lt;&gt;  OwnerId  &amp;&amp;  BMCServiceDesk__state__c  = True &amp;&amp;  PRIORVALUE( BMCServiceDesk__state__c ) = False</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Notify the client when a service request is closed</fullName>
        <actions>
            <name>BMCServiceDesk__notify_client_when_service_request_is_closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notifies the client that the client’s service request is closed</description>
        <formula>(BMCServiceDesk__state__c  =  false) &amp;&amp; NOT( ISBLANK( BMCServiceDesk__FKRequestDetail__c) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Notify the client when a service request is created</fullName>
        <actions>
            <name>BMCServiceDesk__notify_client_when_service_request_is_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notifies the client that the service request requested by the client is created</description>
        <formula>NOT( ISBLANK( BMCServiceDesk__FKRequestDetail__c ) || ISBLANK(  BMCServiceDesk__FKRequestDefinition__c  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Notify the client when a service request is reopened</fullName>
        <actions>
            <name>BMCServiceDesk__notify_client_on_service_request_reopen</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notifies the client that the client’s service request is reopened</description>
        <formula>BMCServiceDesk__state__c  = True &amp;&amp;  PRIORVALUE( BMCServiceDesk__state__c ) = False &amp;&amp;   NOT(ISBLANK( BMCServiceDesk__FKRequestDetail__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Notify the client when a service request status is changed</fullName>
        <actions>
            <name>BMCServiceDesk__Notify_client_when_service_request_status_changes</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notifies the client that the client’s service request status has changed</description>
        <formula>( BMCServiceDesk__FKStatus__c &lt;&gt; PRIORVALUE( BMCServiceDesk__FKStatus__c)) &amp;&amp; NOT(ISBLANK( BMCServiceDesk__FKRequestDetail__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Notify the client when an incident is closed</fullName>
        <actions>
            <name>BMCServiceDesk__notify_client_when_incident_is_closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notifies the client that the client’s incident is closed</description>
        <formula>(BMCServiceDesk__state__c  = False) &amp;&amp; (BMCServiceDesk__followUp__c = False) &amp;&amp; (ISBLANK( BMCServiceDesk__FKRequestDetail__c) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Notify the client when an incident is created</fullName>
        <actions>
            <name>BMCServiceDesk__notify_client_when_incident_is_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notifies the client that the incident requested by the client is created</description>
        <formula>(BMCServiceDesk__state__c = True) &amp;&amp; (ISBLANK( BMCServiceDesk__FKRequestDetail__c)  &amp;&amp; ISBLANK(  BMCServiceDesk__FKRequestDefinition__c  ))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Notify the client when an incident is reopened</fullName>
        <actions>
            <name>BMCServiceDesk__notify_client_on_ticket_reopen</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notifies the client that the client’s incident is reopened</description>
        <formula>BMCServiceDesk__state__c  = True &amp;&amp;  PRIORVALUE( BMCServiceDesk__state__c ) = False &amp;&amp;  ISBLANK( BMCServiceDesk__FKRequestDetail__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Open popup dialog for recalculating due date when priority of incident changes</fullName>
        <actions>
            <name>BMCServiceDesk__Update_the_ShowDueDateDialog_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule has been deprecated. If the rule is active, deactivate it.</description>
        <formula>ISCHANGED( BMCServiceDesk__FKPriority__c ) &amp;&amp; IF( BMCServiceDesk__ShowDueDateDialog__c   = false,true, false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BMCServiceDesk__Update the incident%E2%80%99s status through email</fullName>
        <actions>
            <name>BMCServiceDesk__call_status_from_email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Sends the status of the incident to the sender of the email</description>
        <formula>ISCHANGED( BMCServiceDesk__WorkflowController__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Call Back Number Update</fullName>
        <actions>
            <name>Incident_Call_Back_Number_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.Callback_Phone_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If the Call Back Number is not entered, pull call back number from Client Phone or Cell number with the ServiceClient User Record, if available.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Default Template</fullName>
        <actions>
            <name>Default_Template_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__TemplateName__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__Category_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>notContain</operation>
            <value>IT Queue Assignment: NA SAP</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>notContain</operation>
            <value>IT Queue Assignment: US Systems</value>
        </criteriaItems>
        <description>If no template is selected, pick default.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Modified by Logicalis</fullName>
        <actions>
            <name>Check_Logicalis</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.LastModifiedById</field>
            <operation>equals</operation>
            <value>Sarah Harrison</value>
        </criteriaItems>
        <description>Activate if Last Modified is Sarah Harrison (Logicalis user)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>NSM Template</fullName>
        <actions>
            <name>NSM_Due_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__Category_ID__c</field>
            <operation>equals</operation>
            <value>NSM Request</value>
        </criteriaItems>
        <description>If Category is NSM Request, Set some Settings.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Hire Approval Process</fullName>
        <actions>
            <name>New_Hire_Flow_Trigger</name>
            <type>FlowAction</type>
        </actions>
        <active>false</active>
        <formula>BMCServiceDesk__Approved__c &amp;&amp; ischanged(BMCServiceDesk__Approved__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Client on Acknowledge</fullName>
        <active>false</active>
        <formula>BMCServiceDesk__FKStatus__r.BMCServiceDesk__Stage__c == &apos;Acknowledged&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Finance when a service request contains corporate credit card</fullName>
        <actions>
            <name>Notify_Finance_of_Corporate_Credit_Card_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notifies the Finance contact that the service request requested by the client contains corporate credit card.</description>
        <formula>(BMCServiceDesk__state__c = True) &amp;&amp; 
CONTAINS(BMCServiceDesk__incidentDescription__c,&apos;Corporate Credit Card: true&apos;) &amp;&amp; 
(NOT(ISBLANK(BMCServiceDesk__FKRequestDetail__c) || 
ISBLANK(BMCServiceDesk__FKRequestDefinition__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Fleet when a service request contains company car</fullName>
        <actions>
            <name>Notify_Fleet_of_Company_Car_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notifies the Fleet contact that the service request requested by the client contains company car.</description>
        <formula>(BMCServiceDesk__state__c = True) &amp;&amp; 
CONTAINS(BMCServiceDesk__incidentDescription__c,&apos;Company Car: true&apos;) &amp;&amp; 
(NOT(ISBLANK(BMCServiceDesk__FKRequestDetail__c) || 
ISBLANK(BMCServiceDesk__FKRequestDefinition__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify staff when an incidnet status is escalated</fullName>
        <actions>
            <name>notify_staff_when_incident_is_escalated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notifies staff user or queue that the incident’s status has changed to ESCALATION</description>
        <formula>BMCServiceDesk__Status_ID__c = &apos;ESCALATION&apos; &amp;&amp; (ISCHANGED(BMCServiceDesk__Status_ID__c) || ISNEW())</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Route Incident to CA Service Desk</fullName>
        <actions>
            <name>Assign_to_CA_Service_Desk</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__clientEmail__c</field>
            <operation>contains</operation>
            <value>@ca.sika.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.OwnerId</field>
            <operation>equals</operation>
            <value>Incident Queue</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>notContain</operation>
            <value>IT Queue Assignment: US Systems</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>notContain</operation>
            <value>IT Queue Assignment: NA SAP</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Route Incident to US Service Desk</fullName>
        <actions>
            <name>Assign_to_US_Service_Desk</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__clientEmail__c</field>
            <operation>notContain</operation>
            <value>@ca.sika.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.OwnerId</field>
            <operation>equals</operation>
            <value>Incident Queue</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>notContain</operation>
            <value>IT Queue Assignment: US Systems</value>
        </criteriaItems>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__incidentDescription__c</field>
            <operation>notContain</operation>
            <value>IT Queue Assignment: NA SAP</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SIka - Notify the assigned staff member when a service request is reassigned to the staff member</fullName>
        <actions>
            <name>notify_staff_when_service_incident_is_assigned_to_them</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notifies the staff member when a service request is reassigned to the staff member</description>
        <formula>OwnerId &lt;&gt; $User.Id  &amp;&amp;  
(ISCHANGED(OwnerId) || ISNEW()) &amp;&amp;   
BMCServiceDesk__state__c  = True &amp;&amp; 
(NOT(ISBLANK(BMCServiceDesk__FKRequestDetail__c) || 
ISBLANK(BMCServiceDesk__FKRequestDefinition__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SIka - Notify the assigned staff member when an incident is reassigned to the FTS P1</fullName>
        <actions>
            <name>notify_staff_when_incident_is_assigned_to_FTS_P1</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notifies the staff when an incident is reassigned to the FTS P1 queue</description>
        <formula>OwnerId &lt;&gt; $User.Id  &amp;&amp;  
OwnerId = &apos;00G40000002EPlh&apos;  &amp;&amp;  
(ISCHANGED(OwnerId) || ISNEW()) &amp;&amp;   
BMCServiceDesk__state__c  = True &amp;&amp; 
ISBLANK(BMCServiceDesk__FKRequestDetail__c) &amp;&amp; 
ISBLANK(BMCServiceDesk__FKRequestDefinition__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SIka - Notify the assigned staff member when an incident is reassigned to the staff member</fullName>
        <actions>
            <name>BMCServiceDesk__notify_staff_when_incident_is_assigned_to_them</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notifies the staff member when an incident is reassigned to the staff member</description>
        <formula>OwnerId &lt;&gt; $User.Id  &amp;&amp;  
OwnerId &lt;&gt; &apos;00G40000002EPlh&apos; &amp;&amp; 
(ISCHANGED(OwnerId) || ISNEW()) &amp;&amp;   
BMCServiceDesk__state__c  = True &amp;&amp; 
ISBLANK(BMCServiceDesk__FKRequestDetail__c) &amp;&amp; 
ISBLANK(BMCServiceDesk__FKRequestDefinition__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sika Notify staff of incident nearing due date</fullName>
        <active>false</active>
        <description>Notify staff of incident nearing due date</description>
        <formula>BMCServiceDesk__state__c  = True  &amp;&amp;  NOT(ISBLANK(BMCServiceDesk__dueDateTime__c) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BMCServiceDesk__notify_staff_of_incident_nearing_due_date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>BMCServiceDesk__Incident__c.BMCServiceDesk__dueDateTime__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Tami Laurin EDI</fullName>
        <actions>
            <name>Assign_to_EDI_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>BMCServiceDesk__Incident__c.BMCServiceDesk__FKClient__c</field>
            <operation>equals</operation>
            <value>0054000000161YD</value>
        </criteriaItems>
        <description>Any new Ticket from Tami Laurin is assigned to EDI Queue
laurin.tami@us.sika.com,estes.mike@us.sika.com</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
