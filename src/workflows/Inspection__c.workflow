<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Roofing_Inspection_Status</fullName>
        <description>Roofing Inspection Status</description>
        <protected>false</protected>
        <recipients>
            <field>Roofing_Asst_Tech_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Roofing_CSR_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Roofing_Primary_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Roofing_Tech_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Tech_Service_Rep_Email1__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PTS_Templates/Roofing_Inspection_Status</template>
    </alerts>
    <fieldUpdates>
        <fullName>CongaFieldStart_Reset</fullName>
        <description>Uncheck the Start Box</description>
        <field>CongaFieldReportWorkflowStart__c</field>
        <literalValue>0</literalValue>
        <name>CongaFieldStart Reset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CongaLAMStart_Reset</fullName>
        <description>Uncheck the Start Box</description>
        <field>CongaLAMWorkflowStart__c</field>
        <literalValue>0</literalValue>
        <name>CongaLAMStart Reset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CongaWarrantyStart_Reset</fullName>
        <field>CongaWarrantyWorkflowStart__c</field>
        <literalValue>0</literalValue>
        <name>CongaWarrantyStart Reset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CongaWaterproofingStart_Reset</fullName>
        <description>Uncheck the Start Box</description>
        <field>CongaWaterproofingInstallationWorkflow__c</field>
        <literalValue>0</literalValue>
        <name>CongaWaterproofingStart Reset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Inspection_Name</fullName>
        <field>Name</field>
        <formula>&quot;Inspection #&quot; + TEXT((Opportunity_Name__r.Inspection_Count__c)+1) + &quot;-&quot; +&quot; &quot; + Opportunity_Name__r.Account.Name</formula>
        <name>Field Update: Inspection Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Search_Bucket</fullName>
        <description>Copy Search Terms</description>
        <field>Search_Bucket__c</field>
        <formula>Opportunity_Name__r.Name &amp; br()&amp;
Opportunity_Name__r.City__c &amp;br()&amp;
Opportunity_Name__r.State__c &amp;br()&amp;
br()&amp;
Building_Group__r.Building_Group_name__c &amp;br()&amp;
br()&amp;
Opportunity_Name__r.Account.Name</formula>
        <name>Search Bucket</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Conga_Inspection_Workflow</fullName>
        <apiVersion>34.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>CongaFieldInspection_URL__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>estes.mike@us.sika.com</integrationUser>
        <name>Conga Inspection Workflow</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Conga_LAM_Workflow</fullName>
        <apiVersion>34.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>CongaLAM_URL__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>estes.mike@us.sika.com</integrationUser>
        <name>Conga LAM Workflow</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Conga_Warranty_Workflow</fullName>
        <apiVersion>34.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>CongaWarranty_URL__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>estes.mike@us.sika.com</integrationUser>
        <name>Conga Warranty Workflow</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Conga_Waterproofing_Workflow</fullName>
        <apiVersion>34.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>CongaWaterInspection_URL__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>estes.mike@us.sika.com</integrationUser>
        <name>Conga Waterproofing Workflow</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Conga Field Report Check Box Start</fullName>
        <actions>
            <name>CongaFieldStart_Reset</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Conga_Inspection_Workflow</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Inspection__c.CongaFieldReportWorkflowStart__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Conga LAM Check Box Start</fullName>
        <actions>
            <name>CongaLAMStart_Reset</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Conga_LAM_Workflow</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Inspection__c.CongaLAMWorkflowStart__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Conga Warranty Check Box Start</fullName>
        <actions>
            <name>CongaWarrantyStart_Reset</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Conga_Warranty_Workflow</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Inspection__c.CongaWarrantyWorkflowStart__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Conga Waterproofing Installation Check Box Start</fullName>
        <actions>
            <name>CongaWaterproofingStart_Reset</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Conga_Waterproofing_Workflow</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Inspection__c.CongaWaterproofingInstallationWorkflow__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inspection Search Bucket</fullName>
        <actions>
            <name>Search_Bucket</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Update Search Terms to Search bucket</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Inspection%3A Name Update</fullName>
        <actions>
            <name>Field_Update_Inspection_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Inspection__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
