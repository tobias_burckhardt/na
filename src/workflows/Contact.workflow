<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Bing_Yahoo_Email_to_Inside_Sales</fullName>
        <description>Bing/Yahoo Email to Inside Sales</description>
        <protected>false</protected>
        <recipients>
            <recipient>bellico.william@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_Promote_Landing_Page_Contacts</template>
    </alerts>
    <alerts>
        <fullName>Contact_Us_Eloqua_IS_Manager</fullName>
        <description>Email Notification to Inside Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>yun.alan@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_Promote_Landing_Page_Contacts</template>
    </alerts>
    <alerts>
        <fullName>DFW_Contact_Email_Notification_to_Field_Rep_RSB</fullName>
        <ccEmails>katic-michalos.tina@us.sika.com</ccEmails>
        <description>DFW-Email Notification to Field Rep- RSB</description>
        <protected>false</protected>
        <recipients>
            <recipient>bohannon.kevin@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_Promote_Landing_Page</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_to_Inside_Sales_Rep_BT</fullName>
        <description>Email Notification to Inside Sales Rep</description>
        <protected>false</protected>
        <recipients>
            <recipient>dansby.kim@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Lead_Score_Achieved_Contacts</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_to_Inside_Sales_Rep_EW</fullName>
        <description>Email Notification to Inside Sales Rep</description>
        <protected>false</protected>
        <recipients>
            <recipient>walkama.ellen@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Lead_Score_Achieved_Contacts</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_to_Inside_Sales_Rep_TW</fullName>
        <description>Email Notification to Inside Sales Rep</description>
        <protected>false</protected>
        <recipients>
            <recipient>wallace.trudy@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Lead_Score_Achieved_Contacts</template>
    </alerts>
    <alerts>
        <fullName>Fulfillment_Contacts</fullName>
        <ccEmails>mochun.stephen@us.sika.com</ccEmails>
        <description>Fulfillment  Email for Contacts</description>
        <protected>false</protected>
        <recipients>
            <recipient>bellico.william@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_Promote_Landing_Page_Contacts</template>
    </alerts>
    <alerts>
        <fullName>Fulfillment_Email_Contact</fullName>
        <ccEmails>mochun.stephen@us.sika.com</ccEmails>
        <description>Fulfillment Email- Contact</description>
        <protected>false</protected>
        <recipients>
            <recipient>bellico.william@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/HCD_HCBI_Newsletter</template>
    </alerts>
    <alerts>
        <fullName>Google_Adwords_Contact_Lead_To_IS_Manager</fullName>
        <description>Google Adwords Contact-Lead To IS Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>bellico.william@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_Promote_Landing_Page_Contacts</template>
    </alerts>
    <alerts>
        <fullName>Google_Adwords_Contact_Lead_to_Inside_Sales_Manager</fullName>
        <description>Google Adwords Contact-Lead to Inside Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>bellico.william@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_Promote_Landing_Page_Contacts</template>
    </alerts>
    <alerts>
        <fullName>Google_Adwords_Eloqua_Contacts_Lead_Notify_IS_Manager</fullName>
        <description>Google Adwords Eloqua Contacts-Lead Notify IS Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>bellico.william@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_Promote_Landing_Page_Contacts</template>
    </alerts>
    <alerts>
        <fullName>Google_Adwords_Lead_Contact_Notify_IS_Manager</fullName>
        <description>Google Adwords Lead-Contact Notify IS Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>bellico.william@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_Promote_Landing_Page_Contacts</template>
    </alerts>
    <alerts>
        <fullName>Lead_Score_Achieved_Follow_Up</fullName>
        <description>Lead Score Achieved Follow Up</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Roof_Marketing_Campaigns/Lead_Score_for_Chris_Falcone</template>
    </alerts>
    <alerts>
        <fullName>Lead_Score_Contact_Follow_up_Email</fullName>
        <description>Lead Score Contact Follow up Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Roof_Marketing_Campaigns/Lead_Score_for_Trudy_Wallace</template>
    </alerts>
    <alerts>
        <fullName>Lead_Score_Contact_Follow_up_Email_BT</fullName>
        <description>Lead Score Contact Follow up Email SW</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Lead_Score_for_SW_Region</template>
    </alerts>
    <alerts>
        <fullName>Lead_Score_Contact_Follow_up_Email_EW</fullName>
        <description>Lead Score Contact Follow up Email EW</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Roof_Marketing_Campaigns/Lead_Score_for_Ellen_Walkama</template>
    </alerts>
    <alerts>
        <fullName>Lead_Score_Contact_Follow_up_Email_SM</fullName>
        <description>Lead Score Contact Follow up Email SM</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Roof_Marketing_Campaigns/Lead_Score_for_Stacie_Mooney</template>
    </alerts>
    <alerts>
        <fullName>Lead_Score_Contact_Follow_up_Email_TM</fullName>
        <description>Lead Score Contact Follow up Email TM</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Roof_Marketing_Campaigns/Lead_Score_for_Tom_May</template>
    </alerts>
    <alerts>
        <fullName>West_Coast_Contact_Email</fullName>
        <description>West Coast Contact Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Lead_Score_for_BT</template>
    </alerts>
    <fieldUpdates>
        <fullName>Campaign_Name_Update</fullName>
        <field>Campaign_Name_Original__c</field>
        <formula>&quot;Google Adwords Phone Leads&quot;</formula>
        <name>Campaign Name Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Concrete_TRUE</fullName>
        <description>Check TM Concrete Contact Checkbox TRUE</description>
        <field>TM_Concrete__c</field>
        <literalValue>1</literalValue>
        <name>Check Concrete TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flooring_TM_Check_TRUE</fullName>
        <field>TM_Flooring__c</field>
        <literalValue>1</literalValue>
        <name>Flooring TM Check TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Free_Roof_Eval_Field_Update</fullName>
        <description>Campaign Promo item update for Free Roof Eval</description>
        <field>Campaign_Promo_Item__c</field>
        <formula>&quot;Free Eval Call&quot;</formula>
        <name>Free Roof Eval Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Free_Roof_Eval_Field_Update_Contacts</fullName>
        <description>Free Roof Evaluation field update for Campaign Promo item for contacts</description>
        <field>Campaign_Promo_Item__c</field>
        <formula>&quot;Free Eval Call&quot;</formula>
        <name>Free Roof Eval Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Score_Field_Update</fullName>
        <description>Campaign Promo Item updated for Lead Score Achieved</description>
        <field>Campaign_Promo_Item__c</field>
        <formula>&quot;Lead Score Call&quot;</formula>
        <name>Lead Score Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Make_Outbound_Call_Field_Update</fullName>
        <description>Make Outbound Call Field Update Promo Item</description>
        <field>Campaign_Promo_Item__c</field>
        <formula>&quot;Make Outbound Call&quot;</formula>
        <name>Make Outbound Call Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Promo_Item_Update</fullName>
        <description>Update the Promo Item for Eloqua Forms Fill outs</description>
        <field>Campaign_Promo_Item__c</field>
        <formula>&quot;PPC Call&quot;</formula>
        <name>Promo Item Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Field_Update</fullName>
        <field>Campaign_Promo_Item__c</field>
        <formula>&quot;Quote&quot;</formula>
        <name>Quote Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Field_Update1</fullName>
        <field>Campaign_Promo_Item__c</field>
        <formula>&quot;Quote&quot;</formula>
        <name>Quote Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Field_Update3</fullName>
        <field>Campaign_Promo_Item__c</field>
        <formula>&quot;Quote&quot;</formula>
        <name>Quote Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Field_Update5</fullName>
        <field>Campaign_Promo_Item__c</field>
        <formula>&quot;Quote&quot;</formula>
        <name>Quote Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Field_Update7</fullName>
        <field>Campaign_Promo_Item__c</field>
        <formula>&quot;Quote&quot;</formula>
        <name>Quote Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RSB_Commercial_Contact_Check</fullName>
        <description>Check RSB Commercial Check Box TRUE</description>
        <field>TM_RSB_Commercial__c</field>
        <literalValue>1</literalValue>
        <name>RSB Commercial Contact Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Test_Contact_Account</fullName>
        <field>Account_Owner_or_Created_by_Roofing__c</field>
        <formula>IF( $User.ProfileId = &quot;00e40000000jlWp&quot; /* Profile: BU Roofing */
|| $User.ProfileId = &quot;00e40000000jlhs&quot;  /* Profile: BU Roofing Mgr */
|| $User.ProfileId = &quot;00e40000000jmAL&quot; , /* BU Roofing - IS/Marketing */
&quot;yes&quot; ,
&quot;no&quot;
)</formula>
        <name>Test Contact Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Roofing_Contact</fullName>
        <field>TM_Roofing__c</field>
        <literalValue>1</literalValue>
        <name>Update Roofing Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Waterproofing_TM_Check_TRUE</fullName>
        <description>Check Contact Waterproofing TM TRUE</description>
        <field>TM_Waterproofing__c</field>
        <literalValue>1</literalValue>
        <name>Waterproofing TM Check TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Lead_Score_Follow_Up_Email</fullName>
        <apiVersion>30.0</apiVersion>
        <endpointUrl>http://usa.sarnafil.sika.com/en/solutions_products/10/10sa01/choosing-a-roofing-system.html</endpointUrl>
        <fields>Email</fields>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>falcone.christopher@us.sika.com</integrationUser>
        <name>Lead Score Follow Up Email</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Lead_Score_Follow_up</fullName>
        <apiVersion>30.0</apiVersion>
        <endpointUrl>http://usa.sarnafil.sika.com/en/solutions_products/10/10sa01/choosing-a-roofing-system.html</endpointUrl>
        <fields>Email</fields>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>walkama.ellen@us.sika.com</integrationUser>
        <name>Lead Score Follow up</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Lead_Score_Follow_up_Email_BT</fullName>
        <apiVersion>30.0</apiVersion>
        <endpointUrl>http://usa.sarnafil.sika.com/en/solutions_products/10/10sa01/choosing-a-roofing-system.html</endpointUrl>
        <fields>Email</fields>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>tanguy.brian@us.sika.com</integrationUser>
        <name>Lead Score Follow up Email</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Lead_Score_Follow_up_SM</fullName>
        <apiVersion>30.0</apiVersion>
        <endpointUrl>http://usa.sarnafil.sika.com/en/solutions_products/10/10sa01/choosing-a-roofing-system.html</endpointUrl>
        <fields>Email</fields>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>mooney.stacie@us.sika.com</integrationUser>
        <name>Lead Score Follow up</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Lead_Score_Follow_up_email_TW</fullName>
        <apiVersion>30.0</apiVersion>
        <endpointUrl>http://usa.sarnafil.sika.com/en/solutions_products/10/10sa01/choosing-a-roofing-system.html</endpointUrl>
        <fields>Email</fields>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>wallace.trudy@us.sika.com</integrationUser>
        <name>Lead Score Follow up email</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Bay Area Contacts</fullName>
        <actions>
            <name>Chicago_Follow_up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Bayarea</value>
        </criteriaItems>
        <description>Every Day Building Envelope Follow Up</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Concrete Contact Check</fullName>
        <actions>
            <name>Check_Concrete_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check Contact Record to see if it is owned or modified by Concrete TM User</description>
        <formula>ISPICKVAL ( LastModifiedBy.Target_Market__c, &quot;Concrete&quot; ) ||
ISPICKVAL ( Owner.Target_Market__c, &quot;Concrete&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DFW- Concrete Contact</fullName>
        <actions>
            <name>DFW_Follow_up_on_Concrete_Contacts</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>SikaSmart-Guide</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Area_of_Interest__c</field>
            <operation>equals</operation>
            <value>Concrete admixtures,Parking Garage coatings and sealants</value>
        </criteriaItems>
        <description>DFW -Concrete Contact Object</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DFW-- Waterproofing Contact</fullName>
        <actions>
            <name>Chicago_Follow_up</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>SikaSmart-Guide</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Area_of_Interest__c</field>
            <operation>equals</operation>
            <value>Below grade waterproofing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <description>DFW -Waterproofing  Contact Object</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DFW-AIA Session Contact</fullName>
        <actions>
            <name>DFW_Follow_up_Roofing_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>Lunch and Learn</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.CEU_Request__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <description>DFW-Roffing Contact Object- AIA</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DFW-RSB Contact</fullName>
        <actions>
            <name>DFW_Contact_Email_Notification_to_Field_Rep_RSB</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>RSB_Commercial_Contact_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>DFW_Follow_up_on_RSB_Contacts</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Contact.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>SikaSmart-Guide</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Area_of_Interest__c</field>
            <operation>equals</operation>
            <value>Balconies</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Area_of_Interest__c</field>
            <operation>equals</operation>
            <value>Concrete repair and protection</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Area_of_Interest__c</field>
            <operation>equals</operation>
            <value>Exterior walls &amp; windows</value>
        </criteriaItems>
        <description>DFW-RSB Contact Object</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DFW-Roofing Contact</fullName>
        <actions>
            <name>DFW_Follow_up_Roofing_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>Lunch and Learn</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.CEU_Request__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <description>DFW-Roffing Contact Object</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Decor Package Send Out</fullName>
        <actions>
            <name>Fulfillment_Contacts</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Decor Sample Package</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Campaign_Name_Original__c</field>
            <operation>equals</operation>
            <value>Decor Send Package Trial 2014 Us Roofing</value>
        </criteriaItems>
        <description>For Contacts</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Decor Roof Guide Task Assignment Contacts</fullName>
        <actions>
            <name>Fulfillment_Contacts</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>Google Adwords - Remarketing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <description>Decor Roof Guide Task Assignment Contacts</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Eloqua Contact Score MidWest</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Score_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Score_Achieved_Falcone</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Contact.Eloqua_Lead_Rating__c</field>
            <operation>equals</operation>
            <value>A2,A1,B1,C1,B2,A3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>ND,SD,NE,KS,MN,IA,MO,WI,IL,MI,IN,OH,KY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Score_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.TM_Roofing__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Eloqua Contact Us form for ND,SD,NE,KS,MN,IA,MO,WI,IL,MI,IN,OH,KY</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Eloqua Contact Score NE</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Score_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Score_Achieved_TM</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Contact.Eloqua_Lead_Rating__c</field>
            <operation>equals</operation>
            <value>A2,A1,B1,C1,B2,A3,D1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>VT,NH,ME,MA,RI,CT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Score_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.TM_Roofing__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Eloqua Lead Score for Contact Object  form from VT,NH,ME,MA,RI,CT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Eloqua Contact Score West</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>West_Coast_Contact_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Score_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Score_Achieved_Mooney</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Eloqua_Lead_Rating__c</field>
            <operation>equals</operation>
            <value>A2,A1,B1,C1,B2,A3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>NV,CA,AZ,HI</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Score_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.TM_Roofing__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Eloqua Lead Score for Contacts Object for NV,CA,AZ,HI</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Eloqua Contact Score- Bellico</fullName>
        <actions>
            <name>Lead_Score_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Score_Achieved</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Eloqua_Lead_Rating__c</field>
            <operation>equals</operation>
            <value>D1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Score_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.TM_Roofing__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Lead Score D1 goes to Inside Sales Manager for Contact Object</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Eloqua Contact Score- SW Region</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Rep_BT</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Score_Contact_Follow_up_Email_BT</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Score_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Score_Achieved_BT</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Contact.Eloqua_Lead_Rating__c</field>
            <operation>equals</operation>
            <value>A2,A1,B1,C1,B2,A3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>TX,OK,AR,LA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Score_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.TM_Roofing__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Eloqua Lead Score for Contacts Object for SW Region (AR, LA, OK, TX)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Eloqua Contact Score- Walkama</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Rep_EW</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Score_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Score_Achieved_EW</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Eloqua_Lead_Rating__c</field>
            <operation>equals</operation>
            <value>A2,A1,B1,C1,B2,A3,D1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>TN,NC,MS,AL,GA,SC,FL,PR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Score_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.TM_Roofing__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Eloqua Lead Score for Contacts Object for TN,NC,MS,AL,GA,SC,FL,PR</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Eloqua Contact Score- Wallace</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Rep_TW</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Score_Contact_Follow_up_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Score_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Score_Achieved_TW</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Contact.Eloqua_Lead_Rating__c</field>
            <operation>equals</operation>
            <value>A2,A1,B1,C1,B2,A3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>AK,WA,OR,MT,ID,WY,UT,CO,NM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Score_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.TM_Roofing__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Lead Score for Contacts Object for AK,WA,OR,MT,ID,WY,UT,CO,NM</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Eloqua Flooring Contact Score</fullName>
        <actions>
            <name>Lead_Score_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>D1_Lead_Score_Achieved_Follow_Up_Call</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Eloqua_Lead_Rating__c</field>
            <operation>equals</operation>
            <value>D1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Score_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.TM_Flooring__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Lead Score D1 goes to  Jim Gildea to Filter out</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Eloqua Flooring Contact Score Achieved</fullName>
        <actions>
            <name>Lead_Score_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flooring_Lead_Score_Achieved</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Eloqua_Lead_Rating__c</field>
            <operation>equals</operation>
            <value>A2,A1,B1,C1,B2,A3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.TM_Flooring__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Lead_Score_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Flooring Lead Score Achieved A2,A1,B1,C1,B2,A3</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Energy Saving Kit Task Assignment for Contacts</fullName>
        <actions>
            <name>Fulfillment_Contacts</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Energy Saving Kit</value>
        </criteriaItems>
        <description>Energy Saving Kit Task Assignment for Contacts</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EveryDay Brochure Download Follow Up</fullName>
        <actions>
            <name>DFW_Building_Envelope_Lead</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>EveryDay-Guide</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Area_of_Interest__c</field>
            <operation>equals</operation>
            <value>Balconies,Below grade waterproofing,Building envelope,Concrete admixtures,Concrete repair and protection,Other,Parking Garage coatings and sealants</value>
        </criteriaItems>
        <description>EveryDay Brochure Download Follow Up for City- Focused Campaign</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EveryDay- Roofing</fullName>
        <actions>
            <name>Chicago_Follow_up</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>EveryDay-Guide</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Area_of_Interest__c</field>
            <operation>equals</operation>
            <value>Plaza deck waterproofing,Roofing</value>
        </criteriaItems>
        <description>Chicagoland- Roofing</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EveryDay-Flooring Contact</fullName>
        <actions>
            <name>DFW_Follow_up_Flooring_Contact</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>EveryDay-Guide</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Area_of_Interest__c</field>
            <operation>equals</operation>
            <value>Flooring &amp; wall coatings</value>
        </criteriaItems>
        <description>Chicagoland Every Day brochure download</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Floor Consultation - Organic</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Free_Floor_Eval_from_Website</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>Free Floor Eval- Organic,Design Kit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.TM_Flooring__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.LeadSource</field>
            <operation>equals</operation>
            <value>Floor Marketing Campaigns</value>
        </criteriaItems>
        <description>Free Floor submits from website referral</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flooring Contact Check</fullName>
        <actions>
            <name>Flooring_TM_Check_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check Contact Record to see if it is owned or modified by Concrete TM User</description>
        <formula>ISPICKVAL ( LastModifiedBy.Target_Market__c, &quot;Flooring&quot; ) ||
ISPICKVAL ( Owner.Target_Market__c, &quot;Flooring&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Green Roof Guide Contacts</fullName>
        <actions>
            <name>Fulfillment_Contacts</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Green Roof Guide</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RSB Commercial Contact Check</fullName>
        <actions>
            <name>RSB_Commercial_Contact_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check Contact Record to see if it is owned or modified by Waterproofing TM User</description>
        <formula>ISPICKVAL ( LastModifiedBy.Target_Market__c, &quot;RSB Commercial&quot; ) ||
ISPICKVAL ( Owner.Target_Market__c, &quot;RSB Commercial&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof Evaluation %28Organic%29 - East</fullName>
        <actions>
            <name>Free_Roof_Evaluation_Follow_up_SM</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Free Roof Evaluation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>NY,PA,NJ,DE,MD,VA,WV,DC</value>
        </criteriaItems>
        <description>roof Eval request from Website (organic) for NY,PA,NJ,DE,MD,VA,WV,DC</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof Evaluation %28Organic%29 - MidWest</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Free_Roof_Evaluation_Follo_Up_CF</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Free Roof Evaluation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>ND,SD,NE,KS,MN,IA,MO,WI,IL,MI,IN,OH,KY</value>
        </criteriaItems>
        <description>roof Eval request from Website (organic) for ND,SD,NE,KS,MN,IA,MO,WI,IL,MI,IN,OH,KY</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof Evaluation %28Organic%29 - NE</fullName>
        <actions>
            <name>Free_Roof_Evaluation_Follow_Up_BB</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Free Roof Evaluation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>VT,NH,ME,MA,RI,CT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Free_Eval_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Free Roof Eval VT, NH, ME, MA, RI, CT (Organic)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof Evaluation %28Organic%29 - SW</fullName>
        <actions>
            <name>Free_Roof_Evaluation_Follow_up_BT</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Free Roof Evaluation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>TX,OK,AR,LA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Free_Eval_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Free Roof Evaluation from Website for SW Region (AR, LA, OK, TX)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof Evaluation %28Organic%29- Walkama</fullName>
        <actions>
            <name>Free_Roof_Evaluation_follow_Up_EW</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Free Roof Evaluation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>TN,NC,MS,AL,GA,SC,FL,PR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Free_Eval_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Free Roof Evaluation Request from website for TN,NC,MS,AL,GA,SC,FL,PR</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof Evaluation %28Organic%29- Wallace</fullName>
        <actions>
            <name>Free_Roof_Evaluation_Follow_up_TW</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Free Roof Evaluation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>AK,WA,OR,MT,ID,WY,UT,CO,NM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Free_Eval_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Free Roof Evaluation request from website for AK,WA,OR,MT,ID,WY,UT,CO,NM</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Roof Evaluation %28Organic%29- West coast</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Free_Roof_Eval_Follow_Up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Free Roof Evaluation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Company_State__c</field>
            <operation>equals</operation>
            <value>NV,CA,AZ,HI</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Free_Eval_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Free Roof Evaluation request from NV,CA,AZ,HI</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof PPC -East</fullName>
        <actions>
            <name>Promo_Item_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PPC_ADwords_Follow_up_TM</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 3) AND 2 AND 4</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>call center,Google Adwords</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Company_State__c</field>
            <operation>equals</operation>
            <value>NY,PA,NJ,DE,MD,VA,WV,DC)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>Bing/ Yahoo PPC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <description>Google and Bing PPC for Contacts for NY,PA,NJ,DE,MD,VA,WV,DC</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof PPC Follow Up- MidWest</fullName>
        <actions>
            <name>Google_Adwords_Contact_Lead_To_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>PPC_Follow_Up_Mid</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>call center,Google Adwords,Google Adwords- Sikacoat</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>ND,SD,NE,KS,MN,IA,MO,WI,IL,MI,IN,OH,KY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>Bing/ Yahoo PPC</value>
        </criteriaItems>
        <description>PPC trigger for ND,SD,NE,KS,MN,IA,MO,WI,IL,MI,IN,OH,KY</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof PPC Wallace</fullName>
        <actions>
            <name>Google_Adwords_Eloqua_Contacts_Lead_Notify_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Promo_Item_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>google_Adwords_Follow_up_TW</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>call center,Google Adwords</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>AK,WA,OR,MT,ID,WY,UT,CO,NM</value>
        </criteriaItems>
        <description>Google Adwords Eloqua for Contacts AK,WA,OR,MT,ID,WY,UT,CO,NM</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof PPC- Southwest</fullName>
        <actions>
            <name>Google_Adwords_Contact_Lead_to_Inside_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Promo_Item_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Google_Adwords_Follow_up_BT</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>call center,Google Adwords</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>TX,OK,AR,LA</value>
        </criteriaItems>
        <description>Adwords and Bing rroof ppc request for TX,OK,AR,LA</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof PPC- Walkama</fullName>
        <actions>
            <name>Google_Adwords_Lead_Contact_Notify_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Promo_Item_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Google_Adwords_Follow_up_EW</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>call center,Google Adwords,Google Adwords- Sikacoat</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>TN,NC,MS,AL,GA,SC,FL,PR</value>
        </criteriaItems>
        <description>PPC requests (Bing and Adwords) for Roofing in TN,NC,MS,AL,GA,SC,FL,PR</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof PPC- West Coast</fullName>
        <actions>
            <name>Promo_Item_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Google_Adwords_Follow_up_SM</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>call center,Google Adwords,Google Adwords- Sikacoat</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Company_State__c</field>
            <operation>equals</operation>
            <value>NV,CA,AZ,HI</value>
        </criteriaItems>
        <description>PPC for NV,CA,AZ,HI</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof Quote Requested -NE</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Quote_Follow_Up1</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Quote Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Company_State__c</field>
            <operation>equals</operation>
            <value>VT,NH,ME,MA,RI,CT</value>
        </criteriaItems>
        <description>Quote Requested VT,NH,ME,MA,RI,CT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof Quote Requested- East</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Quote_Follow_Up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Quote Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>NY,PA,NJ,DE,MD,VA,WV,MEXICO</value>
        </criteriaItems>
        <description>Quote Requested for NY,PA,NJ,DE,MD,VA,WV,MEXICO</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof Quote Requested- MidWest</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Quote_Follow_Up7</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Quote Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>ND,SD,NE,KS,MN,IA,MO,WI,IL,MI,IN,OH,KY</value>
        </criteriaItems>
        <description>Quote Requested for Contacts Object for ND,SD,NE,KS,MN,IA,MO,WI,IL,MI,IN,OH,KY</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof Quote Requested- SW Region</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Quote_Follow_Up6</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Quote Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>TX,OK,AR,LA</value>
        </criteriaItems>
        <description>Quote Requested SW Region (AR, LA, OK, TX)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof Quote Requested- South</fullName>
        <actions>
            <name>Roof_Quote_Requested_South</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Quote Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Company_State__c</field>
            <operation>equals</operation>
            <value>TN,MS,AL,GA,SC,NC,SC,FL</value>
        </criteriaItems>
        <description>Quote Requested for TN, MS, AL, GA, SC, NC, SC, FL</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof Quote Requested- Wallace</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Quote_Follow_Up4</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Quote Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>AK,WA,OR,MT,ID,WY,UT,CO,NM</value>
        </criteriaItems>
        <description>Quote Requested for AK,WA,OR,MT,ID,WY,UT,CO,NM</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof Quote Requested- West coast</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Quote_Follow_Up2</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Quote Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Company_State__c</field>
            <operation>equals</operation>
            <value>NV,CA,AZ,HI</value>
        </criteriaItems>
        <description>Quote Requested for NV,CA,AZ,HI</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof Sample Requested -NE</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Sample Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Company_State__c</field>
            <operation>equals</operation>
            <value>VT,NH,ME,MA,RI,CT</value>
        </criteriaItems>
        <description>Quote Requested VT,NH,ME,MA,RI,CT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Sample_Request_Follow_Up_NE</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Contact.LastModifiedDate</offsetFromField>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Roof Sample Requested MidWest</fullName>
        <actions>
            <name>Sample_Requested_Follow_Up_MW</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Sample Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>ND,SD,NE,KS,MN,IA,MO,WI,IL,MI,IN,OH,KY</value>
        </criteriaItems>
        <description>Quote Requested for Contacts Object for ND,SD,NE,KS,MN,IA,MO,WI,IL,MI,IN,OH,KY</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Contact.LastModifiedDate</offsetFromField>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Roof Sample Requested West coast</fullName>
        <actions>
            <name>Sample_Requested_West</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Sample Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Company_State__c</field>
            <operation>equals</operation>
            <value>NV,CA,AZ,HI</value>
        </criteriaItems>
        <description>Quote Requested for NV,CA,AZ,HI</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Contact.pi__last_activity__c</offsetFromField>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Roof Sample Requested- East</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Sample Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>NY,PA,NJ,DE,MD,VA,WV,MEXICO</value>
        </criteriaItems>
        <description>Quote Requested for NY,PA,NJ,DE,MD,VA,WV,MEXICO</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Sample_Request_Follow_Up_E</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Contact.LastModifiedDate</offsetFromField>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Roof Sample Requested- SW Region</fullName>
        <actions>
            <name>Sample_Requested_Follow_Up_SW</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Sample Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>TX,OK,AR,LA</value>
        </criteriaItems>
        <description>Quote Requested SW Region (AR, LA, OK, TX)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Contact.pi__last_activity__c</offsetFromField>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Roof Sample Requested- South</fullName>
        <actions>
            <name>Sample_Requested_South</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Sample Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Company_State__c</field>
            <operation>equals</operation>
            <value>TN,MS,AL,GA,SC,NC,SC,FL</value>
        </criteriaItems>
        <description>Quote Requested for TN, MS, AL, GA, SC, NC, SC, FL</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Contact.pi__last_activity__c</offsetFromField>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Roof Sample Requested- Wallace</fullName>
        <actions>
            <name>Sample_Request_Follow_Up_MR</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Sample Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>AK,WA,OR,MT,ID,WY,UT,CO,NM</value>
        </criteriaItems>
        <description>Quote Requested for AK,WA,OR,MT,ID,WY,UT,CO,NM</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Contact.pi__last_activity__c</offsetFromField>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Roofing Contact</fullName>
        <actions>
            <name>Update_Roofing_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OR(
CreatedBy.ProfileId= &quot;00e40000000jlWp&quot;,
CreatedBy.ProfileId =&quot;00e40000000jlhs&quot;,
CreatedBy.ProfileId =&quot;00e40000000jmAL&quot;,
LastModifiedBy.ProfileId= &quot;00e40000000jlWp&quot;,
LastModifiedBy.ProfileId =&quot;00e40000000jlhs&quot;,
LastModifiedBy.ProfileId =&quot;00e40000000jmAL&quot;,
OwnerId = &quot;005400000019gXr&quot;,
OwnerId = &quot;005400000019LiV&quot; ,
OwnerId = &quot;005400000019LXl&quot; ,
OwnerId = &quot;005400000019Lic&quot; ,
OwnerId = &quot;005400000019Lae&quot; ,
OwnerId = &quot;005400000019Lgw&quot; ,
OwnerId = &quot;005400000019LgK&quot; ,
OwnerId = &quot;005400000019Xcx&quot; ,
OwnerId = &quot;005400000019LcG&quot; ,
OwnerId = &quot;005400000019LcE&quot; ,
OwnerId = &quot;005400000019LiP&quot; ,
OwnerId = &quot;005400000019Xcy&quot; ,
OwnerId = &quot;005400000019LjN&quot; ,
OwnerId = &quot;005400000019LiB&quot; ,
OwnerId = &quot;005400000019Lcs&quot; ,
OwnerId = &quot;005400000019Lc6&quot; ,
OwnerId = &quot;005400000019Lc8&quot; ,
OwnerId = &quot;005400000019Lgx&quot; ,
OwnerId = &quot;005400000019LcF&quot; ,
OwnerId = &quot;005400000019Xcz&quot; ,
OwnerId = &quot;005400000019LbE&quot; ,
OwnerId = &quot;005400000019LhB&quot; ,
OwnerId = &quot;005400000019Lgy&quot; ,
OwnerId = &quot;005400000019Lbn&quot; ,
OwnerId = &quot;005400000019LcR&quot; ,
OwnerId = &quot;00540000001mHG1&quot; ,
OwnerId = &quot;005400000019LXl&quot; ,
OwnerId = &quot;005400000019dGn&quot; ,
OwnerId = &quot;005400000019gUY&quot; ,
OwnerId = &quot;005400000019gUi&quot; ,
OwnerId = &quot;005400000019gUn&quot; ,
OwnerId = &quot;005400000019gUs&quot; ,
OwnerId = &quot;00540000001mHG6&quot; ,
OwnerId = &quot;00540000001mHGB&quot; ,
OwnerId = &quot;00540000001mHGG&quot; ,
OwnerId = &quot;005400000019Lb3&quot; ,
OwnerId = &quot;005400000019LiO&quot; ,
OwnerId = &quot;005400000019Lct&quot; ,
OwnerId = &quot;00540000001mdTU&quot; ,
OwnerId = &quot;00540000001mx7y&quot; ,
OwnerId = &quot;005400000019Lid&quot; ,
OwnerId = &quot;005400000019dGi&quot; ,
OwnerId = &quot;005400000019LbD&quot; ,
OwnerId = &quot;005400000019LcY&quot; ,
OwnerId = &quot;005400000019dGs&quot; ,
OwnerId = &quot;005400000019Laa&quot; ,
OwnerId = &quot;005400000019LdL&quot; ,
OwnerId = &quot;005400000019LaW&quot; ,
OwnerId = &quot;00540000001mQoO&quot; ,
OwnerId = &quot;005400000019LiE&quot; ,
OwnerId = &quot;005400000019LaX&quot; ,
OwnerId = &quot;005400000019Lac&quot; ,
OwnerId = &quot;005400000019LhJ&quot; ,
OwnerId = &quot;005400000019LaQ&quot; ,
OwnerId = &quot;005400000019LiF&quot; ,
OwnerId = &quot;00540000001mb9D&quot; ,
OwnerId = &quot;005400000019LhH&quot; ,
OwnerId = &quot;005400000019LcB&quot; ,
OwnerId = &quot;005400000019LcC&quot; ,
OwnerId = &quot;005400000019U8L&quot; ,
OwnerId = &quot;005400000019Lbc&quot; ,
OwnerId = &quot;00540000001mx7x&quot; ,
OwnerId = &quot;005400000019Lad&quot; ,
OwnerId = &quot;005400000019Lba&quot; ,
OwnerId = &quot;005400000019LhX&quot; ,
OwnerId = &quot;005400000019Lc9&quot; ,
OwnerId = &quot;005400000019LaZ&quot; ,
OwnerId = &quot;005400000019LbS&quot; ,
OwnerId = &quot;005400000019Laf&quot; ,
OwnerId = &quot;005400000019Lb2&quot; ,
OwnerId = &quot;005400000019Lb4&quot; ,
OwnerId = &quot;00540000001mPwq&quot; ,
OwnerId = &quot;005400000019LbR&quot; ,
OwnerId = &quot;005400000019LbU&quot; ,
OwnerId = &quot;005400000019Lh8&quot; ,
OwnerId = &quot;005400000019Lbt&quot; ,
OwnerId = &quot;005400000019LcA&quot; ,
OwnerId = &quot;005400000019LhI&quot; ,
OwnerId = &quot;005400000019LiN&quot; ,
OwnerId = &quot;005400000019LaY&quot; ,
OwnerId = &quot;005400000019Lab&quot; ,
OwnerId = &quot;005400000019LhT&quot; ,
OwnerId = &quot;005400000019Lc2&quot; ,
OwnerId = &quot;005400000019LcD&quot; ,
OwnerId = &quot;00540000001mx7z&quot; ,
OwnerId = &quot;005400000019Lc4&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Roofing Contact Check</fullName>
        <actions>
            <name>Update_Roofing_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check Contact Record to see if it is owned or modified by Roofing TM User</description>
        <formula>ISPICKVAL ( LastModifiedBy.Target_Market__c, &quot;Roofing&quot; ) ||
ISPICKVAL ( Owner.Target_Market__c, &quot;Roofing&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing Make Outbound Call %28East%29</fullName>
        <actions>
            <name>Make_Outbound_Call_East</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (3 OR 2)</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Make Outbound Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>NY,PA,NJ,DE,MD,VA,WV,DC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.State1__c</field>
            <operation>equals</operation>
            <value>NY,PA,NJ,DE,MD,VA,WV,DC</value>
        </criteriaItems>
        <description>roof Eval request from Website (organic) for NY,PA,NJ,DE,MD,VA,WV,DC</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing Make Outbound Call - MidWest</fullName>
        <actions>
            <name>Make_Outbound_Call_MIdW</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (3 OR 2)</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Make OUtbound Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>ND,SD,NE,KS,MN,IA,MO,WI,IL,MI,IN,OH,KY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.State1__c</field>
            <operation>equals</operation>
            <value>ND,SD,NE,KS,MN,IA,MO,WI,IL,MI,IN,OH,KY</value>
        </criteriaItems>
        <description>ND,SD,NE,KS,MN,IA,MO,WI,IL,MI,IN,OH,KY</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing Make Outbound Call - NE</fullName>
        <actions>
            <name>Make_Outbound_Call_NE</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Make Outbound Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>VT,NH,ME,MA,RI,CT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Free_Eval_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>VT, NH, ME, MA, RI, CT (Organic)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing Make Outbound Call - SW</fullName>
        <actions>
            <name>Make_Outbound_Call_SW</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 3) AND 2</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Make Outbound Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>TX,OK,AR,LA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.State1__c</field>
            <operation>equals</operation>
            <value>TX,OK,AR,LA</value>
        </criteriaItems>
        <description>(AR, LA, OK, TX)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing Make Outbound Call Walkama</fullName>
        <actions>
            <name>Make_Outbound_Call_South</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Make Outbound Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>TN,NC,MS,AL,GA,SC,FL,PR,BS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.State1__c</field>
            <operation>equals</operation>
            <value>TN,NC,MS,AL,GA,SC,FL,PR</value>
        </criteriaItems>
        <description>TN,NC,MS,AL,GA,SC,FL,PR</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing Make Outbound Call- Wallace</fullName>
        <actions>
            <name>Make_Outbound_Call_MR</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Make Outbound Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>AK,WA,OR,MT,ID,WY,UT,CO,NM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.State1__c</field>
            <operation>equals</operation>
            <value>AK,WA,OR,MT,ID,WY,UT,CO,NM</value>
        </criteriaItems>
        <description>AK,WA,OR,MT,ID,WY,UT,CO,NM</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing Make Outbound Call- West coast</fullName>
        <actions>
            <name>Make_Outbound_Call_West_Coast</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Make Outbound Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Company_State__c</field>
            <operation>equals</operation>
            <value>NV,CA,AZ,HI</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.State1__c</field>
            <operation>equals</operation>
            <value>NV,CA,AZ,HI</value>
        </criteriaItems>
        <description>NV,CA,AZ,HI</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing PPC - NE</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>,Google Adwords</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>VT,NH,ME,MA,RI,CT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Free_Eval_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Free Roof Eval VT, NH, ME, MA, RI, CT (Organic)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing- Contact Us South</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Promo_Item_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_Us_Follow_up_TW</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Contact Us</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>TN,NC,MS,AL,GA,SC,FL,PR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Contact_Us_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Contact Us TN,NC,MS,AL,GA,SC,FL,PR</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing- Contact Us- MidWest</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Contact_Us_Follow_Up_CF</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Contact Us</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>ND,SD,NE,KS,MN,IA,MO,WI,IL,MI,IN,OH,KY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Contact_Us_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Midwest Contact Us Eloqua Form ND,SD,NE,KS,MN,IA,MO,WI,IL,MI,IN,OH,KY</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing- Contact Us- Mountain</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Contact_Us_Eloqua_Wallace</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Contact Us</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>AK,WA,OR,MT,ID,WY,UT,CO,NM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Contact_Us_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Contact US AK,WA,OR,MT,ID,WY,UT,CO,NM</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing- Contact Us- NE</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Contact_Us_Follow_up_Eloqua</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Contact Us</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>VT,NH,ME,MA,RI,CT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Contact_Us_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Contact us for New England Region VT,NH,ME,MA,RI,CT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing- Contact Us- SW Region</fullName>
        <actions>
            <name>Contact_Us_Eloqua_IS_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Contact_Us_Follow_Up_BT</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Contact Us</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>TX,OK,AR,LA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Contact_Us_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>SW Region (AR, LA, OK, TX)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing- Contact Us- West Coast</fullName>
        <actions>
            <name>Contact_Us_Follow_up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Contact Us</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>NV,CA,AZ,HI</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Contact_Us_Task_Fired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>West Coast Contact Us Eloqua Form NV,CA,AZ,HI</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Waterproofing  Contact Check</fullName>
        <actions>
            <name>Waterproofing_TM_Check_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check Contact Record to see if it is owned or modified by Waterproofing TM User</description>
        <formula>ISPICKVAL ( LastModifiedBy.Target_Market__c, &quot;Waterproofing&quot; ) ||
ISPICKVAL ( Owner.Target_Market__c, &quot;Waterproofing&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Bing_Yahoo_Follow_up_SW</fullName>
        <assignedTo>dansby.kim@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Bing/Yahoo Follow up</subject>
    </tasks>
    <tasks>
        <fullName>Bing_Yahoo_Follow_up_TM</fullName>
        <assignedTo>sarnafil.admin@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Bing/ Yahoo Follow up</subject>
    </tasks>
    <tasks>
        <fullName>Bing_Yahoo_Follow_up_WC</fullName>
        <assignedTo>yun.alan@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Bing/ Yahoo Follow up</subject>
    </tasks>
    <tasks>
        <fullName>Bing_yahoo_Follow_up_NW</fullName>
        <assignedTo>wallace.trudy@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Bing/yahoo Follow up</subject>
    </tasks>
    <tasks>
        <fullName>Bing_yahoo_Follow_up_South</fullName>
        <assignedTo>walkama.ellen@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Bing/yahoo Follow up</subject>
    </tasks>
    <tasks>
        <fullName>Chicago_Follow_up</fullName>
        <assignedToType>owner</assignedToType>
        <description>Every Day Building Envelope Follow Up</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Bay Area Event Follow up</subject>
    </tasks>
    <tasks>
        <fullName>Contact_Us_Eloqua_Wallace</fullName>
        <assignedTo>wallace.trudy@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Contact Us Follow Up- Eloqua</subject>
    </tasks>
    <tasks>
        <fullName>Contact_Us_Follow_Up_BT</fullName>
        <assignedTo>dansby.kim@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Contact Us Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Contact_Us_Follow_Up_CF</fullName>
        <assignedTo>saliy.anna@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Contact Us Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Contact_Us_Follow_Up_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Contact Us Follow Up Call</subject>
    </tasks>
    <tasks>
        <fullName>Contact_Us_Follow_up</fullName>
        <assignedTo>johnson.kevin@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Contact Us Follow up</subject>
    </tasks>
    <tasks>
        <fullName>Contact_Us_Follow_up_Eloqua</fullName>
        <assignedTo>bellico.william@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Contact Us Follow up</subject>
    </tasks>
    <tasks>
        <fullName>Contact_Us_Follow_up_TW</fullName>
        <assignedTo>walkama.ellen@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Contact Us Follow up</subject>
    </tasks>
    <tasks>
        <fullName>Contact_Us_Follow_up_WB</fullName>
        <assignedTo>bellico.william@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Contact Us Follow up</subject>
    </tasks>
    <tasks>
        <fullName>D1_Lead_Score_Achieved_Follow_Up_Call</fullName>
        <assignedTo>gildea.james@us.sika.com.portal</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Flooring Contractors achieve D1 score</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>D1-Lead Score Achieved - Follow Up Call</subject>
    </tasks>
    <tasks>
        <fullName>DFW_Building_Envelope_Lead</fullName>
        <assignedTo>fernald.brian@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>EveryDay Brochure Dwl Follow Up for Chicago</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>EveryDay Brochure Dwl Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>DFW_Follow_up_Building_Envelope</fullName>
        <assignedTo>munt.daniel@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>DFW Follow up Building Envelope</subject>
    </tasks>
    <tasks>
        <fullName>DFW_Follow_up_Flooring_Contact</fullName>
        <assignedTo>hogan.paulette@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>EveryDay Brochure Dwl and  Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>DFW_Follow_up_Roofing_Contact</fullName>
        <assignedTo>bellico.william@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>DFW- AIA/CEU Request</subject>
    </tasks>
    <tasks>
        <fullName>DFW_Follow_up_on_Concrete_Contacts</fullName>
        <assignedTo>Concrete_R50_South_Central_District_B2</assignedTo>
        <assignedToType>role</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>DFW-Follow up on Concrete Contacts</subject>
    </tasks>
    <tasks>
        <fullName>DFW_Follow_up_on_RSB_Contacts</fullName>
        <assignedTo>bohannon.kevin@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>DFW-Follow up on RSB Contacts</subject>
    </tasks>
    <tasks>
        <fullName>Downloaded_Four_Steps_to_Maintenance_Free_Roof_Whitepaper_and_Follow_up_Call</fullName>
        <assignedTo>sarnafil.admin@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Contact.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Downloaded Four Steps to Maintenance Free Roof Whitepaper and Follow-up Call</subject>
    </tasks>
    <tasks>
        <fullName>Downloaded_Four_Steps_to_Maintenance_Free_Roof_Whitepaper_and_Follow_up_Call_Bel</fullName>
        <assignedTo>sarnafil.admin@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Contact.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Downloaded Four Steps to Maintenance Free Roof Whitepaper and Follow-up Call-NE</subject>
    </tasks>
    <tasks>
        <fullName>Downloaded_Four_Steps_to_Maintenance_Free_Roof_Whitepaper_and_Follow_up_Call_Tru</fullName>
        <assignedTo>wallace.trudy@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Contact.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Downloaded Four Steps to Maintenance Free Roof Whitepaper and Follow-up Call-Trudy</subject>
    </tasks>
    <tasks>
        <fullName>Downloaded_Four_Steps_to_Maintenance_Free_Roof_Whitepaper_and_Follow_up_Call_Wal</fullName>
        <assignedTo>bellico.william@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Contact.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Downloaded Four Steps to Maintenance Free Roof Whitepaper and Follow-up Call-Walkama</subject>
    </tasks>
    <tasks>
        <fullName>Flooring_Lead_Score_Achieved</fullName>
        <assignedTo>hogan.paulette@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Flooring_Lead Score Achieved - Follow Up Call</subject>
    </tasks>
    <tasks>
        <fullName>Free_Floor_Eval_from_Website</fullName>
        <assignedTo>hogan.paulette@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Free Floor or Design Kit Request</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Free Floor or Design Kit from Website</subject>
    </tasks>
    <tasks>
        <fullName>Free_Roof_Eval_Follow_Up</fullName>
        <assignedTo>johnson.kevin@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Free Roof Eval Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Free_Roof_Evaluation_Follo_Up_CF</fullName>
        <assignedTo>saliy.anna@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Free Roof Evaluation Follow Up Requested from Website</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Free Roof Evaluation Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Free_Roof_Evaluation_Follow_Up_BB</fullName>
        <assignedTo>bellico.william@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Free Roof Evaluation Follow Up for New England Leads - Roofing</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Free Roof Evaluation Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Free_Roof_Evaluation_Follow_Up_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Free Roof Evaluation Follow Up Call</subject>
    </tasks>
    <tasks>
        <fullName>Free_Roof_Evaluation_Follow_up_BT</fullName>
        <assignedTo>dansby.kim@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Free Roof Evaluation Follow up</subject>
    </tasks>
    <tasks>
        <fullName>Free_Roof_Evaluation_Follow_up_SM</fullName>
        <assignedTo>yun.alan@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Free Roof Evaluation Follow up</subject>
    </tasks>
    <tasks>
        <fullName>Free_Roof_Evaluation_Follow_up_TW</fullName>
        <assignedTo>wallace.trudy@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Free Roof Evaluation Follow up</subject>
    </tasks>
    <tasks>
        <fullName>Free_Roof_Evaluation_follow_Up_EW</fullName>
        <assignedTo>walkama.ellen@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Free Roof Evaluation follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Free_Roof_Follow_Up</fullName>
        <assignedTo>yun.alan@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Free Roof Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Google_Adwords_Follow_up_BT</fullName>
        <assignedTo>dansby.kim@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>PPC Adwords Follow up</subject>
    </tasks>
    <tasks>
        <fullName>Google_Adwords_Follow_up_EW</fullName>
        <assignedTo>walkama.ellen@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>PPC  Adwords Follow up</subject>
    </tasks>
    <tasks>
        <fullName>Google_Adwords_Follow_up_SM</fullName>
        <assignedTo>johnson.kevin@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>PPC Follow up</subject>
    </tasks>
    <tasks>
        <fullName>Lead_Score_Achieved</fullName>
        <assignedTo>bellico.william@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Lead Score Achieved - Follow Up Call</subject>
    </tasks>
    <tasks>
        <fullName>Lead_Score_Achieved_BT</fullName>
        <assignedTo>dansby.kim@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Lead Score Achieved - Follow Up Call</subject>
    </tasks>
    <tasks>
        <fullName>Lead_Score_Achieved_EW</fullName>
        <assignedTo>walkama.ellen@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Lead Score Achieved - Follow Up Call</subject>
    </tasks>
    <tasks>
        <fullName>Lead_Score_Achieved_Falcone</fullName>
        <assignedTo>saliy.anna@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Lead Score Achieved - Follow Up Call</subject>
    </tasks>
    <tasks>
        <fullName>Lead_Score_Achieved_Follow_Up_Call</fullName>
        <assignedTo>bellico.william@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Lead Score Achieved - Follow Up Call</subject>
    </tasks>
    <tasks>
        <fullName>Lead_Score_Achieved_Mooney</fullName>
        <assignedTo>bellico.william@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Lead Score Achieved - Follow Up Call</subject>
    </tasks>
    <tasks>
        <fullName>Lead_Score_Achieved_TM</fullName>
        <assignedTo>bellico.william@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Lead Score Achieved - Follow Up Call</subject>
    </tasks>
    <tasks>
        <fullName>Lead_Score_Achieved_TW</fullName>
        <assignedTo>wallace.trudy@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Lead Score Achieved - Follow Up Call</subject>
    </tasks>
    <tasks>
        <fullName>Maintenace_Workbook_Delivered_and_Follow_up_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Maintenace Workbook Delivered and Follow up Call</subject>
    </tasks>
    <tasks>
        <fullName>Maintenance_Workbook_Delivery_and_Follow_up_Call_Bellico</fullName>
        <assignedTo>bellico.william@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Contact.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Maintenance Workbook Delivery and Follow-up Call-NE</subject>
    </tasks>
    <tasks>
        <fullName>Maintenance_Workbook_Delivery_and_Follow_up_Call_SW</fullName>
        <assignedTo>dansby.kim@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Contact.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Maintenance Workbook Delivery and Follow-up Call-SW</subject>
    </tasks>
    <tasks>
        <fullName>Maintenance_Workbook_Delivery_and_Follow_up_Call_Walkama</fullName>
        <assignedTo>walkama.ellen@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Contact.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Maintenance Workbook Delivery and Follow-up Call-Walkama</subject>
    </tasks>
    <tasks>
        <fullName>Maintenance_Workbook_Delivery_and_Follow_up_Call_Wallace</fullName>
        <assignedTo>wallace.trudy@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Contact.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Maintenance Workbook Delivery and Follow-up Call-Wallace</subject>
    </tasks>
    <tasks>
        <fullName>Make_Outbound_Call</fullName>
        <assignedTo>saliy.anna@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Make Outbound Call</subject>
    </tasks>
    <tasks>
        <fullName>Make_Outbound_Call_East</fullName>
        <assignedTo>walkama.ellen@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Make Outbound Call</subject>
    </tasks>
    <tasks>
        <fullName>Make_Outbound_Call_MIdW</fullName>
        <assignedTo>saliy.anna@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Make Outbound Call</subject>
    </tasks>
    <tasks>
        <fullName>Make_Outbound_Call_MR</fullName>
        <assignedTo>wallace.trudy@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Make Outbound Call_MR</subject>
    </tasks>
    <tasks>
        <fullName>Make_Outbound_Call_NE</fullName>
        <assignedTo>bellico.william@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Make Outbound Call for New England Region Contacts</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Make Outbound Call_NE</subject>
    </tasks>
    <tasks>
        <fullName>Make_Outbound_Call_NW</fullName>
        <assignedTo>wallace.trudy@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Make Outbound Call_NW</subject>
    </tasks>
    <tasks>
        <fullName>Make_Outbound_Call_SW</fullName>
        <assignedTo>dansby.kim@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Make Outbound Call_SW</subject>
    </tasks>
    <tasks>
        <fullName>Make_Outbound_Call_South</fullName>
        <assignedTo>walkama.ellen@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Make Outbound Call_South</subject>
    </tasks>
    <tasks>
        <fullName>Make_Outbound_Call_West_Coast</fullName>
        <assignedTo>johnson.kevin@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Make Outbound Call_West_Coast</subject>
    </tasks>
    <tasks>
        <fullName>PPC_ADwords_Follow_up_TM</fullName>
        <assignedTo>yun.alan@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Form Filled out requesting Free Roof Eval</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>PPC Adwords Follow up</subject>
    </tasks>
    <tasks>
        <fullName>PPC_Follow_Up_Mid</fullName>
        <assignedTo>saliy.anna@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>PPC Follow Up-Mid</subject>
    </tasks>
    <tasks>
        <fullName>Quote_Follow_Up</fullName>
        <assignedTo>bellico.william@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Quote Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Quote_Follow_Up1</fullName>
        <assignedTo>bellico.william@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Quote Follow Up for New England Leads</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Quote Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Quote_Follow_Up2</fullName>
        <assignedTo>johnson.kevin@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Quote Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Quote_Follow_Up4</fullName>
        <assignedTo>wallace.trudy@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Quote Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Quote_Follow_Up6</fullName>
        <assignedTo>dansby.kim@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Quote Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Quote_Follow_Up7</fullName>
        <assignedTo>saliy.anna@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Quote Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Roof_Quote_Requested_South</fullName>
        <assignedTo>walkama.ellen@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Roof Quote Requested (South)</subject>
    </tasks>
    <tasks>
        <fullName>Sample_Request_Follow_Up_E</fullName>
        <assignedTo>walkama.ellen@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Follow up on sample shipment. check with Brian Blaquiere on shipment</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Sample Request Follow Up (E)</subject>
    </tasks>
    <tasks>
        <fullName>Sample_Request_Follow_Up_MR</fullName>
        <assignedTo>wallace.trudy@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Sample Request Follow Up (MR)</subject>
    </tasks>
    <tasks>
        <fullName>Sample_Request_Follow_Up_NE</fullName>
        <assignedTo>spadoni.christiane@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Follow up on sample shipment. check with Brian Blaquiere on shipment</description>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Sample Request Follow Up (NE)</subject>
    </tasks>
    <tasks>
        <fullName>Sample_Requested_Follow_Up_MW</fullName>
        <assignedTo>saliy.anna@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Sample Requested Follow Up (MW)</subject>
    </tasks>
    <tasks>
        <fullName>Sample_Requested_Follow_Up_SW</fullName>
        <assignedTo>dansby.kim@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Sample Requested Follow Up (SW)</subject>
    </tasks>
    <tasks>
        <fullName>Sample_Requested_South</fullName>
        <assignedTo>walkama.ellen@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Sample Requested (South)</subject>
    </tasks>
    <tasks>
        <fullName>Sample_Requested_West</fullName>
        <assignedTo>johnson.kevin@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Sample Requested (West)</subject>
    </tasks>
    <tasks>
        <fullName>google_Adwords_Follow_up_TW</fullName>
        <assignedTo>wallace.trudy@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>PPC Follow up</subject>
    </tasks>
</Workflow>
