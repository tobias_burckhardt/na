<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CMP_ComplaintApprovedNotification</fullName>
        <description>Complaint Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CMP_ComplaintApproved</template>
    </alerts>
    <alerts>
        <fullName>CMP_ComplaintRejectedNotification</fullName>
        <description>Complaint Rejected Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CMP_ComplaintRejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>CMP_CommFinalPendingApprovalIdByOrg</fullName>
        <field>SikaCMP__PendingApprovalIdByOrg__c</field>
        <name>Comm Final Pending Approval ID By Org</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CMP_CommRecallPendingApprovalIdByOrg</fullName>
        <field>SikaCMP__PendingApprovalIdByOrg__c</field>
        <name>Comm Recall Pending Approval ID By Org</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CMP_CommRejectPendingApprovalIdByOrg</fullName>
        <field>SikaCMP__PendingApprovalIdByOrg__c</field>
        <name>Comm Reject Pending Approval ID By Org</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CMP_TechFinalPendingApprovalIdByOrg</fullName>
        <field>SikaCMP__PendingApprovalIdByOrg__c</field>
        <name>Tech Final Pending Approval ID By Org</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CMP_TechRecallPendingApprovalIdByOrg</fullName>
        <field>SikaCMP__PendingApprovalIdByOrg__c</field>
        <name>Tech Recall Pending Approval ID By Org</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CMP_TechRejectPendingApprovalIdByOrg</fullName>
        <field>SikaCMP__PendingApprovalIdByOrg__c</field>
        <name>Tech Reject Pending Approval ID By Org</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CMP_UpdateCommPendingApprovalIdByOrg</fullName>
        <field>SikaCMP__PendingApprovalIdByOrg__c</field>
        <formula>$Organization.Id</formula>
        <name>Update Comm Pending Approval ID By Org</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CMP_UpdateCommercialClosingDate</fullName>
        <field>SikaCMP__CommercialClosingDate__c</field>
        <formula>TODAY()</formula>
        <name>Update Commercial Closing Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CMP_UpdateCommercialStatus</fullName>
        <field>SikaCMP__StatusCommercialProcessing__c</field>
        <literalValue>Closed</literalValue>
        <name>Update Commercial Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CMP_UpdateTechPendingApprovalIdByOrg</fullName>
        <field>SikaCMP__PendingApprovalIdByOrg__c</field>
        <formula>$Organization.Id</formula>
        <name>Update Tech Pending Approval ID By Org</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CMP_UpdateTechnicalClosingDate</fullName>
        <field>SikaCMP__TechnicalClosingDate__c</field>
        <formula>TODAY()</formula>
        <name>Update Technical Closing Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CMP_UpdateTechnicalStatus</fullName>
        <field>SikaCMP__StatusTechnical__c</field>
        <literalValue>Closed</literalValue>
        <name>Update Technical Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
