<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Configurations</fullName>
        <field>Configurations_from_Reprice_Quote__c</field>
        <formula>1</formula>
        <name>Configurations</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Quote_Expiration_Date</fullName>
        <field>Apttus_Proposal__Proposal_Expiration_Date__c</field>
        <formula>CreatedDate + 180</formula>
        <name>Populate Quote Expiration Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Master_Quote</fullName>
        <field>Is_Master_Quote__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Master Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_field</fullName>
        <field>Apttus_Proposal__Proposal_Expiration_Date__c</field>
        <formula>Manager_Expiration_Date_Extension__c</formula>
        <name>Update field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Manager Expiration Date Extension</fullName>
        <actions>
            <name>Update_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ischanged(Manager_Expiration_Date_Extension__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Master Quote Clone</fullName>
        <actions>
            <name>Uncheck_Master_Quote</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.APTS_Is_Clone__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.Is_Master_Quote__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Quote Expiration Date</fullName>
        <actions>
            <name>Populate_Quote_Expiration_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.Apttus_Proposal__Proposal_Expiration_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Reprice Update</fullName>
        <actions>
            <name>Configurations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>BU Roofing</description>
        <formula>Not ( Isblank( LastModifiedDate ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
