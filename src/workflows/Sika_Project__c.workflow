<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Check_Hide_From_Report</fullName>
        <description>Put a check intot he Hide From Report checkbox</description>
        <field>Hide_From_Report__c</field>
        <literalValue>1</literalValue>
        <name>Check Hide From Report</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Hide Sika Projects</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Sika_Project__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <description>This checks to see if the Status of a Sika Project is Completed or Cancelled and if so, it will check the Hide From Report field 10 days after it&apos;s set to Completed or Cancelled.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Check_Hide_From_Report</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
