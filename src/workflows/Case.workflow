<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CASE_RSB_Regional_Manager_Notify</fullName>
        <description>CASE - RSB Regional Manager Notify - NE</description>
        <protected>false</protected>
        <recipients>
            <recipient>estes.mike@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>walther.jim@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/RSB_Manager_Notify</template>
    </alerts>
    <alerts>
        <fullName>Case_Due_Date_Imminent_Notification_IT_Case</fullName>
        <description>Case Due Date Imminent Notification IT Case</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IT_Cases_Service_Cloud/Case_Due_Date_Notification_IT_Case</template>
    </alerts>
    <alerts>
        <fullName>Concrete_Case_Past_Due_Date</fullName>
        <description>Concrete Case Past Due Date Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Concrete</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Concrete_Case_Past_Due_Date_Notification</template>
    </alerts>
    <alerts>
        <fullName>Concrete_Case_Past_Due_Date_Notification</fullName>
        <description>Concrete Case Past Due Date Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Concrete</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Concrete_Case_Past_Due_Date_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_Customer_When_Case_is_Closed</fullName>
        <description>Email Alert to Customer When Case is Closed</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>us.technical.support@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Case_Closed_Customer_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_Customer_When_IT_Case_is_Reopened</fullName>
        <description>Email Alert to Customer When IT Case is Reopened</description>
        <protected>false</protected>
        <recipients>
            <field>Employee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>helpdeskus-it@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IT_Cases_Service_Cloud/Case_IT_Case_Reopen_Employee_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_Employee_When_Case_is_Created</fullName>
        <description>Email Alert to Employee When Case is Created</description>
        <protected>false</protected>
        <recipients>
            <field>Employee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>helpdeskus-it@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IT_Cases_Service_Cloud/IT_Case_Employee_Creation_Response</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_Employee_When_Case_is_Reopened</fullName>
        <description>Email Alert to Employee When Case is Reopened</description>
        <protected>false</protected>
        <recipients>
            <field>Employee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>helpdeskus-it@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IT_Cases_Service_Cloud/Case_IT_Case_Reopen_Employee_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_Employee_When_IT_Case_is_Closed</fullName>
        <description>Email Alert to Employee When IT Case is Closed</description>
        <protected>false</protected>
        <recipients>
            <field>Employee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>helpdeskus-it@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IT_Cases_Service_Cloud/Case_IT_Case_Closed_Employee_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_Non_Contact_Customer_When_Case_is_Created</fullName>
        <description>Email Alert to Non-Contact Customer When Case is Created</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sikarsb.service@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Customer_Service_Case_Creation_Response</template>
    </alerts>
    <alerts>
        <fullName>Email_Employee_when_Case_Ownership_Changes_to_User</fullName>
        <description>Email Employee when Case Ownership Changes to User</description>
        <protected>false</protected>
        <recipients>
            <field>Employee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>helpdeskus-it@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IT_Cases_Service_Cloud/Notify_Employee_on_Ownership_Change_to_IT_User</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_Case_Owner_when_Case_reopened</fullName>
        <description>Email alert to Case Owner when Case reopened</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Case_Reopen_Case_Owner</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_Case_Owner_when_IT_Case_reopened</fullName>
        <description>Email alert to Case Owner when IT Case reopened</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IT_Cases_Service_Cloud/Case_Reopen_IT_Case_Owner</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_customer_when_case_is_reopened</fullName>
        <description>Email Alert to Customer When Case is Reopened</description>
        <protected>false</protected>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>us.technical.support@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Case_Reopen_Customer_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Case_Owner_on_Case_Owner_Change</fullName>
        <description>Email to Case Owner on Case Owner Change</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Case_Owner_Change_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Case_Owner_on_IT_Case_Owner_Change</fullName>
        <description>Email to Case Owner on IT Case Owner Change</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IT_Cases_Service_Cloud/Case_Owner_Change_Notification_IT_Case</template>
    </alerts>
    <alerts>
        <fullName>EqpReq_Notify_Case_Owner_of_Completed_Tasks</fullName>
        <description>Equipment Request - Notify Case Owner of Completed Tasks</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>helpdeskus-it@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Employee_Request_Tasks_Complete</template>
    </alerts>
    <alerts>
        <fullName>Equipment_Request_Notify_Manager_of_Completed_Case</fullName>
        <description>Equipment Request - Notify Manager of Completed Case</description>
        <protected>false</protected>
        <recipients>
            <field>Employee_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>helpdeskus-it@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Employee_Manager_Closed</template>
    </alerts>
    <alerts>
        <fullName>Equipment_Request_Notify_Manager_of_New_Case</fullName>
        <description>Equipment Request - Notify Manager of New Case</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>helpdeskus-it@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Employee_Manager_Start</template>
    </alerts>
    <alerts>
        <fullName>Equipment_Request_Notify_Manager_to_review_equipment_request</fullName>
        <description>Equipment Request - Notify Manager to review equipment request</description>
        <protected>false</protected>
        <recipients>
            <field>Employee_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>helpdeskus-it@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Employee_Manager_Review</template>
    </alerts>
    <alerts>
        <fullName>FTS_Non_Priority_1</fullName>
        <description>FTS Non Priority 1</description>
        <protected>false</protected>
        <recipients>
            <recipient>barresi.joann@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>cariani.michael@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>fry.aaron@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pacheco.tom@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>spohn.darlene@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>urbina.cristian@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IT_Cases_Service_Cloud/FTS_NON_P1</template>
    </alerts>
    <alerts>
        <fullName>FTS_Priority_1_Alert</fullName>
        <ccEmails>5515804717@vtext.com</ccEmails>
        <ccEmails>2016002214@vtext.com</ccEmails>
        <ccEmails>2017040317@vtext.com</ccEmails>
        <ccEmails>2017076417@vtext.com</ccEmails>
        <ccEmails>2016932809@vtext.com</ccEmails>
        <description>FTS Priority 1 Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>cariani.michael@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pacheco.tom@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shah.jj@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>spohn.darlene@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>urbina.cristian@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IT_Cases_Service_Cloud/FTS_Priority_1_Notification</template>
    </alerts>
    <alerts>
        <fullName>Helpdesk_ADP_Alert</fullName>
        <ccEmails>panagos.lisa@us.sika.com</ccEmails>
        <description>Helpdesk ADP Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Employee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Outside_Case_Update</template>
    </alerts>
    <alerts>
        <fullName>Helpdesk_Concur_Alert</fullName>
        <description>Helpdesk Concur Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Employee_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>yarahmadi.pat@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IT_Service_Desk_Templates/Concur_Case_Update</template>
    </alerts>
    <alerts>
        <fullName>Helpdesk_PTS_Alert</fullName>
        <ccEmails>rios.randy@us.sika.com</ccEmails>
        <description>Helpdesk Roofing PTS Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Employee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Outside_Case_Update</template>
    </alerts>
    <alerts>
        <fullName>Helpdesk_SuccessFactors_Alert</fullName>
        <ccEmails>Franz.Karin@us.sika.com</ccEmails>
        <ccEmails>cece.susan@us.sika.com</ccEmails>
        <ccEmails>Frangos.Theresa@us.sika.com</ccEmails>
        <description>Helpdesk Success Factors Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Employee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Outside_Case_Update</template>
    </alerts>
    <alerts>
        <fullName>High_Priority_Concrete_Case_Action</fullName>
        <description>Concrete Case Creation</description>
        <protected>false</protected>
        <recipients>
            <recipient>Concrete</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/High_Priority_Concrete_Case_Notification</template>
    </alerts>
    <alerts>
        <fullName>IT_Case_Past_Due_Date_Notification</fullName>
        <description>IT Case Past Due Date Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IT_Cases_Service_Cloud/IT_Case_Past_Due_Date_Notification</template>
    </alerts>
    <alerts>
        <fullName>Iphone_Closed_Case_Incident_Create</fullName>
        <ccEmails>mjestes@gmail.com</ccEmails>
        <description>Iphone Closed Case - Incident Create</description>
        <protected>false</protected>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Case_Closed_Customer_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_Claims_Manager_of_New_Investigation_Notes</fullName>
        <description>Notification_to_Claims_Manager_of_New_Investigation_Notes</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/CA_Notification_to_Claims_Manager_of_Investigation_Findings</template>
    </alerts>
    <alerts>
        <fullName>NotifyCaseOwnerWhenCaseDueDateIsImminent</fullName>
        <description>Case Due Date Imminent Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Case_Due_Date_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_CA_Case_Team_of_Investigative_Findings</fullName>
        <description>Notify CA Case Team of Investigative Findings</description>
        <protected>false</protected>
        <recipients>
            <recipient>CA Cust Complaint Case Team</recipient>
            <type>caseTeam</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Notice_to_CA_Case_Team_of_Investigative_Findings</template>
    </alerts>
    <alerts>
        <fullName>Notify_CA_Case_Team_of_Update_to_case_comments</fullName>
        <description>Notify CA Case Team of Update to Case Comments</description>
        <protected>false</protected>
        <recipients>
            <recipient>CA Cust Complaint Case Team</recipient>
            <type>caseTeam</type>
        </recipients>
        <senderAddress>sfdc_no_reply@ca.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Notice_to_CA_Case_Team_of_Case_Comment</template>
    </alerts>
    <alerts>
        <fullName>Notify_CA_Claims_Manager_of_Cost_Rejection</fullName>
        <description>Notify CA Claims Manager of Cost Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Notify_CA_Claims_Manager_of_Cost_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Notify_CA_Cust_Complaint_of_Case</fullName>
        <description>Notify CA Cust Complaint of Case</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Notice_to_CA_Customer_Complainant_English</template>
    </alerts>
    <alerts>
        <fullName>Notify_CA_Cust_Complaint_of_Case_French</fullName>
        <description>Notify CA Cust Complaint of Case French</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Customer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Notice_to_CA_Customer_Complainant_French</template>
    </alerts>
    <alerts>
        <fullName>Notify_CA_Cust_Complaint_representative_of_Case_rejection</fullName>
        <description>Notify CA Cust Complaint representative of Case rejection</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CA_Sales_Person__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Rejection_Notification_to_CA_Cust_Case_Rep</template>
    </alerts>
    <alerts>
        <fullName>Notify_CA_Editor_to_Attach_Photos_or_Docs</fullName>
        <description>Notify CA Editor to Attach Photos or Docs</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/CA_Attach_Photo_or_Doc_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Notify_CA_Finance_of_CCR_Compensation</fullName>
        <description>Notify CA Finance of CCR Compensation</description>
        <protected>false</protected>
        <recipients>
            <recipient>CA_Finance_Contacts</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Notice_of_CA_Cost_Approval</template>
    </alerts>
    <alerts>
        <fullName>Notify_CA_Regional_CSR_Contact_of_Cost_Approval</fullName>
        <ccEmails>liscouet.stephanie@ca.sika.com,</ccEmails>
        <ccEmails>Amatucci.Valentina@ca.sika.com</ccEmails>
        <description>Notify CA Regional CSR Contact of Cost Approval</description>
        <protected>false</protected>
        <recipients>
            <field>Regional_CSR_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Notice_of_CA_Cost_Approval</template>
    </alerts>
    <alerts>
        <fullName>Notify_Case_Contributors_of_CA_Cust_Complaint_Case_Closed</fullName>
        <description>Notify Case Contributors of CA Cust Complaint Case Closed</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CA_Sales_Person__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/CA_Complaint_Case_Close_Email</template>
    </alerts>
    <alerts>
        <fullName>Notify_IT_Case_Owner_of_Modifications</fullName>
        <description>Notify IT Case Owner of Modifications</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Case_NON_OWNER_MODIFICATION_NOTIFICATION</template>
    </alerts>
    <alerts>
        <fullName>Notify_Owner_of_Escalation</fullName>
        <description>Notify Owner of Escalation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IT_Cases_Service_Cloud/IT_Case_Non_Owner_Escalation</template>
    </alerts>
    <alerts>
        <fullName>Notify_Owner_of_Status_Update</fullName>
        <description>Notify Owner of Status Update</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IT_Cases_Service_Cloud/IT_Case_Non_Owner_Status_Update</template>
    </alerts>
    <alerts>
        <fullName>Notify_Owner_on_Case_Due_Date_Change</fullName>
        <description>Notify Owner on Case Due Date Change</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Case_Due_Date_Change_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Owner_on_IT_Case_Due_Date_Change</fullName>
        <description>Notify Owner on IT Case Due Date Change</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IT_Cases_Service_Cloud/IT_Case_Due_Date_Change_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_RoofPro_Applicant_of_Approval</fullName>
        <description>Notify RoofPro Applicant of Approval</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>RoofPro_Applicator_Approvers</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>us.technical.support@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Notify_RoofPro_Applicant_of_Final_Approval</template>
    </alerts>
    <alerts>
        <fullName>Notify_RoofPro_Approvers_of_Rejection</fullName>
        <description>Notify RoofPro Approvers of Rejection</description>
        <protected>false</protected>
        <recipients>
            <recipient>RoofPro_Applicator_Approvers</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>trifecta.service@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Notify_RoofPro_Approver_of_Final_Rejection</template>
    </alerts>
    <alerts>
        <fullName>RSBCommunityRequestResponse</fullName>
        <description>Email Alert to Customer When RSB Community Request is Created</description>
        <protected>false</protected>
        <recipients>
            <recipient>estes.mike@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>us.technical.support@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/RSB_Comm_Case_Creation_Response</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Warranty_Service_Request_Status_Change</fullName>
        <description>Roofing Warranty Service Request Status Change</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Roofing_Warranty_Service_Case_Status_Change</template>
    </alerts>
    <alerts>
        <fullName>Send_Due_Date_Notification_to_Concrete_Queue</fullName>
        <description>Send Due Date Notification to Concrete Group</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Concrete_Case_Past_Due_Date_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_emailreceived_notification_to_case_owner</fullName>
        <description>Send Email Received Notification to Case Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Case_Email_Received_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Case_Iphone_Info_and_Desc</fullName>
        <field>Iphone_Request_Full__c</field>
        <formula>Subject &amp; BR()&amp; 
&quot;Iphone Upgrade Request:&quot;&amp; 
BR()&amp; 
BR()&amp; 
&quot;Name: &quot;&amp; SuppliedName &amp;BR()&amp; 
&quot;Phone: &quot;&amp; SuppliedPhone &amp;BR()&amp; 
&quot;Email: &quot;&amp; SuppliedEmail&amp; BR()&amp; 
BR()&amp; 
Web_Street__c&amp; BR()&amp; 
Web_City__c&amp;&quot;, &quot;&amp;Web_State__c&amp;&quot; &quot;&amp;Web_Zip__c&amp;BR()&amp;BR()&amp; 
&quot;Accessories Requested:&quot;&amp;BR()&amp;BR()&amp; 
Description</formula>
        <name>Case Iphone Info and Desc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Count_Action</fullName>
        <field>Case_Update_Count__c</field>
        <formula>Case_Update_Count__c +1</formula>
        <name>Case Update Count Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Conga_Uncheck</fullName>
        <field>BU_Roofing_Warranty_Servicing_Documents__c</field>
        <literalValue>0</literalValue>
        <name>Conga Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Default_the_Customer_Email_Address</fullName>
        <description>Determines the default customer email address</description>
        <field>Customer_Email__c</field>
        <formula>IF( LEN(Contact.Email) &gt; 0, Contact.Email,  IF(LEN(SuppliedEmail)&gt;0,SuppliedEmail,  Customer_Email__c))</formula>
        <name>Default the Customer Email Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Due_Date_Update_Action</fullName>
        <field>Due_Date__c</field>
        <formula>Now() + 1</formula>
        <name>Due Date Update Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Approval_update_status_to_Approval</fullName>
        <field>Status</field>
        <literalValue>Approval</literalValue>
        <name>Final Approval, update status to Approva</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>First_Name</fullName>
        <field>Primary_Contact_First_Name__c</field>
        <formula>Contact.FirstName</formula>
        <name>First Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Last_Name</fullName>
        <field>Primary_Contact_Last_Name__c</field>
        <formula>Contact.LastName</formula>
        <name>Last Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Logical_Check</fullName>
        <field>Logicalis__c</field>
        <literalValue>1</literalValue>
        <name>Logical Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_Cost_Approved</fullName>
        <field>CA_Case_Cost_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Mark Cost Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_In_RoofPro_Approval</fullName>
        <field>RoofPro_Field_Updates_Complete__c</field>
        <literalValue>1</literalValue>
        <name>Mark In RoofPro Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_as_Approved</fullName>
        <field>CA_Case_Initial_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Mark as Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_still_Claims_Manager</fullName>
        <field>OwnerId</field>
        <lookupValue>CA_Claims_Manager</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner still Claims Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Phone</fullName>
        <field>Primary_Contact_Phone_Number__c</field>
        <formula>Contact.Phone</formula>
        <name>Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Preferred_Employee_Name_Update</fullName>
        <description>Update Case name with (Preferred) First Last</description>
        <field>Employee_Name__c</field>
        <formula>If( ISBLANK(Employee_GID__r.Preferred_Name__c), null, &quot;(&quot;&amp;Employee_GID__r.Preferred_Name__c&amp;&quot;) &quot;) &amp;
 Employee_GID__r.Legal_First_Name__c &amp; &quot; &quot; &amp;
 Employee_GID__r.Legal_Last_Name__c</formula>
        <name>Preferred Employee Name Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_status_to_New</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Reset status to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Customer_Email</fullName>
        <description>Update the Customer Email</description>
        <field>Customer_Email__c</field>
        <formula>Contact.Email</formula>
        <name>Update Customer Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_With_Claims_Manager</fullName>
        <field>Status</field>
        <literalValue>With Claims Manager</literalValue>
        <name>Update Status to &quot;With Claims Manager&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_case_status_With_Claims_Manager</fullName>
        <description>For CA Cust Complaint Cases, when a cost is rejected update claim status to &quot;With Claims Manager&quot;</description>
        <field>Status</field>
        <literalValue>With Claims Manager</literalValue>
        <name>Update case status &quot;With Claims Manager&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_iPhone_Request_to_Description</fullName>
        <field>Description</field>
        <formula>Subject &amp; BR()&amp; 
BR()&amp; 
&quot;Name: &quot;&amp; SuppliedName &amp;BR()&amp; 
&quot;Phone: &quot;&amp; SuppliedPhone &amp;BR()&amp; 
&quot;Email: &quot;&amp; SuppliedEmail &amp; BR()&amp; 
&quot;Comments: &quot;&amp;iPhone_Special_Requests_Comments__c&amp;BR()&amp;
BR()&amp; 
&quot;Ship-To Address:&quot;&amp; BR()&amp; 
Web_Street__c&amp; BR()&amp; 
Web_City__c&amp;&quot;, &quot;&amp;Web_State__c&amp;&quot; &quot;&amp;Web_Zip__c&amp;BR()&amp;BR()</formula>
        <name>Update iPhone Request to Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_owner_to_Final_Approval_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>RoofPro_Final_Review_Approval</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update owner to Final Approval Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_owner_to_Finance_Approval_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>RoofPro_Finance_Approval</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update owner to Finance Approval Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_owner_to_Sales_Approval_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>RoofPro_Applicator_Sales_Approval</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update owner to Sales Approval Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_to_Approval</fullName>
        <field>Status</field>
        <literalValue>Approval</literalValue>
        <name>Update status to Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_to_Denial</fullName>
        <field>Status</field>
        <literalValue>Denial</literalValue>
        <name>Update status to Denial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_to_Final_Approval</fullName>
        <field>Status</field>
        <literalValue>Final Approval Review</literalValue>
        <name>Update status to Final Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_to_Finance_Approval</fullName>
        <field>Status</field>
        <literalValue>Finance Approval</literalValue>
        <name>Update status to Finance Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_to_Sales_Approval</fullName>
        <field>Status</field>
        <literalValue>Sales Approval</literalValue>
        <name>Update status to Sales Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>User_Request_Status_Approved</fullName>
        <field>User_Account_Approval__c</field>
        <literalValue>Approved</literalValue>
        <name>User Request Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>User_Request_Status_InProcess</fullName>
        <field>User_Account_Approval__c</field>
        <literalValue>In Process</literalValue>
        <name>User Request Status InProcess</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>User_Request_Status_Rejected</fullName>
        <field>User_Account_Approval__c</field>
        <literalValue>Rejected</literalValue>
        <name>User Request Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>CongaWarrantyServiceDocuments</fullName>
        <apiVersion>45.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>Id</fields>
        <fields>Warranty_Servicing_Documents__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>sarnafil.admin@us.sika.com</integrationUser>
        <name>Conga</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>RoofPro_Applicator_Agreement_Response</fullName>
        <apiVersion>37.0</apiVersion>
        <description>automatically send email containing RoofPro Applicator Agreement form</description>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>Agreement_Document_Conga_Workflow__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>estes.mike@us.sika.com</integrationUser>
        <name>RoofPro Applicator Agreement Response</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>RoofPro_Applicator_Document_Response</fullName>
        <apiVersion>37.0</apiVersion>
        <description>Outbound message for Conga Workflow to send Sika RoofPro Applicator Applicants completed documents after they submit the forms via web page</description>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>Application_Document_Conga_Workflow__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>estes.mike@us.sika.com</integrationUser>
        <name>RoofPro Applicator Document Response</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>BU Roofing Warranty</fullName>
        <actions>
            <name>First_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Last_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.LastName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Roofing Warranty Service</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Closed Customer Notification</fullName>
        <actions>
            <name>Email_Alert_to_Customer_When_Case_is_Closed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Notify the customer when their case has been closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Created - Employee iPhone Request %28Web-to-Case%29</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>iPhone Upgrade Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Case Created - Employee iPhone Request (Web-to-Case)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Update Count Rule</fullName>
        <actions>
            <name>Case_Update_Count_Action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Rule to update the update count field on case records</description>
        <formula>ISCHANGED(LastModifiedDate)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Concrete Case Creation Rule</fullName>
        <actions>
            <name>High_Priority_Concrete_Case_Action</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Target_Market__c</field>
            <operation>equals</operation>
            <value>Concrete</value>
        </criteriaItems>
        <description>If a new case is created with a Target Market of Concrete, an email alert will be sent to the Concrete public group.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Concrete Case Past Due Date</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Target_Market__c</field>
            <operation>equals</operation>
            <value>Concrete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Technical Services Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Warranty User Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Send email alert to concrete public group when the due date of a case has been reached for open case with a Target Market of concrete</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Due_Date_Notification_to_Concrete_Queue</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Due_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Conga for Warranty Servicing Documents</fullName>
        <actions>
            <name>Conga_Uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CongaWarrantyServiceDocuments</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.BU_Roofing_Warranty_Servicing_Documents__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Default the Customer Email Address</fullName>
        <actions>
            <name>Default_the_Customer_Email_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CaseNumber</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Will be either the contact email address or, if that is absent, it will be the web email.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Derive Case Due Date</fullName>
        <actions>
            <name>Due_Date_Update_Action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Due_Date_migrated_date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Set Due Date if Blank</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Full Info Update</fullName>
        <actions>
            <name>Case_Iphone_Info_and_Desc</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>iphone request</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IT Case Past Due Date</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed/No Contact</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Internal IT Support</value>
        </criteriaItems>
        <description>Send email alert to owner when due date is being reached</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>IT_Case_Past_Due_Date_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Due_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>New Employee Case</fullName>
        <actions>
            <name>Preferred_Employee_Name_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Employee Name</description>
        <formula>NOT(ISBLANK( Employee_GID__c )) &amp;&amp;

(isnew() || ISCHANGED( Employee_GID__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Case Owner When Case Due Date is Imminent</fullName>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 4) AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Technical Services Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Warranty User Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Send email alert to case owner 24 hours to prior when the due date of a case has been reached</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>NotifyCaseOwnerWhenCaseDueDateIsImminent</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Due_Date__c</offsetFromField>
            <timeLength>-24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Notify Case Owner When Case Due Date is Imminent for IT Case</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Internal IT Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed/No Contact,Closed</value>
        </criteriaItems>
        <description>Send email alert to case owner 24 hours to prior when the due date of an IT case has been reached</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Due_Date_Imminent_Notification_IT_Case</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Due_Date__c</offsetFromField>
            <timeLength>-24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Notify Case Owner When Case is Reopened</fullName>
        <actions>
            <name>Email_alert_to_Case_Owner_when_Case_reopened</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Send an email to the case owner when their case is reopened by someone else</description>
        <formula>AND(  CreatedDate &lt; NOW() - 0.038, NOT(ISPICKVAL(Status, &quot;Closed&quot;)), ISPICKVAL(PRIORVALUE(Status),&quot;Closed&quot;),  LastModifiedById &lt;&gt; OwnerId )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Case Owner When New Email is Received</fullName>
        <actions>
            <name>Send_emailreceived_notification_to_case_owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Second workflow rule to send email to case owner when last email received field is updated. 
2/11/16 - MEs Added Exception to not send email on new cases. (New Case Notification will take care of it.)</description>
        <formula>(ISCHANGED( Last_Email_Text__c ) &amp;&amp;
Text(Origin) &lt;&gt; &quot;Email&quot;)
||
(ISCHANGED( Last_Email_Text__c ) &amp;&amp;
!ISBLANK(PRIORVALUE(Last_Email_Text__c))&amp;&amp;
Text(Origin) = &quot;Email&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Customer On Case Reopening</fullName>
        <actions>
            <name>Email_alert_to_customer_when_case_is_reopened</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Send an email to the customer when the case is reopened by someone else</description>
        <formula>AND(  CreatedDate &lt; NOW() - 0.038, NOT(ISPICKVAL(Status, &quot;Closed&quot;)), ISPICKVAL(PRIORVALUE(Status),&quot;Closed&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Employee on Case Ownership Change</fullName>
        <actions>
            <name>Email_Employee_when_Case_Ownership_Changes_to_User</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify Employee on Case Ownership Change - Only if owner is changed to a User, not a queue.</description>
        <formula>AND( 
ISCHANGED(OwnerId), 
LEFT(OwnerId, 3) = &apos;005&apos;,
RecordTypeId = &quot;01240000000QMsQ&quot;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify New Case Owner on Owner Change</fullName>
        <actions>
            <name>Email_to_Case_Owner_on_Case_Owner_Change</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>When a case is transferred to a new owner, notify the new owner</description>
        <formula>AND( ISCHANGED( OwnerId ),  LastModifiedById  &lt;&gt;  OwnerId )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Owner on Case Due Date Change</fullName>
        <actions>
            <name>Notify_Owner_on_Case_Due_Date_Change</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Alert the case owner if the due date on the case is changed</description>
        <formula>AND(OwnerId &lt;&gt; LastModifiedById, ISCHANGED(Due_Date__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update the Customer Email</fullName>
        <actions>
            <name>Update_Customer_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the contact on the record is added or changed, update the customer email address.</description>
        <formula>AND (ISCHANGED( ContactId))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>iPhone -Full Info Update</fullName>
        <actions>
            <name>Update_iPhone_Request_to_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>iPhone</value>
        </criteriaItems>
        <description>For Internal Support Record Type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
