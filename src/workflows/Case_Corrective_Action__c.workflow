<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_CCA_Investigator_of_new_CCA</fullName>
        <description>Notify CCA Investigator of new CCA</description>
        <protected>false</protected>
        <recipients>
            <field>Investigator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sika_Service_Cloud_Templates/Notification_to_CA_Investigator_of_New_Case_Corrective_Action</template>
    </alerts>
</Workflow>
