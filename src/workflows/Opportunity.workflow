<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_Fishbowl_Request</fullName>
        <description>Alert Fishbowl Request</description>
        <protected>false</protected>
        <recipients>
            <recipient>bellico.william@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>yun.alan@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>rfdialog@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Alert_Templates/Fishbowl_Assignment_Alert</template>
    </alerts>
    <alerts>
        <fullName>Approved_Notification</fullName>
        <description>Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Roofing_Sales_Person_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Credit_Split_Approved</template>
    </alerts>
    <alerts>
        <fullName>Credit_Split_Declined</fullName>
        <description>Credit Split Declined</description>
        <protected>false</protected>
        <recipients>
            <field>Roofing_Sales_Person_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Credit_Split_Declined</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_to_CSR_Team_to_update_SAP_Sales_Order</fullName>
        <description>Email notification to CSR Team to update SAP Sales Order</description>
        <protected>false</protected>
        <recipients>
            <field>CSR_Mail_Box__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Template/CA_Free_Sample_Request_CSR_Team</template>
    </alerts>
    <alerts>
        <fullName>Request_Approved</fullName>
        <description>Request Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Template/CA_Free_Sample_Requisition_Approved</template>
    </alerts>
    <alerts>
        <fullName>Request_rejected_by_Approver</fullName>
        <description>Request rejected by Approver</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Template/CA_Free_Sample_Approval_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Rmax_Opportunity_Specifier_Support_Rep_Assignment_Notification</fullName>
        <description>Rmax Opportunity Specifier Support Rep Assignment Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Specifier_Support_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Alert_Templates/Specfier_Support_Rep_Alert</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Award_NOA_Notification_external</fullName>
        <description>Roofing Award NOA Notification (external)</description>
        <protected>false</protected>
        <recipients>
            <field>Roofing_Primary_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Roofing_Award_NOA_Notification_External</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Award_NOA_Notification_internal</fullName>
        <description>Roofing Award NOA Notification (internal)</description>
        <protected>false</protected>
        <recipients>
            <field>Assistant_Roofing_Tech_Rep_Mgr__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Assistant_Tech_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_CSR__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Rep_Tech_Mgr__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Salesperson__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Tech_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Roofing_Award_NOA_Notification</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Early_Bird_Warranty_Request_Internal</fullName>
        <ccEmails>SarnafilEBW@us.sika.com</ccEmails>
        <description>Roofing Early Bird Warranty Request (Internal)</description>
        <protected>false</protected>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Roofing_Early_Bird_Warranty_Requested</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Early_Bird_Warranty_Submitted_External</fullName>
        <description>Roofing Early Bird Warranty Submitted (External)</description>
        <protected>false</protected>
        <recipients>
            <field>Roofing_Primary_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Roofing_Early_Bird_Warranty_Submitted</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Inspection_Requested</fullName>
        <description>Roofing Inspection Requested (internal)</description>
        <protected>false</protected>
        <recipients>
            <field>Assistant_Roofing_Tech_Rep_Mgr__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Assigned_Tech_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Assistant_Tech_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Rep_Tech_Mgr__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Tech_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Roofing_Inspection_Requested</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Inspection_Status_Update</fullName>
        <description>Roofing Inspection Status Update - Re-Inspection</description>
        <protected>false</protected>
        <recipients>
            <field>Roofing_Assigned_Tech_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Roofing_Assistant_Tech_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Roofing_Regional_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Assistant_Roofing_Tech_Rep_Mgr__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Assistant_Tech_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Rep_Tech_Mgr__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Roofing_Inspection_Status</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Inspection_Status_Update_Passed</fullName>
        <description>Roofing Inspection Status Update - Passed</description>
        <protected>false</protected>
        <recipients>
            <field>Roofing_Assigned_Tech_Rep_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Roofing_Assistant_Tech_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Tech_Mgr_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Assistant_Roofing_Tech_Rep_Mgr__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Assistant_Tech_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_CSR__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Rep_Tech_Mgr__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Roofing_Inspection_Status</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Inspection_Submitted</fullName>
        <description>Roofing Inspection Submitted (external)</description>
        <protected>false</protected>
        <recipients>
            <field>Roofing_Primary_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Roofing_Inspection_Submitted</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Quote_Request</fullName>
        <description>Roofing Quote Request Internal</description>
        <protected>false</protected>
        <recipients>
            <field>Roofing_Sales_Person_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Roofing_Quote_Request</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Quote_Request_External</fullName>
        <description>Roofing Quote Request External</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Roofing_Quote_Request_External</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Tech_Review_Requested</fullName>
        <description>Roofing Tech Review Requested (internal)</description>
        <protected>false</protected>
        <recipients>
            <field>Assistant_Roofing_Tech_Rep_Mgr__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Assigned_Tech_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Assistant_Tech_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Rep_Tech_Mgr__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Tech_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Roofing_Tech_Review_Requested</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Tech_Review_Status</fullName>
        <description>Roofing Tech Review Status (external)</description>
        <protected>false</protected>
        <recipients>
            <field>Roofing_Primary_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Roofing_Assigned_Tech_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_CSR__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Salesperson__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Tech_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Roofing_Tech_Review_Status</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Tech_Review_Submitted</fullName>
        <description>Roofing Tech Review Submitted (external)</description>
        <protected>false</protected>
        <recipients>
            <field>Roofing_Primary_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Roofing_Tech_Review_Submitted</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Warranted_Notification</fullName>
        <description>Roofing Warranted Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Roofing_Primary_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Primary_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Roofing_Warranty_Status</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Warranty_Requested</fullName>
        <ccEmails>SarnafilEBW@us.sika.com</ccEmails>
        <description>Roofing Warranty Requested (internal)</description>
        <protected>false</protected>
        <recipients>
            <field>Assistant_Roofing_Tech_Rep_Mgr__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Assistant_Tech_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Rep_Tech_Mgr__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Roofing_Salesperson__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Roofing_Warranty_Requested</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Warranty_Submitted</fullName>
        <description>Roofing Warranty Submitted (external)</description>
        <protected>false</protected>
        <recipients>
            <field>Roofing_Primary_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PTS_Templates/Roofing_Warranty_Submitted</template>
    </alerts>
    <alerts>
        <fullName>SSI_Turnkey_Notification</fullName>
        <description>SSI Turnkey Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>murphy.elizabeth@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>piccolo.don@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SSI_Turnkey_Notification</template>
    </alerts>
    <alerts>
        <fullName>SSI_Turnkey_Notification_False</fullName>
        <description>SSI Turnkey Notification False</description>
        <protected>false</protected>
        <recipients>
            <recipient>murphy.elizabeth@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>piccolo.don@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SSI_Turnkey_Notification_False</template>
    </alerts>
    <fieldUpdates>
        <fullName>Add_Current_Status_to_History</fullName>
        <description>Add Current Status Update to Sika Status History</description>
        <field>Sika_Status_History__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName &amp; &quot; -&quot; &amp; TEXT(today())&amp; &quot;:&quot;&amp; BR() &amp; Sika_Status__c &amp; BR()&amp; BR()&amp;  Sika_Status_History__c</formula>
        <name>Add Current Status to History</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BU_Roofing_Opportunity</fullName>
        <field>BURoofing_TrkFlg__c</field>
        <literalValue>1</literalValue>
        <name>BU Roofing Opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CNC_Status_Date_Update</fullName>
        <field>CNC_Last_Update__c</field>
        <formula>today()</formula>
        <name>CNC Status Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CNT_Status_Date_Update</fullName>
        <field>CNT_Last_Update__c</field>
        <formula>TODAY()</formula>
        <name>CNT Status Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Date</fullName>
        <description>When a job is closed, set the close date to Today</description>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Closed_Date_Today</fullName>
        <field>CloseDate</field>
        <formula>today ()</formula>
        <name>Closed Date Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DST_Status_Update_Date</fullName>
        <field>DST_Last_Update__c</field>
        <formula>today()</formula>
        <name>DST Status Update Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Education_Vertical</fullName>
        <field>Sika_Vertical__c</field>
        <literalValue>Education</literalValue>
        <name>Education Vertical</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Export_to_Fishbowl</fullName>
        <field>fishbookspro__FishbowlExport__c</field>
        <literalValue>1</literalValue>
        <name>Export to Fishbowl</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FBSS_2nd_Day_Air</fullName>
        <field>fishbookspro__Shipping_Service__c</field>
        <literalValue>2nd Day Air</literalValue>
        <name>FBSS - 2nd Day Air</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FBSS_2nd_Day_Air_AM</fullName>
        <field>fishbookspro__Shipping_Service__c</field>
        <literalValue>2nd Day Air A.M.</literalValue>
        <name>FBSS - 2nd Day Air AM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FBSS_3_Day_Select</fullName>
        <field>fishbookspro__Shipping_Service__c</field>
        <literalValue>3 Day Select</literalValue>
        <name>FBSS - 3 Day Select</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FBSS_Ground</fullName>
        <field>fishbookspro__Shipping_Service__c</field>
        <literalValue>Ground</literalValue>
        <name>FBSS - Ground</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FBSS_Next_Day_Air_Early_AM</fullName>
        <field>fishbookspro__Shipping_Service__c</field>
        <literalValue>Next Day Air Early A.M.</literalValue>
        <name>FBSS - Next Day Air Early AM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FBSS_Next_Day_Air_Saver</fullName>
        <field>fishbookspro__Shipping_Service__c</field>
        <literalValue>Next Day Air Saver</literalValue>
        <name>FBSS - Next Day Air Saver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FB_to_Shipping_Svc</fullName>
        <field>fishbookspro__Shipping_Service__c</field>
        <literalValue>Next Day Air</literalValue>
        <name>FB to Shipping Svc1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FLG_Status_Date_Update</fullName>
        <field>FLG_Last_Update__c</field>
        <formula>today()</formula>
        <name>FLG Status Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IND_Status_Update_Date</fullName>
        <field>IND_Last_Update__c</field>
        <formula>today()</formula>
        <name>IND Status Update Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_Closed_Won</fullName>
        <field>StageName</field>
        <literalValue>Closed Won</literalValue>
        <name>Mark Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Name_Sequence</fullName>
        <field>Name</field>
        <formula>LEFT( RecordType.Name , 3)+&apos; - &apos;+ text(YEAR( TODAY())) +&apos; - &apos;+ Account.Name</formula>
        <name>Opportunity Name Sequence</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ProjectStageFieldUpdateQuote</fullName>
        <field>StageName</field>
        <literalValue>With Quotes</literalValue>
        <name>Project Stage Field Update - Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Project_Bid_Date_to_Opp_Bid_Date</fullName>
        <description>Set Oppty Sika Bid Date equal to Related Project Bid Date</description>
        <field>Sika_Bid_Date__c</field>
        <formula>Project__r.BidDate__c</formula>
        <name>Project Bid Date to Opp Bid Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RFG_Opp_CS_Approval_Status_Approved</fullName>
        <field>Credit_Split_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>RFG Opp CS Approval Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RFG_Opp_CS_Approval_Status_Declined</fullName>
        <field>Credit_Split_Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>RFG Opp CS Approval Status Declined</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RFG_Status_Date_Update</fullName>
        <field>RFG_Last_Update__c</field>
        <formula>today()</formula>
        <name>RFG Status Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Roofing_Opp_Credit_Split_Approval_Status</fullName>
        <field>Credit_Split_Approval_Status__c</field>
        <literalValue>Sent for Approval</literalValue>
        <name>RFG Opp CS Approval Status Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sika_Cross_Sell_TRUE</fullName>
        <description>Mark Oppty Sika Cross Sell Check Box TRUE</description>
        <field>Sika_Cross_Sell__c</field>
        <literalValue>1</literalValue>
        <name>Sika Cross Sell TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sika_Status_Date_Update</fullName>
        <description>Change the Date of Last Status Update to Today</description>
        <field>Sika_Last_Update__c</field>
        <formula>today()</formula>
        <name>Sika Status Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sika_Status_Opp_Created</fullName>
        <description>Add &quot;Opportunity Created&quot; to Sika Status if Blank on Opp Creation</description>
        <field>Sika_Status__c</field>
        <formula>&quot;Opportunity Created&quot;</formula>
        <name>Sika Status Opp Created</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Specs_Dodge_Equals_Bidding_Lead</fullName>
        <description>Every time Primary Campaign Source is set to &quot;Specs - Dodge&quot; then it updates the IS Lead Developed field to Bidding.</description>
        <field>IS_Lead_Developed_Stage__c</field>
        <literalValue>Bidding</literalValue>
        <name>Specs Dodge Equals Bidding Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Specs_Dodge_Equals_No_HQL</fullName>
        <description>Whenever the Primary Campaign Source is set to &quot;Specs - Dodge&quot; then the job will be marked to &quot;NO&quot; for HQL Status.</description>
        <field>IS_HQL__c</field>
        <literalValue>No</literalValue>
        <name>Specs Dodge Equals No HQL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted</fullName>
        <description>Change Stage to Submitted</description>
        <field>StageName</field>
        <literalValue>Submitted</literalValue>
        <name>Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Un_check_Re_Generate_ANOA_on_Opp</fullName>
        <field>Re_Generate_ANOA__c</field>
        <literalValue>0</literalValue>
        <name>Un-check Re-Generate ANOA on Opp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_EcoSmart_Box</fullName>
        <field>EcoSmart_System__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck EcoSmart Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateSnapshotDate</fullName>
        <description>If GSK Status Snapshot Text is updated, change the Date / Time of the Last Update Field</description>
        <field>Last_GSK_Snapshot_Update__c</field>
        <formula>NOW()</formula>
        <name>Update Snapshot Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_as_Approved</fullName>
        <field>StageName</field>
        <literalValue>Approved</literalValue>
        <name>Update Stage as Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_as_Approved_and_Closed</fullName>
        <description>After the approval request has completed its approval process, CSR team will close the request.</description>
        <field>StageName</field>
        <literalValue>Approved &amp; Closed</literalValue>
        <name>Update Stage as Approved and Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_as_Approved_and_SO_Process</fullName>
        <field>StageName</field>
        <literalValue>Approved &amp; SO Pending</literalValue>
        <name>Update Stage as Approved and SO Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_as_rejected</fullName>
        <field>StageName</field>
        <literalValue>Rejected</literalValue>
        <name>Update Stage as rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Pending_Approval</fullName>
        <field>StageName</field>
        <literalValue>Pending For Approval</literalValue>
        <name>Update Stage to Pending Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_checkbox</fullName>
        <field>Inspection_Requested__c</field>
        <literalValue>1</literalValue>
        <name>Update checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updation_of_closed_date</fullName>
        <field>CloseDate</field>
        <formula>IF(MONTH( DATEVALUE( CreatedDate ))&lt;=6,DATE(YEAR(TODAY()), 12,30),  DATE(YEAR(TODAY())+1, 1,1))</formula>
        <name>Updation of closed date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WTP_Status_Date</fullName>
        <field>WTP_Last_Update__c</field>
        <formula>today()</formula>
        <name>WTP Status Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updateownerCanada</fullName>
        <field>OwnerId</field>
        <lookupValue>edler.mike@us.sika.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>update owner Canada</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updateownerInternational</fullName>
        <field>OwnerId</field>
        <lookupValue>dougherty.elizabeth@us.sika.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>update owner International</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updateownerMidSouth</fullName>
        <field>OwnerId</field>
        <lookupValue>edler.mike@us.sika.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>update owner- Mid South</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updateownerMidWest</fullName>
        <field>OwnerId</field>
        <lookupValue>dougherty.elizabeth@us.sika.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>update owner Mid West</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updateownerNorthEast</fullName>
        <field>OwnerId</field>
        <lookupValue>collins.dan@us.sika.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>update owner North East</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updateownerOhioValley</fullName>
        <field>OwnerId</field>
        <lookupValue>horn.kim@us.sika.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>update owner- Ohio Valley</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updateownerSouthEast</fullName>
        <field>OwnerId</field>
        <lookupValue>jenkins.angie@us.sika.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>update owner South East</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updateownerWestCoast</fullName>
        <field>OwnerId</field>
        <lookupValue>haley.craig@us.sika.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>update owner West Coast</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>EcoSmart_Conga_WF_Message</fullName>
        <apiVersion>43.0</apiVersion>
        <description>Generate EcoSmart Document</description>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>EcoSmart_Document_Conga_WF__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>bluewolf.admin@us.sika.com</integrationUser>
        <name>EcoSmart Conga WF Message</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>IS_Qualified_Planning_Opp</fullName>
        <apiVersion>33.0</apiVersion>
        <endpointUrl>https://powerstandings.insidesales.com/kpi/qualopp</endpointUrl>
        <fields>Id</fields>
        <fields>LastModifiedById</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>sarnafil.admin@us.sika.com</integrationUser>
        <name>IS Qualified Planning Opp</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Opportunity_ANOA_Conga_WF_Message</fullName>
        <apiVersion>37.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>Id</fields>
        <fields>Opportunity_ANOA_Conga_WF__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>bluewolf.admin@us.sika.com</integrationUser>
        <name>Opportunity ANOA Conga WF Message</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>EcoSmart Generation Conga WF</fullName>
        <actions>
            <name>Uncheck_EcoSmart_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>EcoSmart_Conga_WF_Message</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR (4 AND 5) OR (6 AND 7 AND 9)) AND 2 AND 3 AND 8</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Take_Action__c</field>
            <operation>equals</operation>
            <value>Tech Review - Accepted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Roofing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Re_Generate_ANOA__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Take_Action__c</field>
            <operation>equals</operation>
            <value>Warranted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Zero_Value_Warranty__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.EBW__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Take_Action__c</field>
            <operation>equals</operation>
            <value>Warranty Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.EcoSmart_System__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Count_Building_Groups_Approved__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FBSS - 2nd Day Air</fullName>
        <actions>
            <name>FBSS_2nd_Day_Air</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.FB_Shipping_Service__c</field>
            <operation>equals</operation>
            <value>2nd Day Air</value>
        </criteriaItems>
        <description>Shipping Svc to 2nd Day Air</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FBSS - 2nd Day Air AM</fullName>
        <actions>
            <name>FBSS_2nd_Day_Air_AM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.FB_Shipping_Service__c</field>
            <operation>equals</operation>
            <value>2nd Day Air A.M.</value>
        </criteriaItems>
        <description>Shipping Svc to 2nd Day Air AM</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FBSS - 3 Day Select</fullName>
        <actions>
            <name>FBSS_3_Day_Select</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.FB_Shipping_Service__c</field>
            <operation>equals</operation>
            <value>3 Day Select</value>
        </criteriaItems>
        <description>Shipping Svc to 3 Day Select</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FBSS - Ground</fullName>
        <actions>
            <name>FBSS_Ground</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.FB_Shipping_Service__c</field>
            <operation>equals</operation>
            <value>Ground</value>
        </criteriaItems>
        <description>Shipping Svc to Ground</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FBSS - Next Day Air</fullName>
        <actions>
            <name>FB_to_Shipping_Svc</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.FB_Shipping_Service__c</field>
            <operation>equals</operation>
            <value>Next Day Air</value>
        </criteriaItems>
        <description>Shipping Svc to Next Day Air</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FBSS - Next Day Air Early AM</fullName>
        <actions>
            <name>FBSS_Next_Day_Air_Early_AM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.FB_Shipping_Service__c</field>
            <operation>equals</operation>
            <value>Next Day Air Early A.M.</value>
        </criteriaItems>
        <description>Shipping Svc to Next Day Air Early AM</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FBSS - Next Day Air Saver</fullName>
        <actions>
            <name>FBSS_Next_Day_Air_Saver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.FB_Shipping_Service__c</field>
            <operation>equals</operation>
            <value>Next Day Air Saver</value>
        </criteriaItems>
        <description>Shipping Svc to Next Day Air Saver</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IND Status Date</fullName>
        <actions>
            <name>IND_Status_Update_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Date of Last Status Update</description>
        <formula>ISCHANGED( IND_Status__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>If Closed</fullName>
        <actions>
            <name>Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>What happens if a job is closed/</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inital Status Entry</fullName>
        <actions>
            <name>Sika_Status_Opp_Created</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Sika_Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Add Initial Entry if Sika Status is Blank.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity ANOA Conga WF</fullName>
        <actions>
            <name>Opportunity_ANOA_Conga_WF_Message</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR (4 AND 5) OR (6 AND 7 AND 8)) AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Take_Action__c</field>
            <operation>equals</operation>
            <value>Tech Review - Accepted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Roofing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Re_Generate_ANOA__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Take_Action__c</field>
            <operation>equals</operation>
            <value>Warranted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Zero_Value_Warranty__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.EBW__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Take_Action__c</field>
            <operation>equals</operation>
            <value>Warranty Requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Count_Building_Groups_Approved__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity ANOA Conga WF re-generate</fullName>
        <actions>
            <name>Un_check_Re_Generate_ANOA_on_Opp</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opportunity_ANOA_Conga_WF_Message</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Re_Generate_ANOA__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Roofing</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Qualified Opportunity Sent to Field</fullName>
        <actions>
            <name>IS_Qualified_Planning_Opp</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Sika_Sent_To_Field__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Qualified Opportunity sent to field by IS triggers KPI scoring in IS.com. Must be a &quot;Planning Lead.&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing Opportunity</fullName>
        <actions>
            <name>BU_Roofing_Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(
CreatedBy.ProfileId= &quot;00e40000000jlWp&quot;,
CreatedBy.ProfileId =&quot;00e40000000jlhs&quot;,
CreatedBy.ProfileId =&quot;00e40000000jmAL&quot;,
LastModifiedBy.ProfileId= &quot;00e40000000jlWp&quot;,
LastModifiedBy.ProfileId =&quot;00e40000000jlhs&quot;,
LastModifiedBy.ProfileId =&quot;00e40000000jmAL&quot;,
OwnerId = &quot;005400000019gXr&quot;,
OwnerId = &quot;005400000019LiV&quot; ,
OwnerId = &quot;005400000019LXl&quot; ,
OwnerId = &quot;005400000019Lic&quot; ,
OwnerId = &quot;005400000019Lae&quot; ,
OwnerId = &quot;005400000019Lgw&quot; ,
OwnerId = &quot;005400000019LgK&quot; ,
OwnerId = &quot;005400000019Xcx&quot; ,
OwnerId = &quot;005400000019LcG&quot; ,
OwnerId = &quot;005400000019LcE&quot; ,
OwnerId = &quot;005400000019LiP&quot; ,
OwnerId = &quot;005400000019Xcy&quot; ,
OwnerId = &quot;005400000019LjN&quot; ,
OwnerId = &quot;005400000019LiB&quot; ,
OwnerId = &quot;005400000019Lcs&quot; ,
OwnerId = &quot;005400000019Lc6&quot; ,
OwnerId = &quot;005400000019Lc8&quot; ,
OwnerId = &quot;005400000019Lgx&quot; ,
OwnerId = &quot;005400000019LcF&quot; ,
OwnerId = &quot;005400000019Xcz&quot; ,
OwnerId = &quot;005400000019LbE&quot; ,
OwnerId = &quot;005400000019LhB&quot; ,
OwnerId = &quot;005400000019Lgy&quot; ,
OwnerId = &quot;005400000019Lbn&quot; ,
OwnerId = &quot;005400000019LcR&quot; ,
OwnerId = &quot;00540000001mHG1&quot; ,
OwnerId = &quot;005400000019LXl&quot; ,
OwnerId = &quot;005400000019dGn&quot; ,
OwnerId = &quot;005400000019gUY&quot; ,
OwnerId = &quot;005400000019gUi&quot; ,
OwnerId = &quot;005400000019gUn&quot; ,
OwnerId = &quot;005400000019gUs&quot; ,
OwnerId = &quot;00540000001mHG6&quot; ,
OwnerId = &quot;00540000001mHGB&quot; ,
OwnerId = &quot;00540000001mHGG&quot; ,
OwnerId = &quot;005400000019Lb3&quot; ,
OwnerId = &quot;005400000019LiO&quot; ,
OwnerId = &quot;005400000019Lct&quot; ,
OwnerId = &quot;00540000001mdTU&quot; ,
OwnerId = &quot;00540000001mx7y&quot; ,
OwnerId = &quot;005400000019Lid&quot; ,
OwnerId = &quot;005400000019dGi&quot; ,
OwnerId = &quot;005400000019LbD&quot; ,
OwnerId = &quot;005400000019LcY&quot; ,
OwnerId = &quot;005400000019dGs&quot; ,
OwnerId = &quot;005400000019Laa&quot; ,
OwnerId = &quot;005400000019LdL&quot; ,
OwnerId = &quot;005400000019LaW&quot; ,
OwnerId = &quot;00540000001mQoO&quot; ,
OwnerId = &quot;005400000019LiE&quot; ,
OwnerId = &quot;005400000019LaX&quot; ,
OwnerId = &quot;005400000019Lac&quot; ,
OwnerId = &quot;005400000019LhJ&quot; ,
OwnerId = &quot;005400000019LaQ&quot; ,
OwnerId = &quot;005400000019LiF&quot; ,
OwnerId = &quot;00540000001mb9D&quot; ,
OwnerId = &quot;005400000019LhH&quot; ,
OwnerId = &quot;005400000019LcB&quot; ,
OwnerId = &quot;005400000019LcC&quot; ,
OwnerId = &quot;005400000019U8L&quot; ,
OwnerId = &quot;005400000019Lbc&quot; ,
OwnerId = &quot;00540000001mx7x&quot; ,
OwnerId = &quot;005400000019Lad&quot; ,
OwnerId = &quot;005400000019Lba&quot; ,
OwnerId = &quot;005400000019LhX&quot; ,
OwnerId = &quot;005400000019Lc9&quot; ,
OwnerId = &quot;005400000019LaZ&quot; ,
OwnerId = &quot;005400000019LbS&quot; ,
OwnerId = &quot;005400000019Laf&quot; ,
OwnerId = &quot;005400000019Lb2&quot; ,
OwnerId = &quot;005400000019Lb4&quot; ,
OwnerId = &quot;00540000001mPwq&quot; ,
OwnerId = &quot;005400000019LbR&quot; ,
OwnerId = &quot;005400000019LbU&quot; ,
OwnerId = &quot;005400000019Lh8&quot; ,
OwnerId = &quot;005400000019Lbt&quot; ,
OwnerId = &quot;005400000019LcA&quot; ,
OwnerId = &quot;005400000019LhI&quot; ,
OwnerId = &quot;005400000019LiN&quot; ,
OwnerId = &quot;005400000019LaY&quot; ,
OwnerId = &quot;005400000019Lab&quot; ,
OwnerId = &quot;005400000019LhT&quot; ,
OwnerId = &quot;005400000019Lc2&quot; ,
OwnerId = &quot;005400000019LcD&quot; ,
OwnerId = &quot;00540000001mx7z&quot; ,
OwnerId = &quot;005400000019Lc4&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sika Last Update - Update</fullName>
        <actions>
            <name>Sika_Status_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Change the Date of Sika Status Update to Today when record is new or status is changed.</description>
        <formula>ISNEW() 
 || 
ISCHANGED( Sika_Status__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sika Pricing - Opportunity Name Sequence</fullName>
        <actions>
            <name>Opportunity_Name_Sequence</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR(RecordType.DeveloperName=&apos;SPA_Pricing&apos;,RecordType.DeveloperName=&apos;SPQ_Pricing&apos;), Name !=null)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sika Status History</fullName>
        <actions>
            <name>Add_Current_Status_to_History</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISNEW()  ||  ISCHANGED( Sika_Status__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Specs Dodge or Specs Insight Equals Bidding Lead</fullName>
        <actions>
            <name>Specs_Dodge_Equals_Bidding_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.CampaignId</field>
            <operation>equals</operation>
            <value>Roofing Specs - Dodge</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CampaignId</field>
            <operation>equals</operation>
            <value>Roofing Specs - Insight</value>
        </criteriaItems>
        <description>When Primary Campaign Source field is &quot;Roofing Specs - Dodge&quot; or &quot;Roofing Specs - Insight&quot; then IS Lead Developed Stage field will be &quot;Bidding&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Specs Dodge or Specs Insight Equals No HQL</fullName>
        <actions>
            <name>Specs_Dodge_Equals_No_HQL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.CampaignId</field>
            <operation>equals</operation>
            <value>Roofing Specs - Dodge</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CampaignId</field>
            <operation>equals</operation>
            <value>Roofing Specs - Insight</value>
        </criteriaItems>
        <description>Whenever the Primary Campaign Source is set to &quot;Roofing Specs - Dodge&quot; or &quot;Roofing Specs - Insight&quot; then the job will be marked to &quot;NO&quot; for HQL Status.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Blank Bid Date with Project Bid Date</fullName>
        <actions>
            <name>Project_Bid_Date_to_Opp_Bid_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Oppty Sika Bid Date is Blank, Update Blank Bid Date with Project Bid Date</description>
        <formula>ISNULL( Sika_Bid_Date__c)  &amp;&amp; NOT(ISNULL( Project__r.BidDate__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Inspection Requested</fullName>
        <actions>
            <name>Update_checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Take_Action__c</field>
            <operation>equals</operation>
            <value>Inspection - Passed with Punch List,Inspection - Passed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Inspection_Requested__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>BU Roofing - catch all for inspections requested</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>close date should be end of the year for  pricing</fullName>
        <actions>
            <name>Updation_of_closed_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( RecordType.DeveloperName=&apos;SPA_Pricing&apos;,RecordType.DeveloperName=&apos;SPQ_Pricing&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
