<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ApprovedtoNO</fullName>
        <description>Set Approved Checkbox to False</description>
        <field>Approved__c</field>
        <literalValue>0</literalValue>
        <name>ApprovedtoNO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PriceList_Approved</fullName>
        <description>Update PriceList Status if Approved</description>
        <field>Status__c</field>
        <literalValue>Active</literalValue>
        <name>PriceList Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PriceList_Approved_Checkbox</fullName>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>PriceList Approved Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PriceList_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>PriceList Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Planning</fullName>
        <description>Set Status back to Planning</description>
        <field>Status__c</field>
        <literalValue>Planning</literalValue>
        <name>Status to Planning</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_for_Approval</fullName>
        <description>Update Status to Revew</description>
        <field>Status__c</field>
        <literalValue>Review</literalValue>
        <name>Submitted for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>New Agreement Setup</fullName>
        <actions>
            <name>ApprovedtoNO</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Status_to_Planning</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 or 2</booleanFilter>
        <criteriaItems>
            <field>Price_Agreement__c.Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Price_Agreement__c.Status__c</field>
            <operation>equals</operation>
            <value>Active,Cancelled,Review,Rejected,Expired</value>
        </criteriaItems>
        <description>Set Status and Approved back to Start for Cloned Agreements</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
