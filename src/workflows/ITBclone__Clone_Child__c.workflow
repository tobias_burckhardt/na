<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ITBclone__Update_Unique_Parent</fullName>
        <field>ITBclone__Parent_Unique_Name__c</field>
        <formula>TEXT( ITBclone__Parent_Object__c )</formula>
        <name>Update Unique Parent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ITBclone__Update Unique Parent</fullName>
        <actions>
            <name>ITBclone__Update_Unique_Parent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Ensures uniqueness of parent object name</description>
        <formula>OR(ISNEW(), ISCHANGED( ITBclone__Parent_Object__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
