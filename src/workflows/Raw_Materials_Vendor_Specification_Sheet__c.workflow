<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Raw_Materials_Ready_for_Signature_Email_Alert</fullName>
        <description>Raw Materials Ready for Signature Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>finan.barbara@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rusignuolo.j@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>R_D_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Raw_Materials/Raw_Materials_Ready_for_Signature_ET</template>
    </alerts>
    <fieldUpdates>
        <fullName>CongaRMVSSstart_Reset</fullName>
        <description>Reset Checkbox to False</description>
        <field>CongaRMVSSWorkflowStart__c</field>
        <literalValue>0</literalValue>
        <name>CongaRMVSSstart Reset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RMVSS_Out_for_Signature</fullName>
        <field>Status__c</field>
        <literalValue>Out for Signature</literalValue>
        <name>RMVSS Out for Signature</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RMVSS_Out_for_Signature_Status_Update</fullName>
        <field>Status__c</field>
        <literalValue>Out for Signature</literalValue>
        <name>RMVSS Out for Signature Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RMVSS_Rejected_Status_Update</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>RMVSS Rejected Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RMVSS_Signed_Status_Update</fullName>
        <field>Status__c</field>
        <literalValue>Signed</literalValue>
        <name>RMVSS Signed Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RMVSS_Voided_Status_Update</fullName>
        <field>Status__c</field>
        <literalValue>Voided</literalValue>
        <name>RMVSS Voided Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Raw_Material_Revise_Specifications</fullName>
        <description>Unchecks specifications complete</description>
        <field>Specifications_Complete__c</field>
        <literalValue>0</literalValue>
        <name>Raw Material Revise Specifications</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Raw_Materials_Ready_for_Signature_FU</fullName>
        <description>Status field update triggered when Specifications Complete and Vendor information is filled out</description>
        <field>Status__c</field>
        <literalValue>Ready for Signature</literalValue>
        <name>Raw Materials Ready for Signature Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Raw_Materials_Revise</fullName>
        <description>Updates revision number</description>
        <field>Revision__c</field>
        <formula>Revision__c+1</formula>
        <name>Raw Materials Revise #</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Raw_Materials_Revise_Status</fullName>
        <description>Updates Status field to Revision</description>
        <field>Status__c</field>
        <literalValue>Revision</literalValue>
        <name>Raw Materials Revise Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Raw_Materials_Uncheck_Revise</fullName>
        <description>Unchecks revise</description>
        <field>Revise__c</field>
        <literalValue>0</literalValue>
        <name>Raw Materials Uncheck Revise</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Conga_RMVSS_Workflow</fullName>
        <apiVersion>42.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>CongaRMVSS_URL__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>estes.mike@us.sika.com</integrationUser>
        <name>Conga RMVSS Workflow</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Raw Mat Send RMVSS Conga CheckBox Start</fullName>
        <actions>
            <name>CongaRMVSSstart_Reset</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Conga_RMVSS_Workflow</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Raw_Materials_Vendor_Specification_Sheet__c.CongaRMVSSWorkflowStart__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Kicks off CongaWorkflow to Send RMVSS</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Raw Materials Ready for Signature</fullName>
        <actions>
            <name>Raw_Materials_Ready_for_Signature_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Raw_Materials_Ready_for_Signature_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Raw_Materials_Vendor_Specification_Sheet__c.Specifications_Complete__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Raw_Materials_Vendor_Specification_Sheet__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>Criteria to trigger that the RMVSS is Ready for Signature</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Raw Materials Rejected%2CRevision%2CVoided</fullName>
        <actions>
            <name>Raw_Material_Revise_Specifications</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Criteria to trigger that the RMVSS is Rejected,Revision,Voided</description>
        <formula>AND( ISPICKVAL(PRIORVALUE (Status__c),  &quot;Signed&quot;), OR(ISPICKVAL(Status__c, &quot;Rejected&quot;), ISPICKVAL(Status__c, &quot;Voided&quot;), ISPICKVAL(Status__c, &quot;Revision&quot;) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Raw Materials Revise</fullName>
        <actions>
            <name>Raw_Material_Revise_Specifications</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Raw_Materials_Revise</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Raw_Materials_Revise_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Criteria to trigger that the RMVSS is being Revised</description>
        <formula>AND(  Revise__c = TRUE,  ISCHANGED(Revise__c),  OR( ISPICKVAL(Status__c, &quot;Signed&quot;), ISPICKVAL(Status__c, &quot;Rejected&quot;) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Raw Materials Specifications Complete</fullName>
        <actions>
            <name>Raw_Materials_Ready_for_Signature_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Raw_Materials_Uncheck_Revise</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Criteria to trigger that the RMVSS is Specifications Complete</description>
        <formula>AND( Specifications_Complete__c=TRUE, ISCHANGED(Specifications_Complete__c), OR(ISPICKVAL(Status__c, &quot;Rejected&quot;), ISPICKVAL(Status__c, &quot;Voided&quot;), ISPICKVAL(Status__c, &quot;Revision&quot;) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Raw Materials Update Status - Completed</fullName>
        <actions>
            <name>RMVSS_Signed_Status_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Raw_Materials_Vendor_Specification_Sheet__c.Docusign_Envelope_Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>When Docusign Envelope Status is Completed, Status is Updated to Signed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Raw Materials Update Status - Declined</fullName>
        <actions>
            <name>RMVSS_Rejected_Status_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Raw_Materials_Vendor_Specification_Sheet__c.Docusign_Envelope_Status__c</field>
            <operation>equals</operation>
            <value>Declined</value>
        </criteriaItems>
        <description>When Docusign Envelope Status is Declined, Status is Updated to Rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Raw Materials Update Status - Delivered</fullName>
        <actions>
            <name>RMVSS_Out_for_Signature</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Raw_Materials_Vendor_Specification_Sheet__c.Docusign_Envelope_Status__c</field>
            <operation>equals</operation>
            <value>Delivered</value>
        </criteriaItems>
        <description>When Docusign Envelope Status is Delivered, Status is Updated to Out for Signature</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Raw Materials Update Status - Sent</fullName>
        <actions>
            <name>RMVSS_Out_for_Signature_Status_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Raw_Materials_Vendor_Specification_Sheet__c.Docusign_Envelope_Status__c</field>
            <operation>equals</operation>
            <value>Sent</value>
        </criteriaItems>
        <description>When Docusign Envelope Status is Sent, Status is Updated to Out for Signature</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Raw Materials Update Status - Voided</fullName>
        <actions>
            <name>RMVSS_Voided_Status_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Raw_Materials_Vendor_Specification_Sheet__c.Docusign_Envelope_Status__c</field>
            <operation>equals</operation>
            <value>Timed Out</value>
        </criteriaItems>
        <criteriaItems>
            <field>Raw_Materials_Vendor_Specification_Sheet__c.Docusign_Envelope_Status__c</field>
            <operation>equals</operation>
            <value>Deleted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Raw_Materials_Vendor_Specification_Sheet__c.Docusign_Envelope_Status__c</field>
            <operation>equals</operation>
            <value>Voided</value>
        </criteriaItems>
        <description>Criteria to trigger that the RMVSS is Ready for Signature</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
