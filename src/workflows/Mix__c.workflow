<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>MixSheet_Workflow_Uncheck</fullName>
        <field>Mix_Sheet_Workflow_Start__c</field>
        <literalValue>0</literalValue>
        <name>MixSheet Workflow Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Conga_Merge_Mix_Sheet</fullName>
        <apiVersion>33.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>Id</fields>
        <fields>MixSheet_Conga_URL__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>estes.mike@us.sika.com</integrationUser>
        <name>Conga Merge Mix Sheet</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Mix Workflow Trigger Checked</fullName>
        <actions>
            <name>MixSheet_Workflow_Uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Conga_Merge_Mix_Sheet</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Mix__c.Mix_Sheet_Workflow_Start__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Is the Mix SHeet Workflow Trigger Checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
