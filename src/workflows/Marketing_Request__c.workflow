<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>MR_Business_Unit_Aftermarket_Email_Alert</fullName>
        <description>MR Business Unit = Aftermarket (Email Alert)</description>
        <protected>false</protected>
        <recipients>
            <recipient>hemke.sam@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>livermore.russ@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Marketing_Requests/Marketing_Request_Submitted_Without_Email_Approval</template>
    </alerts>
    <alerts>
        <fullName>MR_Business_Unit_Appliances_Components_Email_Alert</fullName>
        <description>MR Business Unit = Appliances &amp; Components (Email Alert)</description>
        <protected>false</protected>
        <recipients>
            <recipient>gapczynski.monica@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hill.joshua@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>livermore.russ@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Marketing_Requests/Marketing_Request_Submitted_Without_Email_Approval</template>
    </alerts>
    <alerts>
        <fullName>MR_Business_Unit_Automotive_Email_Alert</fullName>
        <description>MR Business Unit = Automotive (Email Alert)</description>
        <protected>false</protected>
        <recipients>
            <recipient>fung.kent@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>moran.greg@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Marketing_Requests/Marketing_Request_Submitted_Without_Email_Approval</template>
    </alerts>
    <alerts>
        <fullName>MR_Business_Unit_FFI</fullName>
        <description>MR Business Unit = ​Trans OEM (Email Alert)</description>
        <protected>false</protected>
        <recipients>
            <recipient>gapczynski.monica@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>livermore.russ@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Marketing_Requests/Marketing_Request_Submitted_Without_Email_Approval</template>
    </alerts>
    <alerts>
        <fullName>MR_Business_Unit_FFI_Email_Alert</fullName>
        <description>MR Business Unit = FFI (Email Alert)</description>
        <protected>false</protected>
        <recipients>
            <recipient>gapczynski.monica@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Marketing_Requests/Marketing_Request_Submitted_Without_Email_Approval</template>
    </alerts>
    <alerts>
        <fullName>MR_Business_Unit_Industry_General_Email_Alert</fullName>
        <description>MR Business Unit = Industry General (Email Alert)</description>
        <protected>false</protected>
        <recipients>
            <recipient>gapczynski.monica@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>livermore.russ@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Marketing_Requests/Marketing_Request_Submitted_Without_Email_Approval</template>
    </alerts>
    <alerts>
        <fullName>MR_Business_Unit_Marine_Email_Alert</fullName>
        <description>MR Business Unit = Marine (Email Alert)</description>
        <protected>false</protected>
        <recipients>
            <recipient>gapczynski.monica@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hemke.sam@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>livermore.russ@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Marketing_Requests/Marketing_Request_Submitted_Without_Email_Approval</template>
    </alerts>
    <alerts>
        <fullName>MR_Owner_Change_Email_Alert</fullName>
        <description>MR Owner Change Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Marketing_Requests/Marketing_Request_Owner_Change</template>
    </alerts>
    <alerts>
        <fullName>Marketing_Request_Approved</fullName>
        <description>Marketing Request Approved</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Marketing_Requests/Marketing_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>Marketing_Request_Completed</fullName>
        <description>Marketing Request Completed</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Marketing_Requests/Marketing_Request_Completed</template>
    </alerts>
    <alerts>
        <fullName>Marketing_Request_Rejected</fullName>
        <description>Marketing Request Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Marketing_Requests/Marketing_Request_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved_By</fullName>
        <description>Displays who the request by approved by</description>
        <field>Approved_By__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>Approved By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Completed_Today</fullName>
        <field>Date_Completed__c</field>
        <formula>TODAY()</formula>
        <name>Date Completed Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marketing_Request_Approved_Checked</fullName>
        <description>Approved checkbox is checked</description>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>Marketing Request Approved Checked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marketing_Request_Completed_Date</fullName>
        <description>Updates the Marketing Request Completed Date field</description>
        <field>Date_Completed__c</field>
        <formula>TODAY()</formula>
        <name>Marketing Request Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marketing_Request_Date_Approved</fullName>
        <field>Date_Approved__c</field>
        <formula>TODAY()</formula>
        <name>Marketing Request Date Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marketing_Request_Date_Submitted</fullName>
        <description>Updates Date Submitted to the day of submission</description>
        <field>Date_Submitted__c</field>
        <formula>TODAY()</formula>
        <name>Marketing Request Date Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marketing_Request_Status_Draft</fullName>
        <description>Updates status to Draft</description>
        <field>Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Marketing Request Status = Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marketing_Request_Status_Processing</fullName>
        <description>Updates Marketing Request status to Processing</description>
        <field>Status__c</field>
        <literalValue>Processing</literalValue>
        <name>Marketing Request Status = Processing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marketing_Request_Status_Submitted</fullName>
        <description>Updates Marketing Request status to Submitted</description>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Marketing Request Status = Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Change_to_MS_Queue</fullName>
        <description>Initial submission action to assign submitted requests to MS Queue</description>
        <field>OwnerId</field>
        <lookupValue>Marketing_Request_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner Change to MS Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Completed Date on Marketing Request</fullName>
        <actions>
            <name>Date_Completed_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>Populates the completed date when the Document is marked complete</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MR Business Unit %3D Aftermarket</fullName>
        <actions>
            <name>MR_Business_Unit_Aftermarket_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Request__c.Date_Submitted__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Marketing_Request__c.Business_Unit_Type__c</field>
            <operation>equals</operation>
            <value>Aftermarket</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MR Business Unit %3D Appliances %26 Components</fullName>
        <actions>
            <name>MR_Business_Unit_Appliances_Components_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Request__c.Date_Submitted__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Marketing_Request__c.Business_Unit_Type__c</field>
            <operation>equals</operation>
            <value>Appliances &amp; Components</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MR Business Unit %3D Automotive</fullName>
        <actions>
            <name>MR_Business_Unit_Automotive_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Marketing_Request__c.Business_Unit_Type__c</field>
            <operation>equals</operation>
            <value>Automotive</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MR Business Unit %3D FFI</fullName>
        <actions>
            <name>MR_Business_Unit_FFI_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Request__c.Date_Submitted__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Marketing_Request__c.Business_Unit_Type__c</field>
            <operation>equals</operation>
            <value>FFI</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MR Business Unit %3D Industry General</fullName>
        <actions>
            <name>MR_Business_Unit_Industry_General_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Request__c.Date_Submitted__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Marketing_Request__c.Business_Unit_Type__c</field>
            <operation>equals</operation>
            <value>Industry General</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MR Business Unit %3D Marine</fullName>
        <actions>
            <name>MR_Business_Unit_Marine_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Request__c.Date_Submitted__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Marketing_Request__c.Business_Unit_Type__c</field>
            <operation>equals</operation>
            <value>Marine</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MR Business Unit %3D Trans OEM</fullName>
        <actions>
            <name>MR_Business_Unit_FFI</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Request__c.Date_Submitted__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Marketing_Request__c.Business_Unit_Type__c</field>
            <operation>equals</operation>
            <value>Trans OEM</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MR Owner Email</fullName>
        <actions>
            <name>MR_Owner_Change_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends owner an email when the owner is changed</description>
        <formula>AND(ISCHANGED(OwnerId), $User.Id&lt;&gt;OwnerId, Owner:Queue.QueueName&lt;&gt;&quot;Marketing Request Queue&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Marketing Request Approved</fullName>
        <actions>
            <name>Marketing_Request_Approved_Checked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Processing,On Hold,Complete,Archived</value>
        </criteriaItems>
        <description>Checks Approved checkbox if Marketing Request in approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send MS Request Completed Email</fullName>
        <actions>
            <name>Marketing_Request_Completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
