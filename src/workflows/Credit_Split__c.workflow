<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Credit_Split_Type_Location</fullName>
        <field>Credit_Split_Location__c</field>
        <literalValue>1</literalValue>
        <name>Credit Split- Type-Location</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Opportunity__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Roofing_Credit_Split_Update_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Credit_Split_Edit</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Roofing Credit Split Update RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Roofing_Update_KeySalesperson</fullName>
        <field>KeySalesperson__c</field>
        <formula>Opportunity__r.Id &amp; SAP_Sales_User__c</formula>
        <name>Roofing Update KeySalesperson</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Roofing_Update_KeyType</fullName>
        <field>KeyType__c</field>
        <formula>Opportunity__r.Id &amp; TEXT(Type__c)</formula>
        <name>Roofing Update KeyType</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opp_Credit_Line_Summary_1</fullName>
        <field>Credit_Split_Line_Summary_1__c</field>
        <formula>if( Sort_Number__c = 1, Credit_Split_Line_Summary__c, Opportunity__r.Credit_Split_Line_Summary_1__c )</formula>
        <name>Update Opp Credit Line Summary 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Opportunity__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opp_Credit_Line_Summary_2</fullName>
        <field>Credit_Split_Line_Summary_2__c</field>
        <formula>if( Sort_Number__c = 2, Credit_Split_Line_Summary__c, Opportunity__r.Credit_Split_Line_Summary_2__c )</formula>
        <name>Update Opp Credit Line Summary 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Opportunity__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opp_Credit_Line_Summary_3</fullName>
        <field>Credit_Split_Line_Summary_3__c</field>
        <formula>if( Sort_Number__c = 3, Credit_Split_Line_Summary__c, Opportunity__r.Credit_Split_Line_Summary_3__c )</formula>
        <name>Update Opp Credit Line Summary 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Opportunity__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opp_Credit_Line_Summary_4</fullName>
        <field>Credit_Split_Line_Summary_4__c</field>
        <formula>if( Sort_Number__c = 4, Credit_Split_Line_Summary__c, Opportunity__r.Credit_Split_Line_Summary_4__c)</formula>
        <name>Update Opp Credit Line Summary 4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Opportunity__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Credit Split - Type-Location</fullName>
        <actions>
            <name>Credit_Split_Type_Location</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Credit_Split__c.Type__c</field>
            <operation>equals</operation>
            <value>Location</value>
        </criteriaItems>
        <description>This workflow checks the field &quot;Credit Split -Location&quot; when type location is selected on credit split. This is further used for creating validation that prevents user from selecting same Type more than once</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Roofing Credit Split Change RT</fullName>
        <actions>
            <name>Roofing_Credit_Split_Update_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Roofing Credit Split Duplicate Detection</fullName>
        <actions>
            <name>Roofing_Update_KeySalesperson</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Roofing_Update_KeyType</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Roofing Credit Split On CREATE</fullName>
        <actions>
            <name>Update_Opp_Credit_Line_Summary_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Opp_Credit_Line_Summary_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Opp_Credit_Line_Summary_3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Opp_Credit_Line_Summary_4</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
