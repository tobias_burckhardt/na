<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Final_SPA_Approval_Email_Alert</fullName>
        <description>Final SPA Approval Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Pricing_SPA_SPQ_Templates/SPA_Pricing_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>Final_SPA_Rejection_Email_Alert</fullName>
        <description>Final SPA Rejection Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Pricing_SPA_SPQ_Templates/SPA_Pricing_Request_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Final_SPQ_Approval_Email_Alert</fullName>
        <description>Final SPQ Approval Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Pricing_SPA_SPQ_Templates/SPQ_Pricing_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>Final_SPQ_Rejection_Email_Alert</fullName>
        <description>Final SPQ Rejection Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Pricing_SPA_SPQ_Templates/SPQ_Pricing_Request_Rejected</template>
    </alerts>
    <alerts>
        <fullName>SPA_approval_notification_for_CSR_Team</fullName>
        <description>SPA approval notification for CSR Team</description>
        <protected>false</protected>
        <recipients>
            <field>CSR_Mail_Box__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Pricing_SPA_SPQ_Templates/SPA_Pricing_Request_Approved_CSR_Team</template>
    </alerts>
    <alerts>
        <fullName>SPQ_approval_notification_for_CSR_Team</fullName>
        <description>SPQ approval notification for CSR Team</description>
        <protected>false</protected>
        <recipients>
            <field>CSR_Mail_Box__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Pricing_SPA_SPQ_Templates/SPQ_Pricing_Request_Approved_CSR_Team</template>
    </alerts>
    <fieldUpdates>
        <fullName>Allow_document_generation</fullName>
        <field>Allow_Document_Generation__c</field>
        <literalValue>1</literalValue>
        <name>Allow document generation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_By</fullName>
        <field>Approved_By__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>Approved By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_Date</fullName>
        <field>Approved_Date__c</field>
        <formula>Today()</formula>
        <name>Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Disallow_document_generation</fullName>
        <field>Allow_Document_Generation__c</field>
        <literalValue>0</literalValue>
        <name>Disallow document generation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Approval</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Final Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Rejection</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Final Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Initial_Submission</fullName>
        <field>Status</field>
        <literalValue>Pending For Approvals</literalValue>
        <name>Initial Submission</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SPAApproved_By</fullName>
        <field>Approved_By__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>Approved By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SPQApproved_Date</fullName>
        <field>Approved_Date__c</field>
        <formula>Today()</formula>
        <name>Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Expiration_Date_for_Pricing</fullName>
        <field>ExpirationDate</field>
        <formula>Pricebook2.Validity_End_Date__c</formula>
        <name>Update Expiration Date for Pricing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Generate_Doc_flag_to_false</fullName>
        <field>Generate_Document_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Update Generate Doc flag to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Conga_Template_Generation_Process</fullName>
        <apiVersion>44.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>Conga_Template_Parameters__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>estes.mike@us.sika.com</integrationUser>
        <name>Conga Template Generation Process</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>SPA%2FSPQ for Conga Templates</fullName>
        <actions>
            <name>Update_Generate_Doc_flag_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Conga_Template_Generation_Process</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>AND(OR( RecordType.DeveloperName =&apos;SPA_Pricing&apos;,RecordType.DeveloperName =&apos;SPQ_Pricing&apos;), Generate_Document_Flag__c =true)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sika Pricing - Update Expiration Date for SPA Price Quote</fullName>
        <actions>
            <name>Update_Expiration_Date_for_Pricing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Quote.RecordTypeId</field>
            <operation>equals</operation>
            <value>SPA Pricing</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
