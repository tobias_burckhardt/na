<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Vaykay__TOB_Name_Update</fullName>
        <field>Name</field>
        <formula>Vaykay__Employee_Name__r.FirstName  &amp;&quot; &quot;&amp;  Vaykay__Employee_Name__r.LastName  &amp;&quot; &quot;&amp;     Vaykay__Year__c</formula>
        <name>TOB Name Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
