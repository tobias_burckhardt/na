<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Credit_increase_rejected_by_finance</fullName>
        <ccEmails>courchesne.olivier@ca.sika.com</ccEmails>
        <description>Credit increase rejected by finance</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Credit_increase_request_rejected</template>
    </alerts>
    <alerts>
        <fullName>Final_approval_given</fullName>
        <description>Final approval given</description>
        <protected>false</protected>
        <recipients>
            <recipient>debellis.paola@ca.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Canada_Templates/Final_approval_credit_limit</template>
    </alerts>
    <alerts>
        <fullName>Final_approval_given_PAP</fullName>
        <description>Final approval given</description>
        <protected>false</protected>
        <recipients>
            <recipient>debellis.paola@ca.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Canada_Templates/Final_approval_PAP</template>
    </alerts>
    <alerts>
        <fullName>New_Ship</fullName>
        <description>Your new ship to request has been approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Template/New_Ship_To_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>Second_approval_from_finance</fullName>
        <description>Second approval from finance</description>
        <protected>false</protected>
        <recipients>
            <recipient>courchesne.olivier@ca.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Credit_increase_approval_from_finance</template>
    </alerts>
    <alerts>
        <fullName>Your_account_creation_has_been_rejected</fullName>
        <description>Your account creation has been rejected</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>courchesne.olivier@ca.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/PAP_approval_rejected</template>
    </alerts>
    <alerts>
        <fullName>Your_account_creation_request_has_been_approved</fullName>
        <ccEmails>courchesne.olivier@ca.sika.com</ccEmails>
        <description>Your account creation request has been approved</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/PAP_approval_approved</template>
    </alerts>
    <alerts>
        <fullName>Your_credit_increase_request_has_been_rejected</fullName>
        <ccEmails>courchesne.olivier@ca.sika.com</ccEmails>
        <description>Your credit increase request has been rejected</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Credit_increase_request_rejected</template>
    </alerts>
    <alerts>
        <fullName>Your_new_ship_to_request_has_been_rejected</fullName>
        <description>Your new ship to request has been rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Template/New_Ship_To_Request_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval_rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Approval rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Credit_increase_approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Credit increase approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Approval</fullName>
        <description>The credit manager received a first order and go back in the request to close it by approving this step.</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Final Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Credit_Manager_Approval</fullName>
        <description>Pending Credit Manager Approval</description>
        <field>Status__c</field>
        <literalValue>Approved by sales manager and sales VP, pending credit manager approval</literalValue>
        <name>Pending Credit Manager Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Credit_Manager_Approval_real</fullName>
        <field>Status__c</field>
        <literalValue>Pending Credit Manager Approval</literalValue>
        <name>Pending Credit Manager Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_General_Manager_Approval</fullName>
        <description>Pending General Manager Approval</description>
        <field>Status__c</field>
        <literalValue>Pending General Manager Approval</literalValue>
        <name>Pending General Manager Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Regional_Sales_Manager_Approval</fullName>
        <description>Pending Regional Sales Manager Approval</description>
        <field>Status__c</field>
        <literalValue>Pending approval</literalValue>
        <name>Pending Regional Sales Manager Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Sales_VP_approval</fullName>
        <description>Set status to pending Sales VP Approval</description>
        <field>Status__c</field>
        <literalValue>Pending Sales VP Approval</literalValue>
        <name>Pending Sales VP approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Sr_VP_Finance_Admin_Approval</fullName>
        <description>Pending Sr. VP Finance &amp; Admin Approval</description>
        <field>Status__c</field>
        <literalValue>Pending Sr. VP Finance &amp; Admin Approval</literalValue>
        <name>Pending Sr. VP Finance &amp; Admin Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Put_status_to_conditional_approval</fullName>
        <field>Status__c</field>
        <literalValue>Conditional approval until a first firm order is received</literalValue>
        <name>Put status to conditional approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Approved_2</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Status to  Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Draft</fullName>
        <description>Setting status to draft if the request is recalled by the submitter</description>
        <field>Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Set Status to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_status_to_pending_approval</fullName>
        <description>While a position is in approval process the status should be pending approval.</description>
        <field>Status__c</field>
        <literalValue>Pending approval</literalValue>
        <name>Set status to pending approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_status_to_pending_approval_credit</fullName>
        <field>Status__c</field>
        <literalValue>Approved by sales manager, pending finance approval</literalValue>
        <name>Set status to pending approval credit in</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_With_VP</fullName>
        <description>Pending Sales VP approval</description>
        <field>Status__c</field>
        <literalValue>Pending Sales VP Approval</literalValue>
        <name>Status With VP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Completed_By_Field</fullName>
        <field>Completed_By__c</field>
        <formula>$User.FirstName +&quot; &quot;+$User.LastName</formula>
        <name>Update Completed By Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Completed_Date_Field</fullName>
        <field>Date_Completed__c</field>
        <formula>today()</formula>
        <name>Update Completed Date Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>credit_increase_rejected_by_finance</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>credit increase rejected by finance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>waiting_for_finance_approval</fullName>
        <field>Status__c</field>
        <literalValue>Approved by sales manager, pending finance approval</literalValue>
        <name>waiting for finance approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <tasks>
        <fullName>Create_the_Salesforce_account_of_your_new_customer</fullName>
        <assignedToType>owner</assignedToType>
        <description>The person who requested the creation of a new customer in SAP must himself create the account of the new customer in Salesforce once the account creation request has been approved by the sales manager and the credit manager.
*2 weeks period to do it*</description>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Create the Salesforce account of your new customer</subject>
    </tasks>
</Workflow>
