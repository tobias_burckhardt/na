<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Task_Due_Alert</fullName>
        <description>Task Due Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Alert_Templates/Task_Due_Date</template>
    </alerts>
    <alerts>
        <fullName>Task_Due_Alert2</fullName>
        <description>Task Due Alert2</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Alert_Templates/Task_Due_Date</template>
    </alerts>
    <fieldUpdates>
        <fullName>Comments_to_Short_Comments</fullName>
        <description>Copy First 220 characters into short comments and note that they can see more in the full comments</description>
        <field>Short_Description__c</field>
        <formula>IF(LEFT(Subject, 6) = &quot;Email:&quot; , 
MID(Description,FIND(&quot;Subject:&quot;, Description),220), 
 LEFT(Description, 220) &amp; &quot; ...see comments for more...&quot;)</formula>
        <name>Comments to Short Comments</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ISRType_Sales_Call</fullName>
        <description>Update ISR Type to &quot;Sales Call&quot; when created by Eloqua Marketing</description>
        <field>ISR_Call_Type__c</field>
        <literalValue>Sales Call</literalValue>
        <name>ISRType_Sales_Call</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Short_Comment_Copy</fullName>
        <description>Copies full text from Comments to Short Comments</description>
        <field>Short_Description__c</field>
        <formula>Description</formula>
        <name>Short Comment Copy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>IS_Owner_Appointment_Set</fullName>
        <apiVersion>33.0</apiVersion>
        <endpointUrl>https://powerstandings.insidesales.com/kpi/apptsset</endpointUrl>
        <fields>Id</fields>
        <fields>LastModifiedById</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>sarnafil.admin@us.sika.com</integrationUser>
        <name>IS Owner Appointment Set</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>IS Owner Appointment</fullName>
        <actions>
            <name>IS_Owner_Appointment_Set</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4 OR 5 OR 6 OR 7)</booleanFilter>
        <criteriaItems>
            <field>Task.IS_Owner_Appointment__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.LastModifiedById</field>
            <operation>equals</operation>
            <value>Trudy Wallace</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.LastModifiedById</field>
            <operation>equals</operation>
            <value>Ellen Walkama</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.LastModifiedById</field>
            <operation>equals</operation>
            <value>Paulette Hogan</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.LastModifiedById</field>
            <operation>equals</operation>
            <value>Tom Brady</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.LastModifiedById</field>
            <operation>equals</operation>
            <value>Christopher Falcone</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.LastModifiedById</field>
            <operation>equals</operation>
            <value>Brian Tanguy</value>
        </criteriaItems>
        <description>Workflow for InsideSales.com KPI filter to indicate when Inside Sales reps have scheduled a Owner appointment.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ISR Call Type</fullName>
        <actions>
            <name>ISRType_Sales_Call</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Task.CreatedById</field>
            <operation>equals</operation>
            <value>Eloqua Marketing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>contains</operation>
            <value>PPC,Adwords,Google,Bing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.ISRType2__c</field>
            <operation>equals</operation>
            <value>Sales Call</value>
        </criteriaItems>
        <description>Automatically update field ISR Call Type to &quot;Sales Call&quot; When lead has been created by Eloqua Marketing</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify On Due Date</fullName>
        <active>false</active>
        <formula>ActivityDate &lt;&gt; Today() &amp;&amp;
NOT( IsClosed )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Task_Due_Alert2</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Task.ActivityDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Short Comment Copy</fullName>
        <actions>
            <name>Short_Comment_Copy</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copy Shorter Comments Straight to Short Comments</description>
        <formula>LEN(Description) &lt; 256</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Short Comment Update</fullName>
        <actions>
            <name>Comments_to_Short_Comments</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>LEN(Description) &gt; 255</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
