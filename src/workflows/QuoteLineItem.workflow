<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Total_Cost_updation</fullName>
        <field>Total_Cost__c</field>
        <formula>Net_Cost__c</formula>
        <name>Total Cost updation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QLI_Unique_Identifier</fullName>
        <field>Quote_Line_Item_Unique_Identifier__c</field>
        <formula>IF( Quote.RecordType.DeveloperName=&apos;SPA_Pricing&apos; || Quote.RecordType.DeveloperName=&apos;SPQ_Pricing&apos;, QuoteId +&apos;#&apos;+ Product2Id,&apos;&apos;)</formula>
        <name>Update QLI Unique Identifier</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Standard_Cost_for_Roll_up</fullName>
        <field>Standard_Cost_for_Roll_up__c</field>
        <formula>Standard_Cost_Available__c</formula>
        <name>Update Standard Cost for Roll up</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Pricing - Update QLI Identifier</fullName>
        <actions>
            <name>Update_QLI_Unique_Identifier</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( Quote.RecordType.DeveloperName =&apos;SPA_Pricing&apos;,Quote.RecordType.DeveloperName =&apos;SPQ_Pricing&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Total Cost for Quote Line Items</fullName>
        <actions>
            <name>Total_Cost_updation</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Standard_Cost_for_Roll_up</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This will update the total cost to a read only field which will in turn used for roll up summary field on quote object.</description>
        <formula>OR(Quote.RecordType.DeveloperName =&apos;SPA_Pricing&apos;,
 Quote.RecordType.DeveloperName =&apos;SPQ_Pricing&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
