<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Dispenser_Installations_Approved</fullName>
        <description>Dispenser_Installations_Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Dispenser_Installation_Templates/Dispenser_Installations_Approved</template>
    </alerts>
    <alerts>
        <fullName>Dispenser_Installations_Rejected</fullName>
        <description>Dispenser_Installations_Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Dispenser_Installation_Templates/Dispenser_Installations_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Dispenser_Installations_Submission</fullName>
        <description>Dispenser_Installations_Submission</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Dispenser_Installation_Templates/Dispenser_Installations_Submission</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_By</fullName>
        <field>Approved_By__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>Approved By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_Date</fullName>
        <description>This field update displays the approved date of the record.</description>
        <field>Approval_Date__c</field>
        <formula>Today()</formula>
        <name>Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CA_Free_Lock</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CA_Free_Dispenser_Installation_Lock</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CA Free Lock</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Finance_Review</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending for Finance Review</literalValue>
        <name>Finance Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fince_Review2</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending for Finance 2 nd review</literalValue>
        <name>Finance Review2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_For_Lease_Agreement</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending For Lease Agreement</literalValue>
        <name>Pending For Lease Agreement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Post_Date</fullName>
        <field>Post_Date__c</field>
        <formula>Now()</formula>
        <name>Post Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ROI</fullName>
        <field>Approval_Status__c</field>
        <literalValue>ROI Approved &amp; Pending Installation</literalValue>
        <name>ROI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Regional_Manager_Review</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending for Regional Sales Manager Approval</literalValue>
        <name>Regional Manager Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Representative</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending for Owner Review</literalValue>
        <name>Representative</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Region</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Regional Sales Manager Review</literalValue>
        <name>Sales Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sr_VP_Finance</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending for Sr. VP Finance Admin Approval</literalValue>
        <name>Sr. VP Finance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TM_VP_Review</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending for Target Market VP Approval</literalValue>
        <name>TM VP Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>test</fullName>
        <field>File_del__c</field>
        <literalValue>0</literalValue>
        <name>test</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
