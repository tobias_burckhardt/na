<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>alm_pm2__Capture_Closed_Date</fullName>
        <field>alm_pm2__Closed_Date__c</field>
        <formula>NOW()</formula>
        <name>Capture Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>alm_pm2__Set_Resolved_Date</fullName>
        <field>alm_pm2__Resolved_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Resolved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>alm_pm2__Capture Closed Date</fullName>
        <actions>
            <name>alm_pm2__Capture_Closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>alm_pm2__Backlog__c.alm_pm2__Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Date/Time should be set when defect&apos;s status is changed to Closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>alm_pm2__Capture Resolved Date</fullName>
        <actions>
            <name>alm_pm2__Set_Resolved_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>alm_pm2__Backlog__c.alm_pm2__Status__c</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <description>Date/Time should be set when defect&apos;s status is changed to Resolved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
