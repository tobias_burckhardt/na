<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Knowledge_Article_Approved</fullName>
        <description>Knowledge Article Approved</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>IT_Cases_Service_Cloud/Knowledge_Article_Approved</template>
    </alerts>
    <fieldUpdates>
        <fullName>Check_Needs_Approval_Field</fullName>
        <field>Needs_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Check &quot;Needs Approval&quot; Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unchecks_Approval_Field</fullName>
        <field>Needs_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Unchecks Approval Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>Publish_Approved_Artricle</fullName>
        <action>Publish</action>
        <label>Publish Approved Artricle</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
</Workflow>
