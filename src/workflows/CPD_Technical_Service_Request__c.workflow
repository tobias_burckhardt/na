<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>TSR_Completed_Construction</fullName>
        <description>TSR Completed - Construction</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>gioe.nicole@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Construction_TSR_Email_Templates/TSR_Completed_By_Lab_Construction</template>
    </alerts>
    <alerts>
        <fullName>TSR_In_Progress_Construction</fullName>
        <description>TSR In Progress - Construction</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>gioe.nicole@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Construction_TSR_Email_Templates/TSR_Testing_In_Progress_Construction</template>
    </alerts>
    <alerts>
        <fullName>TSR_Submitted_Construction</fullName>
        <description>TSR Submitted - Construction</description>
        <protected>false</protected>
        <recipients>
            <recipient>Concrete_Technical_Service</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>gioe.nicole@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Construction_TSR_Email_Templates/TSR_Submitted_to_Lab_Construction</template>
    </alerts>
</Workflow>
