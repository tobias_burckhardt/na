<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>alm_pm2__Update_SCAN_End_Time</fullName>
        <field>alm_pm2__Run_Time__c</field>
        <formula>NOW()</formula>
        <name>Update SCAN End Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>alm_pm2__Set Scan Run Time</fullName>
        <actions>
            <name>alm_pm2__Update_SCAN_End_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the End Time field whenever the Result field value changes and the previous value was empty.</description>
        <formula>ISPICKVAL(PRIORVALUE(alm_pm2__Result__c), &apos;&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
