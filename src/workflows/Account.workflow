<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EMail_Notification_to_Key_Account_Manager</fullName>
        <description>EMail Notification to Key Account Manager</description>
        <protected>false</protected>
        <recipients>
            <field>Key_Account_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Template/CST_Project_Manager_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>CNC_Marketing_Return</fullName>
        <description>Return Rec Type to Marketing if Conc Producer is unselected.</description>
        <field>RecordTypeId</field>
        <lookupValue>Marketing</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CNC Marketing Return</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Project_Site_Update</fullName>
        <description>Sets SAP Project Site Accounts with Correct Project Site Type</description>
        <field>Type</field>
        <literalValue>Project Site</literalValue>
        <name>Project Site Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Roofing_Account</fullName>
        <field>TM_Roofing__c</field>
        <literalValue>1</literalValue>
        <name>Roofing Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SAP_Account_Code_Update</fullName>
        <description>Take SAP Account ID and Remove the &quot;US01_&quot; or &quot;CA01_&quot; prefix</description>
        <field>SAP_Account_Code__c</field>
        <formula>mid(SAP_Account_ID__c,6,20)</formula>
        <name>SAP Account Code Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ShipTo_Site_Update</fullName>
        <description>Sets SAP Imported Ship To Sites with Correct Account Type</description>
        <field>Type</field>
        <literalValue>Ship To Site</literalValue>
        <name>ShipTo Site Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Blank Type Field Populated by Lead Account Type Value</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If the &quot;Type&quot; field is blank on an Account but a value has been mapped from a converted lead to the &quot;Lead Account Type&quot; field then that value will be pulled into the Type field.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CNC Marketing Return</fullName>
        <actions>
            <name>CNC_Marketing_Return</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Concrete Marketing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>notEqual</operation>
            <value>Concrete Producer</value>
        </criteriaItems>
        <description>If Account is Conc Marketing and Type is not  Concrete Prod, update rec type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Key Account Manager Change</fullName>
        <actions>
            <name>EMail_Notification_to_Key_Account_Manager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify User if he is named Key Account Manager</description>
        <formula>NOT(isnull( Key_Account_Manager__c )) &amp;&amp;
 Key_Account_Manager__r.Id  &lt;&gt;  $User.Id</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ProjectSiteUpdate</fullName>
        <actions>
            <name>Project_Site_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.SAPAccountGroup__c</field>
            <operation>equals</operation>
            <value>YPRJ</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Roofing Account</fullName>
        <actions>
            <name>Roofing_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>MEs Added step to skip for Tina M.</description>
        <formula>(ISPICKVAL ( LastModifiedBy.Target_Market__c, &quot;Roofing&quot; ) || 
ISPICKVAL ( Owner.Target_Market__c, &quot;Roofing&quot; ))
&amp;&amp;
 NOT(
$User.Id = &quot;005400000019hbzAAA&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SAP Account Code Update</fullName>
        <actions>
            <name>SAP_Account_Code_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If SAP Account ID is changed, Update Account Code (ID minus Sales Org)</description>
        <formula>(ISNEW() &amp;&amp; NOT(ISBLANK(SAP_Account_ID__c))) || ISCHANGED(SAP_Account_ID__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
