<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Clear_PTS_Legacy_ID_on_PLI_Clone</fullName>
        <description>Clears the value in the PTS Legacy ID field if the record is created via clone function.</description>
        <field>PTS_Legacy_ID__c</field>
        <name>Clear PTS Legacy ID on PLI Clone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Expected_Start_Date_from_Opp</fullName>
        <field>Apttus_Proposal__ExpectedStartDate__c</field>
        <formula>Apttus_Proposal__Proposal__r.Apttus_Proposal__Opportunity__r.Anticipated_Start_Date__c</formula>
        <name>Populate Expected Start Date from Opp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Apttus_Proposal__Proposal__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Clear PTS Legacy ID on Clone</fullName>
        <actions>
            <name>Clear_PTS_Legacy_ID_on_PLI_Clone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Expected_Start_Date_from_Opp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal_Line_Item__c.PTS_Legacy_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
