<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Firm_Role</fullName>
        <description>Update Firm Role with Account Type</description>
        <field>Firm_Role__c</field>
        <literalValue>Owner</literalValue>
        <name>Update Firm Role</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Firm Role</fullName>
        <actions>
            <name>Update_Firm_Role</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate Firm Role with &apos;Owner&apos; if left blank upon creation and Account Type = Owner</description>
        <formula>AND( 
TEXT(Account__r.Type) = &quot;Owner&quot;, 
ISBLANK(TEXT(Firm_Role__c)) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
