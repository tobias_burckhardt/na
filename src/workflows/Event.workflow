<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Event_Comment_Copy</fullName>
        <field>Short_Description__c</field>
        <formula>Description</formula>
        <name>Event Comment Copy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Event_Comment_Update</fullName>
        <field>Short_Description__c</field>
        <formula>left(Description, 220) &amp; &quot; ...see comments for more...&quot;</formula>
        <name>Event Comment Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Event Comment Copy</fullName>
        <actions>
            <name>Event_Comment_Copy</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copy Full Comment to Short Comment (less than 255 characters)</description>
        <formula>len(Description) &lt;256</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Short Comment Event Update</fullName>
        <actions>
            <name>Event_Comment_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Event Comments to Short Comments</description>
        <formula>len(Description) &gt;255</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
