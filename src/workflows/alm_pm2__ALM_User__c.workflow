<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>alm_pm2__ALM_User_Email</fullName>
        <field>alm_pm2__Email__c</field>
        <formula>alm_pm2__User__r.Email</formula>
        <name>ALM User Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>alm_pm2__Copy_ALM_User_Name</fullName>
        <field>Name</field>
        <formula>alm_pm2__User__r.FirstName + &quot; &quot; + alm_pm2__User__r.LastName</formula>
        <name>Copy ALM User Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>alm_pm2__Copy User Fields</fullName>
        <actions>
            <name>alm_pm2__ALM_User_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>alm_pm2__Copy_ALM_User_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>!ISBLANK(alm_pm2__User__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
