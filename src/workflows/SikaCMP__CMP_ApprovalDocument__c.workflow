<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CMP_ApprovalDocumentApprovedNotification</fullName>
        <description>Approval Document Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CMP_ApprovalDocumentApproved</template>
    </alerts>
    <alerts>
        <fullName>CMP_ApprovalDocumentRejectedNotification</fullName>
        <description>Approval Document Rejected Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CMP_ApprovalDocumentRejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>CMP_UpdateApprovalDocumentClosingDate</fullName>
        <field>SikaCMP__ApprovalDocumentClosingDate__c</field>
        <formula>TODAY()</formula>
        <name>Update Approval Document Closing Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CMP_UpdateApprovalDocumentStatus</fullName>
        <field>SikaCMP__Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Approval Document Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CMP_UpdateApprovalDocumentStatusRejected</fullName>
        <field>SikaCMP__Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Approval Document Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
