<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Notification_Intent_To_Warranty_Nearing_Expected_Start_Date</fullName>
        <description>Email Notification - Intent To Warranty Nearing Expected Start Date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Warranty_Email_Template/Int_to_War_Expire_Notice</template>
    </alerts>
    <alerts>
        <fullName>Notify_Owner_When_Expected_Complete_Data_Is_Nearing</fullName>
        <description>Notify Owner When Expected Complete Data Is Nearing</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Warranty_Email_Template/War_Lang_Notice_Nearing_Exp_Comp_Date</template>
    </alerts>
    <alerts>
        <fullName>Notify_Owner_When_Warranty_Hasn_t_Been_Modified_In_90_Days</fullName>
        <description>Notify Owner When Warranty Hasn&apos;t Been Modified In 90 Days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Warranty_Email_Template/War_Lang_Notice_Not_Modified_In_Last_90_Days</template>
    </alerts>
    <alerts>
        <fullName>Notify_Regional_Manager_of_Warranty_Submission</fullName>
        <description>Notify Regional Manager of Warranty Submission</description>
        <protected>false</protected>
        <recipients>
            <field>Regional_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Warranty_Email_Template/Notify_Regional_Manager</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Warranty_Status</fullName>
        <description>Roofing Warranty Status</description>
        <protected>false</protected>
        <recipients>
            <field>Applicator_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PTS_Templates/Roofing_Warranty_Status</template>
    </alerts>
    <alerts>
        <fullName>Send_Approved_Notification</fullName>
        <description>Send Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Warranty_Email_Template/Warranty_Approved</template>
    </alerts>
    <alerts>
        <fullName>Send_Construction_Warranty_Recall_Notification</fullName>
        <description>Send Construction Warranty Recall Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>diaz.ed@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>zuppa.tom@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Regional_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Warranty_Email_Template/Warranty_Recalled</template>
    </alerts>
    <alerts>
        <fullName>Send_Flooring_Warranty_Recall_Notification</fullName>
        <description>Send Flooring Warranty Recall Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>diaz.ed@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kubli.ralf@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>zuppa.tom@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Regional_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Warranty_Email_Template/Warranty_Recalled</template>
    </alerts>
    <alerts>
        <fullName>Send_Rejection_Notification</fullName>
        <description>Send Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Warranty_Email_Template/Warranty_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Send_Warranty_w_o_Labor_Recall_Notification</fullName>
        <description>Send Warranty w/o Labor Recall Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>diaz.ed@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>zuppa.tom@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Warranty_Email_Template/Warranty_Recalled</template>
    </alerts>
    <alerts>
        <fullName>Send_Waterproofing_Warranty_Recall_Notification</fullName>
        <description>Send Waterproofing Warranty Recall Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>fisher.ryan@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>loyd.kyle@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Warranty_Email_Template/Warranty_Recalled</template>
    </alerts>
    <fieldUpdates>
        <fullName>Add_Applicator_Address</fullName>
        <field>Applicator_Address__c</field>
        <formula>Applicator__r.BillingStreet</formula>
        <name>Add Applicator Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Applicator_City</fullName>
        <field>Applicator_City__c</field>
        <formula>Applicator__r.BillingCity</formula>
        <name>Add Applicator City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Applicator_Country</fullName>
        <field>Applicator_Country__c</field>
        <formula>Applicator__r.BillingCountry</formula>
        <name>Add Applicator Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Applicator_Name</fullName>
        <field>Applicator_Name__c</field>
        <formula>Applicator__r.Name</formula>
        <name>Add Applicator Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Applicator_State</fullName>
        <field>Applicator_State__c</field>
        <formula>Applicator__r.BillingState</formula>
        <name>Add Applicator State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Applicator_Zip_Code</fullName>
        <field>Applicator_Zip_Code__c</field>
        <formula>Applicator__r.BillingPostalCode</formula>
        <name>Add Applicator Zip Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Architect_Address</fullName>
        <field>Architect_Address__c</field>
        <formula>Architect__r.BillingStreet</formula>
        <name>Add Architect Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Architect_City</fullName>
        <field>Architect_City__c</field>
        <formula>Architect__r.BillingCity</formula>
        <name>Add Architect City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Architect_Country</fullName>
        <field>Architect_Country__c</field>
        <formula>Architect__r.BillingCountry</formula>
        <name>Add Architect Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Architect_Name</fullName>
        <field>Architect_Name__c</field>
        <formula>Architect__r.Name</formula>
        <name>Add Architect Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Architect_State</fullName>
        <field>Architect_State__c</field>
        <formula>Architect__r.BillingState</formula>
        <name>Add Architect State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Architect_Zip_Code</fullName>
        <field>Architect_Zip_Code__c</field>
        <formula>Architect__r.BillingPostalCode</formula>
        <name>Add Architect Zip Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Distributor_Address</fullName>
        <field>Distributor_Address__c</field>
        <formula>Distributor__r.BillingStreet</formula>
        <name>Add Distributor Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Distributor_City</fullName>
        <field>Distributor_City__c</field>
        <formula>Distributor__r.BillingCity</formula>
        <name>Add Distributor City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Distributor_Country</fullName>
        <field>Distributor_Country__c</field>
        <formula>Distributor__r.BillingCountry</formula>
        <name>Add Distributor Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Distributor_Name</fullName>
        <field>Distributor_Name__c</field>
        <formula>Distributor__r.Name</formula>
        <name>Add Distributor Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Distributor_State</fullName>
        <field>Distributor_State__c</field>
        <formula>Distributor__r.BillingState</formula>
        <name>Add Distributor State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Distributor_Zip_Code</fullName>
        <field>Distributor_Zip_Code__c</field>
        <formula>Distributor__r.BillingPostalCode</formula>
        <name>Add Distributor Zip Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_General_Contractor_Address</fullName>
        <field>General_Contractor_Address__c</field>
        <formula>General_Contractor__r.BillingStreet</formula>
        <name>Add General Contractor Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_General_Contractor_City</fullName>
        <field>General_Contractor_City__c</field>
        <formula>General_Contractor__r.BillingCity</formula>
        <name>Add General Contractor City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_General_Contractor_Country</fullName>
        <field>General_Contractor_Country__c</field>
        <formula>General_Contractor__r.BillingCountry</formula>
        <name>Add General Contractor Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_General_Contractor_Name</fullName>
        <field>General_Contractor_Name__c</field>
        <formula>General_Contractor__r.Name</formula>
        <name>Add General Contractor Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_General_Contractor_State</fullName>
        <field>General_Contractor_State__c</field>
        <formula>General_Contractor__r.BillingState</formula>
        <name>Add General Contractor State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_General_Contractor_Zip_Code</fullName>
        <field>General_Contractor_Zip_Code__c</field>
        <formula>General_Contractor__r.BillingPostalCode</formula>
        <name>Add General Contractor Zip Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Owner_Address</fullName>
        <field>Owner_Address__c</field>
        <formula>Owner__r.BillingStreet</formula>
        <name>Add Owner Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Owner_City</fullName>
        <field>Owner_City__c</field>
        <formula>Owner__r.BillingCity</formula>
        <name>Add Owner City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Owner_Country</fullName>
        <field>Owner_Country__c</field>
        <formula>Owner__r.BillingCountry</formula>
        <name>Add Owner Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Owner_Name</fullName>
        <field>Owner_Name__c</field>
        <formula>Owner__r.Name</formula>
        <name>Add Owner Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Owner_State</fullName>
        <field>Owner_State__c</field>
        <formula>Owner__r.BillingState</formula>
        <name>Add Owner State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Owner_Zip_Code</fullName>
        <field>Owner_Zip_Code__c</field>
        <formula>Owner__r.BillingPostalCode</formula>
        <name>Add Owner Zip Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Project_Address</fullName>
        <field>Project_Address__c</field>
        <formula>Opportunity__r.Project__r.Street_Address__c</formula>
        <name>Add Project Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Project_City</fullName>
        <field>Project_City__c</field>
        <formula>Opportunity__r.Project__r.City__c</formula>
        <name>Add Project City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Project_Country</fullName>
        <field>Project_Country__c</field>
        <formula>Opportunity__r.Project__r.Country__c</formula>
        <name>Add Project Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Project_Name</fullName>
        <field>Project_Name__c</field>
        <formula>Opportunity__r.Project__r.Name</formula>
        <name>Add Project Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Project_State</fullName>
        <field>Project_State__c</field>
        <formula>Opportunity__r.Project__r.State__c</formula>
        <name>Add Project State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Project_Zip_Code</fullName>
        <field>Project_Zip_Code__c</field>
        <formula>Opportunity__r.Project__r.Zip__c</formula>
        <name>Add Project Zip Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Date</fullName>
        <description>Set Approval Date/Time to NOW</description>
        <field>Approval_Date__c</field>
        <formula>NOW()</formula>
        <name>Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Increase_Expected_Complete_Date</fullName>
        <description>Increases the Expected Complete Date by 30 days if the Expected Complete Date is greater than today&apos;s date.</description>
        <field>Expected_Complete_Date__c</field>
        <formula>Expected_Complete_Date__c + 30</formula>
        <name>Increase Expected Complete Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Increase_Warranty_Expected_Complete_Date</fullName>
        <description>Increases the Expected Complete Date field by 30 days if Expected Complete Date is 1 day before today&apos;s date.</description>
        <field>Expected_Complete_Date__c</field>
        <formula>Expected_Complete_Date__c  + 30</formula>
        <name>Increase Warranty Expected Complete Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Final_Approved_Warranty_Status</fullName>
        <description>Changes the Warranty Status to &apos;Approved&apos; if approved.</description>
        <field>Warranty_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Final Approved Warranty Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Final_Rejection_Warranty_Status</fullName>
        <description>Changes the Warranty Status to &apos;Open&apos; if approval was rejected.</description>
        <field>Warranty_Status__c</field>
        <literalValue>Open</literalValue>
        <name>Set Final Rejection Warranty Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Initial_Warranty_Status</fullName>
        <description>Change the Warranty Status field to show &apos;Submitted&apos; when a user clicks on the Submit For Approval button.</description>
        <field>Warranty_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Set Initial Warranty Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Recall_Warranty_Status</fullName>
        <description>Changes the Warranty Status to &apos;Open&apos; if approval was recalled.</description>
        <field>Warranty_Status__c</field>
        <literalValue>Open</literalValue>
        <name>Set Recall Warranty Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Conga_Merge_INF_10yr_Warranty</fullName>
        <apiVersion>37.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>INF_10_Year_Warranty_Conga_URL__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>estes.mike@us.sika.com</integrationUser>
        <name>Conga Merge INF 10yr Warranty</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Conga_Merge_INF_Warranty</fullName>
        <apiVersion>37.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>INF_SikaBond_Warranty_Conga_URL__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>estes.mike@us.sika.com</integrationUser>
        <name>Conga Merge INF Warranty</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Conga_Merge_LTD_Community_Warranty</fullName>
        <apiVersion>45.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>Id</fields>
        <fields>LTD_Warranty_Conga_URL__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>estes.mike@us.sika.com</integrationUser>
        <name>Conga Merge LTD Community Warranty</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Conga_Merge_LTD_Warranty_Internal</fullName>
        <apiVersion>45.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>Id</fields>
        <fields>LTD_Warranty_Conga_Internal_URL__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>estes.mike@us.sika.com</integrationUser>
        <name>Conga Merge LTD Warranty Internal</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Conga_Merge_STD_Community_Warranty</fullName>
        <apiVersion>34.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>Id</fields>
        <fields>STD_Warranty_Conga_URL__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>estes.mike@us.sika.com</integrationUser>
        <name>Conga Merge STD Community Warranty</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Conga_Merge_STD_Warranty</fullName>
        <apiVersion>34.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>Id</fields>
        <fields>STD_Warranty_Conga_URL__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>estes.mike@us.sika.com</integrationUser>
        <name>Conga Merge STD Warranty</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Conga_Merge_STD_Warranty_Internal</fullName>
        <apiVersion>45.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>Id</fields>
        <fields>STD_Warranty_Conga_Internal_URL__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>estes.mike@us.sika.com</integrationUser>
        <name>Conga Merge STD Warranty Internal</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Dow_Reuse_Conga_WF_Message</fullName>
        <apiVersion>37.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>Id</fields>
        <fields>Warranty_Dow_Reuse_Conga_WF__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>bluewolf.admin@us.sika.com</integrationUser>
        <name>Dow Reuse Conga WF Message</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>VG_20_Year_Warranty_Conga_Workflow</fullName>
        <apiVersion>43.0</apiVersion>
        <description>Warranty workflow for All VG Warranties (previously only for 20 year items)</description>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>Id</fields>
        <fields>VG_20_Year_Conga_URL__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>estes.mike@us.sika.com</integrationUser>
        <name>VG Warranty Conga Workflow</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>VG_5_Year_Warranty_Conga_Workflow</fullName>
        <apiVersion>43.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>Id</fields>
        <fields>VG_5_Year_Conga_URL__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>estes.mike@us.sika.com</integrationUser>
        <name>VG 5 Year Warranty Conga Workflow</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Warranty_Conga_WF_Message</fullName>
        <apiVersion>38.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>Id</fields>
        <fields>Warranty_Conga_WF__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>bluewolf.admin@us.sika.com</integrationUser>
        <name>Warranty Conga WF Message</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Warranty_Extension_Conga_WF_Message</fullName>
        <apiVersion>42.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>Extended_Warranty_Conga_WF__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>bluewolf.admin@us.sika.com</integrationUser>
        <name>Warranty Extension Conga WF Message</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Add Applicator Information To Warranty</fullName>
        <actions>
            <name>Add_Applicator_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Applicator_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Applicator_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Applicator_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Applicator_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Applicator_Zip_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To link in the Applicator information from the Account App</description>
        <formula>Applicator__c &lt;&gt; &quot;&quot; &amp;&amp;  Applicator_Address__c = &quot;&quot; &amp;&amp;  Applicator_City__c = &quot;&quot; &amp;&amp;  Applicator_Country__c = &quot;&quot; &amp;&amp;  Applicator_Name__c = &quot;&quot; &amp;&amp;  Applicator_State__c = &quot;&quot; &amp;&amp;  Applicator_Zip_Code__c = &quot;&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Add Architect Information To Warranty</fullName>
        <actions>
            <name>Add_Architect_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Architect_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Architect_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Architect_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Architect_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Architect_Zip_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To link in the Architect information from the Account App</description>
        <formula>Architect__c &lt;&gt; &quot;&quot; &amp;&amp; Architect_Address__c = &quot;&quot; &amp;&amp; Architect_City__c = &quot;&quot; &amp;&amp; Architect_Country__c = &quot;&quot; &amp;&amp; Architect_Name__c = &quot;&quot; &amp;&amp; Architect_State__c = &quot;&quot; &amp;&amp; Architect_Zip_Code__c = &quot;&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Add Distributor Information To Warranty</fullName>
        <actions>
            <name>Add_Distributor_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Distributor_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Distributor_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Distributor_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Distributor_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Distributor_Zip_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To link in Distributor information from the Account App</description>
        <formula>Distributor__c &lt;&gt; &quot;&quot; &amp;&amp;  Distributor_Address__c = &quot;&quot; &amp;&amp;  Distributor_City__c = &quot;&quot; &amp;&amp;  Distributor_Country__c = &quot;&quot; &amp;&amp;  Distributor_Name__c = &quot;&quot; &amp;&amp;  Distributor_State__c = &quot;&quot; &amp;&amp;  Distributor_Zip_Code__c = &quot;&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Add General Contractor Information To Warranty</fullName>
        <actions>
            <name>Add_General_Contractor_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_General_Contractor_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_General_Contractor_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_General_Contractor_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_General_Contractor_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_General_Contractor_Zip_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To link in General Contractor information from the Account App</description>
        <formula>General_Contractor__c &lt;&gt; &quot;&quot; &amp;&amp; General_Contractor_Address__c = &quot;&quot; &amp;&amp; General_Contractor_City__c = &quot;&quot; &amp;&amp; General_Contractor_Country__c = &quot;&quot; &amp;&amp; General_Contractor_Name__c = &quot;&quot; &amp;&amp; General_Contractor_State__c = &quot;&quot; &amp;&amp; General_Contractor_Zip_Code__c = &quot;&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Add Owner Information To Warranty</fullName>
        <actions>
            <name>Add_Owner_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Owner_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Owner_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Owner_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Owner_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Owner_Zip_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To link in Owner information from the Account app.</description>
        <formula>Owner__c &lt;&gt; &quot;&quot; &amp;&amp;  Owner_Address__c = &quot;&quot; &amp;&amp; Owner_City__c = &quot;&quot; &amp;&amp; Owner_Country__c = &quot;&quot; &amp;&amp; Owner_Name__c = &quot;&quot; &amp;&amp; Owner_State__c = &quot;&quot; &amp;&amp; Owner_Zip_Code__c = &quot;&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Add Project Information To Warranty</fullName>
        <actions>
            <name>Add_Project_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Project_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Project_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Project_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Project_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Project_Zip_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Add the Project Address fields to the warranty record from Opportunity link.</description>
        <formula>RecordType.DeveloperName &lt;&gt; &quot;Roofing&quot; &amp;&amp;
Opportunity__c &lt;&gt; &quot;&quot; &amp;&amp;  Project_Address__c = &quot;&quot; &amp;&amp;  Project_City__c = &quot;&quot; &amp;&amp;  Project_Country__c = &quot;&quot; &amp;&amp;  Project_Name__c = &quot;&quot; &amp;&amp;  Project_State__c = &quot;&quot; &amp;&amp;  Project_Zip_Code__c = &quot;&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Community Submitted</fullName>
        <actions>
            <name>Conga_Merge_STD_Warranty</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>RecordType.DeveloperName = &quot;Customer_Warranty&quot; &amp;&amp;
 Text(Warranty_Status__c) = &quot;Submitted&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Dow Reuse Document Conga WF</fullName>
        <actions>
            <name>Dow_Reuse_Conga_WF_Message</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Warranty__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Roofing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Warranty__c.Intended_Warranty_Product_Template__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Warranty__c.Number_of_Dow_Reuse_Warranty_BGs__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New RSB Warranty Created %28Community%29</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Warranty__c.Community_User_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notification on Intent to Warranty</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Warranty__c.Warranty_Purpose__c</field>
            <operation>equals</operation>
            <value>Intent to Warrant</value>
        </criteriaItems>
        <criteriaItems>
            <field>Warranty__c.Warranty_Status__c</field>
            <operation>equals</operation>
            <value>Submitted,Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Warranty__c.Actual_Complete_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Warranty__c.Expected_Complete_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Time Dependent workflow - email notification at Expected complete date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Notification_Intent_To_Warranty_Nearing_Expected_Start_Date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Warranty__c.Expected_Complete_Date__c</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Warranty Document Conga WF</fullName>
        <actions>
            <name>Warranty_Conga_WF_Message</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Warranty__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Roofing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Warranty__c.Intended_Warranty_Product_Template__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Warranty Extension Document Conga WF</fullName>
        <actions>
            <name>Warranty_Extension_Conga_WF_Message</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Warranty__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Roofing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Warranty__c.Intended_Warranty_Product_Template__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Warranty__c.Warranty_Extended_Product_Template__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Warranty Nearing Expected Complete Date</fullName>
        <active>false</active>
        <description>Send an email to the owner of a warranty and advise them that the warranty is nearing it&apos;s expected completion date and therefore it has been increased by 30 days.</description>
        <formula>(ISPICKVAL(Warranty_Status__c, &apos;Open&apos;) &amp;&amp; ISNULL(Actual_Complete_Date__c) &amp;&amp; 
NOT(ISNULL(Expected_Complete_Date__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_Owner_When_Expected_Complete_Data_Is_Nearing</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Increase_Warranty_Expected_Complete_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Warranty__c.Expected_Complete_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Warranty Not Modified Within Last 90 Days</fullName>
        <active>false</active>
        <description>Send an email to the owner of a warranty and advise them that the warranty is has not been modified within the last 90 days.</description>
        <formula>(ISPICKVAL(Warranty_Status__c, &apos;Open&apos;) &amp;&amp; NOT(ISNULL(Actual_Complete_Date__c)) &amp;&amp; 
ISNULL(Expected_Complete_Date__c))
 || 
(ISPICKVAL(Warranty_Status__c, &apos;Open&apos;) &amp;&amp; ISNULL(Actual_Complete_Date__c) &amp;&amp; 
ISNULL(Expected_Complete_Date__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_Owner_When_Warranty_Hasn_t_Been_Modified_In_90_Days</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Warranty__c.LastModifiedDate</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
