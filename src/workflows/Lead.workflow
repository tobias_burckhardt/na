<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Contact_Us_Eloqua</fullName>
        <description>Contact Us Eloqua to Inside Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>yun.alan@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_Promote_Landing_Page</template>
    </alerts>
    <alerts>
        <fullName>D1_Lead_Score_Notification</fullName>
        <description>D1 Lead Score Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>bellico.william@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_Promote_Landing_Page_Contacts</template>
    </alerts>
    <alerts>
        <fullName>DFW_Email_Notification_to_Field_Rep</fullName>
        <description>DFW-Email Notification to Field Rep</description>
        <protected>false</protected>
        <recipients>
            <recipient>Concrete_R50_South_Central_District_B2</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Roof_Marketing_Campaigns/HCD_HCBI_Newsletter</template>
    </alerts>
    <alerts>
        <fullName>DFW_Email_Notification_to_Field_Rep_RSB</fullName>
        <ccEmails>katic-michalos.tina@us.sika.com</ccEmails>
        <description>DFW-Email Notification to Field Rep- RSB</description>
        <protected>false</protected>
        <recipients>
            <recipient>bohannon.kevin@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/HCD_HCBI_Newsletter</template>
    </alerts>
    <alerts>
        <fullName>DFW_Lead_Follow_up</fullName>
        <description>Chicago Lead Follow up</description>
        <protected>false</protected>
        <recipients>
            <recipient>munt.daniel@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_Promote_Landing_Page</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_for_Lead_Scoring_Achieved</fullName>
        <description>Email Notification for Lead Scoring Achieved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_Promote_Landing_Page</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_to_Inside_Sales_Manager</fullName>
        <description>Email Notification to Inside Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>yun.alan@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_Promote_Landing_Page</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_to_Inside_Sales_Manager_BB</fullName>
        <description>Email Notification to Inside Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Lead_task_notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_to_Inside_Sales_Rep_Lead</fullName>
        <description>Email Notification to Inside Sales Rep</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Lead_task_notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_to_Manager</fullName>
        <description>Email Notification to Insides Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>bellico.william@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_Promote_Landing_Page</template>
    </alerts>
    <alerts>
        <fullName>Flooring_Google_Adwords_Notification_to_Inside_Sales_Manager</fullName>
        <description>Flooring Google Adwords Notification to Inside Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>bellico.william@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_No_Address_Promote_Item</template>
    </alerts>
    <alerts>
        <fullName>Flooring_Phone_Lead</fullName>
        <description>Flooring Phone Lead Notification to Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>yun.alan@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_Promote_Landing_Page</template>
    </alerts>
    <alerts>
        <fullName>Fulfillment_Email</fullName>
        <ccEmails>mochun.stephen@us.sika.com</ccEmails>
        <description>Fulfillment Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>bellico.william@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/HCD_HCBI_Newsletter</template>
    </alerts>
    <alerts>
        <fullName>Google_Adwords_Email_Notification</fullName>
        <description>Google Adwords Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>yun.alan@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_Promote_Landing_Page</template>
    </alerts>
    <alerts>
        <fullName>Google_Adwords_Lead_to_Inside_Sales_Manager</fullName>
        <description>Google Adwords Lead to Inside Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>bellico.william@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_No_Address_Promote_Item</template>
    </alerts>
    <alerts>
        <fullName>Lead_Open_7_Days</fullName>
        <description>Lead Open 7+ Days</description>
        <protected>false</protected>
        <recipients>
            <field>Owners_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Lead_Open_for_7_Days</template>
    </alerts>
    <alerts>
        <fullName>Lead_Score_Lead_Email_Notification_to_Inside_Sales_Manager</fullName>
        <description>Email Notification to Inside Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>bellico.william@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_Promote_Landing_Page</template>
    </alerts>
    <alerts>
        <fullName>Mail_Literature</fullName>
        <description>Mail Literature</description>
        <protected>false</protected>
        <recipients>
            <recipient>bellico.william@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_Promote_Landing_Page</template>
    </alerts>
    <alerts>
        <fullName>New_Lead_Notification</fullName>
        <description>New Lead Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/New_Lead_Notification</template>
    </alerts>
    <alerts>
        <fullName>New_Lead_Notification_Reminder</fullName>
        <description>New Lead Notification Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/New_Lead_Notification_Reminder</template>
    </alerts>
    <alerts>
        <fullName>New_lead_Canada_needs_qualification_from_marketing_team</fullName>
        <description>New lead Canada needs qualification from marketing team</description>
        <protected>false</protected>
        <recipients>
            <recipient>coenen.job@ca.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>courchesne.olivier@ca.sika.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>culis.christophe@ca.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Canada_Templates/New_Lead_needs_to_be_qualified</template>
    </alerts>
    <alerts>
        <fullName>Roofing_Test</fullName>
        <description>Roofing Test</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Campaign_Name_No_Address_Promote_Item</template>
    </alerts>
    <alerts>
        <fullName>Social_Media_Test_2</fullName>
        <description>Social Media Test 2</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Roof_Marketing_Campaigns/Social_Media_Contest</template>
    </alerts>
    <alerts>
        <fullName>Web_Request_Notification</fullName>
        <description>Web Request Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Marketing_Requests/web_request_automotive</template>
    </alerts>
    <fieldUpdates>
        <fullName>Add_To_Campaign_Checkbox</fullName>
        <field>Add_To_Campaign__c</field>
        <literalValue>1</literalValue>
        <name>Add To Campaign Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Canada_Marketing_Queue</fullName>
        <description>Assign to Canada Marketing Queue</description>
        <field>OwnerId</field>
        <lookupValue>Canada_Marketing</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Canada Marketing Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BU_Roofing</fullName>
        <field>BU_Roofing__c</field>
        <literalValue>1</literalValue>
        <name>BU Roofing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Bing_Yahoo_Campaign_Name_Update</fullName>
        <field>Campaign_Name__c</field>
        <formula>&quot;Bing/Yahoo Phone Lead&quot;</formula>
        <name>Bing/Yahoo Campaign Name Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Us_Promo_Item</fullName>
        <description>Update field fro Promo Item for Contact us Eloqua Froms</description>
        <field>Campaign_Promo_Item__c</field>
        <formula>&quot;Contact Us Call&quot;</formula>
        <name>Contact Us Promo Item</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Us_Task_Checkbox</fullName>
        <field>Contact_Us_Task_Fired__c</field>
        <literalValue>1</literalValue>
        <name>Contact Us Task Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Floor_Google_Phone_Lead_Campaign_Name_Up</fullName>
        <field>Campaign_Name__c</field>
        <name>Floor Google Phone Lead Campaign Name Up</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Floor_Google_Phone_Lead_Campaign_update</fullName>
        <field>Campaign_Name__c</field>
        <formula>&quot;Floor Google Adwords Phone Leads&quot;</formula>
        <name>Floor Google Phone Lead Campaign Name Up</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flooring_Adwords_Lead_Owner</fullName>
        <description>Change to Hogan</description>
        <field>OwnerId</field>
        <lookupValue>hogan.paulette@us.sika.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Flooring Adwords Lead Owner</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Free_Eval_Field_Update</fullName>
        <description>Free Roof from Home Page Campaign Promo Item update</description>
        <field>Campaign_Promo_Item__c</field>
        <formula>&quot;Free Eval Call&quot;</formula>
        <name>Free Eval Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Free_Eval_Task_Fired_Checkbox</fullName>
        <field>Free_Eval_Task_Fired__c</field>
        <literalValue>1</literalValue>
        <name>Free Eval Task Fired Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Google_Phone_Lead_Campaign_Name_Update</fullName>
        <field>Campaign_Name__c</field>
        <formula>&quot;Google Adwords Phone Leads&quot;</formula>
        <name>Google Phone Lead Campaign Name Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Campaign_Detail_Field_Update</fullName>
        <field>Campaign_Details__c</field>
        <formula>SUBSTITUTE( Campaign_Details__c , &quot;%20&quot;, &quot; &quot;)</formula>
        <name>Lead Campaign Detail Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Campaign_Name_Field_Update</fullName>
        <field>Campaign_Name__c</field>
        <formula>SUBSTITUTE( Campaign_Name__c , &quot;%20&quot;, &quot; &quot;)</formula>
        <name>Lead Campaign Name Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Campaign_Text_Updates</fullName>
        <field>Campaign_Name__c</field>
        <formula>&quot;Google Adwords Phone Leads&quot;</formula>
        <name>Lead Campaign Text Updates</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Owner_Update</fullName>
        <description>Update to Ellen Walkaman</description>
        <field>OwnerId</field>
        <lookupValue>walkama.ellen@us.sika.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Lead Owner Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Owner_Update_Flooring</fullName>
        <description>Lead Owner updated for Flooring Google Adwords</description>
        <field>OwnerId</field>
        <lookupValue>hogan.paulette@us.sika.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Lead Owner Update Flooring</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Roofing_Vertical_Updates</fullName>
        <field>Roofing_Vertical__c</field>
        <literalValue>Healthcare</literalValue>
        <name>Lead Roofing Vertical Updates</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Score_Checkbox</fullName>
        <field>Lead_Score_Task_Fired__c</field>
        <literalValue>1</literalValue>
        <name>Lead Score Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Score_Update</fullName>
        <description>Lead Schore Field Update for Leads</description>
        <field>Campaign_Promo_Item__c</field>
        <formula>&quot;Lead Score Call&quot;</formula>
        <name>Lead Score Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>On_Form_But_No_Conversion_Task_Checkbox</fullName>
        <field>On_Form_Pages_but_No_Conversions__c</field>
        <literalValue>1</literalValue>
        <name>On Form But No Conversion Task Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PPC_Call_Promo_Item</fullName>
        <description>Add &quot;PPC Call&quot; to the Lead Promo Item Field</description>
        <field>Campaign_Promo_Item__c</field>
        <formula>&quot;PPC Call&quot;</formula>
        <name>PPC Call Promo Item</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PPC_Campaigns_Company_Type_Field_Update</fullName>
        <field>Company_Type__c</field>
        <literalValue>Owner</literalValue>
        <name>PPC Campaigns Company Type Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Promo_Item_Field_Update</fullName>
        <field>Campaign_Promo_Item__c</field>
        <formula>SUBSTITUTE(Campaign_Promo_Item__c, &quot;%20&quot;, &quot; &quot;)</formula>
        <name>Promo Item Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Roofing_Lead</fullName>
        <field>BU_Roofing__c</field>
        <literalValue>1</literalValue>
        <name>Roofing Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TM_Concrete_Check</fullName>
        <description>Check Tm Concrete</description>
        <field>TM_Concrete__c</field>
        <literalValue>1</literalValue>
        <name>TM Concrete Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TM_Industry</fullName>
        <description>Check TM Industry</description>
        <field>TM_Industry__c</field>
        <literalValue>1</literalValue>
        <name>TM Industry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TM_RSB_Check</fullName>
        <description>Check RSB as part of the DFW project</description>
        <field>TM_RSB_Commercial__c</field>
        <literalValue>1</literalValue>
        <name>TM RSB Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TM_Waterproofing_Check</fullName>
        <description>Check TM Waterproofing = TRUE</description>
        <field>TM_Waterproofing__c</field>
        <literalValue>1</literalValue>
        <name>TM Waterproofing Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Status</fullName>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Update Lead Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Adwords Lead Owner Field Updates</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Campaign_Name__c</field>
            <operation>equals</operation>
            <value>Google Adwords Phone Leads</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Adwords Phone Lead Owner Field Updates</fullName>
        <actions>
            <name>PPC_Campaigns_Company_Type_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 3) AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Description</field>
            <operation>contains</operation>
            <value>Recording: http://reports.callcap.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Company_Type__c</field>
            <operation>notEqual</operation>
            <value>Competitor,Engineer,Sub Contractor,Architect,Vendor,Distributor,General Contractor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Google Adwords - Roof Evaluation</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>All Target Markets Contact Us</fullName>
        <actions>
            <name>Eloqua_Test_2</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Building Envelope - Sika USA Website Contact Us,Concrete Contact Us Sika USA Website,Industry Contact Us Sika USA Website,Residential Contact Us Sika USA Website,RSB Contact Us Sika USA Website</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Wood Floor Bonding Contact Us Sika USA Website</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Automotive Web Request</fullName>
        <actions>
            <name>Web_Request_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Campaign_Name__c</field>
            <operation>equals</operation>
            <value>Automotive Contact Us Web Form</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Chicago-Brochure Download</fullName>
        <actions>
            <name>DFW_Lead_Follow_up</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>DFW_Follow_Up</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>EveryDay-Guide</value>
        </criteriaItems>
        <description>Chicago leads with area of interest: General</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Chicago-Interior Finishing</fullName>
        <actions>
            <name>DFW_Follow_up_Building_Envelope</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Lead.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>EveryDay-Guide</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Area_of_Interest__c</field>
            <operation>equals</operation>
            <value>Interior Finishing</value>
        </criteriaItems>
        <description>Chicago Brochure Offer Leads- Lead Owner Marlene. Chicago Follow up to Dan Munt</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact us page task assignment - Roofing</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Contact_Us_Page_New_Lead_Initial_Call</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Contact Us</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>DFW- Transport-Industry</fullName>
        <actions>
            <name>DFW_Follow_Up_Industry</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>Sika-Industry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Area_of_Interest__c</field>
            <operation>contains</operation>
            <value>Auto Glass Replacement,Commercial/ Flat Glass,Insulating Glass</value>
        </criteriaItems>
        <description>Transport-Industry</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DFW-Concrete</fullName>
        <actions>
            <name>DFW_Email_Notification_to_Field_Rep</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>TM_Concrete_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Concrete_DFW_Contact_US</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Lead.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>SikaSmart-Guide</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Area_of_Interest__c</field>
            <operation>equals</operation>
            <value>Concrete admixtures</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Area_of_Interest__c</field>
            <operation>equals</operation>
            <value>Parking garage coatings and sealants</value>
        </criteriaItems>
        <description>DFW leads with are of interest: concrete admixtures and parking garages</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DFW-Industry</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Rep_Lead</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>TM_Industry</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>DFW_Follow_Up_Industry</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>Sika-Industry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Area_of_Interest__c</field>
            <operation>contains</operation>
            <value>Facade,Fenestration,Insulating Glass</value>
        </criteriaItems>
        <description>DFW leads with are of interest in Facade, Fenestration and Insulating Glass</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DFW-RSB</fullName>
        <actions>
            <name>DFW_Email_Notification_to_Field_Rep_RSB</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>TM_RSB_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>DFW_Follow_Up_RSB_Inquiry</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Lead.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>SikaSmart-Guide,DFW-Event</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Area_of_Interest__c</field>
            <operation>equals</operation>
            <value>Balconies</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Area_of_Interest__c</field>
            <operation>equals</operation>
            <value>Concrete repair and protection</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Area_of_Interest__c</field>
            <operation>equals</operation>
            <value>Exterior walls &amp; windows</value>
        </criteriaItems>
        <description>DFW leads with are of interest: balconies, concrete repair and protection, and exterior wall and windows</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DFW-Waterproofing</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Rep_Lead</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>TM_Waterproofing_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>DFW_Follow_Up</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>SikaSmart-Guide</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Area_of_Interest__c</field>
            <operation>contains</operation>
            <value>Below grade waterproofing</value>
        </criteriaItems>
        <description>DFW leads with are of interest: Below Grade Waterproofing</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Decor Roof Guide Task Assignment</fullName>
        <actions>
            <name>Fulfillment_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Decor Design Guide</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>Google Adwords - Remarketing</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Decor Sample Kit</fullName>
        <actions>
            <name>Fulfillment_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Decor Sample Package</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Decor Send Package Trial 2014 Us Roofing</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Downloaded Company Information Follow-Up</fullName>
        <actions>
            <name>Downloaded_Company_Information_Follow_up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Downloaded Company Information</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Eloqua Flooring lead scoring rule</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Score_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Score_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Score_Achieved</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Eloqua_Lead_Rating__c</field>
            <operation>equals</operation>
            <value>A2,A1,A3,B1,B2,C1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TM_Flooring__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Eloqua_Form_ID__c</field>
            <operation>notEqual</operation>
            <value>Free Floor Eval- Organic</value>
        </criteriaItems>
        <description>Flooring Lead Score Achieved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Eloqua lead scoring rule</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Manager_BB</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Score_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Score_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Score_Achieved</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Eloqua_Lead_Rating__c</field>
            <operation>equals</operation>
            <value>A2,A1,B1,C1,B2,C3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TM_Roofing__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Eloqua lead scoring rule D1</fullName>
        <actions>
            <name>D1_Lead_Score_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Score_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Eloqua_Lead_Rating__c</field>
            <operation>equals</operation>
            <value>D1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Lead Score of D1 lead swill go to Inside Sales Manager</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Energy Saving Kit Task Assignment</fullName>
        <actions>
            <name>Fulfillment_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Energy Saving Kit</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Event Attendee Follow Up</fullName>
        <actions>
            <name>Every_Day_Building_Envelope_Follow_Up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>NYC</value>
        </criteriaItems>
        <description>Leads attended the NYC event. Please look at lead comments section and area of interest for more details on lead.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>F-google Adwords</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Owner_Update_Flooring</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flooring_Google_AdwordsFollow_up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Floor Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>Free Floor Eval- Paid</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.DIY__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Floor Google Phone Leads Task Assignment</fullName>
        <actions>
            <name>Flooring_Phone_Lead</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Floor_Google_Phone_Lead_Campaign_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flooring_Adwords_Lead_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Google_Phone_Lead</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Description</field>
            <operation>contains</operation>
            <value>Recording</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Floor Marketing Campaigns</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flooring-Contact Us</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Eloqua_Test_2</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR (1 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Floor Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>F-Contact Us</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>F-Contact Us</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Free Floor Assessment- Organic</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>F_Free_Floor_Assessment_Follow_up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Floor Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>Free Floor Eval- Organic</value>
        </criteriaItems>
        <description>Website Free Floor Consultation Requests- MOVED TO PARDOT TASK</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Fulfillment</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Sample Requested</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Sample_Requested_Follow_Up</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Google Adwords Eloqua Forms-Bill</fullName>
        <actions>
            <name>Google_Adwords_Lead_to_Inside_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Google_Adwords_Eloqua_Follow_Up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>Google Adwords,Google Adwords- Sikacoat</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.State</field>
            <operation>equals</operation>
            <value>American Samoa,Guam,Micronesia,Marianas,Puerto Rico,Vigin Islands</value>
        </criteriaItems>
        <description>Inside Sales Manager missed States</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Google Phone Leads Task Assignment</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Google_Phone_Lead_Campaign_Name_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PPC_Call_Promo_Item</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Google_Phone_Leads_Follow_Up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Description</field>
            <operation>contains</operation>
            <value>Recording</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Green Roof Guide Task Assignment</fullName>
        <actions>
            <name>Fulfillment_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Green Roof Guide</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Green Roof Page Task Assignment</fullName>
        <actions>
            <name>Email_Notification_to_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Green_Roof_Webpage_Literature_New_Lead_Initial_Call</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Green Roof Webpage</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>HCD %26 HCBI Campaign Task Assignment</fullName>
        <actions>
            <name>Email_Notification_to_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>HCD_HCBI_Campaign_New_Lead_Initial_Call</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>HCD &amp; HCBI</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>HCD%26HCBI Healthcare Update</fullName>
        <actions>
            <name>Lead_Roofing_Vertical_Updates</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>HCD &amp; HCBI</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Hanley Wood University</fullName>
        <actions>
            <name>Hanley_Wood_University_Lead_Call_AIA_Presentation_Downloaded_Packet_Sent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Architect - Hanley Wood CEU Course</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IFMA</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>IFMA_Initial</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>IFMA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Landscape Architecture Magazine Task Assignment</fullName>
        <actions>
            <name>Email_Notification_to_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Landscape_Architecture_Magazine_New_Lead_Initial_Call</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Landscape Architecture Magazine</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Account Team</fullName>
        <active>false</active>
        <formula>OR(
CreatedBy.ProfileId= &quot;00e40000000jlWp&quot;,
CreatedBy.ProfileId =&quot;00e40000000jlhs&quot;,
CreatedBy.ProfileId =&quot;00e40000000jmAL&quot;,
LastModifiedBy.ProfileId= &quot;00e40000000jlWp&quot;,
LastModifiedBy.ProfileId =&quot;00e40000000jlhs&quot;,
LastModifiedBy.ProfileId =&quot;00e40000000jmAL&quot;,
OwnerId = &quot;005400000019gXr&quot;,
OwnerId = &quot;005400000019LiV&quot; ,
OwnerId = &quot;005400000019LXl&quot; ,
OwnerId = &quot;005400000019Lic&quot; ,
OwnerId = &quot;005400000019Lae&quot; ,
OwnerId = &quot;005400000019Lgw&quot; ,
OwnerId = &quot;005400000019LgK&quot; ,
OwnerId = &quot;005400000019Xcx&quot; ,
OwnerId = &quot;005400000019LcG&quot; ,
OwnerId = &quot;005400000019LcE&quot; ,
OwnerId = &quot;005400000019LiP&quot; ,
OwnerId = &quot;005400000019Xcy&quot; ,
OwnerId = &quot;005400000019LjN&quot; ,
OwnerId = &quot;005400000019LiB&quot; ,
OwnerId = &quot;005400000019Lcs&quot; ,
OwnerId = &quot;005400000019Lc6&quot; ,
OwnerId = &quot;005400000019Lc8&quot; ,
OwnerId = &quot;005400000019Lgx&quot; ,
OwnerId = &quot;005400000019LcF&quot; ,
OwnerId = &quot;005400000019Xcz&quot; ,
OwnerId = &quot;005400000019LbE&quot; ,
OwnerId = &quot;005400000019LhB&quot; ,
OwnerId = &quot;005400000019Lgy&quot; ,
OwnerId = &quot;005400000019Lbn&quot; ,
OwnerId = &quot;005400000019LcR&quot; ,
OwnerId = &quot;00540000001mHG1&quot; ,
OwnerId = &quot;005400000019LXl&quot; ,
OwnerId = &quot;005400000019dGn&quot; ,
OwnerId = &quot;005400000019gUY&quot; ,
OwnerId = &quot;005400000019gUi&quot; ,
OwnerId = &quot;005400000019gUn&quot; ,
OwnerId = &quot;005400000019gUs&quot; ,
OwnerId = &quot;00540000001mHG6&quot; ,
OwnerId = &quot;00540000001mHGB&quot; ,
OwnerId = &quot;00540000001mHGG&quot; ,
OwnerId = &quot;005400000019Lb3&quot; ,
OwnerId = &quot;005400000019LiO&quot; ,
OwnerId = &quot;005400000019Lct&quot; ,
OwnerId = &quot;00540000001mdTU&quot; ,
OwnerId = &quot;00540000001mx7y&quot; ,
OwnerId = &quot;005400000019Lid&quot; ,
OwnerId = &quot;005400000019dGi&quot; ,
OwnerId = &quot;005400000019LbD&quot; ,
OwnerId = &quot;005400000019LcY&quot; ,
OwnerId = &quot;005400000019dGs&quot; ,
OwnerId = &quot;005400000019Laa&quot; ,
OwnerId = &quot;005400000019LdL&quot; ,
OwnerId = &quot;005400000019LaW&quot; ,
OwnerId = &quot;00540000001mQoO&quot; ,
OwnerId = &quot;005400000019LiE&quot; ,
OwnerId = &quot;005400000019LaX&quot; ,
OwnerId = &quot;005400000019Lac&quot; ,
OwnerId = &quot;005400000019LhJ&quot; ,
OwnerId = &quot;005400000019LaQ&quot; ,
OwnerId = &quot;005400000019LiF&quot; ,
OwnerId = &quot;00540000001mb9D&quot; ,
OwnerId = &quot;005400000019LhH&quot; ,
OwnerId = &quot;005400000019LcB&quot; ,
OwnerId = &quot;005400000019LcC&quot; ,
OwnerId = &quot;005400000019U8L&quot; ,
OwnerId = &quot;005400000019Lbc&quot; ,
OwnerId = &quot;00540000001mx7x&quot; ,
OwnerId = &quot;005400000019Lad&quot; ,
OwnerId = &quot;005400000019Lba&quot; ,
OwnerId = &quot;005400000019LhX&quot; ,
OwnerId = &quot;005400000019Lc9&quot; ,
OwnerId = &quot;005400000019LaZ&quot; ,
OwnerId = &quot;005400000019LbS&quot; ,
OwnerId = &quot;005400000019Laf&quot; ,
OwnerId = &quot;005400000019Lb2&quot; ,
OwnerId = &quot;005400000019Lb4&quot; ,
OwnerId = &quot;00540000001mPwq&quot; ,
OwnerId = &quot;005400000019LbR&quot; ,
OwnerId = &quot;005400000019LbU&quot; ,
OwnerId = &quot;005400000019Lh8&quot; ,
OwnerId = &quot;005400000019Lbt&quot; ,
OwnerId = &quot;005400000019LcA&quot; ,
OwnerId = &quot;005400000019LhI&quot; ,
OwnerId = &quot;005400000019LiN&quot; ,
OwnerId = &quot;005400000019LaY&quot; ,
OwnerId = &quot;005400000019Lab&quot; ,
OwnerId = &quot;005400000019LhT&quot; ,
OwnerId = &quot;005400000019Lc2&quot; ,
OwnerId = &quot;005400000019LcD&quot; ,
OwnerId = &quot;00540000001mx7z&quot; ,
OwnerId = &quot;005400000019Lc4&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Campaign Detail Field Update</fullName>
        <actions>
            <name>Lead_Campaign_Detail_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Campaign_Details__c</field>
            <operation>contains</operation>
            <value>%20</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Campaign Name Field Update</fullName>
        <actions>
            <name>Lead_Campaign_Name_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Campaign_Name__c</field>
            <operation>contains</operation>
            <value>%20</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Promo Item Field Update</fullName>
        <actions>
            <name>Promo_Item_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>contains</operation>
            <value>%20</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Scoring For Leads</fullName>
        <actions>
            <name>Email_Notification_for_Lead_Scoring_Achieved</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Score_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Score_Achieved</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Eloqua_Lead_Rating__c</field>
            <operation>equals</operation>
            <value>A2,A1,B1,C1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead qualification notification email</fullName>
        <actions>
            <name>Assign_to_Canada_Marketing_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Sika CAN Web Contact Us</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Olivier Courchesne</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Pardot</value>
        </criteriaItems>
        <description>Canada lead creation from contact us web request triggers a notification email</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Linkedin Advertising New Lead Assignment</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Linkedin_Advertising_Roof_Evaluation_New_Lead_Initial_Call</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Linkedin Advertising</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Literature Request page task assignment</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Contact Us</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lunch and Learn</fullName>
        <actions>
            <name>Lunch_And_Learn_Requested</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Waterproofing_AIA; Roofing AIA</value>
        </criteriaItems>
        <description>Lunch and Learn offer for all TM&apos;s</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Maintenance Solution - Top Product Task Assignment</fullName>
        <actions>
            <name>Maintenance_Solutions_Top_Products_Lead_Initial_Phone_Call</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Maintenance Solutions - Top Products</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Maintenance Solution Task Assignment</fullName>
        <actions>
            <name>Email_Notification_to_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Maintenance_Solution_Literature_New_Lead_Initial_Call</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Maintenance Solutions</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Maintenance Workbook Campaign Task Assignment</fullName>
        <actions>
            <name>Fulfillment_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Roof Maintenance Workbook</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Maintenance Workbook send out Book</fullName>
        <actions>
            <name>Fulfillment_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Roof Maintenance Workbook</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Company_Type__c</field>
            <operation>equals</operation>
            <value>Owner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Eloqua_Form_ID__c</field>
            <operation>equals</operation>
            <value>Maintenance Free Workbook</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PreCast Follow Up</fullName>
        <actions>
            <name>WOC_Follow_Up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Campaign_Name__c</field>
            <operation>equals</operation>
            <value>PreCast Show</value>
        </criteriaItems>
        <description>Lead expressed interest in our products at the PreCast Show</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof Evaluation %28Organic%29</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Free_Roof_Evaluation_Initial_Call</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Free Roof Evaluation</value>
        </criteriaItems>
        <description>Free Roof Eval request from Website (organic)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof PPC</fullName>
        <actions>
            <name>Google_Adwords_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Google_Adwords_Follow_up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Google Adwords</value>
        </criteriaItems>
        <description>PPC Request for leads</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof Quote Requested -Lead</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Quote_Initial_Call</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Quote Requested</value>
        </criteriaItems>
        <description>Roof Quote Requested from website (organic)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roof Sample Requested</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Sample Requested</value>
        </criteriaItems>
        <description>Membrane sample requested. Check with Brian Blaquiere if shipped</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Sample_Requested_Follow_Up</name>
                <type>Task</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Roofing Fulfillment</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Sample Requested</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Sample_Requested_Follow_Up</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Roofing Google Adwords Call Center Task</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New_Lead_Initial_Call</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Google Adwords - Roof Evaluation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Campaign_Details__c</field>
            <operation>notContain</operation>
            <value>Google Adwords Remarketing</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing Lead</fullName>
        <actions>
            <name>Roofing_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(
CreatedBy.ProfileId= &quot;00e40000000jlWp&quot;,
CreatedBy.ProfileId =&quot;00e40000000jlhs&quot;,
CreatedBy.ProfileId =&quot;00e40000000jmAL&quot;,
LastModifiedBy.ProfileId= &quot;00e40000000jlWp&quot;,
LastModifiedBy.ProfileId =&quot;00e40000000jlhs&quot;,
LastModifiedBy.ProfileId =&quot;00e40000000jmAL&quot;,
OwnerId = &quot;005400000019gXr&quot;,
OwnerId = &quot;005400000019LiV&quot; ,
OwnerId = &quot;005400000019LXl&quot; ,
OwnerId = &quot;005400000019Lic&quot; ,
OwnerId = &quot;005400000019Lae&quot; ,
OwnerId = &quot;005400000019Lgw&quot; ,
OwnerId = &quot;005400000019LgK&quot; ,
OwnerId = &quot;005400000019Xcx&quot; ,
OwnerId = &quot;005400000019LcG&quot; ,
OwnerId = &quot;005400000019LcE&quot; ,
OwnerId = &quot;005400000019LiP&quot; ,
OwnerId = &quot;005400000019Xcy&quot; ,
OwnerId = &quot;005400000019LjN&quot; ,
OwnerId = &quot;005400000019LiB&quot; ,
OwnerId = &quot;005400000019Lcs&quot; ,
OwnerId = &quot;005400000019Lc6&quot; ,
OwnerId = &quot;005400000019Lc8&quot; ,
OwnerId = &quot;005400000019Lgx&quot; ,
OwnerId = &quot;005400000019LcF&quot; ,
OwnerId = &quot;005400000019Xcz&quot; ,
OwnerId = &quot;005400000019LbE&quot; ,
OwnerId = &quot;005400000019LhB&quot; ,
OwnerId = &quot;005400000019Lgy&quot; ,
OwnerId = &quot;005400000019Lbn&quot; ,
OwnerId = &quot;005400000019LcR&quot; ,
OwnerId = &quot;00540000001mHG1&quot; ,
OwnerId = &quot;005400000019LXl&quot; ,
OwnerId = &quot;005400000019dGn&quot; ,
OwnerId = &quot;005400000019gUY&quot; ,
OwnerId = &quot;005400000019gUi&quot; ,
OwnerId = &quot;005400000019gUn&quot; ,
OwnerId = &quot;005400000019gUs&quot; ,
OwnerId = &quot;00540000001mHG6&quot; ,
OwnerId = &quot;00540000001mHGB&quot; ,
OwnerId = &quot;00540000001mHGG&quot; ,
OwnerId = &quot;005400000019Lb3&quot; ,
OwnerId = &quot;005400000019LiO&quot; ,
OwnerId = &quot;005400000019Lct&quot; ,
OwnerId = &quot;00540000001mdTU&quot; ,
OwnerId = &quot;00540000001mx7y&quot; ,
OwnerId = &quot;005400000019Lid&quot; ,
OwnerId = &quot;005400000019dGi&quot; ,
OwnerId = &quot;005400000019LbD&quot; ,
OwnerId = &quot;005400000019LcY&quot; ,
OwnerId = &quot;005400000019dGs&quot; ,
OwnerId = &quot;005400000019Laa&quot; ,
OwnerId = &quot;005400000019LdL&quot; ,
OwnerId = &quot;005400000019LaW&quot; ,
OwnerId = &quot;00540000001mQoO&quot; ,
OwnerId = &quot;005400000019LiE&quot; ,
OwnerId = &quot;005400000019LaX&quot; ,
OwnerId = &quot;005400000019Lac&quot; ,
OwnerId = &quot;005400000019LhJ&quot; ,
OwnerId = &quot;005400000019LaQ&quot; ,
OwnerId = &quot;005400000019LiF&quot; ,
OwnerId = &quot;00540000001mb9D&quot; ,
OwnerId = &quot;005400000019LhH&quot; ,
OwnerId = &quot;005400000019LcB&quot; ,
OwnerId = &quot;005400000019LcC&quot; ,
OwnerId = &quot;005400000019U8L&quot; ,
OwnerId = &quot;005400000019Lbc&quot; ,
OwnerId = &quot;00540000001mx7x&quot; ,
OwnerId = &quot;005400000019Lad&quot; ,
OwnerId = &quot;005400000019Lba&quot; ,
OwnerId = &quot;005400000019LhX&quot; ,
OwnerId = &quot;005400000019Lc9&quot; ,
OwnerId = &quot;005400000019LaZ&quot; ,
OwnerId = &quot;005400000019LbS&quot; ,
OwnerId = &quot;005400000019Laf&quot; ,
OwnerId = &quot;005400000019Lb2&quot; ,
OwnerId = &quot;005400000019Lb4&quot; ,
OwnerId = &quot;00540000001mPwq&quot; ,
OwnerId = &quot;005400000019LbR&quot; ,
OwnerId = &quot;005400000019LbU&quot; ,
OwnerId = &quot;005400000019Lh8&quot; ,
OwnerId = &quot;005400000019Lbt&quot; ,
OwnerId = &quot;005400000019LcA&quot; ,
OwnerId = &quot;005400000019LhI&quot; ,
OwnerId = &quot;005400000019LiN&quot; ,
OwnerId = &quot;005400000019LaY&quot; ,
OwnerId = &quot;005400000019Lab&quot; ,
OwnerId = &quot;005400000019LhT&quot; ,
OwnerId = &quot;005400000019Lc2&quot; ,
OwnerId = &quot;005400000019LcD&quot; ,
OwnerId = &quot;00540000001mx7z&quot; ,
OwnerId = &quot;005400000019Lc4&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Roofing Lead Check</fullName>
        <actions>
            <name>Roofing_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check if Current Owner or Last Modified is Roofing User</description>
        <formula>ISPICKVAL ( LastModifiedBy.Target_Market__c, &quot;Roofing&quot; ) || 
ISPICKVAL (  Owner:User.Target_Market__c , &quot;Roofing&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing Make Outbound Calls</fullName>
        <actions>
            <name>Make_Outbound_Call</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Make Outbound Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LastModifiedById</field>
            <operation>notEqual</operation>
            <value>Data.com Clean</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing- Contact Us Request</fullName>
        <actions>
            <name>Contact_Us_Eloqua</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Contact_Us_Task_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Eloqua_Test_2</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Contact Us</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>STP Camapaign Task</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sustainability_That_Pays_com_New_Lead_Literature_Initial_Call</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Sustainability That Pays</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Sweet Follow Up Assignment</fullName>
        <actions>
            <name>Send_Sweets_and_Follow_up_Call</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Sweets and Letter</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sika Sarnafil E-Newsletter %28Literature%29 Campaign Task Assignment</fullName>
        <actions>
            <name>Email_Notification_to_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sika_Sarnafil_E_newsletter_New_Lead_Initial_Call</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Sika Sarnafil E-newsletter</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sika Sarnafil Government Newsletter Task Assignment</fullName>
        <actions>
            <name>Email_Notification_to_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sika_Sarnafil_Government_Newsletter_New_Lead_Literature_Initial_Call</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Sika Sarnafil Government Newsletter</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sika Sarnafil Healthcare Newsletter Task Assignment</fullName>
        <actions>
            <name>Email_Notification_to_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sika_Sarnafil_Healthcare_Newsletter</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Sika Sarnafil Healthcare Newsletter</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sikacoat Google Phone Leads Task Assignment</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Owner_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PPC_Call_Promo_Item</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sikacoat_Follow_up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Description</field>
            <operation>contains</operation>
            <value>Recording</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Sikacoat Google campaigns</value>
        </criteriaItems>
        <description>Sikacoat only Google Campaigns</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sikacoat Maintenance Kit Auto Task</fullName>
        <actions>
            <name>Follow_up_on_SikaCoat_Maintenance_Kit_Delivery</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Sikacoat Maintenance Kit</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sikacoat-Bing Phone Leads Task Assignment</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Description</field>
            <operation>contains</operation>
            <value>Recording</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Sikacoat Bing Campaigns</value>
        </criteriaItems>
        <description>Sikacoat</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Social Media Leads Task Assignment</fullName>
        <actions>
            <name>Social_Media_Leads_Initial_Call</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Social Media Leads</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Social Media Task Assignment</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Social_Media_New_Lead_Initial_Call</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Social Media</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sweet New Lead Initial Call</fullName>
        <actions>
            <name>Email_Notification_to_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Send_Sweets_and_Follow_up_Call</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Sweets</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Targeted Leads Auto Task</fullName>
        <actions>
            <name>Targeted_Leads_Outbound_Call</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Targeted Leads Outbound Call</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Waterproofing Web Contact Us</fullName>
        <actions>
            <name>Waterproofing_Contact_Us_Follow_Up</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Waterproofing Contact Us Sika USA Website</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Webinar or CE Course</fullName>
        <actions>
            <name>Webinar_or_CE_Course_Follow_Up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>Webinar or CE Course</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>What it takes booklet task assignment</fullName>
        <actions>
            <name>Fulfillment_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Roof Marketing Campaigns</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Campaign_Promo_Item__c</field>
            <operation>equals</operation>
            <value>What It Takes Booklet</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Yahoo %26 Bing Pay-Per-Click Campaign Free Roof Evaluation</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Bing_Yahoo_Pay_Per_Click_Campaign_Free_Roof_Evaluation</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Yahoo/Bing Pay-Per-Click Campaign</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Yaoo%2FBing Phone Leads Task Assignment</fullName>
        <actions>
            <name>Email_Notification_to_Inside_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Bing_Yahoo_Campaign_Name_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PPC_Call_Promo_Item</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Bing_Yahoo_Pay_Per_Click_Campaign_Free_Roof_Evaluation</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Description</field>
            <operation>contains</operation>
            <value>Recording: http://reports.callcap.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Yahoo/Bing Roof Campaigns</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>AEC_Daily_New_Lead_Initial_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>AEC Daily New Lead Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>BOM_FacilitiesNet_Link</fullName>
        <assignedToType>owner</assignedToType>
        <description>Free Roof Evaluation</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>BOM - FacilitiesNet Link (Roof Evaluation) New Lead Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>Bing_Yahoo_Pay_Per_Click_Campaign_Free_Roof_Evaluation</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Bing/Yahoo Pay-Per-Click Campaign Free Roof Evaluation</subject>
    </tasks>
    <tasks>
        <fullName>Concrete_DFW_Contact_US</fullName>
        <assignedTo>Concrete_R50_South_Central_District_B2</assignedTo>
        <assignedToType>role</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>DFW Contact US Concrete Inquiry</subject>
    </tasks>
    <tasks>
        <fullName>Contact_Us_Page_New_Lead_Initial_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Contact Us Page New Lead Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>DFW_Follow_Up</fullName>
        <assignedToType>owner</assignedToType>
        <description>Download EveryDay Brochure Follow Up for Chicago</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Download Brochure -Chicago Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>DFW_Follow_Up_Industry</fullName>
        <assignedToType>owner</assignedToType>
        <description>DFW Follow up for Industry</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>DFW-Follow Up-Industry</subject>
    </tasks>
    <tasks>
        <fullName>DFW_Follow_Up_RSB_Inquiry</fullName>
        <assignedTo>bohannon.kevin@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>DFW-Follow Up RSB Inquiry</subject>
    </tasks>
    <tasks>
        <fullName>DFW_Follow_up_Building_Envelope</fullName>
        <assignedTo>munt.daniel@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Download  EveryDay Brochure Follow Up for Chicago.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Donwloaded Brochure- Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Downloaded_Company_Information_Follow_up</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Downloaded Company Information Follow-up</subject>
    </tasks>
    <tasks>
        <fullName>E_Newsletter_Free_Roof_Eval</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>E-Newsletter Free Roof Eval</subject>
    </tasks>
    <tasks>
        <fullName>Eloqua_Test_2</fullName>
        <assignedToType>owner</assignedToType>
        <description>Please follow up on this Web Lead</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Contact Us Follow Up Call</subject>
    </tasks>
    <tasks>
        <fullName>Energy_Saving_Kit_and_Follow_up_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Energy Saving Kit and Follow-up Call</subject>
    </tasks>
    <tasks>
        <fullName>Every_Day_Building_Envelope_Follow_Up</fullName>
        <assignedToType>owner</assignedToType>
        <description>City Event campaign: Contacts who attended the city event</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Event Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>F_Free_Floor_Assessment_Follow_up</fullName>
        <assignedTo>hogan.paulette@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>F- Free Floor Assessment Follow up</subject>
    </tasks>
    <tasks>
        <fullName>Flooring_Google_AdwordsFollow_up</fullName>
        <assignedTo>hogan.paulette@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Flooring Google AdwordsFollow up</subject>
    </tasks>
    <tasks>
        <fullName>Follow_up_on_SikaCoat_Maintenance_Kit_Delivery</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Follow up on SikaCoat Maintenance Kit Delivery</subject>
    </tasks>
    <tasks>
        <fullName>Four_Steps_to_Maintenance_Free_Roof_Delivery_and_Follow_up_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Downloaded Four Steps to Maintenance Free Roof Whitepaper and Follow-up Call</subject>
    </tasks>
    <tasks>
        <fullName>Free_Roof_Evaluation_Follow_Up_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Free Roof Evaluation Follow Up Call</subject>
    </tasks>
    <tasks>
        <fullName>Free_Roof_Evaluation_Initial_Call</fullName>
        <assignedToType>owner</assignedToType>
        <description>New Lead coming from Sika Sarnafil Website. Offer is to request a Free Roof Evaluation</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Free Roof Evaluation Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>Google_Adwords_Eloqua_Follow_Up</fullName>
        <assignedTo>bellico.william@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>PPC Adwords Eloqua Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Google_Adwords_Follow_up</fullName>
        <assignedToType>owner</assignedToType>
        <description>PPC Lead - Hot Leads</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>PPC Adwords Follow up</subject>
    </tasks>
    <tasks>
        <fullName>Google_Adwords_Remarketing_Roof_Evaluation_Downloaded_Maintenance_Free_Whitepape</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Google Adwords Remarketing - Roof Evaluation &amp; Downloaded Maintenance Free Whitepaper</subject>
    </tasks>
    <tasks>
        <fullName>Google_Adwords_Remarketing_Roof_Evaluation_Send_out_Decor_Design_Guide</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Google Adwords Remarketing - Roof Evaluation &amp; Send out Decor Design Guide</subject>
    </tasks>
    <tasks>
        <fullName>Google_Adwords_Remarketing_Roof_Evaluation_Send_out_Energy_Saving_Kit</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Google Adwords Remarketing - Roof Evaluation &amp; Send out Energy Saving Kit</subject>
    </tasks>
    <tasks>
        <fullName>Google_Adwords_Remarketing_Roof_Evaluation_Send_out_Green_Roof_Guide</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Google Adwords Remarketing - Roof Evaluation &amp; Send out Green Roof Guide</subject>
    </tasks>
    <tasks>
        <fullName>Google_Adwords_Remarketing_Roof_Evaluation_Send_out_Roof_Maintenance_Workbook</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Google Adwords Remarketing - Roof Evaluation &amp; Send out Roof Maintenance Workbook</subject>
    </tasks>
    <tasks>
        <fullName>Google_Adwords_Remarketing_Roof_Evaluation_Send_out_What_It_Takes_Booklet</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Google Adwords Remarketing - Roof Evaluation &amp; Send out What It Takes Booklet</subject>
    </tasks>
    <tasks>
        <fullName>Google_Phone_Lead</fullName>
        <assignedTo>hogan.paulette@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Google Phone Lead</subject>
    </tasks>
    <tasks>
        <fullName>Google_Phone_Leads_Follow_Up</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Google Phone Leads Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Green_Roof_Webpage_Literature_New_Lead_Initial_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Green Roof Webpage (Literature) New Lead Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>HCD_HCBI_Campaign_New_Lead_Initial_Call</fullName>
        <assignedToType>owner</assignedToType>
        <description>Send Literature &amp; Follow Up Call in Two Week</description>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>HCD&amp;HCBI Campaign (Literature) New Lead Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>Hanley_Wood_University_Lead_Call_AIA_Presentation_Downloaded_Packet_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Hanley Wood University Lead Call (AIA Presentation Downloaded &amp; Packet Sent)</subject>
    </tasks>
    <tasks>
        <fullName>IFMA_Initial</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>IFMA Show New Lead Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>Landscape_Architecture_Magazine_New_Lead_Initial_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Landscape Architecture Magazine New Lead (Literature) Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>Lead_Score_Achieved</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Lead Score Achieved - Follow Up Call</subject>
    </tasks>
    <tasks>
        <fullName>Linkedin_Advertising_Roof_Evaluation_New_Lead_Initial_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Linkedin Advertising - Roof Evaluation New Lead Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>Lunch_And_Learn_Requested</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Lunch And Learn Requested</subject>
    </tasks>
    <tasks>
        <fullName>Maintenance_Solution_Literature_New_Lead_Initial_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Maintenance Solution (Literature) New Lead Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>Maintenance_Solutions</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Maintenance Solutions (Literature) New Lead Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>Maintenance_Solutions_Top_Products_Lead_Initial_Phone_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Maintenance Solutions - Top Products Lead (Literature) Initial Phone Call</subject>
    </tasks>
    <tasks>
        <fullName>Maintenance_Workbook_Delivery_and_Follow_up_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Maintenance Workbook Delivery and Follow-up Call</subject>
    </tasks>
    <tasks>
        <fullName>Maintenance_Workbook_Web_Request_Lead_Initial_Call</fullName>
        <assignedToType>owner</assignedToType>
        <description>Send Workbook and Follow Up Call Within Two Week</description>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Maintenance Workbook Web Request (Literature) Lead Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>Make_Outbound_Call</fullName>
        <assignedToType>owner</assignedToType>
        <description>Lead is coming from a 3rd Party. Responded to our Campaign and requested more info.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Make Outbound Call</subject>
    </tasks>
    <tasks>
        <fullName>New_Lead_Initial_Call</fullName>
        <assignedToType>owner</assignedToType>
        <description>PPC Lead- Hot Leads. Requesting to get more info our roofing products and/ or get Free Roof Evaluation.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Google Adwords-Roof Evaluation Campaign New Lead Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>On_Form_Pages_but_No_Conversions_Follow_Up</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>On Form Pages but No Conversions Follow Up Call</subject>
    </tasks>
    <tasks>
        <fullName>Quote_Initial_Call</fullName>
        <assignedToType>owner</assignedToType>
        <description>Roof Quote Requested via form fill out from website</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Roof Quote Requested</subject>
    </tasks>
    <tasks>
        <fullName>Roof_Evaluation_from_Home_Page_Campaign_Lead_Initial_Call</fullName>
        <assignedToType>owner</assignedToType>
        <description>Free Roof Evaluation</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Roof Evaluation from Home Page Campaign Lead Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>Sample_Requested_Follow_Up</fullName>
        <assignedToType>owner</assignedToType>
        <description>Roofing Membrane Sample was requested from our website. Sample was send 10 days ago.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Sample Requested Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Send_Decor_Roof_Guide_and_Follow_Up_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Decor Roof Guide and Follow Up Call</subject>
    </tasks>
    <tasks>
        <fullName>Send_Green_Roof_Guide_Follow_up_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Send Green Roof Guide &amp; Follow-up Call</subject>
    </tasks>
    <tasks>
        <fullName>Send_Sweets_and_Follow_up_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Send Sweets and Follow-up Call</subject>
    </tasks>
    <tasks>
        <fullName>Sika_Sarnafil_E_newsletter_New_Lead_Initial_Call</fullName>
        <assignedToType>owner</assignedToType>
        <description>Send Literature and Follow Up Call Within Two Week</description>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Sika Sarnafil E-newsletter (Literature) New Lead Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>Sika_Sarnafil_Government_Newsletter_New_Lead_Literature_Initial_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Sika Sarnafil Government Newsletter New Lead (Literature) Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>Sika_Sarnafil_Healthcare_Newsletter</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Sika Sarnafil Healthcare Newsletter(Literature) New Lead Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>Sikacoat_Follow_up</fullName>
        <assignedTo>walkama.ellen@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Sikacoat Follow up</subject>
    </tasks>
    <tasks>
        <fullName>Social_Media_Leads_Initial_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Social Media Leads Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>Social_Media_New_Lead_Initial_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Social Media New Lead Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>Sustainability_That_Pays_com_New_Lead_Literature_Initial_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Sustainability That Pays.com New Lead (Literature) Initial Call</subject>
    </tasks>
    <tasks>
        <fullName>Targeted_Leads_Outbound_Call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Targeted Leads Outbound Call</subject>
    </tasks>
    <tasks>
        <fullName>Test_Eloqua_Campaign</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Test Eloqua Campaign</subject>
    </tasks>
    <tasks>
        <fullName>WOC_Follow_Up</fullName>
        <assignedToType>owner</assignedToType>
        <description>Lead expressed interest in our products at PreCast Trade Show</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Trade Show Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Waterproofing_Contact_Us_Follow_Up</fullName>
        <assignedTo>dougherty.elizabeth@us.sika.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Waterproofing Web Contact Us Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>Webinar_or_CE_Course_Follow_Up</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Webinar or CE Course Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>What_it_takes_booklet</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Send what it takes booklet and follow up call</subject>
    </tasks>
    <tasks>
        <fullName>facilityzone_com_energy_saving_kit_new_lead_initial_call</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>facilityzone.com (energy saving kit) new lead initial call</subject>
    </tasks>
</Workflow>
