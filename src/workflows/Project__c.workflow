<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EMail_Notification_to_Project_Manager</fullName>
        <description>EMail Notification to Project Manager</description>
        <protected>false</protected>
        <recipients>
            <field>Sika_Project_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Template/CST_Project_Manager_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>BU_Concrete_Check</fullName>
        <description>Check BU Concrete True</description>
        <field>TM_Concrete__c</field>
        <literalValue>1</literalValue>
        <name>BU Concrete Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CNT_Check_Box</fullName>
        <field>TM_RSB_Commercial__c</field>
        <literalValue>1</literalValue>
        <name>CNT Check Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flooring_Check_True</fullName>
        <field>TM_Flooring__c</field>
        <literalValue>1</literalValue>
        <name>Flooring Check True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MHC_ID_Format_Change</fullName>
        <description>Convert MHC2013123456 to 13-123456</description>
        <field>Alternate_MHC_ID__c</field>
        <formula>MID(ExternalID__c, 6,2)&amp;&quot;-&quot;&amp; RIGHT(ExternalID__c,6)</formula>
        <name>MHC ID Format Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Project_Record_Type_Data</fullName>
        <description>Change Project Record Type to &quot;Data Entered&quot;</description>
        <field>RecordTypeId</field>
        <lookupValue>Data_Entered</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Project Record Type Data</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Roofing_Project_Update</fullName>
        <field>TM_Roofing__c</field>
        <literalValue>1</literalValue>
        <name>Roofing Project Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TM_BES_TRUE</fullName>
        <description>Mark Building Envelope TM True</description>
        <field>TM_Building_Envelope__c</field>
        <literalValue>1</literalValue>
        <name>TM BES TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WTP_BU_Check</fullName>
        <description>Check the WTP BU CHeck box true</description>
        <field>TM_Waterproofing__c</field>
        <literalValue>1</literalValue>
        <name>WTP BU Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Alternate MHC ID</fullName>
        <actions>
            <name>MHC_ID_Format_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.ExternalID__c</field>
            <operation>startsWith</operation>
            <value>MHC</value>
        </criteriaItems>
        <description>Create alternate MHC ID</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BES Check Box</fullName>
        <actions>
            <name>TM_BES_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.SpecAlerts__c</field>
            <operation>contains</operation>
            <value>BES</value>
        </criteriaItems>
        <description>If BES is in SPec Alerts, check box.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Sika Entered to Data Entered</fullName>
        <actions>
            <name>Project_Record_Type_Data</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.ExternalID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Project__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sika Entered</value>
        </criteriaItems>
        <description>When External ID is added, change record type to Data Entered to show additional fields.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Concrete Check Box</fullName>
        <actions>
            <name>BU_Concrete_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.SpecAlerts__c</field>
            <operation>contains</operation>
            <value>BU Concrete</value>
        </criteriaItems>
        <description>If BU Concrete is in SPec Alerts, check box.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contractor Check Box</fullName>
        <actions>
            <name>CNT_Check_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.SpecAlerts__c</field>
            <operation>contains</operation>
            <value>BU Contractor</value>
        </criteriaItems>
        <description>If BU Contractor is in SPec Alerts, check box.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flooring Check Box</fullName>
        <actions>
            <name>Flooring_Check_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.SpecAlerts__c</field>
            <operation>contains</operation>
            <value>BU Flooring</value>
        </criteriaItems>
        <description>If BU Flooring  is in SPec Alerts, check box.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New RSB Warranty Project Created %28Community%29</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Project__c.Community_User_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Project Manager Added by Other</fullName>
        <actions>
            <name>EMail_Notification_to_Project_Manager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Project Manager is designated by someone other than that person.</description>
        <formula>NOT(isnull(Sika_Project_Manager__c)) &amp;&amp;
Sika_Project_Manager__r.Id   &lt;&gt;  $User.Id</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing Check Box</fullName>
        <actions>
            <name>Roofing_Project_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.SpecAlerts__c</field>
            <operation>contains</operation>
            <value>BU Roofing</value>
        </criteriaItems>
        <description>If BU Roofing is in SPec Alerts, check box.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Roofing Project</fullName>
        <actions>
            <name>Roofing_Project_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(
CreatedBy.ProfileId= &quot;00e40000000jlWp&quot;,
CreatedBy.ProfileId =&quot;00e40000000jlhs&quot;,
CreatedBy.ProfileId =&quot;00e40000000jmAL&quot;,
LastModifiedBy.ProfileId= &quot;00e40000000jlWp&quot;,
LastModifiedBy.ProfileId =&quot;00e40000000jlhs&quot;,
LastModifiedBy.ProfileId =&quot;00e40000000jmAL&quot;,
OwnerId = &quot;005400000019gXr&quot;,
OwnerId = &quot;005400000019LiV&quot; ,
OwnerId = &quot;005400000019LXl&quot; ,
OwnerId = &quot;005400000019Lic&quot; ,
OwnerId = &quot;005400000019Lae&quot; ,
OwnerId = &quot;005400000019Lgw&quot; ,
OwnerId = &quot;005400000019LgK&quot; ,
OwnerId = &quot;005400000019Xcx&quot; ,
OwnerId = &quot;005400000019LcG&quot; ,
OwnerId = &quot;005400000019LcE&quot; ,
OwnerId = &quot;005400000019LiP&quot; ,
OwnerId = &quot;005400000019Xcy&quot; ,
OwnerId = &quot;005400000019LjN&quot; ,
OwnerId = &quot;005400000019LiB&quot; ,
OwnerId = &quot;005400000019Lcs&quot; ,
OwnerId = &quot;005400000019Lc6&quot; ,
OwnerId = &quot;005400000019Lc8&quot; ,
OwnerId = &quot;005400000019Lgx&quot; ,
OwnerId = &quot;005400000019LcF&quot; ,
OwnerId = &quot;005400000019Xcz&quot; ,
OwnerId = &quot;005400000019LbE&quot; ,
OwnerId = &quot;005400000019LhB&quot; ,
OwnerId = &quot;005400000019Lgy&quot; ,
OwnerId = &quot;005400000019Lbn&quot; ,
OwnerId = &quot;005400000019LcR&quot; ,
OwnerId = &quot;00540000001mHG1&quot; ,
OwnerId = &quot;005400000019LXl&quot; ,
OwnerId = &quot;005400000019dGn&quot; ,
OwnerId = &quot;005400000019gUY&quot; ,
OwnerId = &quot;005400000019gUi&quot; ,
OwnerId = &quot;005400000019gUn&quot; ,
OwnerId = &quot;005400000019gUs&quot; ,
OwnerId = &quot;00540000001mHG6&quot; ,
OwnerId = &quot;00540000001mHGB&quot; ,
OwnerId = &quot;00540000001mHGG&quot; ,
OwnerId = &quot;005400000019Lb3&quot; ,
OwnerId = &quot;005400000019LiO&quot; ,
OwnerId = &quot;005400000019Lct&quot; ,
OwnerId = &quot;00540000001mdTU&quot; ,
OwnerId = &quot;00540000001mx7y&quot; ,
OwnerId = &quot;005400000019Lid&quot; ,
OwnerId = &quot;005400000019dGi&quot; ,
OwnerId = &quot;005400000019LbD&quot; ,
OwnerId = &quot;005400000019LcY&quot; ,
OwnerId = &quot;005400000019dGs&quot; ,
OwnerId = &quot;005400000019Laa&quot; ,
OwnerId = &quot;005400000019LdL&quot; ,
OwnerId = &quot;005400000019LaW&quot; ,
OwnerId = &quot;00540000001mQoO&quot; ,
OwnerId = &quot;005400000019LiE&quot; ,
OwnerId = &quot;005400000019LaX&quot; ,
OwnerId = &quot;005400000019Lac&quot; ,
OwnerId = &quot;005400000019LhJ&quot; ,
OwnerId = &quot;005400000019LaQ&quot; ,
OwnerId = &quot;005400000019LiF&quot; ,
OwnerId = &quot;00540000001mb9D&quot; ,
OwnerId = &quot;005400000019LhH&quot; ,
OwnerId = &quot;005400000019LcB&quot; ,
OwnerId = &quot;005400000019LcC&quot; ,
OwnerId = &quot;005400000019U8L&quot; ,
OwnerId = &quot;005400000019Lbc&quot; ,
OwnerId = &quot;00540000001mx7x&quot; ,
OwnerId = &quot;005400000019Lad&quot; ,
OwnerId = &quot;005400000019Lba&quot; ,
OwnerId = &quot;005400000019LhX&quot; ,
OwnerId = &quot;005400000019Lc9&quot; ,
OwnerId = &quot;005400000019LaZ&quot; ,
OwnerId = &quot;005400000019LbS&quot; ,
OwnerId = &quot;005400000019Laf&quot; ,
OwnerId = &quot;005400000019Lb2&quot; ,
OwnerId = &quot;005400000019Lb4&quot; ,
OwnerId = &quot;00540000001mPwq&quot; ,
OwnerId = &quot;005400000019LbR&quot; ,
OwnerId = &quot;005400000019LbU&quot; ,
OwnerId = &quot;005400000019Lh8&quot; ,
OwnerId = &quot;005400000019Lbt&quot; ,
OwnerId = &quot;005400000019LcA&quot; ,
OwnerId = &quot;005400000019LhI&quot; ,
OwnerId = &quot;005400000019LiN&quot; ,
OwnerId = &quot;005400000019LaY&quot; ,
OwnerId = &quot;005400000019Lab&quot; ,
OwnerId = &quot;005400000019LhT&quot; ,
OwnerId = &quot;005400000019Lc2&quot; ,
OwnerId = &quot;005400000019LcD&quot; ,
OwnerId = &quot;00540000001mx7z&quot; ,
OwnerId = &quot;005400000019Lc4&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Waterproofing Check Box</fullName>
        <actions>
            <name>WTP_BU_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.SpecAlerts__c</field>
            <operation>contains</operation>
            <value>BU Waterproofing</value>
        </criteriaItems>
        <description>If BU Waterproofing is in SPec Alerts, check box.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
