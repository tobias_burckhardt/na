<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SikaCMP__CMP_UpdateExternalId</fullName>
        <field>SikaCMP__ExternalId__c</field>
        <formula>SikaCMP__SupplierNumber__c</formula>
        <name>CMP_UpdateExternalId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SikaCMP__CMP_UpdateExternalID</fullName>
        <actions>
            <name>SikaCMP__CMP_UpdateExternalId</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SikaCMP__CMP_Supplier__c.SikaCMP__SupplierNumber__c</field>
            <operation>startsWith</operation>
            <value>SUP</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
