<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Referral_Alert</fullName>
        <description>Referral Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Alert_Templates/Referral_Alert</template>
    </alerts>
    <alerts>
        <fullName>Referral_Alert_BES</fullName>
        <description>Referral BES Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>fraker.sean@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Alert_Templates/Referral_TM_Alert</template>
    </alerts>
    <alerts>
        <fullName>Referral_Alert_Key_User</fullName>
        <description>Referral Alert Key User</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>TM_Key_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Template/Referral_Alert_Key_User</template>
    </alerts>
    <alerts>
        <fullName>Referral_Alert_TM_Aftermarket</fullName>
        <description>Referral TM Aftermarket</description>
        <protected>false</protected>
        <recipients>
            <recipient>weissberg.brian@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Alert_Templates/Referral_TM_Alert</template>
    </alerts>
    <alerts>
        <fullName>Referral_Closed_Alert</fullName>
        <description>Referral Closed Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>TM_Key_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Alert_Templates/Referral_Closed_Alert</template>
    </alerts>
    <alerts>
        <fullName>Referral_Reminder</fullName>
        <description>Referral Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Alert_Templates/Referral_TM_Alert_2_weeks</template>
    </alerts>
    <alerts>
        <fullName>Referral_Reminder_Key_User</fullName>
        <description>Referral Reminder Key User</description>
        <protected>false</protected>
        <recipients>
            <field>TM_Key_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Alert_Templates/Referral_Reminder_Key_User</template>
    </alerts>
    <alerts>
        <fullName>Referral_TM_Alert</fullName>
        <description>Referral TM Alert</description>
        <protected>false</protected>
        <recipients>
            <field>TM_Key_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Alert_Templates/Referral_TM_Alert</template>
    </alerts>
    <alerts>
        <fullName>Referral_TM_Alert_Concrete</fullName>
        <description>Referral TM Concrete Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>artman.nathaniel@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Alert_Templates/Referral_TM_Alert</template>
    </alerts>
    <alerts>
        <fullName>Referral_TM_Alert_FFI</fullName>
        <description>Referral TM FFI Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>labelle.thomas@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Alert_Templates/Referral_TM_Alert</template>
    </alerts>
    <alerts>
        <fullName>Referral_TM_Alert_Flooring</fullName>
        <description>Referral TM Flooring Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>bellico.william@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>marketing.sarnafil@us.sika.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Alert_Templates/Referral_TM_Alert</template>
    </alerts>
    <alerts>
        <fullName>Referral_TM_Alert_RSBC</fullName>
        <description>Referral TM RSB Commercial Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>munt.daniel@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Alert_Templates/Referral_TM_Alert</template>
    </alerts>
    <alerts>
        <fullName>Referral_TM_Alert_Roofing</fullName>
        <description>Referral TM Roofing Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>bellico.william@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Alert_Templates/Referral_TM_Alert</template>
    </alerts>
    <alerts>
        <fullName>Referral_TM_Alert_Waterproofing</fullName>
        <description>Referral TM Waterproofing Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>fisher.ryan@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Alert_Templates/Referral_TM_Alert</template>
    </alerts>
    <fieldUpdates>
        <fullName>TM_Concrete_User</fullName>
        <field>TM_Key_User__c</field>
        <lookupValue>chilton.jeremy@us.sika.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>TM Concrete User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Referral_Due_Date</fullName>
        <description>Add two weeks to reminder date</description>
        <field>Due_Date__c</field>
        <formula>Today()</formula>
        <name>Update Referral Due Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Referral_Reminder_Date</fullName>
        <description>Reminder Interval (up to 14 days) to the Referral Reminder Date</description>
        <field>Reminder_Date__c</field>
        <formula>Reminder_Date__c +  Reminder_Interval__c</formula>
        <name>Update Referral Reminder Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Referral Reminder and Update</fullName>
        <active>true</active>
        <description>When Reminder Date is reached, an email reminder is sent to the user and the Reminder Date is pushed out by 14 days.</description>
        <formula>Referral_State__c &lt;&gt; &quot;Closed&quot; &amp;&amp; 
NOT(ISNULL(Reminder_Date__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Referral_Reminder</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Referral_Reminder_Key_User</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Referral_Reminder_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Referral__c.Reminder_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Reminder</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Referral__c.Referral_Result__c</field>
            <operation>equals</operation>
            <value>No Action</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Referral_Reminder</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Referral_Reminder_Key_User</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Referral_Due_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Referral__c.Due_Date__c</offsetFromField>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
