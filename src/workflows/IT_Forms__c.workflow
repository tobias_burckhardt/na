<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>IT_Form_Notify_Case_Owner</fullName>
        <description>IT Form - Notify Case Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>IT_New_Hire_Mobile_Templates/IT_Form_Notify_Case_Owner</template>
    </alerts>
</Workflow>
