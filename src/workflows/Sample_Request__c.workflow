<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Scofield_Sample_Request_Approval</fullName>
        <description>Scofield Sample Request Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>goldstein.kathy@us.sika.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sika_Template/Sample_Request_Approval_Approved</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Status_to_Draft</fullName>
        <field>Request_Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Set Status to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_In_Fulfillment</fullName>
        <field>Request_Status__c</field>
        <literalValue>Fulfillment</literalValue>
        <name>Set Status to In Fulfillment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Submitted</fullName>
        <field>Request_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Status to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
