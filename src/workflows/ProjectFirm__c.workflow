<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Owner_or_Created_By_Roofing_User</fullName>
        <field>Owner_or_Created_By_Roofing_User__c</field>
        <formula>IF( $User.ProfileId = &quot;00e40000000jlWp&quot; /* Profile: BU Roofing */
|| $User.ProfileId = &quot;00e40000000jlhs&quot;  /* Profile: BU Roofing Mgr */
|| $User.ProfileId = &quot;00e40000000jmAL&quot; , /* BU Roofing - IS/Marketing */
&quot;yes&quot; ,
&quot;no&quot;
)</formula>
        <name>Set Owner or Created By Roofing User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>All new Project Firms</fullName>
        <actions>
            <name>Set_Owner_or_Created_By_Roofing_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
