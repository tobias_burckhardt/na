trigger EmailMessageRestrictDeleteTrigger on EmailMessage (before delete) {
	//For all the email messages in the trigger set, if they are related to a restricted case,
    //  do not allow deletions
    for(EmailMessage em : [SELECT Id, ParentId, Parent.RecordType.DeveloperName FROM EmailMessage WHERE Id IN :Trigger.oldMap.keySet() AND ParentId <> NULL]) {
        if (RestrictedCaseRecordsUtil.isRestrictedCaseRecordType(em.ParentId, em.Parent.RecordType.DeveloperName)) {
	        EmailMessage oldEmail = Trigger.oldMap.get(em.Id);

            if (Trigger.isDelete) {
                oldEmail.addError(System.Label.Restricted_Object_Delete);
            }
        }
    }
}