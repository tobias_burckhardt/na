/**
 * Change in User records when user is Created or updated.
 */
trigger FullUserTrigger on User (after insert, after update) {
    UserTriggerHandler handler = new UserTriggerHandler(Trigger.isExecuting, Trigger.size);
    System.debug('in user trigger');
    if (Trigger.isAfter) {
        // manages sharing records
        if (Trigger.isInsert) {
            System.debug('in user after trigger');
            handler.onAfterInsert(Trigger.new,Trigger.newMap);
            handler.checkUserActivationOnInsert(Trigger.new);
        } else if (Trigger.isUpdate) {
            handler.onAfterUpdate(Trigger.new, Trigger.oldMap);
            handler.checkUserActivationOnUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}