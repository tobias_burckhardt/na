trigger BuildingGroupProductTrigger on Building_Group_Product__c (after update) {
    
    if(Trigger.isUpdate && Trigger.isAfter) {
        BuildingGroupHistoryServices.saveBuildingGroupHistory(Trigger.new, Trigger.oldMap);
        BuildingGroupProductServices.populateAttachmentGroup(Trigger.new, Trigger.oldMap);
    }
    if(Trigger.isInsert && Trigger.isAfter){
    	BuildingGroupProductServices.populateParentSystem(Trigger.new);
    }
}