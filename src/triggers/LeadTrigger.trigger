trigger LeadTrigger on Lead (after insert, after update) {
    Campaign[] campaign  = null;
    Set<Id> leadIds = new Set<Id>();
    Set<Id> leadIdsYahooBing = new Set<Id>();
    Set<Id> leadIdsSikacoatBing = new Set<Id>();
    Set<Id> leadIdsSikacoatGoogle = new Set<Id>();
    Set<Id> leadIdsFloorMarketing = new Set<Id>();
    for(Lead lead: trigger.new){
        String descr = lead.Description;
        if(descr != null && descr.contains('Recording: http://reports.callcap.com') && lead.LeadSource == 'Roof Marketing Campaigns')
                leadIds.add(lead.id);
        if(lead.LeadSource == 'Yahoo/Bing Roof Campaigns')
                leadIdsYahooBing.add(lead.id);
        if(lead.LeadSource == 'Sikacoat Bing Campaigns')
                leadIdsSikacoatBing.add(lead.id);
        if(lead.LeadSource == 'Sikacoat Google campaigns')
                leadIdsSikacoatGoogle.add(lead.id);
        if(descr != null && descr.contains('callcap.com') && lead.LeadSource == 'Floor Marketing Campaigns')
                leadIdsFloorMarketing.add(lead.id);
    }
    
    List<CampaignMember> members = new List<CampaignMember >();
    if(leadIds.size()>0){
        campaign = [select id from Campaign where name = 'Google Adwords Starts2014 Web Roofing US'];
        if(campaign.size()>0){
            for(Id leadId: leadIds){
            
                CampaignMember member = new CampaignMember();
                member.LeadId = leadId;
                //member.status = 'Responded';              
                member.Campaignid = campaign[0].id;
                members.add( member);             
            }   
        }
    }
    
    if(leadIdsYahooBing.size()>0){
        campaign = [select id from Campaign where name = 'Bing/ Yahoo Starts 2014 Roofing Web US'];
        if(campaign.size()>0){
            for(Id leadId: leadIdsYahooBing){
            
                CampaignMember member = new CampaignMember();
                member.LeadId = leadId;
                //member.status = 'Responded';
                member.Campaignid = campaign[0].id;
                members.add( member);             
            }   
        }
    }
    
    if(leadIdsSikacoatBing.size()>0){
        campaign = [select id from Campaign where name = 'Sikacoat BING Starts 2014 Roofing PPC US'];
        if(campaign.size()>0){
            for(Id leadId: leadIdsSikacoatBing){
            
                CampaignMember member = new CampaignMember();
                member.LeadId = leadId;
                //member.status = 'Responded';
                member.Campaignid = campaign[0].id;
                members.add( member);             
            }   
        }
    }
    
    if(leadIdsSikacoatGoogle.size()>0){
        campaign = [select id from Campaign where name = 'Sikacoat Starts 2014 Roofing Adwords US'];
        if(campaign.size()>0){
            for(Id leadId: leadIdsSikacoatGoogle){
            
                CampaignMember member = new CampaignMember();
                member.LeadId = leadId;
                //member.status = 'Responded';
                member.Campaignid = campaign[0].id;
                members.add( member);             
            }   
        }
    }
    
    if(leadIdsFloorMarketing.size()>0){
        campaign = [select id from Campaign where name = 'F- Google Adwords'];
        if(campaign.size()>0){
            for(Id leadId: leadIdsFloorMarketing){
            
                CampaignMember member = new CampaignMember();
                member.LeadId = leadId;
                //member.status = 'Responded';
                member.Campaignid = campaign[0].id;
                members.add( member);             
            }   
        }
    }
    if(members.size()>0)
        database.insert(members,false);
        ///insert members;
}