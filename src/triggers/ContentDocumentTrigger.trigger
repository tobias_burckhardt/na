trigger ContentDocumentTrigger on ContentDocument (after insert, before delete, after delete)
{
	
	if(Trigger.isInsert)
	{
		if(Trigger.isAfter)
		{
			ContentDocumentTriggerHandler.createContentHeaders(Pluck.ids(Trigger.new));
		}
	}
	

	if(Trigger.isDelete)
	{
		if(Trigger.isBefore)
		{
			ContentDocumentTriggerHandler.contentDocumentRestrictDelete(Trigger.old);
		}

		if(Trigger.isAfter)
		{
			ContentDocumentTriggerHandler.deleteContentHeaders(Trigger.old);
		}
	}
}