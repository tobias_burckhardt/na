trigger sika_SCCZoneTestResultTrigger on Test_Result__c (after insert, before update, before delete) {

	// prevents salespeople from editing or deleting records associated with accounts on which they are not accountTeamMembers (or owners)
	// Will only fire once per execution context.
	if(sika_SCCZoneTestResultTriggerHelper.bypassAccountTeamCheck == false){
		if(Trigger.isUpdate || Trigger.isDelete){
			list<Test_Result__c> theResults = new list<Test_Result__c>();
			if(Trigger.isUpdate){
				sika_SCCZoneTestResultTriggerHelper.accountTeamCheck(Trigger.new, 'edit');
			}
			if(Trigger.isDelete){
				sika_SCCZoneTestResultTriggerHelper.accountTeamCheck(Trigger.old, 'delete');
			}
		}
	}

	// locks or unlocks Mix record when the first test result is added, or the last test result is deleted.
	// will need to bypass the preventEdit trigger on Mix so that we can change the isLocked value.
	if(sika_SCCZoneTestResultTriggerHelper.bypassLockOrUnlockMixAndMaterials == false){
		if(trigger.isAfter && trigger.isInsert){ 
			system.debug('********TRG: firing the lockMixAndMaterials trigger on Test Result (after insert)');
	    	sika_SCCZoneTestResultTriggerHelper.lockMixAndMaterials(Trigger.newMap.values());   
	    }

	    if(trigger.isBefore && trigger.isDelete){
	    	system.debug('********TRG: firing the unlockMix trigger on Test Result (before delete)');
	    	sika_SCCZoneTestResultTriggerHelper.unlockMix(Trigger.oldMap.values()); 
	    }
	}
}