trigger EventRestrictUpdateDeleteTrigger on Event (before update, before delete) {
    Schema.FieldSet taskFieldSet = SObjectType.Event.FieldSets.Restricted_Fields;
    List<Event> caseEvents = new List<Event>();
    Set<Id> caseIds = new Set<Id>();

    //Get a list of events that are related to a restricted case
    for (Event e : [SELECT Id, WhatId, What.RecordType.DeveloperName FROM Event WHERE Id IN :Trigger.oldMap.keySet() AND WhatId <> NULL]) {
        if (RestrictedCaseRecordsUtil.isRestrictedCaseRecordType(e.WhatId, e.What.RecordType.DeveloperName)) {
            caseIds.add(e.WhatId);
            caseEvents.add(e);
        }
    }

    //Load details about the cases to determine if the event can be modified
    Map<Id, Case> cases = new Map<Id, Case>();
    for (Case c : [SELECT Id, Status FROM Case WHERE Id IN :caseIds]) {
        cases.put(c.Id, c);
    }
    
    //For all of the filtered events, do not allow deletions or updates to the fields specified
    //in the Restricted_Fields field set
    for(Event ev : caseEvents) {
        Event oldEvent = Trigger.oldMap.get(ev.Id);

        if (Trigger.isDelete) {
            oldEvent.addError(System.Label.Restricted_Object_Delete);
        } else if (Trigger.isUpdate && cases.get(ev.WhatId).Status == 'Closed') {
	        Event newEvent = Trigger.newMap.get(ev.Id);
            boolean validUpdate = RestrictedCaseRecordsUtil.checkUpdatedFields(oldEvent, newEvent, taskFieldSet);

            if (!validUpdate) {
                newEvent.addError(System.Label.Restricted_Object_Update);
            }
        } else if (Trigger.isUpdate) {
	        Event newEvent = Trigger.newMap.get(ev.Id);
            if (oldEvent.WhatId != newEvent.WhatId) {
                newEvent.addError(System.Label.Restricted_Object_Update);
            }
        }
    }
}