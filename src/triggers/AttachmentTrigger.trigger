trigger AttachmentTrigger on Attachment (after insert) {

    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
        	System.enqueueJob(new AttachmentQueueable(Trigger.new));
        }
    }
}