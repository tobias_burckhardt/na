trigger PricingAttachmentTrigger on Attachment (after insert) {
	Map<Id,String> attachmentMap = new Map<Id,String>();
    Map<Id,String> quoteRecordTypeIdMap = new Map<Id,String>();
    Map<Id,String> quoteRecordTypeNameMap = new Map<Id,String>();
    Map<Id,String> quoteWithAccountNameMap = new Map<Id,String>();
	for(Attachment attachmentObj:trigger.new){
        if(attachmentObj.ParentId.getSobjectType() == Quote.SobjectType)
        	attachmentMap.put(attachmentObj.ParentId,attachmentObj.Name);
	}
    try{
        if(attachmentMap!=null && attachmentMap.size()>0){
            List<Quote> quoteObjs = [Select Id,Name,RecordTypeId,OwnerId,Account__r.Name from Quote where Id IN:attachmentMap.keySet()];
            List<String> ownerIds = new List<String>();
            Map<String,String> quoteOwnerMap = new Map<String,String>();
            for(Quote quoteObj:quoteObjs){
                ownerIds.add(quoteObj.OwnerId);
                quoteOwnerMap.put(quoteObj.Id+'-'+quoteObj.OwnerId,quoteObj.Id);
                quoteRecordTypeIdMap.put(quoteObj.Id,quoteObj.RecordTypeId);
                quoteWithAccountNameMap.put(quoteObj.Id,quoteObj.Name+'-'+quoteObj.Account__r.Name);
            }
            List<RecordType> recordTypes = [Select Id,Name,DeveloperName from RecordType where Id IN :quoteRecordTypeIdMap.values()];
            Map<Id,String> recordTypeMap = new Map<Id,String>();
            for(RecordType recordTypeObj:recordTypes){
                recordTypeMap.put(recordTypeObj.Id,recordTypeObj.DeveloperName);
            }
            for(String quoteId:quoteRecordTypeIdMap.keySet()){
                String quoteRecordTypeId = quoteRecordTypeIdMap.get(quoteId);
                if(recordTypeMap.containsKey(quoteRecordTypeId)){
                    quoteRecordTypeNameMap.put(quoteId,recordTypeMap.get(quoteRecordTypeId));
                }
            }
            List<User> users = [Select Id,Name,Email from User where Id IN:ownerIds];
           	List<Attachment> attachmentsList = [SELECT id, Name, body, ContentType FROM Attachment WHERE ParentId IN :attachmentMap.keySet() order by CreatedDate desc limit 1];
            String spaTemplateId = [SELECT Id, Name FROM EmailTemplate WHERE DeveloperName = 'SPA_Pricing_Request_Approved'].Id;
            String spqTemplateId = [SELECT Id, Name FROM EmailTemplate WHERE DeveloperName = 'SPQ_Pricing_Request_Approved'].Id;
            for(String quoteId:quoteRecordTypeNameMap.keySet()){
                String quoteRecordTypeName = quoteRecordTypeNameMap.get(quoteId);
                if(quoteRecordTypeName.equalsIgnoreCase('SPA_Pricing') || quoteRecordTypeName.equalsIgnoreCase('SPQ_Pricing')){
                    Map<Id,String> ownerEmailMap = new Map<Id,String>();
                    Map<Id,String> ownerNameMap = new Map<Id,String>();
                    for(User userObj:users){
                       ownerEmailMap.put(userObj.Id,userObj.Email);
                       ownerNameMap.put(userObj.Id,userObj.Name);
                    }
                    List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
                    for(String ownerEmailMapKey:ownerEmailMap.keySet()){
                        String emailId = ownerEmailMap.get(ownerEmailMapKey);
                        String ownerName = ownerNameMap.get(ownerEmailMapKey);
                        Messaging.SingleEmailMessage mail;
                        if(quoteRecordTypeName.equalsIgnoreCase('SPA_Pricing'))
                           mail = Messaging.renderStoredEmailTemplate(spaTemplateId, ownerEmailMapKey, quoteId);
                        else
                           mail = Messaging.renderStoredEmailTemplate(spqTemplateId, ownerEmailMapKey, quoteId);
                        
                        String[] toAddresses = new String[] {emailId};
                            mail.setToAddresses(toAddresses);
                        String quoteDetail = quoteWithAccountNameMap.get(quoteId);
                        List<String> quoteAttributes = quoteDetail.split('-');
                        OrgWideEmailAddress orgWideEmailAddressObj = [Select Id from OrgWideEmailAddress where Address like 'sfdc_no_reply@ca.sika.com'];
                        mail.setOrgWideEmailAddressId(orgWideEmailAddressObj.Id);
                        mail.setUseSignature(false);
                        mail.setSaveAsActivity(false);
                        Messaging.EmailFileAttachment[] efaList = new List<Messaging.EmailFileAttachment>();
                        for(Attachment att : attachmentsList){ 
                            System.debug('att is :'+att);
                            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                            efa.setFileName(att.Name);
                            efa.setBody(att.body);
                            efa.setContentType(att.ContentType);
                            efa.setInline(false);
                            efaList.add(efa); 
                        }
                        System.debug('efaList--'+efaList);
                        mail.setFileAttachments(efaList);
                        System.debug('mail--'+mail);
                        mails.add(mail);
                    }
                    System.debug('mails--'+mails);
                    List<Messaging.SendEmailResult> results =Messaging.sendEmail(mails);
                    System.debug('results--'+results);
                }
            } 
        }
    }catch(Exception e){
        System.debug(e);
    }
}