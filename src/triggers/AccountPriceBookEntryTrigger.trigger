trigger AccountPriceBookEntryTrigger on Account_Pricebook_Entry__c (before insert,before update) {
    List<String> pricebookIds = new List<String>();
    List<String> accountPBEntryIds = new List<String>();
    Map<String,String> congaTemplatesMap = new Map<string,string>();
    Map<String,String> priceBookMap = new Map<string,string>();
    for(Account_Pricebook_Entry__c ape : trigger.new){
        if(ape.Price_Book__c != null)
          pricebookIds.add(ape.Price_Book__c);
        accountPBEntryIds.add(ape.Id);
    }
    list<Pricebook2> priceBooks = [SELECT Id, Name FROM Pricebook2 where id IN: pricebookIds];
    for(Pricebook2 pri : priceBooks){
        priceBookMap.put(pri.id,pri.name);
    }
    list<APXTConga4__Conga_Template__c> congaTemplates = [select id,APXTConga4__Name__c from APXTConga4__Conga_Template__c where APXTConga4__Name__c IN:priceBookMap.values()];
    list<APXTConga4__Conga_Template__c> genericTemplates = [select id,APXTConga4__Name__c from APXTConga4__Conga_Template__c where APXTConga4__Name__c like '%Generic Price book - Sika Pricing%'];
    for(APXTConga4__Conga_Template__c congaTemplate : congaTemplates){
        congaTemplatesMap.put(congaTemplate.APXTConga4__Name__c,congaTemplate.id);
    }
    if(genericTemplates.size()>0){
        for(APXTConga4__Conga_Template__c congaTemplate : genericTemplates){
            congaTemplatesMap.put(congaTemplate.APXTConga4__Name__c,congaTemplate.id);
        }
    }
    for(Account_Pricebook_Entry__c ape : trigger.new){
          if(congaTemplatesMap!=null && congaTemplatesMap.get(priceBookMap.get(ape.Price_Book__c))!=null)
              ape.Conga_Template_Id__c = congaTemplatesMap.get(priceBookMap.get(ape.Price_Book__c));
          else
              ape.Conga_Template_Id__c = congaTemplatesMap.get('Generic Price book - Sika Pricing');
      }
    /*if(Trigger.isUpdate && Trigger.isBefore){
        Map<Account_Pricebook_Entry__c,Boolean> accountPriceBookEntryStatusMap = new Map<Account_Pricebook_Entry__c,Boolean>();
        for( Id apeId : Trigger.newMap.keySet() ){
          if( Trigger.oldMap.get( apeId ).Price_Book__c != Trigger.newMap.get( apeId ).Price_Book__c ){
              accountPriceBookEntryStatusMap.put(Trigger.newMap.get( apeId ),true);
          }
        }
        if(accountPriceBookEntryStatusMap!=null){
            List<AccountPriceBookLineItem__c> accountPriceBookLineItems = [Select Id from AccountPriceBookLineItem__c where Account_Price_Book_Entry__c IN :accountPriceBookEntryStatusMap.keySet()];
            delete accountPriceBookLineItems;
        }
    }*/
    
}