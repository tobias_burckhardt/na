trigger CaseCommentRestrictUpdateDeleteTrigger on CaseComment (before update, before delete) {
    //For all the case comments, if the comment is related to a restricted case,
    //  do not allow deletions or updates to the fields listed in the CaseComment custom setting
    for(CaseComment comm : [SELECT Id, ParentId, Parent.RecordType.DeveloperName FROM CaseComment WHERE Id IN :Trigger.oldMap.keySet()]) {
        if (RestrictedCaseRecordsUtil.isRestrictedCaseRecordType(comm.ParentId, comm.Parent.RecordType.DeveloperName)) {
	        CaseComment oldComm = Trigger.oldMap.get(comm.Id);

            if (Trigger.isDelete) {
                oldComm.addError(System.Label.Restricted_Object_Delete);
            } else if (Trigger.isUpdate) {
                CaseComment newComm = Trigger.newMap.get(comm.Id);
                boolean validUpdate = RestrictedCaseRecordsUtil.checkUpdatedFields(oldComm, newComm, 'CaseComment');

                if (!validUpdate) {
                    newComm.addError(System.Label.Restricted_Object_Update);
                }
            }
        }
    }
}