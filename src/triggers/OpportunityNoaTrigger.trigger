/*
* Author: Martin Kona
* Company: Bluewolf
* Date: May 26, 2016
* Description: NOA Accepted Opportunity trigger
* 
*/

trigger OpportunityNoaTrigger on Opportunity (before insert, after update) {
    
    if (Trigger.isBefore) {
        if (Trigger.isInsert) {        
			OpportunityNOAService.acceptProposalsInsert(Trigger.New);            
        }
    } else if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
			OpportunityNOAService.acceptProposalsUpdate(Trigger.oldMap, Trigger.NewMap);            
        }         
    }

}