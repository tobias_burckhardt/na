trigger sika_SCCZoneAdmixturePriceTrigger on Admixture_Price__c (after insert, after update) {
	// update the Mix_Material__c.Admixture_Price__c field when an Admixture_Price record is created or updated...
	// ...but only for Mix_Materials that aren't associated with locked Mix records.
	if(Trigger.isInsert || Trigger.isUpdate){
		system.debug('********TRG: firing the updateMixMaterialAdmixturePrice trigger on Admixture_Price insert or update.');
		sika_SCCZoneAdmixturePriceTriggerHelper.updateMixMaterialAdmixturePrice(Trigger.new);
	}
}