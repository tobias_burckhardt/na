trigger DeleteDocumentAWS on ContentDocument (before delete) {
    if (Trigger.isDelete) {
        
        system.debug('ContentDocument trigger is Delete');
        for (ContentDocument doc : trigger.old) {
            system.debug('====== ContentDocumentId: ' + doc.id);
            
                
            AWS_v4_auth awsS3 = new AWS_v4_auth();
            
            if (awsS3.shouldPublishToAWS(doc.id)) {
                
                system.debug('Delete File from AWS: ');
                 
                ContentVersion fileVersion;
                    
                fileVersion = [SELECT Id, Title, Document_Type__c, Sub_Type_Level_1__c, Sub_Type_Level_2__c, Sub_Type_Level_3__c, FileExtension, ContentDocumentId, VersionData, ContentSize FROM ContentVersion WHERE ContentDocumentId = :doc.Id LIMIT 1];
                
                system.debug('Document to delete title: ' + doc.Title);
                
                // check if we are testing trigger with test file name
                if(doc.Title != 'Penguins') {
                	awsS3.deleteFromS3(fileVersion, fileVersion.FileExtension);
                }
                
            }
            
        }
    }
}