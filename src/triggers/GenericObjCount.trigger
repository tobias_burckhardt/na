trigger GenericObjCount on Project__c(after insert, after update, before delete ) {
    Static final String DEF_NAMESPACE = 'ConstructionPts__';
    String strConfigName = 'ViewRelatedObjects_CP_Project__c';
    String strConfigName2 = 'RelatedObjectCounts';
    //private CPFLS m_clsCPFLS = new CPFLS();
    String strConfigLongValue = '';
    //CPConfig__c newObj = new CPConfig__c();
    String[]fieldsToCheck = new String[]{
        'Config_Name__c',
        'Config_Value__c',
        'Config_Long_Value__c'
    };
    //Boolean isAccessible = m_clsCPFLS.checkObjectFields(newObj, 'isAccessible', fieldsToCheck);
    String objectFieldName;
    String hostObjectFieldName;
    Integer updateCount;

    //if (isAccessible) {
    objectFieldName = 'CP_Project__c';
    hostObjectFieldName = 'related_Projects__c';

    System.debug(logginglevel.ERROR, 'config available');

    System.debug(logginglevel.ERROR, 'GenericObjCount Called');
    
    List<ConstructionPts__CPConfig__c> lstCPConfig = [SELECT id,ConstructionPts__Config_Value__c from ConstructionPts__CPConfig__c where ConstructionPts__Config_Name__c = 'ActivateGenericObjCountTrigger'];
    if(lstCPConfig != null && !lstCPConfig.isEmpty() ){
        Integer genericCountConfig  ;
        if( lstCPConfig[0].ConstructionPts__Config_Value__c != null){
            genericCountConfig  = Integer.valueOf(lstCPConfig[0].ConstructionPts__Config_Value__c);
        }else{
            genericCountConfig  = 0;
        }
     
        
        if(genericCOuntConfig == 1){
             updateCount = 1;

        if (objectFieldName != null && hostObjectFieldName != null) {
    
            if (Trigger.isInsert) {
                List < sObject > lstToAdd = new List < sObject > ();
                for (sObject TGObj: trigger.new) {
                    if (TGObj.get('CP_Project__c') != null)
                        lstToAdd.add(TGObj);
                }
                if (lstToAdd.Size() > 0) {
    
                    if (updateCount == 1) {
                        System.debug(logginglevel.ERROR, 'objFieldName = ' + objectFieldName);
                        System.debug(logginglevel.ERROR, 'hostObjFieldName = ' + hostObjectFieldName);
                        String updateCountJSON = '{ "lstToCheck":[';
                        String objsToAdd;
                        for (SObject sobj: lstToAdd) {
                            objsToAdd = '{"CP_Project__c": "' + sobj.get('CP_Project__c') + '"},';
                        }
                        System.debug(logginglevel.ERROR, 'ObjsToAdd = ' + ObjsToAdd);
                        objsToAdd = objsToAdd.substring(0, objsToAdd.length() - 1);
    
                        UpdateCountJSON += objsToAdd;
                        UpdateCountJSON += '], "currObjType":"CP_Project__c","fieldToCheck":"' + objectFieldName + '","fieldToUpdate": "' + hostObjectFieldName + '","updateVal":1}';
                        System.debug(logginglevel.ERROR, 'UpdateCountJSON = ' + UpdateCountJSON);
    
                        ConstructionPts.CPTerritoryGeography.getCountyName(UpdateCountJSON);
                        //ConstructionPts.PipeLine.UpdateCounts(lstToAdd, 'CP_Project__c', objectFieldName, hostObjectFieldName, 1);
    
                    }
                }
            }
    
            if (Trigger.isUpdate) {
                List < sObject > lstToAdd = new List < sObject > ();
                List < sObject > lstToSubtract = new List < sObject > ();
    
                for (sObject TGObj: Trigger.new) {
                    if (Trigger.oldMap.get(TGObj.Id).get('CP_Project__c') != Trigger.newMap.get(TGObj.Id).get('CP_Project__c')) {
                        if (Trigger.newMap.get(TGObj.Id).get('CP_Project__c') != null) {
                            lstToAdd.add(Trigger.newMap.get(TGObj.Id));
                        }
                        if (Trigger.oldMap.get(TGObj.Id).get('CP_Project__c') != null) {
                            lstToSubtract.add(Trigger.oldMap.get(TGObj.Id));
                        }
                    }
                }
    
                if (!lstToAdd.isEmpty()) {
    
                    if (updateCount == 1) {
                        // ConstructionPts.PipeLine.UpdateCounts(lstToAdd, 'CP_Project__c', objectFieldName, hostObjectFieldName, 1);
                        System.debug(logginglevel.ERROR, 'objFieldName = ' + objectFieldName);
                        System.debug(logginglevel.ERROR, 'hostObjFieldName = ' + hostObjectFieldName);
                        String updateCountJSON = '{ "lstToCheck":[';
                        String objsToAdd;
                        for (SObject sobj: lstToAdd) {
                            objsToAdd = '{"CP_Project__c": "' + sobj.get('CP_Project__c') + '"},';
                        }
                        System.debug(logginglevel.ERROR, 'ObjsToAdd = ' + ObjsToAdd);
                        objsToAdd = objsToAdd.substring(0, objsToAdd.length() - 1);
    
                        UpdateCountJSON += objsToAdd;
                        UpdateCountJSON += '], "currObjType":"CP_Project__c","fieldToCheck":"' + objectFieldName + '","fieldToUpdate": "' + hostObjectFieldName + '","updateVal":1}';
                        System.debug(logginglevel.ERROR,'UpdateCountJSON = ' + UpdateCountJSON);
                        ConstructionPts.CPTerritoryGeography.getCountyName(UpdateCountJSON);
                       
                    }
                }
    
                if (!lstToSubtract.isEmpty()) {
    
                    if (updateCount == 1) {
                        System.debug(logginglevel.ERROR, 'objFieldName = ' + objectFieldName);
                        System.debug(logginglevel.ERROR, 'hostObjFieldName = ' + hostObjectFieldName);
                        String updateCountJSON = '{ "lstToCheck":[';
                        String objsToSub;
                        for (SObject sobj: lstToSubtract) {
                            objsToSub = '{"CP_Project__c": "' + sobj.get('CP_Project__c') + '"},';
                        }
    
                        objsToSub = objsToSub.substring(0, objsToSub.length() - 1);
                        System.debug(logginglevel.ERROR, 'ObjsToAdd = ' + objsToSub);
    
                        UpdateCountJSON += objsToSub;
                        UpdateCountJSON += '], "currObjType":"CP_Project__c","fieldToCheck":"' + objectFieldName + '","fieldToUpdate": "' + hostObjectFieldName + '","updateVal":-1}';
                        System.debug(logginglevel.ERROR,'UpdateCountJSON = ' + UpdateCountJSON);
                        ConstructionPts.CPTerritoryGeography.getCountyName(UpdateCountJSON);
                        // ConstructionPts.PipeLine.UpdateCounts(lstToSubtract, 'CP_Project__c', objectFieldName, hostObjectFieldName, -1);
                    }
                }
            }
    
            if (Trigger.isDelete) {
                List < sObject > lstToSubtract = new List < sObject > ();
    
                for (sObject TGObj: trigger.old) {
                    if (TGObj.get('CP_Project__c') != null) {
                        lstToSubtract.add(TGObj);
                    }
                }
                if (!lstToSubtract.isEmpty()) {
    
                    if (updateCount == 1) {
                        System.debug(logginglevel.ERROR, 'objFieldName = ' + objectFieldName);
                        System.debug(logginglevel.ERROR, 'hostObjFieldName = ' + hostObjectFieldName);
                        String updateCountJSON = '{ "lstToCheck":[';
                        String objsToSub;
                        for (SObject sobj: lstToSubtract) {
                            objsToSub = '{"CP_Project__c": "' + sobj.get('CP_Project__c') + '"},';
                        }
    
                        objsToSub = objsToSub.substring(0, objsToSub.length() - 1);
                        System.debug(logginglevel.ERROR, 'ObjsToAdd = ' + objsToSub);
    
                        UpdateCountJSON += objsToSub;
                        UpdateCountJSON += '], "currObjType":"CP_Project__c","fieldToCheck":"' + objectFieldName + '","fieldToUpdate": "' + hostObjectFieldName + '","updateVal":-1}';
                        System.debug(logginglevel.ERROR,'UpdateCountJSON = ' + UpdateCountJSON);
                        ConstructionPts.CPTerritoryGeography.getCountyName(UpdateCountJSON);
                        // ConstructionPts.PipeLine.UpdateCounts(lstToSubtract, 'CP_Project__c', objectFieldName, hostObjectFieldName, -1);
                    }
                }
            }
            
            }
    }
    
   

    }

    //}
}