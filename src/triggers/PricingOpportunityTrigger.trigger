trigger PricingOpportunityTrigger on Opportunity (before insert,before update) {
    
    Set<String> accountIds = new Set<String>();
    List<String> priceBookIds = new List<String>();
    Map<String,String> oppBunitMap = new Map<String,String>();
    List<String> buIds = new List<String>();
    Set<String> recordTypeIds = new Set<String>();
    List<Account_Pricebook_Entry__c> accPBEntries = new List<Account_Pricebook_Entry__c>();
    Map<String,String> accountPBMap = new Map<String,String>();
    Map<String,String> oppBUTMMap = new Map<String,String>();
    Map<String,Map<String,String>> accPBTMMap = new Map<String,Map<String,String>>();
    Map<String,String> pricebookDetailMap = new Map<String,String>();
    Map<String,String> accountRecordTypesMap = new Map<String,String>();
    Map<String,Integer> opportunitySPAStatusMap = new Map<String,Integer>();
    Map<String,Integer> opportunitySPQStatusMap = new Map<String,Integer>();
    List<String> listOfRecordTypes = new List<String>{'SPA_Pricing','SPQ_Pricing'};
    try{
        for(Opportunity opp:Trigger.New){
            recordTypeIds.add(opp.RecordTypeId);
            accountIds.add(opp.AccountId);
            oppBunitMap.put(opp.Name,opp.Business_Unit_Region__c);
        }
        //System.debug('recordTypeIds--'+recordTypeIds);System.debug('accountIds--'+accountIds);System.debug('oppBunitMap--'+oppBunitMap);
        // Checking if the record type is SPA or SPQ.    
        List<RecordType> recordTypes = [Select Id,Name,DeveloperName from RecordType where Id IN:recordTypeIds];
        //System.debug('recordTypes--'+recordTypes);
        Boolean isSpaOrSpqRecord = false;
        for(RecordType recordTypeOb:recordTypes){
            if('SPA_Pricing'.equalsIgnoreCase(recordTypeOb.DeveloperName) || 'SPQ_Pricing'.equalsIgnoreCase(recordTypeOb.DeveloperName)){
                isSpaOrSpqRecord = true;
                break;
            }
        }
        //System.debug('isSpaOrSpqRecord--'+isSpaOrSpqRecord);
        if(isSpaOrSpqRecord){
           //System.debug('oppBunitMap--'+oppBunitMap);
            if(oppBunitMap!=null && oppBunitMap.size()>0){
                List<Region__c> regions = [Select Id,Name,Target_Market_lookup__r.Name from Region__c where Id IN:oppBunitMap.values()];
                //System.debug('regions--'+regions);
                //create a map of opportunity id and business unit's target market
                for(String oppName:oppBunitMap.keySet()){
                    String regionId = oppBunitMap.get(oppName);
                    //System.debug('regionId--'+regionId);
                    //System.debug('regions--'+regions);
                    for(Region__c region:regions){
                        if(regionId==region.Id){
                            oppBUTMMap.put(oppName,region.Target_Market_lookup__r.Name);
                        } 
                    }
                    //System.debug('oppBUTMMap--'+oppBUTMMap);
                }
            }
            // create a map of account and nested map of price book and target market
            //System.debug('accountIds--'+accountIds);
            if(accountIds!=null && accountIds.size()>0){
                /*START - This block of code is used for validating the opportunity's account's record type, which must be sold to Account*/
                List<Account> accounts = [Select Id,RecordType.DeveloperName from Account where Id IN:accountIds];
                //System.debug('accounts--'+accounts);
                for(Account accountObj:accounts){
                    accountRecordTypesMap.put(accountObj.Id,accountObj.RecordType.DeveloperName);
                }
                //System.debug('accountRecordTypesMap--'+accountRecordTypesMap);
                /*END*/
                /*START - This block of code is used for validating the opportunity's account's record type, which must be sold to Account*/
                List<Opportunity> opportunitiesSPA =[SELECT Id,AccountId FROM Opportunity WHERE CreatedDate = THIS_YEAR and AccountId IN:accountIds and RecordType.DeveloperName ='SPA_Pricing'];
                //System.debug('opportunitiesSPA--'+opportunitiesSPA);
                for(Opportunity opp:opportunitiesSPA){
                    //System.debug('opportunitySPAStatusMap--'+opportunitySPAStatusMap);
                    if(opportunitySPAStatusMap!=null && opportunitySPAStatusMap.containsKey(opp.AccountId)){
                        opportunitySPAStatusMap.put(opp.AccountId,opportunitySPAStatusMap.get(opp.AccountId)+1);
                    }else{
                        opportunitySPAStatusMap.put(opp.AccountId,1);
                    }
                }
                List<Opportunity> opportunitiesSPQ =[SELECT Id,AccountId FROM Opportunity WHERE CreatedDate = THIS_YEAR and AccountId IN:accountIds and RecordType.DeveloperName ='SPQ_Pricing'];
                //System.debug('opportunitiesSPQ--'+opportunitiesSPQ);
                for(Opportunity opp:opportunitiesSPQ){
                    //System.debug('opportunitySPQStatusMap--'+opportunitySPQStatusMap);
                    if(opportunitySPQStatusMap!=null && opportunitySPQStatusMap.containsKey(opp.AccountId)){
                        opportunitySPQStatusMap.put(opp.AccountId,opportunitySPQStatusMap.get(opp.AccountId)+1);
                    }else{
                        opportunitySPQStatusMap.put(opp.AccountId,1);
                    }
                }
                /*END*/
                accPBEntries = [Select Id,Price_Book__c,customer_account__c,Target_Market__c from Account_Pricebook_Entry__c where customer_account__c IN:accountIds];
                System.debug('accPBEntries--'+accPBEntries);
                for(Account_Pricebook_Entry__c accPBEntry:accPBEntries){
                    accountPBMap.put(accPBEntry.customer_account__c,accPBEntry.Price_Book__c);
                    //System.debug('accountPBMap--'+accountPBMap);
                    Map<String,String> priceBookTargetmarketMap = new Map<String,String>();
                    priceBookTargetmarketMap.put(accPBEntry.Target_Market__c,accPBEntry.Price_Book__c);
                    //System.debug('priceBookTargetmarketMap--'+priceBookTargetmarketMap);
                    accPBTMMap.put(accPBEntry.customer_account__c,priceBookTargetmarketMap);
                    //System.debug('accPBTMMap--'+accPBTMMap);
                    priceBookIds.add(accPBEntry.Price_Book__c);
                    //System.debug('priceBookIds--'+priceBookIds);
                }
            }
            //System.debug('priceBookIds-1-'+priceBookIds);
            if(priceBookIds!=null && !priceBookIds.isEmpty()){
                List<Pricebook2> pricebooks = [Select Id,Name from Pricebook2 where Id IN:priceBookIds];
                //System.debug('pricebooks--'+pricebooks);
                for(Pricebook2 pricebook:pricebooks){
                    pricebookDetailMap.put(pricebook.Id,pricebook.name);
                }
                //System.debug('pricebookDetailMap--'+pricebookDetailMap);
            }
            for(Opportunity opp:Trigger.New){
                // ASSOCIATING PRICE BOOK TO OPPORTUNITY BASED ON THE ACCOUNT-PRICE BOOK ASSOCIATION
                if(accountPBMap.containsKey(opp.AccountId)){
                    for(String accountPBKey:accountPBMap.keySet()){
                        if(opp.AccountId==accountPBKey){
                            opp.Pricebook2Id = accountPBMap.get(accountPBKey);
                            String pricebookName = pricebookDetailMap.get(opp.Pricebook2Id);
                            opp.Related_Price_Book__c = pricebookName;
                        }
                    }
                }
                // VALIDATING THE RECORD TYPE OF THE SELECTED ACCOUNT
                if(accountRecordTypesMap.containsKey(opp.AccountId)){
                    String recordTypeVal = accountRecordTypesMap.get(opp.AccountId);
                    if(!recordTypeVal.equalsIgnoreCase('SoldTo_Account'))
                        opp.addError(Label.Allow_SoldTo_Accounts);
                    
                }
                if(opportunitySPAStatusMap!=null && opportunitySPAStatusMap.containsKey(opp.AccountId)){
                    Integer oppCount = opportunitySPAStatusMap.get(opp.AccountId);
                    if(oppCount>1){
                        opp.addError(Label.allow_SPA_opportunity_on_same_account);
                    }
                }
                if(opportunitySPQStatusMap!=null && opportunitySPQStatusMap.containsKey(opp.AccountId)){
                    Integer oppCount = opportunitySPQStatusMap.get(opp.AccountId);
                    if(oppCount>1){
                        opp.addError(Label.allow_SPQ_opportunity_on_same_account);
                    }
                }
                // VALIDATING THE PRICE BOOK'S TARGET MARKET WITH THE SELECTED BUSINESS UNIT REGION'S TARGET MARKET
                //System.debug('accPBTMMap--'+accPBTMMap);
                Map<String,String> pbTMMap = accPBTMMap.get(opp.AccountId);
                //System.debug('pbTMMap--'+pbTMMap);
                String selectedTargetMarket = oppBUTMMap.get(opp.name);
                //System.debug('selectedTargetMarket--'+selectedTargetMarket);
                if(pbTMMap!=null && !pbTMMap.containsKey(selectedTargetMarket) && opp.Business_Unit_Region__c !=null)
                    opp.addError(Label.selected_business_unit_not_aligned);
                
            }
        }
    }catch(Exception e){
        System.debug('Exception caught while running opportunity trigger for pricing. - '+e.getStackTraceString());
        System.debug('Cause of error is - '+e.getCause());
        System.debug('Line number is - '+e.getLineNumber());
        System.debug('Error message is - '+e.getMessage());
    }
}