trigger ContentVersionRestrictUpdateTrigger on ContentVersion (before update) {
    
    for ( ContentVersion cv : Trigger.old ) {
        
        List<ContentDocumentLink> links = [SELECT LinkedEntityId FROM ContentDocumentLink WHERE ContentDocumentId = :cv.ContentDocumentId];
        Set<Id> linkIds = new Set<Id>();
        for(ContentDocumentLink cdl : links ) {
            linkIds.add(cdl.LinkedEntityId );
        }
        
        //should only be one case
        List<Case> cases = [SELECT Id, ParentId, RecordType.DeveloperName from Case where Id In :linkIds];
        if( !cases.isEmpty()) {
            if( RestrictedCaseRecordsUtil.isRestrictedCaseRecordType(cases[0].Id, cases[0].RecordType.DeveloperName) ) {
                
                ContentVersion oldCv = Trigger.oldMap.get(cv.Id);
                ContentVersion newCv = Trigger.newMap.get(cv.Id);
                
                boolean validUpdate = RestrictedCaseRecordsUtil.checkUpdatedFields(oldCv, newCv, 'ContentVersion');
                
                if (!validUpdate) {
                    newCv.addError(System.Label.Restricted_Object_Update);
                }
            }
        }
    }
}