trigger RelinkContentDocumentToCaseTrigger on FeedItem (after delete) {
    
    for ( FeedItem fi : Trigger.old ) {
		
        // we are only relinking ContentDocuments to the Case when a FeedItem with an attachment is removed
        // this is only necessary if the case record type is restricted
        if( fi.Type == 'ContentPost') {
            
            // should only be one case
            List<Case> cases = [SELECT Id, ParentId, RecordType.DeveloperName from Case where Id = :fi.ParentId];
            if( !cases.isEmpty()) {
                if( RestrictedCaseRecordsUtil.isRestrictedCaseRecordType(cases[0].Id, cases[0].RecordType.DeveloperName) ) {
                    List<ContentVersion> cvs = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :fi.RelatedRecordId];
                    
                    // recreate ContentDocumentLink to case
                    if ( !cvs.isEmpty() ) {
                        ContentDocumentLink cdl = new ContentDocumentLink();
                        cdl.LinkedEntityId = cases[0].Id;
                        cdl.ContentDocumentId = cvs[0].ContentDocumentId;
                        cdl.ShareType = 'V';
                        insert cdl;
                    }

                }
            }
        }
    }
}