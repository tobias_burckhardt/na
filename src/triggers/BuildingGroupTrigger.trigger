trigger BuildingGroupTrigger on Building_Group__c (after update) {
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            BuildingGroupTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}