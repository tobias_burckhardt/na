trigger RequestForProjectApprovalTrigger on Request_for_Project_Approval__c (before update,
        after update) {
	if (Trigger.isBefore) {
		if (Trigger.isUpdate) {
			RequestForProjectApprovalTriHandler.handleBeforeUpdate(Trigger.new, Trigger.oldMap);
		}
	} else if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            RequestForProjectApprovalTriHandler.handleAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}