trigger sika_SCCZoneUpdateCaseOnCommentSubmitTrigger on CaseComment (before insert) {

	// get types listed in Case.SCC_Zone_Case_Types__c
	list<SelectOption> selectOptions = sika_SCCZonePicklistValuesHelper.getPicklistValues(new Case(), 'SCC_Zone_Case_Types__c');
	set<String> SCCZoneCaseTypes = new set<String>();

	for(SelectOption s : selectOptions){
		SCCZoneCaseTypes.add(s.getLabel());
	}
	
	set<Id> caseId = new set<Id>();

	// for each caseComment
	for(CaseComment cc : trigger.new){

		// get the comment's associated CaseId & add it to the caseId set
		caseId.add(cc.ParentId);
	}

	// get cases where CaseId IN set
	list<Case> theCases = [SELECT Id, Type FROM Case WHERE Id IN :caseId];

	list<Case> SCCZoneCases = new list<Case>();

	// if the case is of one of the types listed in SCC_Zone_Case_Types__c, add it to a set of cases to be updated
	for(Case c : theCases){
		if(SCCZoneCaseTypes.contains(c.Type)){
			SCCZoneCases.add(c);
		}
	}

	// update any SCCZone cases passed to the trigger.  (No changes; just want to update the lastModified date so that cases display in the correct order on the SCC Zone Support Homepage.)
	update(SCCZoneCases);

}