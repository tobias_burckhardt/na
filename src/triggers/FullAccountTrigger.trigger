/**
 * Add to Account Team when user Creates, updates or owns Account record.
 * When Account owner changes recreate existing members created programatically.
 */
trigger FullAccountTrigger on Account (after insert, after update, before update) {
    AccountTriggerHandler handler = new AccountTriggerHandler(Trigger.isExecuting, Trigger.size);

    if (Trigger.isBefore) {
        if (Trigger.isUpdate) {
            handler.onBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
        }
    } else if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            handler.onAfterInsert(Trigger.new, Trigger.newMap);
        } else if (Trigger.isUpdate) {
            handler.onAfterUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
        }
    }
}