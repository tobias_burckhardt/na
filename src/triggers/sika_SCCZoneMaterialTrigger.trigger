trigger sika_SCCZoneMaterialTrigger on Material__c (before update, before delete, after update) {

    String userType = UserInfo.getUserType();

    // prevents Communities users from editing or deleting Admixture materials.  (Admixture material records are visible to
    // anyone, but can only be added/edited/deleted by Sika users.) note that there is no "delete" option on the SCC Zone
    // visualforce pages for admixtures. This just tightens up the security.  (No URL hacking & deleting all Sika's
    // admixture records!!)
    if (Trigger.isUpdate || Trigger.isDelete) {
        if (userType == 'CSPLitePortal' || userType == 'CustomerSuccess' || userType == 'PowerCustomerSuccess') {
            for (Material__c m : Trigger.old) {
                if (m.Type__c.toLowercase() == 'admixture') {
                    m.addError('Insufficient permissions: Only Sika employees may edit or delete Admixture records.');
                }
            }
        }
    }

    // prevents salespeople from editing or deleting records associated with accounts on which they are not
    // accountTeamMembers (or owners) Will only fire once per execution context.
    if (sika_SCCZoneMaterialTriggerHelper.bypassAccountTeamCheck == false) {
        if (Trigger.isUpdate || Trigger.isDelete) {
            list<Material__c> theMaterials = new list<Material__c>();
            if (Trigger.isUpdate) {
                sika_SCCZoneMaterialTriggerHelper.accountTeamCheck(Trigger.new, 'edit');
            }
            if (Trigger.isDelete) {
                sika_SCCZoneMaterialTriggerHelper.accountTeamCheck(Trigger.old, 'delete');
            }
        }
    }

    // if a material is updated, its related Mix_Material records are also (or should also be) affected. (This is here to
    // fire any triggers associated with a Mix_Material update.)
    if (sika_SCCZoneMaterialTriggerHelper.bypassMixMaterialUpdate == false) {
        if (Trigger.isUpdate) {
            sika_SCCZoneMaterialTriggerHelper.updateMixMaterials(trigger.newMap.keySet());
        }
    }

    // if a material's owner has changed, its Account__c field should be updated
    if (sika_SCCZoneMixTriggerHelper.bypassMixOwnerTransferCheck == false) {
        if (Trigger.isBefore && Trigger.isUpdate) {
            sika_SCCZoneMaterialTriggerHelper.materialOwnerTransferCheck(Trigger.new, Trigger.oldMap);
        }
    }

    // prevents Materials that are used in Mixes from being deleted. (They should be disabled, not deleted!)
    if (sika_SCCZoneMaterialTriggerHelper.bypassPreventDelete == false) {
        if (Trigger.isDelete) {
            sika_SCCZoneMaterialTriggerHelper.preventDelete(trigger.old);
        }
    }

    // prevents Materials that are used in LOCKED Mixes from being edited
    if (sika_SCCZoneMaterialTriggerHelper.bypassPreventEdit == false) {
        if (Trigger.isUpdate) {
            sika_SCCZoneMaterialTriggerHelper.preventEdit(trigger.new);
        }
    }

    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            MaterialService.updateMixMaterialMoistureAdjustment(Trigger.new, Trigger.oldMap);
        }
    }
}