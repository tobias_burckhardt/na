/*Team : Apttus Managed Services
Subject: Line Item trigger for approval process
Author : Shashivind Mishra , Ankit Gupta*/

trigger APTS_LineitemTrigger on Apttus_Config2__LineItem__c (after update) {
    
    
  
    
    Boolean checkDiscuont = false;
    Boolean checkNationalDiscount = false;
    Boolean checkRegionalDiscount = false;
    
    
    
    
    List<Apttus_Config2__LineItem__c> lstlineItem = trigger.new;
    
    Apttus_Config2__ProductConfiguration__c con = new Apttus_Config2__ProductConfiguration__c(); 
    con.APTS_CheckDiscount__c = false;
    con.APTS_CheckNationalDiscount__c = false;
    con.APTS_CheckRegionalDiscount__c = false;
    
    if(lstlineItem.size()>0 && trigger.new!=trigger.old){
        for(Apttus_Config2__LineItem__c lineItem:[Select id, Name,APTS_CheckDiscount__c, APTS_CheckRegionalDiscount__c ,APTS_CheckNationalDiscount__c, Apttus_Config2__ConfigurationId__r.APTS_CheckDiscount__c,Apttus_Config2__ConfigurationId__c, Apttus_Config2__ConfigurationId__r.APTS_CheckRegionalDiscount__c ,Apttus_Config2__ConfigurationId__r.APTS_CheckNationalDiscount__c from Apttus_Config2__LineItem__c where Apttus_Config2__ConfigurationId__c=:lstlineItem[0].Apttus_Config2__ConfigurationId__c]){
            con.id = lstlineItem[0].Apttus_Config2__ConfigurationId__c; 
            system.debug('======'+lineItem.APTS_CheckDiscount__c+'------'+lineItem.APTS_CheckRegionalDiscount__c+'-------'+lineItem.APTS_CheckNationalDiscount__c);
        
            if(lineItem.APTS_CheckDiscount__c==true){
                //checkDiscuont= true;
                con.APTS_CheckDiscount__c = true;
            }
            
            if(lineItem.APTS_CheckNationalDiscount__c == true){
                //checkNationalDiscount = true;
                con.APTS_CheckNationalDiscount__c = true;
            } 
            
            if(lineItem.APTS_CheckRegionalDiscount__c == true){
                //checkRegionalDiscount = true;
                con.APTS_CheckRegionalDiscount__c = true;
                
            }
        
         }
    
       }
       
       if(con != null && con.Id != null){
           update con;
       }
       
 

}