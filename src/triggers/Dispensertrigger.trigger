trigger Dispensertrigger on Dispenser_Installation__c (before insert,before update) {
    list<id> rid = new list<id>();
    set<id> did = new set<id>();
    for(Dispenser_Installation__c di : trigger.new){
      if(di.Region__c !=null){
      rid.add(di.Region__c);
      if(trigger.isupdate && trigger.isbefore){
      if(di.Approval_Status__c =='Approved')
      did.add(di.id);
      }
      }
    }
   // list<id> tmid = new list<id>();
    map<id,id> rmid = new map<id,id>();
    map<id,id> tmid = new map<id,id>();
    list<Region__c> rlist = [select id,name,Regional_Manager_EMPL__r.SFDC_User_Account__c,Target_Market_Lookup__r.Technical_Contact__r.SFDC_User_Account__c,Target_Market_Lookup__r.Business_Manager__r.SFDC_User_Account__c from Region__c where id=:rid];
    for(Region__c r : rlist){
    rmid.put(r.id,r.Regional_Manager_EMPL__r.SFDC_User_Account__c);
    tmid.put(r.id,r.Target_Market_Lookup__r.Business_Manager__r.SFDC_User_Account__c);
  //  tmid.add(r.Target_Market_Lookup__r.TM_VP__c);
    }
    for(Dispenser_Installation__c di : trigger.new){
    di.Regional_Sales_Manager__c = rmid.get(di.region__c);
    di.TM_VP__c = tmid.get(di.region__c);
    }
    if(did.size()>0){
    list<ContentDocumentLink> contentlist = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN:did];
       if(contentlist.size()==0){
    for(Dispenser_Installation__c di : trigger.new){
    di.adderror('Please attach lease document');
    }
    }
    }
    
}