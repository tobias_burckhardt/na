/**
 * Add to Opp Team when user Creates, updates or owns opportunity record.
 * When Opportunity owner changes recreate existing members created programatically.
 */
trigger FullOpportunityTrigger on Opportunity (after insert, after update, before update) {
    if (Trigger.isAfter) {
        // manages sharing records based upon accountId being updated on Opportunity
        OpportunityTriggerHandler handler = new OpportunityTriggerHandler(Trigger.isExecuting, Trigger.size);
        if (Trigger.isInsert) {
            handler.onAfterInsert(Trigger.new);
        } else if (Trigger.isUpdate) {
            handler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }        
        
        List<OpportunityTeamMember> members = new List<OpportunityTeamMember>();         
        OpportunityTeamMember member = null;
        Map<Id, List<OpportunityTeamMember>> oppMembersMapTemp = OpportunityTriggerHelper.oppMembersMap;
        if (oppMembersMapTemp == null) {
            oppMembersMapTemp = new Map<Id, List<OpportunityTeamMember>>();
        }
        String userId = UserInfo.getUserId();
        
        
        Set<Id> oppIds = new Set<Id>();
        for (Opportunity opp : Trigger.new) {
            if (opp.Id != null) {
                oppIds.add(opp.Id);
            }
        }
        
        List<List<OpportunityTeamMember>> listOfmembersExisting = oppMembersMapTemp.values();
        Map<String, String> userIdRoleMap = new Map<String,String>();
        for (List<OpportunityTeamMember> membersExisting : listOfmembersExisting) {
            for (OpportunityTeamMember otm : membersExisting) {
                if (userIdRoleMap.get(otm.opportunityId + '' + otm.UserId) == null) {
                    userIdRoleMap.put(otm.opportunityId + '' + otm.UserId, otm.TeamMemberRole);
                }
            }
        }

        Set<Id> ptsProfiles = Pluck.ids(UserDao.getUserPartnerProfileIds());
        for (Opportunity opp : Trigger.new) {
            // PTS Users should not be added as team members
            if (ptsProfiles.contains(UserInfo.getProfileId())) {
                break;
            }
            member = new OpportunityTeamMember();                
            member.OpportunityId = opp.Id;                
            member.UserId = UserInfo.getUserId();
            
            if (Trigger.isInsert || (Trigger.isUpdate && opp.OwnerId != Trigger.oldMap.get(opp.Id).OwnerId)) {
                if (oppMembersMapTemp != null && oppMembersMapTemp.get(opp.Id) != null) {
                    members.addAll(oppMembersMapTemp.get(opp.Id)); 
                }
            }   
            
            if (userId.startsWith('005')) {
                if (Trigger.isInsert) {                
                    member.TeamMemberRole = 'Opportunity Created';
                    members.add(member);
                } else if (Trigger.isUpdate && opp.OwnerId != Trigger.oldMap.get(opp.Id).OwnerId && opp.OwnerId != UserInfo.getUserId()) {                
                    member.TeamMemberRole = 'Opportunity Edited';
                    if (userIdRoleMap.get(opp.Id + '' + member.UserId) != null) {
                        member.TeamMemberRole = userIdRoleMap.get(opp.Id+''+member.UserId);
                    }

                    members.add(member);
                    
                    OpportunityTeamMember member1 = new OpportunityTeamMember();                
                    member1.OpportunityId = opp.Id;                
                    member1.UserId = opp.OwnerId;
                    member1.TeamMemberRole = 'Opportunity Owner';
                    members.add(member1);
                } else if (opp.OwnerId == UserInfo.getUserId()) {               
                    member.TeamMemberRole = 'Opportunity Owner';
                    members.add(member);
                } else if (opp.OwnerId != UserInfo.getUserId()) {
                    OpportunityTeamMember member1 = new OpportunityTeamMember();                
                    member1.OpportunityId = opp.Id;                
                    member1.UserId = opp.OwnerId;
                    member1.TeamMemberRole = 'Opportunity Owner';
                    members.add(member);
                    members.add(member1);
                    
                    member.TeamMemberRole = 'Opportunity Edited';
                    if (userIdRoleMap.get(opp.Id + '' + member.UserId) != null) {
                        member.TeamMemberRole = userIdRoleMap.get(opp.Id + '' + member.UserId);
                    }
                }
            }
        }

        insert members;

        OpportunitySharingServices.manageOpportunityTeamSharing(Trigger.new);

    } else if (Trigger.isBefore) {
        if (Trigger.isUpdate) {
            Set<Id> oppIds = new Set<Id>();

            for (Opportunity o : Trigger.new) {
                oppIds.add(o.Id);
            }

            List<OpportunityTeamMember> members = [
                    SELECT Id, OpportunityId, UserId, TeamMemberRole
                    FROM OpportunityTeamMember
                    WHERE OpportunityId IN :oppIds
            ];

            if (members.size() > 0) {
                List<OpportunityTeamMember> oppMembers = members.deepClone();
                Map<Id, List<OpportunityTeamMember>> oppMembersMapTemp =
                        new Map<Id, List<OpportunityTeamMember>>();
                for (OpportunityTeamMember otm : oppMembers) {
                    if (oppMembersMapTemp.get(otm.OpportunityId) == null) {
                        oppMembersMapTemp.put(otm.OpportunityId, new List<OpportunityTeamMember>());
                    }

                    oppMembersMapTemp.get(otm.OpportunityId).add(otm);
                }

                for (OpportunityTeamMember otm : oppMembers) {
                    if (Trigger.newMap.get(otm.OpportunityId).OwnerId != Trigger.oldMap.get(otm.OpportunityId).OwnerId) {
                        if (otm.TeamMemberRole == 'Opportunity Owner') {
                            otm.TeamMemberRole = 'Former Owner';
                        }
                    }
                }

                OpportunityTriggerHelper.oppMembersMap = oppMembersMapTemp;
            }
        }
    }
}