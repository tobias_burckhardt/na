trigger ContentDocumentLinkTrigger on ContentDocumentLink (before delete, after insert) {
    if (Trigger.isBefore) {
        if (Trigger.isDelete) {
            ContentDocumentLinkTriggerHandler.handleBeforeDelete(Trigger.old);
        }
    } else if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            ContentDocumentLinkTriggerHandler.handleAfterInsert(Trigger.new);
        }
    }
}