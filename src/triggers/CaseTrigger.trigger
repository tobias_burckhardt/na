trigger CaseTrigger on Case (before insert) {
	
	if(Trigger.isInsert && Trigger.isBefore) {
       
       CaseTriggerHandler.filterWebToCase( Trigger.New );
       CaseTriggerHandler.populateAccountandContactMaps();
       CaseTriggerHandler.caseFieldMapping();
       
    }
}