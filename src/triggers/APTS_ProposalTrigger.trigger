trigger APTS_ProposalTrigger on Apttus_Proposal__Proposal__c (before insert, before update) {
     if(Trigger.isbefore && Trigger.isUpdate) {
        APTS_ProposalTriggerHandler.onBeforeUpdate(Trigger.oldMap, Trigger.newMap);
    }
     if(Trigger.isbefore &&Trigger.isInsert) {
        APTS_ProposalTriggerHandler.onBeforeInsert(Trigger.new);
    }

}