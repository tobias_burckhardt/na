trigger NewDocumentLinkUpload on ContentDocumentLink (after insert, after update, after delete) {
    if(Trigger.isAfter){       
        if(Trigger.isInsert || Trigger.isUpdate){
            system.debug('Trigger invoked for ContentDocumentLink');
                       
            for (ContentDocumentLink doc : trigger.new) {
                system.debug('====== ContentDocumentId: ' + doc.ContentDocumentId);
                system.debug('====== Id: ' + doc.ContentDocumentId);
                
                //AWS_Connection awsS3 = new AWS_Connection();
                
                AWS_v4_auth awsS3     = new AWS_v4_auth();
                 
                if (awsS3.shouldPublishToAWS(doc.ContentDocumentId)) {
					ContentVersion fileVersion;
                    
                    fileVersion = [SELECT Id, Title, Document_Type__c, Sub_Type_Level_1__c, Sub_Type_Level_2__c, Sub_Type_Level_3__c, FileExtension, ContentDocumentId, VersionData, ContentSize FROM ContentVersion WHERE ContentDocumentId = :doc.ContentDocumentId LIMIT 1];
                    awsS3.saveToS3(fileVersion, fileVersion.FileExtension);
                            
                }
            }          
        }  
    }
}