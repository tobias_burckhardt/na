trigger TaskTrigger on Task (after insert) {
    if (Trigger.isAfter) {
        if (Trigger.isInsert){
           TaskServiceHandler.addMember(Trigger.new);
        }
    }
}