trigger sika_SCCZoneMixTrigger on Mix__c (before insert, after insert, before update, after update, before delete,
        after delete) {

    // prevents salespeople from editing or deleting records associated with accounts on which they are not accountTeamMembers (or owners)
    // Will only run once per execution context.
    if (Sika_SCCZoneMixTriggerHelper.bypassAccountTeamCheck == false) {
        if (Trigger.isBefore && (Trigger.isUpdate || Trigger.isDelete)) {
            if (Trigger.isUpdate) {
                Sika_SCCZoneMixTriggerHelper.accountTeamCheck(Trigger.new, 'edit');
            }
            if (Trigger.isDelete) {
                Sika_SCCZoneMixTriggerHelper.accountTeamCheck(Trigger.old, 'delete');
            }
        }
    }

    // recalculates any SCM mix_material's quantity if the Mix.Target_SCM_Ratio field changes
    if (Sika_SCCZoneMixTriggerHelper.bypassUpdateSCMQuantity == false) {
        if (Trigger.isAfter && Trigger.isUpdate) {
            Sika_SCCZoneMixTriggerHelper.updateSCMQuantity(Trigger.new);
        }
    }

    // unlocks Materials when the last locked Mix they're associated with is deleted. (When the last locked mix they're associated with is UNLOCKED, the unlock action is fired by the Test Results (on delete) trigger.)
    if (Trigger.isAfter && Trigger.isDelete) {
        Sika_SCCZoneMixTriggerHelper.unlockMaterials(Trigger.old);
    }

    // changes owner of attachments when a mix owner is changed.  (Mix_Material and Test_Results owners are changed automatically.) Use case: Salesperson creates mix & transfers ownership to SCC Zone user.
    // Also updates the Account associated with the mix when the mix owner is changed.
    if (Sika_SCCZoneMixTriggerHelper.bypassMixOwnerTransferCheck == false) {
        if (Trigger.isBefore && Trigger.isUpdate) {
            Sika_SCCZoneMixTriggerHelper.mixOwnerTransferCheck(Trigger.new, Trigger.oldMap);
        }
    }

    // alerts sales rep(s) via Chatter when an SCC Zone user associated with one of their accounts creates or optimizes a mix
    if (Sika_SCCZoneMixTriggerHelper.bypassMixChatterAlert == false) {
        if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
            Sika_SCCZoneMixTriggerHelper.mixChatterAlert(Trigger.new, Trigger.oldMap);
        }
    }

    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            MixService.updateMixMaterialMoistureAdjustment(Trigger.new, Trigger.oldMap);
        }
    }
}