trigger NewDocumentVersion on ContentVersion (after insert, after update) {
    if(Trigger.isAfter){       
        if(Trigger.isInsert){
            system.debug('Trigger invoked for content version insert');
            
            for (ContentVersion doc : trigger.new) {
                system.debug('====== ContentDocumentId: ' + doc.ContentDocumentId);
                system.debug('====== Id: ' + doc.Id);
                
                //AWS_Connection awsS3 = new AWS_Connection();
                
                AWS_v4_auth awsS3     = new AWS_v4_auth();
                
                if (awsS3.shouldPublishToAWS(doc.ContentDocumentId)) {
                    
                    ContentVersion fileVersion;
                    
                    fileVersion = [SELECT Id, Title, Document_Type__c, Sub_Type_Level_1__c, Sub_Type_Level_2__c, Sub_Type_Level_3__c, FileExtension, ContentDocumentId, VersionData, ContentSize FROM ContentVersion WHERE ContentDocumentId = :doc.ContentDocumentId LIMIT 1];
                    
                    awsS3.saveToS3(doc, fileVersion.FileExtension);
                    
                }
            }
        } else if (Trigger.isUpdate) {
            system.debug('Trigger invoked for content version update');
            
            
            for(ContentVersion newDoc : Trigger.new){  // iterates through new versions of records
                ContentVersion oldDoc = Trigger.oldMap.get(newDoc.Id); // get the old version of the current record
                
                //AWS_Connection awsS3 = new AWS_Connection();
                
                AWS_v4_auth awsS3     = new AWS_v4_auth();
                
                if (awsS3.shouldPublishToAWS(oldDoc.ContentDocumentId)) {
                    if(oldDoc.Document_Type__c != null ) {  // if top level cat is null, we will assume is a new file
                        //if(getLibId(oldDoc.doContentDocumentIdc) == getLibId(newDoc.doContentDocumentIdc)) { //check if lib id for old is equal to lib id of new
                            if(oldDoc.Document_Type__c != newDoc.Document_Type__c ||
                               oldDoc.Sub_Type_Level_1__c != newDoc.Sub_Type_Level_1__c ||
                               oldDoc.Sub_Type_Level_2__c != newDoc.Sub_Type_Level_2__c ||
                               oldDoc.Sub_Type_Level_3__c != newDoc.Sub_Type_Level_3__c) { // check whether any of the categories have been updated or not
                                   system.debug('Document Type: ' + oldDoc.Document_Type__c + ' - ' + newDoc.Document_Type__c);
                                   system.debug('Sub_Type_Level_1__c: ' + oldDoc.Sub_Type_Level_1__c + ' - ' + newDoc.Sub_Type_Level_1__c);
                                   system.debug('Sub_Type_Level_2__c: ' + oldDoc.Sub_Type_Level_2__c + ' - ' + newDoc.Sub_Type_Level_2__c);
                                   system.debug('Sub_Type_Level_3__c: ' + oldDoc.Sub_Type_Level_3__c + ' - ' + newDoc.Sub_Type_Level_3__c);
                                   system.debug('Call moveInS3 ');
                                   
                                   
                                   ContentVersion fileVersion;
                                   
                                   fileVersion = [SELECT Id, Title, Document_Type__c, Sub_Type_Level_1__c, Sub_Type_Level_2__c, Sub_Type_Level_3__c, FileExtension, ContentDocumentId, VersionData, ContentSize FROM ContentVersion WHERE ContentDocumentId = :newDoc.ContentDocumentId LIMIT 1];
                                   
                                   // Call saveToS3  
                                   awsS3.saveToS3(fileVersion, fileVersion.FileExtension);
                                   
                                   // Call deleteFromS3
                                   awsS3.deleteFromS3(oldDoc, fileVersion.FileExtension);
                               }
                        //}
                    }
                }
            }
        }
    }
}