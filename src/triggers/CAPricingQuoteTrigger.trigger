trigger CAPricingQuoteTrigger on Quote (after update) {
    Set<String> quoteIds = new Set<String>();
    if(Trigger.isUpdate && Trigger.isAfter){
        for(Quote quoteObj:trigger.new){
            quoteIds.add(quoteObj.Id);
        }
        System.debug('quoteIds:'+quoteIds);
     }
    // GET RECORD TYPE ID OF SPA PRICING RECORD TYPE
    Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('SPA Pricing').getRecordTypeId();
    System.debug('quoteRecordTypeId:'+quoteRecordTypeId);
    // GET QUOTE OBJECTS WITH THEIR RECORD TYPES
    List<Quote> quotes = [Select Id,Name,RecordTypeId,Approved_Date__c from Quote where Id IN:quoteIds];
    System.debug('quotes:'+quotes);
    // SET TO INCLUDE SPA QUOTES ONLY
    Map<ID,Quote> spaQuotesMap = new Map<ID,Quote>();
    for(Quote quote:quotes){
        if(quote.RecordTypeId==quoteRecordTypeId){
            spaQuotesMap.put(quote.Id,quote);
        }else{
            System.debug('This quote has a different record type: '+quote.RecordTypeId);
        }
    }
    System.debug('spaQuotesMap:'+spaQuotesMap);
    // 1ST ENTRY POINT
    if(spaQuotesMap!=null && spaQuotesMap.size()>0){
        Map<Id,Quote> validFromChangedForQuotes = new Map<Id,Quote>();
        for( Id quoteId : Trigger.newMap.keySet() ){
          if(Trigger.oldMap.get( quoteId ).Approved_Date__c != Trigger.newMap.get( quoteId ).Approved_Date__c ){
             validFromChangedForQuotes.put(quoteId,Trigger.newMap.get( quoteId ));
          }
        }
        // 2ND ENTRY POINT
        if(validFromChangedForQuotes!=null){
            Map<Id,Date> quoteWithApprovedDateMap = new Map<Id,Date>();
            for(Quote quoteObj:validFromChangedForQuotes.values()){
                if(quoteObj.Approved_Date__c!=null)
                    quoteWithApprovedDateMap.put(quoteObj.Id,quoteObj.Approved_Date__c);
            }
            System.debug('quoteWithApprovedDateMap--'+quoteWithApprovedDateMap);
            // 3RD ENTRY POINT
            if(quoteWithApprovedDateMap!=null){
                List<QuoteLineItem> updateRelatedQuoteLineItems = new List<QuoteLineItem>();
                if(!Test.isRunningTest()){
                    for(QuoteLineItem quoteLineItemObj:[Select Id,Valid_From__c,QuoteId from QuoteLineItem where QuoteId IN:quoteWithApprovedDateMap.keySet()]){
                        quoteLineItemObj.Valid_From__c = quoteWithApprovedDateMap.get(quoteLineItemObj.quoteId);
                        System.debug('quoteLineItemObj--'+quoteLineItemObj);
                        updateRelatedQuoteLineItems.add(quoteLineItemObj);
                    }
                }
                
                System.debug('updateRelatedQuoteLineItems-1-'+updateRelatedQuoteLineItems);
                if(updateRelatedQuoteLineItems!=null && updateRelatedQuoteLineItems.size()>0)
                	update updateRelatedQuoteLineItems;
                System.debug('updateRelatedQuoteLineItems-2-'+updateRelatedQuoteLineItems);
            }
        }
    }
}