trigger AttachmentRestrictUpdateDeleteTrigger on Attachment (before update, before delete) {
    List<Attachment> caseAttachments = new List<Attachment>();
    Map<Id, List<Attachment>> tasks = new Map<Id, List<Attachment>>();
    Map<Id, List<Attachment>> events = new Map<Id, List<Attachment>>();
    Map<Id, List<Attachment>> emails = new Map<Id, List<Attachment>>();

    //If an attachment is related directly to a restricted case, add it to the list of filtered attachments
    //If it is related to a task, event, or email message, add the object to the map under that related item's Id
    for (Attachment attach : [SELECT Id, ParentId, Parent.RecordType.DeveloperName FROM Attachment WHERE Id IN :Trigger.oldMap.keySet()]) {
        Id parent = attach.ParentId;
        if (attach.ParentId.getSobjectType() == Schema.Task.sObjectType) {
            if (tasks.get(parent) == null) {
                tasks.put(parent, new List<Attachment>());
            }
            tasks.get(parent).add(attach);
        } else if (attach.ParentId.getSobjectType() == Schema.Event.sObjectType) {
            if (events.get(parent) == null) {
                events.put(parent, new List<Attachment>());
            }
            events.get(parent).add(attach);
        } else if (attach.ParentId.getSobjectType() == Schema.EmailMessage.sObjectType) {
            if (emails.get(parent) == null) {
                emails.put(parent, new List<Attachment>());
            }
            emails.get(parent).add(attach);
        } else if (RestrictedCaseRecordsUtil.isRestrictedCaseRecordType(parent, attach.Parent.RecordType.DeveloperName)) {
            caseAttachments.add(attach);
        }
    }

    //For all the tasks collected above, if the task is related to a restricted case,
    //  add its attachments to the list of filtered attachments
    for (Task tsk : [SELECT Id, WhatId, What.RecordType.DeveloperName FROM Task WHERE Id IN :tasks.keySet() AND WhatId <> NULL]) {
        if (RestrictedCaseRecordsUtil.isRestrictedCaseRecordType(tsk.WhatId, tsk.What.RecordType.DeveloperName)) {
            caseAttachments.addAll(tasks.get(tsk.Id));
        }
    }

    //Same as for Tasks but with Events instead
    for (Event ev : [SELECT Id, WhatId, What.RecordType.DeveloperName FROM Event WHERE Id IN :events.keySet() AND WhatId <> NULL]) {
        if (RestrictedCaseRecordsUtil.isRestrictedCaseRecordType(ev.WhatId, ev.What.RecordType.DeveloperName)) {
            caseAttachments.addAll(events.get(ev.Id));
        }
    }
    
    //Same as for Tasks/Events, with Emails
    for (EmailMessage em : [SELECT Id, ParentId, Parent.RecordType.DeveloperName FROM EmailMessage WHERE Id IN :emails.keySet() AND ParentId <> NULL]) {
        if (RestrictedCaseRecordsUtil.isRestrictedCaseRecordType(em.ParentId, em.Parent.RecordType.DeveloperName)) {
            caseAttachments.addAll(emails.get(em.Id));
        }
    }

    //For all the filtered attachments, do not allow deletions or updates to the fields specified in the Attachment custom setting
	for(Attachment attach : caseAttachments) {
        Attachment oldAttach = Trigger.oldMap.get(attach.Id);

        if (Trigger.isDelete) {
            oldAttach.addError(System.Label.Restricted_Object_Delete);
        } else if (Trigger.isUpdate) {
            Attachment newAttach = Trigger.newMap.get(attach.Id);
            boolean validUpdate = RestrictedCaseRecordsUtil.checkUpdatedFields(oldAttach, newAttach, 'Attachment');

            if (!validUpdate) {
                newAttach.addError(System.Label.Restricted_Object_Update);
            }
        }
    }
}