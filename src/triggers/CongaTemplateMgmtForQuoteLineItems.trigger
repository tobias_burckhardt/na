trigger CongaTemplateMgmtForQuoteLineItems on QuoteLineItem (after insert,after update,after delete) {
	 Map<id,Quote> quoteMapToUpdate = new Map<id,Quote>();
	 Set<String> quoteIds = new Set<String>();
    System.debug('Trigger Started');
	 if((Trigger.isInsert && Trigger.isAfter) || (Trigger.isUpdate && Trigger.isAfter)){
		for(QuoteLineItem qli : trigger.new) {
			quoteIds.add(qli.QuoteId);
		}
		System.debug('quoteIds:'+quoteIds);
	 }else if(Trigger.isDelete && Trigger.isAfter){
		for(QuoteLineItem qli : trigger.old) {
			quoteIds.add(qli.QuoteId);
		}
		System.debug('quoteIds:'+quoteIds);
	 }
	
	// GET RECORD TYPE ID OF SPA PRICING RECORD TYPE
	Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('SPA Pricing').getRecordTypeId();
	System.debug('quoteRecordTypeId:'+quoteRecordTypeId);
	// GET QUOTE OBJECTS WITH THEIR RECORD TYPES
	List<Quote> quotes = [Select Id,Name,RecordTypeId,Language__c from Quote where Id IN:quoteIds];
	System.debug('quotes:'+quotes);
	// SET TO INCLUDE SPA QUOTES ONLY
	Map<ID,Quote> spaQuotesMap = new Map<ID,Quote>();
	for(Quote quote:quotes){
		if(quote.RecordTypeId==quoteRecordTypeId){
			spaQuotesMap.put(quote.Id,quote);
		}else{
			System.debug('This quote has a different record type: '+quote.RecordTypeId);
		}
	}
	System.debug('spaQuotesMap:'+spaQuotesMap);
	// FIRST ENTRY POINT FOR PRICING QUOTES
	if(spaQuotesMap!=null && spaQuotesMap.size()>0){
		Map<id,Quote> quoteMapToUpdate = new Map<id,Quote>();
		for(String quoteId:spaQuotesMap.keySet()){
			quoteMapToUpdate.put(quoteId,new Quote(id = quoteId,Price_Tiering_for_SPA__c=''));
		}
		System.debug('quoteMapToUpdate:'+quoteMapToUpdate);
		Map<String,String> priceTierMapForQuotes = new Map<String,String>();
        Quote prevQuote;
		for (QuoteLineItem ql : [Select Id,QuoteId,Scale_Quantity_2__c,Scale_Quantity_3__c,Scale_Quantity_4__c,Scale_Rate_2__c,Scale_Rate_3__c,Scale_Rate_4__c
										   From QuoteLineItem 
										  Where QuoteId in :QuoteIds
									   Order By QuoteId NULLS LAST]) {
			Quote quoteObj = spaQuotesMap.get(ql.QuoteId);
			System.debug('quoteObj:'+quoteObj);
            System.debug('Scale_Rate_2__c:'+ql.Scale_Rate_2__c);  
			System.debug('Scale_Rate_3__c:'+ql.Scale_Rate_3__c); 
			System.debug('Scale_Rate_4__c:'+ql.Scale_Rate_4__c); 
            if(ql.Scale_Rate_4__c!=null && ql.Scale_Rate_4__c>0){
                if(String.isNotBlank(quoteObj.Language__c) && 'English'.equalsIgnoreCase(quoteObj.Language__c)){
                    priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_3_EN');
                }
                else if(String.isNotBlank(quoteObj.Language__c) && 'French'.equalsIgnoreCase(quoteObj.Language__c)){
                    priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_3_FR');
                }
                else{
                    priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_3_EN');
                }
            }
            else if(ql.Scale_Rate_3__c!=null && ql.Scale_Rate_3__c>0){
                if(priceTierMapForQuotes.containsKey(ql.QuoteId)){
                    String holdingValue = priceTierMapForQuotes.get(ql.QuoteId);
                    if(!holdingValue.startsWith('LEVEL_3')){
                        if(String.isNotBlank(quoteObj.Language__c) && 'English'.equalsIgnoreCase(quoteObj.Language__c)){
                            priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_2_EN');
                        }
                        else if(String.isNotBlank(quoteObj.Language__c) && 'French'.equalsIgnoreCase(quoteObj.Language__c)){
                            priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_2_FR');
                        }
                        else{
                            priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_2_EN');
                        }
                    }
                }else{
                    if(String.isNotBlank(quoteObj.Language__c) && 'English'.equalsIgnoreCase(quoteObj.Language__c)){
                        priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_2_EN');
                    }
                    else if(String.isNotBlank(quoteObj.Language__c) && 'French'.equalsIgnoreCase(quoteObj.Language__c)){
                        priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_2_FR');
                    }
                    else{
                        priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_2_EN');
                    }
                }
            }
            else if(ql.Scale_Rate_2__c!=null && ql.Scale_Rate_2__c>0){
                if(priceTierMapForQuotes.containsKey(ql.QuoteId)){
                    String holdingValue = priceTierMapForQuotes.get(ql.QuoteId);
                    if(!holdingValue.startsWith('LEVEL_3') && !holdingValue.startsWith('LEVEL_2')){
                        if(String.isNotBlank(quoteObj.Language__c) && 'English'.equalsIgnoreCase(quoteObj.Language__c)){
                            priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_1_EN');
                        }
                        else if(String.isNotBlank(quoteObj.Language__c) && 'French'.equalsIgnoreCase(quoteObj.Language__c)){
                            priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_1_FR');
                        }
                        else{
                            priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_1_EN');
                        }
                    }
            	}else{
                    if(String.isNotBlank(quoteObj.Language__c) && 'English'.equalsIgnoreCase(quoteObj.Language__c)){
                        priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_1_EN');
                    }
                    else if(String.isNotBlank(quoteObj.Language__c) && 'French'.equalsIgnoreCase(quoteObj.Language__c)){
                        priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_1_FR');
                    }
                    else{
                        priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_1_EN');
                    }
                }
            }
            else if(ql.Scale_Rate_2__c==null && ql.Scale_Rate_3__c==null && ql.Scale_Rate_4__c==null){
                if(priceTierMapForQuotes.containsKey(ql.QuoteId)){
                    String holdingValue = priceTierMapForQuotes.get(ql.QuoteId);
                    if(!holdingValue.startsWith('LEVEL_3') && !holdingValue.startsWith('LEVEL_2') && !holdingValue.startsWith('LEVEL_1')){
                       if(String.isNotBlank(quoteObj.Language__c) && 'English'.equalsIgnoreCase(quoteObj.Language__c)){
                            priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_0_EN');
                        }
                        else if(String.isNotBlank(quoteObj.Language__c) && 'French'.equalsIgnoreCase(quoteObj.Language__c)){
                            priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_0_FR');
                        }
                        else{
                            priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_0_EN');
                        }
                    }
                }else{
                    if(String.isNotBlank(quoteObj.Language__c) && 'English'.equalsIgnoreCase(quoteObj.Language__c)){
                        priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_0_EN');
                    }
                    else if(String.isNotBlank(quoteObj.Language__c) && 'French'.equalsIgnoreCase(quoteObj.Language__c)){
                        priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_0_FR');
                    }
                    else{
                        priceTierMapForQuotes.put(ql.QuoteId,'LEVEL_0_EN');
                    }
                }
            }
                                           
		System.debug('priceTierMapForQuotes:'+priceTierMapForQuotes);
		// GET PRICE TIER CONGA TEMPLATES 
		Map<String,String> languageTemplatesMap = new Map<String,String>();
		list<String> englishTemplateNames = new list<String>{'SPA With Price Tiering 4 - English','SPA With Price Tiering 3 - English','SPA With Price Tiering 2 - English','SPA With Price Tiering 1 - English'};
		list<APXTConga4__Conga_Template__c> englishTemplatesList = [select id,APXTConga4__Name__c from APXTConga4__Conga_Template__c where APXTConga4__Name__c IN:englishTemplateNames];
		System.debug('englishTemplatesList:'+englishTemplatesList);
		for(APXTConga4__Conga_Template__c templateObj:englishTemplatesList){
			if('SPA With Price Tiering 4 - English'.equalsIgnoreCase(templateObj.APXTConga4__Name__c))
				languageTemplatesMap.put('LEVEL_3_EN',templateObj.Id);
			if('SPA With Price Tiering 3 - English'.equalsIgnoreCase(templateObj.APXTConga4__Name__c))
				languageTemplatesMap.put('LEVEL_2_EN',templateObj.Id);
			if('SPA With Price Tiering 2 - English'.equalsIgnoreCase(templateObj.APXTConga4__Name__c))
				languageTemplatesMap.put('LEVEL_1_EN',templateObj.Id);
			if('SPA With Price Tiering 1 - English'.equalsIgnoreCase(templateObj.APXTConga4__Name__c))
				languageTemplatesMap.put('LEVEL_0_EN',templateObj.Id);
		}
		list<String> frenchTemplateNames = new list<String>{'SPA With Price Tiering 4-French','SPA With Price Tiering 3-French','SPA With Price Tiering 2-French','SPA With Price Tiering 1-French'};
		list<APXTConga4__Conga_Template__c> frenchTemplatesList = [select id,APXTConga4__Name__c from APXTConga4__Conga_Template__c where APXTConga4__Name__c IN:frenchTemplateNames];
		System.debug('frenchTemplatesList:'+frenchTemplatesList);
		for(APXTConga4__Conga_Template__c templateObj:frenchTemplatesList){
			if('SPA With Price Tiering 4-French'.equalsIgnoreCase(templateObj.APXTConga4__Name__c))
				languageTemplatesMap.put('LEVEL_3_FR',templateObj.Id);
			if('SPA With Price Tiering 3-French'.equalsIgnoreCase(templateObj.APXTConga4__Name__c))
				languageTemplatesMap.put('LEVEL_2_FR',templateObj.Id);
			if('SPA With Price Tiering 2-French'.equalsIgnoreCase(templateObj.APXTConga4__Name__c))
				languageTemplatesMap.put('LEVEL_1_FR',templateObj.Id);
			if('SPA With Price Tiering 1-French'.equalsIgnoreCase(templateObj.APXTConga4__Name__c))
				languageTemplatesMap.put('LEVEL_0_FR',templateObj.Id);
		}
		System.debug('languageTemplatesMap:'+languageTemplatesMap);
		// LOOP THROUGH languageTemplatesMap and priceTierMapForQuotes TO UPDATE quoteMapToUpdate WITH REQUIRED VALUES
		for(String quoteKey:priceTierMapForQuotes.keySet()){
			String priceTierLevelForQuote = priceTierMapForQuotes.get(quoteKey);
			String templateIdForPriceTierLevel = languageTemplatesMap.get(priceTierLevelForQuote);
			quoteMapToUpdate.get(quoteKey).Price_Tiering_for_SPA__c = templateIdForPriceTierLevel;
		}
        update quoteMapToUpdate.values();
		System.debug('priceTierMapForQuotes:'+priceTierMapForQuotes);
		System.debug('quoteMapToUpdate:'+quoteMapToUpdate);
	}
}
}