trigger sika_SCCZoneMixMaterialTrigger on Mix_Material__c (before insert, after insert, before update, after update,
        before delete, after delete) {

    Set<Id> affectedMixIDs;
    Set<Id> referencedMaterialIDs;

    // Prevents salespeople from editing or deleting records associated with accounts on which they are not accountTeamMembers or owners.
    // The SCC Zone permission set should be added to SCC Zone salespeople, and should provide view all & modify all access to the Mix Material object.
    // The sika_SCCZoneAccountTeamCheckHelper.accountTeamCheck() method will take the place of standard SFDC record access for SCC Zone custom objects,
    // since it is primarily based on whether a user is an accountTeamMember on the record's related Account that determines access levels.
    // This method is fired by all SCC Zone object triggers, but will only fire once per execution context.
    if (!Sika_SCCZoneMixMaterialTriggerHelper.bypassAccountTeamCheck) {
        if (Trigger.isBefore) {
            if (Trigger.isInsert || Trigger.isUpdate) {
                Sika_SCCZoneMixMaterialTriggerHelper.accountTeamCheck(Trigger.new, 'edit');
            } else if (Trigger.isDelete) {
                Sika_SCCZoneMixMaterialTriggerHelper.accountTeamCheck(Trigger.old, 'delete');
            }
        }
    }

    // Create Mix and Mix Material maps, which we will reference instead of querying the database for records that we've already retrieved.
    // (masterMixMap, masterMaterialMap, masterMixMaterialMap, mixId_to_MixMaterialId)
    Sika_SCCZoneMixMaterialTriggerHelper.initializeMasterMaps();

    affectedMixIDs = new Set<Id>();

    // If the trigger isn't firing on Delete, populate the Mix Materials & Materials maps from the Database,
    // and then add the records in the trigger to the maps.
    if (!Trigger.isDelete) {
        for (Mix_Material__c mm : Trigger.new) {
            affectedMixIDs.add(mm.Mix__c);

        }
        Sika_SCCZoneMixMaterialTriggerHelper.populateMixMaterialsMapsFromDB(affectedMixIDs);
        Sika_SCCZoneMixMaterialTriggerHelper.addToMixMaterialsMaps(Trigger.new);

    } else {
        // Populate the Mix Materials maps from the Database, and then remove the mix material records in the trigger from the maps.
        for (Mix_Material__c mm : Trigger.old) {
            affectedMixIDs.add(mm.Mix__c);
        }
        Sika_SCCZoneMixMaterialTriggerHelper.populateMixMaterialsMapsFromDB(affectedMixIDs);
        Sika_SCCZoneMixMaterialTriggerHelper.removeFromMixMaterialsMaps(Trigger.old, affectedMixIDs);

    }
    Sika_SCCZoneMixMaterialTriggerHelper.populateMixMap(affectedMixIDs);
    Sika_SCCZoneMixMaterialTriggerHelper.populateMaterialMap();

    // Update the [MaterialType]_Count fields on Mix
    // (Sand_Count__c, Aggregate_Count__c, Cement_Count__c, SCM_Count__c, Water_Count__c)
    if (!Sika_SCCZoneMixMaterialTriggerHelper.bypassPopulateMaterialTypeCounts) {
        if (Trigger.isBefore) {
            if (Trigger.isInsert) { // no need to recalculate on MM update; just on insert & delete.
                Sika_SCCZoneMixMaterialTriggerHelper.incrementMaterialTypeCounts(affectedMixIDs);
            } else if (Trigger.isDelete) {
                Sika_SCCZoneMixMaterialTriggerHelper.decrementMaterialTypeCounts(Trigger.old);
            }
        }
    }

    // update the SCMEffFactor value of the related Mix, if an SCM has been added or deleted.
    if (!Sika_SCCZoneMixMaterialTriggerHelper.bypassSetSCMWeightAndUnit) {
        if (Trigger.isBefore) {
            if (Trigger.isInsert || Trigger.isDelete) {
                Sika_SCCZoneMixMaterialTriggerHelper.setScmEffFactor(affectedMixIDs);
            }
        }

    }

    // Calculate Paste Volume (in liters) for Mixes (Mix__c.PasteVolumeNoAir__c)
    if (!Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculatePasteVolume) {
        if (Trigger.isBefore) {
            Sika_SCCZoneMixMaterialTriggerHelper.calculatePasteVolume(affectedMixIDs);
        }
    }

    // Calculate Total Mix Solids Volume in liters (Mix__c.Total_Mix_Solids_Volume_liters__c)
    if (!Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculateTotalSolidsVolumeLiters) {
        if (Trigger.isBefore) {
            Sika_SCCZoneMixMaterialTriggerHelper.calculateTotalSolidsVolumeLiters(affectedMixIDs);
        }
    }

    // Calculate Total Mix Volume in liters (Mix__c.Total_Volume_liters__c)
    if (!Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculateTotalMixVolumeLiters) {
        if (Trigger.isBefore) {
            Sika_SCCZoneMixMaterialTriggerHelper.calculateTotalMixVolumeLiters(affectedMixIDs);
        }
    }

    // Calculate variance in water weight, based on the actual % saturation of the materials in the mix
    // (Must run calculate SolidsVolumeLiters first)
    if (!Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculateWaterAdjustment) {
        if (Trigger.isBefore) {
            Sika_SCCZoneMixMaterialTriggerHelper.calculateWaterAdjustment(affectedMixIDs);
        }
    }

    // Calculate the various Mix Material Weight fields on Mix
    // (Total_Cement_Weight__c, Total_Water_Weight__c, Total_Weight_kgs__c)
    if (!Sika_SCCZoneMixMaterialTriggerHelper.bypassPopulateMaterialTypeWeights) {
        if (Trigger.isBefore) {
            Sika_SCCZoneMixMaterialTriggerHelper.populateMaterialTypeWeights(affectedMixIDs);
        }
    }

    // Populate the mix object's sieve value fields. These fields are used to render the concrete curve graph.
    if (!Sika_SCCZoneMixMaterialTriggerHelper.bypassConcreteCurveCalc) {
        if (Trigger.isBefore) {
            Sika_SCCZoneMixMaterialTriggerHelper.populateConcreteCurveValues(affectedMixIDs);
        }
    }

    // calculate the Total Raw Material Cost for each Mix
    if (!Sika_SCCZoneMixMaterialTriggerHelper.bypassCalculateTotalMaterialCost) {
        if (Trigger.isBefore) {
            Sika_SCCZoneMixMaterialTriggerHelper.calculateTotalMaterialCost(affectedMixIDs);
        }
    }

    // Add an error to any mix material that, when added to a mix, puts the mix over the maximum
    // allowable number of materials of the mix_material's type. (e.g., Mixes may contain at most 1 Cement material)
    if (!Sika_SCCZoneMixMaterialTriggerHelper.bypassAddOverLimitErrors) { // would only be true if a unit test set it as such
        if (Trigger.isAfter && Trigger.isInsert) {
            Sika_SCCZoneMixMaterialTriggerHelper.addOverLimitErrorsToMixMaterials(Trigger.new);
        }
    }

    // Set the Mix's isOptimized value to false if any of its Mix_Materials have changed, except if changed by the Optimizer
    // (If changed by the optimizer, set isOptimized == true)
    if (!Sika_SCCZoneMixMaterialTriggerHelper.bypassMixOptimizationCheck) {
        Sika_SCCZoneMixMaterialTriggerHelper.mixIsOptimizedCheck();
    }

    // Write all Mix changes made by this trigger from the masterMixMap to the database
    if (Trigger.isAfter) {
        Sika_SCCZoneMixMaterialTriggerHelper.writeOutMixMap();
    }

    if (Trigger.isBefore) {
        if (Trigger.isInsert || Trigger.isUndelete) {
            MixMaterialService.updateMoistureAdjustment(Trigger.new);
        } else if (Trigger.isUpdate) {
            MixMaterialService.updateMoistureAdjustment(Trigger.new, Trigger.oldMap);
        }
    }
}