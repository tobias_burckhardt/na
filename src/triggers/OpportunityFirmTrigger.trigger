trigger OpportunityFirmTrigger on Opportunity_Firm__c (after insert, after update, after delete, after undelete) {
    
    // this task was transferred to Krishma Kullen, May 26, 2016
    
   /* if (Trigger.isAfter) {
        if (Trigger.isInsert) { 
        	OpportunityFirmService.insertOpportunityContactRole(Trigger.NewMap);   
        } else if (Trigger.isUpdate) {
        	OpportunityFirmService.updateOpportunityContactRole();   
        } else if (Trigger.isDelete) {
         	OpportunityFirmService.deleteOpportunityContactRole(); 
        } else if (Trigger.isUndelete) {
        	//OpportunityFirmService.undeleteOpportunityContactRole();   
        }        
    } */
    
    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            OpportunityFirmTriggerService.createOpportynityContactRole(Trigger.new);
        }
    }
}