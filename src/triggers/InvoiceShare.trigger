trigger InvoiceShare on Invoice__c (after insert) {
    
    if(trigger.isInsert){
        // Create a new list of sharing objects for Invoice
        List<Invoice__Share> InvoiceShrs  = new List<Invoice__Share>();
        
        // Declare variables for Sales Rep Y1 - Y4 sharing
        Invoice__Share Y1Shr;
        Invoice__Share Y2Shr;
        Invoice__Share Y3Shr;
        Invoice__Share Y4Shr;
        
        for(Invoice__c Invoice : trigger.new){
            // Instantiate the sharing objects
            Y1Shr = new Invoice__Share();
            Y2Shr = new Invoice__Share();
            Y3Shr = new Invoice__Share();
            Y4Shr = new Invoice__Share();
            
            // Set the ID of record being shared
            Y1Shr.ParentId = Invoice.Id;
            Y2Shr.ParentId = Invoice.Id;
            Y3Shr.ParentId = Invoice.Id;
            Y4Shr.ParentId = Invoice.Id;
            
            // Set the ID of user or group being granted access
            Y1Shr.UserOrGroupId = Invoice.Sales_Rep_Y1__c;
            Y2Shr.UserOrGroupId = Invoice.Sales_Rep_Y2__c;
            Y3Shr.UserOrGroupId = Invoice.Sales_Rep_Y3__c;
            Y4Shr.UserOrGroupId = Invoice.Sales_Rep_Y4__c;
           
            // Set the access level
            Y1Shr.AccessLevel = 'read';
            Y2Shr.AccessLevel = 'read';
            Y3Shr.AccessLevel = 'read';
            Y4Shr.AccessLevel = 'read';
            
            // Set the Apex sharing reason for Y1 - Y4 (Omit and it will default to "Manual"
            // Y1Shr.RowCause = Schema.Invoice__Share.RowCause.Sales_Rep_Y1__c;
            // Y2Shr.RowCause = Schema.Invoice__Share.RowCause.Sales_Rep_Y2__c;
            // Y3Shr.RowCause = Schema.Invoice__Share.RowCause.Sales_Rep_Y3__c;
            // Y4Shr.RowCause = Schema.Invoice__Share.RowCause.Sales_Rep_Y4__c;
            
            // Add objects to list for insert
            InvoiceShrs.add(Y1Shr);
            InvoiceShrs.add(Y2Shr);
            InvoiceShrs.add(Y3Shr);
            InvoiceShrs.add(Y4Shr);
        }
        
        // Insert sharing records and capture save result 
        // The false parameter allows for partial processing if multiple records are passed 
        // into the operation 
        Database.SaveResult[] lsr = Database.insert(InvoiceShrs,false);
        
        // Create counter
        Integer i=0;
        
        // Process the save results
        for(Database.SaveResult sr : lsr){
            if(!sr.isSuccess()){
                // Get the first save result error
                Database.Error err = sr.getErrors()[0];
                
                // Check if the error is related to a trivial access level
                // Access levels equal or more permissive than the object's default 
                // access level are not allowed. 
                // These sharing records are not required and thus an insert exception is 
                // acceptable. 
                if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  
                                               &&  err.getMessage().contains('AccessLevel'))){
                    // Throw an error when the error is not related to trivial access level.
                    trigger.newMap.get(InvoiceShrs[i].ParentId).
                      addError(
                       'Unable to grant sharing access due to following exception: '
                       + err.getMessage());
                }
            }
            i++;
        }   
    }
    
}