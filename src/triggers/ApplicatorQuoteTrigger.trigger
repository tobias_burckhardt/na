trigger ApplicatorQuoteTrigger on Applicator_Quote__c (after update, after insert, after delete) {
    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            ApplicatorQuoteService.indicateSourcePlantInsert(Trigger.newMap);
            ApplicatorQuoteSharingServices.shareApplicatorQuoteWithPTS(Trigger.new);
        } else if (Trigger.isUpdate) {

            ApplicatorQuoteService.indicateSourcePlantUpdate(Trigger.NewMap, Trigger.oldMap);
            ApplicatorQuoteSharingServices.handleUpdateSharing(Trigger.new, Trigger.oldMap);
        }
    }
}