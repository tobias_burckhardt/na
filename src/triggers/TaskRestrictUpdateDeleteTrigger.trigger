trigger TaskRestrictUpdateDeleteTrigger on Task (before update, before delete) {
    Schema.FieldSet taskFieldSet = SObjectType.Task.FieldSets.Restricted_Fields;

    //For all the tasks in the trigger set, if they are related to a restricted case,
    //  do not allow deletions or updates to the fields specified in the Restricted_Fields field set
    for(Task tsk : [SELECT Id, WhatId, What.RecordType.DeveloperName FROM Task WHERE Id IN :Trigger.oldMap.keySet() AND WhatId <> NULL]) {
        if (RestrictedCaseRecordsUtil.isRestrictedCaseRecordType(tsk.WhatId, tsk.What.RecordType.DeveloperName)) {
	        Task oldTask = Trigger.oldMap.get(tsk.Id);

            if (Trigger.isDelete) {
                oldTask.addError(System.Label.Restricted_Object_Delete);
            } else if (Trigger.isUpdate && oldTask.IsClosed) {
	            Task newTask = Trigger.newMap.get(tsk.Id);
                boolean validUpdate = RestrictedCaseRecordsUtil.checkUpdatedFields(oldTask, newTask, taskFieldSet);

                if (!validUpdate) {
                    newTask.addError(System.Label.Restricted_Object_Update);
                }
            } else if (Trigger.isUpdate) {
	            Task newTask = Trigger.newMap.get(tsk.Id);
                if (oldTask.WhatId != newTask.WhatId) {
                    newTask.addError(System.Label.Restricted_Object_Update);
                }
            }
        }
    }
}