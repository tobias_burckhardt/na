trigger Asserttrigger on Asset_Type__c (after insert,after update,after delete) {
   list<id> did = new list<id>();
   list<datetime> dtime = new list<datetime>();
   if(trigger.isinsert || trigger.isupdate){
   for(Asset_Type__c p2 : trigger.new){
      if(p2.Dispenser_Installation__c !=null){
      did.add(p2.Dispenser_Installation__c);
      }
     
   }
   }
    if(trigger.isdelete){
   for(Asset_Type__c p2 : trigger.old){
      if(p2.Dispenser_Installation__c !=null){
      did.add(p2.Dispenser_Installation__c);
      }
   }
   }
    list<Dispenser_Installation__c> postlist = [select id,name,Post_Date__c from Dispenser_Installation__c where id IN:did];
    for(Dispenser_Installation__c p2 :postlist){
      if(p2.Post_Date__c !=null){
      dtime.add(p2.Post_Date__c);
      }
      }
   
    list<Asset_Type__c> plist = new list<Asset_Type__c>();
   map<id,list<Asset_Type__c>> dmap = new map<id,list<Asset_Type__c>>();
   if(dtime.size()>0){
   plist = [select id,Dispenser_Installation__c,Total_Cost__c from Asset_Type__c where Dispenser_Installation__c IN:did and createddate <:dtime[0]];
   }
   else{
    plist = [select id,Dispenser_Installation__c,Total_Cost__c from Asset_Type__c where Dispenser_Installation__c IN:did];
   }
   if(plist.size()>0){
  for(Asset_Type__c p : plist){
    if(!dmap.containskey(p.Dispenser_Installation__c))
     dmap.put(p.Dispenser_Installation__c,new list<Asset_Type__c>{p});
     else
     dmap.get(p.Dispenser_Installation__c).add(p);
  } 
  list<Dispenser_Installation__c> dup = new list<Dispenser_Installation__c>();
   list<Dispenser_Installation__c> dlist = [select id,name,Budget_Cost__c,Estimated_Gross_Margin__c from Dispenser_Installation__c where id IN:did];
   // if(dlist.size()>0)
    for(Dispenser_Installation__c di: dlist){
       di.Budget_Cost__c=0;
       if(dmap.get(di.id).size()>0)
       for(Asset_Type__c pl : dmap.get(di.id)){
       if(pl.Total_Cost__c != null)
         di.Budget_Cost__c=di.Budget_Cost__c+pl.Total_Cost__c;
     
       }
         dup.add(di);
    }
    update dup;
    }
    list<Asset_Type__c> plist1  = new  list<Asset_Type__c>();
    map<id,list<Asset_Type__c>> dmap1 = new map<id,list<Asset_Type__c>>();
   if(dtime.size()>0){
   plist1 = [select id,Dispenser_Installation__c,Total_Cost__c from Asset_Type__c where Dispenser_Installation__c IN:did and createddate >=:dtime[0]];
   }
  
   if(plist1.size()>0){
  for(Asset_Type__c p : plist1){
    if(!dmap1.containskey(p.Dispenser_Installation__c))
     dmap1.put(p.Dispenser_Installation__c,new list<Asset_Type__c>{p});
     else
     dmap1.get(p.Dispenser_Installation__c).add(p);
  } 
  list<Dispenser_Installation__c> dup1 = new list<Dispenser_Installation__c>();
   list<Dispenser_Installation__c> dlist1 = [select id,name,Real_Cost__c,Estimated_Gross_Margin__c,Budget_Cost__c from Dispenser_Installation__c where id IN:did];
   if(dlist1.size()>0)
    for(Dispenser_Installation__c di: dlist1){
       di.Real_Cost__c=0;
       di.Asset_Type_Check__c =true;
       if(dmap1.get(di.id).size()>0)
       for(Asset_Type__c pl : dmap1.get(di.id)){
       if(pl.Total_Cost__c !=null)
         di.Real_Cost__c=di.Real_Cost__c+pl.Total_Cost__c;
     
       }
       if(di.Real_Cost__c !=null && di.Budget_Cost__c !=null)
       di.Real_Cost__c=di.Real_Cost__c+di.Budget_Cost__c;
         dup1.add(di);
    }
    update dup1;
    }

}