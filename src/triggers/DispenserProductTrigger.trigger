trigger DispenserProductTrigger on Concrete_Dispenser_Product__c (after insert,after update) {
   list<id> did = new list<id>();
   for(Concrete_Dispenser_Product__c p2 : trigger.new){
      if(p2.Dispenser_Installation__c !=null){
      did.add(p2.Dispenser_Installation__c);
      }
   }
   map<id,list<Concrete_Dispenser_Product__c>> dmap = new map<id,list<Concrete_Dispenser_Product__c>>();
   list<Concrete_Dispenser_Product__c> plist = [select id,Dispenser_Installation__c,Estimated_Annual_Sales__c,Gross_Margin__c from Concrete_Dispenser_Product__c where Dispenser_Installation__c IN:did];
  for(Concrete_Dispenser_Product__c p : plist){
    if(!dmap.containskey(p.Dispenser_Installation__c))
     dmap.put(p.Dispenser_Installation__c,new list<Concrete_Dispenser_Product__c>{p});
     else
     dmap.get(p.Dispenser_Installation__c).add(p);
  } 
  list<Dispenser_Installation__c> dup = new list<Dispenser_Installation__c>();
   list<Dispenser_Installation__c> dlist = [select id,name,Total_Estimated_Gross_Sales__c,Estimated_Gross_Margin__c from Dispenser_Installation__c where id IN:did];
    for(Dispenser_Installation__c di: dlist){
       di.Total_Estimated_Gross_Sales__c=0;
       di.Estimated_Gross_Margin__c=0;
       for(Concrete_Dispenser_Product__c pl : dmap.get(di.id)){
       di.Total_Estimated_Gross_Sales__c = di.Total_Estimated_Gross_Sales__c ==null?0:di.Total_Estimated_Gross_Sales__c;
      pl.Estimated_Annual_Sales__c = pl.Estimated_Annual_Sales__c ==null?0:pl.Estimated_Annual_Sales__c;
         di.Total_Estimated_Gross_Sales__c=di.Total_Estimated_Gross_Sales__c+pl.Estimated_Annual_Sales__c;
         di.Estimated_Gross_Margin__c = di.Estimated_Gross_Margin__c ==null?0:di.Estimated_Gross_Margin__c;
      pl.Gross_Margin__c = pl.Gross_Margin__c ==null?0:pl.Gross_Margin__c;
       di.Estimated_Gross_Margin__c=di.Estimated_Gross_Margin__c+pl.Gross_Margin__c;
       }
         dup.add(di);
    }
    update dup;

}